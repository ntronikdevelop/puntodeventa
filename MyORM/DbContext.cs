﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Linq;
using System.Collections;
using System.Linq.Expressions;
using System.Data.OracleClient;
using System.Threading;
using System.Threading.Tasks;

public static class DbContext
{
    public static string connectionString = "";

    public static Server DataBaseServer = Server.MYSQLSERVER;
    public static IDbConnection con = null;

    static IDbConnection GetConnection(string connection)
    {
        if (con != null) return con;

        if (DataBaseServer == Server.MSSQLSERVER)
            return new SqlConnection(connection);
        else if (DataBaseServer == Server.MYSQLSERVER)
            return new MySqlConnection(connection);
        else if (DataBaseServer == Server.ORACLESERVER)
            return new OracleConnection(connection);

        return new SqlConnection(connection);
    }

    static IDbCommand GetCommand()
    {
        if (DataBaseServer == Server.MSSQLSERVER)
            return new SqlCommand();
        else if (DataBaseServer == Server.MYSQLSERVER)
            return new MySqlCommand();
        else if (DataBaseServer == Server.ORACLESERVER)
            return new OracleCommand();

        return new SqlCommand();
    }

    static IDbDataParameter GetParameter(string name, object value)
    {
        if (DataBaseServer == Server.MSSQLSERVER)
            return new SqlParameter(name, value);
        else if (DataBaseServer == Server.MYSQLSERVER)
            return new MySqlParameter(name, value);
        else if (DataBaseServer == Server.ORACLESERVER)
            return new OracleParameter(name, value);

        return new SqlParameter(name, value);
    }
    /// <summary>
    /// Llena las propiedades del objeto que coincidan con los campos retornados en el DataReader
    /// </summary>
    /// <param name="reader">Objeto que contiene los datos</param>
    /// <param name="obj">Objeto donde seran seteados los valores</param>
    /// <returns>Returna un valor que indica si se completo con exito la operacion</returns>
    public static bool FillObject(IDataReader reader, object obj)
    {
        try
        {
            if (reader.IsClosed)
                return false;

            var propiedades = obj.GetType().GetProperties();
            bool tieneReg = reader.Read();

            if (tieneReg)
            {
                foreach (PropertyInfo p in propiedades)
                {
                    try
                    {
                        object val = null;
                        Type tipo = p.PropertyType;

                        if (p.PropertyType.IsGenericType)
                        {
                            tipo = p.PropertyType.GetGenericArguments()[0];
                        }

                        val = Convert.ChangeType(reader[p.Name], tipo);


                        p.SetValue(obj, val, null);
                    }
                    catch { }

                }
            }
            return true;
        }
        catch (Exception)
        {

            return false;
        }

    }

 
    public static T FillObject<T>(IDataReader reader)
    {
        var obj = CrearTipo<T>();
        FillObject(reader, obj);
        return obj;
    }

    /// <summary>
    /// Crea una lista de objetos que estan contenidos en el reader
    /// </summary>
    /// <typeparam name="T">Objeto que sera retornado con los valores</typeparam>
    /// <param name="reader">Objeto que contiene la lisa de valores</param>
    /// <returns>Returna una lista de objetos</returns>
    public static List<T> LIstaObject<T>(IDataReader reader)
    {
        if (reader.IsClosed)
            return new List<T>();

        var lista = new List<T>();
        var tobj = typeof(T);
        var propiedades = tobj.GetProperties();
        while (reader.Read())
        {
            T obj = CrearTipo<T>();

            if (tobj.IsValueType || tobj.Name == "String")
            {
                if (tobj.IsGenericType)
                    tobj = tobj.GetGenericArguments()[0];

                if (reader[0] != DBNull.Value)
                    obj = (T)Convert.ChangeType(reader[0], tobj);
                lista.Add(obj);
                break;
            }

            SetValue(propiedades, reader, obj);

            lista.Add(obj);
        }

        return lista;
    }


    public static void SetValue(PropertyInfo[] propiedades, IDataReader reader, object obj)
    {
        foreach (PropertyInfo p in propiedades)
        {
            try
            {
                object val = null;
                Type tipo = p.PropertyType;

                if (tipo.IsClass && tipo.Name != "String")
                {
                    object newObj = Activator.CreateInstance(tipo);
                    p.SetValue(obj, newObj, null);
                    SetValue(tipo.GetProperties(), reader, newObj);
                    continue;
                }
                else if (p.PropertyType.IsGenericType)
                {
                    tipo = p.PropertyType.GetGenericArguments()[0];
                }

                if (reader.GetOrdinal(p.Name) >= 0)
                    val = Convert.ChangeType(reader[p.Name], tipo);

                p.SetValue(obj, val, null);
            }
            catch { }
        }
    }

    public static T Find<T>(int id)
    {
        Type tipo = typeof(T);
        var p = tipo.GetProperties().FirstOrDefault(a => a.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0);

        if (p == null) throw new MemberAccessException("El objeto no posee una llave primaria");
        return Consulta<T>(string.Format("select * from {0} where {1}={2}", tipo.Name, p.Name, id)).FirstOrDefault();
    }

    public static T Find<T>(string id)
    {
        Type tipo = typeof(T);
        var p = tipo.GetProperties().FirstOrDefault(a => a.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0);

        if (p == null) throw new MemberAccessException("El objeto no posee una llave primaria");
        return Consulta<T>(string.Format("select * from {0} where {1}='{2}'", tipo.Name, p.Name, id)).FirstOrDefault();
    }



    public static List<T> Entities<T>()
    {
        Type tipo = typeof(T);
        return Consulta<T>(string.Format("select * from {0}", tipo.Name));
    }

    public static List<T> EntitiesPaged<T>(int page, int size)
    {
        Type tipo = typeof(T);
        return Consulta<T>(string.Format("select * from {0}  limit {1},{2} ", tipo.Name,((page-1)*size),size));
    }

    public static List<T> EntitiesPaged<T>(int page, int size, bool activos)
    {
        Type tipo = typeof(T);
        return Consulta<T>(string.Format("select * from {0} where Activo={3} limit {1},{2} ", tipo.Name, ((page - 1) * size), size,Convert.ToInt16(activos)));
    }

    public static List<T> EntitiesPagedAndFiltered<T>(int page, int size, bool activos, string campo,int tipoCampo, string valor, out string queryString)
    {
        string queryDeFiltro = string.Empty;
        Type tipo = typeof(T);
        string query = string.Format("select * from {0} where Activo={1}  ", tipo.Name,  Convert.ToInt16(activos));
        if (!string.IsNullOrEmpty(valor) && !string.IsNullOrEmpty(campo))
        {
            query += " AND " + campo +" ";
            queryDeFiltro+= " where Activo= " + Convert.ToInt16(activos) +" AND "+ campo + " ";
            switch (tipoCampo)
            {
                case 0:
                    query += "like '%" + valor + "%' ";
                    queryDeFiltro += "like '%" + valor + "%' ";
                    break;
                case 2:
                    DateTime fechaIni = Convert.ToDateTime(valor).Date;
                    query += $">= '{fechaIni.ToString("yyyy-MM-dd HH:mm:ss")}' AND {campo} < '{fechaIni.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")}'";
                    queryDeFiltro += $">= '{fechaIni.ToString("yyyy-MM-dd HH:mm:ss")}' AND {campo} < '{fechaIni.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")}'";
                    break;
                case 3:
                    query += "= '" + valor + "' ";
                    queryDeFiltro += "= '" + valor + "' ";
                    break;
                default:
                    query += "= " + valor + " ";
                    queryDeFiltro+= "= " + valor + " ";
                    break;
            }
        }
        queryString = queryDeFiltro;
        query += "order by id desc limit " + Convert.ToString(((page - 1) * size)) + " ,  " + Convert.ToString(size);

        return Consulta<T>(query);
    }

    public static string GenerarFiltroBusqueda(string campo, int tipoCampo, string valor)
    {
        

        string query = string.Empty;
        if (!string.IsNullOrEmpty(valor) && !string.IsNullOrEmpty(campo))
        {
            query += " AND " + campo + " ";
           
            switch (tipoCampo)
            {
                case 0:
                    query += "like '%" + valor + "%' ";
                  
                    break;
                case 2:
                    DateTime fechaIni = Convert.ToDateTime(valor).Date;
                    query += $">= '{fechaIni.ToString("yyyy-MM-dd HH:mm:ss")}' AND {campo} < '{fechaIni.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")}'";
                  
                    break;
                case 3:
                    query += "= '" + valor + "' ";
                   
                    break;
                default:
                    query += "= " + valor + " ";
                  
                    break;
            }
        }
        return query;
    }

    public static int GetTableCount<T>()
    {
        Type tipo = typeof(T);
        return Consulta<int>(string.Format("select Count(*) from {0}", tipo.Name)).FirstOrDefault();
    }

    public static int GetTableCountForFilter<T>(string filter)
    {
        Type tipo = typeof(T);
        string query = string.Format("select Count(*) from {0}", tipo.Name);
        query += (" " + filter);
        return Consulta<int>(query).FirstOrDefault();
    }
    public static int GetTableCount<T>(string query)
    {
        Type tipo = typeof(T);
        return Consulta<int>(string.Format(query, tipo.Name)).FirstOrDefault();
    }

    public static int GetTableCountByExpression<T>(Expression<Func<T, bool>> exp)
    {
        Type tipo = typeof(T);
        string textCommand = new QueryTranslator().Translate(exp);
        return Consulta<int>(string.Format("select Count(*) from {0} where {1}", tipo.Name, textCommand)).FirstOrDefault();
    }

    /// <summary>
    /// Ejecuta un query y retorna los datos con el tipo especificado
    /// </summary>
    /// <typeparam name="T">Tipo de objeto que sera retornado</typeparam>
    /// <param name="cons">Query que contiene la instruccion</param>
    /// <returns>Returna una lista de objetos</returns>

    public static List<T> Consulta<T>(string cons)
    {
    
            IDbCommand cmd = GetCommand();
            cmd.CommandText = cons;
            return Consulta<T>(cmd);  
    }

    public static List<T> Consulta<T>(Expression<Func<T,bool>> exp)
    {
        Type tipo = typeof(T);
        string textCommand = new QueryTranslator ().Translate(exp);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0} where {1}",tipo.Name, textCommand);
        return Consulta<T>(cmd);
    }
    public static List<T> ConsultaUltimoRegistro<T>()
    {
        Type tipo = typeof(T);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0}   order by Id Desc limit 1", tipo.Name);
        return Consulta<T>(cmd);
    }


    public static List<T> Consulta<T>(Expression<Func<T, bool>> exp,int cantidadMax)
    {
        Type tipo = typeof(T);
        string textCommand = new QueryTranslator().Translate(exp);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0} where {1} limit 0,{2}", tipo.Name, textCommand, cantidadMax);
        return Consulta<T>(cmd);
    }

    public static List<T> Consulta<T>(int cantidadMax, string NombreCampo, string caracteresABuscar)
    {
        Type tipo = typeof(T);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0} where {1} like '%{2}%' and Activo=1 limit 0,{3}", tipo.Name, NombreCampo, caracteresABuscar, cantidadMax);
        return Consulta<T>(cmd);
    }

    public static List<T> Consulta<T>(int cantidadMax, string NombreCampo,bool activos, string caracteresABuscar)
    {
        int activo = activos ? 1 : 0;
        Type tipo = typeof(T);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0} where {1} like '%{2}%' and Activo={4} limit 0,{3}", tipo.Name, NombreCampo, caracteresABuscar, cantidadMax, activo);
        return Consulta<T>(cmd);
    }

    /// <summary>
    /// Lista de elementos paginado con consulta
    /// </summary>
    /// <typeparam name="T">Tipo de objeto retornado</typeparam>
    /// <param name="exp">expresion de consulta</param>
    /// <returns>retorna lista de objetos T</returns>
    public static List<T> ConsultaPaginado<T>(Expression<Func<T, bool>> exp, int page, int pageZise)
    {
        Type tipo = typeof(T);
        string textCommand = new QueryTranslator().Translate(exp);
        IDbCommand cmd = GetCommand();
        cmd.CommandText = string.Format("select * from {0} where {1} limit {2},{3} ", tipo.Name, textCommand,((page-1)* pageZise),pageZise);
        return Consulta<T>(cmd);
    }

    /// <summary>
    /// Ejecuta un query y retorna los datos con el tipo especificado
    /// </summary>
    /// <typeparam name="T">Tipo de objeto que sera retornado</typeparam>
    /// <param name="cons">Query que contiene la instruccion</param>
    /// <param name="tp">Tipo de comando que sera ejecutado</param>
    /// <returns>Returna una lista de objetos</returns>
    public static List<T> Consulta<T>(string cons, CommandType tp)
    {
        IDbCommand cmd = GetCommand();
        cmd.CommandType = tp;
        cmd.CommandText = cons;
        return Consulta<T>(cmd);
    }

    /// <summary>
    /// Ejecuta un query y retorna los datos con el tipo especificado
    /// </summary>
    /// <typeparam name="T">Tipo de objeto que sera retornado</typeparam>
    /// <param name="cmd">Objeto Command que ejecuta la instruccion</param>
    /// <returns>Returna una lista de objetos</returns>
    public static List<T> Consulta<T>(IDbCommand cmd)
    {
        using (IDbConnection con = GetConnection(connectionString)) 
        {
            cmd.Connection = con;
            con.Open();

            var result = cmd.ExecuteReader();
            var lista = LIstaObject<T>(result);
            con.Close();
            return lista;
        }
    }

    public static T Insert<T>(T obj)
    {
        Type tipo = typeof(T);
        PropertyInfo llave = null;
        PropertyInfo identity = null;
        var con = GetConnection(connectionString);
        var command = GetCommand();
        command.Connection = con;

        string query = string.Format("insert into {0} (", tipo.Name);
        string queryValues = " values (";

        string queryReturn = "Select * from " + tipo.Name;
        queryReturn += " where ";

        foreach (var property in tipo.GetProperties())
        {
            if (property.PropertyType.IsClass && property.PropertyType != typeof(String))
                continue;

            if (property.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0)
                llave = property;

            if (property.GetCustomAttributes(typeof(AutoGenerated), true).Length > 0)
            {
                identity = property;
                continue;
            }




            query += property.Name + ",";
            queryValues += string.Format("@{0},", property.Name);
            command.Parameters.Add(GetParameter("@" + property.Name, property.GetValue(obj, null)));
        }

        query = query.Substring(0, query.Length - 1) + ")";
        queryValues = queryValues.Substring(0, queryValues.Length - 1) + "); SELECT @@identity AS id;";

        command.CommandText = query + queryValues;
        con.Open();

        int id = int.Parse(command.ExecuteScalar().ToString());
        con.Close();

        if (id > 0 && llave != null && identity != null && llave.Name == identity.Name)
        {
            queryReturn += string.Format("{0}={1};", llave.Name, id);
            llave.SetValue(obj, id, null);
            var _obj = Consulta<T>(queryReturn).FirstOrDefault();
            obj = _obj != null ? _obj : obj;
        }
        con.Dispose();
        return obj;
    }

    public static bool Update<T>(T obj)
    {
        Type tipo = typeof(T);
        var con = GetConnection(connectionString);
        var command = GetCommand();
        command.Connection = con;

        string query = string.Format("update {0} set ", tipo.Name);
        string queryValues = " where ";

        foreach (var property in tipo.GetProperties())
        {
            if (property.PropertyType.IsClass && property.PropertyType != typeof(String))
                continue;

            if (property.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0)
            {
                queryValues += string.Format("{0}=@{0} and ", property.Name);
                command.Parameters.Add(GetParameter("@" + property.Name, property.GetValue(obj, null)));
                continue;
            }
            if (property.GetCustomAttributes(typeof(AutoGenerated), true).Length > 0)
            {
                continue;
            }

            query += string.Format("{0}=@{0},", property.Name);
            command.Parameters.Add(GetParameter("@" + property.Name, property.GetValue(obj, null)));
        }

        query = query.Substring(0, query.Length - 1) + "";
        queryValues = queryValues.Substring(0, queryValues.Length - 4) + ";";

        command.CommandText = query + queryValues;
        con.Open();

        command.ExecuteNonQuery();
        con.Close();
        con.Dispose();
        return true;
    }

    public static void Delete<T>(int id)
    {
        Type tipo = typeof(T);
        var p = tipo.GetProperties().FirstOrDefault(a => a.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0);

        if (p == null) throw new MemberAccessException("El objeto no posee una llave primaria");
        Consulta<T>(string.Format("delete from {0} where {1}={2}", tipo.Name, p.Name, id)).FirstOrDefault();
    }

    public static void Delete<T>(T obj)
    {
        Type tipo = typeof(T);
        var p = tipo.GetProperties().FirstOrDefault(a => a.GetCustomAttributes(typeof(PrimaryKey), true).Length > 0);

        if (p == null) throw new MemberAccessException("El objeto no posee una llave primaria");
        Consulta<T>(string.Format("delete from {0} where {1}={2}", tipo.Name, p.Name, p.GetValue(obj, null))).FirstOrDefault();
    }


    public static T CrearTipo<T>()
    {

        Type tipo = typeof(T);

        if (tipo.Name == "String")
            return (T)Activator.CreateInstance(tipo, new object[] { new char[] { } });
        else
            return (T)Activator.CreateInstance(tipo);
    }

}


public enum Server
{
    MSSQLSERVER,
    ORACLESERVER,
    MYSQLSERVER
}

public class AutoGenerated : Attribute
{ }

public class PrimaryKey : Attribute
{ }

