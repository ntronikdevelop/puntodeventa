﻿/*   version 2.2.2
*
*/
function LoadingShow() {
    $("#loading").fadeIn();
}

function LoadingHide() {
    $("#loading").hide();
}

$(document).ready(function () {


    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').click(function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Fullscreen ibox function
    $('.fullscreen-link').click(function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        $('body').toggleClass('fullscreen-ibox-mode');
        button.toggleClass('fa-expand').toggleClass('fa-compress');
        ibox.toggleClass('fullscreen');
        setTimeout(function() {
            $(window).trigger('resize');
        }, 100);
    });

    // Close menu in canvas mode
    $('.close-canvas-menu').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    // Open close right sidebar
    $('.right-sidebar-toggle').click(function () {
        $('#right-sidebar').toggleClass('sidebar-open');
    });

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    });

    // Open close small chat
    $('.open-small-chat').click(function () {
        $(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
        $('.small-chat-box').toggleClass('active');
    });

    // Initialize slimscroll for small chat
    $('.small-chat-box .content').slimScroll({
        height: '234px',
        railOpacity: 0.4
    });

    // Small todo handler
    $('.check-link').click(function () {
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });

    // Append config box / Only for demo purpose
    // Uncomment on server mode to enable XHR calls
    $.get("skin-config.html", function (data) {
        if (!$('body').hasClass('no-skin-config'))
            $('body').append(data);
    });

    // Minimalize menu
    $('.navbar-minimalize').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    // Tooltips demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body");

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
        }

    }

    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });

    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    $("[data-toggle=popover]")
        .popover();

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })
});


// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});

// Local Storage functions
// Set proper body class and plugins based on user configuration
$(document).ready(function () {
    if (localStorageSupport) {

        var collapse = localStorage.getItem("collapse_menu");
        var fixedsidebar = localStorage.getItem("fixedsidebar");
        var fixednavbar = localStorage.getItem("fixednavbar");
        var boxedlayout = localStorage.getItem("boxedlayout");
        var fixedfooter = localStorage.getItem("fixedfooter");

        var body = $('body');

        if (fixedsidebar == 'on') {
            body.addClass('fixed-sidebar');
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }

        if (collapse == 'on') {
            if (body.hasClass('fixed-sidebar')) {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }
            } else {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }

            }
        }

        if (fixednavbar == 'on') {
            $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
            body.addClass('fixed-nav');
        }

        if (boxedlayout == 'on') {
            body.addClass('boxed-layout');
        }

        if (fixedfooter == 'on') {
            $(".footer").addClass('fixed');
        }
    }
});

// check if browser support HTML5 local storage
function localStorageSupport() {
    return (('localStorage' in window) && window['localStorage'] !== null)
}

// For demo purpose - animation css script
function animationHover(element, animation) {
    element = $(element);
    element.hover(
        function () {
            element.addClass('animated ' + animation);
        },
        function () {
            //wait for animation to finish before removing classes
            window.setTimeout(function () {
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

// Dragable panels
function WinMove() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8
        })
        .disableSelection();
}


function ConvertGrid() {
    $('#GridTable').dataTable({
        "aoColumnDefs": [{
            "aTargets": [0]
        }],
        "oLanguage": {
            "sLengthMenu": "Muestra _MENU_ Filas",
            "sSearch": "Buscar:",
            "sInfo": "Mostrando pagina _TOTAL_ de (_START_ de _END_)",
            "sZeroRecords": "No Datos Encontrado",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siquiente"
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 10,
    });
}


function ConvertGridPrint(titlePrint) {
 
    $('#GridTable').dataTable({
        "aoColumnDefs": [{
            "aTargets": [0]
        }],


        "sDom": 'T<"clear">lfrtip',
        "oTableTools":
        {
            "aButtons":
            [
                {
                    "sExtends": "print",
                    "sMessage": titlePrint,
                    "sButtonText": "Imprimir",
                    "sInfo": ""
                }
            ]


        },

        "oLanguage": {
            "sLengthMenu": "Muestra _MENU_ Filas",
            "sSearch": "Buscar:",
            "sInfo": "Mostrando pagina _TOTAL_ de (_START_ de _END_)",
            "sZeroRecords": "No Datos Encontrado",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siquiente"
            }
        },
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "iDisplayLength": 10,
    });
}
function MesEnLetra(mes) {

    var _months = [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
    ];
    return  _months[mes-1];

}




/// version 2.0 pujol ado jqueryiputmask 
function putmask() {

    $(":input[datatype]").each(function () {
        
        var value = $(this).attr("datatype");
        
        switch (value) {
            case "number":
                $(this).inputmask('Regex', { regex: "^[0-9]*$" });
                break;
            case "int":
                $(this).inputmask("integer");
                break;
            case "decimal":
                $(this).inputmask({
                    groupSeparator: ",",
                    removeMaskOnSubmit: true,
                    autoUnmask: true,
                    allowMinus: false,
                    allowPlus: false,
                    autoGroup: true,
                    alias: 'decimal'
                });
                break;
            default:
        }
        
        
    });

}


function removemask() {

    $(":input[datatype]").each(function() {
        $(this).inputmask("remove");
    });
}

function parseJsonDate(jsonDateString)
{
    //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

    //var d = new Date(date || parseInt(jsonDateString.replace('/Date(', ''))),
    var d = new Date(parseInt(jsonDateString.replace('/Date(', ''))),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');



}

function ChangeSolicitud(Solicitud, Estatus, link)
{
    $.get(rootDir + "/Solicitud/CambioEstatus",
          {
              "Solicitud": Solicitud,
              "Estatus":Estatus
           },
function (data)
{
    //console.log(data);
    if (data != "False")
    {
        if (link == null) {
            location.reload();
        } else {
           location.href = link;
        }


    }

}).always(function () {
    LoadingHide();
});


}

function FillDropDownList(data, objecHtml) {

    objecHtml.find("option").remove();
    for (var i = 0; i < data.length; i++) {
        objecHtml.append("<option value='" + data[i].Id + "'>" + data[i].Text + "</option>");
    }

    objecHtml.select2({
        placeholder: "Selecione una opcion",
        allowClear: false,
        width: 200
    });

}

function FillSelectList(data, objecHtml, select2) {

    objecHtml.find("option").remove();
    for (var i = 0; i < data.length; i++) {

        objecHtml.append("<option value='" + data[i].Value + "'>" + data[i].Text + "</option>");
    }

    if (select2 != false) {
        objecHtml.select2({
            placeholder: "Selecione una opcion",
            allowClear: true,
            width: '100%'
        });
        objecHtml.val("").trigger("change");
    }

}


(function () {
    try {

        console.log("%c¡Detente Cuidado!", "color: Red; font-size: 90px; font-weight: bold;");
        console.log("%cEsta función del navegador está pensada para desarrolladores. Si alguien te indicó que copiaras y pegaras algo aquí para habilitar una función o para \"piratear\" la cuenta de alguien, se trata de un fraude.", "color: blue; font-size: x-large;  ");

        var $_console$$ = console;
        Object.defineProperty(window, "console", {
            get: function () {
                if ($_console$$._commandLineAPI)
                    throw "Sorry, for security reasons, the script console is deactivated ";
                return $_console$$;
            },
            set: function ($val$$) {
                $_console$$ = $val$$;
            }
        });
    } catch ($ignore$$) {
    }
})();