﻿var reporteVentas = angular.module('cxcGeneral', []);
reporteVentas.controller('cxcGeneralController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.reporteGeneral = {};
    $scope.date = new Date();






    $scope.BuscarFacturas = function () {
        var error = $scope.frmReporteVentasForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        $scope.Print = function () {
            window.print();
        }


        showLoading();
        let toPost = $scope.busqueda;
        toPost.cliente = null;
        toPost.vendedor = null;
        apiService.post("/api/Factura/ReporteCxC", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {
        var error = $scope.frmReporteVentasForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        empty = { fechaInicial: '', fechaFinal: '', clienteId: null, monedaId: null }
        $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial ? $scope.busqueda.fechaInicial : '';
        $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal ? $scope.busqueda.fechaFinal : '';
        let toPost = $scope.busqueda;
        toPost.cliente = null;
        toPost.vendedor = null;
        apiService.download('api/Factura/ReporteCxCExcel', toPost, empty, 'ReporteCXC.xls', 'POST');


    }



    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.cliente = newValue.originalObject;
            $scope.busqueda.clienteId = newValue.originalObject.id;
        }
        else {
            $scope.busqueda.cliente = null;
            $scope.busqueda.clienteId = null;
        }


    }

    $scope.selectedVendedor = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.vendedor = newValue.originalObject;
            $scope.busqueda.vendedorId = newValue.originalObject.id;
        }
        else {
            $scope.busqueda.vendedor = null;
            $scope.busqueda.vendedorId = null;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            let facturasPorMonedas = result.data.data[0].facturasPorMonedas;
            for (let i = 0; i < facturasPorMonedas.length; i++) {
                let facturas = [];
                for (let j = 0; j < facturasPorMonedas[i].facturas.length; j++) {

                    const clienteIndex = facturas.findIndex(x => x.clienteId == facturasPorMonedas[i].facturas[j].clienteId)
                    if (clienteIndex >= 0) {
                        facturas[clienteIndex].facturas.push(facturasPorMonedas[i].facturas[j]);
                        facturas[clienteIndex].totalItbis += facturasPorMonedas[i].facturas[j].montoItbis;
                        facturas[clienteIndex].totalFacturas += facturasPorMonedas[i].facturas[j].montoTotal;
                        facturas[clienteIndex].totalPagados += facturasPorMonedas[i].facturas[j].montoPagado;
                        facturas[clienteIndex].totalAdeudado += facturasPorMonedas[i].facturas[j].montoAdeudado;
                    }
                    else
                        facturas.push({
                            nombreCliente: facturasPorMonedas[i].facturas[j].clienteNombre,
                            clienteId: facturasPorMonedas[i].facturas[j].clienteId,
                            facturas: [facturasPorMonedas[i].facturas[j]],
                            totalItbis: facturasPorMonedas[i].facturas[j].montoItbis,
                            totalFacturas: facturasPorMonedas[i].facturas[j].montoTotal,
                            totalPagados: facturasPorMonedas[i].facturas[j].montoPagado,
                            totalAdeudado: facturasPorMonedas[i].facturas[j].montoAdeudado
                        });
                }
                facturasPorMonedas[i]["facturasPorCliente"] = facturas;
            }
            $scope.reporteGeneral.reportePorMonedas = facturasPorMonedas;
        }



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    $scope.obtenerSaldoPorAntiguedad = function (reporte, inicio, fin) {
        let resultado = 0;
        let facturas = reporte.facturas;
        if (inicio && fin) {
            resultado = facturas.filter(x => x.cantidadDias > inicio && x.cantidadDias <= fin).sum('montoAdeudado');
        }
        else if (!inicio, fin) {
            resultado = facturas.filter(x => x.cantidadDias <= fin).sum('montoAdeudado');
        }
        else if (inicio, !fin) {
            resultado = facturas.filter(x => x.cantidadDias > inicio).sum('montoAdeudado');
        }

        return resultado;
    }


    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;
    }

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal', 'busqueda.cliente', 'busqueda.vendedor'], function (newValue, oldValue) {
        if ($scope.seleccion == "CxCGeneral")
            $scope.BuscarFacturas();
    });
}]);