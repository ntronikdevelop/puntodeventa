﻿/// <reference path="../../Views/Facturacion/CreacionDetalle.html" />
var creacionFacturaController = angular.module('CreacionFacturaController', []);

creacionFacturaController.controller('CrearFactura', ['$scope', 'ApiService', '$window', 'ModalUtils', 'FileUploader', '$timeout', function ($scope, ApiService, $window, ModalUtils, FileUploader, $timeout) {
    var apiService = new ApiService();
    $scope.TempKey = Math.random().toString();
    $scope.currentModel = {
        Id: 0,
        Concepto: "",
        Detalle: "",
        IdCliente: 0,
        TipoServicioFacturacionId: 0,
        DireccionArchivo: "",
        DetalleInstruccionFacturacions: new Array(),
        FechaSolicitudServicio: "",
        FechaTerminoServicio: "",
        NumeroReferencia: "",
        Comentario: "",
        FechaRecepcionDocumentacion: "",
        FechaEnvioInstruccionACobros: "",
        DatosAdicionales: "",
        CreadoPor: "",
        EmpresaId: 0,
        DepartamentoId: 0,
        ModificadoPor: "",
        Version: 0,
        Archivo: null,
        TempKey: $scope.TempKey,
        AdjuntosInstruccionFacturacion: new Array(),
        EstadoInstruccionFacturacionId: 0
    };
    $scope.EsSeleccion = false;
    $scope.EsAdjuntado = false;
    $scope.esEdicion = false;
    $scope.Clientes = [];
    $scope.clienteBls = [];
    VerifyId();
    ObtenerMonedas();
    ObtenerTarifarioInst();
    ObtenerEmpresas();
    ObtenerDepartamentos();
    $scope.modal = new ModalUtils();
    ObtenerEstados();
    ObtenerCategorias();
    cargarMonedas();
    $scope.ArchivosSeleccionados = new Array();

    // SECCION DE FUNCIONES

    $scope.LimpiarCampos = function (tipoFormObj) {
        $scope.currentModel.Concepto = tipoFormObj != undefined ? tipoFormObj.Descripcion : "";
        $scope.currentModel.DetalleInstruccionFacturacions = new Array();
    }

    $scope.MostrarDetallesDeFacturaEnOrden = function (detalleFactura) {
        var detallesDefinidos = $scope.tipoServicioFacturacionObj != undefined ? $scope.tipoServicioFacturacionObj.CamposLista : [];
        var result = new Array();

        if (detallesDefinidos.length > 0)
            for (var i = 0; i < detallesDefinidos.length; i++) {
                result.push({ Campo: detallesDefinidos[i].NombreCampo, Valor: detalleFactura[detallesDefinidos[i].NombreCampo] })
            }
        return result;
    };

    $scope.VerificarTipoServicio = function () {
        if ($scope.tipoServicioFacturacionObj != undefined && $scope.clienteObj != undefined ||
            ($scope.clienteInicial != undefined)) {
            var clienteObjId = $scope.clienteObj != undefined ? $scope.clienteObj.originalObject != undefined ? $scope.clienteObj.originalObject.Id : 0 : 0;
            var clienteInicialId = $scope.clienteInicial != undefined ? $scope.clienteInicial.Id : 0;

            if (clienteObjId > 0 || clienteInicialId > 0)
                return false;
            return true;
        }
        return true;

    }


    $scope.fileSeleccionado = function (newValue, oldValue) {

        if (newValue) {
            var file = newValue.originalObject;
            $scope.currentModel.CodigoFile = file.CodigoFile;
            $scope.barcoSelected = "(" + file.Barco.nombre + " - " + file.Puerto.Codigo + ")"
            $scope.fechaSelected = file.FechaLlegada != null ? file.FechaLlegada : "";
            $scope.Titulo = 'Fecha de llegada: ' + ($scope.fechaSelected != undefined && $scope.fechaSelected != "" ? ArreglarFecha($scope.fechaSelected).toLocaleDateString() : "");
            $scope.sinFile = false;
        }
        else {
            $scope.currentModel.CodigoFile = null;
            $scope.barcoSelected = "";
            $scope.fechaSelected = null;
            $scope.Titulo = null;
            $scope.sinFile = true;
        }
        $scope.clienteBls = [];
        cargarBls();

        console.log($scope.facturaDetalle);

    }

    $scope.clienteSeleccionado = function (newValue, oldValue) {

        if (newValue) {
            var cliente = newValue.originalObject;
            $scope.currentModel.IdCliente = cliente.Id;
            $scope.sinCliente = false;
        }
        else {
            $scope.currentModel.IdCliente = null;
            $scope.sinCliente = true;
        }
        $scope.clienteBls = [];
        cargarBls();
        console.log($scope.facturaDetalle);

    }

    function cargarBls() {
        $scope.currentModel.Bls = [];
        $scope.clienteBls = [];
        $timeout(function () {
            $("#blsFactura").trigger("chosen:updated");
        }, 0);
        if (!$scope.currentModel.IdCliente || !$scope.currentModel.CodigoFile)
            return;

        var data = {
            "ClienteEmbarcador.Id": $scope.currentModel.IdCliente,
            "ManifiestoFile": $scope.currentModel.CodigoFile
        }
        var url = "/FACTURACION/bls/ObtenerTodos";

        apiService.get(url, data).then(function (result) {
            $scope.clienteBls = result.data.listResult;

            $timeout(function () {
                $("#blsFactura").trigger("chosen:updated");
            }, 0);

        });
    }



    $scope.GuardarInstruccionFacturacion = function () {
        LlamarLoading();
        var error = $scope.frmInstruccion.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            QuitarLoading();
            mostrarErrores();
            return;
        }


        $scope.currentModel.TipoServicioFacturacionId = $scope.tipoServicioFacturacionObj.Id;
        $scope.currentModel.IdCliente = $scope.currentModel.IdCliente == 0 ? null : $scope.currentModel.IdCliente;
        $scope.currentModel.DepartamentoId = $scope.departamentoObj.Id;
        $scope.currentModel.CategoriaFacturacionId = $scope.categoriaFacturaObj.Id;
        $scope.currentModel.EmpresaId = $scope.empresaObj.Id;
        $scope.currentModel.EstadoInstruccionFacturacionId = $scope.currentModel.Id <= 0 ? 1 : $scope.estadoFacturaObj.Id;
        var instruccionFactura = $scope.currentModel;
        instruccionFactura.FechaEnvioInstruccionACobros = ArreglarFecha(instruccionFactura.FechaEnvioInstruccionACobros);
        instruccionFactura.FechaRecepcionDocumentacion = ArreglarFecha(instruccionFactura.FechaRecepcionDocumentacion);
        instruccionFactura.FechaSolicitudServicio = ArreglarFecha(instruccionFactura.FechaSolicitudServicio);
        instruccionFactura = EstablecerInstruccionComoFacturadoPorCategoria(instruccionFactura);

        $scope.instruccionFactura = instruccionFactura;
        $scope.uploader.formData = [{ tempId: $scope.TempKey }];

        if ($scope.uploader.queue.length > 0) {
            LlamarLoading();
            $scope.uploader.uploadAll();
        }
        else {
            LlamarLoading();
            $scope.EjecutarPost();
        }



        return true;
    }

    function ArreglarFecha(date) {
        var newDate = date != null ? (date.toString().indexOf('Date') >= 0 ?
       $scope.ConvertToDate(date) : $scope.ConvertToJDate(date)) : null;
        return newDate;
    }

    $scope.VerificarAdjuntos = function () {
        return $scope.currentModel.AdjuntosInstruccionFacturacion.length > 0 ? false : true;
    }

    $scope.BusquedaCliente = function (nombreCliente, list) {
        var call = $.ajax({
            url: GetBaseUrl() + '/Facturacion/cliente/ObtenerPorNombre',
            data: { campo: 'Nombre', valor: "'" + nombreCliente },
            async: false
        });
        list = JSON.parse(call.responseText).listResult.DeserializedData;
        var filter = list.filter(t => t.Nombre.toLowerCase().includes(nombreCliente.toLowerCase())).slice(0, 5);
        return filter;

    }

    $scope.ClienteActualPorCampos = function (campo, valor) {
        var promise = apiService.get('/Facturacion/cliente/ObtenerPorNombre', { campo: campo, valor: valor });
        promise.then(function (result) {
            $scope.clienteInicial = result.data.listResult.DeserializedData[0];

        });
        return promise;
    }

    $scope.BaseUrl = function (ruta) {
        return "http://" + window.location.hostname + ":" + location.port + ruta;
    }
    function GetBaseUrl() {
        return "http://" + window.location.hostname + ":" + location.port;
    }

    $scope.DescargarArchivo = function (adjuntoObj) {
        var url = '/Facturacion/Facturacion/Download/';
        var promise = apiService.get(url, adjuntoObj);
        promise.then(function (result) {
            var pepe = result;

        });
    }

    $scope.AgregarDetalle = function () {

        var detalleEnBlanco =
            {
                Id: 0,
                IdInstruccionFacturacion: $scope.Id,
                IdDetalleManifiesto: 0,
                Peso: 0,
                Cantidad: 0,
                IdCliente: $scope.IdCliente,
                IdClienteString: "",
                IdLinea: 0,
                IdLineaString: "",
                IdServicio: 0,
                IdServicioString: "",
                IdTarifarioInst: 0,
                Tarifa: 0,
                Desde: 0,
                Hasta: 0,
                IdMoneda: 0,
                IdMonedaString: "",
                IdAgencia: 0,
                IdAgenciaString: "",
                Total: 0,
                IdPorCuentaDe: 0
            };
        $scope.nuevoDetalle = detalleEnBlanco;
        var detalleTemp = $scope.nuevoDetalle;
        $scope.viejoDetalle = detalleTemp;
        $scope.modal.openLarge('/Areas/Facturacion/Content/modals/CreacionDetalle.html', $scope);
    }

    $scope.EditarDetalleFactura = function (detalleFactura) {
        $scope.viejoDetalle = detalleFactura;
        $scope.nuevoDetalle = detalleFactura;
        $scope.modal.openLarge('/Areas/Facturacion/Content/modals/CreacionDetalle.html', $scope);
    }

    $scope.BorrarDetalleFactura = function (index) {
        $scope.currentModel.DetalleInstruccionFacturacions.splice(index, 1);
    }

    $scope.ConvertToDate = function (csDate) {

        if (csDate != null && csDate != undefined && csDate != "") {
            var dateNumeric = parseInt(csDate.substring(6, csDate.length - 2));
            return new Date(dateNumeric);

        }
        return "";
    }

    $scope.ConvertToJDate = function (csDate) {
        if (csDate instanceof Date) {
            if (csDate != null && csDate != undefined && csDate != "") {
                return ((csDate.getMonth() + 1) + '/' + csDate.getDate() + '/' + csDate.getFullYear());
            }
        }

        return "";
    }

    function VerifyId() {
        var currentId = $("#Id").val();
        if (currentId != 0 && currentId != undefined && currentId != null && currentId != "0") {
            ObtenerFactura(currentId);
        }
    }



    function ObtenerFactura(id) {
        var promise = apiService.get('/Facturacion/Facturacion/ObtenerFactura/' + id, null);
        promise.then(function (result) {
            var newDate = $scope.ConvertToDate(result.data.factura.FechaTerminoServicio);
            newDate.setDate(newDate.getDate() + 1);
            result.data.factura.FechaTerminoServicio = newDate;
            $scope.currentModel = result.data.factura;
            $scope.currentModel.AdjuntosInstruccionFacturacion.length > 0 ? $scope.ActivarEsAdjuntado() : $scope.ActivarEsSeleccion();
            $scope.currentModel.TempKey = $scope.TempKey;
            $scope.clienteObj = result.data.factura.Cliente;
            $scope.clienteActual = $scope.clienteObj;
            $scope.tipoServicioFacturacionObj = $scope.TiposServicios != undefined ? $scope.TiposServicios.filter(t=> t.Id == $scope.currentModel.TipoServicioFacturacionId)[0] : null;
            $scope.ClienteActualPorCampos('Id', $scope.currentModel.IdCliente);
            $scope.empresaObj = $scope.Empresas != undefined ? $scope.Empresas.filter(t=> t.Id == $scope.currentModel.EmpresaId)[0] : null;
            $scope.departamentoObj = $scope.Departamentos != undefined ? $scope.Departamentos.filter(t=> t.Id == $scope.currentModel.DepartamentoId)[0] : null;
            $scope.estadoFacturaObj = $scope.EstadosFactura != undefined ? $scope.EstadosFactura.filter(t=> t.Id == $scope.currentModel.EstadoInstruccionFacturacionId)[0] : null;
            $scope.categoriaFacturaObj = $scope.CategoriasFactura != undefined ? $scope.CategoriasFactura.filter(t=> t.Id == $scope.currentModel.categoriaFacturacionId)[0] : null;
            $scope.esEdicion = true;
        });
        return promise;
    }

    $scope.ActivarEsAdjuntado = function () {
        $scope.EsAdjuntado = true;
        $scope.EsSeleccion = false;
    }
    $scope.ActivarEsSeleccion = function () {
        $scope.EsAdjuntado = false;
        $scope.EsSeleccion = true;
    }

    function ObtenerClientes() {
        var promise = apiService.get('/Facturacion/Cliente/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.Clientes = result.data.listResult.DeserializedData;
            $scope.clienteObj = $scope.Clientes != undefined ? $scope.Clientes.filter(t=> t.Id == $scope.currentModel.IdCliente)[0] : null;

        });
        return promise;
    }

    function ObtenerEmpresas() {
        var data = { fields: "Id,Nombre" };
        var promise = apiService.get('/Facturacion/Empresa/ObtenerTodos', data);
        promise.then(function (result) {
            $scope.Empresas = result.data.DeserializedData;
            $scope.empresaObj = $scope.Empresas != undefined ? $scope.Empresas.filter(t=> t.Id == $scope.currentModel.EmpresaId)[0] : null;

        });
        return promise;
    }


    function ObtenerDepartamentos() {
        var promise = apiService.get('/Facturacion/Departamento/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.Departamentos = result.data.listResult.DeserializedData;
            $scope.departamentoObj = $scope.Departamentos != undefined ? $scope.Departamentos.filter(t=> t.Id == $scope.currentModel.DepartamentoId)[0] : null;

        });
        return promise;
    }

    $scope.ObtenerDepartamentosPorEmpresa = function (campo) {
        var promise = apiService.get('/Facturacion/Departamento/ObtenerPorEmpresa', { campo: campo, valor: $scope.empresaObj.Id });
        promise.then(function (result) {
            $scope.Departamentos = result.data.listResult.DeserializedData;
            $scope.departamentoObj = $scope.Departamentos != undefined ? $scope.Departamentos.filter(t=> t.Id == $scope.currentModel.DepartamentoId)[0] : null;
        });
        return promise;
    }

    function ObtenerMonedas() {
        var promise = apiService.get('/Facturacion/Moneda/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.Monedas = result.data.listResult.DeserializedData;
        });
        return promise;
    }

    function ObtenerTarifarioInst() {
        var promise = apiService.get('/Facturacion/TarifarioInstruccionesFacturacion/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.TarifarioInst = result.data.listResult.DeserializedData;

        });
        return promise;
    }

    function ObtenerDetalleManifiesto() {
        var promise = apiService.get('/Facturacion/ManifiestoDetalle/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.DetalleManifiesto = result.data.listResult.DeserializedData;

        });
        return promise;
    }


    $scope.ObtenerTiposServicios = function (empresaId) {
        var data = {
            fields: "Id,NombreTipoServicio,Descripcion,AplicaCliente,AplicaCodigoFile",
            Id: empresaId
        };
        var promise = apiService.get('/Facturacion/Empresa/TipoServicioFacturacion', data);
        promise.then(function (result) {
            $scope.TiposServicios = result.data.DeserializedData;
            $scope.tipoServicioFacturacionObj = $scope.TiposServicios != undefined ? $scope.TiposServicios.filter(t=> t.Id == $scope.currentModel.TipoServicioFacturacionId)[0] : null;
            $scope.currentModel.Concepto = $scope.tipoServicioFacturacionObj != undefined ? $scope.tipoServicioFacturacionObj.Descripcion : "";

        });
        return promise;
    }

    function ObtenerEstados() {
        var promise = apiService.get('/Facturacion/EstadoInstruccionFacturacion/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.EstadosFactura = result.data.listResult.DeserializedData;
            $scope.estadoFacturaObj = $scope.EstadosFactura != undefined ? $scope.EstadosFactura.filter(t=> t.Id == $scope.currentModel.EstadoInstruccionFacturacionId)[0] : null;
        });
        return promise;

    }
    function ObtenerCategorias() {
        var promise = apiService.get('/Facturacion/CategoriasFactura/ObtenerTodos', null);
        promise.then(function (result) {
            $scope.CategoriasFactura = result.data.listResult.DeserializedData;
            $scope.categoriaFacturaObj = $scope.CategoriasFactura != undefined ? $scope.CategoriasFactura.filter(t=> t.Id == $scope.currentModel.CategoriaFacturacionId)[0] : null;
        });
        return promise;
    }

    function cargarMonedas() {
        var data = {};
        apiService.get("/facturacion/moneda/obtenertodos", data)
            .then(llenarMonedas);
    }

    function llenarMonedas(result) {
        $scope.monedas = result.data.listResult.DeserializedData;
    }

    function SuccessAction(response) {
        QuitarLoading();
         bootbox.alert(response.data.Message);
        $scope.uploader.clearQueue();
        $scope.ArchivosSeleccionados = new Array();
        $("#FileSelected").val('');

        if (response.data.Status == "Ok") {
            var endpoint = "http://" + window.location.hostname + ":" + location.port;
            window.location.href = endpoint + '/Facturacion/Facturacion/Editar/' + response.data.Id;
        }

    }
    function ErrorAction(response) {
        QuitarLoading();
        $scope.uploader.clearQueue();
        $("#FileSelected").val('');
         bootbox.alert('Hubo un error al procesar su solicitud. Favor intentar mas tarde o contactar administrador.');
    }

    $scope.DetallesTableDetails = function (detalle, campo) {
        var proccessTemp;
        var toShow = detalle[campo];
        switch (campo) {
            case 'IdLinea':
                proccessTemp = $scope.Lineas != undefined ? $scope.Lineas.filter(t=> t.Id == detalle.IdLinea)[0] : undefined;
                toShow = proccessTemp != undefined ? proccessTemp.Nombre : "";
                break;
            case 'IdMoneda':
                proccessTemp = $scope.Monedas != undefined ? $scope.Monedas.filter(t=> t.Id == detalle.IdMoneda)[0] : undefined;
                toShow = proccessTemp != undefined ? proccessTemp.Codigo : "";
                break;
            case 'IdAgencia':
                proccessTemp = $scope.Agencias != undefined ? $scope.Agencias.filter(t=> t.Id == detalle.IdAgencia)[0] : undefined;
                toShow = proccessTemp != undefined ? proccessTemp.Nombre : "";
                break;


        }
        return toShow;
    }

    $scope.VerificarFecha = function (fecha) {
        if (fecha == undefined || fecha == "" || fecha == null)
            return true;
        return false;
    }
    $scope.fileCounter = 0;

    $scope.uploader = new FileUploader({
        url: GetBaseUrl() + '/Facturacion/Facturacion/UploadFile',
        formData: [{ tempId: $scope.currentModel.TempKey }],
        removeAfterUpload: true,
        onCompleteAll: function () {
            $scope.uploader.clearQueue();
            $scope.EjecutarPost();
        },
        onErrorItem: function (item, response, status, headers) {
            $scope.uploader.clearQueue();
            $("#FileSelected").val('');
             bootbox.alert('Hubo un error al procesar su solicitud. Favor intentar mas tarde o contactar administrador.');


        },
        onSuccessItem: function (item, response, status, headers) {
            QuitarLoading();
        },
        onAfterAddingFile: function (item) {
            $scope.ActivarEsSeleccion();
            var fileObj = { NombreArchivo: item.file.name };
            $scope.ArchivosSeleccionados.push(fileObj);
        },
        filters: [
            {
                name: 'TamanoArchivosValido', fn: function (item) {
                    var maxSize = 2097152;
                    var validation = item.size <= maxSize;
                    if (!validation)
                         bootbox.alert('El  maximo del archivo no debe de pasar los ' + (maxSize / 1048576).toString() + 'MB');
                    return validation;
                }
            },
              {
                  name: 'ExtensionesArchivoValidas', fn: function (item) {
                      validExtensions = ['pdf', 'xlsx', 'xls', 'txt', 'doc', 'docx', 'jpg'];
                      item.name = item.name.toLowerCase();
                      var nameArr = item.name.split('.');
                      var extension = nameArr[nameArr.length - 1];
                      if (validExtensions.indexOf(extension) >= 0)
                          return true;
                       bootbox.alert('El archivo no es válido.');
                      return false;
                  }
              }
        ]
    });

    $scope.ReiniciarSeleccion = function () {
        $scope.ArchivosSeleccionados = new Array();
        $scope.uploader.queue = new Array();
    }

    $scope.EjecutarPost = function () {
        LlamarLoading();
        var addUrl = '/Facturacion/Facturacion/Crear/';
        var editUrl = '/Facturacion/Facturacion/Editar/';
        apiService.post($scope.currentModel.Id == 0 ? addUrl : editUrl, $scope.instruccionFactura, SuccessAction, ErrorAction);
    }


    function GetCurrentUrl() {
        var baseurl = "http://" + window.location.hostname + ":" + location.port;
        var addUrl = '/Facturacion/Facturacion/Crear/';
        var editUrl = '/Facturacion/Facturacion/Editar/';
        var currentUrl = $scope.currentModel.Id == 0 ? addUrl : editUrl;
        baseurl = baseurl + currentUrl;
        return baseurl;
    }

    function EstablecerInstruccionComoFacturadoPorCategoria(instruccion) {
        if ($scope.categoriaFacturaObj.CrearComoFactura != undefined && $scope.categoriaFacturaObj.CrearComoFactura != null && $scope.categoriaFacturaObj.CrearComoFactura) {
            instruccion.Facturado = true;
            var estado = $scope.EstadosFactura.filter(function (estado) {
                return estado.NombreEstado.toLowerCase() == 'facturado'
            });
            instruccion.EstadoInstruccionFacturacionId = estado[0].Id;
        }
        return instruccion;
    }

    // FIN SECCION DE FUNCIONES

    $scope.IsEditing = function () {
        return $scope.currentModel.Id > 0 ? false : true;
    }


}])