﻿var gastoAgendasFormController = angular.module('NavegationApp');
gastoAgendasFormController.controller('gastoAgendaFormController', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();

    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var gastoAgendasUrl = '/api/gastoagendado';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.busquedaCompleja = modalFunctions.busquedaCompleja;
    $scope.id = modalFunctions.Id;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.gastoAgenda = {
        id: 0,
        suplidorId: 0,
        activo: true,
        monedaId: 0,
        referenciagastoAgenda: '',
        suplidor: null,
        moneda: null,
        montoPagado: 0,
        montoDevuelto: 0,
        montoTotal: 0,
        montoItbis: 0,
        montoGrabado: 0,
        estado: '',
        montoDado: 0,
        montoAdeudado: 0,
        NCF:0
    };
    cargarLocalidades();
    cargarMonedas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    verificargastoAgenda($scope.id);
    $scope.closeDialoggastoAgenda = function () {
        $uibModalInstance.close();
    }
    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.gastoAgenda.localidad = $scope.localidades.filter(t => t.id == $scope.gastoAgenda.localidadId)[0];

        }, submitgastoAgendaError);
    }

    $scope.mostrarDetalles = true;

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.gastoAgenda.moneda = $scope.monedas.filter(t=> t.id == $scope.gastoAgenda.monedaId)[0];

        }, submitgastoAgendaError);
    }


    $scope.agregarImpuesto = function (impuesto,monto) {
        $scope.gastoAgenda.impuestos = $scope.gastoAgenda.impuestos ? $scope.gastoAgenda.impuestos : [];
        if (impuesto && monto && monto>0) {
            const index = $scope.gastoAgenda.impuestos.findIndex(x => x.impuesto.id == impuesto.id);
            if (index < 0)
                $scope.gastoAgenda.impuestos.push({
                    id: 0,
                    impuestoId: impuesto.id,
                    montoImpuesto: monto,
                    impuesto: impuesto
                });
            else {
                $scope.gastoAgenda.impuestos[index].montoImpuesto = monto;
            }
        }
        let montoImpuestoTotal = $scope.gastoAgenda.impuestos.sum("montoImpuesto");
        $scope.gastoAgenda.montoItbis = montoImpuestoTotal;

    }

    $scope.anularImpuesto = function (index) {
        $scope.gastoAgenda.impuestos = $scope.gastoAgenda.impuestos ? $scope.gastoAgenda.impuestos : [];
        $scope.gastoAgenda.impuestos.splice(index, 1);
        let montoImpuestoTotal = $scope.gastoAgenda.impuestos.sum("montoImpuesto");
        $scope.gastoAgenda.montoItbis = montoImpuestoTotal;
    }

    $scope.guardargastoAgenda = function () {
        var error = $scope.frmgastoAgendaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        if ($scope.gastoAgenda.montoPagado > ($scope.gastoAgenda.montoTotal)) {
            bootbox.alert("El monto pagado no debe de exceder el monto total del gastoAgenda.");
            return;
        }

        showLoading();
        var toPost = $scope.gastoAgenda;
        toPost.fechaEmision = $scope.fechaSeleccionada;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/gastoagendado", toPost, submitgastoAgendaSuccess, submitgastoAgendaError);
                break;
            default:
                apiService.put("/api/gastoagendado", toPost, submitgastoAgendaSuccess, submitgastoAgendaError);
                break;
        }

    }

   

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.gastoAgenda.suplidor = newValue.originalObject;
            $scope.gastoAgenda.suplidorId = newValue.originalObject.id;
        }
        else {
            $scope.gastoAgenda.suplidor = null;
            $scope.gastoAgenda.suplidorId =0;
        }


    }




    function submitgastoAgendaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(gastoAgendasUrl + '/BusquedaCompleja');

        $scope.showValidate = false;
    }

    function submitgastoAgendaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function verificargastoAgenda(dataId) {
        if (dataId > 0) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/gastoagendado", { id: dataId })
                      .then(llenargastoAgenda, submitgastoAgendaError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenargastoAgenda(result) {
        hideLoading();
        $scope.gastoAgenda = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.gastoAgenda;
        $scope.selectedSuplidor({ originalObject: $scope.gastoAgenda.suplidor }, null);
        $scope.fechaSeleccionada = new Date($scope.gastoAgenda.fechaEmision);
    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.gastoAgendas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalgastoAgendas = result.data.pagedData.count;
        }, submitgastoAgendaError);
        return promise;

    }

    $scope.$watch('gastoAgenda.montoDado', function (newValue, oldValue) {
        $scope.gastoAgenda.montoPagado = $scope.gastoAgenda.montoDado;
        $scope.gastoAgenda.montoDevuelto = $scope.gastoAgenda.montoDado - $scope.gastoAgenda.montoTotal;
        $scope.gastoAgenda.montoAdeudado = $scope.gastoAgenda.montoTotal - $scope.gastoAgenda.montoPagado;
        if ($scope.gastoAgenda.montoRecibido >= ($scope.gastoAgenda.montoTotal)) {
            $scope.gastoAgenda.montoAdeudado = 0;
            $scope.gastoAgenda.montoPagado = $scope.gastoAgenda.montoTotal;

        }


    });

    $scope.$watch('gastoAgenda.montoTotal', function (newValue, oldValue) {
        $scope.gastoAgenda.montoPagado = $scope.gastoAgenda.montoDado;
        $scope.gastoAgenda.montoDevuelto = $scope.gastoAgenda.montoDado - $scope.gastoAgenda.montoTotal;
        $scope.gastoAgenda.montoAdeudado = $scope.gastoAgenda.montoTotal - $scope.gastoAgenda.montoPagado;
        if ($scope.gastoAgenda.montoDado >= ($scope.gastoAgenda.montoTotal)) {
            $scope.gastoAgenda.montoAdeudado = 0;
            $scope.gastoAgenda.montoPagado = $scope.gastoAgenda.montoTotal;

        }


    });



    $scope.$watch('gastoAgenda.moneda', function (newValue, oldValue) {
        let ultimaFechaActualizacion = newValue != undefined ? newValue.fechaModificacion : null;
        let notUpdated = newValue != undefined ? newValue.tasaActualizada : null;
        if (ultimaFechaActualizacion != null && !newValue.esMonedaLocal == true)
            $scope.verificarFechaDeActualizacionDeMoneda(ultimaFechaActualizacion, notUpdated);
    });



    $scope.$watchGroup(['gastoAgenda.montoGrabado', 'gastoAgenda.montoItbis'], function (newVal, oldVal) {
        $scope.gastoAgenda.montoTotal = $scope.gastoAgenda.montoGrabado + $scope.gastoAgenda.montoItbis;
    });


});