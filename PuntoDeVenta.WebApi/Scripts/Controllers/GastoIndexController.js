﻿var gastoIndexController = angular.module('gastoIndexController', []);
gastoIndexController.controller('TablaDeGastosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var gastosUrl = '/api/gasto';
    var gastosAgendadosUrl = '/api/gastoagendado';
    $scope.resultadoEnPantalla = { gastos: [], totalgastos: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    $scope.filtroBusqueda = [
        { descripcion: 'No. Referencia', campo: 'ReferenciaGasto', tipoCampo: '0' },
        { descripcion: 'Suplidor', campo: 'SuplidorId', tipoCampo: '1' },
        { descripcion: 'Fecha', campo: 'FechaEmision', tipoCampo: '2' },
        { descripcion: 'Estado ', campo: 'Estado', tipoCampo: '3' }
    ]

    $scope.estados = [{ descripcion: 'TODOS', valorEstado: "" }, { descripcion: 'Ingresado', valorEstado: "i" }, { descripcion: 'ANULADO', valorEstado: "a" }, { descripcion: 'PAGADO', valorEstado: "p" }]

    $scope.valorBusqueda = { campo: '', valor: '' };

    $scope.busquedaCompleja = {
        page: $scope.resultadoEnPantalla.currentPage,
        size: 10,
        activo: !$scope.inactivos,
        filtros: new Array()
    };

    $scope.verificarBusquedaCompleja = function () {
        return ($scope.busquedaCompleja && $scope.busquedaCompleja.filtros && $scope.busquedaCompleja.filtros.length > 0);
    }

    $scope.crearAgendados = function () {
        $scope.search();
        //apiService.post(`${gastosAgendadosUrl}/CrearGastosAgendados`, {}, function () {
        //    $scope.search();
        //}, ErrorResult);


    }

    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.gastos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalgastos = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }
    $scope.ObtenerSoloFecha = function (date) {
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
        return (day + '-' + monthIndex + '-' + year);
    }

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.valorBusqueda.valor = newValue.originalObject.id;
            $scope.valorBusqueda.descripcionValor = newValue.originalObject.nombre;
            $scope.agregarFiltro();

        }
        else {
            $scope.valorBusqueda.valor = null;
            $scope.valorBusqueda.descripcionValor = null;
        }


    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'gastos')
            $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.editarGasto = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/gastoForm?data=00046120", "gastoFormController", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, busquedaCompleja: $scope.busquedaCompleja });
    }



    $scope.borrarGasto = function (Id) {
        bootbox.confirm('Esta seguro de eliminar el registro del gasto?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = gastosUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    $scope.search();
                }, ErrorResult);
            }
        })

    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = gastosUrl;


        llenarTabla(urlAUsar + '/BusquedaCompleja');
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
    }

    $scope.agregarFiltro = function () {
        let aGuardar = {};
        angular.copy($scope.valorBusqueda, aGuardar);
        if ($scope.busquedaCompleja.filtros) {
            let index = $scope.busquedaCompleja.filtros.findIndex(x => x.campo == aGuardar.campo);
            if (index >= 0) {
                $scope.busquedaCompleja.filtros.splice(index, 1);
            }
            $scope.busquedaCompleja.filtros.push(aGuardar);
        }
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'suplidor');
        return;
    }

    $scope.borrarFiltro = function (index) {
        $scope.busquedaCompleja.filtros.splice(index, 1);
    }

    $scope.init = function () {


        $q.all([
            $scope.crearAgendados()
        ])
    };
    if ($scope.seleccion == "Gastos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Gastos")
            $scope.crearAgendados();

    });

    $scope.obtenerEstadoDeGasto = function (estatus) {
        let resultado = '';
        estatus = estatus.toLowerCase();

        switch (estatus) {
            case 'i':
                resultado = "Ingresado";
                break;

            case 'a':
                resultado = "ANULADO";
                break;

            case 'p':
                resultado = "PAGADO";
                break;
            default:

                break;
        }

        return resultado;

    }


}]);