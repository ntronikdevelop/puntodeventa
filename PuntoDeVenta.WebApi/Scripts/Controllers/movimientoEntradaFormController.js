﻿var movimientoEntradasFormController = angular.module('NavegationApp');
movimientoEntradasFormController.controller('FormularioMovimientoEntrada', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var movimientoEntradasUrl = '/api/movimientoEntrada';
    var apiService = new ApiService();
    $scope.esEdicion = false;
    $scope.editandoMovimiento = false;
    hideLoading();
    $scope.localidades = new Array();
    $scope.almacenes = new Array();
    $scope.id = modalFunctions.Id;
    $scope.movimientoVacio = { cantidad: 0, monto: 0, montoItbis: 0, montoTotal: 0, localidadId: 0, productoId: 0, suplidorId: 0, almacenId: 0 };
    $scope.movimientoEntradaTemp = { cantidad: 0, monto: 0, montoItbis: 0, montoTotal: 0, localidadId: 0, productoId: 0, suplidorId: 0, almacenId: 0 };
    $scope.movimientoEntrada = { suplidor: null, localidad: null, referencia: null, almacen: null, movimientosEntradas: new Array() };
    $scope.resultadoEnPantalla = modalFunctions.ResultadoEnPantalla;
    cargarImpuestos();
    cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarMovimientoEntrada = function () {
        if (!$scope.movimientoEntrada.suplidor) {
            bootbox.alert('Favor seleccionar un suplidor');
            return;
        }

        if (!$scope.movimientoEntrada.almacen) {
            bootbox.alert('Favor seleccionar un almacen');
            return;
        }

        showLoading();
        var toPost = {};
        angular.copy($scope.movimientoEntrada, toPost);

        apiService.post("/api/movimientoEntrada/RegistrarEntradas", toPost, submitmovimientoEntradaSuccess, submitmovimientoEntradaError);




    }




    $scope.convertirListaEnTexto = function (lista) {
        var texto = "";

        for (var i = 0; i < lista.length; i++) {
            texto += texto == "" ? lista[i] : "," + lista[i];
        }

        return texto;
    }

    $scope.agregarMovimiento = function () {
        var error = $scope.frmMovimientoEntradaForm.$error;
        if (error.required || error["autocomplete-required"] || error.min) {
            $scope.showValidate = true;
            return;
        }


        let movimientoEntradaAGuardar = {};
        angular.copy($scope.movimientoEntradaTemp, movimientoEntradaAGuardar);
        var index = $scope.movimientoEntrada.movimientosEntradas.map(function (el) {
            return el.producto.id;
        }).indexOf(movimientoEntradaAGuardar.producto.id);

        if (index >= 0) {
            $scope.movimientoEntrada.movimientosEntradas.splice(index, 1);
            $scope.movimientoEntrada.movimientosEntradas.push(movimientoEntradaAGuardar);
        }

        else {
            $scope.movimientoEntrada.movimientosEntradas.push(movimientoEntradaAGuardar);
        }
        angular.copy($scope.movimientoVacio, $scope.movimientoEntradaTemp);
        $scope.$broadcast('angucomplete-alt:clearInput', 'productoNombre');
        $scope.editandoMovimiento = false;
    }

    $scope.EditarMovimiento = function (movimiento) {
        angular.copy(movimiento, $scope.movimientoEntradaTemp);
        $scope.editandoMovimiento = true;
    }

    $scope.AnularMovimiento = function (detalle) {
        var index = $scope.movimientoEntrada.movimientosEntradas.map(function (el) {
            return el.producto.id;
        }).indexOf(detalle.producto.id);
        $scope.movimientoEntrada.movimientosEntradas.splice(index, 1);
    }

    $scope.agregar = function (operacion) {
        var index = $scope.movimientoEntradaTemp.operaciones.indexOf(operacion);
        if (index >= 0) {
            $scope.movimientoEntradaTemp.operaciones.splice(index, 1);
        }
        else {
            $scope.movimientoEntradaTemp.operaciones.push(operacion);
        }
    }

    $scope.$watch('movimientoEntradaTemp.localidadId', function (newValue, oldValue) {
        if (newValue && newValue > 0)
            cargarAlmacenes(newValue);

    });

    $scope.$watch('movimientoEntradaTemp.costoActual', function (newValue, oldValue) {
        if (newValue && newValue > 0) {
            $scope.movimientoEntradaTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientoEntradaTemp.cantidad * $scope.movimientoEntradaTemp.costoActual);
            $scope.movimientoEntradaTemp.montoTotal = $scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual * (1 + $scope.impuesto.porciento));
        }

    });



    $scope.$watchGroup(['movimientoEntradaTemp.productoUnidad', 'movimientoEntradaTemp.cantidad'], function (newVal, oldVal) {
        if (newVal[0]) {
            // $scope.movimientoEntradaTemp.costoActual = newVal[0].precioCompra;
            $scope.movimientoEntradaTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientoEntradaTemp.cantidad * $scope.movimientoEntradaTemp.costoActual);
            $scope.movimientoEntradaTemp.montoTotal = $scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual * (1 + $scope.impuesto.porciento));

        }
        else {
            $scope.movimientoEntradaTemp.costoActual = 0;
            $scope.movimientoEntradaTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientoEntradaTemp.cantidad * $scope.movimientoEntradaTemp.costoActual);
            $scope.movimientoEntradaTemp.montoTotal = $scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual * (1 + $scope.impuesto.porciento));

        }
    });


    $scope.$watchGroup(['movimientoEntradaTemp.cantidad',
        'movimientoEntradaTemp.montoItbis'], function (newVal, oldVal) {

            // $scope.movimientoEntradaTemp.costoActual = newVal[0].precioCompra;
            $scope.movimientoEntradaTemp.montoTotal = ($scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual)) + $scope.movimientoEntradaTemp.montoItbis;


        });

    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            for (var i = 0; i < $scope.movimientoEntradaTemp.producto.productoUnidades.length; i++) {

                $scope.movimientoEntradaTemp.producto.productoUnidades[i].unidad = $scope.unidades.filter(t => t.id ==
                    $scope.movimientoEntradaTemp.producto.productoUnidades[i].unidadId)[0];
            }



        }, submitmovimientoEntradaError);
    }





    $scope.$watch('movimientoEntradaTemp.cantidad', function (newValue, oldValue) {


        if (newValue > 0 && $scope.movimientoEntradaTemp.producto) {
            $scope.movimientoEntradaTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientoEntradaTemp.cantidad * $scope.movimientoEntradaTemp.costoActual);
            $scope.movimientoEntradaTemp.montoTotal = $scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual * (1 + $scope.impuesto.porciento));
        }


    });




    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();

            $scope.localidades = result.data.data;
            if ($scope.localidades.length == 1) {
                $scope.movimientoEntrada.localidad = $scope.localidades[0];
                $scope.movimientoEntrada.localidadId = $scope.movimientoEntrada.localidad.id;
                $scope.movimientoEntradaTemp.localidadId = $scope.movimientoEntrada.localidad.id
            }


        }, submitmovimientoEntradaError);
    }

    function cargarAlmacenes(localidadId) {
        if (localidadId && localidadId > 0) {
            showLoading();
            apiService.get("/api/almacen/ObtenerAlmacenesPorLocalidad/" + localidadId, null).then(function (result) {
                hideLoading();
                $scope.almacenes = [{ id: 0, nombre: '' }];
                $scope.almacenes = result.data.data;


            }, submitmovimientoEntradaError);
        }

    }

    function cargarImpuestos() {

        showLoading();
        apiService.get("/api/impuesto", { pagesize: 1, nombreCampo: 'nombre', filtro: 'itbis' }).then(function (result) {
            hideLoading();

            $scope.impuesto = result.data.pagedData.result[0];


        }, submitmovimientoEntradaError);


    }

    function cargarProductoUnidades(productoId) {

        showLoading();
        apiService.get("/api/productounidades/obenerunidadesdeProducto/" + productoId, null).then(function (result) {
            hideLoading();


            $scope.movimientoEntradaTemp.producto.productoUnidades = result.data.data;

            cargarUnidades();
            if (!$scope.movimientoEntradaTemp.productoUnidad || !$scope.movimientoEntradaTemp.unidadId || $scope.movimientoEntradaTemp.unidadId <= 0)
            {
                $scope.movimientoEntradaTemp.productoUnidad = $scope.movimientoEntradaTemp.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                $scope.movimientoEntradaTemp.unidadId = $scope.movimientoEntradaTemp.productoUnidad.unidadId;
            }
        }, submitmovimientoEntradaError);


    }


    function submitmovimientoEntradaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            var urlAUsar = movimientoEntradasUrl;
            angular.copy($scope.movimientoVacio, $scope.movimientoEntrada);
            angular.copy({}, $scope.movimientoEntradaTemp);
            llenarTabla(urlAUsar);
            $uibModalInstance.close();

        }



        $scope.showValidate = false;
    }

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.movimientoEntrada.suplidor = newValue.originalObject;
            $scope.movimientoEntradaTemp.suplidorId = $scope.movimientoEntrada.suplidor.id;
        }
        else {
            $scope.movimientoEntrada.suplidor = null;
            $scope.movimientoEntradaTemp.suplidorId = 0;
        }


    }

    $scope.selectedProducto = function (newValue, oldValue) {

        if (newValue) {


            $scope.movimientoEntradaTemp.producto = newValue.originalObject;
            $scope.movimientoEntradaTemp.productoId = $scope.movimientoEntradaTemp.producto.id;
            cargarProductoUnidades($scope.movimientoEntradaTemp.productoId);
            $scope.movimientoEntradaTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientoEntradaTemp.cantidad * $scope.movimientoEntradaTemp.costoActual);
            $scope.movimientoEntradaTemp.montoTotal = $scope.movimientoEntradaTemp.cantidad * ($scope.movimientoEntradaTemp.costoActual * (1 + $scope.impuesto.porciento));
        }
        else {

            $scope.movimientoEntradaTemp.producto = null;
            $scope.movimientoEntradaTemp.productoId = 0;
        }


    }

    function submitmovimientoEntradaError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }





    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.movimientoEntradas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalMovimientoEntradas = result.data.pagedData.count;
        }, submitmovimientoEntradaError);
        return promise;
    }


    $scope.$watchGroup(['movimientoEntrada.suplidor', 'movimientoEntradaTemp.producto', 'movimientoEntradaTemp.productoUnidad'], function (newVal, oldVal) {

        if ($scope.movimientoEntrada.suplidor && $scope.movimientoEntradaTemp.producto && $scope.movimientoEntradaTemp.productoUnidad) {

            let costosProducto = null;
            if ($scope.movimientoEntradaTemp.producto.detalles && isJson($scope.movimientoEntradaTemp.producto.detalles)) {
                costosProducto = JSON.parse($scope.movimientoEntradaTemp.producto.detalles);
            }

            if (costosProducto) {
                let costo = costosProducto.filter(c => c.suplidorId == $scope.movimientoEntrada.suplidor.id)[0];
                if (!costo)
                    $scope.movimientoEntradaTemp.costoActual = $scope.movimientoEntradaTemp.producto.costo / $scope.movimientoEntradaTemp.productoUnidad.equivalencia;
                //PROBAR  PROBAR  PROBAR  PROBAR  PROBAR
                else
                $scope.movimientoEntradaTemp.costoActual = costo.costo / $scope.movimientoEntradaTemp.productoUnidad.equivalencia;
            }
        }
    });

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
});