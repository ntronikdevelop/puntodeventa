﻿var vendedorFormController = angular.module('NavegationApp');
vendedorFormController.controller('FormularioVendedores', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var vendedoresUrl = '/api/vendedor';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.valorBusqueda = modalFunctions.valorBusqueda;
    $scope.id = modalFunctions.Id;
    $scope.vendedor = { id: 0, nombre: '', cedula: '', activo: true, telefono: '', zona: 0 };
    cargarZonas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    verificarVendedor($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

  


    $scope.setZona = function (zona) {
        if (zona) {
            $scope.vendedor.zonaId = zona.id;
        }
        else {

            $scope.vendedor.zonaId = 0;
        }
    };


    //$scope.$watch('tiposNcf', function (newValue, oldValue) {
    //    if ($scope.tiposNcf && $scope.tiposNcf.length > 0) {
    //        let algo = [{ nombre: 'No aplica', tipo: null }];
    //      algo=  algo.concat($scope.tiposNcf);
    //        angular.copy(algo, $scope.tiposNcf);
    //    }
    //});

    $scope.guardarVendedor = function () {
        var error = $scope.frmVendedorForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.vendedor;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/vendedor", toPost, submitVendedoreSuccess, submitVendedorError);
                break;
            default:
                apiService.put("/api/vendedor", toPost, submitVendedoreSuccess, submitVendedorError);
                break;
        }

    }
    function cargarZonas() {
        showLoading();
        apiService.get("/api/zona", null).then(function (result) {
            hideLoading();
            $scope.zonas = result.data.data;
            $scope.vendedor.zona = $scope.zonas.filter(t=> t.id == $scope.vendedor.zonaId)[0];

        }, submitVendedorError);
    }


    function submitVendedoreSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(vendedoresUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.vendedor = {};

        }


        $scope.showValidate = false;
    }

    function submitVendedorError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function verificarVendedor(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/vendedor", { id: dataId })
                      .then(llenarVendedor, submitVendedorError);
        }
    }



    function llenarVendedor(result) {
        hideLoading();
        $scope.vendedor = result.data != undefined && result.data != null ? result.data.data[0] : $scope.vendedor;
        $scope.vendedor.zona = $scope.zonas.filter(t => t.id == $scope.vendedor.zonaId)[0];
    }

 

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: $scope.valorBusqueda.campo,
            tipoCampo: $scope.valorBusqueda.tipoCampo,
            valor: $scope.valorBusqueda.valor
        };

        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.vendedores = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalVendedores = result.data.pagedData.count;
        }, submitVendedorError);
        return promise;
    }
});