﻿var reporteCxP = angular.module('reporteCxP', []);
reporteCxP.controller('reporteCxPController', ['$scope', '$filter', "$q", 'ApiService', '$window', '$http', function ($scope, $filter, $q, ApiService, $window,$http) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/gasto';
    var apiService = new ApiService();
    $scope.reporteCXP = {};
    $scope.date = new Date();

 

    $scope.Print = function () {
        window.print();
    }


    $scope.buscarGastos = function () {
        var error = $scope.frmreporteCxPForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

      


        showLoading();
        var toPost = $scope.busqueda;
        apiService.post("/api/gasto/reporteCxP", toPost, submitFacturaSuccess, submitFacturaError);

    }

  
      $scope.exportarExcel = function () {

          empty = { fechaInicial: '', fechaFinal: '', suplidorId: null, almacenId: null };
          if ($scope.busqueda)
          {
              $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal == null ? "" : $scope.busqueda.fechaFinal;
              $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial == null ? "" : $scope.busqueda.fechaInicial;
          }

            apiService.download('api/Gasto/ReporteCxPExcel', $scope.busqueda, empty, 'Reporte CXP.xls', 'POST');
        }


    

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.suplidor = newValue.originalObject;
            $scope.busqueda.suplidorId = newValue.originalObject.id;
        }
        else {
            $scope.busqueda.suplidor = null;
            $scope.busqueda.suplidorId = null;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteCXP.reporteGastos = result.data.data[0];
        }
        else
            bootbox.alert(result.data.Mensaje);



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }








 

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal', 'busqueda.suplidor','busqueda.moneda'], function (newValue, oldValue) {
        if ($scope.seleccion == "reporteCxP")
            $scope.buscarGastos();
    });
}]);