﻿var reporteVentas = angular.module('estadoCuentaCliente', []);
reporteVentas.controller('estadoCuentaClienteController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.reporteGeneral = {};
    $scope.date = new Date();


    $scope.Print = function () {
        window.print();
    }



    $scope.buscarEstadoCuenta = function () {
        var error = $scope.frmestadoCuentaClienteForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        showLoading();
        var toPost = $scope.busqueda;
        apiService.post("/api/Factura/estadoCuenta", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {
        var error = $scope.frmestadoCuentaClienteForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

      let  empty = { fechaInicial: '', fechaFinal: '', clienteId: null, monedaId: null };
        $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial ? $scope.busqueda.fechaInicial : '';
        $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal ? $scope.busqueda.fechaFinal : '';
        let copy = $scope.busqueda;
        copy.cliente = null;
        apiService.download('api/Factura/estadoCuentaExcel', copy, empty, 'Estado de cuenta.xls', 'POST');


    }



    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.cliente = newValue.originalObject;
            $scope.busqueda.clienteId = newValue.originalObject.id;
        }
        else {
            $scope.busqueda.cliente = null;
            $scope.busqueda.clienteId = null;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteGeneral = result.data.data;
        }



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    $scope.obtenerSaldoPorAntiguedad = function (reporte, inicio, fin) {
        let resultado = 0;
        let facturas = reporte.facturas;
        if (inicio && fin) {
            resultado = facturas.filter(x => x.cantidadDias > inicio && x.cantidadDias <= fin).sum('montoAdeudado');
        }
        else if (!inicio, fin) {
            resultado = facturas.filter(x => x.cantidadDias <= fin).sum('montoAdeudado');
        }
        else if (inicio, !fin) {
            resultado = facturas.filter(x => x.cantidadDias > inicio).sum('montoAdeudado');
        }

        return resultado;
    }


    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;
    }

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal', 'busqueda.cliente'], function (newValue, oldValue) {
        if ($scope.seleccion == "EstadoCuentaCliente" && $scope.busqueda.cliente)
            $scope.buscarEstadoCuenta();
    });
}]);