﻿var FacturasFormController = angular.module('NavegationApp');
FacturasFormController.controller('FormularioFacturasAntiguas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.busquedaCompleja = modalFunctions.busquedaCompleja;
    $scope.id = modalFunctions.Id;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.factura = {
        id: 0,
        clienteId: 0,
        activo: true,
        monedaId: 0,
        numeroFactura: '',
        cliente: null,
        moneda: null,
        montoPagado: 0,
        montoRecibido: 0,
        montoTotal: 0,
        montoItbis: 0,
        montoGrabado: 0,
        estado: '',
        detallesFactura: []
    };
    cargarMonedas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarFactura($scope.id);
    $scope.closeDialogFactura = function () {
        $uibModalInstance.close();
    }

    $scope.mostrarDetalles = true;

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.factura.moneda = $scope.monedas.filter(t=> t.id == $scope.factura.monedaId)[0];

        }, submitFacturaError);
    }




    $scope.GuardarFactura = function () {
        var error = $scope.frmFacturaAntiguaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        if ($scope.factura.montoPagado > ($scope.factura.montoTotal)) {
            bootbox.alert("El monto pagado no debe de exceder el monto total de la factura.");
            return;
        }

        showLoading();
        var toPost = $scope.factura;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/factura/AgregarFacturaAntigua", toPost, submitFacturaSuccess, submitFacturaError);
                break;
            default:
                apiService.put("/api/factura/AgregarFacturaAntigua", toPost, submitFacturaSuccess, submitFacturaError);
                break;
        }

    }

   

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.factura.cliente = newValue.originalObject;
            $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? newValue.originalObject.moneda : $scope.factura.moneda;
            $scope.factura.monedaId = $scope.factura.moneda.id;
            $scope.factura.vendedor = !$scope.factura.vendedor ? $scope.factura.cliente.vendedor : $scope.factura.vendedor;
            $scope.factura.vendedorId = $scope.factura.vendedor ? $scope.factura.vendedor.id : null;
            $scope.aplicaItbis = $scope.factura.cliente.aplicarItbis;
            $scope.factura.clienteId = newValue.originalObject.id;
            $scope.factura.clienteId = newValue.originalObject.id;
            $scope.factura.rnc = $scope.factura.cliente.cedulaRnc;
            if ($scope.aplicaItbis && $scope.factura.cliente.tipoNcf)
                $scope.factura.controlNCF = $scope.controlNCFs.filter(t => t.tipo == $scope.factura.cliente.tipoNcf)[0];

        }
        else {
            $scope.factura.cliente = null;
            $scope.factura.clienteId = 0;
            $scope.factura.moneda = null;
            $scope.factura.monedaId = 0;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(facturasUrl + '/BusquedaCompleja');
        $window.open('/ImpresionFactura?id=' + result.data.status, '_blank');
        if (result.data.status >= 0 && $scope.id>0) {
            $uibModalInstance.close();
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.factura = {};
            $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
        }


        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarFactura(dataId) {
        if (dataId > 0) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/factura", { id: dataId })
                      .then(llenarFactura, submitFacturaError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarFactura(result) {
        hideLoading();
        $scope.factura = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.factura;
        $scope.selectedCliente({ originalObject: $scope.factura.cliente }, null);
        $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? $scope.factura.cliente.moneda : $scope.factura.moneda;
    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;

    }

    $scope.$watch('factura.montoRecibido', function (newValue, oldValue) {
        $scope.factura.montoPagado = $scope.factura.montoRecibido;
        $scope.factura.montoDevuelto = $scope.factura.montoRecibido - $scope.factura.montoTotal;
        $scope.factura.montoAdeudado = $scope.factura.montoTotal - $scope.factura.montoPagado;
        if ($scope.factura.montoRecibido >= ($scope.factura.montoTotal)) {
            $scope.factura.montoAdeudado = 0;
            $scope.factura.montoPagado = $scope.factura.montoTotal;

        }


    });

    $scope.$watch('factura.montoTotal', function (newValue, oldValue) {
        $scope.factura.montoPagado = $scope.factura.montoRecibido;
        $scope.factura.montoDevuelto = $scope.factura.montoRecibido - $scope.factura.montoTotal;
        $scope.factura.montoAdeudado = $scope.factura.montoTotal - $scope.factura.montoPagado;
        if ($scope.factura.montoRecibido >= ($scope.factura.montoTotal)) {
            $scope.factura.montoAdeudado = 0;
            $scope.factura.montoPagado = $scope.factura.montoTotal;

        }


    });



    $scope.$watch('factura.moneda', function (newValue, oldValue) {
        let ultimaFechaActualizacion = newValue != undefined ? newValue.fechaModificacion : null;
        let notUpdated = newValue != undefined ? newValue.tasaActualizada : null;
        if (ultimaFechaActualizacion != null && !newValue.esMonedaLocal == true)
            $scope.verificarFechaDeActualizacionDeMoneda(ultimaFechaActualizacion, notUpdated);
    });

    $scope.$watch('factura.notaCreditoAplicada', function (newValue, oldValue) {
        if (newValue && newValue != '') {
            let currentUrl = '/api/MovimientoDevolucionCliente/ObtenerNotaCredito/' + newValue;
            var promise = apiService.get(currentUrl, null);
            promise.then(function (result) {
                if (result.status >= 0) {
                    $scope.notaCreditoAplicada = result.data.data[0];
                    $scope.factura.montoNC = $scope.notaCreditoAplicada.monto;
                }
                else {
                    $scope.notaCreditoAplicada = { monto: 0 };
                    $scope.factura.montoNC = 0;
                }


            }, submitFacturaError);
        }
        else {
            $scope.notaCreditoAplicada = { monto: 0 };
            $scope.factura.montoNC = 0;
        }
    });

    $scope.$watchGroup(['factura.montoGrabado', 'factura.montoItbis'], function (newVal, oldVal) {
        $scope.factura.montoTotal = $scope.factura.montoGrabado + $scope.factura.montoItbis;
    });


});