﻿var devolucionClientesFormController = angular.module('NavegationApp');
devolucionClientesFormController.controller('FormularioDevolucionCliente', function ($scope, $filter, $q, ApiService, modalFunctions, $window, $uibModalInstance) {
    var devolucionClientesUrl = '/api/MovimientodevolucionCliente';
    var apiService = new ApiService();
    $scope.esEdicion = false;
    $scope.editandoMovimiento = false;
    hideLoading();
    cargarNcfs();
    $scope.id = modalFunctions.Id;
   

 
    $scope.resultadoEnPantalla = modalFunctions.ResultadoEnPantalla;

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.guardarDevolucionCliente = function () {
        var error = $scope.frmDevolucionClienteForm.$error;
        if (error.required || error["autocomplete-required"] || error.min) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = {};
        angular.copy($scope.devolucion, toPost);
        if ($scope.devolucion.detallesDevolucion.filter(x=>x.cantidadDevolucion > 0)[0] ==null) {
            hideLoading();
            bootbox.alert('Favor por lo menos indicar devolución de un articulo');
            return;
        }

        if ($scope.devolucion.detallesDevolucion.filter(x=>x.cantidadDevolucion > x.cantidad)[0] != null) {
            hideLoading();
            bootbox.alert('La cantidad de devolución no puede ser mayor que la cantidad facturada.');
            return;
        }

        apiService.post("/api/MovimientoDevolucionCliente/IngresarDevolucionCliente", toPost, submitdevolucionClienteSuccess, submitdevolucionClienteError);




    }





    function cargarNcfs() {
        showLoading();
        apiService.get("/api/controlNCF", null).then(function (result) {
            hideLoading();
            let tempArr = [{ tipo: null, nombre: 'No aplica', id: 0 }];
            $scope.controlNCFs = tempArr.concat(result.data.data);

        }, submitdevolucionClienteError);
    }
 

    $scope.editarDevolucion = function (devolucion) {
        angular.copy(devolucion, $scope.devolucion);
        $scope.esEdicion = true;
    }






    function submitdevolucionClienteSuccess(result) {
        hideLoading();
       
       
        if (result.data.status >= 0) {
            $window.open('/ImpresionEntrada/ImprimirNotaCredito?referencia=' + result.data.mensaje, '_blank');
            var urlAUsar = devolucionClientesUrl;
            llenarTabla(urlAUsar);
            $uibModalInstance.close();

        }
        else
            bootbox.alert(result.data.mensaje);



        $scope.showValidate = false;
    }


    $scope.selectedFactura = function (newValue, oldValue) {

        if (newValue) {
            
            let copiaFactura = newValue.originalObject;
            copiaFactura.tipoNCF = null;
            copiaFactura.controlNCF = null;
            copiaFactura.ncf = null;
            $scope.devolucion = {
                factura: copiaFactura,
                detallesDevolucion: copiaFactura.detallesFactura
            };
           
        }
        else {
           
            $scope.devolucion = {};
        }


    }

    function submitdevolucionClienteError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }





    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.devolucionesClientes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totaldevolucionesCliente = result.data.pagedData.count;
        },submitdevolucionClienteError);
        return promise;
    }





});