﻿var usuariosFormController = angular.module('NavegationApp');
usuariosFormController.controller('FormularioUsuarios', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var usuariosUrl = '/api/usuarios';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.id = modalFunctions.Id;
    $scope.usuario = { id: '', nombre: '', activoBool: true, activo:0, apellido: '', localidadId: 0, cedula: '', localidad:null, email:'', clave:''};
    $scope.cajas = [{ id: null, nombre: 'No Aplica' }];
   
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarUsuario($scope.id);
  
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarUsuario = function () {
        var error = $scope.frmUsuarioForm.$error;
        if (error.required || error["autocomplete-required"] || error.maxlength) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = $scope.usuario;
        toPost.activo = toPost.activoBool ? 1 : 0;
        switch ($scope.id) {
            case '':
                apiService.post("/api/usuarios", toPost, submitUsuarioSuccess, submitUsuarioError);
                break;
            default:
                apiService.put("/api/usuarios", toPost, submitUsuarioSuccess, submitUsuarioError);
                break;
        }

    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.usuario.localidad = $scope.localidades.filter(t => t.id == $scope.usuario.localidadId)[0];
            if ($scope.usuario.localidadId && $scope.usuario.localidadId > 0)
                $scope.obtenerAlmacenes($scope.usuario.localidadId);
        }, submitUsuarioError);
    }

    function cargarCajas() {
        showLoading();
        apiService.get("/api/caja", null).then(function (result) {
            hideLoading();
            $scope.cajas=$scope.cajas.concat(result.data.data);
            if ($scope.usuario.cajaId && $scope.usuario.cajaId > 0)
            $scope.usuario.caja = $scope.cajas.filter(t => t.id == $scope.usuario.cajaId)[0];
           
        }, submitUsuarioError);
    }

    $scope.obtenerAlmacenes=  function (localidadId) {
        showLoading();
        if (localidadId && localidadId > 0)
            apiService.get("/api/almacen/ObtenerAlmacenesPorLocalidad/" + localidadId, null).then(function (result) {
            hideLoading();
            $scope.almacenes = result.data.data;
                $scope.usuario.almacen = $scope.almacenes.filter(t => t.id == $scope.usuario.almacenId)[0];

        }, submitUsuarioError);
    }

    function submitUsuarioSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(usuariosUrl);
        if (result.data.status >= 0 && $scope.id!='') {
           
            $uibModalInstance.close();

        }

        if (result.data.status >= 0 && $scope.id =='') {
            $scope.usuario = {};

        }

        $scope.showValidate = false;
    }

    function submitUsuarioError(result) {
        hideLoading();
        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');
        }
        else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }
        
    }

    function VerificarUsuario(dataId) {
        if (dataId != undefined && dataId != null && dataId != '') {
            showLoading();
            apiService.get("/api/usuario/ObtenerUsuarioPorId", { idString: dataId })
                .then(llenarUsuario, submitUsuarioError);
        }
        else {
            cargarLocalidades();
            cargarCajas();
        }
      
    }


    function llenarUsuario(result) {
    hideLoading();
    $scope.usuario = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.usuario;
    $scope.usuario.activoBool = $scope.usuario.activo == 1 ? true : false;
    cargarLocalidades();
    cargarCajas();
    }

    function llenarTabla(urlAUsar) {
        showLoading();
         var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos:!$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.usuarios = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalUsuarios = result.data.pagedData.count;
        }, submitUsuarioError);
        return promise;
    }
});