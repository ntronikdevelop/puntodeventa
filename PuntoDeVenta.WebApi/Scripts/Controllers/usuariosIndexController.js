﻿var usuarioIndexController = angular.module('usuarioIndexController', []);
usuarioIndexController.controller('TablaDeUsuariosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var usuariosUrl = '/api/usuarios';
    $scope.resultadoEnPantalla = { usuarios: [], totalUsuarios: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.usuarios = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalUsuarios = result.data.pagedData.count;
           
        }, ErrorResult);
        return promise;
    }

    $scope.asignarRoles = function (userId) {
        showLoading();
        $scope.usuarioId = userId;
        modalUtils.openInstance("/Modals/UsuarioRolesForm?data=00046120", "FormularioUsuarioRoles", { UserId: userId, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.EditarUsuario = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/UsuarioForm?data=00046120", "FormularioUsuarios", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.cambiarPassword = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/CambiarPassword?data=00046119", "cambiarContrasena", { usuarioId: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'usuarios')
        $scope.search();

    });

    $scope.BorrarUsuario = function (Id) {

       bootbox.confirm('Esta seguro de desactivar el usuario?',function(desicion){
           if (desicion) {
               showLoading();
               var url = usuariosUrl+'/BorrarUsuario/'+ Id;
               var promise = apiService.get(url, null);
               promise.then(function (result) {
                   hideLoading();
                   bootbox.alert(result.data.mensaje);
                   llenarTabla(usuariosUrl);
               }, ErrorResult);
           }
        }) 
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = usuariosUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "usuarios")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "usuarios")
            $scope.search();

    });




}]);