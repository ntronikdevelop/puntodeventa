﻿var clienteFormController = angular.module('NavegationApp');
clienteFormController.controller('FormularioClientes', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance,$window) {
    hideLoading();
    var clientesUrl = '/api/cliente';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.valorBusqueda = modalFunctions.valorBusqueda;
    $scope.id = modalFunctions.Id;
    $scope.cliente = { id: 0, nombre: '', cedulaRnc: '', activo: true, aplicarItbis: false, telefono: '', diasVencimiento: 0, monedaId: 0 };
    cargarMonedas();
    cargarZonas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarCliente($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

  


    $scope.setTipoNcf = function (controlNcf) {
        if (controlNcf) {
            $scope.cliente.tipoNcf = controlNcf.tipo;
            $scope.cliente.aplicarItbis = $scope.cliente.tipoNcf?true:false;
        }
        else {

            $scope.cliente.aplicarItbis = false;
        }
    };


    $scope.$watch('tiposNcf', function (newValue, oldValue) {
        if ($scope.tiposNcf && $scope.tiposNcf.length > 0) {
            let algo = [{ nombre: 'No aplica', tipo: null }];
          algo=  algo.concat($scope.tiposNcf);
            angular.copy(algo, $scope.tiposNcf);
        }
    });

    $scope.GuardarCliente = function () {
        var error = $scope.frmClienteForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.cliente;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/cliente", toPost, submitClienteSuccess, submitClienteError);
                break;
            default:
                apiService.put("/api/cliente", toPost, submitClienteSuccess, submitClienteError);
                break;
        }

    }
    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.cliente.moneda = $scope.monedas.filter(t=> t.id == $scope.cliente.monedaId)[0];

        }, submitClienteError);
    }
    function cargarZonas() {
        showLoading();
        apiService.get("/api/zona", null).then(function (result) {
            hideLoading();
            $scope.zonas = result.data.data;
            $scope.cliente.zona = $scope.zonas.filter(t => t.id == $scope.cliente.zonaId)[0];

        }, submitClienteError);
    }


    function submitClienteSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(clientesUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.cliente = {};

        }


        $scope.showValidate = false;
    }

    function submitClienteError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarCliente(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/cliente", { id: dataId })
                      .then(llenarCliente, submitClienteError);
        }
    }



    function llenarCliente(result) {
        hideLoading();
        $scope.cliente = result.data != undefined && result.data != null ? result.data.data[0] : $scope.cliente;
        $scope.cliente.zona = $scope.zonas.filter(x => x.id == $scope.cliente.zonaId)[0];
    }

 

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: $scope.valorBusqueda.campo,
            tipoCampo: $scope.valorBusqueda.tipoCampo,
            valor: $scope.valorBusqueda.valor
        };

        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.clientes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalClientes = result.data.pagedData.count;
        }, submitClienteError);
        return promise;
    }
});