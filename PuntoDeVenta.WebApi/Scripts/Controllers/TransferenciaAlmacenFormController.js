﻿var transferenciaAlmacensFormController = angular.module('NavegationApp');
transferenciaAlmacensFormController.controller('FormularioTransferenciaAlmacen', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var transferenciaAlmacensUrl = '/api/TransferenciaAlmacen';
    var apiService = new ApiService();
    $scope.esEdicion = false;
    $scope.editandoTransferencia = false;
    hideLoading();
    $scope.localidadesOrigen = new Array();
    $scope.localidadesDestino = new Array();
    $scope.almacenesDestino = new Array();
    $scope.almacenesOrigen = new Array();
    $scope.id = modalFunctions.Id;
    $scope.transferenciaVacia = { cantidad: 0, localidadOrigen: 0, localidadDestino: 0, localidadOrigenId: 0, localidadDestinoId: 0, productoId: 0, almacenOrigenId: 0, almacenDestinoId:0 };
    $scope.transferenciaAlmacenTemp = { cantidad: 0, localidadOrigen: 0, localidadDestino: 0, localidadOrigenId: 0, localidadDestinoId: 0, productoId: 0, almacenOrigenId: 0, almacenDestinoId: 0 };
    $scope.transferenciaAlmacen = {  localidadOrigen:null,localidadDestino:null , almacenOrigen:null,almacenDestino:null, transferencias: new Array() };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
   
    cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarTransferenciaAlmacen = function () {
        if (!$scope.transferenciaAlmacen.localidadOrigen || !$scope.transferenciaAlmacen.localidadDestino) {
            bootbox.alert('Debe seleccionar las localidades correspodientes');
            return;
        }

        if (!$scope.transferenciaAlmacen.almacenOrigen || !$scope.transferenciaAlmacen.almacenDestino) {
            bootbox.alert('Debe seleccionar los almacenes correspodientes');
            return;
        }

        showLoading();
        var toPost = {};
        angular.copy($scope.transferenciaAlmacen, toPost);

        apiService.post("/api/transferenciaAlmacen/RegistrarTransferencias", toPost, submitTransferenciaAlmacenSuccess, submitTransferenciaAlmacenError);




    }



    $scope.convertirListaEnTexto = function (lista) {
        var texto = "";

        for (var i = 0; i < lista.length; i++) {
            texto += texto == "" ? lista[i] : "," + lista[i];
        }

        return texto;
    }

    $scope.agregarTransferencia = function () {
        var error = $scope.frmtransferenciaAlmacenForm.$error;
        if (error.required || error["autocomplete-required"] || error.min) {
            $scope.showValidate = true;
            return;
        }

        if ($scope.transferenciaAlmacenTemp.producto.existenciaEnAlmacen < $scope.transferenciaAlmacenTemp.cantidad || !$scope.transferenciaAlmacenTemp.cantidad)
        {
            bootbox.alert('No se puede transferir una cantidad mayor a la existente');
            return;
        }
        if ( $scope.transferenciaAlmacenTemp.cantidad == 0) {
            bootbox.alert('Debe de especificar una cantidad');
            return;
        }


        let transferenciaAlmacenAGuardar = {};
        angular.copy($scope.transferenciaAlmacenTemp, transferenciaAlmacenAGuardar);
        var index = $scope.transferenciaAlmacen.transferencias.map(function (el) {
            return el.producto.id;
        }).indexOf(transferenciaAlmacenAGuardar.producto.id);

        if (index >= 0) {
            $scope.transferenciaAlmacen.transferencias.splice(index, 1);
            $scope.transferenciaAlmacen.transferencias.push(transferenciaAlmacenAGuardar);
        }

        else {
            $scope.transferenciaAlmacen.transferencias.push(transferenciaAlmacenAGuardar);
        }
        angular.copy($scope.transferenciaVacia, $scope.transferenciaAlmacenTemp);
        $scope.$broadcast('angucomplete-alt:clearInput', 'productoNombre');
        $scope.editandoTransferencia = false;
    }

    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            for (var i = 0; i < $scope.transferenciaAlmacenTemp.producto.productoUnidades.length; i++) {

                $scope.transferenciaAlmacenTemp.producto.productoUnidades[i].unidad = $scope.unidades.filter(t=>t.id ==
                $scope.transferenciaAlmacenTemp.producto.productoUnidades[i].unidadId)[0];
            }

            $scope.unidadesCargadas = true;

        }, submitTransferenciaAlmacenError);
    }

    function cargarProductoUnidades(productoId) {

        showLoading();
        apiService.get("/api/productounidades/obenerunidadesdeProducto/" + productoId, null).then(function (result) {
            hideLoading();


            $scope.transferenciaAlmacenTemp.producto.productoUnidades = result.data.data;

            cargarUnidades();
          
        }, submitTransferenciaAlmacenError);


    }
    $scope.$watch('unidadesCargadas', function (newValue, oldValue) {
        if (newValue)
        {
            if (!$scope.transferenciaAlmacenTemp.productoUnidad || !$scope.transferenciaAlmacenTemp.unidadId || $scope.transferenciaAlmacenTemp.unidadId <= 0) {
                $scope.transferenciaAlmacenTemp.productoUnidad = $scope.transferenciaAlmacenTemp.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                $scope.transferenciaAlmacenTemp.unidadId = $scope.transferenciaAlmacenTemp.productoUnidad.unidadId;
            }
        }
    })
    $scope.EditarTransferencia = function (index) {
        angular.copy($scope.transferenciaAlmacen.transferencias[index], $scope.transferenciaAlmacenTemp);
        $scope.editandoTransferencia = true;
    }

    $scope.AnularTransferencia = function (index) {

        $scope.transferenciaAlmacen.transferencias.splice(index, 1);
    }



    $scope.$watch('transferenciaAlmacenTemp.localidadOrigenId', function (newValue, oldValue) {
        if (newValue )
            cargarAlmacenes(newValue,'origen');

    });

    $scope.$watch('transferenciaAlmacenTemp.productoUnidad', function (newValue, oldValue) {
        if (newValue) {
            $scope.transferenciaAlmacenTemp.producto.existenciaEnAlmacen = ($scope.cantidadOrigen * newValue.equivalencia);
        }
           

    });

    $scope.$watch('transferenciaAlmacenTemp.localidadDestinoId', function (newValue, oldValue) {
        if (newValue )
            cargarAlmacenes(newValue, 'destino');

    });

    $scope.$watch('transferenciaAlmacen.almacenOrigen', function (newValue, oldValue) {
        if (newValue && newValue.id > 0 && $scope.transferenciaAlmacenTemp.productoId>0)
            buscarExistenciaEnAlmacen($scope.transferenciaAlmacenTemp.productoId, $scope.transferenciaAlmacen.almacenOrigen.id);

    });





 

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();

            
            angular.copy(result.data.data, $scope.localidadesOrigen);
            angular.copy(result.data.data, $scope.localidadesDestino);

            if (result.data.data && result.data.data.length == 1) {
                $scope.transferenciaAlmacen.localidadOrigen = $scope.localidadesOrigen[0];
                $scope.transferenciaAlmacen.localidadDestino = $scope.localidadesDestino[0];
                $scope.transferenciaAlmacenTemp.localidadOrigenId = $scope.transferenciaAlmacen.localidadOrigen.id;
                $scope.transferenciaAlmacenTemp.localidadDestinoId = $scope.transferenciaAlmacen.localidadDestino.id;
            }

        }, submitTransferenciaAlmacenError);
    }

    function cargarAlmacenes(localidadId,seleccion ) {
        if (localidadId) {
            showLoading();
            let url = '';
            let parametro;
            switch (localidadId)
            {
                case -1:
                    url = "/api/almacen/ObtenerAlmacenesPorCodigo/"
                    parametro = 'DEF';
                    break;

                default:
                    url = "/api/almacen/ObtenerAlmacenesPorLocalidad/"
                    parametro = localidadId;
                    break;
            }
            apiService.get(url + parametro, null).then(function (result) {
                hideLoading();

                switch (seleccion) {
                    case 'origen':
                        angular.copy(result.data.data, $scope.almacenesOrigen);
                        break;
                    case 'destino':
                        angular.copy(result.data.data, $scope.almacenesDestino);
                        break;
                }
              


            }, submitTransferenciaAlmacenError);
        }

    }




    function submitTransferenciaAlmacenSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        var urlAUsar = transferenciaAlmacensUrl;
        llenarTabla(urlAUsar);
        if (result.data.status >= 0 && $scope.id > 0) {

            angular.copy($scope.transferenciaVacia, $scope.transferenciaAlmacen);
            angular.copy({}, $scope.transferenciaAlmacenTemp);

            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            
            angular.copy($scope.transferenciaVacia, $scope.transferenciaAlmacen);
            angular.copy({}, $scope.transferenciaAlmacenTemp);
          
        

        }



        $scope.showValidate = false;
    }



    $scope.selectedProducto = function (newValue, oldValue) {

        if (newValue) {
        
           
            $scope.transferenciaAlmacenTemp.producto = newValue.originalObject;
            $scope.transferenciaAlmacenTemp.productoId = $scope.transferenciaAlmacenTemp.producto.id;
            cargarProductoUnidades($scope.transferenciaAlmacenTemp.productoId);
            buscarExistenciaEnAlmacen($scope.transferenciaAlmacenTemp.productoId, $scope.transferenciaAlmacen.almacenOrigen.id);
           
        }
        else {
            $scope.unidadesCargadas = false;
            $scope.transferenciaAlmacenTemp.producto = null;
            $scope.transferenciaAlmacenTemp.productoId = 0;
        }


    }

    function buscarExistenciaEnAlmacen(productoId,almacenId) {
        if (almacenId && almacenId > 0) {
            showLoading();
            apiService.get("/api/almacenProducto/ObtenerExistenciaDeProductoEnAlmacen/" + productoId + "/" + almacenId, null).then(function (result) {
                hideLoading();
                $scope.cantidadOrigen = result.data.data[0] ? result.data.data[0].cantidad : 0;
                $scope.transferenciaAlmacenTemp.producto.existenciaEnAlmacen = !$scope.transferenciaAlmacenTemp.productoUnidad ? 0 : 
                    ($scope.cantidadOrigen * transferenciaAlmacenTemp.productoUnidad.equivalencia);

            }, submitTransferenciaAlmacenError);
        }
    }

    function submitTransferenciaAlmacenError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }





    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.transferenciaAlmacenes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalTransferenciaAlmacenes = result.data.pagedData.count;
        },submitTransferenciaAlmacenError);
        return promise;
    }
});