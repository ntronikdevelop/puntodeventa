﻿var reporteVentas = angular.module('reporteImpuestos', []);
reporteVentas.controller('reporteImpuestosController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var apiService = new ApiService();
    $scope.reporteGeneral = {};
    $scope.date = new Date();




    $scope.buscarImpuestos = function () {
        var error = $scope.frmReporteImpuestosForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        $scope.Print = function () {
            window.print();
        }


        showLoading();
        var toPost = $scope.busqueda;
        apiService.post("/api/factura/reporteImpuestos", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {
        var error = $scope.frmReporteImpuestosForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        
        empty = { fechaInicial: '', fechaFinal: ''};
        $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial ? $scope.busqueda.fechaInicial : '';
        $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal ? $scope.busqueda.fechaFinal : '';

        apiService.download('api/factura/reporteImpuestosExcel', $scope.busqueda, empty, 'ReporteImpuestos.xls','POST');
       

    }









    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteGeneral.reportePorMonedas = result.data.data;
        }



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    $scope.obtenerSaldoPorAntiguedad = function (reporte, inicio, fin) {
        let resultado = 0;
        const recibos = reporte.detalles;
        if (inicio && fin) {
            resultado = recibos.filter(x=> x.cantidadDias > inicio && x.cantidadDias <= fin).sum('monto');
        }
        else if (!inicio, fin) {
            resultado = recibos.filter(x => x.cantidadDias <= fin).sum('monto');
        }
        else if (inicio, !fin) {
            resultado = recibos.filter(x => x.cantidadDias > inicio).sum('monto');
        }
        
        return resultado;
    }


    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;
    }

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal'], function (newValue, oldValue) {
        if ($scope.seleccion == "reporteImpuestos")
            $scope.buscarImpuestos();
    });
}]);