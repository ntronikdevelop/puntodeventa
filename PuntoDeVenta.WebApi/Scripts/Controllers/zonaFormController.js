﻿

var zonasFormController = angular.module('NavegationApp');
zonasFormController.controller('FormularioZonas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    
    var zonasUrl = '/api/zona';
    var apiService = new ApiService();
    $scope.sectores = new Array();
    $scope.id = modalFunctions.Id;
    $scope.nuevoSector = '';
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.zona = { id: 0, nombre: '', activo: true};
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarZona($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarZona = function () {
        var error = $scope.frmZonaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.zona;
        toPost.detalles = JSON.stringify($scope.sectores);
        switch ($scope.id) {
            case 0:
                apiService.post("/api/zona", toPost, submitZonaSuccess, submitZonaError);
                break;
            default:
                apiService.put("/api/zona", toPost, submitZonaSuccess, submitZonaError);
                break;
        }

    }

    $scope.agregarSector = function () {
        if ($scope.nuevoSector) {
            if ($scope.sectores.findIndex(x => x.nombre == $scope.nuevoSector)<0)
                $scope.sectores.push({ nombre: $scope.nuevoSector });
            $scope.nuevoSector = '';
        }
    }

    $scope.borrarSector = function (nombre) {
        let index = $scope.sectores.findIndex(x => x.nombre === nombre);
        if(index>=0)
        $scope.sectores.splice(index, 1);
    }

    function cargarZonas() {
        showLoading();
        apiService.get("/api/zona", null).then(function (result) {
            hideLoading();
            $scope.zonas = result.data.data;

        }, submitZonaError);
    }

 

    function submitZonaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(zonasUrl);
        if (result.data.status >= 0 && $scope.id) {
            cargarZonas();
            $scope.zona = { id: 0, nombre: '', activo: true };
            $scope.sectores = new Array();
            $uibModalInstance.close();

        }

       

        $scope.showValidate = false;
    }

    function submitZonaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarZona(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/zona", { id: dataId })
                      .then(llenarZona,submitZonaError);
        }
    }


    function llenarZona(result) {
        hideLoading();
        $scope.zona = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.zona;
        $scope.sectores = $scope.zona.detalles ? JSON.parse($scope.zona.detalles) : $scope.sectores;
       
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos:!$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.zonas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalZonas = result.data.pagedData.count;
        },submitZonaError);
        return promise;
    }
});