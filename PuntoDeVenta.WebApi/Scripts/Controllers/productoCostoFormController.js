﻿var ProductoCostoFormController = angular.module('NavegationApp');
ProductoCostoFormController.controller('FormularioProductoCosto', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var productoCostoUrl = '/api/productoCostoes';
    $scope.esEdicion = false;
    var apiService = new ApiService();

    $scope.defaultCosto = {};
    $scope.producto = modalFunctions.Producto;
    $scope.producto.productoCostos = !$scope.producto.productoCostos ? new Array() : $scope.producto.productoCostos;

    $scope.defaultProductoCosto = {
        id: 0,
        producto: $scope.producto,
        productoId: $scope.producto.id,
        monedaId: $scope.producto.monedaId,
        unidadId: $scope.producto.productoUnidades.filter(p => p.esPrincipal)[0].unidadId

    };

    $scope.productoCosto = {
        id: 0,
        producto: $scope.producto,
        productoId: $scope.producto.id,
        monedaId: $scope.producto.monedaId,
        unidadId: $scope.producto.productoUnidades.filter(p => p.esPrincipal)[0].unidadId


    };
    if (modalFunctions.ProductoCostoActual.suplidor) {
        $scope.productoCosto = modalFunctions.ProductoCostoActual;
        $scope.productoCosto.producto = $scope.producto;
    }


    //function cargarCostoes() {
    //    showLoading();
    //    apiService.get("/api/Costoes", null).then(function (result) {
    //        hideLoading();
    //        $scope.Costoes = result.data.data;
    //        $scope.productoCosto.Costo = $scope.Costoes.filter(t => t.id == $scope.productoCosto.CostoId)[0];

    //    }, submitproductoCostoError);
    //}

    $scope.esEdicion = modalFunctions.ProductoCostoActual.suplidor ? true : false;

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.guardarProductoCosto = function () {
        let indice = modalFunctions.Index;
        let aGuardar = {};
        angular.copy($scope.productoCosto, aGuardar);
        let error = $scope.frmProductoCostoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        aGuardar.producto = null;
        aGuardar.suplidorId = aGuardar.suplidor.id;
        aGuardar.suplidor = { id: aGuardar.suplidor.id, nombre: aGuardar.suplidor.nombre };

        if (indice >= 0) {

            $scope.producto.productoCostos[indice] = aGuardar;
        }
        else {

            let indexExistente = $scope.producto.productoCostos.findIndex(x => x.suplidor.id == aGuardar.suplidorId);
            if (indexExistente >= 0)
                $scope.producto.productoCostos[indexExistente] = aGuardar;
            else
                $scope.producto.productoCostos.push(aGuardar);

           
        }

        let promedioCosto = ($scope.ItemSumm($scope.producto.productoCostos, 'costo') / $scope.producto.productoCostos.length);
        $scope.producto.costo = promedioCosto;

        limpiar();




        $scope.closeDialog();



    }



    function limpiar() {
        $scope.productoCosto = {
            id: 0,
            producto: $scope.producto,
            productoId: $scope.producto.id
        };
    }


    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }




    function submitproductoCostoError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }





    //$scope.$watch('productoCosto.monto', function (newValue, oldValue) {
    //    let valorImpuesto = $scope.productoCosto.impuesto != undefined ? $scope.productoCosto.impuesto.porciento : 0;
    //    $scope.productoCosto.montoItbis = $scope.productoCosto.monto * valorImpuesto;

    //});


});