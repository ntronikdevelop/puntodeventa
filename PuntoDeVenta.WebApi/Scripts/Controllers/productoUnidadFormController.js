﻿var ProductoUnidadFormController = angular.module('NavegationApp');
ProductoUnidadFormController.controller('FormularioProductoUnidad', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var productoUnidadUrl = '/api/productoUnidades';
    $scope.esEdicion = false;
    var apiService = new ApiService();
    cargarUnidades();
    $scope.defaultUnidad = {};
    $scope.producto = modalFunctions.Producto;

    $scope.defaultProductoUnidad = {
        id: 0,
        producto: $scope.producto,
        productoId: $scope.producto.id,
        unidad: null,
        unidadId: 0,
        equivalencia: 0,
        precioCompra: 0,
        precioVenta: 0,
        esPrincipal: false
    };

    $scope.productoUnidad = {
        id: 0,
        producto: $scope.producto,
        productoId: $scope.producto.id,
        unidad: null,
        unidadId: 0,
        equivalencia: 0,
        precioCompra: 0,
        precioVenta: 0,
        esPrincipal: false

    };
    if (modalFunctions.ProductoUnidadActual.unidad) {
        $scope.productoUnidad = modalFunctions.ProductoUnidadActual;
        $scope.productoUnidad.producto = $scope.producto;
    }


    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            $scope.productoUnidad.unidad = $scope.unidades.filter(t => t.id == $scope.productoUnidad.unidadId)[0];

        }, submitproductoUnidadError);
    }

    $scope.esEdicion = modalFunctions.ProductoUnidadActual.unidad ? true : false;

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarProductoUnidad = function () {
        $scope.producto.productoUnidades = $scope.producto.productoUnidades ? $scope.producto.productoUnidades : new Array();
        let indice = modalFunctions.Index;
        let aGuardar = {};
        angular.copy($scope.productoUnidad, aGuardar);
        aGuardar.producto = null;
        let error = $scope.frmProductoUnidadForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        if (aGuardar.esPrincipal) {
            aGuardar.equivalencia = 1;
            aGuardar.precioCompra = $scope.producto.costo;
            aGuardar.precioVenta = $scope.producto.precio;
        }
        else {
            aGuardar.precioCompra = $scope.producto.costo / aGuardar.equivalencia;
            aGuardar.precioVenta = $scope.producto.precio / aGuardar.equivalencia;
        }
        for (var i = 0; i < $scope.producto.productoUnidades.length; i++) {
            if(aGuardar.esPrincipal)
                $scope.producto.productoUnidades[i].esPrincipal = false;

                $scope.producto.productoUnidades[i].precioVenta = ( $scope.producto.precio / $scope.producto.productoUnidades[i].equivalencia);
                $scope.producto.productoUnidades[i].precioCompra = ( $scope.producto.costo / $scope.producto.productoUnidades[i].equivalencia);
            }
       
 


        if (indice >= 0) {
            
            $scope.producto.productoUnidades[indice] = aGuardar;
        }
        else {
            let indexExistente = $scope.producto.productoUnidades.findIndex(x => x.unidad.id == aGuardar.unidadId);
            if (indexExistente >= 0)
                $scope.producto.productoUnidades[indexExistente] = aGuardar;
            else
            $scope.producto.productoUnidades.push(aGuardar);
        }
        limpiar();
        $scope.closeDialog();
    }



    function limpiar() {
        $scope.productoUnidad = {
            id: 0,
            producto: $scope.producto,
            productoId: $scope.producto.id,
            unidad: null,
            unidadId: 0,
            equivalencia: 0,
            precioCompra: 0,
            precioVenta: 0,
            esPrincipal: false

        };
    }







    function submitproductoUnidadError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }





    //$scope.$watch('productoUnidad.monto', function (newValue, oldValue) {
    //    let valorImpuesto = $scope.productoUnidad.impuesto != undefined ? $scope.productoUnidad.impuesto.porciento : 0;
    //    $scope.productoUnidad.montoItbis = $scope.productoUnidad.monto * valorImpuesto;

    //});


});