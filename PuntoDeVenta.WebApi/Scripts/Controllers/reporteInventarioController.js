﻿var reporteVentas = angular.module('inventarioAlmacenes', []);
reporteVentas.controller('reporteInventarioController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.reporteGeneral = {};
    $scope.date = new Date();
    $scope.busqueda = { localidadId: null, almacenId: null, noProductosAgotados:false };
    llenarLocalidades();
    $scope.productoEspecifico = {};

    function llenarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            if ($scope.localidades.length == 1) {
                $scope.busqueda.localidad = $scope.localidades[0];
                $scope.busqueda.localidadId = $scope.busqueda.localidad.id;
                $scope.almacenes = $scope.busqueda.localidad.almacenes;
            }

        });
    }

    $scope.buscarAlmacenes= function(localidadId) {
        if (localidadId) {
            showLoading();
            apiService.get("/api/almacen/ObtenerAlmacenesPorLocalidad/" + localidadId, null).then(function (result) {
                hideLoading();
                $scope.almacenes = result.data.data;
                if ($scope.almacenes.length == 1) {
                    $scope.busqueda.almacen = $scope.almacenes[0];
                    $scope.busqueda.almacenId = $scope.busqueda.almacen.id;
                }

            }, submitFacturaError);
        }
      
    }
    

    $scope.BuscarLocalidades = function () {
        var error = $scope.frmReporteInventarioForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = $scope.busqueda;
        var url = `/api/localidad/InventarioAlmacenes/${(!$scope.busqueda.localidadId ? "0" : $scope.busqueda.localidadId)}/${(!$scope.busqueda.almacenId ? "0" : $scope.busqueda.almacenId)}/${(!$scope.busqueda.noProductosAgotados ? "0" : "1")}`;
        apiService.get(url, null).then(submitFacturaSuccess, submitFacturaError);

    }

    $scope.obtenerOrden = function () {
        return $scope.busqueda.noProductosAgotados ? 'nombre' : 'cantidad';
    }
    $scope.exportarExcel = function () {

       
       
        apiService.download(`/api/localidad/InventarioAlmacenesExcel/${(!$scope.busqueda.localidadId ? "0" : $scope.busqueda.localidadId)}/${(!$scope.busqueda.almacenId ? "0" : $scope.busqueda.almacenId)}/${(!$scope.busqueda.noProductosAgotados ? "0" : "1")}`,
            $scope.busqueda, {}, 'Reporte de Inventario.xls','GET');
    }

    $scope.obtenerCostoTotal = function (existencia) {
        let costo = existencia.producto.productoUnidades.filter(p => p.unidadId == existencia.unidadId)[0];
        return costo ? existencia.cantidad * costo.precioCompra : 0;
    }

    $scope.obtenerCosto = function (existencia) {
        let costo = existencia.producto.productoUnidades.filter(p => p.unidadId == existencia.unidadId)[0];
        return costo ? costo.precioCompra : 0;
    }

    $scope.obtenerPrecioTotal = function (existencia) {
        let costo = existencia.producto.productoUnidades.filter(p => p.unidadId == existencia.unidadId)[0];
        return costo ? existencia.cantidad * costo.precioVenta : 0;
    }

    $scope.obtenerPrecio = function (existencia) {
        let costo = existencia.producto.productoUnidades.filter(p => p.unidadId == existencia.unidadId)[0];
        return costo ? costo.precioVenta : 0;
    }
    $scope.obtenerTotalCostoAlmacen = function (almacen) {
        let costoTotal = 0;
        for (let i = 0; i < almacen.productos.length; i++) {
            costoTotal += $scope.obtenerCostoTotal(almacen.productos[i]);
        }
        return costoTotal;
    }

    $scope.obtenerTotalPrecioAlmacen = function (almacen) {
        let precioTotal = 0;
        for (let i = 0; i < almacen.productos.length; i++) {
            precioTotal += $scope.obtenerPrecioTotal(almacen.productos[i]);
        }
        return precioTotal;
    }
    $scope.buscarLocalidadDeProducto = function (localidadId) {


        if (localidadId) {
            showLoading();
            var toPost = $scope.busqueda;
            var url = "/api/localidad/buscarSoloLocalidad/" + localidadId;
            apiService.get(url, null).then(submitLocalidadSuccess, submitFacturaError);
        }
        else
            return '';

    }


    $scope.Print = function () {
        window.print();
    }

    function submitLocalidadSuccess(result) {
        hideLoading();
        let resultado = '';
        if (result.data.status >= 0) {

            resultado = result.data.data[0].nombre;
        }
        return resultado;
    }


    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteGeneral.inventarioPorLocalidades = result.data.data;
        }

    }

    $scope.obtenerProductoDeInventario = function (str) {
        var matches = [];

        $scope.reporteGeneral.inventarioPorLocalidades.forEach(function (localidad) {
            localidad.almacenes.forEach(function (almacen) {
                almacen.productos.forEach(function (producto) {
                    if (producto.producto.nombre.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
                        matches.push(producto);
                });
            });
        });

        return matches;
    };

    $scope.selectedProducto = function (newValue, oldValue) {

        if (newValue) {
            $scope.productoEspecifico = newValue.originalObject;
            $scope.mostrarProductoEspecifico = true;
        }
        else {
            $scope.productoEspecifico = {};
            $scope.mostrarProductoEspecifico = false;
        }


        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }






    $scope.$watch('busqueda.localidadId', function (newValue, oldValue) {
        if ($scope.seleccion == "ReporteInventario") {
            $scope.BuscarFacturas();
            $scope.buscarAlmacenes($scope.busqueda.localidadId);
        }
        
    });
}]);