﻿var SuplidoresFormController = angular.module('NavegationApp');
SuplidoresFormController.controller('FormularioSuplidores', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var SuplidoressUrl = '/api/suplidor';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.suplidor = { id: 0, nombre: '', cedulaRnc: '', activo: true, direccion: '', telefono: '' };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarSuplidores($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarSuplidores = function () {
        var error = $scope.frmSuplidoresForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        showLoading();

        var toPost = $scope.suplidor;
        toPost.direccion = toPost.direccion ? toPost.direccion : '';
        switch ($scope.id) {
            case 0:
                apiService.post("/api/suplidor", toPost, submitSuplidoresSuccess, submitSuplidoresError);
                break;
            default:
                apiService.put("/api/suplidor", toPost, submitSuplidoresSuccess, submitSuplidoresError);
                break;
        }

    }

    function submitSuplidoresSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(SuplidoressUrl);
        if (result.data.status >= 0 && $scope.id>0) {
            $uibModalInstance.close();

        }

        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.suplidor = {};

        }



        $scope.showValidate = false;
    }

    function submitSuplidoresError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }
       
      
    }

    function VerificarSuplidores(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/suplidor", { id: dataId })
                      .then(llenarSuplidores);
        }
    }



    function llenarSuplidores(result) {
        hideLoading();
        $scope.suplidor = result.data != undefined && result.data != null ? result.data.data[0] : $scope.suplidor;

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };


        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.suplidores = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalSuplidores = result.data.pagedData.count;

        }, function (result) {
                if (result.status == 401) {
                    window.location.href = apiService.getFullPath('/account/login');
            };
            hideLoading();
        });
        return promise;
    }
});