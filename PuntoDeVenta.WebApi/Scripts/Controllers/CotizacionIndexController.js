﻿var FacturaIndexController = angular.module('cotizacionesIndexModule', []);
FacturaIndexController.controller('TablaDeCotizacionesController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var FacturasUrl = '/api/cotizacion';
    $scope.resultadoEnPantalla = { facturas: [], totalFacturas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    $scope.filtroBusqueda = [
        { descripcion: 'No. Factura', campo: 'NumeroFactura', tipoCampo: '0' },
        { descripcion: 'Cliente', campo: 'ClienteId', tipoCampo: '1' },
        { descripcion: 'Vendedor', campo: 'VendedorId', tipoCampo: '1' },
        { descripcion: 'Fecha', campo: 'FechaFacturado', tipoCampo: '2' },
        { descripcion: 'Estado de factura', campo: 'Estado', tipoCampo: '3' }
    ]

    $scope.estados = [
        { descripcion: 'TODOS', valorEstado: "" },
        { descripcion: 'FACTURADO', valorEstado: "f" },
        { descripcion: 'ANULADO', valorEstado: "a" },
        { descripcion: 'COTIZADO', valorEstado: "c" },
        { descripcion: 'PAGADO', valorEstado: "p" }
    ]

    $scope.valorBusqueda = { campo: '', valor: '' };

    $scope.busquedaCompleja = {
        page: $scope.resultadoEnPantalla.currentPage,
        size: 10,
        activo: !$scope.inactivos,
        filtros: new Array()
       };

    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }
    $scope.ObtenerSoloFecha = function (date) {
        var day = date.getDate();
        var monthIndex = date.getMonth()+1;
        var year = date.getFullYear();
        return (day + '-' + monthIndex + '-' + year);
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.valorBusqueda.valor = newValue.originalObject.id;
            $scope.valorBusqueda.descripcionValor = newValue.originalObject.nombre;
            $scope.agregarFiltro();
            $scope.search();
        }
        else {
            $scope.valorBusqueda.valor = null;
            $scope.valorBusqueda.descripcionValor = null;
        }

       
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'cotizaciones')
            $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.EditarFactura = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/CotizacionForm?data=00046120", "FormularioCotizaciones", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, busquedaCompleja:$scope.busquedaCompleja });
    }

  

    $scope.BorrarFactura = function (Id) {
        bootbox.confirm('Esta seguro de eliminar el registro de la Cotización?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = FacturasUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    $scope.search();
                }, ErrorResult);
            }
        })

    }

    $scope.FacturarCotizacion = function (Id) {
        bootbox.confirm('Esta seguro de facturar esta cotización?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = FacturasUrl + '/FacturarCotizacion/' + Id;
                var promise = apiService.get(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    $scope.search();
                }, ErrorResult);
            }
        })

    }

    $scope.DuplicarCotizacion = function (Id,clienteId) {
        bootbox.confirm('Esta seguro de duplicar esta cotización?', function (desicion) {
            if (desicion) {
                showLoading();
                const url = FacturasUrl + `/DuplicarCotizacion/${Id}/${clienteId}`;
                const promise = apiService.get(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    $scope.search();
                }, ErrorResult);
            }
        })

    }
    $scope.CopiarCotizacion = function (factura) {
        showLoading();
        modalUtils.openInstance("/Modals/DuplicarCotizacion?data=00046120", "CopiarCotizacion", { Id: factura.id, clienteNombre:factura.cliente.nombre,clienteId:factura.clienteId, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, busquedaCompleja: $scope.busquedaCompleja });
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = FacturasUrl;


        llenarTabla(urlAUsar + '/BusquedaCompleja');
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
    }
    $scope.verificarBusquedaCompleja = function () {
        return ($scope.busquedaCompleja && $scope.busquedaCompleja.filtros && $scope.busquedaCompleja.filtros.length > 0);
    }

    $scope.agregarFiltro = function () {
        let aGuardar = {};
        angular.copy($scope.valorBusqueda, aGuardar);
        if ($scope.busquedaCompleja.filtros) {
            let index = $scope.busquedaCompleja.filtros.findIndex(x => x.campo == aGuardar.campo);
            if (index >= 0) {
                $scope.busquedaCompleja.filtros.splice(index, 1);
            }
            $scope.busquedaCompleja.filtros.push(aGuardar);
        }
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
        return;
    }

    $scope.borrarFiltro = function (index) {
        $scope.busquedaCompleja.filtros.splice(index, 1);
    }

    $scope.init = function () {


        $q.all([
            $scope.search()
        ])
    };
    if ($scope.seleccion == "Cotizaciones")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Cotizaciones")
            $scope.search();

    });

    $scope.obtenerEstadoDeFactura = function (estatus) {
        let resultado = '';
        estatus = estatus.toLowerCase();

        switch (estatus) {
            case 'f':
                resultado = "FACTURADO";
                break;
            case 'd':
                resultado = "CONVERTIDO";
                break;

            case 'a':
                resultado = "ANULADO";
                break;
            case 'c':
                resultado = "COTIZADO";
                break;

            case 'p':
                resultado = "PAGADO";
                break;
            default:

                break;
        }

        return resultado;

    }


}]);