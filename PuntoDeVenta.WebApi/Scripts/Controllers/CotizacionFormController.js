﻿var FacturasFormController = angular.module('NavegationApp');
FacturasFormController.controller('FormularioCotizaciones', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var facturasUrl = '/api/cotizacion';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.busquedaCompleja = modalFunctions.busquedaCompleja;
    $scope.id = modalFunctions.Id;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.factura = { id: 0, clienteId: 0, activo: true, monedaId: 0, numeroFactura: '', cliente: null, moneda: null, montoPagado: 0, montoRecibido: 0, montoTotal: 0, montoItbis: 0, montoGrabado: 0, estado: '', detallesFactura: [] };
    cargarMonedas();
    // cargarVendedores();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarFactura($scope.id);
    $scope.closeDialogFactura = function () {
        $uibModalInstance.close();
    }

    $scope.mostrarDetalles = true;

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.factura.moneda = $scope.monedas.filter(t => t.id == $scope.factura.monedaId)[0];

        }, submitFacturaError);
    }

    function cargarVendedores(zonaId) {
        showLoading();
        apiService.get("/api/vendedor/vendedorPorZona/" + zonaId, null).then(function (result) {
            hideLoading();
            $scope.vendedores = result.data.data;
            //  $scope.factura.vendedor = $scope.vendedores.filter(t => t.id == $scope.factura.vendedorId)[0];

        }, submitFacturaError);
    }




    $scope.GuardarFactura = function () {
        var error = $scope.frmFacturaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        if ($scope.factura.montoPagado > ($scope.factura.montoTotal)) {
            bootbox.alert("El monto pagado no debe de exceder el monto total de la factura.");
            return;
        }
        if (!$scope.factura.detallesFactura || $scope.factura.detallesFactura.length <= 0) {
            bootbox.alert("Favor de ingresar por lo menos un articulo y/o servicio.");
            return;
        }
        showLoading();
        var toPost = $scope.factura;
        toPost.tipoNCF = toPost.controlNCF ? toPost.controlNCF.tipo : null;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/cotizacion", toPost, submitFacturaSuccess, submitFacturaError);
                break;
            default:
                apiService.put("/api/cotizacion", toPost, submitFacturaSuccess, submitFacturaError);
                break;
        }

    }

    $scope.EditarDetalle = function (detalle, index) {

        $scope.esEditando = false;
        if (detalle == null) {
            detalle = {
                id: 0,
                productoId: 0,
                activo: true,
                cantidad: 0,
                monto: 0,
                producto: null,
                impuestos: [],
                moneda: $scope.factura.moneda,
                montoItbis: 0,
                montoTotal: 0,
                facturaId: $scope.factura.id,
                impuestoId: 0,
                esGratis: false

            }
        }

        else
            $scope.esEditando = true;

        showLoading();
        $scope.detalleActual = detalle;
        modalUtils.openInstance("/Modals/DetalleCotizacionForm?data=00046120", "FormularioDetalleCotizaciones", {
            DetalleActual: detalle,
            Factura: $scope.factura,
            modal: modalUtils,
            porcientoDescuento: $scope.factura.porcientoDescuento,
            esEdicion: $scope.esEditando,
            resultadoEnPantalla: $scope.resultadoEnPantalla,
            currentIndex: index
        });
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.factura.cliente = newValue.originalObject;
            $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? newValue.originalObject.moneda : $scope.factura.moneda;
            $scope.factura.monedaId = $scope.factura.moneda.id;
            $scope.factura.clienteId = newValue.originalObject.id;
            cargarVendedores($scope.factura.cliente.zonaId);
            //$scope.factura.vendedor = !$scope.factura.vendedor ? $scope.factura.cliente.vendedor : $scope.factura.vendedor;
            //$scope.factura.vendedorId = $scope.factura.vendedor ? $scope.factura.vendedor.id : null;
            $scope.factura.rnc = $scope.factura.cliente.cedulaRnc;
            $scope.verificarAlmacen();
        }
        else {
            $scope.factura.cliente = null;
            $scope.factura.clienteId = 0;
            $scope.factura.vendedorId = null;
            $scope.factura.vendedor = null;
            $scope.factura.moneda = null;
            $scope.factura.monedaId = 0;
            $scope.factura.rnc = null;
            $scope.factura.almacen = null;
            $scope.factura.almacenId = null;
            $scope.vendedores = [];
        }


    }
    $scope.verificarAlmacen = function () {
        $scope.factura.almacen = $scope.almacenes && $scope.almacenes.length > 0 ? ($scope.factura.cliente.almacenId && $scope.factura.cliente.almacenId > 0 ?
            $scope.almacenes.filter(a => a.id === $scope.factura.cliente.almacenId)[0] : $scope.almacenes.filter(a => a.almacenAsignado)[0]) : null;
        $scope.factura.almacenId = $scope.factura.almacen ? $scope.factura.almacen.id : null;
    }

    $scope.$watch('almacenes', function (newValue, oldValue) {
        $scope.verificarAlmacen();
    });

    $scope.AnularDetalle = function (index) {
        $scope.mostrarDetalles = false;
        bootbox.confirm("Esta seguro de eliminar el producto de la cotización?", function (desicion) {
            if (desicion) {
                if (index >= 0) {
                    let detalle = $scope.factura.detallesFactura[index];
                    let detallesHijas = $scope.factura.detallesFactura.find(x => x.detallePadreId == detalle.id);

                    $scope.factura.detallesFactura.splice(index, 1);
                    if (detallesHijas != null) {
                        for (var i = 0; i < detallesHijas.length; i++) {
                            let indexHijo = $scope.factura.detallesFactura.findIndex(y => y.id == detallesHijas[i].id);
                            if (indexHijo >= 0)
                                $scope.factura.detallesFactura.splice(indexHijo, 1);
                        }
                    }
                    $scope.$apply();
                    $scope.RecalcularDescuento();

                    $scope.factura.montoPagado = $scope.factura.montoRecibido;
                    $scope.factura.montoDevuelto = $scope.factura.montoRecibido - $scope.factura.montoTotal;
                    $scope.factura.montoAdeudado = $scope.factura.montoTotal - ($scope.factura.montoPagado);
                    if ($scope.factura.montoRecibido >= ($scope.factura.montoTotal)) {
                        $scope.factura.montoAdeudado = 0;
                        $scope.factura.montoPagado = $scope.factura.montoTotal;

                    }
                }
            }
        })
        $scope.mostrarDetalles = true;
    }



    function submitFacturaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(facturasUrl + '/BusquedaCompleja');

        if (result.data.status >= 0) {
            $window.open('/ImpresionFactura?id=' + result.data.status, '_blank');
            $uibModalInstance.close();
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.factura = {};
            $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
        }


        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarFactura(dataId) {
        if (dataId > 0) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/cotizacion", { id: dataId })
                .then(llenarFactura, submitFacturaError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarFactura(result) {
        hideLoading();
        $scope.factura = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.factura;
        $scope.selectedCliente({ originalObject: $scope.factura.cliente }, null);
        $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? $scope.factura.cliente.moneda : $scope.factura.moneda;
    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;

    }

    $scope.$watch('factura.montoRecibido', function (newValue, oldValue) {
        $scope.factura.montoNC = $scope.factura.montoNC ? $scope.factura.montoNC : 0;
        $scope.factura.montoPagado = $scope.factura.montoRecibido;
        $scope.factura.montoDevuelto = $scope.factura.montoRecibido + $scope.factura.montoNC - $scope.factura.montoTotal;
        $scope.factura.montoAdeudado = $scope.factura.montoTotal - $scope.factura.montoPagado - $scope.factura.montoNC;
        if ($scope.factura.montoRecibido >= ($scope.factura.montoTotal)) {
            $scope.factura.montoAdeudado = 0;
            $scope.factura.montoPagado = $scope.factura.montoTotal;

        }


    });



    $scope.$watch('factura.moneda', function (newValue, oldValue) {
        let ultimaFechaActualizacion = newValue != undefined ? newValue.fechaModificacion : null;
        let notUpdated = newValue != undefined ? newValue.tasaActualizada : null;
        if (ultimaFechaActualizacion != null && !newValue.esMonedaLocal == true)
            $scope.verificarFechaDeActualizacionDeMoneda(ultimaFechaActualizacion, notUpdated);
    });


    $scope.$watch('factura.porcientoDescuento', function (newValue, oldValue) {
        let valor = !newValue ? 0 : newValue;
        $scope.factura.montoNC = $scope.factura.montoNC ? $scope.factura.montoNC : 0;

        if ($scope.factura.detallesFactura && $scope.factura.detallesFactura.length > 0)
            for (var i = 0; i < $scope.factura.detallesFactura.length; i++) {

                if ($scope.factura.detallesFactura[i].esGratis) {
                    $scope.factura.detallesFactura[i].porcientoDescuento = 0;

                    $scope.factura.detallesFactura[i].montoDescuento = 0;

                    $scope.factura.detallesFactura[i].montoItbis = 0;

                    $scope.factura.detallesFactura[i].montoGrabado = 0;

                    $scope.factura.detallesFactura[i].montoTotal = 0;
                }
                else {
                    $scope.factura.detallesFactura[i].porcientoDescuento = valor;
                    ObtenerImpuestosProducto($scope.factura.detallesFactura[i], i);
                }


            }


    });


    function ObtenerImpuestosProducto(detalleFactura, i) {


        detalleFactura.montoDescuento = (detalleFactura.porcientoDescuento / 100) * detalleFactura.montoGrabado;

        detalleFactura.montoGrabado =
            (detalleFactura.monto - detalleFactura.montoDescuento) * detalleFactura.cantidad;

        detalleFactura.montoItbis = obtenerMontoImpuestos(detalleFactura);


        detalleFactura.montoTotal = detalleFactura.cantidad *
            (detalleFactura.monto - detalleFactura.montoDescuento) + detalleFactura.montoItbis;

        $scope.factura.detallesFactura[i] = detalleFactura;

        $scope.factura.montoTotal = $scope.factura.detallesFactura.sum("montoTotal");
        $scope.factura.montoGrabado = $scope.factura.detallesFactura.sum("montoGrabado");
        $scope.factura.montoDescuento = $scope.factura.detallesFactura.sum("montoDescuento");
        $scope.factura.montoItbis = $scope.factura.detallesFactura.sum("montoItbis");
        $scope.factura.costo = $scope.factura.detallesFactura.sum("costo");
        $scope.factura.montoAdeudado = $scope.factura.montoTotal - ($scope.factura.montoPagado + $scope.factura.montoNC);
        $scope.factura.montoDevuelto = $scope.factura.montoRecibido - $scope.factura.montoTotal;
    }

    function obtenerMontoImpuestos(detalleFactura) {
        let totalImpuesto = 0;
        if (!detalleFactura.impuestos || detalleFactura.impuestos.length == 0)
            return totalImpuesto;

        for (let i = 0; i < detalleFactura.impuestos.length; i++) {
            totalImpuesto += (detalleFactura.impuestos[i].impuesto.porciento *
                (detalleFactura.monto - detalleFactura.montoDescuento) * detalleFactura.cantidad);
        }

        return totalImpuesto;

    }

    $scope.$watchCollection('factura.detallesFactura', function (newValue, oldValue) {

        if ($scope.factura.detallesFactura && $scope.factura.detallesFactura.length > 0)
            for (var i = 0; i < $scope.factura.detallesFactura.length; i++) {
                if ($scope.factura.detallesFactura[i]) {
                    if ($scope.factura.detallesFactura[i].esGratis) {
                        $scope.factura.detallesFactura[i].porcientoDescuento = 0;

                        $scope.factura.detallesFactura[i].montoDescuento = 0;

                        $scope.factura.detallesFactura[i].montoItbis = 0;

                        $scope.factura.detallesFactura[i].montoGrabado = 0;

                        $scope.factura.detallesFactura[i].montoTotal = 0;
                    }
                    else {

                        ObtenerImpuestosProducto($scope.factura.detallesFactura[i], i);



                    }
                }



            }
    });

    $scope.RecalcularDescuento = function () {
        for (var i = 0; i < $scope.factura.detallesFactura.length; i++) {

            if ($scope.factura.detallesFactura[i]) {
                if ($scope.factura.detallesFactura[i].esGratis) {
                    $scope.factura.detallesFactura[i].porcientoDescuento = 0;

                    $scope.factura.detallesFactura[i].montoDescuento = 0;

                    $scope.factura.detallesFactura[i].montoItbis = 0;

                    $scope.factura.detallesFactura[i].montoGrabado = 0;

                    $scope.factura.detallesFactura[i].montoTotal = 0;
                }
                else {
                    ObtenerImpuestosProducto($scope.factura.detallesFactura[i], i);
                }
            }



        }

    }

    $scope.$watch('factura.notaCreditoAplicada', function (newValue, oldValue) {
        if (newValue && newValue != '') {
            let currentUrl = '/api/MovimientoDevolucionCliente/ObtenerNotaCredito/' + newValue;
            var promise = apiService.get(currentUrl, null);
            promise.then(function (result) {
                if (result.status >= 0) {
                    $scope.notaCreditoAplicada = result.data.data[0];
                    $scope.factura.montoNC = $scope.notaCreditoAplicada ? $scope.notaCreditoAplicada.monto : 0;
                    $scope.factura.montoTotal = $scope.factura.detallesFactura.sum("montoTotal");
                    $scope.factura.montoDescuento = $scope.factura.detallesFactura.sum("montoDescuento");
                    $scope.factura.montoGrabado = $scope.factura.detallesFactura.sum("montoGrabado");
                    $scope.factura.montoItbis = $scope.factura.detallesFactura.sum("montoItbis");
                    $scope.factura.costo = $scope.factura.detallesFactura.sum("costo");
                    $scope.factura.montoAdeudado = $scope.factura.montoTotal - ($scope.factura.montoPagado + $scope.factura.montoNC);
                    $scope.factura.montoDevuelto = $scope.factura.montoRecibido + $scope.factura.montoNC - $scope.factura.montoTotal;

                }
                else {
                    $scope.notaCreditoAplicada = { monto: 0 };
                    $scope.factura.montoNC = 0;
                }


            }, submitFacturaError);
        }
        else {
            $scope.notaCreditoAplicada = { monto: 0 };
            $scope.factura.montoNC = 0;
        }
    });


});


FacturasFormController.controller('CopiarCotizacion', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();


    $scope.inactivos = modalFunctions.inactivos;
    $scope.busquedaCompleja = modalFunctions.busquedaCompleja;
    $scope.id = modalFunctions.Id;
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;

    var modalUtils = modalFunctions.modal;
    var facturasUrl = '/api/cotizacion';
    var apiService = new ApiService();
    $scope.cliente = {
        id: modalFunctions.clienteId,
         nombre: modalFunctions.clienteNombre
    };
    $scope.id = modalFunctions.Id;

    $scope.closeDialogCopiarCotizacion = function () {
        $uibModalInstance.close();
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.cliente = newValue.originalObject;

        }
        else {
            $scope.cliente = null;
        }


    }

     
    $scope.duplicarCotizacion = function () {
        var error = $scope.frmDuplicarCotizacion.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        showLoading();
        const url = facturasUrl + `/DuplicarCotizacion/${$scope.id}/${$scope.cliente.id}`;
        const promise = apiService.get(url, null);
        promise.then(function (result) {
            hideLoading();
            bootbox.alert(result.data.mensaje);
            llenarTabla(facturasUrl + '/BusquedaCompleja');
            $uibModalInstance.close();
        }, ErrorResult);

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, ErrorResult);
        return promise;

    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    




});