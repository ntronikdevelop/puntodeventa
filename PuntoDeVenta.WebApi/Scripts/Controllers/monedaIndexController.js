﻿var monedaIndexController = angular.module('MonedaIndexController', []);
monedaIndexController.controller('TablaDeMonedasController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var MonedasUrl = '/api/moneda';
    $scope.resultadoEnPantalla = { monedas: [], totalMonedas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
       showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.monedas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalMonedas = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.EditarMoneda = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/MonedaForm?data=00046120", "FormularioMonedas", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.BorrarMoneda = function (Id) {

        bootbox.confirm('Esta seguro de eliminar la moneda?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = MonedasUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(MonedasUrl);
                }, ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = MonedasUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Monedas")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Monedas")
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'monedas')
            $scope.search();

    });




}]);