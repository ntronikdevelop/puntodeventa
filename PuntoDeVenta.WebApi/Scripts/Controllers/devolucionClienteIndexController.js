﻿var movimientoEntradaIndexController = angular.module('devolucionClienteIndexController', []);
movimientoEntradaIndexController.controller('tablaDevolucionClienteController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var devolucionClienteUrl = '/api/MovimientodevolucionCliente';
    $scope.resultadoEnPantalla = { movimientoEntradas: [], totalMovimientoEntradas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.devolucionesClientes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totaldevolucionesCliente = result.data.pagedData.count;
           
        }, ErrorResult);
        return promise;
    }

    $scope.editarDevolucionCliente = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/DevolucionClienteForm?data=00046120", "FormularioDevolucionCliente", { Id: Id, modal: modalUtils, ResultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'devolucioncliente')
        $scope.search();

    });

    $scope.borrarDevolucionCliente = function (Id) {

       bootbox.confirm('Esta seguro de eliminar esta entrada de devolución de este cliente?',function(desicion){
           if (desicion) {
               showLoading();
               var url = devolucionClienteUrl + '?idABorrar=' + Id;
               var promise = apiService.delete(url, null);
               promise.then(function (result) {
                   hideLoading();
                   bootbox.alert(result.data.mensaje);
                   llenarTabla(devolucionClienteUrl);
               }, ErrorResult);
           }
        }) 
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = devolucionClienteUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "devolucionCliente")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "devolucionCliente")
            $scope.search();

    });




}]);