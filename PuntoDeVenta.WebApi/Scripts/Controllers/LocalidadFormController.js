﻿var localidadesFormController = angular.module('NavegationApp');
localidadesFormController.controller('FormularioLocalidades', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var localidadesUrl = '/api/localidad';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.localidad = { id: 0, nombre: '', activo: true};
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarLocalidad($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarLocalidad = function () {
        var error = $scope.frmLocalidadForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.localidad;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/localidad", toPost, submitlocalidadSuccess, submitlocalidadError);
                break;
            default:
                apiService.put("/api/localidad", toPost, submitlocalidadSuccess, submitlocalidadError);
                break;
        }

    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.localidad.localidad = $scope.localidades.filter(t=> t.id == $scope.localidad.localidadId)[0];

        }, submitlocalidadError);
    }

 

    function submitlocalidadSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(localidadesUrl);
        if (result.data.status >= 0 && $scope.id) {
            $uibModalInstance.close();

        }

        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.localidad = {};

        }

        $scope.showValidate = false;
    }

    function submitlocalidadError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarLocalidad(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/localidad", { id: dataId })
                      .then(llenarlocalidad,submitlocalidadError);
        }
    }


    function llenarlocalidad(result) {
        hideLoading();
        $scope.localidad = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.localidad;
       
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos:!$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.localidades = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totallocalidades = result.data.pagedData.count;
        },submitlocalidadError);
        return promise;
    }
});