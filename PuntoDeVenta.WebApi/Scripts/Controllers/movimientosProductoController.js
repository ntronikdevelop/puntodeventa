﻿var reporteVentas = angular.module('movimientosProducto', []);
reporteVentas.controller('movimientosProductoController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/producto';
    var apiService = new ApiService();
    $scope.reporteMovimientos = {};
    $scope.date = new Date();

    $scope.Print = function () {
        window.print();
    }




    $scope.BuscarMovimientos = function () {
        var error = $scope.frmMovimientosProductoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

       


        showLoading();
        var toPost = $scope.busqueda;
        apiService.post("/api/producto/MovimientosProducto", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {

        empty = { fechaInicial: '', fechaFinal: '', productoId: null, almacenId: null }
        $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial ? $scope.busqueda.fechaInicial : '';
        $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal ? $scope.busqueda.fechaFinal : '';
        apiService.download('api/Producto/MovimientosProductoExcel', $scope.busqueda, empty, 'Movimientos de productos.xls', 'POST');
    }

    $scope.selectedProducto = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.producto = newValue.originalObject;
            $scope.busqueda.productoId = newValue.originalObject.id;
        }
        else {
            $scope.busqueda.producto = null;
            $scope.busqueda.productoId = null;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteMovimientos.reportePorAlmacen = result.data.data[0].reportePorAlmacen;
        }
        else
            bootbox.alert(result.data.Mensaje);



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }




    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;
    }

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal', 'busqueda.producto'], function (newValue, oldValue) {
        if ($scope.seleccion == "movimientosProducto")
            $scope.BuscarMovimientos();
    });
}]);