﻿var ImpuestosFormController = angular.module('NavegationApp');
ImpuestosFormController.controller('FormularioImpuestos', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var ImpuestosUrl = '/api/impuesto';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.impuesto = { id: 0, nombre: '', activo: true, porciento: 0 };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarImpuesto($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarImpuesto = function () {
        var error = $scope.frmImpuestoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
     showLoading();


        var toPost = $scope.impuesto;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/impuesto", toPost, submitImpuestoSuccess, submitImpuestoError);
                break;
            default:
                apiService.put("/api/impuesto", toPost, submitImpuestoSuccess, submitImpuestoError);
                break;
        }

    }

    function submitImpuestoSuccess(result) {
        hideLoading();
         bootbox.alert(result.data.mensaje);
            llenarTabla(ImpuestosUrl);
        if (result.data.status >= 0 && $scope.id>0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.impuesto = {};

        }


        $scope.showValidate = false;
    }

    function submitImpuestoError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
            else
         bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }

    function VerificarImpuesto(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/impuesto", { id: dataId })
                      .then(llenarImpuesto,submitImpuestoError);
        }
    }


    function llenarImpuesto(result) {
        hideLoading();
        $scope.impuesto = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.impuesto;
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.impuestos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalImpuestos = result.data.pagedData.count;
        },submitImpuestoError);
        return promise;
    }
});