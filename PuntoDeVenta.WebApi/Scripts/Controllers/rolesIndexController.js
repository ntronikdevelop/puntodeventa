﻿var rolesIndexController = angular.module('rolesIndexController', []);
rolesIndexController.controller('TablaDerolesController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var rolesUrl = '/api/roles';
    $scope.resultadoEnPantalla = { roles: [], totalRoles: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.roles = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalRoles = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.editarRoles = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/rolesForm?data=00046120", "FormularioRoles", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos  });
    }

    $scope.asignarPermisos = function (role) {
   
        showLoading();
        $scope.Id = role.id;
        modalUtils.openInstance("/Modals/permisosRoleForm?data=00046120", "FormularioPermisosRoles", { Role: role, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }





    $scope.borrarRole = function (roleId) {

        bootbox.confirm('Esta seguro de eliminar el rol?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = rolesUrl + '?idABorrar=' + roleId;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.message);
                    $scope.search();
                }, ErrorResult);
            }
        });
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = rolesUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion === "roles")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue === "roles")
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'roles')
            $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status === 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


}]);