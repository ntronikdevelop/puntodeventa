﻿var pagoGastoIndexController = angular.module('pagoGastoIndexModule', []);
pagoGastoIndexController.controller('tablaDePagoGastosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var pagosUrl = '/api/pagoGasto';
    $scope.resultadoEnPantalla = { pagoGastos: [], totalPagoGastos: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    $scope.filtroBusqueda = [
        { descripcion: 'No. pago', campo: 'Secuencia', tipoCampo: '0' }
    ]

    $scope.valorBusqueda = { campo: '', valor: '' };

    function llenarTabla(urlAUsar) {
       showLoading();
       var data = {
           page: $scope.resultadoEnPantalla.currentPage,
           size: 10,
           activo: !$scope.inactivos,
           campo: $scope.valorBusqueda.campo,
           tipoCampo: $scope.valorBusqueda.tipoCampo,
           valor: $scope.valorBusqueda.valor
       };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.pagoGastos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalPagoGastos = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }


    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'pagogastos')
        $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador.');
        }


    }

    $scope.EditarPagoGasto = function (secuencia) {
       showLoading();
       $scope.secuencia = secuencia;
        modalUtils.openInstance("/Modals/pagoGastoForm?data=00046120", "FormularioPagoGastos", { Secuencia: secuencia, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.imprimirRecibo = function (secuencia) {
        $window.open('/ImpresionEntrada/ImprimirReciboGasto?secuencia=' + secuencia, '_blank');
    }



    $scope.borrarPagoGasto = function (Id) {
        bootbox.confirm('Esta seguro de eliminar el registro del pago?',function(desicion){
            if (desicion) {
                showLoading();
                var url = pagosUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(pagosUrl);
                },ErrorResult);
            }
        })
  
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = pagosUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "PagoGastos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "PagoGastos")
            $scope.search();

    });




}]);