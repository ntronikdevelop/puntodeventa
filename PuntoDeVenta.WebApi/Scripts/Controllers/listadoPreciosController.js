﻿var listadoPrecios = angular.module('listadoPrecios', []);
listadoPrecios.controller('listadoPreciosController', ['$scope', '$filter', "$q", 'ApiService', '$window', function ($scope, $filter, $q, ApiService, $window) {
    hideLoading();
    $scope.ocultarCosto = true;
    $scope.esCreacion = true;
    var facturasUrl = '/api/producto';
    var apiService = new ApiService();
    $scope.reporte = {};
    $scope.date = new Date();
    $scope.filtroBusqueda = [
        { descripcion: 'Nombre', campo: 'Nombre', tipoCampo: '0' }
    ]
 
    $scope.valorBusqueda = { campo: '', valor: '', tipoCampo:0 };

    $scope.busquedaCompleja = {
        activo: true,
        filtros: new Array()
    };
    $scope.Print = function () {
        window.print();
    }
    $scope.buscarPrecios = function () {
        var error = $scope.frmListadoPreciosForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }


        $scope.agregarFiltro();

        showLoading();
        var toPost = $scope.busquedaCompleja;
        apiService.post("/api/producto/ProductosFiltrados", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {

        $scope.valorBusqueda.campo = $scope.valorBusqueda.campo == '' ? '^^' : $scope.valorBusqueda.campo;
        $scope.valorBusqueda.valor = $scope.valorBusqueda.valor == '' ? '^^' : $scope.valorBusqueda.valor;
        var toPost = $scope.valorBusqueda;
        apiService.download('api/producto/ProductosFiltradosExcel/' + toPost.campo + '/' + toPost.tipoCampo + '/' + toPost.valor, {}, {}, 'Listado de precios.xls', 'GET');
        $scope.valorBusqueda.campo = $scope.valorBusqueda.campo == '^^' ? '' : $scope.valorBusqueda.campo;
        $scope.valorBusqueda.valor = $scope.valorBusqueda.valor == '^^' ? '' : $scope.valorBusqueda.valor;
    }

    $scope.agregarFiltro = function () {
        let aGuardar = {};
        angular.copy($scope.valorBusqueda, aGuardar);
        if ($scope.busquedaCompleja.filtros) {
            let index = $scope.busquedaCompleja.filtros.findIndex(x => x.campo == aGuardar.campo);
            if (index >= 0) {
                $scope.busquedaCompleja.filtros.splice(index, 1);
            }
            $scope.busquedaCompleja.filtros.push({ campo: aGuardar.campo, tipoCampo: aGuardar.tipoCampo, valor: aGuardar.valor });
        }
       
        return;
    }








    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporte.productos = result.data.pagedData.result;
        }
        else
            bootbox.alert(result.data.Mensaje);



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }
}]);