﻿var zonaIndexController = angular.module('zonaIndexController', []);
zonaIndexController.controller('TablaDeZonaController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var zonasUrl = '/api/zona';
    $scope.resultadoEnPantalla = { zonas: [], totalZonas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.zonas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalZonas = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }

    $scope.EditarZona = function (Id) {
      showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/ZonaForm?data=00046120", "FormularioZonas", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.BorrarZona = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el registro de la zona?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = zonasUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(zonasUrl);
                },ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = zonasUrl;


        llenarTabla(urlAUsar);
    }
    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status === 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion === "zonas")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue === "zonas")
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() === 'zonas')
            $scope.search();

    });




}]);