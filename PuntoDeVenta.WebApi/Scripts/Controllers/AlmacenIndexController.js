﻿var almacenIndexController = angular.module('AlmacenIndexController', []);
almacenIndexController.controller('TablaDeAlmacenController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var almacensUrl = '/api/almacen';
    $scope.resultadoEnPantalla = { almacenes: [], totalAlmacenes: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.localidades = new Array();
    var apiService = new ApiService();
    $scope.inactivos = false;

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
       // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.almacenes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalalmacenes = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }

    $scope.EditarAlmacen = function (Id) {
      showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/AlmacenForm?data=00046120", "FormularioAlmacenes", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.BorrarAlmacen = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el registro del almacen?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = almacensUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(almacensUrl);
                },ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        cargarLocalidades();
        var urlAUsar = almacensUrl;


        llenarTabla(urlAUsar);
    }
    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
          

        }, ErrorResult);
    }

    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Almacen")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Almacen")
         
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'almacen')
            $scope.search();

    });

    $scope.$watch('localidades', function (newValue, oldValue) {
        if (newValue && newValue.length > 0 && $scope.resultadoEnPantalla.almacenes.length>0)
            for (var i = 0; i < $scope.resultadoEnPantalla.almacenes.length; i++)
            {
                $scope.resultadoEnPantalla.almacenes[i].localidad = $scope.localidades.filter(x => x.id == $scope.resultadoEnPantalla.almacenes[i].localidadId)[0];
            }

    });

    $scope.$watch('resultadoEnPantalla.almacenes', function (newValue, oldValue) {
        if (newValue && newValue.length > 0 && $scope.localidades.length > 0)
            for (var i = 0; i < $scope.resultadoEnPantalla.almacenes.length; i++) {
                $scope.resultadoEnPantalla.almacenes[i].localidad = $scope.localidades.filter(x => x.id == $scope.resultadoEnPantalla.almacenes[i].localidadId)[0];
            }

    });



}]);