﻿var movimientoEntradaIndexController = angular.module('movimientoEntradaIndexController', []);
movimientoEntradaIndexController.controller('TablaDeMovimientoEntradasController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var movimientoEntradasUrl = '/api/movimientoEntrada';
    $scope.resultadoEnPantalla = { movimientoEntradas: [], totalMovimientoEntradas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.movimientoEntradas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalMovimientoEntradas = result.data.pagedData.count;
           
        }, ErrorResult);
        return promise;
    }

    $scope.EditarmovimientoEntrada = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/MovimientoEntradaForm?data=00046120", "FormularioMovimientoEntrada", { Id: Id, modal: modalUtils, ResultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'movimientoentradas')
        $scope.search();

    });

    $scope.BorrarmovimientoEntrada = function (Id) {

       bootbox.confirm('Esta seguro de eliminar el movimiento de entrada de este artículo?',function(desicion){
           if (desicion) {
               showLoading();
               var url = movimientoEntradasUrl + '?idABorrar=' + Id;
               var promise = apiService.delete(url, null);
               promise.then(function (result) {
                   hideLoading();
                   bootbox.alert(result.data.mensaje);
                   llenarTabla(movimientoEntradasUrl);
               }, ErrorResult);
           }
        }) 
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = movimientoEntradasUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "movimientoEntradas")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "movimientoEntrada")
            $scope.search();

    });




}]);