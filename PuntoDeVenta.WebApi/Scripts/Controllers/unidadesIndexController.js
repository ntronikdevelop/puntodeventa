﻿var UnidadIndexController = angular.module('UnidadIndexController', []);
UnidadIndexController.controller('TablaDeUnidadesController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var unidadsUrl = '/api/Unidades';
    $scope.resultadoEnPantalla = { Unidads: [], totalUnidads: 0, currentPage: 1, pageSize: 10, search: null };
     $scope.inactivos = false;
    var apiService = new ApiService();
   
    function llenarTabla(urlAUsar) {
       showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };

        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.unidades = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalUnidades = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'unidades')
        $scope.search();

    });

    $scope.EditarUnidad = function (Id) {
       showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/UnidadesForm?data=00046119", "FormularioUnidades", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }



    $scope.BorrarUnidad = function (Id) {

        bootbox.confirm('Esta seguro de eliminar esta Unidad?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = unidadsUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(unidadsUrl);
                }, ErrorResult);
            }
        })

    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = unidadsUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {

     
        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion=="unidades")
        $scope.init();

    $scope.$watch('seleccion', function(newValue, oldValue) {
        if (newValue == "Unidades")
            $scope.search();

    });




}]);