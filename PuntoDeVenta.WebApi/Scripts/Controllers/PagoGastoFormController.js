﻿var pagoGastosFormController = angular.module('NavegationApp');
pagoGastosFormController.controller('FormularioPagoGastos', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var pagoGastosUrl = '/api/pagoGasto';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.montoPagoGastoAcc = new Array();
    $scope.id = modalFunctions.Id;
    // cargarMonedas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarPagoGasto($scope.id);
    $scope.closeDialogpagoGasto = function () {
        $uibModalInstance.close();
    }



    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;


        }, submitpagoGastoError);
    }


    $scope.filtrarPorMoneda = function () {
        $scope.pagoGasto.monedaId = $scope.pagoGasto.moneda.id;
        $scope.pagoGasto.tasa = $scope.pagoGasto.moneda.tasa;
        buscargastosPendientesSuplidor($scope.pagoGasto.suplidorId, $scope.pagoGasto.monedaId);
    }

    $scope.GuardarPagoGasto = function () {
        var error = $scope.frmpagoGastoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        if ($scope.pagoGasto.montoPagoGasto < $scope.montoPagoGastoAcc.sum('monto')) {
            bootbox.alert('Error al procesar el pagoGasto.  Favor verificar el monto pagado');
            return;
        }

        showLoading();
        let toPost = {};
        angular.copy($scope.pagoGasto, toPost);
        toPost.detallesPago = toPost.detallesPago? toPost.detallesPago.filter(d => d.seleccionado) : new Array();
        switch ($scope.id) {
            case undefined:
                apiService.post("/api/pagoGasto/aplicarPagos", toPost, submitpagoGastoSuccess, submitpagoGastoError);
                break;
            default:
                apiService.put("/api/pagoGasto", toPost, submitpagoGastoSuccess, submitpagoGastoError);
                break;
        }

    }


    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.pagoGasto.suplidor = newValue.originalObject;
            $scope.pagoGasto.suplidorId = newValue.originalObject.id;
            buscargastosPendientesSuplidor($scope.pagoGasto.suplidorId, $scope.pagoGasto.monedaId);
        }
        else {
            $scope.pagoGasto.suplidor = null;
            $scope.pagoGasto.suplidorId = 0;
          
        }


    }

    function buscargastosPendientesSuplidor(suplidorId, monedaId) {
        showLoading();
       
        apiService.post("/api/Gasto/ReporteCxP", { suplidorId: suplidorId, monedaId: monedaId }).then(function (result) {
            hideLoading();
            $scope.pagoGasto.detallesPago = result.data.data[0].gastos;
            $scope.montoPagoGastoAcc = new Array();

        }, submitpagoGastoError);
    }

    $scope.verificarMontoAPagar = function (detalle) {
        if (detalle.montoAPagar > detalle.montoAdeudado) {
            bootbox.alert('El monto a pagar no debe exceder el monto adeudado');
            detalle.montoAPagar = 0;
        }
        var index = $scope.montoPagoGastoAcc.findIndex(x => x.referenciaGasto == detalle.referenciaGasto);
        if (index >= 0) {
            $scope.montoPagoGastoAcc[index].monto = detalle.montoAPagar;
        }
    }

    $scope.AbonarGasto = function (detalle) {

        if (detalle.seleccionado) {

            if (($scope.pagoGasto.montoPagoGasto - $scope.montoPagoGastoAcc.sum('monto')) >= detalle.montoAdeudado) {
                detalle.montoAPagar = detalle.montoAdeudado;

            }
            else if (($scope.pagoGasto.montoPagoGasto - $scope.montoPagoGastoAcc.sum('monto')) >= 0) {
                //   angular.copy(($scope.pagoGasto.montoPagoGasto - $scope.montoPagoGastoAcc.sum('monto')), detalle.montoAPagar);
                detalle.montoAPagar = ($scope.pagoGasto.montoPagoGasto - $scope.montoPagoGastoAcc.sum('monto'));
            }
            else {
                detalle.montoAPagar = 0;

            }
            if (detalle.montoAPagar > 0)
                $scope.montoPagoGastoAcc.push({ referenciaGasto: detalle.referenciaGasto, monto: detalle.montoAPagar });
            else
                detalle.seleccionado = false;
        }
        else {
            detalle.montoAPagar = 0;
            var index = $scope.montoPagoGastoAcc.findIndex(x => x.referenciaGasto == detalle.referenciaGasto);
            if (index >= 0)
                $scope.montoPagoGastoAcc.splice(index, 1);
        }

    }

    $scope.$watch('pagoGasto.montoPagoGasto', function (newValue, oldValue) {
        // angular.copy($scope.pagoGasto.montoPagoGasto, $scope.montoPagoGastoAcc);
    });

    function submitpagoGastoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            llenarTabla(pagoGastosUrl);
            $window.open('/ImpresionEntrada/ImprimirReciboGasto?secuencia=' + result.data.codigo, '_blank');
            buscargastosPendientesSuplidor($scope.pagoGasto.suplidorId, $scope.pagoGasto.monedaId);
            $uibModalInstance.close();

        }
       


        $scope.showValidate = false;
    }

    function submitpagoGastoError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarPagoGasto(secuencia) {
        if (secuencia) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/pagoGasto", { secuencia: secuencia })
                .then(llenarpagoGasto, submitpagoGastoError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarpagoGasto(result) {
        hideLoading();

    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.pagoGastos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalPagoGastos = result.data.pagedData.count;
        }, submitpagoGastoError);
        return promise;
    }




});