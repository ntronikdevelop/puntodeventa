﻿var SuplidoresIndexController = angular.module('SuplidoresIndexController', []);
SuplidoresIndexController.controller('TablaDeSuplidoresController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
   
    var modalUtils = new ModalUtils();
    var SuplidoresUrl = '/api/suplidor';
    $scope.resultadoEnPantalla = { suplidores: [], totalSuplidores: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();
   
    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };

        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.suplidores = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalSuplidores = result.data.pagedData.count;
           
        }, ErrorResult);
        return promise;
    }

    $scope.EditarSuplidores = function (Id) {
         showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/SuplidoresForm?data=00046119", "FormularioSuplidores", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.BorrarSuplidores = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el Suplidor?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = SuplidoresUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(SuplidoresUrl);
                }, ErrorResult);
            }
        })

    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase()=='suplidores')
        $scope.search();

    });

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = SuplidoresUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {

     
        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion=="Suplidores")
        $scope.init();

    $scope.$watch('seleccion', function(newValue, oldValue) {
        if (newValue == "Suplidores")
            $scope.search();

    });




}]);