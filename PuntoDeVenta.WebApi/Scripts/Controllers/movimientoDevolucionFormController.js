﻿var movimientosDevolucionsFormController = angular.module('NavegationApp');
movimientosDevolucionsFormController.controller('FormularioMovimientoDevolucion', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var movimientosDevolucionsUrl = '/api/movimientoDevolucion';
    var apiService = new ApiService();
    $scope.esEdicion = false;
    $scope.editandoMovimiento = false;
    hideLoading();
    $scope.localidades = new Array();
    $scope.almacenes = new Array();
    $scope.id = modalFunctions.Id;
    $scope.movimientoVacio = { cantidad: 0, montoItbis: 0, montoTotal: 0, productoId: 0, suplidorId: 0  };
    $scope.movimientosDevolucionTemp = { cantidad:0, montoItbis:0, montoTotal:0, localidadId:0, productoId:0, suplidorId:0, almacenId:0  };
    $scope.movimientosDevolucion = { suplidor:null, localidad:null, referencia:null , almacen:null, movimientosDevoluciones: new Array() };
    $scope.resultadoEnPantalla = modalFunctions.ResultadoEnPantalla;
    cargarImpuestos();
    cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarmovimientosDevolucion = function () {
        if (!$scope.movimientosDevolucion.suplidor) {
            bootbox.alert('Favor seleccionar un suplidor');
            return;
        }

        if (!$scope.movimientosDevolucion.almacen) {
            bootbox.alert('Favor seleccionar un almacen');
            return;
        }
        //if (!$scope.movimientosDevolucion.referencia) {
        //    bootbox.alert('La referencia es obligatoria. Favor ingresar el numero de control del suplidor');
        //    return;
        //}
        showLoading();
        var toPost = {};
        angular.copy($scope.movimientosDevolucion, toPost);

        apiService.post("/api/movimientoDevolucion/RegistrarSalidas", toPost, submitmovimientosDevolucionSuccess, submitmovimientosDevolucionError);




    }




    $scope.convertirListaEnTexto = function (lista) {
        var texto = "";

        for (var i = 0; i < lista.length; i++) {
            texto += texto == "" ? lista[i] : "," + lista[i];
        }

        return texto;
    }

    $scope.agregarMovimiento = function () {
        var error = $scope.frmMovimientosDevolucionForm.$error;
        if (error.required || error["autocomplete-required"] || error.min) {
            $scope.showValidate = true;
            return;
        }


        let movimientosDevolucionAGuardar = {};
        angular.copy($scope.movimientosDevolucionTemp, movimientosDevolucionAGuardar);
        var index = $scope.movimientosDevolucion.movimientosDevoluciones.map(function (el) {
            return el.producto.id;
        }).indexOf(movimientosDevolucionAGuardar.producto.id);

        if (index >= 0) {
            $scope.movimientosDevolucion.movimientosDevoluciones.splice(index, 1);
            $scope.movimientosDevolucion.movimientosDevoluciones.push(movimientosDevolucionAGuardar);
        }

        else {
            $scope.movimientosDevolucion.movimientosDevoluciones.push(movimientosDevolucionAGuardar);
        }
        angular.copy($scope.movimientoVacio, $scope.movimientosDevolucionTemp);
        $scope.$broadcast('angucomplete-alt:clearInput', 'productoNombre');
        $scope.editandoMovimiento = false;
    }

    $scope.EditarMovimiento = function (movimiento) {
        angular.copy(movimiento, $scope.movimientosDevolucionTemp);
        $scope.editandoMovimiento = true;
    }

    $scope.AnularMovimiento = function (index) {
        $scope.movimientosDevolucion.movimientosDevoluciones.splice(index, 1);
    }



    $scope.$watch('movimientosDevolucionTemp.localidadId', function (newValue, oldValue) {
        if (newValue )
            cargarAlmacenes(newValue);

    });

 

    $scope.$watchGroup(['movimientosDevolucionTemp.productoUnidad', 'movimientosDevolucionTemp.cantidad'], function (newVal, oldVal) {
        if (newVal[0]) {
            $scope.movimientosDevolucionTemp.costoActual = newVal[0].precioCompra;
            $scope.movimientosDevolucionTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientosDevolucionTemp.cantidad * $scope.movimientosDevolucionTemp.costoActual);
            $scope.movimientosDevolucionTemp.montoTotal = $scope.movimientosDevolucionTemp.cantidad * ($scope.movimientosDevolucionTemp.costoActual * (1 + $scope.impuesto.porciento));

        }
        else {
            $scope.movimientosDevolucionTemp.costoActual = 0;
            $scope.movimientosDevolucionTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientosDevolucionTemp.cantidad * $scope.movimientosDevolucionTemp.costoActual);
            $scope.movimientosDevolucionTemp.montoTotal = $scope.movimientosDevolucionTemp.cantidad * ($scope.movimientosDevolucionTemp.costoActual * (1 + $scope.impuesto.porciento));

        }
    });

    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            for (var i = 0; i < $scope.movimientosDevolucionTemp.producto.productoUnidades.length; i++) {
               
                $scope.movimientosDevolucionTemp.producto.productoUnidades[i].unidad = $scope.unidades.filter(t=>t.id ==
                $scope.movimientosDevolucionTemp.producto.productoUnidades[i].unidadId)[0];
            }

            $scope.unidadesCargadas = true;

        }, submitmovimientosDevolucionError);
    }





    $scope.$watch('movimientosDevolucionTemp.cantidad', function (newValue, oldValue) {

        
        if (newValue > 0 && $scope.movimientosDevolucionTemp.producto)
        {
            $scope.movimientosDevolucionTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientosDevolucionTemp.cantidad * $scope.movimientosDevolucionTemp.costoActual);
            $scope.movimientosDevolucionTemp.montoTotal = $scope.movimientosDevolucionTemp.cantidad * ($scope.movimientosDevolucionTemp.costoActual * (1 + $scope.impuesto.porciento));
        }
           

    });


 

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();

            $scope.localidades = result.data.data;
            if (result.data.data && result.data.data.length == 1) {
                $scope.movimientosDevolucion.localidad = $scope.localidades[0];
                $scope.movimientosDevolucionTemp.localidadId = $scope.movimientosDevolucion.localidad.id;
            }

        }, submitmovimientosDevolucionError);
    }

    function cargarAlmacenes(localidadId) {
        if (localidadId) {

            let url = '';
            let parametro;
            switch (localidadId) {
                case -1:
                    url = "/api/almacen/ObtenerAlmacenesPorCodigo/"
                    parametro = 'DEF';
                    break;

                default:
                    url = "/api/almacen/ObtenerAlmacenesPorLocalidad/"
                    parametro = localidadId;
                    break;
            }
            showLoading();
            apiService.get(url + parametro, null).then(function (result) {
                hideLoading();
                $scope.almacenes = [{ id: 0, nombre: '' }];
                $scope.almacenes = result.data.data;


            }, submitmovimientosDevolucionError);
        }

    }

    function cargarImpuestos() {
        
            showLoading();
            apiService.get("/api/impuesto", { pagesize :1, nombreCampo:'nombre', filtro:'itbis'}).then(function (result) {
                hideLoading();
               
                $scope.impuesto = result.data.pagedData.result[0];


            }, submitmovimientosDevolucionError);
        

    }

    function cargarProductoUnidades(productoId) {

        showLoading();
        apiService.get("/api/productounidades/obenerunidadesdeProducto/" + productoId,null).then(function (result) {
            hideLoading();

           
            $scope.movimientosDevolucionTemp.producto.productoUnidades = result.data.data;
          
                cargarUnidades();

        }, submitmovimientosDevolucionError);


    }


    function submitmovimientosDevolucionSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            var urlAUsar = movimientosDevolucionsUrl;
            angular.copy($scope.movimientoVacio, $scope.movimientosDevolucion);
            angular.copy({}, $scope.movimientosDevolucionTemp);
            llenarTabla(urlAUsar);
            $uibModalInstance.close();

        }



        $scope.showValidate = false;
    }

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.movimientosDevolucion.suplidor = newValue.originalObject;
            $scope.movimientosDevolucionTemp.suplidorId = $scope.movimientosDevolucion.suplidor.id;
        }
        else {
            $scope.movimientosDevolucion.suplidor = null;
            $scope.movimientosDevolucionTemp.suplidorId = 0;
        }


    }

    $scope.$watch('unidadesCargadas', function (newValue, oldValue) {
        if (newValue)
        {
            if (!$scope.movimientosDevolucionTemp.productoUnidad || !$scope.movimientosDevolucionTemp.unidadId || $scope.movimientosDevolucionTemp.unidadId <= 0) {
                $scope.movimientosDevolucionTemp.productoUnidad = $scope.movimientosDevolucionTemp.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                $scope.movimientosDevolucionTemp.unidadId = $scope.movimientosDevolucionTemp.productoUnidad.unidadId;
            }
        }
    })

    $scope.selectedProducto = function (newValue, oldValue) {

        if (newValue) {
        
           
            $scope.movimientosDevolucionTemp.producto = newValue.originalObject;
            $scope.movimientosDevolucionTemp.productoId = $scope.movimientosDevolucionTemp.producto.id;
            cargarProductoUnidades($scope.movimientosDevolucionTemp.productoId);
            $scope.movimientosDevolucionTemp.montoItbis = ($scope.impuesto.porciento * $scope.movimientosDevolucionTemp.cantidad * $scope.movimientosDevolucionTemp.costoActual);
            $scope.movimientosDevolucionTemp.montoTotal = $scope.movimientosDevolucionTemp.cantidad * ($scope.movimientosDevolucionTemp.costoActual * (1 + $scope.impuesto.porciento));
        }
        else {
            $scope.unidadesCargadas = false;
            $scope.movimientosDevolucionTemp.producto = null;
            $scope.movimientosDevolucionTemp.productoId = 0;
        }


    }

    function submitmovimientosDevolucionError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }





    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.movimientoDevoluciones = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalMovimientoDevoluciones = result.data.pagedData.count;
        },submitmovimientosDevolucionError);
        return promise;
    }





});