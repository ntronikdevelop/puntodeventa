﻿var reporteVentas = angular.module('ReporteVentas', []);
reporteVentas.controller('ReporteVentasController', ['$scope', '$filter', "$q", 'ApiService', '$window','$http', function ($scope, $filter, $q, ApiService, $window,$http) {
    hideLoading();
    $scope.esCreacion = true;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.reporteGeneral = {};
    $scope.date = new Date();

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.factura.moneda = $scope.monedas.filter(t => t.id == $scope.factura.monedaId)[0];

        }, submitFacturaError);
    }

    $scope.Print = function () {
        window.print();
    }


    $scope.BuscarFacturas = function () {
        var error = $scope.frmReporteVentasForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }


        showLoading();
        var toPost = $scope.busqueda;
        apiService.post("/api/Factura/ReporteDeVentas", toPost, submitFacturaSuccess, submitFacturaError);

    }

    $scope.exportarExcel = function () {
        var error = $scope.frmReporteVentasForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        $scope.busqueda.fechaFinal = $scope.busqueda.fechaFinal ? $scope.busqueda.fechaFinal : '';
        $scope.busqueda.fechaInicial = $scope.busqueda.fechaInicial ? $scope.busqueda.fechaInicial : '';
        empty = { fechaInicial: '', fechaFinal: '', clienteId: null, monedaId: null };
        apiService.download('api/Factura/ReporteVentasExcel', $scope.busqueda, empty, 'Reporte de ventas.xls','POST');
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.cliente = newValue.originalObject;
            $scope.busqueda.clienteId = newValue.originalObject.id;
          
        }
        else {
            $scope.busqueda.cliente = null;
            $scope.busqueda.clienteId = null;
        }


    }

    $scope.selectedVendedor = function (newValue, oldValue) {

        if (newValue) {
            $scope.busqueda.vendedor = newValue.originalObject;
            $scope.busqueda.vendedorId = newValue.originalObject.id;

        }
        else {
            $scope.busqueda.vendedor = null;
            $scope.busqueda.vendedorId = null;
        }


    }




    function submitFacturaSuccess(result) {
        hideLoading();

        if (result.data.status >= 0) {
            $scope.busquedaFinalizada = true;
            $scope.reporteGeneral.reportePorMonedas = result.data.data[0].facturasPorMonedas;
        }



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }




    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, submitFacturaError);
        return promise;
    }

    $scope.$watchGroup(['busqueda.fechaInicial', 'busqueda.fechaFinal', 'busqueda.vendedorId', 'busqueda.clienteId'], function (newValue, oldValue) {
        if ($scope.seleccion == "ReporteVentas")
        $scope.BuscarFacturas();
    });
}]);