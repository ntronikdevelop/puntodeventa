﻿var permisosIndexController = angular.module('PermisosIndexController', []);
permisosIndexController.controller('TablaDePermisosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var permisosUrl = '/permisos';
    $scope.resultadoEnPantalla = { permisos: [], totalPermisos: 0, currentPage: 1, pageSize: 10, search: null };

    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.permisos = result.data.permisos;
            $scope.resultadoEnPantalla.totalPermisos = result.data.totalPermisos;
        }, ErrorResult);
        return promise;
    }

    $scope.EditarPermisos = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/PermisoForm?data=00046120", "FormularioPermisos", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla });
    }





    $scope.BorrarPermiso = function (userId, seccion) {

        bootbox.confirm('Esta seguro de eliminar los permisos para esta sección?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = permisosUrl + '/BorrarPermisoAUsuario?userId=' + userId + '&seccion='+seccion;
                var promise = apiService.get(url, null);
                promise.then(function (result) {
                hideLoading();
                    bootbox.alert(result.data.message);
                    $scope.search();
                }, ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = permisosUrl + '/ObtenerDatosDeUsuarios';


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Permisos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Permisos")
            $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


}]);