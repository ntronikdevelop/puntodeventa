﻿var facturacionIndexController = angular.module('FacturacionIndexController', []);

facturacionIndexController.controller('ListController', ['$scope', 'ApiService',"$filter","$q", function ($scope, ApiService,$filter,$q) {
    var facturacionUrl = '/Facturacion/Facturacion/ObtenerFacturasPaginado';
 
    $scope.estadoFacturaId = 0;
    $scope.resultadoEnPantalla = { facturas: [], totalFacturas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.filtros = [{ label: "Numero File", codigo: "file", dataField: "CodigoFile" ,searchField : "CodigoFile" },
                      { label: "Cliente", codigo: "cliente", dataField: "Id", searchField: "IdCliente" }];
    
    var selectionFactura = ['Id', 'Concepto', 'TipoServicioFacturacion.NombreTipoServicio', 'Empresa.Nombre', 'Cliente.Nombre', 'CodigoFile', 'FechaSolicitudServicio', 'Departamento.Nombre',
        'fechaRecepcionDocumentacion', 'estadoInstruccionFacturacion.nombreEstado', 'Activo', 'CategoriasFacturas.Descripcion','NoFactura', 'NoDocumento'];
    var apiService = new ApiService();
    ObtenerEstadosFactura();
    function llenarTabla(urlAUsar)
    {
        var data = {};
        apiService.addPagination(data, $scope.resultadoEnPantalla.currentPage, $scope.resultadoEnPantalla.pageSize);
        apiService.addSelection(data, selectionFactura);
        apiService.addFilter(data, "Activo", true);
        apiService.orderBy(data, ["Id"], true);
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            $scope.resultadoEnPantalla.facturas = result.data.data;
            $scope.resultadoEnPantalla.totalFacturas = result.data.count;
        });
        return promise;
    }

    $scope.BuscarPorEstado = function (estadoId) {
        $scope.estadoFacturaId = estadoId;
        $scope.search();
    }

    $scope.search = function (propiedad) {
        var urlAUsar = facturacionUrl;

        var separador = "?";
        if($scope.estadoFacturaId > 0){
            urlAUsar += separador + 'EstadoInstruccionFacturacionId=' + $scope.estadoFacturaId;
            separador = "&";
        }

        if ($scope.soloFacturas) {
            urlAUsar += separador + "Facturado=true";
            separador = "&";
        }


        if ($scope.filterValue)
        urlAUsar += separador + $scope.filtroSeleccionado.searchField + '=' + $scope.filterValue.originalObject[$scope.filtroSeleccionado.dataField];
        llenarTabla(urlAUsar);
    }

    $scope.init = function () {

        $scope.filtroSeleccionado = $scope.filtros[0];
        $q.all([
              $scope.search()
        ])
    };

        $scope.pageChanged = function () {
            $scope.search();
        };

    $scope.ConvertToDate= function (csDate){
       
        if (csDate != null && csDate != undefined && csDate != "")
        {
            var dateNumeric = parseInt(csDate.substring(6, csDate.length - 2));
            var date = new Date(dateNumeric);
            return ((date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear());
        }
        return "";
    }

    $scope.BorrarFactura = function (id) {
       
        if (confirm('Esta seguro de eliminar esta instrucción?')) {
            LlamarLoading();
            var promise = apiService.get('/Facturacion/Facturacion/Borrar/' + id, null);
            promise.then(function (result) {
                QuitarLoading();
                alert(result.data.Message);
                if (result.data.Status >= 0) {
                    llenarTabla();
                }

            });
           
            return promise;
        }
        return null;
    }

    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ObtenerEstadosFactura() {
        var data = {};
        var promise = apiService.get('/Facturacion/EstadoInstruccionFacturacion/ObtenerTodos', data);
        apiService.addSelection(data, selectionFactura);
        promise.then(function (result) {
            $scope.EstadosFactura = result.data.listResult.DeserializedData;
        });
        return promise;
    }
   
    $scope.init();

   

}]);