﻿var clienteIndexController = angular.module('ClienteIndexController', []);
clienteIndexController.controller('TablaDeClientesController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    $scope.filtroBusqueda = [
        { descripcion: 'Nombre', campo: 'Nombre', tipoCampo:'0' },
        { descripcion: 'Cedula / RNC', campo: 'CedulaRnc', tipoCampo: '0' }
    ]

    $scope.valorBusqueda = {campo:'', valor:''};

    
    var modalUtils = new ModalUtils();
    var clientesUrl = '/api/cliente';
    $scope.resultadoEnPantalla = { clientes: [], totalClientes: 0, currentPage: 1, pageSize: 10, search: null };
     $scope.inactivos = false;
    var apiService = new ApiService();
   
    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: $scope.valorBusqueda.campo,
            tipoCampo: $scope.valorBusqueda.tipoCampo,
            valor: $scope.valorBusqueda.valor
        };

        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.clientes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalClientes = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'clientes')
        $scope.search();

    });

    $scope.EditarCliente = function (Id) {
       showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/ClienteForm?data=00046119", "FormularioClientes", {
            Id: Id,
            modal: modalUtils,
            resultadoEnPantalla: $scope.resultadoEnPantalla,
            inactivos: $scope.inactivos,
            valorBusqueda: $scope.valorBusqueda
        });
    }

    $scope.BorrarCliente = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el cliente?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = clientesUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(clientesUrl);
                }, ErrorResult);
            }
        })

    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = clientesUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {

     
        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion=="Clientes")
        $scope.init();

    $scope.$watch('seleccion', function(newValue, oldValue) {
        if (newValue == "Clientes")
            $scope.search();

    });




}]);