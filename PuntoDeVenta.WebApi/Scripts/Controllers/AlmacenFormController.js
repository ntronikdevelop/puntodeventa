﻿var almacensFormController = angular.module('NavegationApp');
almacensFormController.controller('FormularioAlmacenes', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var almacensUrl = '/api/almacen';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.almacen = { id: 0, nombre: '', activo: true, localidad: '' };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarAlmacen($scope.id);
    cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarAlmacen = function () {
        var error = $scope.frmAlmacenForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.almacen;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/almacen", toPost, submitAlmacenSuccess, submitAlmacenError);
                break;
            default:
                apiService.put("/api/almacen", toPost, submitAlmacenSuccess, submitAlmacenError);
                break;
        }

    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.almacen.localidad = $scope.localidades.filter(t=> t.id == $scope.almacen.localidadId)[0];

            if ($scope.localidades.length == 1) {
                $scope.almacen.localidad = $scope.localidades[0];
                $scope.almacen.localidadId = $scope.almacen.localidad.id;
            }
               

        }, submitAlmacenError);
    }



    function submitAlmacenSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(almacensUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.almacen = {};

        }



        $scope.showValidate = false;
    }

    function submitAlmacenError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarAlmacen(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/almacen", { id: dataId })
                      .then(llenarAlmacen, submitAlmacenError);
        }
    }


    function llenarAlmacen(result) {
        hideLoading();
        $scope.almacen = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.almacen;

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
        // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.almacenes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalalmacenes = result.data.pagedData.count;
        }, submitAlmacenError);
        return promise;
    }
});