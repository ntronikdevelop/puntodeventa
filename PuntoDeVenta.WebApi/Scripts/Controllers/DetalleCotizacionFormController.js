﻿var DetalleFacturasFormController = angular.module('NavegationApp');
DetalleFacturasFormController.controller('FormularioDetalleCotizaciones', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var DetalleFacturasUrl = '/api/DetalleFactura';
    $scope.esEdicion = false;
    $scope.currentIndex = modalFunctions.currentIndex;
    $scope.porcientoDescuentoGral = modalFunctions.porcientoDescuento ? modalFunctions.porcientoDescuento : 0;
    var apiService = new ApiService();
    $scope.puedeFacturar = false;
    $scope.monedaProducto = null;
    $scope.defaultImpuesto = {};
    $scope.factura = modalFunctions.Factura;
    $scope.esEdicion = modalFunctions.esEdicion;



    $scope.defaultDetalle = {
        id: 0,
        productoId: 0,
        activo: true,
        cantidad: 1,
        unidadId: null,
        unidad: null,
        monto: 0,
        costo: 0,
        producto: null,
        impuestos:[],
        moneda: $scope.factura.moneda,
        montoItbis: 0,
        montoTotal: 0,
        facturaId: $scope.factura.id,
        impuesto: null,
        impuestoId: 0,
        montoGrabado: 0,
        comentario: '',
        montoDop: 0,
        montoDescuento: 0,
        porcientoDescuento: $scope.porcientoDescuentoGral,
        esGratis: false

    };

    $scope.detalleFactura = {
        id: 0,
        productoId: 0,
        activo: true,
        unidadId: null,
        unidad:null,
        cantidad: 1,
        costo: 0,
        impuestos:[],
        monto: 0,
        producto: null,
        moneda: $scope.factura.moneda,
        montoItbis: 0,
        montoTotal: 0,
        facturaId: $scope.factura.id,
        impuesto: null,
        impuestoId: 0,
        montoGrabado: 0,
        comentario: '',
        montoDop: 0,
        montoDescuento: 0,
        porcientoDescuento: $scope.porcientoDescuentoGral,
        esGratis: false

    };


    if ($scope.esEdicion) {
        $scope.detalleFactura = modalFunctions.DetalleActual;
        $scope.detalleFactura.moneda = $scope.factura.moneda;
        $scope.monedaProducto = modalFunctions.DetalleActual.producto.moneda;
        cargarProductoUnidades(modalFunctions.DetalleActual.producto);
        $scope.puedeFacturar = true;
        let constanteConversion = 1;
        if ($scope.factura.moneda.esMonedaLocal && !$scope.monedaProducto.esMonedaLocal) {
            constanteConversion = $scope.monedaProducto.tasa * $scope.factura.moneda.tasa;
        }
        else if (!$scope.factura.moneda.esMonedaLocal && $scope.monedaProducto.esMonedaLocal) {
            constanteConversion = $scope.monedaProducto.tasa / $scope.factura.moneda.tasa;
        }
        else if (!$scope.factura.moneda.esMonedaLocal && !$scope.monedaProducto.esMonedaLocal) {
            constanteConversion = !$scope.factura.moneda * (1 / $scope.monedaProducto.tasa);
        }
        $scope.preciosProducto = [{ id: 1, monto: $scope.detalleFactura.producto.precio * constanteConversion }, { id: 2, monto: $scope.detalleFactura.producto.precio2 * constanteConversion }, { id: 3, monto: $scope.detalleFactura.producto.precio3 * constanteConversion }];
        $scope.detalleFactura.precioProductoSel = ($scope.preciosProducto.filter(p => p.monto == $scope.detalleFactura.monto)[0] * constanteConversion);
        ObtenerImpuestosProducto($scope.detalleFactura.producto);
    }




    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            for (var i = 0; i < $scope.detalleFactura.producto.productoUnidades.length; i++) {

                $scope.detalleFactura.producto.productoUnidades[i].unidad = $scope.unidades.filter(t => t.id ==
                    $scope.detalleFactura.producto.productoUnidades[i].unidadId)[0];
            }



        }, submitDetalleFacturaError);
    }



    function cargarProductoUnidades(producto, seleccionarUnidad = true) {
        if (producto && !producto.esServicio && seleccionarUnidad) {
            if (!producto.productoUnidades || producto.productoUnidades.length == 0) {
                showLoading();
                apiService.get("/api/productounidades/obenerunidadesdeProducto/" + producto.id, null).then(function (result) {
                    hideLoading();


                    $scope.detalleFactura.producto.productoUnidades = result.data.data;
                    if (!$scope.detalleFactura.productoUnidad || $scope.detalleFactura.productoUnidad.id <= 0) {
                        $scope.detalleFactura.productoUnidad = $scope.detalleFactura.unidadId && $scope.detalleFactura.unidadId > 0 ? $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.unidadId == $scope.detalleFactura.unidadId)[0] :
                            $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                        if (!$scope.detalleFactura.productoUnidad)
                            $scope.detalleFactura.productoUnidad = $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                        seleccionDeUnidad();

                    }



                }, submitDetalleFacturaError);
            }
            else {
                $scope.detalleFactura.unidadId && $scope.detalleFactura.unidadId > 0 ? $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.unidadId == $scope.detalleFactura.unidadId)[0] :
                    $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                if (!$scope.detalleFactura.productoUnidad)
                    $scope.detalleFactura.productoUnidad = $scope.detalleFactura.producto.productoUnidades.filter(pu => pu.esPrincipal)[0];
                seleccionDeUnidad();
                //  cargarExistenciaLocalidad(producto.id);
            }
        }

        if (!seleccionarUnidad) {
            $scope.detalleFactura.montoDescuento = $scope.detalleFactura.cantidad *
                ($scope.detalleFactura.porcientoDescuento * $scope.detalleFactura.monto) / 100;
            $scope.detalleFactura.montoItbis = obtenerMontoImpuestos($scope.detalleFactura.producto);
            $scope.detalleFactura.montoTotal = $scope.detalleFactura.cantidad *
                ($scope.detalleFactura.monto - ($scope.detalleFactura.montoDescuento / $scope.detalleFactura.cantidad)) + $scope.detalleFactura.montoItbis;

        }
    }

    function ObtenerImpuestosProducto(producto, seleccionarUnidad = true) {

        if (producto && producto.id > 0) {
            apiService.get(`/api/producto/ObtenerImpuestosDeProducto/${producto.id}`)
                .then(function (result2) {
                    $scope.productoImpuestos = result2.data.data;
                    $scope.detalleFactura.impuestos = result2.data.data;
                    $scope.detalleFactura.precioProductoSel = !$scope.detalleFactura.precioProductoSel ? $scope.preciosProducto[0] : $scope.detalleFactura.precioProductoSel;
                    let precioProducto = $scope.detalleFactura.precioProductoSel.monto;
                    $scope.puedeFacturar = true;
                    $scope.detalleFactura.productoId = producto.id;
                    if (producto.esServicio) {
                        if ($scope.detalleFactura.monto <= 0) {
                            $scope.detalleFactura.monto = ((precioProducto * producto.moneda.tasa) / $scope.factura.moneda.tasa);
                        }
                        $scope.detalleFactura.montoDescuento = $scope.detalleFactura.cantidad *
                            ($scope.detalleFactura.porcientoDescuento * $scope.detalleFactura.monto) / 100;
                        $scope.detalleFactura.montoItbis = obtenerMontoImpuestos($scope.detalleFactura.producto);
                        $scope.detalleFactura.montoTotal = $scope.detalleFactura.cantidad *
                            ($scope.detalleFactura.monto - ($scope.detalleFactura.montoDescuento / $scope.detalleFactura.cantidad)) + $scope.detalleFactura.montoItbis;
                    }

                    else {
                        cargarProductoUnidades(producto, seleccionarUnidad);
                    }


                }, submitDetalleFacturaError);
        }

    }

    function obtenerMontoImpuestos(producto) {
        let totalImpuesto = 0;
        if (!$scope.detalleFactura.impuestos || $scope.detalleFactura.impuestos.length == 0)
            return totalImpuesto;

        for (let i = 0; i < $scope.detalleFactura.impuestos.length; i++) {
            totalImpuesto += ($scope.detalleFactura.impuestos[i].impuesto.porciento *
                ($scope.detalleFactura.monto - ($scope.detalleFactura.montoDescuento / $scope.detalleFactura.cantidad)) * $scope.detalleFactura.cantidad);
        }

        return totalImpuesto;

    }


    function cargarProductoUnidadesConMonto(producto) {
        if (!producto.productoUnidades || producto.productoUnidades.length === 0) {
            showLoading();
            apiService.get("/api/productounidades/obenerunidadesdeProducto/" + producto.id, null).then(function (result) {
                hideLoading();


                $scope.detalleFactura.producto.productoUnidades = result.data.data;
                $scope.detalleFactura.productoUnidad = $scope.detalleFactura.producto.productoUnidades.filter(x => x.unidadId == $scope.detalleFactura.unidad.id)[0];
                $scope.detalleFactura.productoUnidad = !$scope.detalleFactura.productoUnidad ? $scope.detalleFactura.producto.productoUnidades.filter(x => x.esPrincipal)[0] : $scope.detalleFactura.productoUnidad;
                $scope.detalleFactura.monto = $scope.detalleFactura.monto <= 0 && $scope.detalleFactura.productoUnidad ?
                    (($scope.detalleFactura.productoUnidad.precioVenta * $scope.detalleFactura.producto.moneda.tasa) / $scope.factura.moneda.tasa) :
                    $scope.detalleFactura.monto;

                ObtenerImpuestosProducto($scope.detalleFactura.producto);
                //  cargarExistenciaLocalidad(producto.id);

            }, submitDetalleFacturaError);
        }
        else {
            $scope.detalleFactura.productoUnidad = $scope.detalleFactura.producto.productoUnidades.filter(x => x.unidadId == $scope.detalleFactura.unidad.id)[0];
            $scope.detalleFactura.monto = $scope.detalleFactura.monto <= 0 && $scope.detalleFactura.productoUnidad ?
                (($scope.detalleFactura.productoUnidad.precioVenta * $scope.detalleFactura.producto.moneda.tasa) / $scope.factura.moneda.tasa) :
                $scope.detalleFactura.monto;

            ObtenerImpuestosProducto($scope.detalleFactura.producto);
            // cargarExistenciaLocalidad(producto.id);
        }



    }

    $scope.seleccionDeUnidad = function () {
        seleccionDeUnidad();
    }

    function seleccionDeUnidad() {
        $scope.detalleFactura.unidadId = $scope.esEdicion && $scope.detalleFactura.unidadId ? $scope.detalleFactura.unidadId :
            ($scope.detalleFactura.productoUnidad ? $scope.detalleFactura.productoUnidad.unidadId : null);
        $scope.detalleFactura.unidad = $scope.esEdicion && $scope.detalleFactura.unidad ? $scope.detalleFactura.unidad :
            ($scope.detalleFactura.productoUnidad ? $scope.detalleFactura.productoUnidad.unidad : null);

        $scope.detalleFactura.monto = $scope.detalleFactura.productoUnidad ? (($scope.detalleFactura.productoUnidad.precioVenta * $scope.detalleFactura.producto.moneda.tasa) / $scope.factura.moneda.tasa) :
            $scope.detalleFactura.monto;

        $scope.detalleFactura.montoDescuento = $scope.detalleFactura.cantidad *
            ($scope.detalleFactura.porcientoDescuento * $scope.detalleFactura.monto) / 100;
        $scope.detalleFactura.montoItbis = obtenerMontoImpuestos($scope.detalleFactura.producto);
        $scope.detalleFactura.montoTotal = $scope.detalleFactura.cantidad *
            ($scope.detalleFactura.monto - ($scope.detalleFactura.montoDescuento / $scope.detalleFactura.cantidad)) + $scope.detalleFactura.montoItbis;

    }


    function cargarImpuestos() {
        showLoading();
        apiService.get("/api/impuesto", null).then(function (result) {
            hideLoading();
            $scope.impuestos = result.data.data;
            let defaultImpuesto = $scope.impuestos.filter(t => t.porciento == 0)[0];
            $scope.defaultImpuesto = (!$scope.detalleFactura || ($scope.detalleFactura && !$scope.detalleFactura.impuesto)) ? defaultImpuesto : $scope.detalleFactura.impuesto;
            $scope.detalleFactura.impuesto = $scope.defaultImpuesto;
            $scope.detalleFactura.impuestoId = $scope.defaultImpuesto.id;


        }, submitDetalleFacturaError);
    }

    $scope.esEdicion = modalFunctions.DetalleActual.producto ? true : false;

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarDetalleFactura = function () {
        let aGuardar = {};
        angular.copy($scope.detalleFactura, aGuardar);
        let error = $scope.frmDetalleFacturaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            $scope.puedeFacturar = false;
            return;
        }

        $scope.puedeFacturar = true;
        aGuardar.montoDescuento = aGuardar.cantidad *
            (aGuardar.porcientoDescuento * aGuardar.monto) / 100;
        aGuardar.montoGrabado = (aGuardar.monto - (aGuardar.montoDescuento / aGuardar.cantidad)) * aGuardar.cantidad;
        aGuardar.montoItbis = (aGuardar.montoItbis - aGuardar.montoDescuento) * aGuardar.cantidad;
        aGuardar.montoTotal = aGuardar.montoGrabado + aGuardar.montoItbis - aGuardar.montoDescuento;
        aGuardar.montoDop = (aGuardar.montoTotal) * aGuardar.moneda.tasa;
        aGuardar.costo = aGuardar.producto.esServicio ? aGuardar.producto.costo : (aGuardar.productoUnidad.precioCompra);
        aGuardar.costo = aGuardar.costo * aGuardar.cantidad;
        if ($scope.esEdicion) {
            ActualizarDetallePorProducto(aGuardar);
        }
        else {
            $scope.factura.detallesFactura.push(aGuardar);
        }

        actualizarMontosFactura();

        limpiar();
        $scope.$broadcast('angucomplete-alt:clearInput');

        if ($scope.esEdicion) {

            $scope.closeDialog();
        }


    }

    function actualizarMontosFactura() {
        $scope.factura.montoTotal = $scope.factura.detallesFactura.sum("montoTotal");
        $scope.factura.montoGrabado = $scope.factura.detallesFactura.sum("montoGrabado");
        $scope.factura.montoItbis = $scope.factura.detallesFactura.sum("montoItbis");
        $scope.factura.montoDescuento = $scope.factura.detallesFactura.sum("montoDescuento");
        $scope.factura.costo = $scope.factura.detallesFactura.sum("costo");

        if ($scope.factura.montoRecibido != null && $scope.factura.montoRecibido != undefined) {
            $scope.factura.montoPagado = $scope.factura.montoRecibido;
            $scope.factura.montoDevuelto = $scope.factura.montoRecibido - $scope.factura.montoTotal;
            $scope.factura.montoAdeudado = $scope.factura.montoTotal - $scope.factura.montoPagado;
            if ($scope.factura.montoRecibido >= ($scope.factura.montoTotal)) {
                $scope.factura.montoAdeudado = 0;
                $scope.factura.montoPagado = $scope.factura.montoTotal;

            }
        }

    }


    function limpiar() {
        $scope.detalleFactura = {
            id: 0,
            productoId: 0,
            activo: true, 
            unidadId: null,
            unidad: null,
            cantidad: 1,
            costo: 0,
            monto: 0,
            producto: null,
            moneda: $scope.factura.moneda,
            montoItbis: 0,
            montoTotal: 0,
            facturaId: $scope.factura.id,
            impuesto: $scope.defaultImpuesto,
            impuestoId: $scope.defaultImpuesto.id,
            montoGrabado: 0,
            comentario: '',
            montoDop: 0,
            montoDescuento: 0,
            porcientoDescuento: $scope.porcientoDescuentoGral,
            esGratis: false

        };
    }

    function ActualizarDetallePorProducto(nuevoDetalle) {
        $scope.factura.detallesFactura.splice($scope.currentIndex, 1);
        $scope.factura.detallesFactura.push(nuevoDetalle);
    }
  

    $scope.selectedProducto = async function (newValue, oldValue) {

        if (newValue) {
            let constanteConversion = 1;
            const producto = newValue.originalObject;
            $scope.monedaProducto = producto.moneda;

            $scope.detalleFactura.producto = producto;
            if ($scope.factura.moneda.esMonedaLocal && !$scope.monedaProducto.esMonedaLocal) {
                constanteConversion = $scope.monedaProducto.tasa * $scope.factura.moneda.tasa;
            }
            else if (!$scope.factura.moneda.esMonedaLocal && $scope.monedaProducto.esMonedaLocal) {
                constanteConversion = $scope.monedaProducto.tasa / $scope.factura.moneda.tasa;
            }
            else if (!$scope.factura.moneda.esMonedaLocal && !$scope.monedaProducto.esMonedaLocal) {
                constanteConversion = !$scope.factura.moneda * (1 / $scope.monedaProducto.tasa);
            }
                      
            $scope.preciosProducto = [{ id: 1, monto: (producto.precio * constanteConversion) }, { id: 2, monto: (producto.precio2 * constanteConversion) }, { id: 3, monto: (producto.precio3 * constanteConversion) }];

                ObtenerImpuestosProducto(producto);
            
           

            //$scope.detalleFactura.monto = ((producto.precio * producto.moneda.tasa) / $scope.detalleFactura.moneda.tasa);
        }
        else {
            $scope.detalleFactura.producto = null;
            $scope.productoImpuestos = [];
            $scope.detalleFactura.impuestos = [];
            $scope.detalleFactura.productoId = 0;
            $scope.detalleFactura.monto = 0;
            $scope.detalleFactura.costo = 0;
            $scope.detalleFactura.unidadId = null;
            $scope.detalleFactura.unidad = null;
            $scope.detalleFactura.productoUnidad = null;
            $scope.detalleFactura.montoGrabado = 0;
            $scope.detalleFactura.montoDop = 0;
            $scope.detalleFactura.porcientoDescuento = $scope.porcientoDescuentoGral;
            $scope.detalleFactura.montoDescuento = $scope.detalleFactura.cantidad *
                ($scope.detalleFactura.porcientoDescuento * $scope.detalleFactura.monto) / 100;
        }


    }
    $scope.$watch('detalleFactura.esGratis', function (newVal, oldVal) {
        if (newVal) {
            if ($scope.detalleFactura.esGratis) {
                $scope.detalleFactura.monto = 0;

                $scope.detalleFactura.montoDescuento = 0;

                $scope.detalleFactura.montoItbis = 0;

                $scope.detalleFactura.montoTotal = 0;
            }
        }
        else {
            $scope.detalleFactura.monto = $scope.detalleFactura.monto <= 0 && $scope.detalleFactura.productoUnidad ?
                (($scope.detalleFactura.productoUnidad.precioVenta * $scope.detalleFactura.producto.moneda.tasa) / $scope.factura.moneda.tasa) :
                $scope.detalleFactura.monto;
            ObtenerImpuestosProducto($scope.detalleFactura.producto);
        }
    });

    $scope.$watch('detalleFactura.precioProductoSel', function (newVal, oldVal) {
        if (newVal) {
            $scope.detalleFactura.monto = newVal.monto;
            //    ObtenerImpuestosProducto($scope.detalleFactura.producto);
        }

    });

    function actualizarListaDePrecios() {
        $scope.preciosProducto = [{ id: 1, monto: $scope.detalleFactura.producto.precio / $scope.detalleFactura.productoUnidad.equivalencia },
        { id: 2, monto: $scope.detalleFactura.producto.precio2 / $scope.detalleFactura.productoUnidad.equivalencia },
        { id: 3, monto: $scope.detalleFactura.producto.precio3 / $scope.detalleFactura.productoUnidad.equivalencia }];
        if ($scope.detalleFactura.precioProductoSel) {
            $scope.detalleFactura.precioProductoSel.monto = $scope.detalleFactura.precioProductoSel.monto / $scope.detalleFactura.productoUnidad.equivalencia;
            $scope.detalleFactura.monto = $scope.detalleFactura.precioProductoSel.monto;
        }
    }

    $scope.$watchGroup(['detalleFactura.producto', 'detalleFactura.productoUnidad'], function (newVal, oldVal) {
        if (newVal[0] && !newVal[0].esServicio && newVal[1]) {
            actualizarListaDePrecios();
            if ($scope.detalleFactura.esGratis) {
                $scope.detalleFactura.monto = 0;

                $scope.detalleFactura.montoDescuento = 0;

                $scope.detalleFactura.montoItbis = 0;

                $scope.detalleFactura.montoTotal = 0;
            }
            else {
                ObtenerImpuestosProducto($scope.detalleFactura.producto, false);
            }


        }
        else if (newVal[0] && newVal[0].esServicio) {

            if ($scope.detalleFactura.esGratis) {
                $scope.detalleFactura.monto = 0;

                $scope.detalleFactura.montoDescuento = 0;

                $scope.detalleFactura.montoItbis = 0;

                $scope.detalleFactura.montoTotal = 0;
            }
            else {
                ObtenerImpuestosProducto($scope.detalleFactura.producto);
            }



        }
        else if (newVal[0] && !newVal[0].esServicio && $scope.esEdicion) {
            if ($scope.detalleFactura.esGratis) {
                $scope.detalleFactura.monto = 0;

                $scope.detalleFactura.montoDescuento = 0;

                $scope.detalleFactura.montoItbis = 0;

                $scope.detalleFactura.montoTotal = 0;
            }
            else {
                cargarProductoUnidadesConMonto($scope.detalleFactura.producto);

            }



        }
        else {
            $scope.detalleFactura.monto = 0;
            $scope.detalleFactura.montoItbis = 0;
            $scope.detalleFactura.montoTotal = 0;

            $scope.detalleFactura.porcientoDescuento = $scope.porcientoDescuentoGral;
            $scope.detalleFactura.montoDescuento = $scope.detalleFactura.cantidad *
                ($scope.detalleFactura.porcientoDescuento * $scope.detalleFactura.monto) / 100;
        }
    });



    $scope.accionesDeImpuestos = function () {
        $scope.detalleFactura.impuestoId = $scope.detalleFactura.impuesto.id;
        //$scope.detalleFactura.montoItbis = ($scope.detalleFactura.monto - ($scope.detalleFactura.montoDescuento / $scope.detalleFactura.cantidad))
        //    * $scope.detalleFactura.impuesto.porciento * $scope.detalleFactura.cantidad;
    }

    function submitDetalleFacturaSuccess(result) {
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            llenarTabla(DetalleFacturasUrl);
            $uibModalInstance.close();

        }



        $scope.showValidate = false;
    }

    function submitDetalleFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }



    function llenarTabla(urlAUsar) {
        let data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        let promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            $scope.resultadoEnPantalla.detalleFacturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalDetalleFacturas = result.data.pagedData.count;
        }, submitDetalleFacturaError);
        return promise;
    }

    $scope.$watchGroup(['detalleFactura.cantidad', 'detalleFactura.monto'], function (newVal, oldVal) {
        if ($scope.detalleFactura.esGratis) {
            $scope.detalleFactura.monto = 0;

            $scope.detalleFactura.montoDescuento = 0;

            $scope.detalleFactura.montoItbis = 0;

            $scope.detalleFactura.montoTotal = 0;
        }
        else {
            ObtenerImpuestosProducto($scope.detalleFactura.producto, false);
        }
    });


});