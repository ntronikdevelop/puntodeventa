﻿var MonedasFormController = angular.module('NavegationApp');
MonedasFormController.controller('FormularioMonedas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var MonedasUrl = '/api/moneda';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.moneda = { id: 0, nombre: '', activo: true,  tasa: 0, codigo: '' };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarMoneda($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarMoneda = function () {
        var error = $scope.frmMonedaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

      showLoading();

        var toPost = $scope.moneda;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/moneda", toPost, submitMonedaSuccess, submitMonedaError);
                break;
            default:
                apiService.put("/api/moneda", toPost, submitMonedaSuccess, submitMonedaError);
                break;
        }

    }

    function submitMonedaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(MonedasUrl);
        if (result.data.status >= 0 && $scope.id>0) {
           
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.moneda = {};

        }


        $scope.showValidate = false;
    }

    function submitMonedaError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
         bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }

    function VerificarMoneda(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/moneda", { id: dataId })
                      .then(llenarMoneda,submitMonedaError);
        }
    }


    function llenarMoneda(result) {
        hideLoading();
        $scope.moneda = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.moneda;
    }

    function llenarTabla(urlAUsar) {
         showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
         hideLoading();
            $scope.resultadoEnPantalla.monedas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalMonedas = result.data.pagedData.count;
        },submitMonedaError);
        return promise;
    }
});