﻿var pagoIndexController = angular.module('PagoIndexController', []);
pagoIndexController.controller('TablaDePagosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var pagosUrl = '/api/pago';
    $scope.resultadoEnPantalla = { pagos: [], totalPagos: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    $scope.filtroBusqueda = [
        { descripcion: 'No. pago', campo: 'Secuencia', tipoCampo: '0' }
    ]

    $scope.valorBusqueda = { campo: '', valor: '' };

    function llenarTabla(urlAUsar) {
       showLoading();
       var data = {
           page: $scope.resultadoEnPantalla.currentPage,
           size: 10,
           activo: !$scope.inactivos,
           campo: $scope.valorBusqueda.campo,
           tipoCampo: $scope.valorBusqueda.tipoCampo,
           valor: $scope.valorBusqueda.valor
       };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.pagos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalPagos = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }


    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'pagos')
        $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador.');
        }


    }

    $scope.EditarPago = function (secuencia) {
       showLoading();
       $scope.secuencia = secuencia;
       modalUtils.openInstance("/Modals/pagoForm?data=00046120", "FormularioPagos", { Secuencia: secuencia, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.imprimirRecibo = function (secuencia) {
        $window.open('/ImpresionEntrada/ImprimirRecibo?secuencia=' + secuencia, '_blank');
    }


    $scope.imprimirReciboPos = function (secuencia) {
        $window.open('/ImpresionEntrada/ImprimirReciboPOS?secuencia=' + secuencia, '_blank');
    }



    $scope.borrarPago = function (Id) {
        bootbox.confirm('Esta seguro de eliminar el registro del pago?',function(desicion){
            if (desicion) {
                showLoading();
                var url = pagosUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(pagosUrl);
                },ErrorResult);
            }
        })
  
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = pagosUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Pagos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Pagos")
            $scope.search();

    });




}]);