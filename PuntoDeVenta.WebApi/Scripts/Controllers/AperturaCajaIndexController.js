﻿var aperturaCajaIndexController = angular.module('aperturaCajaIndexController', []);
aperturaCajaIndexController.controller('TablaDeAperturaCajaController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var aperturaCajasUrl = '/api/aperturaCaja';
    $scope.resultadoEnPantalla = { aperturaCajas: [], totalAperturaCajas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.localidades = new Array();
    var apiService = new ApiService();
    $scope.inactivos = false;

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };

        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.aperturaCajas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalAperturaCajas = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.EditarAperturaCaja = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/aperturaCajaForm?data=00046120", "FormularioAperturaCajas",
            {
                Id: Id,
                modal: modalUtils,
                resultadoEnPantalla: $scope.resultadoEnPantalla,
                inactivos: $scope.inactivos
            });
    }

    $scope.CerrarCaja = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/CierreCajaForm?data=00046120", "FormularioCierreCajas",
            {
                Id: Id,
                modal: modalUtils,
                resultadoEnPantalla: $scope.resultadoEnPantalla,
                inactivos: $scope.inactivos
            });
    }

    $scope.BorrarAperturaCaja = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el registro de la apertura de Caja?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = aperturaCajasUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(aperturaCajasUrl);
                }, ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {

        var urlAUsar = aperturaCajasUrl;


        llenarTabla(urlAUsar);
    }


    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    $scope.init = function () {


        $q.all([
            $scope.search()
        ])
    };
    if ($scope.seleccion == "AperturaCierreCaja")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "AperturaCierreCaja")

            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'aperturacierrecaja')
            $scope.search();

    });



    //$scope.$watch('resultadoEnPantalla.aperturaCajas', function (newValue, oldValue) {
    //    if (newValue && newValue.length > 0 && $scope.localidades.length > 0)
    //        for (var i = 0; i < $scope.resultadoEnPantalla.aperturaCajas.length; i++) {
    //            $scope.resultadoEnPantalla.aperturaCajas[i].localidad = $scope.localidades.filter(x => x.id == $scope.resultadoEnPantalla.aperturaCajas[i].localidadId)[0];
    //        }

    //});



}]);