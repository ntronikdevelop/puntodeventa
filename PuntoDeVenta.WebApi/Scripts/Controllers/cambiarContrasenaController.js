﻿var userPasswordController = angular.module('NavegationApp');
userPasswordController.controller('cambiarContrasena', function ($scope, $filter, $q, ApiService, modalFunctions) {
    hideLoading();
    var clientesUrl = '/api/usuarios';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.usuarioId = modalFunctions.usuarioId;
   $scope.id = modalFunctions.Id;
   $scope.usuario = { id: $scope.usuarioId, contrasena:'' };
  
   $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
   
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.cambiarContrasena = function () {
        var error = $scope.frmUserPasswordForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
       showLoading();

       apiService.get(clientesUrl + "/CambiarPassword/" + $scope.usuario.id + "/" + $scope.usuario.contrasena, null).then(submitPasswordSuccess, submitPasswordError);
               
    }




    function submitPasswordSuccess(result) {
        hideLoading();
         bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            llenarTabla(clientesUrl);
            $uibModalInstance.close();
            
        }
           
       
        
        $scope.showValidate = false;
    }

    function submitPasswordError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }





  

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };


        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.clientes = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalClientes = result.data.pagedData.count;
        }, submitPasswordError);
        return promise;
    }
});