﻿var navegationController = angular.module('NavegationModule', []);
navegationController.controller('MenuNavegacion', ['$scope', '$filter', "$q", function ($scope, $filter, $q) {


    $scope.estadoDeSecciones = {
        estadoPagos: false,
        estadoFacturas: false,
        estadoClientes: false,
        estadoProductos: false,
        estadoAlmacen: false,
        estadoReportes: false,
        estadoImpuestos: false,
        estadoMonedas: false
    };






    $scope.HabilitarSeccion = function (seccionNombre) {
        ActivarSeccion(seccionNombre);
    }



    function ActivarSeccion(seccionNombre) {

        for (var seccion in $scope.estadoDeSecciones) {
            var nombre = 'estado' + seccionNombre;
            if (seccion == nombre) {
                $scope.estadoDeSecciones[seccion] = true;
            }
            else {
                $scope.estadoDeSecciones[seccion] = false;
            }
           
        }
    }


}])