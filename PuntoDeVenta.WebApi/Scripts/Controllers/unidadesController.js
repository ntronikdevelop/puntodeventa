﻿var UnidadFormController = angular.module('NavegationApp');
UnidadFormController.controller('FormularioUnidades', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var unidadsUrl = '/api/Unidades';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
   $scope.id = modalFunctions.Id;
   $scope.unidad = { id: 0, nombre: '', activo: true};

   $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
   VerificarUnidad($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarUnidad = function () {
        var error = $scope.frmUnidadForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
       showLoading();

      
        var toPost = $scope.unidad;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/Unidades", toPost, submitUnidadSuccess, submitUnidadError);
                break;
            default:
                apiService.put("/api/Unidades", toPost, submitUnidadSuccess, submitUnidadError);
                break;
        }
       
    }



    function submitUnidadSuccess(result) {
        hideLoading();
         bootbox.alert(result.data.mensaje);
         llenarTabla(unidadsUrl);
        if (result.data.status >= 0 && $scope.id>0) {
            
            $uibModalInstance.close();
            
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.unidad = {};

        }
       
        
        $scope.showValidate = false;
    }

    function submitUnidadError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarUnidad(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/Unidades", { id: dataId })
                      .then(llenarUnidad,submitUnidadError);
        }
    }



    function llenarUnidad(result) {
        hideLoading();
        $scope.unidad = result.data!=undefined && result.data!=null ?result.data.data[0]:$scope.unidad;
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };


        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.unidades = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalUnidades = result.data.pagedData.count;
        },submitUnidadError);
        return promise;
    }
});