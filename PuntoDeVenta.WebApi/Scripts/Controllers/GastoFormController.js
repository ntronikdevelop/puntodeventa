﻿var gastosFormController = angular.module('NavegationApp');
gastosFormController.controller('gastoFormController', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();

    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var gastosUrl = '/api/gasto';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.busquedaCompleja = modalFunctions.busquedaCompleja;
    $scope.id = modalFunctions.Id;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.gasto = {
        id: 0,
        suplidorId: 0,
        activo: true,
        monedaId: 0,
        referenciaGasto: '',
        suplidor: null,
        moneda: null,
        montoPagado: 0,
        montoDevuelto: 0,
        montoTotal: 0,
        montoItbis: 0,
        montoGrabado: 0,
        estado: '',
        montoDado: 0,
        montoAdeudado: 0,
        NCF:0
    };
    cargarMonedas();
    cargarLocalidades();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    verificarGasto($scope.id);
    $scope.closeDialogGasto = function () {
        $uibModalInstance.close();
    }

    $scope.mostrarDetalles = true;

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.gasto.moneda = $scope.monedas.filter(t=> t.id == $scope.gasto.monedaId)[0];

        }, submitgastoError);
    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.gasto.localidad = $scope.localidades.filter(t => t.id == $scope.gasto.localidadId)[0];

        }, submitgastoError);
    }


    $scope.agregarImpuesto = function (impuesto,monto) {
        $scope.gasto.impuestos = $scope.gasto.impuestos ? $scope.gasto.impuestos : [];
        if (impuesto && monto && monto>0) {
            const index = $scope.gasto.impuestos.findIndex(x => x.impuesto.id == impuesto.id);
            if (index < 0)
                $scope.gasto.impuestos.push({
                    id: 0,
                    impuestoId: impuesto.id,
                    montoImpuesto: monto,
                    impuesto: impuesto
                });
            else {
                $scope.gasto.impuestos[index].montoImpuesto = monto;
            }
        }
        let montoImpuestoTotal = $scope.gasto.impuestos.sum("montoImpuesto");
        $scope.gasto.montoItbis = montoImpuestoTotal;

    }

    $scope.anularImpuesto = function (index) {
        $scope.gasto.impuestos = $scope.gasto.impuestos ? $scope.gasto.impuestos : [];
        $scope.gasto.impuestos.splice(index, 1);
        let montoImpuestoTotal = $scope.gasto.impuestos.sum("montoImpuesto");
        $scope.gasto.montoItbis = montoImpuestoTotal;
    }

    $scope.guardarGasto = function () {
        var error = $scope.frmGastoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        if ($scope.gasto.montoPagado > ($scope.gasto.montoTotal)) {
            bootbox.alert("El monto pagado no debe de exceder el monto total del gasto.");
            return;
        }

        showLoading();
        var toPost = $scope.gasto;
        toPost.fechaEmision = $scope.fechaSeleccionada;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/gasto", toPost, submitgastoSuccess, submitgastoError);
                break;
            default:
                apiService.put("/api/gasto", toPost, submitgastoSuccess, submitgastoError);
                break;
        }

    }

   

    $scope.selectedSuplidor = function (newValue, oldValue) {

        if (newValue) {
            $scope.gasto.suplidor = newValue.originalObject;
            $scope.gasto.suplidorId = newValue.originalObject.id;
        }
        else {
            $scope.gasto.suplidor = null;
            $scope.gasto.suplidorId =0;
        }


    }




    function submitgastoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(gastosUrl + '/BusquedaCompleja');
       
        if (result.data.status >= 0 && $scope.id > 0) {
            $window.open('/ImpresionEntrada/ImprimirGasto?id=' + result.data.status, '_blank');
            $uibModalInstance.close();
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $window.open('/ImpresionEntrada/ImprimirGasto?id=' + result.data.status, '_blank');
            $scope.gasto = {};
            $scope.fechaSeleccionada = null;
            $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
        }


        $scope.showValidate = false;
    }

    function submitgastoError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function verificarGasto(dataId) {
        if (dataId > 0) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/gasto", { id: dataId })
                      .then(llenarGasto, submitgastoError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarGasto(result) {
        hideLoading();
        $scope.gasto = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.gasto;
        $scope.selectedSuplidor({ originalObject: $scope.gasto.suplidor }, null);
        $scope.fechaSeleccionada = new Date($scope.gasto.fechaEmision);
    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.gastos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalgastos = result.data.pagedData.count;
        }, submitgastoError);
        return promise;

    }

    $scope.$watch('gasto.montoDado', function (newValue, oldValue) {
        $scope.gasto.montoPagado = $scope.gasto.montoDado;
        $scope.gasto.montoDevuelto = $scope.gasto.montoDado - $scope.gasto.montoTotal;
        $scope.gasto.montoAdeudado = $scope.gasto.montoTotal - $scope.gasto.montoPagado;
        if ($scope.gasto.montoRecibido >= ($scope.gasto.montoTotal)) {
            $scope.gasto.montoAdeudado = 0;
            $scope.gasto.montoPagado = $scope.gasto.montoTotal;

        }


    });

    $scope.$watch('gasto.montoTotal', function (newValue, oldValue) {
        $scope.gasto.montoPagado = $scope.gasto.montoDado;
        $scope.gasto.montoDevuelto = $scope.gasto.montoDado - $scope.gasto.montoTotal;
        $scope.gasto.montoAdeudado = $scope.gasto.montoTotal - $scope.gasto.montoPagado;
        if ($scope.gasto.montoDado >= ($scope.gasto.montoTotal)) {
            $scope.gasto.montoAdeudado = 0;
            $scope.gasto.montoPagado = $scope.gasto.montoTotal;

        }


    });



    $scope.$watch('gasto.moneda', function (newValue, oldValue) {
        let ultimaFechaActualizacion = newValue != undefined ? newValue.fechaModificacion : null;
        let notUpdated = newValue != undefined ? newValue.tasaActualizada : null;
        if (ultimaFechaActualizacion != null && !newValue.esMonedaLocal == true)
            $scope.verificarFechaDeActualizacionDeMoneda(ultimaFechaActualizacion, notUpdated);
    });



    $scope.$watchGroup(['gasto.montoGrabado', 'gasto.montoItbis'], function (newVal, oldVal) {
        $scope.gasto.montoTotal = $scope.gasto.montoGrabado + $scope.gasto.montoItbis;
    });


});