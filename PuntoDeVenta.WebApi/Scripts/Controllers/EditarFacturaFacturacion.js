﻿'use-strict'
var app = angular.module('FacturacionEditarDetalle');
app.controller('DetalleFacturaController', ['$scope', '$rootScope', '$location', '$q', 'ApiService',
    'ModalUtils', 'FileUploader','$timeout', function ($scope, $rootScope, $location, $q, ApiService, ModalUtils, FileUploader,$timeout) {
        var apiService = new ApiService();
        var modalUtils = new ModalUtils();
        $scope.tempKey = Math.random().toString();
        $scope.esVista = false;
        $scope.OcultarSiFacturaTieneCliente = function () {
            if ($scope.factura.idCliente != null)
                return true;
            return false;
        }
        CargarEmpresas();
       
        $scope.factura = { id: $scope.Id, tempKey: $scope.tempKey }
        $scope.EsSeleccion = false;
        $scope.EsAdjuntado = false;
        $scope.facturaDetalle = { InstruccionFacturacionId: $scope.Id };
        $scope.estadoInstruccionObj = null;
        $scope.categoriaFacturaObj = null;
        $scope.tipoServicios = [];
        $scope.EstadosFactura = [];
        $scope.CategoriasFactura = [];
        $scope.EsSeleccion = false;
        $scope.EsAdjuntado = false;
        $scope.servicios = [];
        $scope.monedas = [];

        //-----------------------
        $scope.Clientes = [];
        $scope.VerificarFecha = function (fecha) {
            if (fecha == undefined || fecha == "" || fecha == null)
                return true;
            return false;
        }

        function CargarEmpresas() {
            var data = { Exclude: 'TiposServiciofacturacion,Departamentoes' };
            var promise = apiService.get('/Facturacion/Empresa/ObtenerTodos', data);
            promise.then(function (result) {
                $scope.Empresas = result.data.DeserializedData;
            });
            return promise;
        }

        $scope.ClienteActualPorCampos = function (campo, valor) {
            var promise = apiService.get('/Facturacion/cliente/ObtenerPorNombre', { campo: campo, valor: valor });
            promise.then(function (result) {
                $scope.clienteInicial = result.data.listResult.DeserializedData[0];

            });
            return promise;
        }

        $scope.BusquedaCliente = function (nombreCliente, list) {
            var call = $.ajax({
                url: GetBaseUrl() + '/Facturacion/cliente/ObtenerPorNombre',
                data: { campo: 'Nombre', valor: "'" + nombreCliente },
                async: false
            });
            list = JSON.parse(call.responseText).listResult.DeserializedData;
            var filter = list.filter(t => t.Nombre.toLowerCase().includes(nombreCliente.toLowerCase())).slice(0, 5);
            return filter;

        }

        function ObtenerDepartamentos() {
            var promise = apiService.get('/Facturacion/Departamento/ObtenerPorEmpresa', { campo: "EmpresaId", valor: $scope.factura.empresaId });
            promise.then(function (result) {
                $scope.Departamentos = result.data.listResult.DeserializedData;
                $scope.factura.departamento = $scope.Departamentos.filter(t=> t.Id == $scope.factura.departamentoId)[0];
            });
            return promise;
        }

        $scope.ObtenerDepartamentosPorEmpresa = function (valor) {
            var promise = apiService.get('/Facturacion/Departamento/ObtenerPorEmpresa', { campo: "EmpresaId", valor: valor });
            promise.then(function (result) {
                $scope.Departamentos = result.data.listResult.DeserializedData;
                $scope.factura.departamento = $scope.Departamentos.filter(t=> t.Id == $scope.factura.departamentoId)[0];
            });
            return promise;
        }










        //------------------
        $scope.HuboCambiosDeAdjuntos = false;
        $scope.tipoServicioSeleccionado = null;
        $scope.ArchivosSeleccionados = new Array();
        $rootScope.cargarFacturaBackend = cargarFactura;
        $scope.Init = function () {

            $q.all([
                cargarFactura(),
                VerifyId(),
              
            ]);

        }

        function VerifyId() {
            var currentId = $("#Id").val();
            if (currentId != 0 && currentId != undefined && currentId != null && currentId != "0") {
            }
        }



        $scope.tipoServicioChange = function () {
            if ($scope.tipoServicioSeleccionado != null)
                cargarServicios($scope.tipoServicioSeleccionado.id == undefined ? $scope.tipoServicioSeleccionado.Id : $scope.tipoServicioSeleccionado.id);
        }

        $scope.CambiarEstadoFactura = function () {
            var confirmacion = confirm("Esta seguro de cambiar el estado de la factura?");
            if (confirmacion) {
                var toUpdate = { Id: $scope.factura.id, EstadoInstruccionFacturacionId: $scope.estadoInstruccionObj.Id };
                var editUrl = '/Facturacion/Facturacion/ModificarEstadoInstruccion/';
                apiService.post(editUrl, toUpdate, SuccessAction, ErrorAction);

            }
        };

        $scope.CambiarCategoriaFactura = function () {
            var confirmacion = confirm("Esta seguro de cambiar la categoria de la factura?");
            if (confirmacion) {
                var toUpdate = { Id: $scope.factura.id, CategoriaFacturacionId: $scope.categoriaFacturaObj.Id };
                var editUrl = '/Facturacion/Facturacion/ModificarCategoriaInstruccion/';
                apiService.post(editUrl, toUpdate, SuccessAction, ErrorAction);

            }
        };

        function cargarFactura() {
            LlamarLoading();
            var data = {
                Id: $scope.factura.id, fields: _selection

            };

            apiService.post("/Facturacion/Facturacion/ObtenerFacturasRaw/", data,llenarFactura,ErrorAction);
               // .then(llenarFactura);
          //  apiService.post($scope.factura.id == 0 ? addUrl : editUrl, $scope.instruccionFactura, SuccessAction, ErrorAction);
        }

        function llenarFactura(result) {
            angular.copy(result.data.data[0], $scope.factura)
            $scope.factura.fechaTerminoServicio = $scope.Date($scope.factura.fechaTerminoServicio);
            $scope.facturaDetalle.TipoServicioFacturacionId = $scope.factura.tipoServicioFacturacionId;
            $scope.factura.clienteAplicaItbis = $scope.factura.cliente ? $scope.factura.cliente.aplicaItbis : false;
            $scope.factura.clienteNombre = $scope.factura.cliente ? $scope.factura.cliente.nombre : "";
            $scope.facturaDetalle.clienteId = $scope.factura.cliente != null ? $scope.factura.cliente.id : $scope.facturaDetalle.clienteId;
            $scope.factura.empresa = $scope.Empresas != undefined ? $scope.Empresas.filter(t=> t.Id == $scope.factura.empresaId)[0] : null;

            cargarCategoriasFacturas();
            $scope.HuboCambiosDeAdjuntos = false;
            ObtenerDepartamentos();

            if ($scope.factura.facturado)
                $scope.esVista = true;

            cargarBls();
            QuitarLoading();
        }



        function cargarBls() {
            $scope.clienteBls = [];
            $timeout(function () {
                $("#blsFactura").trigger("chosen:updated");
            }, 0);
            if (!$scope.factura.idCliente || !$scope.factura.codigoFile)
                return;

            var data = {
                "ClienteEmbarcador.Id": $scope.factura.idCliente,
                "ManifiestoFile": $scope.factura.codigoFile
            }
            var url = "/FACTURACION/bls/ObtenerTodos";

            apiService.get(url, data).then(function (result) {
                $scope.clienteBls = result.data.listResult;

                for (var i = 0; i < $scope.factura.bls.length; i++)
                    $scope.factura.bls[i] = $scope.clienteBls.filter(a=> a.Id == $scope.factura.bls[i].id)[0];

                $timeout(function () {

                    $("#blsFactura").trigger("chosen:updated");
                }, 0);

            });
        }

        $scope.ActivarEsAdjuntado = function () {
            $scope.EsAdjuntado = true;
            $scope.EsSeleccion = false;
            $scope.HuboCambiosDeAdjuntos = true;
        }
        $scope.ActivarEsSeleccion = function () {
            $scope.EsAdjuntado = false;
            $scope.EsSeleccion = true;
            $scope.HuboCambiosDeAdjuntos = true;
        }

        $scope.DescargarArchivo = function (adjuntoObj) {
            var url = '/Facturacion/Facturacion/Download/';
            var promise = apiService.get(url, adjuntoObj);
            promise.then(function (result) {
                var pepe = result;

            });
        }

        function cargarCategoriasFacturas() {
            var data = {};
            apiService.get("/facturacion/CategoriasFactura/obtenertodos", data)
                .then(llenarCategoriasFactura);
        }

        function llenarTipoServicios(result) {
            $scope.tipoServicios = eval(result.data.listResult.DeserializedData);
        }

        function llenarEstadosInstruccion(result) {
            $scope.estadosInstruccion = eval(result.data.listResult.DeserializedData);
            $scope.estadoInstruccionObj = $scope.estadosInstruccion != undefined ? $scope.estadosInstruccion.filter(t=> t.Id == $scope.factura.estadoInstruccionFacturacionId)[0] : null;

        }
        function llenarCategoriasFactura(result) {
            $scope.CategoriasFactura = eval(result.data.listResult.DeserializedData);
            $scope.factura.categoriasFacturas = $scope.CategoriasFactura != undefined && $scope.CategoriasFactura.length > 0 ? $scope.CategoriasFactura.filter(t=> t.Id == $scope.factura.categoriaFacturacionId)[0] : $scope.factura.categoriasFacturas;

        }

        function cargarServicios(Id) {
            var data = {};
            apiService.addFilter(data, { TipoServicioId: Id, OrderBy:'Nombre' });
            apiService.get("/facturacion/Servicio/obtenertodos", data)
                .then(llenarServicios);
        }

        function llenarServicios(result) {
            $scope.servicios = result.data.listResult.DeserializedData;
        }


        function cargarMonedas() {
            var data = {};
            apiService.get("/facturacion/moneda/obtenertodos", data)
                .then(llenarMonedas);
        }

        function llenarMonedas(result) {
            $scope.monedas = result.data.listResult.DeserializedData;
        }

        function sum(source, expresion) {
            var total = 0;
            for (var i = 0; i < source.length; i++) {
                total += expresion(source[i]);
            }
            return total;
        }

        $scope.Date = function (value) {
            if (value != undefined && value != null) {
                var fechaString = value != null ? value.replace("/Date(", "").replace(")/", "") : null;
                var fecha = new Date(Number(fechaString) || fechaString);
                return fecha;
            }
            return null;
        }

        $scope.total = function (source, func) {

            return sum($scope.factura.detalleControlContenedores, func);
        }

        $scope.anularDetalle = function (detalle) {
            LlamarLoading();
            var data = {
                "Clase": $scope.factura.tipoServicioFacturacionClase,
                "DetalleJson": "{'Id':" + detalle.id + ", 'InstruccionFacturacionId':" + detalle.instruccionFacturacionId + "}"
            };

            apiService.post("/facturacion/detalles/borrar", data).then(function () {
                QuitarLoading();
                cargarFactura();
            });

        }

        function SuccessAction(response) {
     
            QuitarLoading();
            alert(response.data.Message);
            $scope.uploader.clearQueue();
            $scope.ArchivosSeleccionados = new Array();
            $("#FileSelected").val('');
            cargarFactura();
        }
        function ErrorAction(response) {
            QuitarLoading();
            alert('Hubo un error al procesar su solicitud. Favor intentar mas tarde o contactar administrador.');
            $scope.uploader.clearQueue();
            location.reload();

        }

        $scope.fileCounter = 0;

        $scope.uploader = new FileUploader({
            url: GetBaseUrl() + '/Facturacion/Facturacion/UploadFile',
            formData: [{ tempId: $scope.factura.tempKey }],
            removeAfterUpload: true,
            onCompleteAll: function () {
                LlamarLoading();
                $scope.uploader.clearQueue();
                $scope.EjecutarPost();
            },
            onErrorItem: function (item, response, status, headers) {
                QuitarLoading();
                $scope.uploader.clearQueue();
                $scope.HuboCambiosDeAdjuntos = false;
                $scope.EsAdjuntado = true;
                $scope.EsSeleccion = true;
                $("#FileSelected").val('');
                alert('Hubo un error al procesar su solicitud. Favor intentar mas tarde o contactar administrador.');


            },
            onSuccessItem: function (item, response, status, headers) {

            },
            onAfterAddingFile: function (item) {
                $scope.ActivarEsSeleccion();
                var fileObj = { NombreArchivo: item.file.name };
                $scope.ArchivosSeleccionados.push(fileObj);
            },
            filters: [
                {
                    name: 'TamanoArchivosValido', fn: function (item) {
                        var maxSize = 2097152;
                        var validation = item.size <= maxSize;
                        if (!validation)
                            alert('El  maximo del archivo no debe de pasar los ' + (maxSize / 1048576).toString() + 'MB');
                        return validation;
                    }
                },
                  {
                      name: 'ExtensionesArchivoValidas', fn: function (item) {
                          validExtensions = ['pdf', 'xlsx', 'xls', 'txt', 'doc', 'docx', 'jpg'];
                          item.name = item.name.toLowerCase();
                          var nameArr = item.name.split('.');
                          var extension = nameArr[nameArr.length - 1];
                          if (validExtensions.indexOf(extension) >= 0)
                              return true;
                          alert('El archivo no es válido.');
                          return false;
                      }
                  }
            ]
        });

        $scope.ReiniciarSeleccion = function () {
            $scope.ArchivosSeleccionados = new Array();
            $scope.uploader.queue = new Array();
            $scope.HuboCambiosDeAdjuntos = false;
            $scope.EsAdjuntado = true;
            $scope.EsSeleccion = true;
        }

        function GetBaseUrl() {
            return "http://" + window.location.hostname + ":" + location.port;
        }

        $scope.GuardarInstruccionFacturacion = function () {
            LlamarLoading();
            $scope.factura.tempKey = $scope.tempKey;
            if ($scope.factura.idCliente!=null)
                $scope.factura.idCliente = $scope.factura.idCliente == null ? $scope.clienteObj != undefined && $scope.clienteObj.originalObject!=undefined ? $scope.clienteObj.originalObject.Id : $scope.clienteInicial.Id : $scope.factura.idCliente;
            $scope.factura.tipoServicioFacturacionId = $scope.factura.tipoServicioFacturacionId == undefined || $scope.factura.tipoServicioFacturacionId <= 0 ? $scope.factura.tipoServicioFacturacion.Id : $scope.factura.tipoServicioFacturacionId;
            $scope.factura.departamentoId = $scope.factura.departamento.Id;
            $scope.factura.categoriaFacturacionId = $scope.factura.categoriasFacturas.Id;
            $scope.factura.empresaId = $scope.factura.empresa.Id;
            var instruccionFactura = $scope.factura;

            instruccionFactura.fechaEnvioInstruccionACobros = ArreglarFecha(instruccionFactura.fechaEnvioInstruccionACobros);
            instruccionFactura.fechaRecepcionDocumentacion = ArreglarFecha(instruccionFactura.fechaRecepcionDocumentacion);
            instruccionFactura.fechaSolicitudServicio = ArreglarFecha(instruccionFactura.fechaSolicitudServicio);
            instruccionFactura.servicio = null;
            instruccionFactura.tipoServicioFacturacion = null;
            instruccionFactura = LimpiarDetallesFactura(instruccionFactura);
            instruccionFactura = EstablecerInstruccionComoFacturadoPorCategoria(instruccionFactura);
            $scope.instruccionFactura = instruccionFactura;
            $scope.uploader.formData = [{ tempId: $scope.TempKey }];

            if ($scope.uploader.queue.length > 0) {
                $scope.uploader.uploadAll();
            }
            else {
                $scope.EjecutarPost();
            }



            return true;
        }

        function EstablecerInstruccionComoFacturadoPorCategoria(instruccion) {
            if ($scope.factura.categoriasFacturas.crearComoFactura != undefined && $scope.factura.categoriasFacturas.crearComoFactura != null && $scope.factura.categoriasFacturas.crearComoFactura) {
                instruccion.facturado = true;
                instruccion.estadoInstruccionFacturacionId = $scope.estadosFactura.filter(function (estado) {
                    return estado.nombreEstado.toLowerCase() == 'facturado'
                }).id;
            }
            return instruccion;
        }

        function LimpiarDetallesFactura(factura) {
            for (var campo in factura) {
                if (campo.toString().toLowerCase().indexOf('detalle') >= 0) {
                    factura[campo] = null;
                }
            }
            return factura;
        }


        $scope.LimpiarCampos = function (tipoFormObj) {
            $scope.factura.concepto = tipoFormObj != undefined ? tipoFormObj.Descripcion : "";
        }


        $scope.EjecutarPost = function () {
            var addUrl = '/Facturacion/Facturacion/Crear/';
            var editUrl = '/Facturacion/Facturacion/Editar/';
            apiService.post($scope.factura.id == 0 ? addUrl : editUrl, $scope.instruccionFactura, SuccessAction, ErrorAction);
        }

        function ArreglarFecha(date) {
            var newDate = date != null ? (date.toString().indexOf('Date') >= 0 ?
           $scope.ConvertToDate(date) : $scope.ConvertToJDate(date)) : null;
            return newDate;
        }
        $scope.ConvertToDate = function (csDate) {

            if (csDate != null && csDate != undefined && csDate != "") {
                var dateNumeric = parseInt(csDate.substring(6, csDate.length - 2));
                return new Date(dateNumeric);

            }
            return "";
        }

        $scope.ConvertToJDate = function (csDate) {
            if (csDate instanceof Date) {
                if (csDate != null && csDate != undefined && csDate != "") {
                    return ((csDate.getMonth() + 1) + '/' + csDate.getDate() + '/' + csDate.getFullYear());
                }
            }

            return "";
        }


        $scope.AgregarPago = function () {

            modalUtils.openInstance("/facturacion/facturacion/AddPago?data=0008846119", "InstruccionPagoController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils }, "md");
        }

        $scope.AgregarDetalleContenedores = function () {

            modalUtils.openInstance("/facturacion/DetalleControlContenedore/crear?data=00046119", "DetalleControlContenedoresController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }

        $scope.AgregarDetalleFile = function () {

            modalUtils.openInstance("/facturacion/DetalleFile/crear?data=00046119", "DetalleFileController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }

        $scope.AgregarDetalleTaller = function () {

            modalUtils.openInstance("/facturacion/DetalleTaller/crear?data=00046119", "DetalleTallerController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }


        $scope.AgregarDetalleGeneral = function () {

            modalUtils.openInstance("/facturacion/DetalleGeneral/crear?data=00046119", "DetalleGeneralController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }

        $scope.AgregarDetalleTrafico = function () {

            modalUtils.openInstance("/facturacion/DetalleGeneral/crear?data=00046119", "DetalleGeneralController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }


        $scope.AgregarDetalleContabilidad = function () {

            modalUtils.openInstance("/facturacion/DetalleContabilidad/crear?data=00046119", "DetalleContabilidadController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }

        $scope.AgregarDetalleManzanillo = function () {

            modalUtils.openInstance("/facturacion/DetalleManzanillo/crear?data=00046119", "DetalleManzanilloController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }

        $scope.AgregarDetalleLiquidaciones = function () {

            modalUtils.openInstance("/facturacion/DetalleLiquidaciones/crear?data=00046119", "DetalleLiquidacionesController", { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils });
        }


        $scope.editarDetalle = function (factura,detalleId) {
            factura.tipoServicioFacturacionClase
            var detalleNombre = factura.tipoServicioFacturacionClase;
            var controller = detalleNombre + "Controller";
            var listaDetalles = factura[firstLetterToLower(detalleNombre)];
            var result = $.grep(listaDetalles, function (e) { return e.id == detalleId; })[0];
            delete result["valorItbis"]
            modalUtils.openInstance("/facturacion/" + detalleNombre + "/crear?data=00046119", "" + controller, { factura: $scope.factura, cargarFactura: cargarFactura, modal: modalUtils, facturaDetalle: result, esEdicion:true });


        }

        function firstLetterToLower(string) {
            return string.charAt(0).toLowerCase() + string.slice(1);
        }

        $scope.generarFactura = function () {
            
            var tieneDetalles = VerificarSiInstruccionTieneDetalles($scope.factura);
            if (tieneDetalles) {
                LlamarLoading();
                var data = {};
                apiService.post("/facturacion/Facturacion/FacturarInstruccion/" + $scope.factura.id, data, function () {
                    cargarFactura();
                    console.log($location);
                }, function (response) {
                    QuitarLoading();
                    alert(eval(response).data.StatusDescription);
                });
            }
            else {
                alert('No se puede facturar una instrucción sin detalles. Favor crear por lo menos uno.');
            }


        }

        function VerificarSiInstruccionTieneDetalles(instruccion) {
            var tieneDetalles = false;
            for (var propiedad in instruccion) {
                if (propiedad.toString().toLowerCase().indexOf('detalle') >= 0) {
                    if (instruccion[propiedad] != null && instruccion[propiedad] != undefined && instruccion[propiedad].constructor === Array) {
                        if (instruccion[propiedad].length > 0)
                            tieneDetalles = true;
                    }
                }
            }
            return tieneDetalles;
        }

        $scope.BorrarAdjunto = function (id) {

            if (confirm('Esta seguro de eliminar esta archivo?')) {
                LlamarLoading();
                var promise = apiService.get('/Facturacion/Facturacion/BorrarAdjunto/' + id, null);
                promise.then(function (result) {
                    QuitarLoading();
                    alert(result.data.Message);
                    if (result.data.Status >= 0) {
                        cargarFactura();
                    }

                });

                return promise;
            }
            return null;
        }

        $scope.Init();
    }]);
