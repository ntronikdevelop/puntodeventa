﻿var FacturaIndexController = angular.module('FacturaIndexController', []);
FacturaIndexController.controller('TablaDeFacturasController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var FacturasUrl = '/api/factura';
    $scope.resultadoEnPantalla = { facturas: [], totalFacturas: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    $scope.filtroBusqueda = [
        { descripcion: 'No. Factura', campo: 'NumeroFactura', tipoCampo: '0' },
        { descripcion: 'Cliente', campo: 'ClienteId', tipoCampo: '1' },
        { descripcion: 'Vendedor', campo: 'VendedorId', tipoCampo: '1' },
        { descripcion: 'Fecha', campo: 'FechaFacturado', tipoCampo: '2' },
        { descripcion: 'Estado de factura', campo: 'Estado', tipoCampo: '3' }
    ]

    $scope.estados = [{ descripcion: 'TODOS', valorEstado: "" }, { descripcion: 'FACTURADO', valorEstado: "f" }, { descripcion: 'COTIZADO', valorEstado: "c" },{ descripcion: 'ANULADO', valorEstado: "a" }, { descripcion: 'PAGADO', valorEstado: "p" }]

    $scope.valorBusqueda = { campo: '', valor: '' };

    $scope.busquedaCompleja = {
        page: $scope.resultadoEnPantalla.currentPage,
        size: 10,
        activo: !$scope.inactivos,
        filtros: new Array()
    };

    $scope.verificarBusquedaCompleja = function () {
        return ($scope.busquedaCompleja && $scope.busquedaCompleja.filtros && $scope.busquedaCompleja.filtros.length > 0);
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        $scope.busquedaCompleja.page = $scope.resultadoEnPantalla.currentPage;
        var data = $scope.busquedaCompleja;
        var promise = apiService.post(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }
    $scope.ObtenerSoloFecha = function (date) {
        var day = date.getDate();
        var monthIndex = date.getMonth()+1;
        var year = date.getFullYear();
        return (day + '-' + monthIndex + '-' + year);
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.valorBusqueda.valor = newValue.originalObject.id;
            $scope.valorBusqueda.descripcionValor = newValue.originalObject.nombre;
            $scope.agregarFiltro();
            $scope.search();
        }
        else {
            $scope.valorBusqueda.valor = null;
            $scope.valorBusqueda.descripcionValor = null;
        }

       
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'facturas')
            $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.EditarFactura = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/FacturaForm?data=00046120", "FormularioFacturas", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, busquedaCompleja:$scope.busquedaCompleja });
    }

    $scope.EditarFacturaAntigua = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/FacturaAntiguaForm?data=00046120", "FormularioFacturasAntiguas", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, busquedaCompleja: $scope.busquedaCompleja });
    }

    $scope.BorrarFactura = function (Id) {
        bootbox.confirm('Esta seguro de eliminar el registro de la Factura?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = FacturasUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    $scope.search();
                }, ErrorResult);
            }
        })

    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = FacturasUrl;


        llenarTabla(urlAUsar + '/BusquedaCompleja');
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
    }

    $scope.agregarFiltro = function () {
        let aGuardar = {};
        angular.copy($scope.valorBusqueda, aGuardar);
        if ($scope.busquedaCompleja.filtros) {
            let index = $scope.busquedaCompleja.filtros.findIndex(x => x.campo == aGuardar.campo);
            if (index >= 0) {
                $scope.busquedaCompleja.filtros.splice(index, 1);
            }
            $scope.busquedaCompleja.filtros.push(aGuardar);
        }
        $scope.valorBusqueda.valor = '';
        $scope.$broadcast('angucomplete-alt:clearInput', 'cliente');
        return;
    }

    $scope.borrarFiltro = function (index) {
        $scope.busquedaCompleja.filtros.splice(index, 1);
    }

    $scope.init = function () {


        $q.all([
            $scope.search()
        ])
    };
    if ($scope.seleccion == "Facturas")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Facturas")
            $scope.search();

    });

    $scope.obtenerEstadoDeFactura = function (estatus) {
        let resultado = '';
        estatus = estatus.toLowerCase();

        switch (estatus) {
            case 'f':
                resultado = "FACTURADO";
                break;
            case 'c':
                resultado = "COTIZADO";
                break;
            case 'd':
                resultado = "CONVERTIDO";
                break;

            case 'a':
                resultado = "ANULADO";
                break;

            case 'p':
                resultado = "PAGADO";
                break;
            default:

                break;
        }

        return resultado;

    }


}]);