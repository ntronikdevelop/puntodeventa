﻿var PagosFormController = angular.module('NavegationApp');
PagosFormController.controller('FormularioPagos', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance, $window) {
    hideLoading();
    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var PagosUrl = '/api/pago';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;
    $scope.montoPagoAcc = new Array();
    $scope.id = modalFunctions.Id;
    // cargarMonedas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarPago($scope.id);
    $scope.closeDialogPago = function () {
        $uibModalInstance.close();
    }



    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;


        }, submitPagoError);
    }


    $scope.filtrarPorMoneda = function () {
        $scope.pago.monedaId = $scope.pago.moneda.id;
        $scope.pago.tasa = $scope.pago.moneda.tasa;
        buscarFacturasPendientesCliente($scope.pago.clienteId, $scope.pago.monedaId);
    }

    $scope.GuardarPago = function () {
        var error = $scope.frmPagoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        if ($scope.pago.montoPago < $scope.montoPagoAcc.sum('monto')) {
            bootbox.alert('Error al procesar el pago.  Favor verificar el monto pagado');
            return;
        }

        showLoading();
        let toPost = {};
        angular.copy($scope.pago, toPost);
        toPost.detallesPago = toPost.detallesPago.filter(d => d.seleccionado);
        switch ($scope.id) {
            case undefined:
                apiService.post("/api/Pago/aplicarPagos", toPost, submitPagoSuccess, submitPagoError);
                break;
            default:
                apiService.put("/api/Pago", toPost, submitPagoSuccess, submitPagoError);
                break;
        }

    }

    function cargarVendedores(zonaId) {
        showLoading();
        apiService.get("/api/vendedor/vendedorPorZona/" + zonaId, null).then(function (result) {
            hideLoading();
            $scope.vendedores = result.data.data;
            //  $scope.factura.vendedor = $scope.vendedores.filter(t => t.id == $scope.factura.vendedorId)[0];

        }, submitPagoError);
    }


    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.pago.cliente = newValue.originalObject;
            $scope.pago.moneda = newValue.originalObject.moneda;
            $scope.pago.monedaId = $scope.pago.moneda.id;
            $scope.pago.clienteId = newValue.originalObject.id;
            buscarFacturasPendientesCliente($scope.pago.clienteId, $scope.pago.monedaId);
            cargarVendedores(newValue.originalObject.zonaId);
        }
        else {
            $scope.pago.cliente = null;
            $scope.pago.clienteId = 0;
            $scope.pago.moneda = null;
            $scope.pago.monedaId = 0;
            $scope.pago.vendedor = null;
            $scope.pago.vendedorId = null;
            $scope.vendedores = [];
        }


    }

    function buscarFacturasPendientesCliente(clienteId, monedaId) {
        showLoading();
        apiService.post("/api/Factura/ReporteCxC", { clienteId: clienteId, monedaId: monedaId }).then(function (result) {
            hideLoading();
            $scope.pago.detallesPago = result.data.data[0].facturas;


        }, submitPagoError);
    }

    $scope.verificarMontoAPagar = function (detalle) {
        if (detalle.montoAPagar > detalle.montoAdeudado) {
            bootbox.alert('El monto a pagar no debe exceder el monto adeudado');
            detalle.montoAPagar = 0;
        }
        var index = $scope.montoPagoAcc.findIndex(x => x.noFactura == detalle.numeroFactura);
        if (index >= 0) {
            $scope.montoPagoAcc[index].monto = detalle.montoAPagar;
        }
    }

    $scope.AbonarFactura = function (detalle) {

        if (detalle.seleccionado) {

            if (($scope.pago.montoPago - $scope.montoPagoAcc.sum('monto')) >= detalle.montoAdeudado) {
                detalle.montoAPagar = detalle.montoAdeudado;

            }
            else if (($scope.pago.montoPago - $scope.montoPagoAcc.sum('monto')) >= 0) {
                //   angular.copy(($scope.pago.montoPago - $scope.montoPagoAcc.sum('monto')), detalle.montoAPagar);
                detalle.montoAPagar = ($scope.pago.montoPago - $scope.montoPagoAcc.sum('monto'));
            }
            else {
                detalle.montoAPagar = 0;

            }
            if (detalle.montoAPagar > 0)
                $scope.montoPagoAcc.push({ noFactura: detalle.numeroFactura, monto: detalle.montoAPagar });
            else
                detalle.seleccionado = false;
        }
        else {
            detalle.montoAPagar = 0;
            var index = $scope.montoPagoAcc.findIndex(x => x.noFactura == detalle.numeroFactura);
            if (index >= 0)
                $scope.montoPagoAcc.splice(index, 1);
        }

    }

    $scope.$watch('pago.montoPago', function (newValue, oldValue) {
        // angular.copy($scope.pago.montoPago, $scope.montoPagoAcc);
    });

    function submitPagoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            llenarTabla(PagosUrl);
            $window.open('/ImpresionEntrada/ImprimirRecibo?secuencia=' + result.data.codigo, '_blank');

            $uibModalInstance.close();

        }



        $scope.showValidate = false;
    }

    function submitPagoError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarPago(secuencia) {
        if (secuencia) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/Pago", { secuencia: secuencia })
                .then(llenarPago, submitPagoError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarPago(result) {
        hideLoading();

    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {


        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    //function llenarTabla(urlAUsar) {
    //    showLoading();
    //    var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
    //    var promise = apiService.get(urlAUsar, data);
    //    promise.then(function (result) {
    //        hideLoading();
    //        $scope.resultadoEnPantalla.pagos = result.data.pagedData.result;
    //        $scope.resultadoEnPantalla.totalPagos = result.data.pagedData.count;
    //    }, submitPagoError);
    //    return promise;
    //}

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos
        };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.pagos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalPagos = result.data.pagedData.count;
        }, submitPagoError);
        return promise;
    }





});