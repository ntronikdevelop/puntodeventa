﻿var localidadIndexController = angular.module('LocalidadIndexController', []);
localidadIndexController.controller('TablaDeLocalidadController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var localidadesUrl = '/api/localidad';
    $scope.resultadoEnPantalla = { localidades: [], totalLocalidades: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.localidades = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totallocalidades = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }

    $scope.EditarLocalidad = function (Id) {
      showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/LocalidadForm?data=00046120", "FormularioLocalidades", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.BorrarLocalidad = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el registro de la localidad?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = localidadesUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(localidadesUrl);
                },ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = localidadesUrl;


        llenarTabla(urlAUsar);
    }
    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Localidad")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Localidad")
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'localidad')
            $scope.search();

    });




}]);