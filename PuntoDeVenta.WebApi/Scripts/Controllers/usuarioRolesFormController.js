﻿var usuarioRolesFormController = angular.module('NavegationApp');
usuarioRolesFormController.controller('FormularioUsuarioRoles', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var usuarioRolesUrl = 'api/usuarios/';
    var apiService = new ApiService();
    hideLoading();
    $scope.userId = modalFunctions.UserId;
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    cargarRoles();
    cargarRolesDeUsuario($scope.userId);

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.agregarRoleAUsuario = function (roleId) {
        if (roleId && roleId > 0 && !$scope.usuarioRoles.filter(r => r.id === roleId)[0]) {

            showLoading();
            var toPost = { userId: $scope.userId, roleId: roleId };
            let url = `/api/roles/agregarRoleAUsuario`;
            apiService.post(url, toPost, submitusuarioRoleSuccess, submitusuarioRoleError);




        }
    }

    $scope.eliminarRoleDeUsuario = function (roleId) {
        if (roleId && roleId > 0) {

            showLoading();
            let url = `/api/roles/eliminarRoleDeUsuario/${$scope.userId}/${roleId}`;

            var promise = apiService.delete(url, null);
            promise.then(function (result) {
                hideLoading();
                cargarRolesDeUsuario($scope.userId);
                bootbox.alert(result.data.mensaje);
            }, submitusuarioRoleError);


        }
    }

    function cargarRolesDeUsuario(userId) {
        showLoading();
        apiService.get("/api/roles/obtenerRolesUsuario/" + userId, null).then(function (result) {
            hideLoading();
            $scope.usuarioRoles = result.data.data;

        }, submitusuarioRoleError);
    }

    function cargarRoles() {
        showLoading();
        apiService.get("/api/roles", null).then(function (result) {
            hideLoading();
            $scope.roles = result.data.data;

        }, submitusuarioRoleError);
    }

    function submitusuarioRoleSuccess(result) {
        hideLoading();
        if (result.data.status < 0)
            bootbox.alert(result.data.message);

        cargarRolesDeUsuario($scope.userId);
        $scope.showValidate = false;
    }

    $scope.selectedUsuario = function (newValue, oldValue) {

        if (newValue) {
            $scope.usuarioRole.usuario = newValue.originalObject;
            $scope.usuarioRole.usuarioId = newValue.originalObject.id;
        }
        else {
            $scope.usuarioRole.usuario = null;
            $scope.usuarioRole.usuarioId = null;
        }


    }

    function submitusuarioRoleError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }




    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.usuarioRoles = result.data.usuarioRoles;
            $scope.resultadoEnPantalla.totalusuarioRoles = result.data.totalusuarioRoles;
        }, submitusuarioRoleError);
        return promise;
    }
});