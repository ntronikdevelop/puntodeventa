﻿var productoIndexController = angular.module('ProductoIndexController', []);
productoIndexController.controller('TablaDeProductosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;

    $scope.filtroBusqueda = [
        { descripcion: 'Nombre', campo: 'Nombre', tipoCampo: '0' },
        { descripcion: 'Codigo', campo: 'CodigoBarra', tipoCampo: '0' }
    ]

    $scope.valorBusqueda = { campo: '', valor: '' };

    var modalUtils = new ModalUtils();
    var productosUrl = '/api/producto';
    $scope.resultadoEnPantalla = { productos: [], totalProductos: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: $scope.valorBusqueda.campo,
            tipoCampo: $scope.valorBusqueda.tipoCampo,
            valor: $scope.valorBusqueda.valor
        };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.productos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalProductos = result.data.pagedData.count;
           
        }, ErrorResult);
        return promise;
    }

    $scope.EditarProducto = function (Id) {
        showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/ProductoForm?data=00046120", "FormularioProductos", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos, valorBusqueda: $scope.valorBusqueda });
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'productos')
        $scope.search();

    });

    $scope.BorrarProducto = function (Id) {

       bootbox.confirm('Esta seguro de eliminar el producto?',function(desicion){
           if (desicion) {
               showLoading();
               var url = productosUrl + '?idABorrar=' + Id;
               var promise = apiService.delete(url, null);
               promise.then(function (result) {
                   hideLoading();
                   bootbox.alert(result.data.mensaje);
                   llenarTabla(productosUrl);
               }, ErrorResult);
           }
        }) 
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = productosUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Productos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Productos")
            $scope.search();

    });



}]);