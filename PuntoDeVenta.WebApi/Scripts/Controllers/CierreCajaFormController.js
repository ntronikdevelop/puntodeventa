﻿var aperturaCajasFormController = angular.module('NavegationApp');
aperturaCajasFormController.controller('FormularioCierreCajas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var aperturaCajasUrl = '/api/aperturaCaja';
    var apiService = new ApiService();
    $scope.montosApertura = [];
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.aperturaCaja = { id: 0, userId: '', activo: true, caja: null };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarAperturaCaja($scope.id);
    cargarCajas();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.obtenerInfoUsuario = function (userId) {
        showLoading();
        apiService.get("/api/usuarios/obtenerDatosAdicionales/" + userId, null).then(function (result) {
            hideLoading();
            let datosUsuario = result.data.data[0];
            if (datosUsuario) {

                $scope.aperturaCaja.caja = datosUsuario.caja;
                $scope.aperturaCaja.cajaId = datosUsuario.caja.id;
                $scope.aperturaCaja.nombreUsuario = datosUsuario.nombreYApellido;
                $scope.aperturaCaja.localidad = datosUsuario.localidad;
                $scope.aperturaCaja.localidadId = datosUsuario.localidad.id;
            }
            else
                bootbox.alert('Error al obtener informacion del usuario.');




        }, submitaperturaCajaError);
    }

    $scope.evaluarTiposCierre = function () {
        $scope.tiposCierre = [];
        for (var tipo in $scope.tipoPagos) {
            $scope.tiposCierre.push({ codigo: $scope.tipoPagos[tipo].codigo, nombre: $scope.tipoPagos[tipo].nombre, detalles: null, monto: 0 });
        }
    }

    $scope.GuardarAperturaCaja = function () {
        bootbox.confirm("Está seguro de cerrar la caja a este usuario?", function (result) {
            if (result) {
                var error = $scope.frmAperturaCajaForm.$error;
                if (error.required || error["autocomplete-required"]) {
                    $scope.showValidate = true;
                    return;
                }
                showLoading();
                if (!$scope.aperturaCaja.montoCierreTotal || $scope.aperturaCaja.montoCierreTotal <= 0) {
                    hideLoading();
                    bootbox.alert('La caja no se puede cerrar vacia');
                    return;
                }

                var toPost = $scope.aperturaCaja;
                toPost.detallesCierre = JSON.stringify($scope.tiposCierre);
                let totalCierre = 0;
                for (var i = 0; i < $scope.tiposCierre.length; i++) {
                    totalCierre += $scope.tiposCierre[i].monto;
                }
                toPost.montoCierreTotal = totalCierre;
                apiService.put("/api/aperturaCaja/cerrarCaja", toPost, submitaperturaCajaSuccess, submitaperturaCajaError);


            }
        })


    }


    $scope.$watch('tipoPagos', function (oldValue, newValue) {
        if ($scope.tipoPagos && $scope.tipoPagos.length > 0)
            $scope.evaluarTiposCierre();
    })

    $scope.$watch('aperturaCaja.montoCierreTotal', function (oldValue, newValue) {
        if ($scope.aperturaCaja.montoAperturaTotal && $scope.aperturaCaja.montoCierreTotal)
            $scope.aperturaCaja.diferenciaAperturaCierre = $scope.aperturaCaja.montoCierreTotal - $scope.aperturaCaja.montoAperturaTotal;
    })
    function cargarCajas() {
        showLoading();
        apiService.get("/api/caja", null).then(function (result) {
            hideLoading();
            $scope.cajas = result.data.data;
            $scope.aperturaCaja.caja = $scope.cajas.filter(t => t.id == $scope.aperturaCaja.cajaId)[0];

            if ($scope.cajas.length == 1) {
                $scope.aperturaCaja.caja = $scope.cajas[0];
                $scope.aperturaCaja.cajaId = $scope.aperturaCaja.caja.id;
            }


        }, submitaperturaCajaError);
    }

    $scope.calcularTotalEfectivo = function () {
        let montosSeleccionados = $scope.montosApertura.filter(x => x.cantidad && x.cantidad > 0);
        let total = 0;
        for (var i = 0; i < montosSeleccionados.length; i++) {
            let monto = montosSeleccionados[i].cantidad * montosSeleccionados[i].monto;
            total += monto;
        }
        $scope.montoCierre = total;
    }


    function submitaperturaCajaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(aperturaCajasUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.aperturaCaja = {};

        }



        $scope.showValidate = false;
    }

    function submitaperturaCajaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarAperturaCaja(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/aperturaCaja", { id: dataId })
                .then(llenaraperturaCaja, submitaperturaCajaError);
        }
    }


    function llenaraperturaCaja(result) {
        hideLoading();
        $scope.aperturaCaja = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.aperturaCaja;
        if ($scope.aperturaCaja.userId)
            apiService.get("/api/aperturaCaja/ObtenerMontoTotalPagos", { userId: $scope.aperturaCaja.userId, aperturaCajaId: $scope.aperturaCaja.id })
                .then(function (result) {
                    $scope.aperturaCaja.montoTotalPagos = (result.data != undefined && result.data != null) ? result.data.data[0].montoTotalPagos : 0;
                }, submitaperturaCajaError);

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
        // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.aperturaCajas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalAperturaCajas = result.data.pagedData.count;
        }, submitaperturaCajaError);
        return promise;
    }

    $scope.evaluarCierreTipo = function (tipoPago) {
        let index = $scope.tiposCierre.findIndex(t => t.codigo == tipoPago.codigo);
        if (index >= 0) {
            if (tipoPago.codigo == 'EFE') {
                $scope.tiposCierre[index].detalles = $scope.montosApertura;
                $scope.calcularTotalEfectivo();
            }

            $scope.tiposCierre[index].monto = $scope.montoCierre;
        }

        let total = 0;
        for (var i = 0; i < $scope.tiposCierre.length; i++) {
            let monto = $scope.tiposCierre[i].monto ? $scope.tiposCierre[i].monto : 0;
            total += monto;
        }
        $scope.aperturaCaja.montoCierreTotal = total;

    }
});