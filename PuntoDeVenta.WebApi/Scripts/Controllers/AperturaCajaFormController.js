﻿var aperturaCajasFormController = angular.module('NavegationApp');
aperturaCajasFormController.controller('FormularioAperturaCajas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var aperturaCajasUrl = '/api/aperturaCaja';
    var apiService = new ApiService();
    $scope.montosApertura = [];
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.aperturaCaja = { id: 0, userId: '', activo: true, caja: null };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarAperturaCaja($scope.id);
    cargarCajas();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.obtenerInfoUsuario = function (userId) {
        showLoading();
        apiService.get("/api/usuarios/obtenerDatosAdicionales/" + userId, null).then(function (result) {
            hideLoading();
            let datosUsuario = result.data.data[0];
            if (datosUsuario) {

                $scope.aperturaCaja.caja = datosUsuario.caja;
                $scope.aperturaCaja.cajaId = datosUsuario.caja.id;
                $scope.aperturaCaja.nombreUsuario = datosUsuario.nombreYApellido;
                $scope.aperturaCaja.localidad = datosUsuario.localidad;
                $scope.aperturaCaja.localidadId = datosUsuario.localidad.id;
            }
            else
                bootbox.alert('Error al obtener informacion del usuario.');




        }, submitaperturaCajaError);
    }

    $scope.GuardarAperturaCaja = function () {
        var error = $scope.frmAperturaCajaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        if (!$scope.aperturaCaja.montoAperturaTotal || $scope.aperturaCaja.montoAperturaTotal <= 0) {
            bootbox.alert('La caja no se puede aperturar vacia');
            hideLoading();
            return;
        }

        var toPost = $scope.aperturaCaja;
        toPost.detallesApertura = JSON.stringify($scope.montosApertura);
        switch ($scope.id) {
            case 0:
                apiService.post("/api/aperturaCaja", toPost, submitaperturaCajaSuccess, submitaperturaCajaError);
                break;
            default:
                apiService.put("/api/aperturaCaja", toPost, submitaperturaCajaSuccess, submitaperturaCajaError);
                break;
        }

    }

    $scope.restaurarMontosApertura = function () {
        if ($scope.id > 0) {
            let montosCreados = JSON.parse($scope.aperturaCaja.detallesApertura);
            if (Array.isArray(montosCreados)) {
                for (var monto in montosCreados) {
                    let index = $scope.montosApertura.findIndex(m => m.id == montosCreados[monto].id);
                    if (index >= 0)
                        $scope.montosApertura[index].cantidad = montosCreados[monto].cantidad;
                }
            }
        }

    }

    function cargarCajas() {
        showLoading();
        apiService.get("/api/caja", null).then(function (result) {
            hideLoading();
            $scope.cajas = result.data.data;
            $scope.aperturaCaja.caja = $scope.cajas.filter(t => t.id == $scope.aperturaCaja.cajaId)[0];

            if ($scope.cajas.length == 1) {
                $scope.aperturaCaja.caja = $scope.cajas[0];
                $scope.aperturaCaja.cajaId = $scope.aperturaCaja.caja.id;
            }


        }, submitaperturaCajaError);
    }

    $scope.calcularTotal = function () {
        let montosSeleccionados = $scope.montosApertura.filter(x => x.cantidad && x.cantidad > 0);
        let total = 0;
        for (var i = 0; i < montosSeleccionados.length; i++) {
            let monto = montosSeleccionados[i].cantidad * montosSeleccionados[i].monto;
            total += monto;
        }
        $scope.aperturaCaja.montoAperturaTotal = total;
    }


    function submitaperturaCajaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(aperturaCajasUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.aperturaCaja = {};

        }



        $scope.showValidate = false;
    }

    function submitaperturaCajaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarAperturaCaja(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/aperturaCaja", { id: dataId })
                .then(llenaraperturaCaja, submitaperturaCajaError);
        }
    }


    function llenaraperturaCaja(result) {
        hideLoading();
        $scope.aperturaCaja = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.aperturaCaja;
        $scope.restaurarMontosApertura();

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
        // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.aperturaCajas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalAperturaCajas = result.data.pagedData.count;
        }, submitaperturaCajaError);
        return promise;
    }
});