﻿var rolesFormController = angular.module('NavegationApp');
rolesFormController.controller('FormularioRoles', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var rolesUrl = '/api/roles';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.role = { id: 0, nombre: '', activo: true };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarRole($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.guardarRole = function () {
        var error = $scope.frmRolesForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
     showLoading();


        var toPost = $scope.role;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/roles", toPost, submitroleSuccess, submitroleError);
                break;
            default:
                apiService.put("/api/roles", toPost, submitroleSuccess, submitroleError);
                break;
        }

    }

    function submitroleSuccess(result) {
        hideLoading();
         bootbox.alert(result.data.mensaje);
            llenarTabla(rolesUrl);
        if (result.data.status >= 0 && $scope.id>0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.role = {};

        }


        $scope.showValidate = false;
    }

    function submitroleError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
            else
         bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }

    function VerificarRole(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/roles", { id: dataId })
                      .then(llenarrole,submitroleError);
        }
    }


    function llenarrole(result) {
        hideLoading();
        $scope.role = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.role;
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.roles = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalRoles = result.data.pagedData.count;
        },submitroleError);
        return promise;
    }
});