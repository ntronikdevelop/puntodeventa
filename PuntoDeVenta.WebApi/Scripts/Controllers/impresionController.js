﻿var impresionController = angular.module('impresionApp');
impresionController.controller('ImpresionFacturas', function ($scope) {
    hideLoading();
    $scope.esCreacion = true;
    var modalUtils = modalFunctions.modal;
    var facturasUrl = '/api/factura';
    var apiService = new ApiService();
    $scope.inactivos = modalFunctions.inactivos;

    $scope.id = modalFunctions.Id;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.factura = { id: 0, clienteId: 0, activo: true, monedaId: 0, numeroFactura: '', cliente: null, moneda: null, montoPagado: 0, montoTotal: 0, montoItbis: 0, montoGrabado: 0, estado: '', detallesFactura: [] };
    cargarMonedas();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarFactura($scope.id);
    $scope.closeDialogFactura = function () {
        $uibModalInstance.close();
    }


    function cargarMonedas() {
       showLoading();
       apiService.get("/api/moneda", null).then(function (result) {
           hideLoading();
            $scope.monedas = result.data.data;
            $scope.factura.moneda = $scope.monedas.filter(t=> t.id == $scope.factura.monedaId)[0];

        },submitFacturaError);
    }




    $scope.GuardarFactura = function () {
        var error = $scope.frmFacturaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
       
        if ($scope.factura.montoPagado > ($scope.factura.montoTotal + 1)) {
            return;
        }
       showLoading();
        var toPost = $scope.factura;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/factura", toPost, submitFacturaSuccess, submitFacturaError);
                break;
            default:
                apiService.put("/api/factura", toPost, submitFacturaSuccess, submitFacturaError);
                break;
        }

    }

    $scope.EditarDetalle = function (detalle) {
        if (detalle == null)
            detalle = {
                id: 0,
                productoId: 0,
                activo: true,
                cantidad: 0,
                monto: 0,
                producto: null,
                moneda: $scope.factura.moneda,
                montoItbis: 0,
                montoTotal: 0,
                facturaId: $scope.factura.id,
                impuestoId: 0

            };
        showLoading();
        $scope.detalleActual = detalle;
        modalUtils.openInstance("/Modals/DetalleFacturaForm?data=00046120", "FormularioDetalleFacturas", { DetalleActual: detalle, Factura: $scope.factura, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla });
    }

    $scope.selectedCliente = function (newValue, oldValue) {

        if (newValue) {
            $scope.factura.cliente = newValue.originalObject;
            $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? newValue.originalObject.moneda : $scope.factura.moneda;
            $scope.factura.monedaId = $scope.factura.moneda.id;
            $scope.aplicaItbis = $scope.factura.cliente.aplicarItbis;
            $scope.factura.clienteId = newValue.originalObject.id;
        }
        else {
            $scope.factura.cliente = null;
            $scope.factura.clienteId = 0;
            $scope.factura.moneda = null;
            $scope.factura.monedaId = 0;
        }


    }
    $scope.AnularDetalle = function (detalle) {

        bootbox.confirm("Esta seguro de eliminar el producto de la factura?", function (desicion) {
            var index = $scope.factura.detallesFactura.indexOf(detalle);
            if (index >= 0) {
                $scope.factura.montoGrabado -= detalle.monto;
                $scope.factura.montoItbis -= detalle.montoItbis;
                $scope.factura.montoTotal -= detalle.montoTotal;
                $scope.factura.detallesFactura.splice(index, 1);
            }
        })

    }



    function submitFacturaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        if (result.data.status >= 0) {
            llenarTabla(facturasUrl);
            $uibModalInstance.close();

        }



        $scope.showValidate = false;
    }

    function submitFacturaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarFactura(dataId) {
        if (dataId > 0) {
            showLoading();
            $scope.esCreacion = false;
            apiService.get("/api/factura", { id: dataId })
                      .then(llenarFactura, submitFacturaError);
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }


    function llenarFactura(result) {
        hideLoading();
        $scope.factura = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.factura;
        $scope.selectedCliente({ originalObject: $scope.factura.cliente }, null);
        $scope.factura.moneda = $scope.factura.moneda == null || $scope.factura.moneda == undefined ? $scope.factura.cliente.moneda : $scope.factura.moneda;
    }

    $scope.verificarFechaDeActualizacionDeMoneda = function (fechaString, updated) {
      

        if (!updated && $scope.esCreacion)
            bootbox.alert("La ultima fecha de actualización de la tasa para la moneda seleccionada fue en " + fechaString.split('T')[0]);

    }


    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.facturas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalFacturas = result.data.pagedData.count;
        },submitFacturaError);
        return promise;
    }

    $scope.$watch('factura.montoPagado', function (newValue, oldValue) {
        if ($scope.factura.montoTotal > 0 && (($scope.factura.montoPagado) > ($scope.factura.montoTotal + 1)))
            bootbox.alert('El monto pagado no debe de ser mayor que el monto a facturar');

    });

    $scope.$watch('factura.montoTotal', function (newValue, oldValue) {
        let monto = newValue;

        $scope.factura.montoPagado = monto;

    });

    $scope.$watch('factura.moneda', function (newValue, oldValue) {
        let ultimaFechaActualizacion = newValue != undefined ? newValue.fechaModificacion : null;
        let notUpdated = newValue != undefined ? newValue.tasaActualizada : null;
        if (ultimaFechaActualizacion != null && !newValue.esMonedaLocal== true)
            $scope.verificarFechaDeActualizacionDeMoneda(ultimaFechaActualizacion,notUpdated);
    });
});