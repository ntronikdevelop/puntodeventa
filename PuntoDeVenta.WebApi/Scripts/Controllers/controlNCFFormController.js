﻿var controlNCFsFormController = angular.module('NavegationApp');
controlNCFsFormController.controller('FormularioControlNCFs', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var controlNCFsUrl = '/api/controlNCF';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.controlNCF = { id: 0, nombre: '', activo: true, localidad: '' };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarcontrolNCF($scope.id);
  //  cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarControlNCF = function () {
        var error = $scope.frmControlNCFForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.controlNCF;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/controlNCF", toPost, submitcontrolNCFSuccess, submitcontrolNCFError);
                break;
            default:
                apiService.put("/api/controlNCF", toPost, submitcontrolNCFSuccess, submitcontrolNCFError);
                break;
        }

    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.controlNCF.localidad = $scope.localidades.filter(t=> t.id == $scope.controlNCF.localidadId)[0];

        }, submitcontrolNCFError);
    }



    function submitcontrolNCFSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(controlNCFsUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.controlNCF = {};

        }



        $scope.showValidate = false;
    }

    function submitcontrolNCFError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarcontrolNCF(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/controlNCF", { id: dataId })
                      .then(llenarcontrolNCF, submitcontrolNCFError);
        }
    }


    function llenarcontrolNCF(result) {
        hideLoading();
        $scope.controlNCF = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.controlNCF;

    }

 
    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
        // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.controlNCFs = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalcontrolNCFs = result.data.pagedData.count;
        }, submitcontrolNCFError);
        return promise;
    }

});