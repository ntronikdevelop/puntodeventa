﻿var permisosFormController = angular.module('NavegationApp');
permisosFormController.controller('FormularioPermisos', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var permisosUrl = '/permisos';
    var apiService = new ApiService();
    hideLoading();
    $scope.id = modalFunctions.Id;
    $scope.permisotemp = { seccion: null, operaciones: new Array() };
    $scope.permiso = { usuario: null, permisos: new Array() };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    cargarSecciones();
   
    VerificarPermiso($scope.id);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarPermiso = function () {
        var error = $scope.frmPermisoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = {};
        angular.copy($scope.permiso, toPost);

        apiService.post("/permisos/guardarpermisosusuario", toPost, submitPermisoSuccess, submitPermisoError);




    }

    $scope.convertirListaEnTexto = function (lista) {
        var texto = "";

        for (var i = 0; i < lista.length; i++) {
            texto += texto == "" ? lista[i] : "," + lista[i];
        }

        return texto;
    }

    $scope.EditarDetallePermiso = function () {
        var permisoAGuardar = {};
        angular.copy($scope.permisotemp, permisoAGuardar);
        var index = $scope.permiso.permisos.map(function (el) {
            return el.seccion.nombre;
        }).indexOf(permisoAGuardar.seccion.nombre);

        if (index >= 0) {
            $scope.permiso.permisos.splice(index, 1);
            $scope.permiso.permisos.push(permisoAGuardar);
        }

        else {
            $scope.permiso.permisos.push(permisoAGuardar);
        }
    }

    $scope.AnularDetalle = function (detalle) {
        var index = $scope.permiso.permisos.map(function (el) {
            return el.seccion.nombre;
        }).indexOf(detalle.seccion.nombre);
        $scope.permiso.permisos.splice(index, 1);
    }

    $scope.agregarOperacion = function (operacion) {
        var index = $scope.permisotemp.operaciones.indexOf(operacion);
        if (index >= 0) {
            $scope.permisotemp.operaciones.splice(index, 1);
        }
        else {
            $scope.permisotemp.operaciones.push(operacion);
        }
    }


    function cargarSecciones() {
        showLoading();
        apiService.get("/api/secciones", null).then(function (result) {
            hideLoading();
            $scope.secciones = result.data.data;
            $scope.permisotemp.seccion = $scope.secciones.filter(t=> t.nombre == $scope.permisotemp.nombreSeccion)[0];

        },submitPermisoError);
    }

  

    $scope.cargarOperaciones = function (seccionId) {
        showLoading();
        let url = "/api/operaciones/ObtenerOperacionesSeccion";
        apiService.get(url, { seccionId: seccionId }).then(function (result) {
            hideLoading();
            $scope.operaciones = result.data.data;

        }, submitPermisoError);
    }

    function submitPermisoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.message);
        var urlAUsar = permisosUrl + '/ObtenerDatosDeUsuarios';
        llenarTabla(urlAUsar);
        if (result.data.status >= 0 && $scope.id>0) {

            $uibModalInstance.close();

        }

        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.permiso = {};

        }

        $scope.showValidate = false;
    }

    $scope.selectedUsuario = function (newValue, oldValue) {

        if (newValue) {
            $scope.permiso.usuario = newValue.originalObject;
            $scope.permiso.usuarioId = newValue.originalObject.id;
        }
        else {
            $scope.permiso.usuario = null;
            $scope.permiso.usuarioId = null;
        }


    }

    function submitPermisoError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }

    function VerificarPermiso(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/permiso", { id: dataId })
                      .then(llenarPermiso,submitPermisoError);
        }
    }


    function llenarPermiso(result) {
        hideLoading();
        $scope.permiso = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.permiso;
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.permisos = result.data.permisos;
            $scope.resultadoEnPantalla.totalPermisos = result.data.totalPermisos;
        },submitPermisoError);
        return promise;
    }
});