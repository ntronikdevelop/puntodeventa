﻿var productosFormController = angular.module('NavegationApp');
productosFormController.controller('FormularioProductos', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var productosUrl = '/api/producto';
    var modalUtils = modalFunctions.modal;
    var apiService = new ApiService();
    $scope.valorBusqueda = modalFunctions.valorBusqueda;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.id = modalFunctions.Id;
    $scope.producto = {
        id: 0,
        nombre: '',
        activo: true,
        descripcion: '',
        precio: 0,
        codigoBarra: '',
        esServicio: false,
        tarifasPorClientes: new Array(),
        suplidor: null,
        suplidorId: 0,
        monedaId: 0,
        impuestos:[],
        productoUnidades: new Array()
    };
    cargarMonedas();
    cargarSuplidores();
    cargarUnidades();
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarProducto($scope.id);

    $scope.closeDialog = function () {
        $uibModalInstance.close();
        
        $('.modal').remove();
        $('.modal-backdrop').remove();
    }

    $scope.agregarImpuesto = function (impuesto) {
        $scope.producto.impuestos = $scope.producto.impuestos ? $scope.producto.impuestos : [];
        if (impuesto) {
            const index = $scope.producto.impuestos.findIndex(x => x.impuesto.id == impuesto.id);
            if (index < 0)
                $scope.producto.impuestos.push({
                    id: 0,
                    impuestoId: impuesto.id,
                    productoId: $scope.producto.id,
                    impuesto: impuesto
                });
        }
      

    }

    $scope.anularImpuesto = function (index) {
        $scope.producto.impuestos = $scope.producto.impuestos ? $scope.producto.impuestos : [];
        $scope.producto.impuestos.splice(index, 1);
    }

    $scope.GuardarProducto = function () {
       
        var error = $scope.frmProductoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = $scope.producto;
        if ((!toPost.unidadesProductosBase || toPost.unidadesProductosBase.length <= 0) && toPost.esCompuesto)
        {
            alert('Favor ingresar por lo menos un producto base.');
            hideLoading();
            return;
        }
        toPost.productoCostos = toPost.productoCostos ? toPost.productoCostos : new Array();

        if (!toPost.esServicio)
            toPost.detalles = JSON.stringify(toPost.productoCostos);



        if (toPost.esServicio && (toPost.productoUnidades && toPost.productoUnidades.length>0))
            toPost.productoUnidades = new Array();

        switch ($scope.id) {
            case 0:
                apiService.post("/api/producto", toPost, submitProductoSuccess, submitProductoError);
                break;
            default:
                apiService.put("/api/producto", toPost, submitProductoSuccess, submitProductoError);
                break;
        }

    }

    $scope.checkKeyEvent = function (event) {
        if(event.keyCode===13)
        event.preventDefault();
    }

    function cargarMonedas() {
        showLoading();
        apiService.get("/api/moneda", null).then(function (result) {
            hideLoading();
            $scope.monedas = result.data.data;
            $scope.producto.moneda = $scope.monedas.filter(t=> t.id == $scope.producto.monedaId)[0];

        });
    }

    $scope.editarUnidad = function (index) {
        let productoUnidad = null;
        if (index < 0) {
            productoUnidad = {
                id: 0,
                producto: $scope.producto,
                productoId: $scope.producto.id,
                unidad: null,
                unidadId: 0,
                equivalencia: 0,
                esPrincipal: false

            };
        }
        else {
            productoUnidad = $scope.producto.productoUnidades[index];
        }

        showLoading();
        $scope.productoUnidadActual = productoUnidad;
        modalUtils.openInstance("/Modals/ProductoUnidadForm?data=00046120", "FormularioProductoUnidad", { Index: index, ProductoUnidadActual: productoUnidad, Producto: $scope.producto, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla });
    }

    $scope.editarCosto = function (index) {
        let productoCosto = null;
        if (index < 0) {
            productoCosto = {
                id: 0,
                producto: $scope.producto,
                productoId: $scope.producto.id

            };
        }
        else {
            productoCosto = $scope.producto.productoCostos[index];
        }

        showLoading();
        $scope.productoCostoActual = productoCosto;
        modalUtils.openInstance("/Modals/ProductoCostoForm?data=00046120", "FormularioProductoCosto", { Index: index, ProductoCostoActual: productoCosto, Producto: $scope.producto, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla });
    }

    $scope.editarProductoBase = function (index) {
        let base = null;
        if (index < 0) {
            base = {

                productoCompuesto: $scope.producto,
                productoCompuestoId: $scope.producto.id

            };
        }
        else {
            base = $scope.producto.unidadesProductosBase[index];
            base.productoCompuesto = $scope.producto;
        }

        showLoading();
        $scope.base = base;
        modalUtils.openInstance("/Modals/BasesProductoForm?data=00046120", "FormulariounidadesProductosBase", { Index: index, baseActual: base, Producto: $scope.producto, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla });
    }

    function cargarSuplidores() {
        showLoading();
        apiService.get("/api/suplidor", null).then(function (result) {
            hideLoading();
            $scope.suplidores = result.data.data;
            $scope.producto.suplidor = $scope.suplidores.filter(t=> t.id == $scope.producto.suplidorId)[0];


        }, submitProductoError);
    }

    function cargarUnidades() {
        showLoading();
        apiService.get("/api/unidades", null).then(function (result) {
            hideLoading();
            $scope.unidades = result.data.data;
            for (var i = 0; i < $scope.producto.productoUnidades.length; i++) {
                $scope.producto.productoUnidades[i].unidad = $scope.unidades.filter(t=>t.id == $scope.producto.productoUnidades[i].unidadId)[0];
            }



        }, submitProductoError);
    }



    function submitProductoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);
        llenarTabla(productosUrl);
        if (result.data.status >= 0 && $scope.id>0) {
         
            $uibModalInstance.close();
            return null;
        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.producto = {};

        }

        VerificarProducto($scope.id);



        $scope.showValidate = false;
    }

    function submitProductoError(result) {
        hideLoading();
        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');
        }
        else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }

    }

    function VerificarProducto(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/producto", { id: dataId })
                      .then(llenarProducto, submitProductoError);
        }
    }

    $scope.AnularUnidad = function (index) {
        $scope.producto.productoUnidades.splice(index, 1);
    }

    $scope.AnularCosto = function (index) {
        $scope.producto.productoCostos.splice(index, 1);
        $scope.producto.costo = ($scope.ItemSumm($scope.producto.productoCostos, 'costo') / $scope.producto.productoCostos.length);
        $scope.producto.costo = !$scope.producto.costo ? 0 : $scope.producto.costo;
    }

    $scope.AnularProductoBase = function (index) {
        $scope.producto.unidadesProductosBase.splice(index, 1);
        $scope.producto.costo = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'costoTotal');
        $scope.producto.precio = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'precioTotal');
    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.verificarExistenciaUnidadPrincipal = function () {
        let validacion =$scope.producto.productoUnidades? $scope.producto.productoUnidades.filter(p=>p.esPrincipal)[0] != null:false;
        return validacion;
    }
    function llenarProducto(result) {
        hideLoading();

        $scope.producto = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.producto;
        if (!$scope.producto.productoUnidades)
            $scope.producto.productoUnidades = new Array();
        apiService.get(`/api/producto/ObtenerImpuestosDeProducto/${$scope.producto.id}`)
            .then(function (result2) {
                $scope.producto.impuestos = result2.data.data;
            }, submitProductoError);

        if ($scope.producto.detalles && $scope.producto.detalles != '')
            $scope.producto.productoCostos = JSON.parse($scope.producto.detalles);

       
        if ($scope.unidades)
        for (var i = 0; i < $scope.producto.productoUnidades.length; i++) {
            $scope.producto.productoUnidades[i].unidad = $scope.unidades.filter(t=>t.id == $scope.producto.productoUnidades[i].unidadId)[0];
        }

        if ($scope.producto.esServicio && $scope.producto.esCompuesto)
        {
            apiService.get("/api/producto/cargarBasesServicioCompuesto/" + $scope.producto.id, null).then(function (result) {
                hideLoading();
                $scope.producto.unidadesProductosBase = result.data.data;
                for (var i = 0; i < $scope.producto.productoUnidades.length; i++) {
                    $scope.producto.productoUnidades[i].unidad = $scope.unidades.filter(t => t.id == $scope.producto.productoUnidades[i].unidadId)[0];
                }
               

            }, submitProductoError);
        }
    }

    

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: $scope.valorBusqueda.campo,
            tipoCampo: $scope.valorBusqueda.tipoCampo,
            valor: $scope.valorBusqueda.valor
        };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.productos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalProductos = result.data.pagedData.count;

        }, submitProductoError);
        return promise;
    }



    $scope.$watchGroup(['producto.costo', 'producto.precio'], function (newVal, oldVal) {
        for (var i = 0; i < $scope.producto.productoUnidades.length; i++) {
            $scope.producto.productoUnidades[i].precioCompra = ($scope.producto.costo / $scope.producto.productoUnidades[i].equivalencia);
            $scope.producto.productoUnidades[i].precioVenta = ($scope.producto.precio / $scope.producto.productoUnidades[i].equivalencia);
        }

    });

    $scope.$watch('producto.unidadesProductosBase', function (newValue, oldValue) {
        if (producto.unidadesProductosBase && producto.unidadesProductosBase.length > 0) {
        $scope.producto.costo = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'costoTotal');
        $scope.producto.precio = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'precioTotal');
        }
    });


});