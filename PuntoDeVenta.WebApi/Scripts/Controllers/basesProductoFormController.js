﻿var ProductoCostoFormController = angular.module('NavegationApp');
ProductoCostoFormController.controller('FormulariounidadesProductosBase', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();

    $scope.esEdicion = false;
    var apiService = new ApiService();


    $scope.producto = modalFunctions.Producto;
    $scope.producto.unidadesProductosBase = !$scope.producto.unidadesProductosBase ? new Array : $scope.producto.unidadesProductosBase;




    $scope.base = modalFunctions.baseActual;



    $scope.esEdicion = modalFunctions.baseActual.productoUnidadBase ? true : false;
    $scope.esServicioBase = $scope.base.productoUnidadBase && $scope.base.productoUnidadBase.esServicio ? true : false;

    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.guardarproductoUnidadBase = function () {
        let indice = modalFunctions.Index;
        let aGuardar = {};
        angular.copy($scope.base, aGuardar);

        aGuardar.costoTotal = aGuardar.cantidad * (aGuardar.productoUnidadBase ? aGuardar.productoUnidadBase.precioCompra : aGuardar.productoBase.costo);
        aGuardar.precioTotal = aGuardar.cantidad *( aGuardar.productoUnidadBase ? aGuardar.productoUnidadBase.precioVenta : aGuardar.productoBase.precio);

        let error = $scope.frmproductoUnidadBaseForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }

        aGuardar.productoCompuesto = null;

        if (indice >= 0) {

            $scope.producto.unidadesProductosBase[indice] = aGuardar;
        }
        else {

            let indexExistente = $scope.producto.unidadesProductosBase.findIndex(x => x.productoBaseId == aGuardar.productoBaseId);
            if (indexExistente >= 0)
                $scope.producto.unidadesProductosBase[indexExistente] = aGuardar;
            else
                $scope.producto.unidadesProductosBase.push(aGuardar);

        }

        $scope.producto.costo = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'costoTotal');
        $scope.producto.precio = $scope.ItemSumm($scope.producto.unidadesProductosBase, 'precioTotal');
        limpiar();




        $scope.closeDialog();



    }



    function limpiar() {
        $scope.base = {};
    }


    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }





    $scope.selectedproductoUnidadBase = function (newValue, oldValue) {

        if (newValue) {
            if (newValue.originalObject.id == $scope.base.productoCompuesto.id) {
                bootbox.alert('El servicio base no puede ser el mismo servicio compuesto');
                $scope.$broadcast('angucomplete-alt:clearInput');
                $scope.base.productoBase = null;
                $scope.base.productoUnidadBaseId = null;
                $scope.base.productoUnidadBase = null;
                return;

            }
            $scope.base.productoBase = newValue.originalObject;
            $scope.base.productoBaseId = $scope.base.productoBase.id;
            if ($scope.base.productoBase.esServicio) {
                $scope.esServicioBase = true;
                $scope.base.productoUnidadBase = null;
            }

            else
                $scope.esServicioBase = false;
        }
        else {
            $scope.base.productoBase = null;
            $scope.base.productoBaseId = null;
            $scope.esServicioBase = false;
        }


    }






});