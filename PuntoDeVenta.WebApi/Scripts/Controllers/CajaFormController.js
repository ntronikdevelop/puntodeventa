﻿var CajasFormController = angular.module('NavegationApp');
CajasFormController.controller('FormularioCajas', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    hideLoading();
    var CajasUrl = '/api/Caja';
    var apiService = new ApiService();
    $scope.id = modalFunctions.Id;
    $scope.inactivos = modalFunctions.inactivos;
    $scope.esEdicion = $scope.id > 0 ? true : false;
    $scope.Caja = { id: 0, nombre: '', activo: true, localidad: '' };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    VerificarCaja($scope.id);
    cargarLocalidades();
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarCaja = function () {
        var error = $scope.frmCajaForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();


        var toPost = $scope.Caja;
        switch ($scope.id) {
            case 0:
                apiService.post("/api/Caja", toPost, submitCajaSuccess, submitCajaError);
                break;
            default:
                apiService.put("/api/Caja", toPost, submitCajaSuccess, submitCajaError);
                break;
        }

    }

    function cargarLocalidades() {
        showLoading();
        apiService.get("/api/localidad", null).then(function (result) {
            hideLoading();
            $scope.localidades = result.data.data;
            $scope.Caja.localidad = $scope.localidades.filter(t=> t.id == $scope.Caja.localidadId)[0];

            if ($scope.localidades.length == 1) {
                $scope.Caja.localidad = $scope.localidades[0];
                $scope.Caja.localidadId = $scope.Caja.localidad.id;
            }
               

        }, submitCajaError);
    }



    function submitCajaSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.mensaje);

        llenarTabla(CajasUrl);
        if (result.data.status >= 0 && $scope.id > 0) {
            $uibModalInstance.close();

        }
        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.Caja = {};

        }



        $scope.showValidate = false;
    }

    function submitCajaError(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    function VerificarCaja(dataId) {
        if (dataId > 0) {
            showLoading();
            apiService.get("/api/Caja", { id: dataId })
                      .then(llenarCaja, submitCajaError);
        }
    }


    function llenarCaja(result) {
        hideLoading();
        $scope.Caja = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.Caja;

    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
        // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.Cajas = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalCajas = result.data.pagedData.count;
        }, submitCajaError);
        return promise;
    }
});