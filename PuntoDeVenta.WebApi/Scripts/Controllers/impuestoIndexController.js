﻿var impuestoIndexController = angular.module('ImpuestoIndexController', []);
impuestoIndexController.controller('TablaDeImpuestosController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var impuestosUrl = '/api/impuesto';
    $scope.resultadoEnPantalla = { impuestos: [], totalImpuestos: 0, currentPage: 1, pageSize: 10, search: null };
    $scope.inactivos = false;
    var apiService = new ApiService();

    function llenarTabla(urlAUsar) {
       showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10, activos: !$scope.inactivos };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
         hideLoading();
            $scope.resultadoEnPantalla.impuestos = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalImpuestos = result.data.pagedData.count;
        }, ErrorResult);
        return promise;
    }

    $scope.EditarImpuesto = function (Id) {
       showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/ImpuestoForm?data=00046120", "FormularioImpuestos", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'impuestos')
        $scope.search();

    });

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }

    $scope.BorrarImpuesto = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el impuesto?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = impuestosUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(impuestosUrl);
                }, ErrorResult);
            }
        });
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        var urlAUsar = impuestosUrl;


        llenarTabla(urlAUsar);
    }

    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "Impuestos")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "Impuestos")
            $scope.search();

    });




}]);