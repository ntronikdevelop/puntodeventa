﻿var permisosFormController = angular.module('NavegationApp');
permisosFormController.controller('FormularioPermisosRoles', function ($scope, $filter, $q, ApiService, modalFunctions, $uibModalInstance) {
    var permisosUrl = '/permisos';
    var apiService = new ApiService();
    hideLoading();
    $scope.role = modalFunctions.Role;
    $scope.permisotemp = { seccion: null, operaciones: new Array() };
    $scope.permiso = { roleId: $scope.role.id, permisos: new Array() };
    $scope.resultadoEnPantalla = modalFunctions.resultadoEnPantalla;
    cargarSecciones();
   
    VerificarPermiso($scope.role);
    $scope.closeDialog = function () {
        $uibModalInstance.close();
    }

    $scope.GuardarPermiso = function () {
        var error = $scope.frmPermisoForm.$error;
        if (error.required || error["autocomplete-required"]) {
            $scope.showValidate = true;
            return;
        }
        showLoading();
        var toPost = {};
        angular.copy($scope.permiso, toPost);
        toPost.permisosJson = JSON.stringify(toPost.permisos);
        toPost.permisos = null;

        apiService.post("/api/roles/GuardarPermisos", toPost, submitPermisoSuccess, submitPermisoError);
    }

    $scope.convertirListaEnTexto = function (lista) {
        var texto = "";

        for (var i = 0; i < lista.length; i++) {
            texto += texto == "" ? lista[i].operacionNombreIlustrativo : "," + lista[i].operacionNombreIlustrativo;
        }

        return texto;
    }

    $scope.EditarDetallePermiso = function () {
        if ($scope.permisotemp.operaciones && $scope.permisotemp.operaciones.length > 0) {
            let permisoAGuardar = {};
            angular.copy($scope.permisotemp, permisoAGuardar);
            var index = $scope.permiso.permisos.map(function (el) {
                return el.seccion.nombre;
            }).indexOf(permisoAGuardar.seccion.nombre);

            if (index >= 0) {
                $scope.permiso.permisos.splice(index, 1);
                $scope.permiso.permisos.push(permisoAGuardar);
            }

            else {
                $scope.permiso.permisos.push(permisoAGuardar);
            }
            $scope.permisotemp.operaciones = new Array();

            for (let i = 0; i < $scope.operaciones.length; i++)
                $scope.operaciones[i].seleccion = false;
            $scope.seleccionarTodos = false;

        }

    
    }

    $scope.AnularDetalle = function (detalle) {
        var index = $scope.permiso.permisos.map(function (el) {
            return el.seccion.nombre;
        }).indexOf(detalle.seccion.nombre);
        $scope.permiso.permisos.splice(index, 1);
    }

    $scope.agregarOperacion = function (operacion) {
        let aGuardar = {
            seccionId: $scope.permisotemp.seccion.id,
            nombreSeccion: $scope.permisotemp.seccion.nombre,
            id: operacion.id,
            operacionId: operacion.operacionId,
            operacionNombre: operacion.operacion.nombre,
            operacionNombreIlustrativo: operacion.operacion.nombreIlustrativo
        }

        var index = $scope.permisotemp.operaciones.findIndex(o => o.id == aGuardar.id);
        if (index >= 0 && !operacion.seleccion) {
            $scope.permisotemp.operaciones.splice(index, 1);
        }
        else if (index < 0 && operacion.seleccion) {
            $scope.permisotemp.operaciones.push(aGuardar);
        }
    }

    $scope.$watch('seleccionarTodos', function (newValue, oldValue) {
        let value = $scope.seleccionarTodos ? true : false;
        if ($scope.operaciones && $scope.operaciones.length>0)
        for (let i = 0; i < $scope.operaciones.length; i++)
        {
            $scope.operaciones[i].seleccion = value;
            $scope.agregarOperacion($scope.operaciones[i]);
        }
    });


    function cargarSecciones() {
        showLoading();
        apiService.get("/api/secciones", null).then(function (result) {
            hideLoading();
            $scope.secciones = result.data.data;
            $scope.permisotemp.seccion = $scope.secciones.filter(t=> t.nombre == $scope.permisotemp.nombreSeccion)[0];

        },submitPermisoError);
    }

  

    $scope.cargarOperaciones = function (seccionId) {
        showLoading();
        let url = "/api/operaciones/ObtenerOperacionesSeccion";
        apiService.get(url, { seccionId: seccionId }).then(function (result) {
            hideLoading();
            $scope.operaciones = result.data.data;

        }, submitPermisoError);
    }

    function submitPermisoSuccess(result) {
        hideLoading();
        bootbox.alert(result.data.message);
        var urlAUsar = permisosUrl + '/ObtenerDatosDeUsuarios';
        llenarTabla(urlAUsar);
        if (result.data.status >= 0 && $scope.id>0) {

            $uibModalInstance.close();

        }

        if (result.data.status >= 0 && $scope.id <= 0) {
            $scope.permiso = {};

        }

        $scope.showValidate = false;
    }


    function submitPermisoError(result) {
        hideLoading();
        if (result.status == 401)
            window.location.href = apiService.getFullPath('/account/login');
        else
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
    }

    function VerificarPermiso(role) {
        if (role && role.id > 0) {
            showLoading();
            apiService.get("/api/roles/permisosDeRole", { roleId: role.id })
                      .then(llenarPermiso,submitPermisoError);
        }
    }


    function llenarPermiso(result) {
        hideLoading();
        $scope.permiso = (result.data != undefined && result.data != null && result.data.pagedData.count>0) ? result.data.pagedData.result[0] : $scope.permiso;
        $scope.permiso.permisos = JSON.parse($scope.permiso.permisosJson);
    }

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.get(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.permisos = result.data.permisos;
            $scope.resultadoEnPantalla.totalPermisos = result.data.totalPermisos;
        },submitPermisoError);
        return promise;
    }
});