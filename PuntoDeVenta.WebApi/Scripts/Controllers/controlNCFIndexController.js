﻿var controlNCFIndexController = angular.module('controlNCFIndexController', []);
controlNCFIndexController.controller('TablaDecontrolNCFController', ['$scope', '$filter', "$q", 'ApiService', 'ModalUtils', function ($scope, $filter, $q, ApiService, ModalUtils) {
    var algo = $scope.seleccion;
    var modalUtils = new ModalUtils();
    var controlNCFsUrl = '/api/controlNCF';
    $scope.resultadoEnPantalla = { controlNCFs: [], totalcontrolNCFs: 0, currentPage: 1, pageSize: 10, search: null };
   
    var apiService = new ApiService();
    $scope.inactivos = false;

    function llenarTabla(urlAUsar) {
        showLoading();
        var data = {
            page: $scope.resultadoEnPantalla.currentPage,
            size: 10,
            activo: !$scope.inactivos,
            campo: "",
            tipoCampo: "",
            valor: ""
        };
       // var data = { page: $scope.resultadoEnPantalla.currentPage, size: 10 };
        var promise = apiService.patch(urlAUsar, data);
        promise.then(function (result) {
            hideLoading();
            $scope.resultadoEnPantalla.controlNCFs = result.data.pagedData.result;
            $scope.resultadoEnPantalla.totalcontrolNCFs = result.data.pagedData.count;
        },ErrorResult);
        return promise;
    }

    $scope.EditarControlNCF = function (Id) {
      showLoading();
        $scope.Id = Id;
        modalUtils.openInstance("/Modals/controlNCFForm?data=00046120", "FormularioControlNCFs", { Id: Id, modal: modalUtils, resultadoEnPantalla: $scope.resultadoEnPantalla, inactivos: $scope.inactivos });
    }

    $scope.BorrarControlNCF = function (Id) {

        bootbox.confirm('Esta seguro de eliminar el registro del controlNCF?', function (desicion) {
            if (desicion) {
                showLoading();
                var url = controlNCFsUrl + '?idABorrar=' + Id;
                var promise = apiService.delete(url, null);
                promise.then(function (result) {
                    hideLoading();
                    bootbox.alert(result.data.mensaje);
                    llenarTabla(controlNCFsUrl);
                },ErrorResult);
            }
        })
    }

    $scope.pageChanged = function () {
        $scope.search();
    };

    $scope.search = function (propiedad) {
        
        var urlAUsar = controlNCFsUrl;


        llenarTabla(urlAUsar);
    }


    $scope.ObtenerSoloFecha = function (fechaString) {
        var dateOnly = fechaString.split('T')[0];
        return dateOnly;
    }

    function ErrorResult(result) {
        hideLoading();

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            bootbox.alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


    $scope.init = function () {


        $q.all([
              $scope.search()
        ])
    };
    if ($scope.seleccion == "controlNCF")
        $scope.init();

    $scope.$watch('seleccion', function (newValue, oldValue) {
        if (newValue == "controlNCF")
         
            $scope.search();

    });

    $scope.$watch('inactivos', function (newValue, oldValue) {
        let seleccion = $scope.seleccion;
        if (seleccion.toLowerCase() == 'controlncf')
            $scope.search();

    });







}]);