﻿var App = angular.module('ModalService', []);
App.factory('ModalUtils', function ($uibModal) {
    var loadingTemplate = '/Content/modals/loadingModal.html'
    var ModalUtils;
    var modal = null;

    function ModalUtils() {
    }

    ModalUtils.prototype.open = open;
    ModalUtils.prototype.close = close;
    ModalUtils.prototype.openLoadingModal = function () {
        open(loadingTemplate);
    }
    ModalUtils.prototype.waitRequests = function (requests) {
        open(loadingTemplate);
        requests.then(close);
    }
    ModalUtils.prototype.openLarge = function (template, scope) {
        var modalInstance = null;
        var opts =
        {
            animation: true,
            backdrop: 'static',
            templateUrl: template,
            size: 'lg',
            scope: scope,
            resolve: {
                modal: function () {
                    return {};
                }
            }
        };
        modalInstance = $uibModal.open(opts);
        modal = modalInstance;
        return modalInstance;
    }
    ModalUtils.prototype.openInstance = function (url, controller, functions, size) {
        var opts =
        {
            animation: true,
            backdrop: 'static',
            templateUrl: url,
            size: size || 'lg',
            controller: controller,
            resolve: { modalFunctions: functions },
        };
        modal = $uibModal.open(opts);

    }
    function open(template, scope) {
        var opts = createOptions(template, scope);
        modal = $uibModal.open(opts);
    }
    function close() {
        if (modal) modal.close();
    }
    function createOptions(template, scope) {
        var opts =
        {
            animation: true,
            backdrop: 'static',
            templateUrl: template,
            scope: scope
        };
        return opts;
    }

    return ModalUtils;
});