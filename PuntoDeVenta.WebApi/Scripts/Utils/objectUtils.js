﻿var objectutilapp = angular.module('objectutilapp');
objectutilapp.factory('objectutils', function () {

    var objectutils;

    function objectutils() {
    }

    objectutils.prototype.map = function (destination, source) {
        for (var property in source) {
                destination[property] = source[property];
        }
    }

    return objectutils;
});