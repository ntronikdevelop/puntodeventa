﻿
var app = angular.module('CrudService', ['ngResource']);

app.factory('ApiService', function ($http, $resource) {

    var ApiService;
    var endpoint = "http://" + window.location.hostname +(location.port?( ":" + location.port):'');

    function ApiService() {
        ApiService.constructor.apply(this, arguments);
    }
    ApiService.prototype.getFullPath = function (url) {
        return endpoint + url;
    }
    ApiService.prototype.get = function (url, data) {
        return $http({ url: endpoint + url, params: data , headers: { "X-Requested-With": "XMLHttpRequest"}});
    }
    ApiService.prototype.addPagination = function (data, page, pageSize) {
        data.page = page;
        data.pageSize = pageSize;
        return data;
    }
    ApiService.prototype.addFilter = function (data, property, value) {
        if (value != null) {
            data[property] = value;
        }
        return data;
    }
    ApiService.prototype.orderBy = function (data, fields, descending) {
        if (fields) {
            data['orderBy'] = fields.join();
            data['descending'] = descending == true;
        }
        return data;
    }
    ApiService.prototype.addSelection = function (data, values) {
        if (values != null && values.length > 0) {
            data.fields = values.join();
        }
        return data;
    }
    ApiService.prototype.post = function (url, data, onSuccess, onError) {
        var request = {
            method: 'POST',
            url: endpoint + url,
            data: data,
            headers: { "Content-Type": "application/Json", "Access-Control-Allow-Origin": "*", "X-Requested-With": "XMLHttpRequest" }
        };
        return $http(request).then(onSuccess, onError);
    }

    ApiService.prototype.download = function (url, data,emptyModel,reportName,verb) {
      
        showLoading();
        var toPost = data == undefined ? {}:data;
        if (angular.equals(toPost, {}))
            toPost = emptyModel;
        $http({
            url: endpoint + '/' + url,
            method: verb,
            params: toPost,
            headers: {
                'Content-type': 'application/octet-stream',
            },
            responseType: 'arraybuffer'
        }).success(function (data, status, headers, config) {
            // TODO when WS success
            hideLoading();
            var file = new Blob([data], {
                type: 'application/octet-stream'
            });
            //trick to download store a file having its URL
            var fileURL = URL.createObjectURL(file);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = reportName;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }).error(function (data, status, headers, config) {
            hideLoading();
            alert(data);
        });
    }

    ApiService.prototype.postDownload = function (url, data, onSuccess, onError) {
        var request = {
            method: 'POST',
            url: endpoint + url,
            data: data,
            headers: { "Content-Type": "application/octet-stream", "Access-Control-Allow-Origin": "*", "X-Requested-With": "XMLHttpRequest" }
        };
        return $http(request).then(onSuccess, onError);
    }

    ApiService.prototype.put = function (url, data, onSuccess, onError) {
        var request = {
            method: 'PUT',
            url: endpoint + url,
            data: data,
            headers: { "Content-Type": "application/Json", "Access-Control-Allow-Origin": "*", "X-Requested-With": "XMLHttpRequest" }
        };
        return $http(request).then(onSuccess, onError);
    }


    ApiService.prototype.delete = function (url, data) {
        var request = {
            method: 'DELETE',
            url: endpoint + url,
            data: data,
            headers: { "X-Requested-With": "XMLHttpRequest" }
        };
        var promise = $http(request);
        return promise;
    }



    ApiService.prototype.patch = function (url, data) {
        var request = {
            method: 'PATCH',
            url: endpoint + url,
            data: data,
            headers: { "Content-Type": "application/Json", "Access-Control-Allow-Origin": "*", "X-Requested-With": "XMLHttpRequest"}
        };
        var promise = $http(request);
        return promise;
    }
    return ApiService;
});

Array.prototype.sum = function (prop) {
    var total = 0
    for (var i = 0, _len = this.length; i < _len; i++) {
        total += this[i][prop]
    }
    return total
}


app.factory('httpInterceptor', function httpInterceptor($q, $window, $location) {
    return function (promise) {
        var success = function (response) {
            return response;
        };

        var error = function (response) {
            if (response.status === 401) {
                $location.url('Account/login');
            }

            return $q.reject(response);
        };

        return promise.then(success, error);
    };
});
