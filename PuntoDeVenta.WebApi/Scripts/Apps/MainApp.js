﻿var navegationApp = angular.module('NavegationApp', ['NavegationModule', 'ClienteIndexController', 'movimientoEntradaIndexController', 'ProductoIndexController',
    'gastoIndexController', 'MonedaIndexController', 'PermisosIndexController', 'ImpuestoIndexController', 'FacturaIndexController','gastoAgendaIndexController', 'usuarioIndexController',
    'AlmacenIndexController', 'SuplidoresIndexController', 'LocalidadIndexController', 'transferenciaAlmacenController', 'UnidadIndexController',
    'ReporteVentas', 'cxcGeneral', 'inventarioAlmacenes', 'movimientosProducto', 'vendedorIndexController', 'controlNCFIndexController', 'PagoIndexController', 'listadoClientes',
    'devolucionClienteIndexController', 'movimientoDevolucionIndexController', 'rolesIndexController', 'zonaIndexController', 'CajaIndexController', 'aperturaCajaIndexController',
    'CrudService', 'ModalService', 'reporteCxP', 'listadoPrecios', 'ngResource', 'angucomplete-alt', 'ui.select', 'ngSanitize', 'cotizacionesIndexModule', 'estadoCuentaCliente',
    'ui.bootstrap', 'frapontillo.bootstrap-switch', 'pagoGastoIndexModule', 'reporteRecibos', 'reporteComisiones', 'reporteEstadoEmpresa','reporteImpuestos']);

navegationApp.directive('myLoadData', function ($http, $templateCache) {

    return {
        scope: {
            url: '@',
            model: '=',
            urlDataField: '@',
            dependency: '=',
            onLoaded: '='
        },
        controller: function ($scope, $http, $attrs) {

            var init = function () {
                var request = {
                    url: $scope.url
                };
                var http = $http(request).then(
                    function (result) {
                        if ($scope.urlDataField)
                            $scope.model = result.data[$scope.urlDataField];
                        else
                            $scope.model = result.data;

                        if ($scope.onLoaded)
                            $scope.onLoaded();
                    });
            }

            $scope.$watch("url", function () {
                if ($attrs.dependency) {
                    if ($scope.dependency)
                        init();
                    else {
                        $scope.data = null;
                        $scope.model = null;
                    }

                }
                else
                    init();
            });


        },
        template: function (element, attrs) {

            var attrList = "";
            for (var i in attrs) {
                if (typeof attrs[i] != 'string' || i == 'model') continue;
                attrList += (i + "='" + attrs[i] + "'");
            }


            return '<input type="hidden" ' + attrList + '/>';
        },
        restric: "A",
        replace: true
    };

});

navegationApp.directive('mySelect', function ($http, $templateCache) {

    return {
        scope: {
            url: '@',
            titleField: '@',
            model: '=',
            urlDataField: '@',
            dependency: '='
        },
        controller: function ($scope, $http, $attrs) {

            var init = function () {
                var request = {
                    url: $scope.url
                };
                var http = $http(request).then(
                    function (result) {
                        $scope.data = result.data[$scope.urlDataField];
                    });
            }

            $scope.$watch("url", function () {
                if ($attrs.dependency) {
                    if ($scope.dependency)
                        init();
                    else {
                        $scope.data = null;
                        $scope.model = null;
                    }

                }
                else
                    init();
            });


        },
        template: function (element, attrs) {

            var attrList = "";
            var trackBy = attrs.trackBy ? " track by item." + attrs.trackBy : "";
            var titleFieldAs = attrs.titleFieldAs ? " as item." + attrs.titleFieldAs : "";
            for (var i in attrs) {
                if (typeof attrs[i] != 'string' || i == 'model') continue;
                attrList += (i + "='" + attrs[i] + "'");
            }

            console.log(typeof attrs['model']);

            return '<select ' + attrList + ' ng-model="model"  ng-options="item.' + attrs.titleField + titleFieldAs + ' for item in data' + trackBy + '"></select>';
        },
        restric: "A",
        replace: true
    };

});


