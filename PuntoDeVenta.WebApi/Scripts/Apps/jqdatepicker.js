﻿var app = angular.module("dateapp");

app.directive('jqdatepicker', function () {
    return {
        restrict: 'a',
        require: 'ngmodel',
        link: function (scope, element, attrs, ctrl) {
            $(element).datepicker({
                dateformat: 'yy/mm/dd',
                onselect: function (date) {
                    ctrl.$setviewvalue(date);
                    ctrl.$render();
                    scope.$apply();
                }
            });
        }
    };
});