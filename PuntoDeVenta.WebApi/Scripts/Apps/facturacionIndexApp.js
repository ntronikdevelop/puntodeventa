﻿var facturacionApp = angular.module('FacturacionApp', ['FacturacionIndexController', 'FacturacionService', 'ui.bootstrap', 'ngResource', 'angucomplete-alt', 'ui.select', 'ngSanitize']);

var facturacionFleteApp = angular.module('FacturacionFleteApp', ['FacturacionFleteController', 'FacturacionService', 'ui.bootstrap', 'ngResource', 'angucomplete-alt', 'ui.select', 'ngSanitize']);