﻿var impresionApp = angular.module('impresionApp', ['CrudService','ngResource']);

impresionApp.controller('impresionController', function ($scope, ApiService) {
  
    var apiService = new ApiService();
    $scope.tiposImpresora = ['Normal','POS80'];
  
    cargarFactura();

    function cargarFactura() {
        let dataId = $scope.facturaId;
        if (dataId > 0) {
        
            $scope.esCreacion = false;
            apiService.get("/api/factura", { id: dataId })
                      .then(llenarFactura, submitFacturaError);
        }
    }
 
 $scope.addDays= function(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

   
    function llenarFactura(result) {
       
        $scope.factura = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.factura;
     
    }

    $scope.PrintFactura = function (tipoImpresora, imprimir) {
        $scope.tipoImpresora = '';
        $scope.tipoImpresora = tipoImpresora;
        if (imprimir) {

            setTimeout(() => {
                window.print();
            }, 200);
        }

        window.onafterprint = (event) => {
            setTimeout(() => {
                document.getElementById('printButton_reset').click();
            }, 200)
            
        }
    }
    function submitFacturaError(result) {
      

        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
           alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionEntradaController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.movimientoId;
        if (dataId !=undefined && dataId!=null && dataId !='') {

            $scope.esCreacion = false;
            apiService.get("/api/movimientoEntrada/ObtenerEntradasPorReferencia", { referencia: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.movimiento = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.movimiento;

    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])){
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionTransferenciaController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.movimientoId;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/transferenciaAlmacen/ObtenerEntradasPorReferencia", { referencia: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.movimiento = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.movimiento;

    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});
impresionApp.controller('impresionDevolucionController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.movimientoId;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/MovimientoDevolucion/ObtenerEntradasPorReferencia", { referencia: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.movimiento = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.movimiento;

    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionNotaCreditoController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.notaCreditoRef;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/MovimientoDevolucionCliente/ObtenerNotaCredito/" + dataId, null)
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.notaCredito = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.notaCredito;

    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionPagoController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.secuencia;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/Pago/obtenerPorSecuencia", { secuencia: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.pagos = (result.data != undefined && result.data != null) ? result.data.data : $scope.pagos;
        let pago = $scope.pagos[0];
        $scope.pago = {
            fechaCreacion: pago.fechaCreacion,
            creadoPor: pago.creadoPor,
            secuencia: pago.secuencia,
            cliente: pago.cliente,
            moneda: pago.moneda,
            detallesPago: $scope.pagos
        }
    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionPagoGastoController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.secuencia;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/pagoGasto/ObtenerPorSecuencia", { secuencia: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.pagosGastos = (result.data != undefined && result.data != null) ? result.data.data : $scope.pagosGastos;
        let pago = $scope.pagosGastos[0];
        $scope.pagoGasto = {
            fechaCreacion: pago.fechaCreacion,
            creadoPor: pago.creadoPor,
            secuencia: pago.secuencia,
            suplidor: pago.suplidor,
            moneda: pago.moneda,
            detallesPago: $scope.pagosGastos
        }
    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});

impresionApp.controller('impresionGastoController', function ($scope, ApiService) {


    var apiService = new ApiService();

    cargarEntradas();

    function cargarEntradas() {
        let dataId = $scope.secuencia;
        if (dataId != undefined && dataId != null && dataId != '') {

            $scope.esCreacion = false;
            apiService.get("/api/gasto", { id: dataId })
                .then(llenarEntrada, submitFacturaError);
        }
    }

    function llenarEntrada(result) {

        $scope.gasto = (result.data != undefined && result.data != null) ? result.data.data[0] : $scope.gasto;
  
    }

    $scope.ItemSumm = function (array, item) {
        if (array != undefined && array != null) {
            let result = 0;
            for (var i = 0; i < array.length; i++) {
                if (!isNaN(array[i][item])) {
                    result += array[i][item];
                }
            }
            return result;
        }
        return 0;
    }

    $scope.Print = function () {
        window.print();
    }
    function submitFacturaError(result) {


        if (result.status == 401) {
            window.location.href = apiService.getFullPath('/account/login');

        } else {
            alert('Ha ocurrido un error. Favor contactar administrador');
        }


    }


});


