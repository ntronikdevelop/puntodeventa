Collapse =
{

    toggle: function(item, id)
    {
        var root = document.getElementById("root_" + id);
        // only process for root posts
        if (root)
        {
            console.log("processing root post.");
            var postmeta = getDescendentByTagAndClassName(item, "div", "postmeta");

            var close = getDescendentByTagAndClassName(postmeta, "a", "close");
            var show = getDescendentByTagAndClassName(postmeta, "a", "showpost");

            close.addEventListener("click", function() { Collapse.close(id); });
            show.addEventListener("click", function() { Collapse.show(id); });
        }
    },

    close: function(id)
    {
        console.log("closing: " + id);
    },

    show: function(id)
    {
        console.log("showing: " + id);
    }

}

processPostEvent.addHandler(Collapse.toggle);
