﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Caching;

namespace PuntoDeVenta.WebApi.Helpers
{
    public class CacheHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Tipo de objeto a insertar</typeparam>
        /// <param name="objeto"> Objeto a guardar en cache</param>
        /// <param name="idCache"> Identificador de objeto en cache</param>
        /// <param name="minutos"> Cantidad de tiempo en minutos que se guardara</param>
        public static void InsertarObjetoEnCache<T>(T objeto,string idCache,int minutos)
        {
            var cacheItem = (T)MemoryCache.Default.Get(idCache);
            if (cacheItem != null)
                MemoryCache.Default.Remove(idCache);
            MemoryCache.Default.Add(idCache, objeto, DateTime.Now.AddMinutes(minutos));
        }

        public static T ObtenerObjetoEnCache<T>( string idCache)
        {
         return  (T) MemoryCache.Default.Get(idCache) ;
        }

        public static void BorrarObjetoEnCache(string idCache)
        {
           MemoryCache.Default.Remove(idCache);
        }
    }
}