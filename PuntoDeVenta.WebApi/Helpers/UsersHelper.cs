﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using PuntoDeVenta.WebApi.Models;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Globalization;
using System.Threading.Tasks;

using System.Web.Mvc;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;


namespace PuntoDeVenta.WebApi.Helpers
{
    public class UsersHelper
    {
        public static List<PermisoModel> ObtenerPermisosDeUsuarioDesdeClaimsEnCache(string userId, Users usuario=null)
        {
            List<PermisoModel> resultado = new List<PermisoModel>();
            Users usuarioNew = usuario?? CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            usuarioNew.Claims.Where(x => x.Type.ToLower() == "seccion").ToList().ForEach(x => {
                PermisoModel permiso = JsonConvert.DeserializeObject<PermisoModel>(x.Value);
                resultado.Add(permiso);
            });
            return resultado;
        }

        public static List<PermisoModel> ObtenerPermisosDeUsuarioDesdeClaimsEnDb(string userId, Users usuario = null)
        {
            var UserManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            List<PermisoModel> resultado = new List<PermisoModel>();
            IList<Claim> claims = usuario!=null?usuario.Claims: UserManager.GetClaims(userId);
            claims.Where(x => x.Type.ToLower() == "seccion").ToList().ForEach(x => {
                PermisoModel permiso = JsonConvert.DeserializeObject<PermisoModel>(x.Value);
                resultado.Add(permiso);
            });
            return resultado;
        }
    }
}