﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using PuntoDeVenta.WebApi.Models;


namespace PuntoDeVenta.WebApi.Helpers
{
    public class ActionAuthorize : AuthorizeAttribute
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public object ControllerContext { get; private set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)

        {

            
            string routeValues = HttpContext.Current.Request.Path;
            //ARREGLAR LO DEL CONTROLADOR QUE ESTA COGIENDO EL ULTIMO
            var urlArr = routeValues.Contains("/") ? routeValues.Split('/').ToList(): new List<string>() { };
            int apiIndex = urlArr.FindIndex(x => x.ToLower() == "api");
            if (apiIndex < 0)
                return false;
            this.Controller = urlArr[apiIndex + 1];
           // this.Controller = routeValues.Contains("/")?routeValues.Split('/').LastOrDefault():string.Empty;
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId))
                return false;
            var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            if (usuario != null)
            {
                var permisoActual = usuario.SeccionesUsuario.Where(x => x.NombreSeccion.ToLower() == Controller.ToLower() || x.NombreSeccion.ToLower() == "*").ToList();
                if (usuario.SeccionesUsuario.Count == 0 || permisoActual.Exists(p=>p.OperacionNombre.ToLower()==Action.ToLower()))
                    return true;
                else
                    return false;
            }
            return false;
        }

    }
}