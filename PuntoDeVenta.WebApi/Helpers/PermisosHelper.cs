﻿using PuntoDeVenta.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service;

namespace PuntoDeVenta.WebApi.Helpers
{
    public class PermisosHelper
    {
        public static List<Claim> ConvertirCreacionDePermisosAClaims(PermisosUsuarioModel modelo)
        {
            List<Claim> result = new List<Claim>();

            modelo.Permisos.ForEach(x =>
           {
               PermisoModel permiso = new PermisoModel();
               permiso.Operaciones = x.Operaciones ?? new List<string>();
               permiso.Nombre = x.Seccion.Nombre.ToLower() == "todas" ? "*" : x.Seccion.Nombre;
               result.Add(new Claim("Seccion", JsonConvert.SerializeObject(permiso)));
           });

            return result;
        }

        public static void GuardarClaimsDeUsuarioEnDb(string userId, List<Claim> claims)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            claims.ForEach(x =>
            {
                var result = userManager.AddClaim(userId, x);
                if (!result.Succeeded)
                    throw new Exception("Los permisos no pudieron ser procesados. Favor contactar al administrador");
            });



        }

        public static void BorrarPermisosDeSeccionAUsuario(string userId, string seccion)
        {
            IUserClaimsService service = new UserClaimsService();
            seccion = seccion.ToLower() == "todas" ? "*" : seccion;
            var claimsDeUsuario = service.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo = "UserId", TipoCampo = (int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUALSTRING, Valor = userId } }
            }).Result;
            List<PermisoModel> permisos = new List<PermisoModel>();
           
            var claims = claimsDeUsuario.Where(x => x.ClaimType.ToLower() == "seccion").ToList();
            claims.ForEach(t =>
            {
                permisos.Add(JsonConvert.DeserializeObject<PermisoModel>(t.ClaimValue));
            });
            var claimABorrar = claims.Where(x => x.ClaimValue.ToLower() == (JsonConvert.SerializeObject(permisos.Where(y => y.Nombre.ToLower() == seccion.ToLower()).FirstOrDefault()).ToLower()));
            claimABorrar.ToList().ForEach(x => {
                service.Eliminar(x.Id, string.Empty);
            });
            
        }
    }
}