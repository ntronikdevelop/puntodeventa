﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using PuntoDeVenta.WebApi.Models;


namespace PuntoDeVenta.WebApi.Helpers
{
    public class ModuleAuthorize : AuthorizeAttribute
    {
       
        public string Module { get; set; }
        public object ControllerContext { get; private set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)

        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId))
                return false;
            var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            if (usuario != null)
            {
                var permisoActual = usuario.Permisos.Where(x => x.Nombre.ToLower() == Module.ToLower() || x.Nombre.ToLower() == "*").FirstOrDefault();
               if(permisoActual!=null)
                    return true;
                else
                    return false;
            }
            return false;
        }

    }
}