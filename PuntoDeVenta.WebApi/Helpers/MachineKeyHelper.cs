﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuntoDeVenta.Constants.WindowsHelpers;

namespace PuntoDeVenta.WebApi.Helpers
{
    public class MachineKeyHelper
    {
        public static string GetMachineKey()
        {

            string serialWindows =  KeyDecoder.GetWindowsProductKey();
            string macAdrr = MACDecoder.ObtenerMACAddress();
            return $"{serialWindows}-{macAdrr}";
        }

    }
}