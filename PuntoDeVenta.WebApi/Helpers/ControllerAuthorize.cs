﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using PuntoDeVenta.WebApi.Models;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net.Http;

namespace PuntoDeVenta.WebApi.Helpers
{
    public class ControllerAuthorize : System.Web.Http.AuthorizeAttribute
    {
        public string Controller { get; set; }
        protected override bool IsAuthorized(HttpActionContext actionContext)

        {


            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId))
                return false;
            var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            if (usuario != null)
            {
                var permisoActual = usuario.SeccionesUsuario.Where(x => x.NombreSeccion.ToLower() == Controller.ToLower() || x.NombreSeccion.ToLower() == "*").FirstOrDefault();
                if (permisoActual != null || usuario.SeccionesUsuario.Count==0)
                    return true;
                else
                    return false;
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            base.HandleUnauthorizedRequest(actionContext);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            base.OnAuthorization(actionContext);
        }



    }

    public class MVCControllerAuthorize :System.Web.Mvc.AuthorizeAttribute
    {
        public string Controller { get; set; }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId))
                filterContext.Result = new System.Web.Mvc.RedirectResult($"/account/login/");
             var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            if (usuario != null)
            {
                var permisoActual = usuario.SeccionesUsuario.Where(x => x.NombreSeccion.ToLower() == Controller.ToLower() || x.NombreSeccion.ToLower() == "*").FirstOrDefault();
                if (permisoActual != null || usuario.SeccionesUsuario.Count == 0)
                    base.OnAuthorization(filterContext);

                else
                    filterContext.Result = new System.Web.Mvc.RedirectResult($"/account/login/");
            }
            else
            {
                filterContext.Result = new System.Web.Mvc.RedirectResult($"/account/login/");
            }
          


        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
            else
            {
                filterContext.Result = new System.Web.Mvc.RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account/Login" }));
            }
        }



    }


}