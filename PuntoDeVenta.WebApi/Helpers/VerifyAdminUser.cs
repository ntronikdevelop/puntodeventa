﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PuntoDeVenta.WebApi.Models;
using System.Runtime.Caching;
using PuntoDeVenta.WebApi.Helpers;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PuntoDeVenta.WebApi.Helpers
{
    public class VerifyAdminUser
    {
        public static void CheckIfAdminExist(ApplicationUserManager userManager)
        {

            userManager = userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            // var UserManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var defaultUser = new Users {
                UserName = "admin",
                Email = "admin@gmail.com",
                Claims = new List<Claim>() { new Claim("Seccion", "*") },
                Cedula = "00000000000",
                Nombre = "Administrador",
                Apellido = string.Empty,
                Activo = 1,
                LocalidadId=0
                
            };
            bool existsRecordsInUsersDb = (userManager.FindByName("admin") != null ? true : false);
            if (!existsRecordsInUsersDb)
            {
                var result = userManager.Create(defaultUser, "Noel.12345");
                if (result.Succeeded)
                {
                    var user = userManager.Find("admin", "Noel.12345");
                    userManager.AddClaim(user.Id, new Claim("Seccion", "{Nombre:'*', Operaciones:[]}"));
                }
                else
                    throw new Exception("Error al ingresar admin user");
            }
        }

        public static string GetCurrentUserAccess()
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!string.IsNullOrEmpty(userId))
            {
             var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
                return usuario != null ? JsonConvert.SerializeObject(new Users() { UserName=usuario.UserName, Nombre=usuario.Nombre, Apellido=usuario.Apellido, SeccionesUsuario=usuario.SeccionesUsuario }) : 
                    JsonConvert.SerializeObject(new Users() { SeccionesUsuario = usuario?.SeccionesUsuario ?? null, Nombre = usuario?.Nombre ?? "", UserName = usuario?.UserName ?? "" });
            }
            else
                return JsonConvert.SerializeObject(new Users() { SeccionesUsuario = null, Nombre = "", UserName = "" });
        }

        public static string verifyUserAccess(string section, string operation)
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!string.IsNullOrEmpty(userId))
            {
                var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
                if (usuario == null)
                    return "hideMenu";
                var exist = usuario.SeccionesUsuario.Exists(s => s.NombreSeccion.ToLower() == section.ToLower() && s.OperacionNombre.ToLower() == operation.ToLower())?"":"hideMenu";
                return (usuario.SeccionesUsuario!=null && usuario.SeccionesUsuario.Count == 0) ? "" : exist;
            }
            else
                return "hideMenu";
        }
    }
}