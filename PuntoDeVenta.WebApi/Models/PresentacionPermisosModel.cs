﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class PresentacionPermisosModel
    {
        public string UserId { get; set; }

        public string NombreUsuario { get; set; }

        public string Acciones { get; set; }

        public string Seccion { get; set; }

        public List<PermisoModel> Permisos { get; set; }
    }
}