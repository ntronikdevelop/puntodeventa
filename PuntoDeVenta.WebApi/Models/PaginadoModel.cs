﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class PaginadoModel
    {
        public int Page { get; set; }
        public int Size { get; set; }
        public bool Activo { get; set; }
        public string Campo { get; set; }
        public int TipoCampo { get; set; }
        public string Valor { get; set; }

    }
}