﻿using PuntoDeVenta.DataAccess.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class PermisosUsuarioModel
    {
        public string UsuarioId { get; set; }
        public Users usuario { get; set; }

        public List<Permiso> Permisos { get; set; }
    }

    public class Permiso
    {
        public List<string> Operaciones { get; set; }
        public Secciones Seccion { get; set; }
    }
}