﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class MovimientoEntradaModel
    {
        public Suplidores Suplidor { get; set; }
        public Localidad Localidad { get; set; }

        public Almacen Almacen { get; set; }

        public string Referencia { get; set; }

        public List<MovimientoEntrada> MovimientosEntradas { get; set; }
        public string NCF { get; set; }
    }
}