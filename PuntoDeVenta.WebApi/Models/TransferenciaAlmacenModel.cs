﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;

namespace PuntoDeVenta.WebApi.Models
{
    public class TransferenciaAlmacenModel
    {
        public Almacen AlmacenOrigen { get; set; }
        public Almacen AlmacenDestino { get; set; }
        public string Referencia { get; set; }
        public DateTime FechaCreacion { get; set; }
        public List<TransferenciaAlmacen> Transferencias { get; set; }
        public string CreadoPor { get; set; }
    }
}