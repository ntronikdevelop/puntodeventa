﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class DevolucionClienteModel
    {
        public Factura Factura { get; set; }

        public List<DetalleDevolucion> DetallesDevolucion { get; set; }
    }
}