﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Models
{
    public class PagoModel
    {
        public string Secuencia { get; set; }
        public string NoTalonario { get; set; }
        public decimal MontoPago { get; set; }

        public int TipoPagoId { get; set; }
        public string Detalles { get; set; }

        public int ClienteId { get; set; }
        public int VendedorId { get; set; }
        public int MonedaId { get; set; }

        public List<DetallePagoModel> DetallesPago { get; set; }

    }

    public class DetallePagoModel : Factura
    {
        public bool Seleccionado { get; set; }
        public decimal MontoAPagar { get; set; }
    }

    public class PagoGastoModel
    {
        public string Secuencia { get; set; }
        public decimal MontoPago { get; set; }

        public int TipoPagoId { get; set; }
        public string Detalles { get; set; }

        public int SuplidorId { get; set; }

        public int MonedaId { get; set; }

        public List<DetallePagoGastoModel> DetallesPago { get; set; }

    }

    public class DetallePagoGastoModel : GastoEmpresa
    {
        public bool Seleccionado { get; set; }
        public decimal MontoAPagar { get; set; }
    }
}