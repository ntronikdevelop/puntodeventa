﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class PermisoModel
    {
        public string Nombre { get; set; }
        public List<string> Operaciones { get; set; }
    }
}