﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.WebApi.Models
{
    public class Respuesta<T> where T: class
    {
        public Respuesta() { }

        public Respuesta(int status, string mensaje, List<T> data,string codigo="")
        {
            this.Status = status;
            this.Mensaje = mensaje;
            this.Data=data;
            this.Codigo = codigo;
            this.Count = data != null  ? data.Count : 0;
        }

        public Respuesta(int status, string mensaje, PagedResult<T> pagedData, string codigo = "")
        {
            this.Status = status;
            this.Mensaje = mensaje;
            this.PagedData = pagedData;
            this.Codigo = codigo;
            
        }


        public int Status { get; set; }
        public string Mensaje { get; set; }
        public string Codigo { get; set; }
        public List<T> Data { get; set; }

        public PagedResult<T> PagedData { get; set; }
        public int Count { get; set; }
    }
}