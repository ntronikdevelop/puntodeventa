﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class MovimientoDevolucionModel
    {
        public Suplidores Suplidor { get; set; }
        public Localidad Localidad { get; set; }

        public Almacen Almacen { get; set; }

        public string Referencia { get; set; }

        public string CreadoPor { get; set; }
        public DateTime FechaCreacion { get; set; }

        public List<MovimientoDevolucion> MovimientosDevoluciones { get; set; }
    }
}