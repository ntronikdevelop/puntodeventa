﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class ReporteMovimientosProducto
    {
        public List<ReportePorAlmacen> ReportePorAlmacen { get; set; }
    }

    public class ReportePorAlmacen
    {
        public string AlmacenNombre { get; set; }
        public decimal cantidadExistente { get; set; }
        public List<MovimientoProductoModel> Movimientos { get; set; }
        public List<IGrouping<string, MovimientoProductoModel>> MovimientosPorProducto { get; set; }
    }
}