﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PuntoDeVenta.WebApi.Models
{
    public class ReporteDeVentasModel
    {
        public List<Factura> Resultado { get; set; }

        public List<ParametrosBusquedaModel> Parametros { get; set; }
    }

    public class ParametrosBusquedaModel
    {
        public string Campo { get; set; }
        public string Operador { get; set; }

        public string Condiciones { get; set; }
    }

    public class BusquedaPorFechasModel
    {
        public string FechaInicial { get; set; }

        public string FechaFinal { get; set; }
        public int? ClienteId { get; set; }
        public int? VendedorId { get; set; }
    }

    public class BusquedaCxCBasico
    {
        public string FechaInicial { get; set; }

        public bool Sumarizar { get; set; }
        public string FechaFinal { get; set; }

        public Nullable<int> ClienteId { get; set; }
        public Nullable<int> VendedorId { get; set; }
        public Nullable<int> MonedaId { get; set; }
    }

    public class BusquedaCxPBasico
    {
        public string FechaInicial { get; set; }

        public string FechaFinal { get; set; }

        public Nullable<int> SuplidorId { get; set; }
        public Nullable<int> MonedaId { get; set; }
    }

    public class BusquedaMovimientosProductoBasico
    {
        public string FechaInicial { get; set; }

        public string FechaFinal { get; set; }

        public Nullable<int> ProductoId { get; set; }
        public Nullable<int> AlmacenId { get; set; }
    }

 
}