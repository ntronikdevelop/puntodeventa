﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "TransferenciaAlmacen")]
    [Route("api/TransferenciaAlmacen")]

    public class TransferenciaAlmacenController : BaseController<ITransferenciaAlmacenService, TransferenciaAlmacen>
    {
      private ITransferenciaAlmacenService internalService;

        public TransferenciaAlmacenController():base(new TransferenciaAlmacenService())
        {
            this.internalService = new TransferenciaAlmacenService();
        }
        [HttpPost]
        [Route("api/TransferenciaAlmacen/RegistrarTransferencias")]
        [ActionAuthorize(Action = "RegistrarTransferencias")]
        public IHttpActionResult RegistrarTransferencias(TransferenciaAlmacenModel toInsert)
        {
            try
            {
               
                string secuenciaActual = SecuenciasHelper.CrearControlDeTransferencias();
                Respuesta<TransferenciaAlmacenModel> respuesta = new Respuesta<TransferenciaAlmacenModel>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew,option))
                {
                    foreach (TransferenciaAlmacen transferencia in toInsert.Transferencias)
                    {
                        TransferenciaAlmacen aguardar = DtoHelper.LlenarCamposPorDefectoCreacion(transferencia) as TransferenciaAlmacen;
                        aguardar.AlmacenDestinoId = toInsert.AlmacenDestino.Id;
                        aguardar.AlmacenOrigenId = toInsert.AlmacenOrigen.Id;
                        aguardar.Referencia = secuenciaActual;
                        if (aguardar.AlmacenDestinoId == aguardar.AlmacenOrigenId)
                            throw new Exception("Los almacenes no pueden ser el mismo");
                        internalService.Insertar(aguardar);
                    }
                    scope.Complete();
                   respuesta = new Respuesta<TransferenciaAlmacenModel>(1, "Se ha registrado con exito.", new List<TransferenciaAlmacenModel>());
                    
                }
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }


        [HttpGet]
        [Route("api/TransferenciaAlmacen/ObtenerEntradasPorReferencia")]
        [ActionAuthorize(Action = "ImprimirTransferencia")]
        public IHttpActionResult ObtenerEntradasPorReferencia(string referencia)
        {
            try
            {
               
                Respuesta<TransferenciaAlmacenModel> respuesta = new Respuesta<TransferenciaAlmacenModel>();
                var movimientos = internalService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo = "Referencia", TipoCampo = (int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUALSTRING, Valor = referencia } }
                }).Result; 


                if (movimientos != null && movimientos.Count > 0)
                {
                    var movimientoTemp = movimientos.FirstOrDefault();
                    respuesta.Data = new List<TransferenciaAlmacenModel>() { new TransferenciaAlmacenModel() {
                    AlmacenOrigen=movimientoTemp.AlmacenOrigen,
                    AlmacenDestino= movimientoTemp.AlmacenDestino,
                    Referencia=movimientoTemp.Referencia,
                    FechaCreacion=movimientoTemp.FechaCreacion,
                    CreadoPor=movimientoTemp.CreadoPor,
                    Transferencias=movimientos
                } };
                }

                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<MovimientoEntradaModel>(-1, ex.Message, new List<MovimientoEntradaModel>()));
            }
        }


    }
}
