﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using System.Configuration;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Pago")]
    [Route("api/pago")]
    public class PagoController : BaseController<IPagoService, Pago>
    {

        IPagoService pagoService;
        public PagoController() : base(new PagoService())
        {
            this.pagoService = new PagoService();
        }


        [ActionAuthorize(Action = "aplicarPagos")]
        [Route("api/pago/aplicarPagos")]
        [HttpPost]
        public IHttpActionResult aplicarPagos([FromBody]PagoModel toInsert)
        {
           
            List<string> errores = new List<string>();
            List<Pago> pagosARealizar = new List<Pago>();
            Respuesta<Pago> respuesta = new Respuesta<Pago>();

            var option = new TransactionOptions();
            option.IsolationLevel = IsolationLevel.ReadCommitted;
            option.Timeout = TimeSpan.FromMinutes(2);
            try
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["RequiredNoTalonario"]) && string.IsNullOrEmpty(toInsert.NoTalonario))
                    throw new Exception("Numero de talonario requerido");

                if(toInsert.DetallesPago.Count==0)
                    return Ok(new Respuesta<Pago>(-1, "No se ha seleccionado ningun pago.", new List<Pago>()));

                string secuencia = SecuenciasHelper.CrearControlDePagos();
                foreach (var detalle in toInsert.DetallesPago.Where(x=>x.MontoAPagar>0))
                {
                    pagosARealizar.Add(new Pago(
                        toInsert.ClienteId,
                        toInsert.MonedaId,
                        detalle.MonedaId,
                        toInsert.TipoPagoId,
                        detalle.MontoTotal,
                        detalle.MontoAPagar,
                        detalle.NumeroFactura,
                        1,
                        (detalle.MontoTotal - detalle.MontoAPagar),
                        secuencia, // secuencia
                        toInsert.Detalles,
                        DateTime.Now,
                        string.Empty,//creadoPor
                        true,
                        detalle.MontoAdeudado,
                        toInsert.NoTalonario,
                        detalle.PorcentajeVendedor,
                       toInsert.VendedorId==0? detalle.VendedorId:toInsert.VendedorId
                        ));
                }

                foreach (var pago in pagosARealizar)
                {
                    try
                    {
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                        {

                            Pago insertar = DataAccess.Dto.Helpers.DtoHelper.LlenarCamposPorDefectoCreacion(pago) as Pago;
                            var resultado = service.Insertar(insertar);
                            respuesta = new Respuesta<Pago>(resultado.Id, "Se ha registrado con exito.", new List<Pago>(), secuencia);
                            scope.Complete();

                        }
                    }
                    catch (Exception ex)
                    {
                        errores.Add(ex.Message);
                    }
                }




                if (errores.Count > 0)
                    throw new Exception(string.Join(",", errores));
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Pago>(-1, ex.Message, new List<Pago>()));
            }
        }

        [ActionAuthorize(Action = "leer")]
        [Route("api/pago/ObtenerPorSecuencia")]
        [HttpGet]
        public IHttpActionResult ObtenerPorSecuencia(string secuencia)
        {
           
            Respuesta<Pago> respuesta = new Respuesta<Pago>();


            try
            {
                var result = service.ObtenerListaFiltrada( new BuquedaCompleja() {
                    Activo=true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {Campo="Secuencia", TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING, Valor=secuencia } }
                }).Result;
                respuesta = new Respuesta<Pago>(0,"OK",result);
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Pago>(-1, ex.Message, new List<Pago>()));
            }
        }

        [HttpPost]
        [Route("api/Pago/ReporteRecibos")]
        [ActionAuthorize(Action = "ReporteRecibos")]
        public IHttpActionResult ReporteRecibos([FromBody] BusquedaReporteRecibos busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = pagoService.ReporteDeIngresos(busqueda);


                Respuesta<ReporteRecibosDto> respuesta = new Respuesta<ReporteRecibosDto>(1, "Se ha registrado con exito.", new List<ReporteRecibosDto>() { resultado});
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

   


        [HttpPost]
        [Route("api/Pago/ReporteRecibosExcel")]
        [ActionAuthorize(Action = "ReporteRecibos")]
        public HttpResponseMessage ReporteRecibosExcel([FromUri] BusquedaReporteRecibosExcel busqueda)
        {
            try
            {
                BusquedaReporteRecibos busquedaNew = new BusquedaReporteRecibos()
                {
                    clienteId = busqueda.clienteId,
                    monedaId = busqueda.monedaId,
                    NoFactura = busqueda.NoFactura,
                    TipoReporte = busqueda.TipoReporte,
                    vendedorId = busqueda.vendedorId,
                    fechaFinal = string.IsNullOrEmpty(busqueda.fechaFinal) ? DateTime.Now:Convert.ToDateTime(busqueda.fechaFinal),
                    fechaInicial = string.IsNullOrEmpty(busqueda.fechaInicial) ?
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : Convert.ToDateTime(busqueda.fechaInicial)
                };
                var resultado = pagoService.ReporteDeIngresosExcel(busquedaNew);

                var response = base.GenerateExcel(new string[] {
                    "Cliente",
                    "Fecha Pago",
                    "No. Recibo",
                    "Monto",
                    "Tipo de Pago",
                    "No. Talonario",
                    "No. Factura",
                    "Monto Pendiente",
                    "Vendedor",
                    "Cantidad días",
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.INTEGER, },

                resultado.Select(f => new string[] {
                  f.Cliente.CodigoYNombre,
                 f.FechaCreacion.ToString("dd-MM-yyyy"),
                  f.Secuencia,
                  f.MontoPagado.ToString(),
                  f.TipoPagoNombre,
                  f.NoTalonario,
                  f.NoFactura,
                  f.MontoPendiente.ToString(),
                  f.Vendedor.CodigoYNombre,
                  f.CantidadDias.ToString()

}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

    }
}
