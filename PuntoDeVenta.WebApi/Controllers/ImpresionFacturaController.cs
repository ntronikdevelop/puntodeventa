﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    public class ImpresionFacturaController : Controller
    {
        // GET: ImpresionFactura
        public ActionResult Index(int id)
        {
            ViewBag.facturaId = id;
            return View();
        }

        public ActionResult ImpresionPos(int id)
        {
            ViewBag.facturaId = id;
            return View();
        }

        public ActionResult TemplateRecibos() 
        {
            return View();
        }
    }
}