﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    [ControllerAuthorize(Controller = "Movimiento")]
    [Route("api/TipoMovimiento")]

    public class TipoMovimientoController : BaseController<ITipoMovimientoService, TipoMovimiento>
    {
      private ITipoMovimientoService service;

        public TipoMovimientoController():base(new TipoMovimientoService())
        {
            this.service = new TipoMovimientoService();
        }

    

    }
}
