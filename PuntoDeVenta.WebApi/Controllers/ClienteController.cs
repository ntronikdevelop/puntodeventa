﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    
    [Route("api/cliente")]
    [ControllerAuthorize(Controller = "cliente")]
    public class ClienteController : BaseController<IClienteService, Cliente>
    {
      private   IClienteService service;

        public ClienteController():base(new ClienteService())
        {
            this.service = new ClienteService();
        }
        [HttpPost]
        [ActionAuthorize(Action = "leer")]
        [Route("api/cliente/BusquedaCompleja")]
        public  IHttpActionResult BusquedaCompleja([FromBody] BuquedaCompleja data)
        {
            try
            {
                return Ok(base.BusquedaComplejaGenerica(data));
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Cliente>(-1, ex.Message, new List<Cliente>() { }));
            }
        }

        [HttpPost]
        [Route("api/cliente/ClientesFiltrados")]
        [ActionAuthorize(Action = "ClientesFiltrados")]
        public Respuesta<Cliente> ClientesFiltrados([FromBody] BuquedaCompleja data)
        {
            try
            {

                var request = Request;
                var resultado = service.ObtenerListaFiltrada(data);
                resultado.Result = resultado.Result.OrderBy(x => x.Codigo).ToList();
                Respuesta<Cliente> respuesta = new Respuesta<Cliente>(0, "ok", resultado);
                return respuesta;
            }
            catch (Exception ex)
            {
                return new Respuesta<Cliente>(-1, ex.Message, new List<Cliente>());
            }
        }

        [HttpGet]
        [Route("api/cliente/ClientesFiltradosExcel/{campo}/{tipoCampo:int}/{valor}")]
        [ActionAuthorize(Action = "ClientesFiltrados")]
        public HttpResponseMessage ClientesFiltradosExcel(string campo, int tipoCampo, string valor)
        {
            try
            {
                var diaHoy = DateTime.Now;
                var movimientos = new MovimientoAlmacenService();

                var resultado = base.BusquedaComplejaGenericaSinPaginado(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda(){ Campo=campo=="^^"?string.Empty:campo, TipoCampo=tipoCampo, Valor=valor=="^^"?string.Empty:valor }
                }
                });

                var response = base.GenerateExcel(new string[] {
                    "Nombre",
                    "Cedula/Rnc",
                    "Telefono",
                    "Dirección",
                    "Moneda",
                    "Días de vencimiento"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL
                },

                resultado.PagedData.Result.Select(f => new string[] {
                  f.CodigoYNombre,
                  f.CedulaRnc,
                  f.Telefono.ToString(),
                  f.Direccion??string.Empty,
                  f.Moneda.Codigo,
                  f.DiasVencimiento.ToString()
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


      
    }
}
