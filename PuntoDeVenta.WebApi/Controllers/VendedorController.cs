﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Vendedor")]
    [Route("api/vendedor")]
    public class VendedorController : BaseController<IVendedorService, Vendedor>
    {
      private IVendedorService service;
        IPagoService pagoService;
        IFacturaService facturaService;
        public VendedorController():base(new VendedorService())
        {
            this.service = new VendedorService();
            this.pagoService = new PagoService();
            this.facturaService = new FacturaService();
        }


        [Route("api/vendedor/vendedorPorZona/{id:int}")]
        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        public virtual IHttpActionResult vendedorPorZona(int id)
        {
            try
            {
                var resultado = service.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja() {
                    Activo=true,
                    Filtros=new List<DataAccess.FiltroBusqueda>()
                    {
                        new DataAccess.FiltroBusqueda()
                        {
                            Campo="ZonaId",
                            Valor=id.ToString(),
                            TipoCampo = 1
                        }
                    }
                }).Result;
                Respuesta<Vendedor> respuesta = new Respuesta<Vendedor>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Vendedor>(-1, ex.Message, new List<Vendedor>()));
            }
        }


        [HttpPost]
        [Route("api/Vendedor/ReporteComisiones")]
        [ActionAuthorize(Action = "ReporteComisiones")]
        public IHttpActionResult ReporteComisiones([FromBody] BusquedaReporteRecibos busqueda)
        {
            try
            {

               
                var resultado = busqueda.TipoReporte<=0? facturaService.ComisionesPorVentas(busqueda): pagoService.ComisionesPorCobros(busqueda);


                Respuesta<ReporteComisiones> respuesta = new Respuesta<ReporteComisiones>(1, "Se ha registrado con exito.", new List<ReporteComisiones>() { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpPost]
        [Route("api/Vendedor/ReporteComisionesExcel")]
        [ActionAuthorize(Action = "ReporteComisiones")]
        public HttpResponseMessage ReporteComisionesExcel([FromUri] BusquedaReporteRecibosExcel busqueda)
        {
            try
            {
                BusquedaReporteRecibos busquedaNew = new BusquedaReporteRecibos()
                {
                    clienteId = busqueda.clienteId,
                    monedaId = busqueda.monedaId,
                    NoFactura = busqueda.NoFactura,
                    TipoReporte = busqueda.TipoReporte,
                    vendedorId = busqueda.vendedorId,
                    fechaFinal = string.IsNullOrEmpty(busqueda.fechaFinal) ? DateTime.Now : Convert.ToDateTime(busqueda.fechaFinal),
                    fechaInicial = string.IsNullOrEmpty(busqueda.fechaInicial) ?
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : Convert.ToDateTime(busqueda.fechaInicial)
                };
                var resultado = busqueda.TipoReporte <= 0 ? facturaService.ComisionesPorVentasExcel(busquedaNew) : pagoService.ComisionesPorCobrosExcel(busquedaNew);
               
                var response = base.GenerateExcel(new string[] {
                    "Vendedor",
                    "Fecha",
                    "Referencia",
                    "Cliente",
                    "Monto",
                    "Moneda",
                    "Cantidad días",
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.INTEGER },

                resultado.Select(f => new string[] {
                  f.Vendedor,
                 f.Fecha.ToString("dd-MM-yyyy"),
                  f.Referencia,
                  f.Cliente,
                  f.Monto.ToString(),
                  f.CodigoMoneda,
                  f.CantidadDias.ToString()
                 
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

    }
}
