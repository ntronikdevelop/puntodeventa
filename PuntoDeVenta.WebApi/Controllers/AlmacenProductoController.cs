﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "AlmacenProducto")]
    [Route("api/AlmacenProducto")]
    public class AlmacenProductoController : BaseController<IAlmacenProductoService, AlmacenProducto>
    {
        IAlmacenProductoService internalService;

        public AlmacenProductoController():base(new AlmacenProductoService())
        {
            this.internalService = new AlmacenProductoService();
        }
        [HttpGet]
        [ActionAuthorize( Action ="Leer")]
        [Route("api/almacenProducto/ObtenerExistenciaDeProductoEnAlmacen/{productoId:int}/{almacenId:int}")]
        public IHttpActionResult ObtenerExistenciaDeProductoEnAlmacen(int productoId,int almacenId)
        {
            try
            {
               
                var request = Request;
                var resultado = internalService.ObtenerExistenciaDeProductoEnAlmacen(productoId,almacenId);
                Respuesta<AlmacenProducto> respuesta = new Respuesta<AlmacenProducto>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Almacen>(-1, ex.Message, new List<Almacen>()));
            }
        }

 
    }
}
