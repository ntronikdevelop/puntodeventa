﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "productounidades")]
    [Route("api/productounidades")]
    public class ProductoUnidadesController : BaseController<IProductosUnidadesService, ProductoUnidadesEquivalencia>
    {
        ProductoUnidadesService internalService;

        public ProductoUnidadesController():base(new ProductoUnidadesService())
        {
            internalService = new ProductoUnidadesService();
        }


       
        [Route("api/productounidades/obenerunidadesdeProducto/{productoId:int}")]
        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        public virtual IHttpActionResult Get(int productoId)
        {
            try
            {
              
                var request = Request;
                var resultado = internalService.ObtenerUnidadesDeProducto(productoId);
                Respuesta<ProductoUnidadesEquivalencia> respuesta = new Respuesta<ProductoUnidadesEquivalencia>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Producto>(-1, ex.Message, new List<Producto>()));
            }
        }


    }
}
