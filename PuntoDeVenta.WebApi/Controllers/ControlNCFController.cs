﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "ControlNCF")]
    [Route("api/ControlNCF")]
    public class ControlNCFController : BaseController<IControlNCFService,ControlNCF>
    {
    

        public ControlNCFController():base(new ControlNCFService())
        {
           
        }
        [HttpGet]
        public override IHttpActionResult Get()
        {
           
            try
            {
                var request = Request;
                var algo = request.Headers;
                var result = new List<ControlNCF>() { new ControlNCF() { Id = 0, Nombre = "NO APLICA", Tipo = null } };
                result.AddRange(base.service.ObtenerTodos());
                Respuesta<ControlNCF> respuesta = new Respuesta<ControlNCF>(0, "ok", result);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<ControlNCF>(-1, ex.Message, new List<ControlNCF>()));
            }
        }

    }
}
