﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Cotizacion")]
    [Route("api/cotizacion")]
    public class CotizacionController : BaseController<ICotizacionService, Factura>
    {
      private ICotizacionService internalService;
        private IAperturaCajaService aperturaCajaService;

        public CotizacionController():base(new CotizacionService())
        {
            this.internalService = new CotizacionService();
            this.aperturaCajaService = new AperturaCajaService();
        }


        // POST: api/Cliente
        [ActionAuthorize(Action = "crear")]
        [HttpPost]
        public override IHttpActionResult Post([FromBody] Factura toInsert)
        {
           
            Respuesta<Factura> respuesta = new Respuesta<Factura>();
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            toInsert.Localidad = localidadService.ObtenerPorId( usuario.LocalidadId);
            toInsert.LocalidadId = usuario.LocalidadId;
            toInsert.CajaId = usuario.CajaId ?? null;
           
            if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
            {
                DateTime fechaActual = DateTime.Now;
                var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                });
                if (aperturaActual.Count == 0)
                    return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                if (aperturaActual.Count > 1)
                    return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                    return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

            }
            return base.Post(toInsert);


         
        }



        [ActionAuthorize(Action = "modificar")]
        [HttpPut]
        public override IHttpActionResult Put([FromBody] Factura toInsert)
        {
           
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            toInsert.Localidad = localidadService.ObtenerPorId(toInsert.LocalidadId);
            //toInsert.LocalidadId = usuario.LocalidadId;
            //toInsert.CajaId = usuario.CajaId ?? null;
            if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
            {
                DateTime fechaActual = DateTime.Now;
                var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                });
                if (aperturaActual.Count == 0)
                    return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                if (aperturaActual.Count > 1)
                    return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                    return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

            }
            return base.Put(toInsert);
        }


        
        [Route("api/cotizacion/FacturarCotizacion/{id:int}")]
        [ActionAuthorize(Action = "FacturarCotizacion")]
        [HttpGet]
        public virtual IHttpActionResult FacturarCotizacion(int id)
        {
            
            try
            {
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
                {
                    DateTime fechaActual = DateTime.Now;
                    var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                    });
                    if (aperturaActual.Count == 0)
                        return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                    if (aperturaActual.Count > 1)
                        return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                    if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                        return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

                }
                Respuesta<object> respuesta = new Respuesta<object>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    internalService.FacturarCotizacion(id, System.Web.HttpContext.Current.User.Identity.GetUserId());
                    scope.Complete();
                   respuesta = new Respuesta<object>(0, "Cotización facturada correctamente.", new List<object>() { });
                }
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<object>(-1, ex.Message, new List<object>()));
            }
            
        }

        [Route("api/cotizacion/DuplicarCotizacion/{id:int}/{clienteId:int}")]
        [ActionAuthorize(Action = "FacturarCotizacion")]
        [HttpGet]
        public virtual IHttpActionResult DuplicarCotizacion(int id, int clienteId=0)
        {

            try
            {
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
                {
                    DateTime fechaActual = DateTime.Now;
                    var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                    });
                    if (aperturaActual.Count == 0)
                        return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                    if (aperturaActual.Count > 1)
                        return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                    if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                        return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

                }
                Respuesta<object> respuesta = new Respuesta<object>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    internalService.DuplicarCotizacion(id, System.Web.HttpContext.Current.User.Identity.GetUserId(),clienteId);
                    scope.Complete();
                    respuesta = new Respuesta<object>(0, "Cotización duplicada correctamente.", new List<object>() { });
                }
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<object>(-1, ex.Message, new List<object>()));
            }

        }






        [HttpPost]
        [Route("api/cotizacion/ReporteAvanzado")]
        [ActionAuthorize(Action = "ReporteAvanzado")]
        public IHttpActionResult ReporteAvanzado(ReporteDeVentasModel busquedaObj)
        {
            try
            {
                string query = "select * from factura where ";
                for (int i = 0; i < busquedaObj.Parametros.Count; i++)
                {
                    query += busquedaObj.Parametros[i].Campo + busquedaObj.Parametros[i].Operador + busquedaObj.Parametros[i].Condiciones +
                       (i == (busquedaObj.Parametros.Count - 1) ? ";" : " AND ");
                }
                
                CxCDto resultado = new CxCDto();
                resultado.Facturas= base.service.ConsultarPorQuery(query);
                resultado.Total = resultado.Facturas.Sum(x => x.MontoTotal);
                Respuesta<CxCDto> respuesta = new Respuesta<CxCDto>(1, "Se ha registrado con exito.", new List<CxCDto> { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }
        [HttpPost]
        [ActionAuthorize(Action = "leer")]
        [Route("api/cotizacion/BusquedaCompleja")]
        public  IHttpActionResult BusquedaCompleja([FromBody] BuquedaCompleja data)
        {
            try
            {
                return Ok(base.BusquedaComplejaGenerica(data));
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Factura>(-1, ex.Message, new List<Factura>() { }));
            }
           
        }

    }
}


