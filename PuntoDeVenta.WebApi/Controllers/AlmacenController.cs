﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Almacen")]
    [Route("api/almacen")]
    public class AlmacenController : BaseController<IAlmacenService, Almacen>
    {
        IAlmacenService internalService;

        public AlmacenController() : base(new AlmacenService())
        {
            this.internalService = new AlmacenService();
        }

        [ActionAuthorize(Action="Leer")]
        [HttpGet]
        [Route("api/almacen/ObtenerAlmacenesPorLocalidad/{localidadId:int}")]
        public IHttpActionResult ObtenerAlmacenesPorLocalidad(int localidadId)
        {
            try
            {
                var request = Request;
                var resultado = internalService.ObtenerAlmacenesPorLocalidad(localidadId);
                Respuesta<Almacen> respuesta = new Respuesta<Almacen>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Almacen>(-1, ex.Message, new List<Almacen>()));
            }
        }

        [HttpGet]
        [Route("api/almacen/obtenerAlmacenesUsuario")]
        [ActionAuthorize(Action = "Leer")]
        public IHttpActionResult ObtenerAlmacenesUsuario()
        {
            List<AlmacenModel> resultado = new List<AlmacenModel>() { new AlmacenModel() { Id = null, Nombre = "Automático." } };
            try
            {
                
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                var request = Request;
                resultado.AddRange(internalService.ObtenerAlmacenesPorLocalidad(usuario.LocalidadId).Where(a => a.Codigo != "DEF").Select(x => new AlmacenModel(x)));
                var indexAsignado = resultado.FindIndex(a => a.Id == usuario.AlmacenId);
                if (indexAsignado >= 0)
                    resultado[indexAsignado].AlmacenAsignado = true;

                Respuesta<AlmacenModel> respuesta = new Respuesta<AlmacenModel>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Almacen>(-1, ex.Message, new List<Almacen>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        [Route("api/almacen/ObtenerAlmacenesPorCodigo/{codigo}")]
        public IHttpActionResult ObtenerAlmacenesPorCodigo(string codigo)
        {
            try
            {
                var request = Request;
                var resultado = internalService.ObtenerPorCodigo(codigo);
                Respuesta<Almacen> respuesta = new Respuesta<Almacen>(0, "ok", new List<Almacen>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Almacen>(-1, ex.Message, new List<Almacen>()));
            }
        }

        [HttpPost]
        [ActionAuthorize(Action = "Crear")]
        public override IHttpActionResult Post([FromBody] Almacen toInsert)
        {
            toInsert.Codigo = toInsert.Codigo.ToUpper();
            return base.Post(toInsert);
        }
        [HttpPut]
        [ActionAuthorize(Action = "Actualizar")]
        public override IHttpActionResult Put([FromBody] Almacen toUpdate)
        {
            toUpdate.Codigo = toUpdate.Codigo.ToUpper();
            return base.Put(toUpdate);
        }

    }
}
