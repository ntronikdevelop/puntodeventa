﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Gasto")]
    [Route("api/gastoagendado")]
    public class GastoAgendadoController : BaseController<IGastoAgendadoService, GastoAgendado>
    {
        IGastoAgendadoService custom = new GastoAgendadoService();
        public GastoAgendadoController() : base(new GastoAgendadoService())
        {
         
        }


        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public override IHttpActionResult Post([FromBody] GastoAgendado toInsert)
        {
            return base.Post(toInsert);
        }

        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        [Route("api/gastoagendado/CrearGastosAgendados")]
        public  IHttpActionResult CrearGastosAgendados()
        {
             custom.GenerarGastosAgendados();
            return Ok(new Respuesta<GastoAgendado>(0, "Ok", new List<GastoAgendado>()));
        }




        [HttpPut]
        [ActionAuthorize(Action = "modificar")]
        public override IHttpActionResult Put([FromBody] GastoAgendado toInsert)
        {
            return base.Put(toInsert);
        }




        [HttpPost]
        [ActionAuthorize(Action = "leer")]
        [Route("api/gastoagendado/BusquedaCompleja")]
        public IHttpActionResult BusquedaCompleja([FromBody] BuquedaCompleja data)
        {
            try
            {
                return Ok(base.BusquedaComplejaGenerica(data));
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<GastoAgendado>(-1, ex.Message, new List<GastoAgendado>() { }));
            }

        }



    }
}


