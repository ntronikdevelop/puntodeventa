﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    [Route("api/MovimientoDevolucion")]
    [ControllerAuthorize(Controller = "MovimientoDevolucion")]
    public class MovimientoDevolucionController : BaseController<IMovimientoDevolucionService, MovimientoDevolucion>
    {
      private IMovimientoDevolucionService service;

        public MovimientoDevolucionController():base(new MovimientoDevolucionService())
        {
            this.service = new MovimientoDevolucionService();
        }

        [HttpPost]
        [Route("api/MovimientoDevolucion/RegistrarSalidas")]
        [ActionAuthorize(Action = "RegistrarSalidas")]
        public IHttpActionResult RegistrarEntradas(MovimientoDevolucionModel toInsert)
        {
            try
            {
               
                Respuesta<MovimientoDevolucion> respuesta = new Respuesta<MovimientoDevolucion>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    foreach (MovimientoDevolucion movimiento in toInsert.MovimientosDevoluciones)
                    {
                        string control =string.IsNullOrEmpty(toInsert.Referencia)? SecuenciasHelper.CrearControlDeDevolucionesSuplidor(): movimiento.Referencia;
                        MovimientoDevolucion aguardar = DtoHelper.LlenarCamposPorDefectoCreacion(movimiento) as MovimientoDevolucion;
                        aguardar.AlmacenId = toInsert.Almacen.Id;
                        aguardar.SuplidorId = toInsert.Suplidor.Id;
                        aguardar.Referencia = control;
                        aguardar.UnidadId = movimiento.UnidadId;
                        this.service.Insertar(aguardar);
                    }
                    scope.Complete();
                    respuesta = new Respuesta<MovimientoDevolucion>(1, "Se ha registrado con exito.", new List<MovimientoDevolucion>());

                }
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<MovimientoEntradaModel>(-1, ex.Message, new List<MovimientoEntradaModel>()));
            }
        }

        [HttpGet]
        [Route("api/MovimientoDevolucion/ObtenerEntradasPorReferencia")]
        [ActionAuthorize(Action = "ImprimirDevolucion")]
        public IHttpActionResult ObtenerEntradasPorReferencia(string referencia)
        {
            try
            {
               
                Respuesta<MovimientoDevolucionModel> respuesta = new Respuesta<MovimientoDevolucionModel>();
                var movimientos = service.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja() {
                    Activo=true,
                    Filtros= new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo="Referencia", TipoCampo=(int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUALSTRING, Valor=referencia } }
                }).Result;


                if (movimientos != null && movimientos.Count > 0)
                {
                    var movimientoTemp = movimientos.FirstOrDefault();
                    respuesta.Data = new List<MovimientoDevolucionModel>() { new MovimientoDevolucionModel() {
                    Almacen=movimientoTemp.Almacen,
                    Suplidor= movimientoTemp.Suplidor,
                    Referencia=movimientoTemp.Referencia,
                    CreadoPor=movimientoTemp.CreadoPor,
                    FechaCreacion=movimientoTemp.FechaCreacion,
                    MovimientosDevoluciones=movimientos
                } };
                }

                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<MovimientoDevolucionModel>(-1, ex.Message, new List<MovimientoDevolucionModel>()));
            }
        }

    }
}
