﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Factura")]
    [Route("api/factura")]
    public class FacturaController : BaseController<IFacturaService, Factura>
    {
        private IFacturaService internalService;
        private IAperturaCajaService aperturaCajaService;
        IEstadoEmpresaService estadoEmpresaService;
        public FacturaController() : base(new FacturaService())
        {
            this.internalService = new FacturaService();
            this.aperturaCajaService = new AperturaCajaService();
            this.estadoEmpresaService = new EstadoEmpresaService();
        }


        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public override IHttpActionResult Post([FromBody] Factura toInsert)
        {

            Respuesta<Factura> respuesta = new Respuesta<Factura>();
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
            {
                DateTime fechaActual = DateTime.Now;
                var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                });
                if (aperturaActual.Count == 0)
                    return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                if (aperturaActual.Count > 1)
                    return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                    return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

            }
            toInsert.Localidad = localidadService.ObtenerPorId(usuario.LocalidadId);
            toInsert.LocalidadId = usuario.LocalidadId;
            toInsert.CajaId = usuario.CajaId ?? null;
            return base.Post(toInsert);



        }


        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "CrearFacturaAntigua")]
        [Route("api/Factura/AgregarFacturaAntigua")]
        public IHttpActionResult AgregarFacturaAntigua([FromBody] Factura toInsert)
        {
            Respuesta<Factura> respuesta = new Respuesta<Factura>();
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            toInsert.Localidad = localidadService.ObtenerPorId(usuario.LocalidadId);
            toInsert.LocalidadId = usuario.LocalidadId;
            toInsert.CajaId = usuario.CajaId ?? null;
            try
            {

                if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
                {
                    DateTime fechaActual = DateTime.Now;
                    var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                    });
                    if (aperturaActual.Count == 0)
                        return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                    if (aperturaActual.Count > 1)
                        return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                    if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                        return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

                }

                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    toInsert = DataAccess.Dto.Helpers.DtoHelper.LlenarCamposPorDefectoCreacion(toInsert) as Factura;

                    var resultado = internalService.InsertarFacturaAntigua(toInsert);
                    respuesta = new Respuesta<Factura>(resultado.Id, "Se ha registrado con exito.", new List<Factura>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Factura>(-1, ex.Message, new List<Factura>()));
            }




        }

        [HttpPut]
        [ActionAuthorize(Action = "modificar")]

        public override IHttpActionResult Put([FromBody] Factura toInsert)
        {
            toInsert.MontoDevuelto = toInsert.MontoDevuelto == (toInsert.MontoTotal * -1) ? 0 : toInsert.MontoDevuelto;
            ILocalidadService localidadService = new LocalidadService();
            IAlmacenService almacenService = new AlmacenService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            //toInsert.Localidad = localidadService.ObtenerPorId(usuario.LocalidadId);
            //toInsert.LocalidadId = usuario.LocalidadId;
            if (toInsert.Localidad != null && toInsert.Localidad.Id > 0 && toInsert.Localidad.Almacenes == null)
                toInsert.Localidad.Almacenes = almacenService.ObtenerAlmacenesPorLocalidad(toInsert.Localidad.Id);
            if (usuario.CajaId.HasValue && usuario.CajaId.Value > 0)
            {
                DateTime fechaActual = DateTime.Now;
                var aperturaActual = aperturaCajaService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() {
                        Campo="Estado",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor="A"
                    },
                    new FiltroBusqueda(){
                        Campo="UserId",
                        TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING,
                        Valor=usuario.Id
                    } }
                });
                if (aperturaActual.Count == 0)
                    return Ok(new Respuesta<Factura>(-1, "El usuario no ha aperturado ninguna caja. No puede facturar.", new List<Factura>()));

                if (aperturaActual.Count > 1)
                    return Ok(new Respuesta<Factura>(-1, "El usuario tiene más de una apertura. No puede facturar.", new List<Factura>()));

                if ((fechaActual - aperturaActual.Result[0].FechaApertura).TotalMinutes > (usuario.TiempoAperturaCajaHoras * 60))
                    return Ok(new Respuesta<Factura>(-1, "El usuario ha excedido el tiempo de apertura de la caja. No puede facturar. Favor hacer el cierre de caja.", new List<Factura>()));

            }
            return base.Put(toInsert);
        }

        [HttpPost]
        [Route("api/Factura/ReporteDeVentas")]
        [ActionAuthorize(Action = "ReporteDeVentas")]
        public IHttpActionResult ReporteDeVentas([FromBody] BusquedaPorFechasModel busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerVentas(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? new DateTime(diaHoy.Year, diaHoy.Month, diaHoy.Day, 0, 0, 0) : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal), busqueda.ClienteId,busqueda.VendedorId
                    );


                Respuesta<CxCDto> respuesta = new Respuesta<CxCDto>(1, "Se ha registrado con exito.", new List<CxCDto> { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }
        [HttpPost]
        [Route("api/Factura/ReporteVentasExcel")]
        [ActionAuthorize(Action = "ReporteDeVentas")]
        public HttpResponseMessage ReporteVentasExcel([FromUri] BusquedaPorFechasModel busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerVentas(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),busqueda.ClienteId,busqueda.VendedorId);

                var response = base.GenerateExcel(new string[] {
                    "Cliente",
                    "Fecha facturado",
                    "No. Factura",
                    "Facturado por",
                    "Vendedor",
                    "Monto Itbis",
                    "Monto devolución",
                    "Monto NC",
                    "Monto total",
                    "Monto pagado",
                    "Monto adeudado",
                    "Monto vendedor",
                    "Cantidad días",
                    "Vencida"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.INTEGER,
                    ExcelColumnsDef.TEXT },

                resultado.Facturas.Select(f => new string[] {
                  f.Cliente.CodigoYNombre,
                 f.FechaFacturado.HasValue?  f.FechaFacturado.Value.ToString("dd-MM-yyyy"):string.Empty,
                  f.NumeroFactura,
                  f.CreadoPor,
                  f.Vendedor?.CodigoYNombre??"",
                  f.MontoItbis.ToString(),
                  f.MontoDevuelto.ToString(),
                  f.MontoNotaCreditoAplicada.ToString(),
                  f.MontoTotal.ToString(),
                  f.MontoPagado.ToString(),
                  f.MontoAdeudado.ToString(),
                  f.PorcentajeVendedor.ToString(),
                  f.CantidadDias.ToString(),
                  f.CantidadDias>f.Cliente.DiasVencimiento?"SI":"NO"
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


        [HttpPost]
        [Route("api/Factura/ReporteCxC")]
        [ActionAuthorize(Action = "ReporteCxC")]
        public IHttpActionResult ReporteCxC([FromBody] BusquedaCxCBasico busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerCxCGeneral(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.ClienteId,
                    busqueda.MonedaId,
                    busqueda.VendedorId,
                    busqueda.Sumarizar
                    );


                Respuesta<CxCDto> respuesta = new Respuesta<CxCDto>(1, "Se ha registrado con exito.", new List<CxCDto> { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpPost]
        [Route("api/Factura/ReporteCxCExcel")]
        [ActionAuthorize(Action = "ReporteCxC")]
        public HttpResponseMessage ReporteCxCExcel([FromUri] BusquedaCxCBasico busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerCxCGeneral(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.ClienteId,
                    busqueda.MonedaId,
                    busqueda.VendedorId
                    );

                var response = base.GenerateExcel(new string[] {
                    "Cliente",
                    "Fecha facturado",
                    "No. Factura",
                    "Facturado por",
                    "Vendedor",
                    "Monto Itbis",
                    "Monto devolución",
                    "Monto NC",
                    "Monto total",
                    "Monto pagado",
                    "Monto adeudado",
                    "Monto vendedor",
                    "Cantidad días",
                    "Vencida"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.INTEGER,
                    ExcelColumnsDef.TEXT },

                resultado.Facturas.Select(f => new string[] {
                  f.Cliente.CodigoYNombre,
                   f.FechaFacturado.HasValue?f.FechaFacturado.Value.ToString("dd-MM-yyyy"):string.Empty,
                  f.NumeroFactura,
                  f.CreadoPor,
                  f.Vendedor?.CodigoYNombre??"",
                  f.MontoItbis.ToString(),
                  f.MontoDevuelto.ToString(),
                  f.MontoNotaCreditoAplicada.ToString(),
                  f.MontoTotal.ToString(),
                  f.MontoPagado.ToString(),
                  f.MontoAdeudado.ToString(),
                  f.PorcentajeVendedor.ToString(),
                  f.CantidadDias.ToString(),
                  f.CantidadDias>f.Cliente.DiasVencimiento?"SI":"NO"
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


        [HttpPost]
        [ActionAuthorize(Action = "EstadoCuenta")]
        [Route("api/factura/estadoCuenta")]
        public IHttpActionResult estadoCuenta([FromBody] BusquedaCxCBasico busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerEstadoCuenta(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.ClienteId,
                    busqueda.MonedaId
                    );
                var facturas = resultado.Where(r => r.Concepto.Contains("FAC")).OrderBy(f=>f.Fecha).ToList();
                List<ResultadoEstadoCuenta> resultadoEstadoCuentas = new List<ResultadoEstadoCuenta>();
                facturas.ToList().ForEach(f => {
                    var pagos = resultado.Where(p => p.Referencia == f.Concepto).OrderBy(p => p.Fecha).ToList();
                    resultadoEstadoCuentas.Add(new ResultadoEstadoCuenta() {Factura=f, MontoComprado=f.Monto, MontoPagado=pagos.Sum(p=>p.Monto), Pagos=pagos });
                });
                                   
                Respuesta<ResultadoEstadoCuenta> respuesta = 
                    new Respuesta<ResultadoEstadoCuenta>(1, "Se ha registrado con exito.",
                 resultadoEstadoCuentas);
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpPost]
        [Route("api/factura/estadoCuentaExcel")]
        [ActionAuthorize(Action = "EstadoCuenta")]
        public HttpResponseMessage estadoCuentaExcel([FromUri] BusquedaCxCBasico busqueda)
        {
            try
            {

                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerEstadoCuenta(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.ClienteId,
                    busqueda.MonedaId
                    );
                var facturas = resultado.Where(r => r.Concepto.Contains("FAC")).OrderBy(f => f.Fecha).ToList();
                List<ResultadoEstadoCuenta> resultadoEstadoCuentas = new List<ResultadoEstadoCuenta>();
                facturas.ToList().ForEach(f => {
                    var pagos = resultado.Where(p => p.Referencia == f.Concepto).OrderBy(p=>p.Fecha).ToList();
                    resultadoEstadoCuentas.Add(new ResultadoEstadoCuenta() { Factura = f, MontoComprado = f.Monto, MontoPagado = pagos.Sum(p => p.Monto), Pagos = pagos });
                });

                List<EstadoCuentaCliente> aExcel = new List<EstadoCuentaCliente>();
                resultadoEstadoCuentas.ForEach(r => {
                    aExcel.Add(r.Factura);
                    r.Pagos?.ForEach(p => { aExcel.Add(p); });
                });

                var response = base.GenerateExcel(new string[] {
                    "Concepto",
                    "Fecha",
                    "Monto",
                    "Referencia",
                    "Moneda Nombre",
                    "Tasa Conversion",
                    "Monto Adeudado",
                    "Monto vendedor"
                     }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL  },

                     aExcel.Select(f => new string[] {
                  f.Concepto,
                   f.Fecha.ToString("dd-MM-yyyy"),
                  f.Monto.ToString(),
                  f.Referencia,
                  f.MonedaNombre,
                  f.TasaConversion.ToString(),
                  f.MontoAdeudado.ToString(),
                  f.PorcentajeVendedor.ToString()
     }).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ActionAuthorize(Action = "estadoEmpresa")]
        [Route("api/factura/reporteFinanciero")]
        public IHttpActionResult reporteFinanciero([FromBody] BusquedaCxCBasico busqueda)
        {
            try
            {

                DateTime? inicio = null;
                DateTime? final = null;
                inicio = !string.IsNullOrEmpty(busqueda.FechaInicial) ? Convert.ToDateTime(busqueda.FechaInicial) : inicio;
                final = !string.IsNullOrEmpty(busqueda.FechaFinal) ? Convert.ToDateTime(busqueda.FechaFinal) : final;
                var resultado = estadoEmpresaService.EstadoFinancieroEmpresa(inicio, final);
               
                return Ok(new Respuesta<ResultadoEstadoCuenta>(0, "OK", resultado));

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpPost]
        [Route("api/factura/reporteFinancieroExcel")]
        [ActionAuthorize(Action = "estadoEmpresa")]
        public HttpResponseMessage reporteFinancieroExcel([FromUri] BusquedaCxCBasico busqueda)
        {
            try
            {

                DateTime? inicio = null;
                DateTime? final = null;
                inicio = !string.IsNullOrEmpty(busqueda.FechaInicial) ? Convert.ToDateTime(busqueda.FechaInicial) : inicio;
                final = !string.IsNullOrEmpty(busqueda.FechaFinal) ? Convert.ToDateTime(busqueda.FechaFinal) : final;
                var resultado = estadoEmpresaService.EstadoFinancieroEmpresa(inicio, final);


                List<EstadoCuentaCliente> aExcel = new List<EstadoCuentaCliente>();
                resultado.ForEach(r => {
                    aExcel.AddRange(r.Pagos);
                   
                });

                var response = base.GenerateExcel(new string[] {
                    "Concepto",
                    "Fecha",
                    "Monto",
                    "Referencia",
                    "Moneda Nombre",
                    "Tasa Conversion",
                    "Monto Adeudado",
                     }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL},

                     aExcel.Select(f => new string[] {
                  f.Concepto,
                   f.Fecha.ToString("dd-MM-yyyy"),
                  f.Monto.ToString(),
                  f.Referencia,
                  f.MonedaNombre,
                  f.TasaConversion.ToString(),
                  f.MontoAdeudado.ToString()
     }).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [Route("api/Factura/ReporteAvanzado")]
        [ActionAuthorize(Action = "ReporteAvanzado")]
        public IHttpActionResult ReporteAvanzado(ReporteDeVentasModel busquedaObj)
        {
            try
            {

                string query = "select * from factura where ";
                for (int i = 0; i < busquedaObj.Parametros.Count; i++)
                {
                    query += busquedaObj.Parametros[i].Campo + busquedaObj.Parametros[i].Operador + busquedaObj.Parametros[i].Condiciones +
                       (i == (busquedaObj.Parametros.Count - 1) ? ";" : " AND ");
                }

                CxCDto resultado = new CxCDto();
                resultado.Facturas = base.service.ConsultarPorQuery(query);
                resultado.Total = resultado.Facturas.Sum(x => x.MontoTotal);
                Respuesta<CxCDto> respuesta = new Respuesta<CxCDto>(1, "Se ha registrado con exito.", new List<CxCDto> { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }
        [HttpPost]
        [ActionAuthorize(Action = "leer")]
        [Route("api/factura/BusquedaCompleja")]
        public IHttpActionResult BusquedaCompleja([FromBody] BuquedaCompleja data)
        {
            try
            {
                return Ok(base.BusquedaComplejaGenerica(data));
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Factura>(-1, ex.Message, new List<Factura>() { }));
            }

        }



        [HttpPost]
        [ActionAuthorize(Action = "reporteImpuestos")]
        [Route("api/factura/reporteImpuestos")]
        public IHttpActionResult reporteImpuestos([FromBody] BusquedaCxCBasico busqueda)
        {
            try
            {

                DateTime? inicio = null;
                DateTime? final = null;
                inicio = !string.IsNullOrEmpty(busqueda.FechaInicial) ? Convert.ToDateTime(busqueda.FechaInicial) : inicio;
                final = !string.IsNullOrEmpty(busqueda.FechaFinal) ? Convert.ToDateTime(busqueda.FechaFinal) : final;
                var resultado = internalService.ReporteDeImpuestosDeFacturas(inicio, final);
                List<ResultadoReporteImpuestos> resultadoReporte = new List<ResultadoReporteImpuestos>();
                resultado.GroupBy(x => x.MonedaId).ToList().ForEach(r => 
                {
                    resultadoReporte.Add(new ResultadoReporteImpuestos()
                    {
                        Moneda=r.FirstOrDefault().Moneda,
                        ImpuestosPagos=r.Where(x=>x.MontoImpuesto<0).ToList(),
                        ImpuestosVentas=r.Where(x=>x.MontoImpuesto>=0).ToList(),
                        MontoTotalPagos= r.Where(x => x.MontoImpuesto < 0).Sum(i=>i.MontoImpuesto),
                        MontoTotalVentas= r.Where(x => x.MontoImpuesto >= 0).Sum(i => i.MontoImpuesto)
                    });
                });

                return Ok(new Respuesta<ResultadoReporteImpuestos>(0, "OK", resultadoReporte));

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpPost]
        [Route("api/factura/reporteImpuestosExcel")]
        [ActionAuthorize(Action = "reporteImpuestos")]
        public HttpResponseMessage reporteImpuestosExcel([FromUri] BusquedaCxCBasico busqueda)
        {
            try
            {

                DateTime? inicio = null;
                DateTime? final = null;
                inicio = !string.IsNullOrEmpty(busqueda.FechaInicial) ? Convert.ToDateTime(busqueda.FechaInicial) : inicio;
                final = !string.IsNullOrEmpty(busqueda.FechaFinal) ? Convert.ToDateTime(busqueda.FechaFinal) : final;
                var resultado = internalService.ReporteDeImpuestosDeFacturas(inicio, final);


               

                var response = base.GenerateExcel(new string[] {
                    "Numero Factura",
                    "Fecha",
                    "Impuesto",
                    "Monto",
                    "Moneda Nombre",
                    "Tasa Conversion",
                     }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL},

                     resultado.Select(f => new string[] {
                  f.NoFactura,
                   f.Fecha.ToString("dd-MM-yyyy"),
                  f.Impuesto.Nombre,
                  f.MontoImpuesto.ToString(),
                  f.Moneda.Nombre,
                  f.Moneda.Tasa.ToString()
     }).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

    }
}


