﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Permisos")]
    [Route("api/seccionesoperaciones")]
    public class SeccionesOperacionesController : IdentityBaseController<ISeccionOperacionesService,SeccionOperaciones>
    {
      private ISeccionOperacionesService service;

        public SeccionesOperacionesController():base(new SeccionOperacionService())
        {
            this.service = new SeccionOperacionService();
        }

     
    }
}
