﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Factura")]
    [Route("api/detallefactura")]
    public class DetalleFacturaController : BaseController<IDetalleFacturaService,DetalleFactura>
    {
      private   IDetalleFacturaService service;

        public DetalleFacturaController():base(new DetalleFacturaService())
        {
            this.service = new DetalleFacturaService();
        }

      
    }
}
