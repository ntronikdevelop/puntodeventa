﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    public abstract class IdentityBaseController<TService, T> : ApiController
        where T : class
        where TService : IProcessCrud<T>
    {
        private ModelFactory _modelFactory;
        private ApplicationUserManager _AppUserManager = null;
        public IProcessCrud<T> service;
        protected string _module;


        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public IdentityBaseController(TService newService ) : base()
        {
            this.service = newService;
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.AppUserManager);
                }
                return _modelFactory;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
       
        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        public virtual IHttpActionResult Get()
        {
            try
            {
                var request = Request;
                var algo = request.Headers;
                var lista = service.ObtenerTodos();
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", lista);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        [ActionAuthorize(Action = "leer")]
        [HttpGet]
        public virtual IHttpActionResult Get(int id)
        {
            try
            {
                var resultado = service.ObtenerPorId(id);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", new List<T>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }


        [ActionAuthorize(Action = "leer")]
        [HttpGet]
        public virtual IHttpActionResult Get(int page, int size)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerTodosPaginado(page, size,true);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        [ActionAuthorize(Action = "leer")]
        [HttpGet]
        public virtual IHttpActionResult Get(int pagesize, string nombreCampo, string filtro)
        {
            try
            {
                var request = Request;
                var resultado = service.ObtenerListaFiltradaPorCampoYLimitada(pagesize, nombreCampo, filtro);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }
        
        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        public virtual IHttpActionResult Get(int page, int size, bool activos)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerTodosPaginado(page, size, activos);
                Respuesta<T> respuesta = new Respuesta<T>(0, "Consulta generada exitosamente.", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

      
        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public virtual IHttpActionResult Post([FromBody]T toInsert)
        {
            try
            {

               
                var resultado = service.Insertar(toInsert);
                Respuesta<T> respuesta = new Respuesta<T>(1, "Creado exitosamente.", new List<T>());
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        // PUT: api/Cliente/5
       
        [HttpPut]
        [ActionAuthorize(Action = "modificar")]
        public virtual IHttpActionResult Put([FromBody]T toUpdate)
        {
            try
            {

               
                var resultado = service.Actualizar(toUpdate);
                Respuesta<T> respuesta = new Respuesta<T>(resultado ? 1 : -1, resultado ? "Actualizado exitosamente." : "Error al actualizar.", new List<T>());
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }
        [HttpDelete]
        [ActionAuthorize(Action = "borrar")]
        public virtual IHttpActionResult Delete(int idABorrar)
        {
            try
            {
               
                service.Eliminar(idABorrar, Helpers.DtoHelper.GetUserName());
                Respuesta<T> respuesta = new Respuesta<T>(0, "Borrado exitosamente.", new List<T>());
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        [HttpDelete]
        [ActionAuthorize(Action = "borrar")]
        public virtual IHttpActionResult Delete(string idABorrar)
        {
            throw new NotImplementedException();
        }

      

      
    }
}

