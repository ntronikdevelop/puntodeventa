﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Moneda")]
    [Route("api/moneda")]
    public class MonedaController : BaseController<IMonedaService,Moneda>
    {
      private   IMonedaService service;

        public MonedaController():base(new MonedaService())
        {
            this.service = new MonedaService();
        }

     
    }
}
