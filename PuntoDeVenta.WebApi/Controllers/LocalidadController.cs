﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Localidad")]
    [Route("api/localidad")]

    public class LocalidadController : BaseController<ILocalidadService, Localidad>
    {
        private ILocalidadService internalService;
        private IAlmacenProductoService almacenProductos;
        private IAlmacenService almacenes;
        private BaseService<AlmacenProductoReporte> almacenProductosRep = new BaseService<AlmacenProductoReporte>(); 

        public LocalidadController():base(new LocalidadService())
        {
            this.internalService = new LocalidadService();
            this.almacenes = new AlmacenService();
            this.almacenProductos = new AlmacenProductoService();
        }

        [HttpGet]
        [Route("api/localidad/InventarioAlmacenes/{localidadId:int}/{almacenId:int}/{noProductosAgotados:int}")]
        [ActionAuthorize(Action = "InventarioAlmacenes")]
        public IHttpActionResult InventarioAlmacenes(Nullable<int> localidadId, Nullable<int> almacenId, int noProductosAgotados=0)
        {
            try
            {
              
                List<FiltroBusqueda> filtros = new List<FiltroBusqueda>();

                List<Localidad> result = new List<Localidad>();
                List<Almacen> almacenList = new List<Almacen>();
                localidadId = localidadId == 0 ? null : localidadId;
                almacenId = almacenId == 0 ? null : almacenId;
                if (localidadId.HasValue)
                    filtros.Add(new FiltroBusqueda()
                    {
                        Campo = "LocalidadId",
                        TipoCampo = 1,
                        Valor = localidadId.Value.ToString()
                    });
                if (almacenId.HasValue)
                    filtros.Add(new FiltroBusqueda()
                    {
                        Campo = "Id",
                        TipoCampo = 1,
                        Valor = almacenId.Value.ToString()
                    });
                almacenList = almacenes.ObtenerListaFiltrada(new BuquedaCompleja() {
                        Activo=true,
                        Filtros= filtros
                
                    }).Result;
                for (int i=0;i< almacenList.Count;i++)
                {
                    string query = $"select a.id, a.almacenId, a.cantidad,a.unidadid, a.localidadid,l.nombre as NombreLocalidad,p.Nombre as NombreProducto, " +
                        $"pu.PrecioCompra as ProductoPrecioCompra, pu.PrecioVenta as ProductoPrecioVenta,u.nombre as NombreUnidad, m.nombre as MonedaNombre,p.id as" +
                        $" productoId, m.codigo as MonedaCodigo from almacenproducto a inner join producto p on (a.ProductoId = p.id and p.Activo=1) " +
                        $"inner join unidades u on (a.unidadid = u.id and u.activo=1) inner join productounidadesequivalencia pu on " +
                        $"(a.ProductoId=pu.ProductoId and pu.activo=1)left join moneda m on (p.MonedaId= m.Id and m.activo=1)left join localidad l on" +
                        $" (a.localidadid = l.id and l.activo=1)   where a.almacenId= {almacenList[i].Id} and a.activo= 1 ";
                    query += noProductosAgotados > 0 ? $"and a.cantidad > 0 order by p.nombre" : "";
                    almacenList[i].Productos = almacenProductosRep.ConsultarPorQuery(query).Select(r=>new AlmacenProducto() {
                        Id=r.Id,
                        ProductoId=r.ProductoId,
                        AlmacenId=r.AlmacenId,
                        Activo=true,
                        UnidadId= r.UnidadId,
                        LocalidadId=r.LocalidadId??null,
                        Producto= new Producto() {Nombre=r.NombreProducto, Moneda= new Moneda() { Nombre= r.MonedaNombre, Codigo=r.MonedaCodigo } ,
                        ProductoUnidades= new List<ProductoUnidadesEquivalencia>() { new ProductoUnidadesEquivalencia() { ProductoId=r.ProductoId, PrecioCompra=r.ProductoPrecioCompra, PrecioVenta=r.ProductoPrecioVenta, UnidadId=r.UnidadId } } },
                        Unidad= new Unidades() { Nombre=r.NombreUnidad},
                        Cantidad=r.Cantidad
                    }).ToList();
                }
                result = localidadId.HasValue ? new List<Localidad>() { internalService.ObtenerBasico(localidadId.Value) } :
                    internalService.ObtenerTodosBasicos();

                result.ForEach(r =>
                {
                    r.Almacenes = almacenList.Where(a => a.LocalidadId == r.Id).ToList();
                   
                });


                Respuesta<Localidad> respuesta = new Respuesta<Localidad>(1, "Se ha registrado con exito.", result);
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }

        [HttpGet]
        [Route("api/localidad/InventarioAlmacenesExcel/{localidadId:int}/{almacenId:int}/{noProductosAgotados:int}")]
        [ActionAuthorize(Action = "InventarioAlmacenes")]
        public HttpResponseMessage ReporteVentasExcel([FromUri] Nullable<int> localidadId, Nullable<int> almacenId, int noProductosAgotados=0)
        {
            try
            {
       
                List<FiltroBusqueda> filtros = new List<FiltroBusqueda>();
                List<AlmacenProductoReporte> productos = new List<AlmacenProductoReporte>();
                List<Almacen> almacenList = new List<Almacen>();
                localidadId = localidadId == 0 ? null : localidadId;
                almacenId = almacenId == 0 ? null : almacenId;
                if (localidadId.HasValue)
                    filtros.Add(new FiltroBusqueda()
                    {
                        Campo = "LocalidadId",
                        TipoCampo = 1,
                        Valor = localidadId.Value.ToString()
                    });
                if (almacenId.HasValue)
                    filtros.Add(new FiltroBusqueda()
                    {
                        Campo = "Id",
                        TipoCampo = 1,
                        Valor = almacenId.Value.ToString()
                    });
                almacenList = almacenes.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = filtros

                }).Result;
                for (int i = 0; i < almacenList.Count; i++)
                {
                    string query = $"select a.id, a.almacenId, a.cantidad,a.unidadid, a.localidadid,l.nombre as NombreLocalidad,p.Nombre as NombreProducto, " +
                       $"pu.PrecioCompra as ProductoPrecioCompra, pu.PrecioVenta as ProductoPrecioVenta,u.nombre as NombreUnidad, m.nombre as MonedaNombre,p.id as" +
                       $" productoId, m.codigo as MonedaCodigo from almacenproducto a inner join producto p on (a.ProductoId = p.id and p.Activo=1) " +
                       $"inner join unidades u on (a.unidadid = u.id and u.activo=1) inner join productounidadesequivalencia pu on " +
                       $"(a.ProductoId=pu.ProductoId and pu.activo=1)left join moneda m on (p.MonedaId= m.Id and m.activo=1)left join localidad l on" +
                       $" (a.localidadid = l.id and l.activo=1)   where a.almacenId= {almacenList[i].Id} and a.activo= 1 ";
                    query += noProductosAgotados > 0 ? $"and a.cantidad > 0 order by p.nombre" : "";
                    productos.AddRange(almacenProductosRep.ConsultarPorQuery(query));
                }

                var response = base.GenerateExcel(new string[] {
                    "Almacen",
                    "Producto",
                    "Existencia",
                    "Unidad",
                    "Total Costo",
                    "Total precio",
                    "Moneda",
                    "Localidad",
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT},

               productos.Select(f => new string[] {
                  almacenList.Where(x=>x.Id==f.AlmacenId).FirstOrDefault().Nombre,
                  f.NombreProducto,
                  f.Cantidad.ToString(),
                  f.NombreUnidad,
                  (f.ProductoPrecioCompra * f.Cantidad).ToString(),
                  (f.ProductoPrecioVenta * f.Cantidad).ToString(),
                  f.MonedaCodigo,
                  f.NombreLocalidad }).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [Route("api/localidad/BuscarSoloLocalidad/{localidadId:int}")]
        [ActionAuthorize(Action = "InventarioAlmacenes")]
        public IHttpActionResult BuscarSoloLocalidad(int localidadId)
        {
            try
            {
               
                var resultado = internalService.ObtenerBasico(localidadId);


                Respuesta<Localidad> respuesta = new Respuesta<Localidad>(1, "Se ha registrado con exito.", new List<Localidad>() { resultado});
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }



    }
}
