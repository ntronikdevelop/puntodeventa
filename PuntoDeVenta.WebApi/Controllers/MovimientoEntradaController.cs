﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "MovimientoEntrada")]
    [Route("api/MovimientoEntrada")]

    public class MovimientoEntradaController : BaseController<IMovimientoEntradaService, MovimientoEntrada>
    {
      private IMovimientoEntradaService internalService;

        private IGastoEmpresaService gastoEmpresaService;

        public MovimientoEntradaController():base(new MovimientoEntradaService())
        {
            this.internalService = new MovimientoEntradaService();

            this.gastoEmpresaService = new GastoEmpresaService();
        }
        [HttpPost]
        [Route("api/movimientoEntrada/RegistrarEntradas")]
        [ActionAuthorize(Action = "RegistrarEntradas")]
        public IHttpActionResult RegistrarEntradas(MovimientoEntradaModel toInsert)
        {
            try
            {
               
                Respuesta<MovimientoEntradaModel> respuesta = new Respuesta<MovimientoEntradaModel>();
               
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    string control= string.IsNullOrEmpty(toInsert.Referencia) ? SecuenciasHelper.CrearControlDeEntradasInventario() : toInsert.Referencia;
                    control = control.ToUpper();
                    //GastoEmpresa gastoEmpresa = new GastoEmpresa()
                    //{
                    //    SuplidorId = toInsert.Suplidor.Id,
                    //    Detalles = control,
                    //    LocalidadId = toInsert.Localidad.Id,
                    //    MonedaId = toInsert.Suplidor.MonedaId,
                    //    ReferenciaGasto = SecuenciasHelper.CrearControlDeGastos(),
                    //    MontoTotal= toInsert.MovimientosEntradas.Sum(m=>m.MontoTotal),
                    //    MontoAdeudado= toInsert.MovimientosEntradas.Sum(m => m.MontoTotal),
                    //    MontoItbis= toInsert.MovimientosEntradas.Sum(m => m.MontoItbis),
                    //    Estado='I'
                    //};
                    //gastoEmpresa = DtoHelper.LlenarCamposPorDefectoCreacion(gastoEmpresa) as GastoEmpresa;
                    //gastoEmpresa.MontoGrabado = gastoEmpresa.MontoTotal = gastoEmpresa.MontoItbis;
                    //gastoEmpresa.FechaEmision = gastoEmpresa.FechaCreacion;
                    //gastoEmpresa.NCF = toInsert.NCF??string.Empty;
                    foreach (MovimientoEntrada movimiento in toInsert.MovimientosEntradas)
                    {
                        MovimientoEntrada aguardar = DtoHelper.LlenarCamposPorDefectoCreacion(movimiento) as MovimientoEntrada;
                        aguardar.Referencia = control;
                        aguardar.AlmacenId = toInsert.Almacen.Id;
                        aguardar.SuplidorId = toInsert.Suplidor.Id;
                        aguardar.UnidadId = movimiento.UnidadId;
                        internalService.Insertar(aguardar);
                    }
                   // gastoEmpresaService.Insertar(gastoEmpresa);
                    scope.Complete();
                    respuesta = new Respuesta<MovimientoEntradaModel>(1, "Se ha registrado con exito.", new List<MovimientoEntradaModel>());
                    
                }
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<MovimientoEntradaModel>(-1, ex.Message, new List<MovimientoEntradaModel>()));
            }
        }

        [HttpGet]
        [Route("api/movimientoEntrada/ObtenerEntradasPorReferencia")]
        [ActionAuthorize(Action = "ImprimirEntrada")]
        public IHttpActionResult ObtenerEntradasPorReferencia(string referencia)
        {
            try
            {
               
                Respuesta<MovimientoEntradaModel> respuesta = new Respuesta<MovimientoEntradaModel>();
                var movimientos = internalService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo = "Referencia", TipoCampo = (int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUALSTRING, Valor = referencia } }
                }).Result;


                if (movimientos != null && movimientos.Count > 0)
                {
                    var movimientoTemp = movimientos.FirstOrDefault();
                    respuesta.Data = new List<MovimientoEntradaModel>() { new MovimientoEntradaModel() {
                    Almacen=movimientoTemp.Almacen,
                    Referencia=movimientoTemp.Referencia,
                    Suplidor=movimientoTemp.Suplidor,
                    MovimientosEntradas=movimientos
                } };
                }

                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<MovimientoEntradaModel>(-1, ex.Message, new List<MovimientoEntradaModel>()));
            }
        }


    }
}
