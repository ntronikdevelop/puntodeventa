﻿using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using System.Runtime.Caching;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using System.Collections.Generic;
using System.Transactions;

namespace PuntoDeVenta.WebApi.Controllers
{
    [MVCControllerAuthorize(Controller = "Permisos")]
    [Route("permisos")]
    public class PermisosController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        public JsonResult ObtenerDatosDeUsuarios()
        {
            try
            {
                List<PresentacionPermisosModel> temp = new List<PresentacionPermisosModel>();
                List<PresentacionPermisosModel> resultado = new List<PresentacionPermisosModel>();
                var service = new UsuarioService();
                var usuarios = service.ObtenerTodos();
                usuarios.ForEach(x => { temp.Add(new PresentacionPermisosModel() { UserId = x.Id, NombreUsuario = x.UserName, Permisos = UsersHelper.ObtenerPermisosDeUsuarioDesdeClaimsEnDb(x.Id) }); });
                temp.ForEach(x =>
                {
                    x.Permisos.ForEach(t =>
                    {
                        var aPresentar = new PresentacionPermisosModel() { NombreUsuario = x.NombreUsuario, UserId = x.UserId, Seccion = t.Nombre, Acciones = string.Empty, Permisos = x.Permisos };
                        if (t.Operaciones != null)
                            foreach (string operacion in t.Operaciones)
                                aPresentar.Acciones += operacion + ",";
                        aPresentar.Acciones = aPresentar.Acciones.EndsWith(",") ? aPresentar.Acciones.Remove(aPresentar.Acciones.Length - 1, 1) : aPresentar.Acciones;
                        resultado.Add(aPresentar);
                    });
                });

                return Json(new { permisos = resultado, totalPermisos = resultado.Count, currentPage = 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {  currentPage = 1 }, JsonRequestBehavior.AllowGet);
            }
           

        }

        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        public JsonResult ObtenerObtenerListaUsuarios(string nombre)
        {
            var service = new UsuarioService();
            var usuarios = service.ObtenerListaFiltradaPorCampoYLimitada(10, "username", nombre);

            return Json(new { usuarios = usuarios }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [ActionAuthorize(Action = "Leer")]
        public JsonResult ObtenerObtenerListaSecciones(string nombre)
        {
            var service = new UsuarioService();
            var usuarios = service.ObtenerListaFiltradaPorCampoYLimitada(10, "username", nombre);

            return Json(new { usuarios = usuarios }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [ActionAuthorize(Action = "Crear")]
        public JsonResult GuardarPermisosUsuario(PermisosUsuarioModel model)
        {
            try
            {
              
                Respuesta<MovimientoEntradaModel> respuesta = new Respuesta<MovimientoEntradaModel>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    var permisosAGuardar = PermisosHelper.ConvertirCreacionDePermisosAClaims(model);
                    PermisosHelper.GuardarClaimsDeUsuarioEnDb(model.UsuarioId, permisosAGuardar);
                    scope.Complete();
                    
                }
                return Json(new { status = 0, message = "ok" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {



                return Json(new { status = -1, message = e.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpGet]
        [ActionAuthorize(Action = "borrar")]
        public JsonResult BorrarPermisoAUsuario(string userId, string seccion)
        {
            try
            {
             
                using (TransactionScope scope = new TransactionScope())
                {
                    PermisosHelper.BorrarPermisosDeSeccionAUsuario(userId, seccion);
                    scope.Complete();
                    return Json(new { status = 0, message = "ok" }, JsonRequestBehavior.AllowGet);
                }
                 
            }
            catch (Exception e)
            {
                return Json(new { status = -1, message = e.Message }, JsonRequestBehavior.AllowGet);
            }


        }




    }
}
