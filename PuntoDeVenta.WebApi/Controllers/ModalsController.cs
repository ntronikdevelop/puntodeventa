﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    public class ModalsController : Controller
    {
        // GET: Modals


        [HttpGet]
        public ActionResult ClienteForm()
        {
            return new FilePathResult("/Content/modals/ClienteForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult VendedorForm()
        {
            return new FilePathResult("/Content/modals/VendedoresForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult ZonaForm()
        {
            return new FilePathResult("/Content/modals/ZonaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult UsuarioRolesForm()
        {
            return new FilePathResult("/Content/modals/UsuarioRolesForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult ControlNCFForm()
        {
            return new FilePathResult("/Content/modals/ControlNCFForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult rolesForm()
        {
            return new FilePathResult("/Content/modals/rolesForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult permisosRoleForm()
        {
            return new FilePathResult("/Content/modals/permisosRoleForm.html", "text/html");
        }


        [HttpGet]
        public ActionResult FacturaForm()
        {
            return new FilePathResult("/Content/modals/FacturaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult CotizacionForm()
        {
            return new FilePathResult("/Content/modals/CotizacionForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult DuplicarCotizacion()
        {
            return new FilePathResult("/Content/modals/DuplicarCotizacion.html", "text/html");
        }

        [HttpGet]
        public ActionResult FacturaAntiguaForm()
        {
            return new FilePathResult("/Content/modals/FacturaAntiguaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult GastoForm()
        {
            return new FilePathResult("/Content/modals/GastoForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult GastoAgendaForm()
        {
            return new FilePathResult("/Content/modals/GastoAgendaForm.html", "text/html");
        }


        [HttpGet]
        public ActionResult ProductoForm()
        {
            return new FilePathResult("/Content/modals/ProductoForm.html", "text/html");
        }



        [HttpGet]
        public ActionResult ImpuestoForm()
        {
            return new FilePathResult("/Content/modals/ImpuestoForm.html", "text/html");
        }



        [HttpGet]
        public ActionResult AlmacenForm()
        {
            return new FilePathResult("/Content/modals/AlmacenForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult CajaForm()
        {
            return new FilePathResult("/Content/modals/CajaForm.html", "text/html");
        }


        [HttpGet]
        public ActionResult AperturaCajaForm()
        {
            return new FilePathResult("/Content/modals/AperturaCajaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult CierreCajaForm()
        {
            return new FilePathResult("/Content/modals/CierreCajaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult MonedaForm()
        {
            return new FilePathResult("/Content/modals/MonedaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult BasesProductoForm()
        {
            return new FilePathResult("/Content/modals/BasesProductoForm.html", "text/html");
        }




        [HttpGet]
        public ActionResult PagoForm()
        {
            return new FilePathResult("/Content/modals/PagoForm.html", "text/html");
        }

 

        [HttpGet]
        public ActionResult TarifarioClienteForm()
        {
            return new FilePathResult("/Content/modals/TarifarioClienteForm.html", "text/html");
        }


        [HttpGet]
        public ActionResult DetalleFacturaForm()
        {
            return new FilePathResult("/Content/modals/DetalleFacturaForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult DetalleCotizacionForm()
        {
            return new FilePathResult("/Content/modals/DetalleCotizacionForm.html", "text/html");
        }




        [HttpGet]
        public ActionResult SeccionesForm()
        {
            return new FilePathResult("/Content/modals/SeccionesForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult OperacionesForm()
        {
            return new FilePathResult("/Content/modals/OperacionesForm.html", "text/html");
        
        }

        [HttpGet]
        public ActionResult PermisoForm()
        {
            return new FilePathResult("/Content/modals/PermisoForm.html", "text/html");
           
        }

        [HttpGet]
        public ActionResult SuplidoresForm()
        {
            return new FilePathResult("/Content/modals/SuplidoresForm.html", "text/html");
        
        }

        [HttpGet]
        public ActionResult LocalidadForm()
        {
            return new FilePathResult("/Content/modals/LocalidadForm.html", "text/html");
        }

        [HttpGet]
        public ActionResult UsuarioForm()
        {
            return new FilePathResult("/Content/modals/UsuarioForm.html", "text/html");
        
        }
        [HttpGet]
        public ActionResult CambiarPassword()
        {
            return new FilePathResult("/Content/modals/CambiarContrasena.html", "text/html");

        }


        [HttpGet]
        public ActionResult MovimientoEntradaForm()
        {
            return new FilePathResult("/Content/modals/MovimientoEntradaForm.html", "text/html");
        
        }

        [HttpGet]
        public ActionResult MovimientoDevolucionForm()
        {
            return new FilePathResult("/Content/modals/MovimientoDevolucionForm.html", "text/html");

        }

        [HttpGet]
        public ActionResult TrasferenciaAlmacenForm()
        {
            return new FilePathResult("/Content/modals/TransferenciaAlmacenesForm.html", "text/html");
       
        }

        [HttpGet]
        public ActionResult DevolucionClienteForm()
        {
            return new FilePathResult("/Content/modals/MovimientoDevolucionClienteForm.html", "text/html");
           
        }

        [HttpGet]
        public ActionResult UnidadesForm()
        {
            return new FilePathResult("/Content/modals/UnidadesForm.html", "text/html");
       
        }

        [HttpGet]
        public ActionResult ProductoUnidadForm()
        {
            return new FilePathResult("/Content/modals/ProductoUnidadForm.html", "text/html");
          
        }

        [HttpGet]
        public ActionResult ProductoCostoForm()
        {
            return new FilePathResult("/Content/modals/ProductoCostoForm.html", "text/html");

        }

        [HttpGet]
        public ActionResult PagoGastoForm()
        {
            return new FilePathResult("/Content/modals/PagoGastoForm.html", "text/html");

        }



        [HttpGet]
        public ActionResult ImprimirFactura(int id)
        {
            ViewBag.IdFactura = id;
            return View();
        }


    }
}