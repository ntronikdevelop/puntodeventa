﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using System.Runtime.Caching;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dto.Identity;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Usuarios")]
    [Route("api/usuarios")]
    public class UsuariosController : IdentityBaseController<IusuarioService, DataAccess.Dto.Users>
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private IusuarioService usuarioService;
        private IProcessCrud<Roles> rolesService;
        private ICajaService cajaService;
        private ILocalidadService localidadService;
        private ISeccionOperacionesService seccionOperacionesService;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public UsuariosController():base(new UsuarioService())
        {
            this.usuarioService = new UsuarioService();
            this.seccionOperacionesService = new SeccionOperacionService();
            this.cajaService = new CajaService();
            this.localidadService = new LocalidadService();
        }
        [ActionAuthorize(Action = "crear")]
        [HttpPost]
        public override  IHttpActionResult Post([FromBody] DataAccess.Dto.Users toInsert)
        {
            try
            {
               
                var user = new Models.Users
                {
                    UserName = toInsert.UserName,
                    Email = toInsert.Email,
                    Nombre = toInsert.Nombre,
                    Apellido = toInsert.Apellido,
                    Cedula = toInsert.Cedula,
                    Activo= toInsert.Activo,
                    LocalidadId= toInsert.LocalidadId,
                    TiempoAperturaCajaHoras=toInsert.TiempoAperturaCajaHoras,
                    AlmacenId=toInsert.AlmacenId,
                    CajaId=toInsert.CajaId??null
                };
                var result = UserManager.Create(user, toInsert.Clave);

                if (result.Succeeded)
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(1, "Creado exitosamente.", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);

                }
                else
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(-1, result.Errors.FirstOrDefault(), new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }

         
        }




        [ActionAuthorize(Action = "borrar")]
        [HttpDelete]
        public override IHttpActionResult Delete(string idABorrar)
        {
            try
            {
               
                var currentUser = UserManager.FindById(idABorrar);
                currentUser.Activo = 0;
                var result = UserManager.Update(currentUser);

                if (result.Succeeded)
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(1, "Borrado exitosamente.", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);

                }
                else
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(-1, result.Errors.FirstOrDefault(), new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "borrar")]
        [Route("api/usuarios/BorrarUsuario/{idAborrar}")]
        public  IHttpActionResult BorrarUsuario(string idABorrar)
        {
            try
            {
               
                var currentUser = UserManager.FindById(idABorrar);
                if (currentUser.UserName.ToLower() == "admin")
                    throw new Exception("EL administrador no se puede eliminar.");
                currentUser.Activo = 0;
                var result = UserManager.Update(currentUser);

                if (result.Succeeded)
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(1, "ok", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);

                }
                else
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(-1, result.Errors.FirstOrDefault(), new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        [Route("api/usuarios/obtenerDatosAdicionales/{id}")]
        public IHttpActionResult obtenerDatosAdicionales(string id)
        {
            Respuesta<DataAccess.Dto.Users> respuesta;
            try
            {

                var currentUser = usuarioService.ObtenerPorId(id);
                if (currentUser == null)
                {
                     respuesta = new Respuesta<DataAccess.Dto.Users>(-1, "Usuario no encontrado", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
                currentUser.Localidad = localidadService.ObtenerPorIdBasico(currentUser.LocalidadId);
                currentUser.Caja = cajaService.ObtenerPorId(currentUser.CajaId.Value);

                
                   respuesta = new Respuesta<DataAccess.Dto.Users>(1, "ok", new List<DataAccess.Dto.Users>() { currentUser});
                    return Ok(respuesta);

               
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }
        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        [Route("api/usuarios/obtenerUsuariosConCajas")]
        public IHttpActionResult obtenerUsuariosConCajas()
        {
            Respuesta<DataAccess.Dto.Users> respuesta;
            try
            {

                var currentUser = usuarioService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja() {
                    Activo=true,
                    Filtros=new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() {Campo="CajaId", TipoCampo= (int)BusquedaComplejaEnum.GREATER, Valor="0" } }
                });

                if (currentUser == null )
                {
                    respuesta = new Respuesta<DataAccess.Dto.Users>(-1, "Usuarios no encontrado", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
                respuesta = new Respuesta<DataAccess.Dto.Users>(1, "ok",currentUser.Result);
                return Ok(respuesta);


            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "CambiarContrasena")]
        [Route("api/usuarios/CambiarPassword/{id}/{newPassword}")]
        public IHttpActionResult CambiarPassword(string id,string newPassword)
        {
            try
            {
               
                IdentityResult result = new IdentityResult(new string[1] {"Error al procesar la solicitud" }); ;
                var manager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
              var resultado=  manager.UserManager.RemovePassword(id);
                if (resultado.Succeeded)
                {
                result=    manager.UserManager.AddPassword(id,newPassword);
                }

                else
                    throw new Exception(string.Join(",", resultado.Errors.ToArray()));
              

                if (result.Succeeded)
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(1, "ok", new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);

                }
                else
                {
                    Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(-1, result.Errors.FirstOrDefault(), new List<DataAccess.Dto.Users>());
                    return Ok(respuesta);
                }
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }

        
        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        [Route("api/usuario/ObtenerUsuarioPorId")]

        public  IHttpActionResult ObtenerUsuarioPorId(string idString)
        {
            try
            {
               
                var resultado = usuarioService.ObtenerPorId(idString);
                Respuesta<DataAccess.Dto.Users> respuesta = new Respuesta<DataAccess.Dto.Users>(0, "ok", new List<DataAccess.Dto.Users>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }

       
      

    


    }
}
