﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "MovimientoDevolucionCliente")]
    [Route("api/MovimientoDevolucionCliente")]

    public class MovimientoDevolucionClienteController : BaseController<IMovimientoDevolucionClienteService, MovimientoDevolucionCliente>
    {
        private IMovimientoDevolucionClienteService service;

        public MovimientoDevolucionClienteController() : base(new MovimientoDevolucionClienteService())
        {
            this.service = new MovimientoDevolucionClienteService();
        }



        [HttpPost]
        [Route("api/MovimientoDevolucionCliente/IngresarDevolucionCliente")]
        [ActionAuthorize(Action = "IngresarDevolucionCliente")]
        public IHttpActionResult IngresarDevolucionCliente([FromBody] DevolucionCliente toInsert)
        {
            Respuesta<DevolucionCliente> respuesta = new Respuesta<DevolucionCliente>();
            try
            {
               
               
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    string codigoDevolucion = SecuenciasHelper.CrearControlDeDevoluciones();
                    string userName = Helpers.DtoHelper.GetUserName();
                    // toInsert = DtoHelper.LlenarCamposPorDefectoCreacion(toInsert) as T;

                    var resultado = service.AplicarDevolucion(toInsert, userName, codigoDevolucion);
                    respuesta = new Respuesta<DevolucionCliente>(resultado,codigoDevolucion, new List<DevolucionCliente>());
                    scope.Complete();

                }
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<DevolucionClienteModel>(-1, ex.Message, new List<DevolucionClienteModel>()));
            }
        }


        [HttpGet]
        [Route("api/MovimientoDevolucionCliente/ObtenerNotaCredito/{referencia}")]
        [ActionAuthorize(Action = "leer")]
        public IHttpActionResult ObtenerNotaCredito(string referencia)
        {
            Respuesta<NotaCredito> respuesta = new Respuesta<NotaCredito>();
            try
            {
               
                var resultado = service.ObtenerNotaDeCredito(referencia);
                respuesta = new Respuesta<NotaCredito>(0, "OK", new List<NotaCredito>() { resultado });
                return Ok(respuesta);
            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<NotaCredito>(-1, ex.Message, new List<NotaCredito>()));
            }
        }

    }
}
