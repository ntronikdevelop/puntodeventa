﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "PagoGasto")]
    [Route("api/pagoGasto")]
    public class PagoGastoController : BaseController<IPagoGastoService, PagoGasto>
    {


        public PagoGastoController() : base(new PagoGastoService())
        {

        }

        [ActionAuthorize(Action = "AplicarPagos")]
        [Route("api/pagoGasto/AplicarPagos")]
        [HttpPost]
        public IHttpActionResult AplicarPagos([FromBody]PagoGastoModel toInsert)
        {
            List<string> errores = new List<string>();
            List<PagoGasto> pagosARealizar = new List<PagoGasto>();
            Respuesta<PagoGasto> respuesta = new Respuesta<PagoGasto>();

            var option = new TransactionOptions();
            option.IsolationLevel = IsolationLevel.ReadCommitted;
            option.Timeout = TimeSpan.FromMinutes(2);
            try
            {
               
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {

                    string secuencia = SecuenciasHelper.CrearControlDePagoGastos();
                    foreach (var detalle in toInsert.DetallesPago)
                    {
                        pagosARealizar.Add(new PagoGasto(
                            toInsert.SuplidorId,
                            toInsert.MonedaId,
                            detalle.MonedaId,
                            toInsert.TipoPagoId,
                            detalle.MontoTotal,
                            detalle.MontoAPagar,
                            detalle.ReferenciaGasto,
                            1,
                            (detalle.MontoTotal - detalle.MontoAPagar),
                            secuencia, // secuencia
                            toInsert.Detalles,
                            DateTime.Now,
                            string.Empty,//creadoPor
                            true,
                            detalle.MontoAdeudado
                            ));
                    }
                    foreach (var pago in pagosARealizar)
                    {

                        try
                        {


                            PagoGasto insertar = DtoHelper.LlenarCamposPorDefectoCreacion(pago) as PagoGasto;
                            var resultado = service.Insertar(insertar);
                            respuesta = new Respuesta<PagoGasto>(resultado.Id, "Se ha registrado con exito.", new List<PagoGasto>(), secuencia);



                        }
                        catch (Exception ex)
                        {
                            errores.Add(ex.Message);

                        }

                    }
                    if (errores.Count == 0) 
                    scope.Complete();
                    scope.Dispose();
                }




                if (errores.Count > 0)
                    throw new Exception(string.Join(",", errores));
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Pago>(-1, ex.Message, new List<Pago>()));
            }
        }

        
        [Route("api/pagoGasto/ObtenerPorSecuencia")]
        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        public IHttpActionResult ObtenerPorSecuencia(string secuencia)
        {

            Respuesta<PagoGasto> respuesta = new Respuesta<PagoGasto>();


            try
            {
               
                var result = service.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo = "Secuencia", TipoCampo = (int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUALSTRING, Valor = secuencia } }
                }).Result;
                respuesta = new Respuesta<PagoGasto>(0, "OK", result);
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<PagoGasto>(-1, ex.Message, new List<PagoGasto>()));
            }
        }

    }
}
