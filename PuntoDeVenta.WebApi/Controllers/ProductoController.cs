﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.Service.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "producto")]
    [Route("api/producto")]
    public class ProductoController : BaseController<IProductoService, Producto>
    {
        private IProductoService service;
        IProductoImpuestoService productoImpuestoService;
        private IBaseServicioCompuestoService basesService;

        public ProductoController() : base(new ProductoService())
        {
            this.productoImpuestoService = new ProductoImpuestoService();
            this.service = new ProductoService();
            this.basesService = new BaseServicioCompuestoService();
        }

        [ActionAuthorize(Action = "LeerPorFiltro")]
        [HttpGet]
        public virtual IHttpActionResult Get(int pagesize, string nombreCampo, string nombreCampo2, bool esServicio, string filtro)
        {
            try
            {
                var request = Request;
                var resultado = service.ObtenerProductosOServiciosFiltradosPorCampoYLimitado(pagesize, nombreCampo, nombreCampo2, filtro, esServicio);
                Respuesta<Producto> respuesta = new Respuesta<Producto>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Producto>(-1, ex.Message, new List<Producto>()));
            }
        }

        [ActionAuthorize(Action = "LeerPorFiltro")]
        [HttpGet]
        public virtual IHttpActionResult Get(int pagesize, string nombreCampo, string nombreCampo2, string filtro)
        {
            try
            {
                var request = Request;
                var resultado = service.ObtenerProductosYServiciosFiltradosPorCampoYLimitado(pagesize, nombreCampo, nombreCampo2, filtro);
                Respuesta<Producto> respuesta = new Respuesta<Producto>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Producto>(-1, ex.Message, new List<Producto>()));
            }
        }

        [ActionAuthorize(Action = "LeerProductoEnLocalidad")]
        [HttpGet]
        [Route("api/producto/cargarCantidadDeProductoLocalidad/{productoId:int}")]
        public virtual IHttpActionResult cargarCantidadDeProductoLocalidad(int productoId)
        {
            try
            {

                IProductosEnLocalidadService servicio = new ProductoEnLocalidadService();
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                var resultado = servicio.ObtenerExistenciaDeProductoEnLocalidad(usuario.LocalidadId, productoId);
                Respuesta<ProductosEnLocalidad> respuesta = new Respuesta<ProductosEnLocalidad>(0, "ok", new List<ProductosEnLocalidad>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Producto>(-1, ex.Message, new List<Producto>()));
            }

        }
        [ActionAuthorize(Action = "Leer")]
        [HttpGet]
        [Route("api/producto/ObtenerImpuestosDeProducto/{productoId:int}")]
        public virtual IHttpActionResult ObtenerImpuestosDeProducto(int productoId)
        {
            try
            {

                
                var resultado = productoImpuestoService.ObtenerImpuestosDeProducto( productoId);
                Respuesta<ProductoImpuesto> respuesta = new Respuesta<ProductoImpuesto>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<ProductoImpuesto>(-1, ex.Message, new List<ProductoImpuesto>()));
            }

        }

        [HttpGet]
        [ActionAuthorize(Action = "LeerBasesServicioCompuesto")]
        [Route("api/producto/cargarBasesServicioCompuesto/{productoId:int}")]
        public virtual IHttpActionResult cargarBasesServicioCompuesto(int productoId)
        {
            try
            {
               // base.IsAuthorized();
                var resultado = basesService.ObtenerBasesDeProducto(productoId);
                Respuesta<BaseServicioCompuesto> respuesta = new Respuesta<BaseServicioCompuesto>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<Producto>(-1, ex.Message, new List<Producto>()));
            }
        }

        [HttpPost]
        [ActionAuthorize(Action = "MovimientosProducto")]
        [Route("api/Producto/MovimientosProducto")]
        public IHttpActionResult MovimientosProducto(BusquedaMovimientosProductoBasico busquedaObj)
        {

            ReporteMovimientosProducto result = new ReporteMovimientosProducto() { ReportePorAlmacen = new List<ReportePorAlmacen>() };
            try
            {
                var movimientos = new MovimientoAlmacenService();
                try
                {
                    service.ConsultarPorQuery($"CALL `ObtenerAcumulativoMovimientosAlmacen`();");
                }
                catch(Exception ex)
                {

                }
                List<MovimientoProductoModel> resultado = new List<MovimientoProductoModel>();
                resultado = movimientos.ObtenerMovimientosPorProducto(busquedaObj.ProductoId, busquedaObj.AlmacenId, busquedaObj.FechaInicial, busquedaObj.FechaFinal);
                var resultadoPorAlmacen = resultado.GroupBy(x => x.CodigoAlmacen);
                foreach (var almacen in resultadoPorAlmacen)
                {
                    result.ReportePorAlmacen.Add(new ReportePorAlmacen()
                    {
                        AlmacenNombre = almacen.FirstOrDefault().AlmacenNombre,
                        cantidadExistente = almacen.Sum(x => x.Cantidad),
                        MovimientosPorProducto = almacen.GroupBy(x => x.ProductoNombre).ToList()
                    });
                }
                Respuesta<ReporteMovimientosProducto> respuesta = new Respuesta<ReporteMovimientosProducto>(0, "OK", new List<ReporteMovimientosProducto>() { result });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }
        [HttpPost]
        [Route("api/Producto/MovimientosProductoExcel")]
        [ActionAuthorize(Action = "MovimientosProducto")]
        public HttpResponseMessage MovimientosProductoExcel([FromUri] BusquedaMovimientosProductoBasico busquedaObj)
        {
            try
            {
                var diaHoy = DateTime.Now;
                var movimientos = new MovimientoAlmacenService();

                List<MovimientoProductoModel> resultado = new List<MovimientoProductoModel>();
                try
                {
                    service.ConsultarPorQuery($"CALL `ObtenerAcumulativoMovimientosAlmacen`();");
                }
                catch { }
                resultado = movimientos.ObtenerMovimientosPorProducto(busquedaObj.ProductoId, busquedaObj.AlmacenId, busquedaObj.FechaInicial, busquedaObj.FechaFinal);
                var response = base.GenerateExcel(new string[] {
                    "Producto",
                    "Fecha movimiento",
                    "Referencia",
                    "Cantidad",
                    "Unidad",
                    "Almacen",
                    "Tipo de movimiento"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT},

                resultado.Select(f => new string[] {
                  f.ProductoNombre,
                  f.FechaCreacion.ToString("dd-MM-yyyy"),
                  f.Referencia,
                  f.Cantidad.ToString(),
                  f.UnidadNombre,
                  f.AlmacenNombre,
                 f.TipoMovimiento.ToLower()=="in"?"ENTRADA":"SALIDA"
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }



        [HttpPost]
        [Route("api/Producto/productosFiltrados")]
        [ActionAuthorize(Action = "productosFiltrados")]
        public Respuesta<Producto> ProductosFiltrados([FromBody] BuquedaCompleja data)
        {
            return base.BusquedaComplejaGenericaSinPaginado(data);
        }

        [HttpGet]
        [Route("api/Producto/productosFiltradosExcel/{campo}/{tipoCampo:int}/{valor}")]
        [ActionAuthorize(Action = "productosFiltrados")]
        public HttpResponseMessage productosFiltradosExcel(string campo, int tipoCampo, string valor)
        {
            try
            {
                var diaHoy = DateTime.Now;
                var movimientos = new MovimientoAlmacenService();

                var resultado = base.BusquedaComplejaGenericaSinPaginado(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda(){ Campo=campo=="^^"?string.Empty:campo, TipoCampo=tipoCampo, Valor=valor=="^^"?string.Empty:valor }
                }
                });

                var response = base.GenerateExcel(new string[] {
                    "Producto",
                    "Descripcion",
                    "Precio",
                    "Precio 2",
                    "Precio 3",
                    "Costo",
                    "Moneda"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT},

                resultado.PagedData.Result.Select(f => new string[] {
                  f.Nombre,
                  f.Descripcion,
                  f.Precio.ToString(),
                  f.Precio2.ToString(),
                  f.Precio3.ToString(),
                  f.Costo.ToString(),
                  f.Moneda.Codigo
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
