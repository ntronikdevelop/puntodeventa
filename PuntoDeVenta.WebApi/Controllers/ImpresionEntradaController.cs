﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    public class ImpresionEntradaController : Controller
    {
        // GET: ImpresionEntrada
        public ActionResult Index(string id)
        {
            ViewBag.movimientoId = id;
            return View();
        }

        public ActionResult ImprimirTransferencia(string id)
        {
            ViewBag.movimientoId = id;
            return View();
        }

        public ActionResult ImprimirDevolucion(string id)
        {
            ViewBag.movimientoId = id;
            return View();
        }

        public ActionResult ImprimirRecibo(string secuencia)
        {
            ViewBag.secuencia = secuencia;
            return View();
        }
        public ActionResult ImprimirReciboPOS(string secuencia)
        {
            ViewBag.secuencia = secuencia;
            return View();
        }

        public ActionResult ImprimirReciboGasto(string secuencia)
        {
            ViewBag.secuencia = secuencia;
            return View();
        }

        public ActionResult ImprimirNotaCredito(string referencia)
        {
            ViewBag.notaCreditoReferencia = referencia;
            return View();
        }

        public ActionResult ImprimirGasto(string id)
        {
            ViewBag.gastoEntrada = id;
            return View();
        }


    }
}