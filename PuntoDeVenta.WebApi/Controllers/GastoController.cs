﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;
using System.Transactions;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Gasto")]
    [Route("api/gasto")]
    public class GastoController : BaseController<IGastoEmpresaService,GastoEmpresa>
    {
      private   IGastoEmpresaService internalService;

        public GastoController():base(new GastoEmpresaService())
        {
            this.internalService = new GastoEmpresaService();
        }

     
        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public override IHttpActionResult Post([FromBody] GastoEmpresa toInsert)
        {
            Respuesta<GastoEmpresa> respuesta = new Respuesta<GastoEmpresa>();
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            toInsert.Localidad = localidadService.ObtenerPorId( usuario.LocalidadId);
            toInsert.LocalidadId = usuario.LocalidadId;

            return base.Post(toInsert);


         
        }

       

       
        [HttpPut]
        [ActionAuthorize(Action = "modificar")]
        public override IHttpActionResult Put([FromBody] GastoEmpresa toInsert)
        {
            ILocalidadService localidadService = new LocalidadService();
            var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
            toInsert.Localidad = localidadService.ObtenerPorId(usuario.LocalidadId);
            toInsert.LocalidadId = usuario.LocalidadId;
            return base.Put(toInsert);
        }

       

        [HttpPost]
        [Route("api/Gasto/ReporteCxP")]
        [ActionAuthorize(Action = "ReporteCxP")]
        public IHttpActionResult ReporteCxP([FromBody] BusquedaCxPBasico busqueda)
        {
            try
            {
              
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerCxPGeneral(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.SuplidorId,
                    busqueda.MonedaId,
                    usuario.LocalidadId
                    );


                Respuesta<CxPDto> respuesta = new Respuesta<CxPDto>(1, "Se ha registrado con exito.", new List<CxPDto> { resultado });
                return Ok(respuesta);

            }

            catch (Exception ex)
            {
                return Ok(new Respuesta<TransferenciaAlmacenModel>(-1, ex.Message, new List<TransferenciaAlmacenModel>()));
            }
        }
        [HttpPost]
        [Route("api/Gasto/ReporteCxPExcel")]
        [ActionAuthorize(Action = "ReporteCxP")]
        public HttpResponseMessage ReporteCxPExcel([FromUri]  BusquedaCxPBasico busqueda)
        {
            try
            {
            
                var usuario = CacheHelper.ObtenerObjetoEnCache<Models.Users>(System.Web.HttpContext.Current.User.Identity.GetUserId());
                var diaHoy = DateTime.Now;
                var resultado = internalService.ObtenerCxPGeneral(
                   (string.IsNullOrEmpty(busqueda.FechaInicial)) ? DateTime.MinValue : Convert.ToDateTime(busqueda.FechaInicial),
                    (string.IsNullOrEmpty(busqueda.FechaFinal)) ? diaHoy : Convert.ToDateTime(busqueda.FechaFinal),
                    busqueda.SuplidorId,
                    busqueda.MonedaId,
                    usuario.LocalidadId
                    );

                var response = base.GenerateExcel(new string[] {
                    "Suplidor",
                    "Fecha",
                    "Referencia",
                    "Moneda",
                    "Monto total",
                    "Monto pagado",
                    "Monto adeudado",
                    "Cantidad de dias",
                    "Detalles"
                }, new ExcelColumnsDef[] {
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DATETIME,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.TEXT,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.DECIMAL,
                    ExcelColumnsDef.TEXT },

                resultado.Gastos.Select(f => new string[] {
                  f.Suplidor.Nombre,
                  f.FechaEmision.ToString("dd-MM-yyyy"),
                  f.ReferenciaGasto,
                  f.Moneda.Codigo,
                  f.MontoTotal.ToString(),
                  f.MontoPagado.ToString(),
                  f.MontoAdeudado.ToString(),
                  f.CantidadDias.ToString(),
                  f.Detalles
}).ToList());
                return response;
            }

            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


        [HttpPost]
        [ActionAuthorize(Action = "leer")]
        [Route("api/Gasto/BusquedaCompleja")]
        public  IHttpActionResult BusquedaCompleja([FromBody] BuquedaCompleja data)
        {
            try
            {
                return Ok(base.BusquedaComplejaGenerica(data));
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<GastoEmpresa>(-1, ex.Message, new List<GastoEmpresa>() { }));
            }
           
        }



    }
}


