﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    [Route("api/tarifariocliente")]
    public class TarifarioClienteController : BaseController<ITarifarioClienteService,TarifarioCliente>
    {
        private ITarifarioClienteService service;

        public TarifarioClienteController(): base(new TarifarioClienteService())
        {
            this.service = new TarifarioClienteService();
        }


    }
}
