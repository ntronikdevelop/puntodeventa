﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.WebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using PuntoDeVenta.Service.Helpers;
using System.Net.Http.Headers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    public abstract class BaseController<TService, T> : ApiController
        where T : class, IDatosComunes
        where TService : IProcessCrud<T>
    {
        private ModelFactory _modelFactory;
        private ApplicationUserManager _AppUserManager = null;
        protected IProcessCrud<T> service;
        protected  string _module;


        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public BaseController(TService newService) : base()
        {
            this.service = newService;
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.AppUserManager);
                }
                return _modelFactory;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
       
        [HttpGet]
        [ActionAuthorize(Action = "LeerTodos")]
        public virtual IHttpActionResult Get()
        {
            try
            {
                var request = Request;
                var algo = request.Headers;
                var lista = service.ObtenerTodos();
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", lista);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

       
        [HttpGet]
        [ActionAuthorize(Action = "LeerPorId")]
        public virtual IHttpActionResult Get(int id)
        {
            try
            {
                var resultado = service.ObtenerPorId(id);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", new List<T>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

       
        [HttpGet]
        [ActionAuthorize(Action = "LeerPaginadoBasico")]
        public virtual IHttpActionResult Get(int page, int size)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerTodosPaginado(page, size, true);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }
        [ActionAuthorize(Action = "LeerPaginadoPorActivos")]
        public virtual IHttpActionResult Get(int page, int size, bool activos)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerTodosPaginado(page, size, activos);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        [ActionAuthorize(Action = "LeerPaginadoAvanzado")]
        [HttpPatch]
        public virtual IHttpActionResult Patch([FromBody] PaginadoModel data)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerTodosPaginadoYFiltro(data.Page, data.Size, data.Activo, data.Campo, data.TipoCampo, data.Valor);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }



        protected virtual Respuesta<T> BusquedaComplejaGenerica([FromBody] BuquedaCompleja data)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerListaPaginadoYFiltro(data);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return respuesta;
            }
            catch (Exception ex)
            {
                return new Respuesta<T>(-1, ex.Message, new List<T>());
            }
        }

        protected virtual Respuesta<T> BusquedaComplejaGenericaSinPaginado([FromBody] BuquedaCompleja data)
        {
            try
            {
               
                var request = Request;
                var resultado = service.ObtenerListaFiltrada(data);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return respuesta;
            }
            catch (Exception ex)
            {
                return new Respuesta<T>(-1, ex.Message, new List<T>());
            }
        }

       
        [HttpGet]
        [ActionAuthorize(Action = "LeerPorFiltro")]
        public virtual IHttpActionResult Get(int pagesize, string nombreCampo, string filtro)
        {
            try
            {
                var request = Request;
                var resultado = service.ObtenerListaFiltradaPorCampoYLimitada(pagesize, nombreCampo, filtro);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        
        [HttpGet]
        [ActionAuthorize(Action = "LeerPorFiltro")]
        public virtual IHttpActionResult Get(int pagesize, string nombreCampo, bool activo, string filtro)
        {
            try
            {
                var request = Request;
                var resultado = service.ObtenerListaFiltradaPorCampoYLimitada(pagesize, nombreCampo, activo, filtro);
                Respuesta<T> respuesta = new Respuesta<T>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

      
        // POST: api/Cliente
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public virtual IHttpActionResult Post([FromBody]T toInsert)
        {
            Respuesta<T> respuesta = new Respuesta<T>();
            try
            {
               
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    toInsert = DtoHelper.LlenarCamposPorDefectoCreacion(toInsert) as T;

                    var resultado = service.Insertar(toInsert);
                    respuesta = new Respuesta<T>(resultado.Id, "Se ha registrado con exito.", new List<T>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        // PUT: api/Cliente/5
        [ActionAuthorize(Action = "modificar")]
        [HttpPut]
        public virtual IHttpActionResult Put([FromBody]T toUpdate)
        {
            try
            {
               
                Respuesta<T> respuesta = new Respuesta<T>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    toUpdate = DtoHelper.LlenarCamposPorDefectoEdicion(toUpdate) as T;
                    toUpdate.Detalles = string.Equals(toUpdate.Detalles, string.Empty) ? null : toUpdate.Detalles;
                    var resultado = service.Actualizar(toUpdate);
                    respuesta = new Respuesta<T>(resultado ? toUpdate.Id : -1, resultado ? "se ha actualizado con exito." : "Error al actualizar.", new List<T>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }

        [HttpDelete]
        [ActionAuthorize(Action = "borrar")]
        public virtual IHttpActionResult Delete(int idABorrar)
        {
            try
            {
               
                Respuesta<T> respuesta = new Respuesta<T>();
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    service.Eliminar(idABorrar,Helpers.DtoHelper.GetUserName());
                    respuesta = new Respuesta<T>(0, "Se ha desactivado con exito", new List<T>());
                    scope.Complete();

                }
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<T>(-1, ex.Message, new List<T>()));
            }
        }


        protected HttpResponseMessage GenerateExcel(string[] headers, ExcelColumnsDef[] colDefs, List<string[]> data, string extension = "xls")
        {
            

          //  string filePath = ImportToExcel.CreateXlsFile(headers, colDefs, data, extension);

          //  var fileStream = ImportToExcel.CreateXlsStream(headers, colDefs, data, extension);
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            try
            {
               
                string fileName = $"reporte.xls";
                using (MemoryStream ms = ImportToExcel.CreateXlsStream(headers, colDefs, data, extension))
                {
                        httpResponseMessage.Content = new ByteArrayContent(ms.ToArray());
                        httpResponseMessage.Content.Headers.Add("x-filename", fileName);
                        httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        httpResponseMessage.Content.Headers.ContentDisposition.FileName = fileName;
                        httpResponseMessage.StatusCode = HttpStatusCode.OK;
                        
                    
                    
                }
                return httpResponseMessage;


            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        protected void IsAuthorized()
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(userId))
                throw new Exception("No está autorizado");

            var usuario = CacheHelper.ObtenerObjetoEnCache<Users>(userId);
            if (usuario != null)
            {
                var permisoActual = usuario.Permisos.Where(x => x.Nombre.ToLower() == _module.ToLower() || x.Nombre.ToLower() == "*").FirstOrDefault();
                if (permisoActual == null)
                    throw new Exception("No está autorizado");
            }
            
        }

        
    }
}

