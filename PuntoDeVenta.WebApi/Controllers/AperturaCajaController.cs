﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using System.Transactions;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "AperturaCaja")]
    [Route("api/AperturaCaja")]
    public class AperturaCajaController : BaseController<IAperturaCajaService, AperturaCaja>
    {
        private IPagoService pagoService;
        private IAperturaCajaService service;
        private IusuarioService usuarioService;
        public AperturaCajaController() : base(new AperturaCajaService())
        {
            this.service = new AperturaCajaService();
            this.usuarioService = new UsuarioService();
            this.pagoService = new PagoService();
        }
        [HttpPost]
        [ActionAuthorize(Action = "crear")]
        public override IHttpActionResult Post([FromBody]AperturaCaja toInsert)
        {
            Respuesta<AperturaCaja> respuesta = new Respuesta<AperturaCaja>();
            try
            {

                var currentOpenBoxes = this.service.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() {
                        new DataAccess.FiltroBusqueda(){
                            Campo="UserId",
                            TipoCampo= (int)BusquedaComplejaEnum.EQUALSTRING,
                            Valor=toInsert.UserId
                        },
                        new DataAccess.FiltroBusqueda(){
                            Campo="Estado",
                            TipoCampo= (int)BusquedaComplejaEnum.EQUALSTRING,
                            Valor="A"
                        },
                        new DataAccess.FiltroBusqueda(){
                            Campo="MonedaId",
                            TipoCampo= (int)BusquedaComplejaEnum.EQUAL,
                            Valor=toInsert.MonedaId.ToString()
                        }
                    }
                });

                if (currentOpenBoxes.Result.Count > 0)
                    return Ok(new Respuesta<AperturaCaja>(-1, "El usuario tiene cajas abiertas. Favor cerrar dichas cajas antes de abrir otra nuevamente.", new List<AperturaCaja>()));

                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    toInsert = DataAccess.Dto.Helpers.DtoHelper.LlenarCamposPorDefectoCreacion(toInsert) as AperturaCaja;
                    toInsert.FechaApertura = DateTime.Now;
                    toInsert.FechaCierreMaxima = toInsert.FechaApertura.AddMinutes(Convert.ToDouble(toInsert.Usuario?.TiempoAperturaCajaHoras.Value * 60));
                    toInsert.Estado = 'A';

                    var resultado = service.Insertar(toInsert);
                    respuesta = new Respuesta<AperturaCaja>(resultado.Id, "Se ha registrado con exito.", new List<AperturaCaja>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<AperturaCaja>(-1, ex.Message, new List<AperturaCaja>()));
            }
        }

        [HttpPut]
        [ActionAuthorize(Action = "cerrarCaja")]
        [Route("api/AperturaCaja/CerrarCaja")]
        public IHttpActionResult CerrarCaja([FromBody]AperturaCaja toInsert)
        {
            Respuesta<AperturaCaja> respuesta = new Respuesta<AperturaCaja>();
            try
            {


                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    toInsert = DataAccess.Dto.Helpers.DtoHelper.LlenarCamposPorDefectoEdicion(toInsert) as AperturaCaja;
                    toInsert.FechaCierre = DateTime.Now;
                    toInsert.Estado = 'C';

                    var resultado = service.Actualizar(toInsert);
                    respuesta = new Respuesta<AperturaCaja>(0, "Se ha cerrado la caja con exito.", new List<AperturaCaja>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<AperturaCaja>(-1, ex.Message, new List<AperturaCaja>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        public override IHttpActionResult Get(int id)
        {
            try
            {
                var resultado = service.ObtenerPorId(id);
                resultado.Usuario = usuarioService.ObtenerPorId(resultado.UserId);
                Respuesta<AperturaCaja> respuesta = new Respuesta<AperturaCaja>(0, "ok", new List<AperturaCaja>() { resultado });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<AperturaCaja>(-1, ex.Message, new List<AperturaCaja>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        [Route("api/AperturaCaja/ObtenerMontoTotalPagos")]
        public IHttpActionResult ObtenerMontoTotalPagos(string userId, int aperturaCajaId)
        {
            try
            {
                var apertura = this.service.ObtenerPorId(aperturaCajaId);
                var result = this.pagoService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() {
                        new DataAccess.FiltroBusqueda(){
                            Campo="CreadoPor",
                            TipoCampo= (int)BusquedaComplejaEnum.EQUALSTRING,
                            Valor=userId
                        },
                        new DataAccess.FiltroBusqueda(){
                            Campo="FechaCreacion",
                            TipoCampo= (int)BusquedaComplejaEnum.GREATEROREQUALDATE,
                            Valor=apertura.FechaApertura.ToString("yyyy-MM-dd HH:mm:ss")
                        },
                        new DataAccess.FiltroBusqueda(){
                            Campo="FechaCreacion",
                            TipoCampo= (int)BusquedaComplejaEnum.LESSOREQUALDATE,
                            Valor=apertura.FechaCierreMaxima.ToString("yyyy-MM-dd HH:mm:ss")
                        },
                    }
                });

                Respuesta<object> respuesta = new Respuesta<object>(0, "ok", new List<object>() { new { montoTotalPagos = result.Result.Sum(x => x.MontoPagado) } });
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<AperturaCaja>(-1, ex.Message, new List<AperturaCaja>()));
            }
        }

    }
}
