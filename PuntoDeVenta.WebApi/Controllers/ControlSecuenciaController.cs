﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    [Route("api/controlfacturas")]
    public class ControlSecuenciaController : BaseController<IControlSecuenciaService, ControlSecuencias>
    {
      private   IControlSecuenciaService service;

        public ControlSecuenciaController():base(new ControlSecuenciaService())
        {
            this.service = new ControlSecuenciaService();
        }



    }
}
