﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [Authorize]
    [Route("api/tipopago")]
    public class TipoPagoController : BaseController<ITipoPagoService, TipoPago>
    {
        private ITipoPagoService service;

        public TipoPagoController() : base(new TipoPagoService())
        {
            this.service = new TipoPagoService();
        }


    }
}
