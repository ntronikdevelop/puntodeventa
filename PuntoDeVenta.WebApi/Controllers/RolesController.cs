﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using System.Runtime.Caching;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dto.Identity;
using System.Web.Http;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "Roles")]
    [Route("api/roles")]
    public class RolesController : IdentityBaseController<IdentityBaseService<Roles>, DataAccess.Dto.Identity.Roles>
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public RolesController() : base(new IdentityBaseService<Roles>())
        {

        }

        [HttpPost]
        [ActionAuthorize(Action = "Crear")]
        [Route("api/roles/GuardarPermisos")]
        public IHttpActionResult GuardarPermisos([FromBody]RoleSeccion toInsert)
        {
            IdentityBaseService<RoleSeccion> roleSeccionService = new IdentityBaseService<RoleSeccion>();
            Respuesta<RoleSeccion> respuesta = new Respuesta<RoleSeccion>();
            try
            {

                toInsert.Activo = true;
                var option = new TransactionOptions();
                option.IsolationLevel = IsolationLevel.ReadCommitted;
                option.Timeout = TimeSpan.FromMinutes(2);
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                {
                    if (toInsert.Id <= 0)
                        roleSeccionService.Insertar(toInsert);
                    else
                        roleSeccionService.Actualizar(toInsert);

                    respuesta = new Respuesta<RoleSeccion>(0, "Se ha registrado los permisos con exito.", new List<RoleSeccion>());
                    scope.Complete();

                }
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<RoleSeccion>(-1, ex.Message, new List<RoleSeccion>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "Crear")]
        [Route("api/roles/permisosDeRole")]
        public IHttpActionResult permisosDeRole(int roleId)
        {
            IdentityBaseService<RoleSeccion> roleSeccionService = new IdentityBaseService<RoleSeccion>();
            Respuesta<RoleSeccion> respuesta = new Respuesta<RoleSeccion>();
            try
            {



                respuesta.PagedData = roleSeccionService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>()
                        {
                            new DataAccess.FiltroBusqueda(){ Campo="RoleId", TipoCampo=(int)BusquedaComplejaEnum.EQUAL, Valor=roleId.ToString()}
                        }
                });
                respuesta.Mensaje = "OK";
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<RoleSeccion>(-1, ex.Message, new List<RoleSeccion>()));
            }
        }

        [HttpGet]
        [ActionAuthorize(Action = "AsignarRoleUsuario")]
        [Route("api/roles/obtenerRolesUsuario/{userId}")]
        public IHttpActionResult obtenerRolesUsuario(string userId)
        {
            Respuesta<Roles> respuesta = new Respuesta<Roles>();
            try
            {

                string query = $"select r.Id,r.Name,r.Activo from roles r inner join userroles ur on r.Id=ur.RoleId where ur.UserId='{userId}' and ur.Activo=0x01";

                respuesta.Data = base.service.ConsultarPorQuery(query);
                respuesta.Mensaje = "OK";
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<RoleSeccion>(-1, ex.Message, new List<RoleSeccion>()));
            }
        }

        [HttpPost]
        [ActionAuthorize(Action = "AsignarRoleUsuario")]
        [Route("api/roles/agregarRoleAUsuario")]
        public IHttpActionResult agregarRoleAUsuario([FromBody] AgregarRoleUsuario agregarRoleUsuario)
        {
            IdentityBaseService<UserRoles> urService = new IdentityBaseService<UserRoles>();
            Respuesta<Roles> respuesta = new Respuesta<Roles>();
            try
            {
                var currentRole = urService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() {Campo="UserId", TipoCampo=(int)BusquedaComplejaEnum.EQUALSTRING, Valor= agregarRoleUsuario.userId },
                    new DataAccess.FiltroBusqueda(){ Campo="RoleId",TipoCampo=(int)BusquedaComplejaEnum.EQUAL, Valor= agregarRoleUsuario.roleId.ToString()}
                    }
                }).Result;
                if (currentRole.Count == 0)
                    urService.Insertar(new UserRoles() { UserId = agregarRoleUsuario.userId, RoleId = agregarRoleUsuario.roleId, Activo = true });

                respuesta.Mensaje = "OK";
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<RoleSeccion>(-1, ex.Message, new List<RoleSeccion>()));
            }
        }

        [HttpDelete]
        [ActionAuthorize(Action = "AsignarRoleUsuario")]
        [Route("api/roles/eliminarRoleDeUsuario/{userId}/{roleId:int}")]
        public IHttpActionResult eliminarRoleDeUsuario(string userId, int roleId)
        {
            IdentityBaseService<UserRoles> urService = new IdentityBaseService<UserRoles>();
            Respuesta<Roles> respuesta = new Respuesta<Roles>();
            try
            {

                string query = $"delete from userroles where UserId='{userId}' and RoleId={roleId}";

                 base.service.ConsultarPorQuery(query);

                respuesta.Mensaje = "OK";
                return Ok(respuesta);

            }
            catch (Exception ex)
            {
                return Ok(new Respuesta<RoleSeccion>(-1, ex.Message, new List<RoleSeccion>()));
            }
        }

    }

    public class AgregarRoleUsuario
    {
        public string userId { get; set; }
        public int roleId { get; set; }
    }
}
