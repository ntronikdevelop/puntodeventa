﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "montosapertura")]
    [Route("api/montosapertura")]
    public class MontosAperturaController : BaseController<IProcessCrud<MontosApertura>,MontosApertura>
    {
      

        public MontosAperturaController():base(new BaseService<MontosApertura>())
        {
            
        }

    
    }
}
