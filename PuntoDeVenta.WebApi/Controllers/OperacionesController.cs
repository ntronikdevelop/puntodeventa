﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "operaciones")]
    [Route("api/operaciones")]
    public class OperacionesController : IdentityBaseController<IOperacionesService,Operaciones>
    {
        private ISeccionOperacionesService seccionOperacionesService;

        public OperacionesController():base(new OperacionesService())
        {
            this.seccionOperacionesService = new SeccionOperacionService();
        }

        [HttpGet]
        [ActionAuthorize(Action = "leer")]
        [Route("api/operaciones/ObtenerOperacionesSeccion")]
        public IHttpActionResult ObtenerOperacionesSeccion(int seccionId)
        {
            try
            {

                var resultado = seccionOperacionesService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<DataAccess.FiltroBusqueda>() { new DataAccess.FiltroBusqueda() { Campo = "SeccionId", TipoCampo = (int)DataAccess.Dto.Helpers.BusquedaComplejaEnum.EQUAL, Valor = seccionId.ToString() } }
                }).Result;
                Respuesta<DataAccess.Dto.SeccionOperaciones> respuesta = new Respuesta<DataAccess.Dto.SeccionOperaciones>(0, "ok", resultado);
                return Ok(respuesta);
            }
            catch (Exception ex)
            {
                return base.Ok(new Respuesta<DataAccess.Dto.Users>(-1, ex.Message, new List<DataAccess.Dto.Users>()));
            }
        }


    }
}
