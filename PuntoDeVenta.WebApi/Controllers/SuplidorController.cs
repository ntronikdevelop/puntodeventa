﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.WebApi.Models;
using PuntoDeVenta.WebApi.Helpers;
using Microsoft.AspNet.Identity;

namespace PuntoDeVenta.WebApi.Controllers
{
    [ControllerAuthorize(Controller = "suplidor")]
    [Route("api/suplidor")]
    public class SuplidorController : BaseController<ISuplidoresService, Suplidores>
    {
        private ISuplidoresService service;

        public SuplidorController():base(new SuplidoresService())
        {
            this.service = new SuplidoresService();
        }
    }
}
