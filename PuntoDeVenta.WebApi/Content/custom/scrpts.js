﻿function mostrarErrores() {

    var elementos = $("input.ng-invalid-autocomplete-required,input.ng-invalid,select.ng-invalid");;
    $(".content-error").html("");

    elementos.each(function (index, element) {
        var _message = element.dataset.error;

        _message = _message == null ? element.parentElement.parentElement.dataset.error : _message;
        if (_message == null) return;
        $(".content-error").append("<span>" + _message + "</span>");
        setTimeout(function () {
            $(".content-error").html("");
        }, 4000)
    });
}