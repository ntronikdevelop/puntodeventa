﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PuntoDeVenta.WebApi.Helpers;
using System.Configuration;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Interfaces;

namespace PuntoDeVenta.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string resultado = MachineKeyHelper.GetMachineKey();
        }

        [TestMethod]
        public void TestMethod2()
        {
            string resultado = EncryptionHelper.Encrypt( MachineKeyHelper.GetMachineKey(), ConfigurationManager.AppSettings["AppIdUnique"]);
        }

        [TestMethod]
        public void TestMethod3()
        {
            string resultado = EncryptionHelper.Decrypt(ConfigurationManager.AppSettings["Algo"], ConfigurationManager.AppSettings["AppIdUnique"]);
        }
    }

    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestCacheGetByExp()
        {
            CachingHelper.InsertarListaDtoEnCache<Factura>( new List<Factura>() {
                new Factura() {Id=1, ClienteId=1, Activo=true, LocalidadId=1, MonedaId=1, MontoTotal=100  },
                new Factura() {Id=2, ClienteId=2, Activo=true, LocalidadId=2, MonedaId=2, MontoTotal=200  },
                new Factura() {Id=3, ClienteId=3, Activo=true, LocalidadId=3, MonedaId=3, MontoTotal=300  },
                new Factura() {Id=4, ClienteId=4, Activo=true, LocalidadId=4, MonedaId=4, MontoTotal=400  },
                new Factura() {Id=5, ClienteId=5, Activo=true, LocalidadId=5, MonedaId=5, MontoTotal=500  },
            });

            //var factura1 = CachingHelper.ObtenerDtoDeCachePorExpresion<Factura>(f => f.Id == 1);
            //var factura2 = CachingHelper.ObtenerDtoDeCachePorExpresion<Factura>(f => f.Id >1 && f.Id<5);
            //var factura3 = CachingHelper.ObtenerDtoDeCachePorExpresion<Factura>(f =>f.MontoTotal >350 && f.MontoTotal<500);
        }


    }
}
