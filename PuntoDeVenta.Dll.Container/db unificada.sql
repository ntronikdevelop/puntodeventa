CREATE DATABASE  IF NOT EXISTS `db_a63aa6_luisa` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_a63aa6_luisa`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: MYSQL5021.site4now.net    Database: db_a63aa6_luisa
-- ------------------------------------------------------
-- Server version	5.6.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `almacen`
--

DROP TABLE IF EXISTS `almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `almacen` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `Nombre` varchar(200) NOT NULL,
  `LocalidadId` int(11) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Codigo_Unique` (`Codigo`),
  KEY `LocalidadId` (`LocalidadId`),
  KEY `AlmacenProducto_Localidad_idx` (`LocalidadId`),
  CONSTRAINT `AlmacenProducto_Localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`),
  CONSTRAINT `almacen_ibfk_1` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacen`
--

LOCK TABLES `almacen` WRITE;
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;
INSERT INTO `almacen` VALUES (1,'Administrador ss','Luis Alberto','2019-08-13 06:20:34','2020-07-09 17:21:16',_binary '','DEFECTUOSOS',1,'DEF'),(2,'Administrador ss','Administrador ss','2019-08-13 06:20:49','2020-07-10 01:24:33',_binary '','ALMACEN PRINCIPAL',1,'ALP'),(3,'Usuario Marpos RD usuario','Usuario Marpos RD usuario','2019-08-20 08:14:56','0001-01-01 00:00:00',_binary '\0','shampoo keratin 33.oz',1,'3001R');
/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacenproducto`
--

DROP TABLE IF EXISTS `almacenproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `almacenproducto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(10) unsigned NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  `LocalidadId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  KEY `almacenproducto_localidad_idx` (`LocalidadId`),
  CONSTRAINT `almacenproducto_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `almacenproducto_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `almacenproducto_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacenproducto`
--

LOCK TABLES `almacenproducto` WRITE;
/*!40000 ALTER TABLE `almacenproducto` DISABLE KEYS */;
/*!40000 ALTER TABLE `almacenproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacenproductodefectuoso`
--

DROP TABLE IF EXISTS `almacenproductodefectuoso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `almacenproductodefectuoso` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(10) unsigned NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `almacenproductodefectuoso_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `almacenproductodefectuoso_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `almacenproductodefectuoso_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacenproductodefectuoso`
--

LOCK TABLES `almacenproductodefectuoso` WRITE;
/*!40000 ALTER TABLE `almacenproductodefectuoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `almacenproductodefectuoso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aperturacaja`
--

DROP TABLE IF EXISTS `aperturacaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aperturacaja` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NombreUsuario` varchar(200) NOT NULL,
  `UserId` varchar(128) NOT NULL,
  `LocalidadId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `CajaId` int(11) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaApertura` datetime NOT NULL,
  `FechaCierre` datetime NOT NULL,
  `FechaCierreMaxima` datetime NOT NULL,
  `DetallesApertura` longtext NOT NULL,
  `DetallesCierre` longtext NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `MontoAperturaTotal` decimal(18,2) NOT NULL,
  `MontoCierreTotal` decimal(18,2) NOT NULL,
  `DiferenciaAperturaCierre` decimal(18,2) NOT NULL,
  `MontoTotalPagos` decimal(18,2) NOT NULL,
  `Estado` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `aperturacaja_Localidad_idx` (`LocalidadId`),
  KEY `aperturacaja_caja_idx` (`CajaId`),
  KEY `aperturacaja_moneda_idx` (`MonedaId`),
  CONSTRAINT `aperturacaja_caja` FOREIGN KEY (`CajaId`) REFERENCES `cajaregistradora` (`Id`),
  CONSTRAINT `aperturacaja_localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`),
  CONSTRAINT `aperturacaja_moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aperturacaja`
--

LOCK TABLES `aperturacaja` WRITE;
/*!40000 ALTER TABLE `aperturacaja` DISABLE KEYS */;
/*!40000 ALTER TABLE `aperturacaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancecliente`
--

DROP TABLE IF EXISTS `balancecliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `balancecliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MontoAdeudado` decimal(18,2) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Factura_Moneda_Balance` (`MonedaId`),
  KEY `Factura_Cliente_Balance` (`ClienteId`),
  CONSTRAINT `Factura_Cliente_Balance` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `Factura_Moneda_Balance` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancecliente`
--

LOCK TABLES `balancecliente` WRITE;
/*!40000 ALTER TABLE `balancecliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `balancecliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancesuplidores`
--

DROP TABLE IF EXISTS `balancesuplidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `balancesuplidores` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Factura_Moneda_Balance` (`MonedaId`),
  KEY `Factura_Suplidor_Balance` (`SuplidorId`),
  CONSTRAINT `Factura_Moneda_Balance1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Factura_Suplidor_Balance1` FOREIGN KEY (`SuplidorId`) REFERENCES `cliente` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancesuplidores`
--

LOCK TABLES `balancesuplidores` WRITE;
/*!40000 ALTER TABLE `balancesuplidores` DISABLE KEYS */;
/*!40000 ALTER TABLE `balancesuplidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baseserviciocompuesto`
--

DROP TABLE IF EXISTS `baseserviciocompuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `baseserviciocompuesto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoCompuestoId` int(10) unsigned NOT NULL,
  `ProductoBaseId` int(10) unsigned NOT NULL,
  `ProductoUnidadBaseId` int(11) DEFAULT NULL,
  `CreadoPor` varchar(50) NOT NULL,
  `ModificadoPor` varchar(50) DEFAULT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'1',
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CostoTotal` decimal(18,6) DEFAULT NULL,
  `PrecioTotal` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoCompuestoId` (`ProductoCompuestoId`),
  KEY `ProductoBaseId` (`ProductoBaseId`),
  KEY `ProductoUnidadBaseId` (`ProductoUnidadBaseId`),
  CONSTRAINT `baseserviciocompuesto_ibfk_1` FOREIGN KEY (`ProductoUnidadBaseId`) REFERENCES `productounidadesequivalencia` (`Id`),
  CONSTRAINT `baseserviciocompuesto_ibfk_2` FOREIGN KEY (`ProductoCompuestoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `baseserviciocompuesto_ibfk_3` FOREIGN KEY (`ProductoBaseId`) REFERENCES `producto` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baseserviciocompuesto`
--

LOCK TABLES `baseserviciocompuesto` WRITE;
/*!40000 ALTER TABLE `baseserviciocompuesto` DISABLE KEYS */;
/*!40000 ALTER TABLE `baseserviciocompuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajaregistradora`
--

DROP TABLE IF EXISTS `cajaregistradora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cajaregistradora` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `localidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `CodigoCaja_UNIQUE` (`Codigo`),
  KEY `cajaregistradora_localidad_idx` (`localidadId`),
  CONSTRAINT `cajaregistradora_localidad` FOREIGN KEY (`localidadId`) REFERENCES `localidad` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajaregistradora`
--

LOCK TABLES `cajaregistradora` WRITE;
/*!40000 ALTER TABLE `cajaregistradora` DISABLE KEYS */;
INSERT INTO `cajaregistradora` VALUES (2,'CAJA ORINCIPAL','CP1','Administrador ss','2019-08-20 08:05:53',NULL,'0001-01-01 00:00:00',_binary '',1);
/*!40000 ALTER TABLE `cajaregistradora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `CedulaRnc` varchar(100) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `Detalles` longtext,
  `ClaseDetalles` varchar(500) DEFAULT NULL,
  `AplicarItbis` bit(1) NOT NULL DEFAULT b'0',
  `Activo` bit(1) NOT NULL,
  `DiasVencimiento` int(11) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MonedaId` int(11) DEFAULT NULL,
  `Direccion` varchar(2000) DEFAULT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `Codigo` varchar(50) DEFAULT NULL,
  `TipoNcf` varchar(2) DEFAULT NULL,
  `VendedorId` int(11) DEFAULT NULL,
  `MontoLimiteDeFacturacion` decimal(18,2) NOT NULL DEFAULT '0.00',
  `MontoLimiteDeCredito` decimal(18,2) NOT NULL DEFAULT '0.00',
  `ZonaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `MonedaId` (`MonedaId`),
  KEY `cliente_vendedor_idx` (`VendedorId`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `cliente_vendedor` FOREIGN KEY (`VendedorId`) REFERENCES `vendedor` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (128,'Grupo Jowilsa','131067158','8293996962','Luis Alberto','2020-07-13 11:38:11',NULL,NULL,_binary '\0',_binary '',30,'Luis Alberto','2020-07-13 11:55:04',1,'Altos de Arroyo Hondo, villa amanda, B22',NULL,'CLI00001','01',NULL,0.00,50000.00,3),(129,'Ocipav','130819378','8299867464','Luis Alberto','2020-07-15 17:15:44',NULL,NULL,_binary '\0',_binary '',30,NULL,'0001-01-01 00:00:00',1,NULL,2,'CLI00002','01',NULL,0.00,50000.00,3),(130,'CONSTANZA AGROINDUSTRIAL, S.R.L','101140895','8093722955','Luis Alberto','2020-07-15 17:20:40',NULL,NULL,_binary '\0',_binary '',0,NULL,'0001-01-01 00:00:00',1,'PROLONGACIÓN 27 DE FEBRERO. 1515, ALAMEDA',2,'CLI00003','01',NULL,0.00,0.00,3),(131,'CONSTRUCTORA COHEN LOPEZ','131706241','8297909734','Luis Alberto','2020-07-15 17:24:14',NULL,NULL,_binary '\0',_binary '',30,NULL,'0001-01-01 00:00:00',1,NULL,2,'CLI00004','01',NULL,0.00,30000.00,3);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlncf`
--

DROP TABLE IF EXISTS `controlncf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `controlncf` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Secuencia` int(11) NOT NULL,
  `ControlNumerico` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Tipo` varchar(2) NOT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Serie` char(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`),
  UNIQUE KEY `Codigo_UNIQUE` (`Tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlncf`
--

LOCK TABLES `controlncf` WRITE;
/*!40000 ALTER TABLE `controlncf` DISABLE KEYS */;
INSERT INTO `controlncf` VALUES (5,'Consumidor final',9,0,7,'01','Luis Alberto','Administrador ss','2020-07-13 11:30:59','2020-07-13 13:46:03',_binary '','B');
/*!40000 ALTER TABLE `controlncf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlsecuencias`
--

DROP TABLE IF EXISTS `controlsecuencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `controlsecuencias` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ControlNumerico` int(11) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlsecuencias`
--

LOCK TABLES `controlsecuencias` WRITE;
/*!40000 ALTER TABLE `controlsecuencias` DISABLE KEYS */;
INSERT INTO `controlsecuencias` VALUES (1,0,'FAC','Secuencia de facturas','noel','','2017-01-01 00:00:00','0001-01-01 00:00:00',_binary ''),(2,0,'DEV','SECUENCIA DEVOLUCIONES','NOEL',NULL,'2017-01-01 00:00:00','0001-01-01 00:00:00',_binary ''),(3,0,'TRF','CONTROL DE TRANSFERENCIAS','NOEL',NULL,'2017-01-01 00:00:00','0001-01-01 00:00:00',_binary ''),(4,0,'R','CONTROL DE PAGOS','NOEL','','2017-01-01 00:00:00','0001-01-01 00:00:00',_binary ''),(8,0,'NC','NOTAS DE CREDITO','NOEL',NULL,'2018-04-14 00:00:00',NULL,_binary ''),(9,1,'ENT','ENTRADAS','noel','','2018-04-14 00:00:00','0001-01-01 00:00:00',_binary ''),(10,0,'R','Recibos de ingreso','noel',NULL,'2018-04-14 00:00:00',NULL,_binary ''),(11,0,'DS','Devoluciones a suplidor','noel',NULL,'2018-04-14 00:00:00','0001-01-01 00:00:00',_binary ''),(13,0,'PG','Pagos','noel',NULL,'2018-04-30 00:00:00','0001-01-01 00:00:00',_binary ''),(14,0,'RG','Recibo de gastos','noel',NULL,'2018-04-30 00:00:00','0001-01-01 00:00:00',_binary ''),(15,1,'COT','Cotizaciones','noel',NULL,'2018-07-08 00:00:00','0001-01-01 00:00:00',_binary ''),(16,4,'CLI','Clientes','noel',NULL,'2018-01-01 00:00:00','0001-01-01 00:00:00',_binary ''),(17,0,'VND','Control de Vendedores','noel',NULL,'2018-01-01 00:00:00','2018-01-01 00:00:00',_binary '');
/*!40000 ALTER TABLE `controlsecuencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalledevolucion`
--

DROP TABLE IF EXISTS `detalledevolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalledevolucion` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `CantidadDevolucion` decimal(18,2) NOT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `Fecha` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FacturaId` int(11) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT b'0',
  `Defectuoso` bit(1) DEFAULT b'0',
  `ImpuestoId` int(11) NOT NULL,
  `MontoGrabado` decimal(18,2) NOT NULL,
  `Comentario` varchar(200) DEFAULT NULL,
  `MontoDop` decimal(18,2) NOT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `UnidadId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `detalleProducto` (`ProductoId`),
  KEY `FacturaId` (`FacturaId`),
  KEY `ImpuestoId` (`ImpuestoId`),
  KEY `detalledevolucion_ibfk_4` (`AlmacenId`),
  KEY `detalledevolucion_ibfk_5` (`UnidadId`),
  CONSTRAINT `detalledevolucion_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `detalledevolucion_ibfk_2` FOREIGN KEY (`FacturaId`) REFERENCES `factura` (`Id`),
  CONSTRAINT `detalledevolucion_ibfk_3` FOREIGN KEY (`ImpuestoId`) REFERENCES `impuesto` (`Id`),
  CONSTRAINT `detalledevolucion_ibfk_4` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `detalledevolucion_ibfk_5` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalledevolucion`
--

LOCK TABLES `detalledevolucion` WRITE;
/*!40000 ALTER TABLE `detalledevolucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalledevolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefactura`
--

DROP TABLE IF EXISTS `detallefactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detallefactura` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `Fecha` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FacturaId` int(11) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `ImpuestoId` int(11) NOT NULL DEFAULT '1',
  `MontoGrabado` decimal(18,2) NOT NULL,
  `Comentario` varchar(200) DEFAULT NULL,
  `MontoDop` decimal(18,2) NOT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `UnidadId` int(11) DEFAULT NULL,
  `MontoDescuento` decimal(18,2) NOT NULL,
  `MontoNotaCreditoAplicada` decimal(18,2) NOT NULL,
  `DetallePadreId` int(11) DEFAULT NULL,
  `PorcientoDescuento` decimal(18,2) NOT NULL,
  `EsGratis` bit(1) NOT NULL DEFAULT b'0',
  `PorcentajeVendedor` decimal(18,2) NOT NULL DEFAULT '0.00',
  `Costo` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`Id`),
  KEY `detalleProducto` (`ProductoId`),
  KEY `FacturaId` (`FacturaId`),
  KEY `ImpuestoId` (`ImpuestoId`),
  KEY `detallefactura_ibfk_4` (`AlmacenId`),
  KEY `detallefactura_ibfk_5` (`UnidadId`),
  CONSTRAINT `detalleProducto` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `detallefactura_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `detallefactura_ibfk_2` FOREIGN KEY (`FacturaId`) REFERENCES `factura` (`Id`),
  CONSTRAINT `detallefactura_ibfk_3` FOREIGN KEY (`ImpuestoId`) REFERENCES `impuesto` (`Id`),
  CONSTRAINT `detallefactura_ibfk_4` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `detallefactura_ibfk_5` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefactura`
--

LOCK TABLES `detallefactura` WRITE;
/*!40000 ALTER TABLE `detallefactura` DISABLE KEYS */;
INSERT INTO `detallefactura` VALUES (151,65,1.00,1032.73,185.89,1218.62,'2020-07-13 11:52:35','Luis Alberto',175,'2020-07-13 11:52:35',NULL,'2020-07-13 11:52:35',_binary '',1,1032.73,'',1218.62,NULL,1,0.00,0.00,NULL,0.00,_binary '\0',0.00,900.00);
/*!40000 ALTER TABLE `detallefactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL,
  `NumeroFactura` varchar(50) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Estado` varchar(1) NOT NULL,
  `FechaFacturado` datetime DEFAULT NULL,
  `MontoItbis` decimal(18,6) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `MontoGrabado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MontoDevuelto` decimal(18,6) NOT NULL,
  `MontoRecibido` decimal(18,6) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `MontoDescuento` decimal(18,6) NOT NULL,
  `MontoNotaCreditoAplicada` decimal(18,6) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `PorcientoDescuento` decimal(18,6) NOT NULL,
  `LocalidadId` int(11) DEFAULT NULL,
  `NCF` varchar(100) DEFAULT NULL,
  `TipoNCF` varchar(45) DEFAULT NULL,
  `rnc` varchar(100) DEFAULT NULL,
  `NoDocumento` varchar(50) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `AlmacenId` int(11) NOT NULL DEFAULT '0',
  `CajaId` int(11) DEFAULT NULL,
  `VendedorId` int(11) DEFAULT NULL,
  `PorcentajeVendedor` decimal(18,2) NOT NULL DEFAULT '0.00',
  `ZonaId` int(11) NOT NULL DEFAULT '0',
  `Costo` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `NumeroFactura_UNIQUE` (`NumeroFactura`),
  KEY `Factura_Moneda` (`MonedaId`),
  KEY `Factura_Cliente` (`ClienteId`),
  KEY `Factura_localidad_idx` (`LocalidadId`),
  KEY `factura_caja_idx` (`CajaId`),
  KEY `factura_vendedor_idx` (`VendedorId`),
  CONSTRAINT `Factura_Cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `Factura_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Factura_localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`),
  CONSTRAINT `factura_caja` FOREIGN KEY (`CajaId`) REFERENCES `cajaregistradora` (`Id`),
  CONSTRAINT `factura_vendedor` FOREIGN KEY (`VendedorId`) REFERENCES `vendedor` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (175,128,'COT00001',1,1218.621400,0.000000,_binary '','C',NULL,185.891400,'Luis Alberto',1032.730000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,1218.621400,0.000000,0.000000,NULL,0.000000,1,NULL,NULL,'131067158','COT00001','2020-07-13 11:52:35',2,NULL,NULL,0.00,0,900.00);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaimpuesto`
--

DROP TABLE IF EXISTS `facturaimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturaimpuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FacturaId` int(11) NOT NULL,
  `MonedaId` int(11) DEFAULT NULL,
  `ImpuestoId` int(11) DEFAULT NULL,
  `NoFactura` varchar(50) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `montoImpuesto` decimal(18,2) DEFAULT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `modificadoPor` varchar(45) DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `activo` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaimpuesto`
--

LOCK TABLES `facturaimpuesto` WRITE;
/*!40000 ALTER TABLE `facturaimpuesto` DISABLE KEYS */;
INSERT INTO `facturaimpuesto` VALUES (8,175,1,2,'COT00001','2020-07-13 11:52:36',185.89,'Luis Alberto',NULL,'0001-01-01 00:00:00','2020-07-13 11:52:36',_binary '');
/*!40000 ALTER TABLE `facturaimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturarproductoservicio`
--

DROP TABLE IF EXISTS `facturarproductoservicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facturarproductoservicio` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EsServicio` bit(1) NOT NULL,
  `EsCompuesto` bit(1) NOT NULL,
  `NombreDeClase` varchar(2000) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturarproductoservicio`
--

LOCK TABLES `facturarproductoservicio` WRITE;
/*!40000 ALTER TABLE `facturarproductoservicio` DISABLE KEYS */;
INSERT INTO `facturarproductoservicio` VALUES (20,_binary '\0',_binary '\0','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarProducto, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0'),(21,_binary '',_binary '\0','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarServicioSimple, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0'),(22,_binary '',_binary '','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarServicioCompuesto, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0');
/*!40000 ALTER TABLE `facturarproductoservicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastoempresa`
--

DROP TABLE IF EXISTS `gastoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gastoempresa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(11) NOT NULL,
  `ReferenciaGasto` varchar(50) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Estado` varchar(1) NOT NULL,
  `FechaEmision` datetime NOT NULL,
  `MontoItbis` decimal(18,6) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `MontoGrabado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MontoDevuelto` decimal(18,6) NOT NULL,
  `MontoDado` decimal(18,6) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `LocalidadId` int(11) DEFAULT NULL,
  `NCF` varchar(100) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `ReferenciaGasto_UNIQUE` (`ReferenciaGasto`),
  KEY `Gasto_Moneda` (`MonedaId`),
  KEY `Gasto_Suplidor` (`SuplidorId`),
  KEY `Gasto_Localidad_idx` (`LocalidadId`),
  CONSTRAINT `Gasto_Localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`),
  CONSTRAINT `Gasto_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Gasto_Suplidor` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastoempresa`
--

LOCK TABLES `gastoempresa` WRITE;
/*!40000 ALTER TABLE `gastoempresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastoempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastoimpuesto`
--

DROP TABLE IF EXISTS `gastoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gastoimpuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `GastoEmpresaId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `ImpuestoId` int(11) DEFAULT NULL,
  `MontoImpuesto` decimal(18,2) DEFAULT '0.00',
  `referencia` varchar(50) DEFAULT NULL,
  `Fecha` datetime NOT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `modificadoPor` varchar(45) DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `activo` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastoimpuesto`
--

LOCK TABLES `gastoimpuesto` WRITE;
/*!40000 ALTER TABLE `gastoimpuesto` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuesto`
--

DROP TABLE IF EXISTS `impuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `impuesto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Porciento` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impuesto`
--

LOCK TABLES `impuesto` WRITE;
/*!40000 ALTER TABLE `impuesto` DISABLE KEYS */;
INSERT INTO `impuesto` VALUES (1,'NO APLICA',0.00,'Administrador ss','2019-08-13 06:21:26','Usuario Marpos RD usuario','0001-01-01 00:00:00',_binary '\0'),(2,'ITBIS',0.18,'Administrador ss','2019-08-13 06:21:35','Luis Alberto','2020-07-09 17:21:32.400839',_binary ''),(3,'NO APLICA',0.00,'Administrador ss','2019-08-25 08:58:14','Usuario Marpos RD usuario','0001-01-01 00:00:00',_binary '\0'),(4,'No aplica',0.00,'Administrador ss','2019-09-05 07:58:51','Luis Alberto','2020-07-09 17:21:36.557136',_binary '');
/*!40000 ALTER TABLE `impuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `localidad` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
INSERT INTO `localidad` VALUES (1,'LOCALIDAD PRINCIPAL','Administrador ss','2019-08-13 06:20:19',NULL,'0001-01-01 00:00:00',_binary '');
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moneda`
--

DROP TABLE IF EXISTS `moneda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `moneda` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  `Tasa` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `EsMonedaLocal` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moneda`
--

LOCK TABLES `moneda` WRITE;
/*!40000 ALTER TABLE `moneda` DISABLE KEYS */;
INSERT INTO `moneda` VALUES (1,'Pesos dominicanos','DOP',1.00,'Administrador ss','2019-08-13 06:21:11','Luis Alberto','2020-07-13 11:24:25',_binary '',_binary '');
/*!40000 ALTER TABLE `moneda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `montosapertura`
--

DROP TABLE IF EXISTS `montosapertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `montosapertura` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Monto` varchar(128) NOT NULL,
  `Orden` int(11) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `montosapertura`
--

LOCK TABLES `montosapertura` WRITE;
/*!40000 ALTER TABLE `montosapertura` DISABLE KEYS */;
INSERT INTO `montosapertura` VALUES (3,'1','1',0,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(4,'5','5',1,'noel','2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00',_binary ''),(6,'10','10',2,'noel','2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00',_binary ''),(7,'25','25',3,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(8,'50','50',4,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(9,'100','100',5,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(10,'200','200',6,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(11,'500','500',7,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(12,'1000','1000',8,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary ''),(13,'2000','2000',9,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',_binary '');
/*!40000 ALTER TABLE `montosapertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientoalmacen`
--

DROP TABLE IF EXISTS `movimientoalmacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientoalmacen` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(10) unsigned NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,6) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  `LocalidadId` int(11) NOT NULL,
  `TipoMovimiento` varchar(5) NOT NULL,
  `Referencia` varchar(50) NOT NULL DEFAULT '',
  `BalanceActual` decimal(18,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`Id`),
  KEY `movimiento_producto` (`ProductoId`),
  KEY `movimiento_AlmacenId` (`AlmacenId`),
  KEY `movimiento_unidad` (`UnidadId`),
  KEY `movimiento_TipoMovimiento` (`TipoMovimiento`),
  CONSTRAINT `movimientoalmacen_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `movimientoalmacen_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `movimientoalmacen_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`),
  CONSTRAINT `movimientoalmacen_ibfk_4` FOREIGN KEY (`TipoMovimiento`) REFERENCES `tipomovimiento` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientoalmacen`
--

LOCK TABLES `movimientoalmacen` WRITE;
/*!40000 ALTER TABLE `movimientoalmacen` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientoalmacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodevolucion`
--

DROP TABLE IF EXISTS `movimientodevolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientodevolucion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `SuplidorId` int(11) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(10) unsigned NOT NULL,
  `MontoItbis` decimal(18,2) DEFAULT '0.00',
  `MontoTotal` decimal(18,2) DEFAULT '0.00',
  `UnidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `SuplidorId` (`SuplidorId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `movimientodevolucion_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `movimientodevolucion_ibfk_2` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`),
  CONSTRAINT `movimientodevolucion_ibfk_3` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `movimientodevolucion_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodevolucion`
--

LOCK TABLES `movimientodevolucion` WRITE;
/*!40000 ALTER TABLE `movimientodevolucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientodevolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodevolucioncliente`
--

DROP TABLE IF EXISTS `movimientodevolucioncliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientodevolucioncliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `ClienteId` int(11) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `MontoItbis` decimal(18,2) DEFAULT '0.00',
  `MontoTotal` decimal(18,2) DEFAULT '0.00',
  `UnidadId` int(11) DEFAULT NULL,
  `Defectuoso` bit(1) NOT NULL,
  `NoFactura` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `movimientodevolucioncliente_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `movimientodevolucioncliente_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `movimientodevolucioncliente_ibfk_3` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `movimientodevolucioncliente_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodevolucioncliente`
--

LOCK TABLES `movimientodevolucioncliente` WRITE;
/*!40000 ALTER TABLE `movimientodevolucioncliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientodevolucioncliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientoentrada`
--

DROP TABLE IF EXISTS `movimientoentrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientoentrada` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `SuplidorId` int(11) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `UnidadId` (`UnidadId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `SuplidorId` (`SuplidorId`),
  KEY `ProductoId` (`ProductoId`),
  CONSTRAINT `movimientoentrada_ibfk_1` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`),
  CONSTRAINT `movimientoentrada_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `movimientoentrada_ibfk_3` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`),
  CONSTRAINT `movimientoentrada_ibfk_4` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientoentrada`
--

LOCK TABLES `movimientoentrada` WRITE;
/*!40000 ALTER TABLE `movimientoentrada` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientoentrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notacredito`
--

DROP TABLE IF EXISTS `notacredito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notacredito` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoFacturaOrigenId` varchar(50) NOT NULL,
  `NoFacturaAplicadaId` varchar(50) DEFAULT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Aplicado` bit(1) NOT NULL,
  `Secuencia` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Secuencia_UNIQUE` (`Secuencia`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `FacturaOrigenId_UNIQUE` (`NoFacturaOrigenId`),
  UNIQUE KEY `FacturaAplicadaId_UNIQUE` (`NoFacturaAplicadaId`),
  KEY `NoFacturaOrigenId` (`NoFacturaOrigenId`),
  KEY `NCMonedaId` (`MonedaId`),
  KEY `NoFacturaAplicadaId` (`NoFacturaAplicadaId`),
  CONSTRAINT `notacredito_ibfk_1` FOREIGN KEY (`NoFacturaOrigenId`) REFERENCES `factura` (`NumeroFactura`),
  CONSTRAINT `notacredito_ibfk_2` FOREIGN KEY (`NoFacturaAplicadaId`) REFERENCES `factura` (`NumeroFactura`),
  CONSTRAINT `notacredito_ibfk_3` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notacredito`
--

LOCK TABLES `notacredito` WRITE;
/*!40000 ALTER TABLE `notacredito` DISABLE KEYS */;
/*!40000 ALTER TABLE `notacredito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operaciones`
--

DROP TABLE IF EXISTS `operaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `operaciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `NombreIlustrativo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operaciones`
--

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;
INSERT INTO `operaciones` VALUES (1,'Leer','Leer'),(2,'Crear','Crear'),(3,'Modificar','Modificar'),(4,'Borrar','Borrar'),(5,'CrearFacturaAntigua','Crear Factura Antigua'),(6,'ReporteDeVentas','Reporte De Ventas'),(7,'ReporteCxC','Reporte CxC'),(8,'ReporteAvanzado','Reporte Avanzado'),(9,'ReporteCxP','Reporte CxP'),(10,'InventarioAlmacenes','Inventario Almacenes'),(11,'RegistrarSalidas','Registrar Salidas'),(12,'ImprimirDevolucion','Imprimir Devolucion'),(13,'RegistrarEntradas','Registrar Entradas'),(14,'ImprimirEntrada','Imprimir Entrada'),(15,'MovimientosProducto','Movimientos Producto'),(16,'ImprimirTransferencia','Imprimir Transferencia'),(17,'CambiarContrasena','Cambiar Contrasena'),(18,'IngresarDevolucionCliente','Ingresar Devolucion Cliente'),(19,'RegistrarTransferencias','Registrar Transferencias'),(20,'AplicarPagos','Aplicar Pagos'),(21,'CrearFacturaAntigua','Crear Factura Antigua'),(22,'AplicarPagos','Aplicar Pagos'),(23,'*','Todas'),(24,'LeerTodos','Leer Todos'),(25,'LeerPorId','Leer Por Id'),(26,'LeerPaginadoBasico','Leer Paginado Basico'),(27,'LeerPaginadoPorActivos','Leer Paginado Por Activos'),(28,'LeerPaginadoAvanzado','Leer Paginado Avanzado'),(29,'LeerPorFiltro','Leer Por Filtro'),(30,'AsignarRoleUsuario','Asignar Rol Usuario'),(31,'FacturarCotizacion','Facturar Cotizacion'),(32,'LeerProductoEnLocalidad','Leer Producto En Localidad'),(33,'cerrarCaja','Cerrar Caja'),(34,'productosFiltrados','Listado de precios'),(35,'ClientesFiltrados','Listado de clientes'),(36,'InventarioAlmacenes','Inventario Almacenes'),(37,'Mantenimiento','Mantenimiento'),(38,'Mantenimiento-Clientes','Mantenimiento-Clientes'),(39,'Mantenimiento-Productos','Mantenimiento-Productos'),(40,'Mantenimiento-Suplidores','Mantenimiento-Suplidores'),(41,'Mantenimiento-Almacenes','Mantenimiento-Almacenes'),(42,'Mantenimiento-Impuestos','Mantenimiento-Impuestos'),(43,'Mantenimiento-Localidades','Mantenimiento-Localidades'),(44,'Mantenimiento-Monedas','Mantenimiento-Monedas'),(45,'Mantenimiento-Unidades','Mantenimiento-Unidades'),(46,'Mantenimiento-ControlNCF','Mantenimiento-ControlNCF'),(47,'Cajas','Cajas'),(48,'Cajas-Mantenimiento','Cajas-Mantenimiento'),(49,'Cajas-AperturaCierre','Cajas-AperturaCierre'),(50,'Facturacion','Facturacion'),(51,'Facturacion-Cotizaciones','Facturacion-Cotizaciones'),(52,'Facturacion-Facturas','Facturacion-Facturas'),(53,'Facturacion-Pagos','Facturacion-Pagos'),(54,'Gastos','Gastos'),(55,'Gastos-Gastos','Gastos-Gastos'),(56,'Gastos-Pagos','Gastos-Pagos'),(57,'Reportes','Reportes'),(58,'Reportes-ReporteVentas','Reportes-ReporteVentas'),(59,'Reportes-CxCGeneral','Reportes-CxCGeneral'),(60,'Reportes-ReporteInventario','Reportes-ReporteInventario'),(61,'Reportes-movimientosProducto','Reportes-movimientosProducto'),(62,'Reportes-reporteCxP','Reportes-reporteCxP'),(63,'Reportes-listadoPrecios','Reportes-listadoPrecios'),(64,'Reportes-listadoClientes','Reportes-listadoClientes'),(65,'Movimientos','Movimientos'),(66,'Movimientos-MovimientoEntrada','Movimientos-MovimientoEntrada'),(67,'Movimientos-TransferenciaAlmacen','Movimientos-TransferenciaAlmacen'),(68,'Movimientos-DevolucionCliente','Movimientos-DevolucionCliente'),(69,'Movimientos-MovimientoDevolucion','Movimientos-MovimientoDevolucion'),(70,'Usuarios','Usuarios'),(71,'Usuarios-Usuarios','Usuarios-Usuarios'),(72,'Usuarios-Roles','Usuarios-Roles'),(73,'Reportes-estadoCuentaClientes','Reporte Estado de Cuenta'),(74,'EstadoCuenta','Estado de Cuenta'),(75,'Mantenimiento-Zonas','Mantenimiento de zonas'),(76,'Mantenimiento-Vendedores','Mantenimiento de vendedores');
/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pago` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MonedaFacturaId` int(11) NOT NULL,
  `TipoPagoId` int(11) NOT NULL,
  `NoFactura` varchar(50) NOT NULL,
  `MontoPagado` decimal(18,2) NOT NULL,
  `TasaConversion` decimal(18,2) NOT NULL,
  `MontoPendiente` decimal(18,2) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Secuencia` varchar(100) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL DEFAULT '0.00',
  `NoTalonario` varchar(50) DEFAULT NULL,
  `PorcentajeVendedor` decimal(18,2) NOT NULL DEFAULT '0.00',
  `VendedorId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Pago_Moneda` (`MonedaId`),
  KEY `Pago_Cliente` (`ClienteId`),
  KEY `pago_factura` (`NoFactura`),
  KEY `Moneda_Factura` (`MonedaFacturaId`),
  KEY `Pago_TipoPago` (`TipoPagoId`),
  CONSTRAINT `Moneda_Factura` FOREIGN KEY (`MonedaFacturaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Pago_Cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `Pago_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Pago_MonedaFactura` FOREIGN KEY (`MonedaFacturaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Pago_TipoPago` FOREIGN KEY (`TipoPagoId`) REFERENCES `tipopago` (`Id`),
  CONSTRAINT `pago_factura` FOREIGN KEY (`NoFactura`) REFERENCES `factura` (`NumeroFactura`)
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagogasto`
--

DROP TABLE IF EXISTS `pagogasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagogasto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(11) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `MonedaGastoId` int(11) NOT NULL,
  `TipoPagoId` int(11) NOT NULL,
  `ReferenciaGasto` varchar(50) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `TasaConversion` decimal(18,6) NOT NULL,
  `MontoPendiente` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Secuencia` varchar(100) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Pago_Moneda` (`MonedaId`),
  KEY `Pago_Suplidor` (`SuplidorId`),
  KEY `pago_GastoEmpresa` (`ReferenciaGasto`),
  KEY `Moneda_Gasto` (`MonedaGastoId`),
  KEY `Pago_TipoPago` (`TipoPagoId`),
  CONSTRAINT `Pago_MonedaGasto_Constrain` FOREIGN KEY (`MonedaGastoId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Pago_Moneda_Constrain` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `Pago_Suplidor_Constrain` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`),
  CONSTRAINT `Pago_TipoPago_Constrain` FOREIGN KEY (`TipoPagoId`) REFERENCES `tipopago` (`Id`),
  CONSTRAINT `pago_Gastoempresa_Constrain` FOREIGN KEY (`ReferenciaGasto`) REFERENCES `gastoempresa` (`ReferenciaGasto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagogasto`
--

LOCK TABLES `pagogasto` WRITE;
/*!40000 ALTER TABLE `pagogasto` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagogasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Precio` decimal(18,2) NOT NULL,
  `Detalles` longtext,
  `ClaseDetalles` varchar(200) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(50) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `EsServicio` bit(1) NOT NULL,
  `CodigoBarra` varchar(500) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Costo` decimal(18,2) NOT NULL,
  `MonedaId` int(11) NOT NULL,
  `Existencia` decimal(18,2) NOT NULL,
  `EsCompuesto` bit(1) NOT NULL DEFAULT b'1',
  `Precio2` decimal(18,2) NOT NULL DEFAULT '0.00',
  `Precio3` decimal(18,2) NOT NULL DEFAULT '0.00',
  `PorcentajeVendedor` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `MonedaId` (`MonedaId`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='Tabla donde se registran los productos.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (65,'FILTRO','P551423',1032.73,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":1,\"unidadId\":1,\"suplidor\":{\"id\":13,\"nombre\":\"VILLA GARCÍA\"},\"costo\":900,\"suplidorId\":13,\"$$hashKey\":\"object:719\"}]',NULL,'2020-07-13 11:43:45','Luis Alberto',_binary '',_binary '\0','P551423','Luis Alberto','2020-07-13 11:57:38',900.00,1,0.00,_binary '\0',0.00,0.00,0.00);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productoimpuesto`
--

DROP TABLE IF EXISTS `productoimpuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productoimpuesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(11) NOT NULL,
  `ImpuestoId` int(11) DEFAULT NULL,
  `montoImpuesto` decimal(18,2) DEFAULT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `modificadoPor` varchar(45) DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `activo` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productoimpuesto`
--

LOCK TABLES `productoimpuesto` WRITE;
/*!40000 ALTER TABLE `productoimpuesto` DISABLE KEYS */;
INSERT INTO `productoimpuesto` VALUES (6,65,2,NULL,'Luis Alberto','Luis Alberto','2020-07-13 11:57:38','2020-07-13 11:43:45',_binary '');
/*!40000 ALTER TABLE `productoimpuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productounidadesequivalencia`
--

DROP TABLE IF EXISTS `productounidadesequivalencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productounidadesequivalencia` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  `Equivalencia` decimal(18,4) NOT NULL,
  `EsPrincipal` bit(1) NOT NULL,
  `Orden` int(11) NOT NULL,
  `PrecioCompra` decimal(18,2) NOT NULL,
  `PrecioVenta` decimal(18,2) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `productounidadesequivalencia_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `productounidadesequivalencia_ibfk_2` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productounidadesequivalencia`
--

LOCK TABLES `productounidadesequivalencia` WRITE;
/*!40000 ALTER TABLE `productounidadesequivalencia` DISABLE KEYS */;
INSERT INTO `productounidadesequivalencia` VALUES (64,65,'Luis Alberto','2020-07-13 11:43:45','Luis Alberto','2020-07-13 11:57:38',_binary '',1,1.0000,_binary '',0,900.00,1032.73);
/*!40000 ALTER TABLE `productounidadesequivalencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Mantenimiento-Clientes',_binary ''),(2,'Mantenimiento-Productos',_binary ''),(3,'Mantenimiento-Suplidores',_binary ''),(4,'Mantenimiento-Almacenes',_binary ''),(5,'Mantenimiento-Impuestos',_binary ''),(6,'Mantenimiento-Localidades',_binary ''),(7,'Mantenimiento-Monedas',_binary ''),(8,'Mantenimiento-Unidades',_binary ''),(9,'Mantenimiento-NCF',_binary ''),(10,'Facturacion-Cotizaciones',_binary ''),(11,'Facturacion-Facturas',_binary ''),(12,'Facturacion-Pagos',_binary ''),(13,'Gastos-Ingreso',_binary ''),(14,'Gastos-Pagos',_binary ''),(15,'test',_binary ''),(16,'Mantenimiento-Cajas',_binary ''),(17,'Apertura-Cierre-Cajas',_binary ''),(18,'Reporte-Ventas',_binary ''),(19,'Reporte-CXC',_binary ''),(21,'Reporte-Existencia',_binary ''),(22,'Reporte-Movimientos',_binary ''),(25,'Reporte-CXP',_binary ''),(26,'Reporte-Clientes',_binary ''),(27,'Reporte-Precios',_binary ''),(28,'Movimiento-Entrada-Inventario',_binary ''),(29,'Movimiento-Transferencia-Almacenes',_binary ''),(30,'Movimiento-Devolucion-Cliente',_binary ''),(31,'Movimiento-Devolucion',_binary ''),(32,'Admin-Usuarios',_binary ''),(33,'Admin-Roles',_binary ''),(34,'Reporte-Estado-Cuenta',_binary ''),(35,'Mantenimiento-Vendedores',_binary ''),(37,'Mantenimiento-Zonas',_binary ''),(38,'Anular Facturas',_binary ''),(39,'Anular Cotizaciones',_binary ''),(40,'Eliminar Caja',_binary ''),(41,'Anular Gasto',_binary ''),(42,'Anular pago gasto',_binary ''),(43,'Anular Pagos facturas',_binary '');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roleseccion`
--

DROP TABLE IF EXISTS `roleseccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roleseccion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermisosJson` longtext NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `roleseccion_Roles` (`RoleId`),
  CONSTRAINT `roleseccion_Roles` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roleseccion`
--

LOCK TABLES `roleseccion` WRITE;
/*!40000 ALTER TABLE `roleseccion` DISABLE KEYS */;
INSERT INTO `roleseccion` VALUES (1,1,'[{\"seccion\": {\"id\": 10, \"nombre\": \"Cliente\"}, \"operaciones\": [{\"id\": 171, \"seccionId\": 10, \"operacionId\": 28, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 91, \"seccionId\": 10, \"operacionId\": 24, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 41, \"seccionId\": 10, \"operacionId\": 1, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 42, \"seccionId\": 10, \"operacionId\": 2, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 111, \"seccionId\": 10, \"operacionId\": 25, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 43, \"seccionId\": 10, \"operacionId\": 3, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 131, \"seccionId\": 10, \"operacionId\": 26, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 151, \"seccionId\": 10, \"operacionId\": 27, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 44, \"seccionId\": 10, \"operacionId\": 4, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}]}, {\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 112, \"seccionId\": 11, \"operacionId\": 25, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 14, \"nombre\": \"ControlNCF\"}, \"operaciones\": [{\"id\": 49, \"seccionId\": 14, \"operacionId\": 1, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 95, \"seccionId\": 14, \"operacionId\": 24, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 115, \"seccionId\": 14, \"operacionId\": 25, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 108, \"seccionId\": 6, \"operacionId\": 25, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 360, \"seccionId\": 38, \"operacionId\": 38, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Clientes\", \"operacionNombreIlustrativo\": \"Mantenimiento-Clientes\"}]}]',_binary ''),(2,6,'[{\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 59, \"seccionId\": 16, \"operacionId\": 2, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 60, \"seccionId\": 16, \"operacionId\": 3, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 61, \"seccionId\": 16, \"operacionId\": 4, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 117, \"seccionId\": 16, \"operacionId\": 25, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 137, \"seccionId\": 16, \"operacionId\": 26, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 157, \"seccionId\": 16, \"operacionId\": 27, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 177, \"seccionId\": 16, \"operacionId\": 28, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 365, \"seccionId\": 38, \"operacionId\": 43, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Localidades\", \"operacionNombreIlustrativo\": \"Mantenimiento-Localidades\"}]}]',_binary ''),(3,3,'[{\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 67, \"seccionId\": 20, \"operacionId\": 1, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 100, \"seccionId\": 20, \"operacionId\": 24, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 180, \"seccionId\": 20, \"operacionId\": 28, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 120, \"seccionId\": 20, \"operacionId\": 25, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 68, \"seccionId\": 20, \"operacionId\": 2, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 69, \"seccionId\": 20, \"operacionId\": 3, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 140, \"seccionId\": 20, \"operacionId\": 26, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 160, \"seccionId\": 20, \"operacionId\": 27, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 70, \"seccionId\": 20, \"operacionId\": 4, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 362, \"seccionId\": 38, \"operacionId\": 40, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Suplidores\", \"operacionNombreIlustrativo\": \"Mantenimiento-Suplidores\"}]}]',_binary ''),(4,4,'[{\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 46, \"seccionId\": 11, \"operacionId\": 2, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 47, \"seccionId\": 11, \"operacionId\": 3, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 48, \"seccionId\": 11, \"operacionId\": 4, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 112, \"seccionId\": 11, \"operacionId\": 25, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 132, \"seccionId\": 11, \"operacionId\": 26, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 152, \"seccionId\": 11, \"operacionId\": 27, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 172, \"seccionId\": 11, \"operacionId\": 28, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 117, \"seccionId\": 16, \"operacionId\": 25, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 363, \"seccionId\": 38, \"operacionId\": 41, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Almacenes\", \"operacionNombreIlustrativo\": \"Mantenimiento-Almacenes\"}]}]',_binary ''),(5,5,'[{\"seccion\": {\"id\": 7, \"nombre\": \"Impuesto\"}, \"operaciones\": [{\"id\": 29, \"seccionId\": 7, \"operacionId\": 1, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 30, \"seccionId\": 7, \"operacionId\": 2, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 31, \"seccionId\": 7, \"operacionId\": 3, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 32, \"seccionId\": 7, \"operacionId\": 4, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 89, \"seccionId\": 7, \"operacionId\": 24, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 109, \"seccionId\": 7, \"operacionId\": 25, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 129, \"seccionId\": 7, \"operacionId\": 26, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 149, \"seccionId\": 7, \"operacionId\": 27, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 169, \"seccionId\": 7, \"operacionId\": 28, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 364, \"seccionId\": 38, \"operacionId\": 42, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Impuestos\", \"operacionNombreIlustrativo\": \"Mantenimiento-Impuestos\"}]}]',_binary ''),(6,7,'[{\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 26, \"seccionId\": 6, \"operacionId\": 2, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 27, \"seccionId\": 6, \"operacionId\": 3, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 28, \"seccionId\": 6, \"operacionId\": 4, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 108, \"seccionId\": 6, \"operacionId\": 25, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 128, \"seccionId\": 6, \"operacionId\": 26, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 148, \"seccionId\": 6, \"operacionId\": 27, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 168, \"seccionId\": 6, \"operacionId\": 28, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 366, \"seccionId\": 38, \"operacionId\": 44, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Monedas\", \"operacionNombreIlustrativo\": \"Mantenimiento-Monedas\"}]}]',_binary ''),(7,8,'[{\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 76, \"seccionId\": 22, \"operacionId\": 2, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 77, \"seccionId\": 22, \"operacionId\": 3, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 78, \"seccionId\": 22, \"operacionId\": 4, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 122, \"seccionId\": 22, \"operacionId\": 25, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 142, \"seccionId\": 22, \"operacionId\": 26, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 162, \"seccionId\": 22, \"operacionId\": 27, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 182, \"seccionId\": 22, \"operacionId\": 28, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 367, \"seccionId\": 38, \"operacionId\": 45, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Unidades\", \"operacionNombreIlustrativo\": \"Mantenimiento-Unidades\"}]}]',_binary ''),(8,9,'[{\"seccion\": {\"id\": 14, \"nombre\": \"ControlNCF\"}, \"operaciones\": [{\"id\": 49, \"seccionId\": 14, \"operacionId\": 1, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 50, \"seccionId\": 14, \"operacionId\": 2, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 51, \"seccionId\": 14, \"operacionId\": 3, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 52, \"seccionId\": 14, \"operacionId\": 4, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 95, \"seccionId\": 14, \"operacionId\": 24, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 115, \"seccionId\": 14, \"operacionId\": 25, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 135, \"seccionId\": 14, \"operacionId\": 26, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 155, \"seccionId\": 14, \"operacionId\": 27, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 175, \"seccionId\": 14, \"operacionId\": 28, \"nombreSeccion\": \"ControlNCF\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 368, \"seccionId\": 38, \"operacionId\": 46, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-ControlNCF\", \"operacionNombreIlustrativo\": \"Mantenimiento-ControlNCF\"}]}]',_binary ''),(9,2,'[{\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 108, \"seccionId\": 6, \"operacionId\": 25, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 122, \"seccionId\": 22, \"operacionId\": 25, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 67, \"seccionId\": 20, \"operacionId\": 1, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 100, \"seccionId\": 20, \"operacionId\": 24, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 120, \"seccionId\": 20, \"operacionId\": 25, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 18, \"seccionId\": 4, \"operacionId\": 1, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 19, \"seccionId\": 4, \"operacionId\": 2, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 20, \"seccionId\": 4, \"operacionId\": 3, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 21, \"seccionId\": 4, \"operacionId\": 4, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Borrar\", \"operacionNombreIlustrativo\": \"Borrar\"}, {\"id\": 22, \"seccionId\": 4, \"operacionId\": 15, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"MovimientosProducto\", \"operacionNombreIlustrativo\": \"Movimientos Producto\"}, {\"id\": 86, \"seccionId\": 4, \"operacionId\": 24, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 106, \"seccionId\": 4, \"operacionId\": 25, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 126, \"seccionId\": 4, \"operacionId\": 26, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 146, \"seccionId\": 4, \"operacionId\": 27, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 166, \"seccionId\": 4, \"operacionId\": 28, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 359, \"seccionId\": 38, \"operacionId\": 37, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento\", \"operacionNombreIlustrativo\": \"Mantenimiento\"}, {\"id\": 361, \"seccionId\": 38, \"operacionId\": 39, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Mantenimiento-Productos\", \"operacionNombreIlustrativo\": \"Mantenimiento-Productos\"}]}]',_binary ''),(10,10,'[{\"seccion\":{\"id\":22,\"nombre\":\"Unidades\"},\"operaciones\":[{\"id\":75,\"seccionId\":22,\"operacionId\":1,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":102,\"seccionId\":22,\"operacionId\":24,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":122,\"seccionId\":22,\"operacionId\":25,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":6,\"nombre\":\"Moneda\"},\"operaciones\":[{\"id\":25,\"seccionId\":6,\"operacionId\":1,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":88,\"seccionId\":6,\"operacionId\":24,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":108,\"seccionId\":6,\"operacionId\":25,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":10,\"nombre\":\"Cliente\"},\"operaciones\":[{\"id\":200,\"seccionId\":10,\"operacionId\":29,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"},{\"id\":111,\"seccionId\":10,\"operacionId\":25,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":41,\"seccionId\":10,\"operacionId\":1,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":91,\"seccionId\":10,\"operacionId\":24,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"}]},{\"seccion\":{\"id\":11,\"nombre\":\"Almacen\"},\"operaciones\":[{\"id\":45,\"seccionId\":11,\"operacionId\":1,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":92,\"seccionId\":11,\"operacionId\":24,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":112,\"seccionId\":11,\"operacionId\":25,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":4,\"nombre\":\"Producto\"},\"operaciones\":[{\"id\":18,\"seccionId\":4,\"operacionId\":1,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":86,\"seccionId\":4,\"operacionId\":24,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":106,\"seccionId\":4,\"operacionId\":25,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":201,\"seccionId\":4,\"operacionId\":29,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"}]},{\"seccion\":{\"id\":8,\"nombre\":\"Factura\"},\"operaciones\":[{\"id\":110,\"seccionId\":8,\"operacionId\":25,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":24,\"nombre\":\"Cotizacion\"},\"operaciones\":[{\"id\":185,\"seccionId\":24,\"operacionId\":1,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":186,\"seccionId\":24,\"operacionId\":2,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"Crear\",\"operacionNombreIlustrativo\":\"Crear\"},{\"id\":187,\"seccionId\":24,\"operacionId\":3,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"Modificar\",\"operacionNombreIlustrativo\":\"Modificar\"},{\"id\":193,\"seccionId\":24,\"operacionId\":24,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":194,\"seccionId\":24,\"operacionId\":25,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":195,\"seccionId\":24,\"operacionId\":26,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"LeerPaginadoBasico\",\"operacionNombreIlustrativo\":\"Leer Paginado Basico\"},{\"id\":196,\"seccionId\":24,\"operacionId\":27,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"LeerPaginadoPorActivos\",\"operacionNombreIlustrativo\":\"Leer Paginado Por Activos\"},{\"id\":202,\"seccionId\":24,\"operacionId\":31,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"FacturarCotizacion\",\"operacionNombreIlustrativo\":\"Facturar Cotizacion\"},{\"id\":197,\"seccionId\":24,\"operacionId\":28,\"nombreSeccion\":\"Cotizacion\",\"operacionNombre\":\"LeerPaginadoAvanzado\",\"operacionNombreIlustrativo\":\"Leer Paginado Avanzado\"}]},{\"seccion\":{\"id\":38,\"nombre\":\"Menu\"},\"operaciones\":[{\"id\":372,\"seccionId\":38,\"operacionId\":50,\"nombreSeccion\":\"Menu\",\"operacionNombre\":\"Facturacion\",\"operacionNombreIlustrativo\":\"Facturacion\"},{\"id\":373,\"seccionId\":38,\"operacionId\":51,\"nombreSeccion\":\"Menu\",\"operacionNombre\":\"Facturacion-Cotizaciones\",\"operacionNombreIlustrativo\":\"Facturacion-Cotizaciones\"}]},{\"seccion\":{\"id\":39,\"nombre\":\"Vendedor\"},\"operaciones\":[{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":403,\"operacionId\":1,\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":407,\"operacionId\":24,\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":412,\"operacionId\":29,\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":408,\"operacionId\":25,\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]}]',_binary ''),(11,10,'[{\"seccion\": {\"id\": 10, \"nombre\": \"Cliente\"}, \"operaciones\": [{\"id\": 41, \"seccionId\": 10, \"operacionId\": 1, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 91, \"seccionId\": 10, \"operacionId\": 24, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 111, \"seccionId\": 10, \"operacionId\": 25, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 108, \"seccionId\": 6, \"operacionId\": 25, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 18, \"seccionId\": 4, \"operacionId\": 1, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 86, \"seccionId\": 4, \"operacionId\": 24, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 106, \"seccionId\": 4, \"operacionId\": 25, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 122, \"seccionId\": 22, \"operacionId\": 25, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 7, \"nombre\": \"Impuesto\"}, \"operaciones\": [{\"id\": 29, \"seccionId\": 7, \"operacionId\": 1, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 89, \"seccionId\": 7, \"operacionId\": 24, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 109, \"seccionId\": 7, \"operacionId\": 25, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}]',_binary ''),(12,11,'[{\"seccion\":{\"id\":11,\"nombre\":\"Almacen\"},\"operaciones\":[{\"id\":45,\"seccionId\":11,\"operacionId\":1,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":92,\"seccionId\":11,\"operacionId\":24,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":112,\"seccionId\":11,\"operacionId\":25,\"nombreSeccion\":\"Almacen\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":13,\"nombre\":\"AlmacenProducto\"},\"operaciones\":[{\"id\":94,\"seccionId\":13,\"operacionId\":24,\"nombreSeccion\":\"AlmacenProducto\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":114,\"seccionId\":13,\"operacionId\":25,\"nombreSeccion\":\"AlmacenProducto\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":10,\"nombre\":\"Cliente\"},\"operaciones\":[{\"id\":41,\"seccionId\":10,\"operacionId\":1,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":91,\"seccionId\":10,\"operacionId\":24,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":111,\"seccionId\":10,\"operacionId\":25,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":200,\"seccionId\":10,\"operacionId\":29,\"nombreSeccion\":\"Cliente\",\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"}]},{\"seccion\":{\"id\":14,\"nombre\":\"ControlNCF\"},\"operaciones\":[{\"id\":49,\"seccionId\":14,\"operacionId\":1,\"nombreSeccion\":\"ControlNCF\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":95,\"seccionId\":14,\"operacionId\":24,\"nombreSeccion\":\"ControlNCF\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":115,\"seccionId\":14,\"operacionId\":25,\"nombreSeccion\":\"ControlNCF\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":8,\"nombre\":\"Factura\"},\"operaciones\":[{\"id\":33,\"seccionId\":8,\"operacionId\":1,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":34,\"seccionId\":8,\"operacionId\":2,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"Crear\",\"operacionNombreIlustrativo\":\"Crear\"},{\"id\":35,\"seccionId\":8,\"operacionId\":3,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"Modificar\",\"operacionNombreIlustrativo\":\"Modificar\"},{\"id\":37,\"seccionId\":8,\"operacionId\":21,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"CrearFacturaAntigua\",\"operacionNombreIlustrativo\":\"Crear Factura Antigua\"},{\"id\":90,\"seccionId\":8,\"operacionId\":24,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":110,\"seccionId\":8,\"operacionId\":25,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":130,\"seccionId\":8,\"operacionId\":26,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerPaginadoBasico\",\"operacionNombreIlustrativo\":\"Leer Paginado Basico\"},{\"id\":150,\"seccionId\":8,\"operacionId\":27,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerPaginadoPorActivos\",\"operacionNombreIlustrativo\":\"Leer Paginado Por Activos\"},{\"id\":170,\"seccionId\":8,\"operacionId\":28,\"nombreSeccion\":\"Factura\",\"operacionNombre\":\"LeerPaginadoAvanzado\",\"operacionNombreIlustrativo\":\"Leer Paginado Avanzado\"}]},{\"seccion\":{\"id\":7,\"nombre\":\"Impuesto\"},\"operaciones\":[{\"id\":29,\"seccionId\":7,\"operacionId\":1,\"nombreSeccion\":\"Impuesto\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":89,\"seccionId\":7,\"operacionId\":24,\"nombreSeccion\":\"Impuesto\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":109,\"seccionId\":7,\"operacionId\":25,\"nombreSeccion\":\"Impuesto\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":16,\"nombre\":\"Localidad\"},\"operaciones\":[{\"id\":58,\"seccionId\":16,\"operacionId\":1,\"nombreSeccion\":\"Localidad\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":97,\"seccionId\":16,\"operacionId\":24,\"nombreSeccion\":\"Localidad\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":117,\"seccionId\":16,\"operacionId\":25,\"nombreSeccion\":\"Localidad\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":6,\"nombre\":\"Moneda\"},\"operaciones\":[{\"id\":25,\"seccionId\":6,\"operacionId\":1,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":88,\"seccionId\":6,\"operacionId\":24,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":108,\"seccionId\":6,\"operacionId\":25,\"nombreSeccion\":\"Moneda\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":22,\"nombre\":\"Unidades\"},\"operaciones\":[{\"id\":75,\"seccionId\":22,\"operacionId\":1,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"id\":102,\"seccionId\":22,\"operacionId\":24,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":122,\"seccionId\":22,\"operacionId\":25,\"nombreSeccion\":\"Unidades\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]},{\"seccion\":{\"id\":4,\"nombre\":\"Producto\"},\"operaciones\":[{\"id\":203,\"seccionId\":4,\"operacionId\":32,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerProductoEnLocalidad\",\"operacionNombreIlustrativo\":\"Leer Producto En Localidad\"},{\"id\":201,\"seccionId\":4,\"operacionId\":29,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"},{\"id\":106,\"seccionId\":4,\"operacionId\":25,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"id\":86,\"seccionId\":4,\"operacionId\":24,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"id\":18,\"seccionId\":4,\"operacionId\":1,\"nombreSeccion\":\"Producto\",\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"}]},{\"seccion\":{\"id\":38,\"nombre\":\"Menu\"},\"operaciones\":[{\"id\":372,\"seccionId\":38,\"operacionId\":50,\"nombreSeccion\":\"Menu\",\"operacionNombre\":\"Facturacion\",\"operacionNombreIlustrativo\":\"Facturacion\"},{\"id\":374,\"seccionId\":38,\"operacionId\":52,\"nombreSeccion\":\"Menu\",\"operacionNombre\":\"Facturacion-Facturas\",\"operacionNombreIlustrativo\":\"Facturacion-Facturas\"}]},{\"seccion\":{\"id\":39,\"nombre\":\"Vendedor\"},\"operaciones\":[{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":403,\"operacionId\":1,\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":407,\"operacionId\":24,\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":411,\"operacionId\":28,\"operacionNombre\":\"LeerPaginadoAvanzado\",\"operacionNombreIlustrativo\":\"Leer Paginado Avanzado\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":412,\"operacionId\":29,\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":408,\"operacionId\":25,\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"}]}]',_binary ''),(13,12,'[{\"seccion\": {\"id\": 10, \"nombre\": \"Cliente\"}, \"operaciones\": [{\"id\": 41, \"seccionId\": 10, \"operacionId\": 1, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 200, \"seccionId\": 10, \"operacionId\": 29, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 111, \"seccionId\": 10, \"operacionId\": 25, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 25, \"nombre\": \"TipoPago\"}, \"operaciones\": [{\"id\": 204, \"seccionId\": 25, \"operacionId\": 1, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 205, \"seccionId\": 25, \"operacionId\": 24, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 5, \"nombre\": \"Pago\"}, \"operaciones\": [{\"id\": 23, \"seccionId\": 5, \"operacionId\": 20, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"AplicarPagos\", \"operacionNombreIlustrativo\": \"Aplicar Pagos\"}, {\"id\": 24, \"seccionId\": 5, \"operacionId\": 1, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 87, \"seccionId\": 5, \"operacionId\": 24, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 107, \"seccionId\": 5, \"operacionId\": 25, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 167, \"seccionId\": 5, \"operacionId\": 28, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 147, \"seccionId\": 5, \"operacionId\": 27, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 127, \"seccionId\": 5, \"operacionId\": 26, \"nombreSeccion\": \"Pago\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}]}, {\"seccion\": {\"id\": 8, \"nombre\": \"Factura\"}, \"operaciones\": [{\"id\": 39, \"seccionId\": 8, \"operacionId\": 7, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"ReporteCxC\", \"operacionNombreIlustrativo\": \"Reporte CxC\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 372, \"seccionId\": 38, \"operacionId\": 50, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Facturacion\", \"operacionNombreIlustrativo\": \"Facturacion\"}, {\"id\": 375, \"seccionId\": 38, \"operacionId\": 53, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Facturacion-Pagos\", \"operacionNombreIlustrativo\": \"Facturacion-Pagos\"}]}]',_binary ''),(14,13,'[{\"seccion\": {\"id\": 15, \"nombre\": \"Gasto\"}, \"operaciones\": [{\"id\": 53, \"seccionId\": 15, \"operacionId\": 1, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 54, \"seccionId\": 15, \"operacionId\": 2, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 55, \"seccionId\": 15, \"operacionId\": 3, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 136, \"seccionId\": 15, \"operacionId\": 26, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 116, \"seccionId\": 15, \"operacionId\": 25, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 96, \"seccionId\": 15, \"operacionId\": 24, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 156, \"seccionId\": 15, \"operacionId\": 27, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 176, \"seccionId\": 15, \"operacionId\": 28, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 206, \"seccionId\": 20, \"operacionId\": 29, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 376, \"seccionId\": 38, \"operacionId\": 54, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Gastos\", \"operacionNombreIlustrativo\": \"Gastos\"}, {\"id\": 377, \"seccionId\": 38, \"operacionId\": 55, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Gastos-Gastos\", \"operacionNombreIlustrativo\": \"Gastos-Gastos\"}]}]',_binary ''),(15,13,'[{\"seccion\": {\"id\": 15, \"nombre\": \"Gasto\"}, \"operaciones\": [{\"id\": 53, \"seccionId\": 15, \"operacionId\": 1, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 54, \"seccionId\": 15, \"operacionId\": 2, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 55, \"seccionId\": 15, \"operacionId\": 3, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 136, \"seccionId\": 15, \"operacionId\": 26, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 116, \"seccionId\": 15, \"operacionId\": 25, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 96, \"seccionId\": 15, \"operacionId\": 24, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 156, \"seccionId\": 15, \"operacionId\": 27, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 176, \"seccionId\": 15, \"operacionId\": 28, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 67, \"seccionId\": 20, \"operacionId\": 1, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 100, \"seccionId\": 20, \"operacionId\": 24, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 120, \"seccionId\": 20, \"operacionId\": 25, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}]',_binary ''),(16,14,'[{\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 206, \"seccionId\": 20, \"operacionId\": 29, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 15, \"nombre\": \"Gasto\"}, \"operaciones\": [{\"id\": 57, \"seccionId\": 15, \"operacionId\": 9, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"ReporteCxP\", \"operacionNombreIlustrativo\": \"Reporte CxP\"}]}, {\"seccion\": {\"id\": 18, \"nombre\": \"PagoGasto\"}, \"operaciones\": [{\"id\": 62, \"seccionId\": 18, \"operacionId\": 1, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 63, \"seccionId\": 18, \"operacionId\": 22, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"AplicarPagos\", \"operacionNombreIlustrativo\": \"Aplicar Pagos\"}, {\"id\": 64, \"seccionId\": 18, \"operacionId\": 2, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 65, \"seccionId\": 18, \"operacionId\": 3, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 139, \"seccionId\": 18, \"operacionId\": 26, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 99, \"seccionId\": 18, \"operacionId\": 24, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 119, \"seccionId\": 18, \"operacionId\": 25, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 159, \"seccionId\": 18, \"operacionId\": 27, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 179, \"seccionId\": 18, \"operacionId\": 28, \"nombreSeccion\": \"PagoGasto\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 25, \"nombre\": \"TipoPago\"}, \"operaciones\": [{\"id\": 204, \"seccionId\": 25, \"operacionId\": 1, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 205, \"seccionId\": 25, \"operacionId\": 24, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 376, \"seccionId\": 38, \"operacionId\": 54, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Gastos\", \"operacionNombreIlustrativo\": \"Gastos\"}, {\"id\": 378, \"seccionId\": 38, \"operacionId\": 56, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Gastos-Pagos\", \"operacionNombreIlustrativo\": \"Gastos-Pagos\"}]}]',_binary ''),(17,16,'[{\"seccion\": {\"id\": 26, \"nombre\": \"Caja\"}, \"operaciones\": [{\"id\": 207, \"seccionId\": 26, \"operacionId\": 1, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 208, \"seccionId\": 26, \"operacionId\": 2, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 209, \"seccionId\": 26, \"operacionId\": 3, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 211, \"seccionId\": 26, \"operacionId\": 24, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 212, \"seccionId\": 26, \"operacionId\": 25, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 213, \"seccionId\": 26, \"operacionId\": 26, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 214, \"seccionId\": 26, \"operacionId\": 27, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 215, \"seccionId\": 26, \"operacionId\": 28, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 117, \"seccionId\": 16, \"operacionId\": 25, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 369, \"seccionId\": 38, \"operacionId\": 47, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Cajas\", \"operacionNombreIlustrativo\": \"Cajas\"}, {\"id\": 370, \"seccionId\": 38, \"operacionId\": 48, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Cajas-Mantenimiento\", \"operacionNombreIlustrativo\": \"Cajas-Mantenimiento\"}]}]',_binary ''),(18,17,'[{\"seccion\": {\"id\": 23, \"nombre\": \"Usuarios\"}, \"operaciones\": [{\"id\": 79, \"seccionId\": 23, \"operacionId\": 1, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 103, \"seccionId\": 23, \"operacionId\": 24, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 26, \"nombre\": \"Caja\"}, \"operaciones\": [{\"id\": 207, \"seccionId\": 26, \"operacionId\": 1, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 211, \"seccionId\": 26, \"operacionId\": 24, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 27, \"nombre\": \"AperturaCaja\"}, \"operaciones\": [{\"id\": 222, \"seccionId\": 27, \"operacionId\": 1, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 223, \"seccionId\": 27, \"operacionId\": 2, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 224, \"seccionId\": 27, \"operacionId\": 3, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 226, \"seccionId\": 27, \"operacionId\": 24, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 227, \"seccionId\": 27, \"operacionId\": 25, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 228, \"seccionId\": 27, \"operacionId\": 26, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 229, \"seccionId\": 27, \"operacionId\": 27, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 230, \"seccionId\": 27, \"operacionId\": 28, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 237, \"seccionId\": 27, \"operacionId\": 33, \"nombreSeccion\": \"AperturaCaja\", \"operacionNombre\": \"cerrarCaja\", \"operacionNombreIlustrativo\": \"Cerrar Caja\"}]}, {\"seccion\": {\"id\": 28, \"nombre\": \"montosapertura\"}, \"operaciones\": [{\"id\": 238, \"seccionId\": 28, \"operacionId\": 1, \"nombreSeccion\": \"montosapertura\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 239, \"seccionId\": 28, \"operacionId\": 24, \"nombreSeccion\": \"montosapertura\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 25, \"nombre\": \"TipoPago\"}, \"operaciones\": [{\"id\": 205, \"seccionId\": 25, \"operacionId\": 24, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 204, \"seccionId\": 25, \"operacionId\": 1, \"nombreSeccion\": \"TipoPago\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 369, \"seccionId\": 38, \"operacionId\": 47, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Cajas\", \"operacionNombreIlustrativo\": \"Cajas\"}, {\"id\": 371, \"seccionId\": 38, \"operacionId\": 49, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Cajas-AperturaCierre\", \"operacionNombreIlustrativo\": \"Cajas-AperturaCierre\"}]}]',_binary ''),(19,18,'[{\"seccion\": {\"id\": 8, \"nombre\": \"Factura\"}, \"operaciones\": [{\"id\": 38, \"seccionId\": 8, \"operacionId\": 6, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"ReporteDeVentas\", \"operacionNombreIlustrativo\": \"Reporte De Ventas\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 380, \"seccionId\": 38, \"operacionId\": 58, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-ReporteVentas\", \"operacionNombreIlustrativo\": \"Reportes-ReporteVentas\"}]}]',_binary ''),(20,19,'[{\"seccion\": {\"id\": 8, \"nombre\": \"Factura\"}, \"operaciones\": [{\"id\": 39, \"seccionId\": 8, \"operacionId\": 7, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"ReporteCxC\", \"operacionNombreIlustrativo\": \"Reporte CxC\"}]}, {\"seccion\": {\"id\": 10, \"nombre\": \"Cliente\"}, \"operaciones\": [{\"id\": 200, \"seccionId\": 10, \"operacionId\": 29, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 381, \"seccionId\": 38, \"operacionId\": 59, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-CxCGeneral\", \"operacionNombreIlustrativo\": \"Reportes-CxCGeneral\"}]}]',_binary ''),(21,25,'[{\"seccion\": {\"id\": 15, \"nombre\": \"Gasto\"}, \"operaciones\": [{\"id\": 57, \"seccionId\": 15, \"operacionId\": 9, \"nombreSeccion\": \"Gasto\", \"operacionNombre\": \"ReporteCxP\", \"operacionNombreIlustrativo\": \"Reporte CxP\"}]}, {\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 206, \"seccionId\": 20, \"operacionId\": 29, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 67, \"seccionId\": 20, \"operacionId\": 1, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 384, \"seccionId\": 38, \"operacionId\": 62, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-reporteCxP\", \"operacionNombreIlustrativo\": \"Reportes-reporteCxP\"}]}]',_binary ''),(22,26,'[{\"seccion\": {\"id\": 10, \"nombre\": \"Cliente\"}, \"operaciones\": [{\"id\": 356, \"seccionId\": 10, \"operacionId\": 35, \"nombreSeccion\": \"Cliente\", \"operacionNombre\": \"ClientesFiltrados\", \"operacionNombreIlustrativo\": \"Listado de clientes\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 386, \"seccionId\": 38, \"operacionId\": 64, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-listadoClientes\", \"operacionNombreIlustrativo\": \"Reportes-listadoClientes\"}]}]',_binary ''),(23,27,'[{\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 355, \"seccionId\": 4, \"operacionId\": 34, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"productosFiltrados\", \"operacionNombreIlustrativo\": \"Listado de precios\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 385, \"seccionId\": 38, \"operacionId\": 63, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-listadoPrecios\", \"operacionNombreIlustrativo\": \"Reportes-listadoPrecios\"}]}]',_binary ''),(24,28,'[{\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 67, \"seccionId\": 20, \"operacionId\": 1, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 120, \"seccionId\": 20, \"operacionId\": 25, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 206, \"seccionId\": 20, \"operacionId\": 29, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 6, \"nombre\": \"Moneda\"}, \"operaciones\": [{\"id\": 25, \"seccionId\": 6, \"operacionId\": 1, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 108, \"seccionId\": 6, \"operacionId\": 25, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 88, \"seccionId\": 6, \"operacionId\": 24, \"nombreSeccion\": \"Moneda\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 122, \"seccionId\": 22, \"operacionId\": 25, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 117, \"seccionId\": 16, \"operacionId\": 25, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 112, \"seccionId\": 11, \"operacionId\": 25, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 18, \"seccionId\": 4, \"operacionId\": 1, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 106, \"seccionId\": 4, \"operacionId\": 25, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 201, \"seccionId\": 4, \"operacionId\": 29, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 29, \"nombre\": \"MovimientoEntrada\"}, \"operaciones\": [{\"id\": 240, \"seccionId\": 29, \"operacionId\": 1, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 242, \"seccionId\": 29, \"operacionId\": 13, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"RegistrarEntradas\", \"operacionNombreIlustrativo\": \"Registrar Entradas\"}, {\"id\": 246, \"seccionId\": 29, \"operacionId\": 14, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"ImprimirEntrada\", \"operacionNombreIlustrativo\": \"Imprimir Entrada\"}, {\"id\": 249, \"seccionId\": 29, \"operacionId\": 24, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 250, \"seccionId\": 29, \"operacionId\": 25, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 251, \"seccionId\": 29, \"operacionId\": 26, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 252, \"seccionId\": 29, \"operacionId\": 27, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 253, \"seccionId\": 29, \"operacionId\": 28, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 300, \"seccionId\": 29, \"operacionId\": 2, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 301, \"seccionId\": 29, \"operacionId\": 3, \"nombreSeccion\": \"MovimientoEntrada\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}]}, {\"seccion\": {\"id\": 7, \"nombre\": \"Impuesto\"}, \"operaciones\": [{\"id\": 302, \"seccionId\": 7, \"operacionId\": 29, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 29, \"seccionId\": 7, \"operacionId\": 1, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 109, \"seccionId\": 7, \"operacionId\": 25, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 89, \"seccionId\": 7, \"operacionId\": 24, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 33, \"nombre\": \"ProductoUnidades\"}, \"operaciones\": [{\"id\": 303, \"seccionId\": 33, \"operacionId\": 1, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 387, \"seccionId\": 38, \"operacionId\": 65, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos\", \"operacionNombreIlustrativo\": \"Movimientos\"}, {\"id\": 388, \"seccionId\": 38, \"operacionId\": 66, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos-MovimientoEntrada\", \"operacionNombreIlustrativo\": \"Movimientos-MovimientoEntrada\"}]}]',_binary ''),(25,29,'[{\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 201, \"seccionId\": 4, \"operacionId\": 29, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 33, \"nombre\": \"ProductoUnidades\"}, \"operaciones\": [{\"id\": 303, \"seccionId\": 33, \"operacionId\": 1, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 117, \"seccionId\": 16, \"operacionId\": 25, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 112, \"seccionId\": 11, \"operacionId\": 25, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 32, \"nombre\": \"TransferenciaAlmacen\"}, \"operaciones\": [{\"id\": 285, \"seccionId\": 32, \"operacionId\": 1, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 292, \"seccionId\": 32, \"operacionId\": 19, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"RegistrarTransferencias\", \"operacionNombreIlustrativo\": \"Registrar Transferencias\"}, {\"id\": 293, \"seccionId\": 32, \"operacionId\": 16, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"ImprimirTransferencia\", \"operacionNombreIlustrativo\": \"Imprimir Transferencia\"}, {\"id\": 294, \"seccionId\": 32, \"operacionId\": 24, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 295, \"seccionId\": 32, \"operacionId\": 25, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 296, \"seccionId\": 32, \"operacionId\": 26, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 297, \"seccionId\": 32, \"operacionId\": 27, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 298, \"seccionId\": 32, \"operacionId\": 28, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 318, \"seccionId\": 32, \"operacionId\": 29, \"nombreSeccion\": \"TransferenciaAlmacen\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 13, \"nombre\": \"AlmacenProducto\"}, \"operaciones\": [{\"id\": 320, \"seccionId\": 13, \"operacionId\": 1, \"nombreSeccion\": \"AlmacenProducto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 122, \"seccionId\": 22, \"operacionId\": 25, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 387, \"seccionId\": 38, \"operacionId\": 65, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos\", \"operacionNombreIlustrativo\": \"Movimientos\"}, {\"id\": 389, \"seccionId\": 38, \"operacionId\": 67, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos-TransferenciaAlmacen\", \"operacionNombreIlustrativo\": \"Movimientos-TransferenciaAlmacen\"}]}]',_binary ''),(26,30,'[{\"seccion\": {\"id\": 8, \"nombre\": \"Factura\"}, \"operaciones\": [{\"id\": 110, \"seccionId\": 8, \"operacionId\": 25, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 321, \"seccionId\": 8, \"operacionId\": 29, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 33, \"seccionId\": 8, \"operacionId\": 1, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 31, \"nombre\": \"MovimientoDevolucionCliente\"}, \"operaciones\": [{\"id\": 272, \"seccionId\": 31, \"operacionId\": 13, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"RegistrarEntradas\", \"operacionNombreIlustrativo\": \"Registrar Entradas\"}, {\"id\": 275, \"seccionId\": 31, \"operacionId\": 11, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"RegistrarSalidas\", \"operacionNombreIlustrativo\": \"Registrar Salidas\"}, {\"id\": 276, \"seccionId\": 31, \"operacionId\": 14, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"ImprimirEntrada\", \"operacionNombreIlustrativo\": \"Imprimir Entrada\"}, {\"id\": 277, \"seccionId\": 31, \"operacionId\": 19, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"RegistrarTransferencias\", \"operacionNombreIlustrativo\": \"Registrar Transferencias\"}, {\"id\": 278, \"seccionId\": 31, \"operacionId\": 16, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"ImprimirTransferencia\", \"operacionNombreIlustrativo\": \"Imprimir Transferencia\"}, {\"id\": 270, \"seccionId\": 31, \"operacionId\": 1, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 271, \"seccionId\": 31, \"operacionId\": 18, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"IngresarDevolucionCliente\", \"operacionNombreIlustrativo\": \"Ingresar Devolucion Cliente\"}, {\"id\": 274, \"seccionId\": 31, \"operacionId\": 12, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"ImprimirDevolucion\", \"operacionNombreIlustrativo\": \"Imprimir Devolucion\"}, {\"id\": 279, \"seccionId\": 31, \"operacionId\": 24, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 280, \"seccionId\": 31, \"operacionId\": 25, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 281, \"seccionId\": 31, \"operacionId\": 26, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 282, \"seccionId\": 31, \"operacionId\": 27, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 283, \"seccionId\": 31, \"operacionId\": 28, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 322, \"seccionId\": 31, \"operacionId\": 29, \"nombreSeccion\": \"MovimientoDevolucionCliente\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 387, \"seccionId\": 38, \"operacionId\": 65, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos\", \"operacionNombreIlustrativo\": \"Movimientos\"}, {\"id\": 390, \"seccionId\": 38, \"operacionId\": 68, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos-DevolucionCliente\", \"operacionNombreIlustrativo\": \"Movimientos-DevolucionCliente\"}]}]',_binary ''),(27,31,'[{\"seccion\": {\"id\": 30, \"nombre\": \"MovimientoDevolucion\"}, \"operaciones\": [{\"id\": 257, \"seccionId\": 30, \"operacionId\": 13, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"RegistrarEntradas\", \"operacionNombreIlustrativo\": \"Registrar Entradas\"}, {\"id\": 260, \"seccionId\": 30, \"operacionId\": 11, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"RegistrarSalidas\", \"operacionNombreIlustrativo\": \"Registrar Salidas\"}, {\"id\": 261, \"seccionId\": 30, \"operacionId\": 14, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"ImprimirEntrada\", \"operacionNombreIlustrativo\": \"Imprimir Entrada\"}, {\"id\": 262, \"seccionId\": 30, \"operacionId\": 19, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"RegistrarTransferencias\", \"operacionNombreIlustrativo\": \"Registrar Transferencias\"}, {\"id\": 263, \"seccionId\": 30, \"operacionId\": 16, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"ImprimirTransferencia\", \"operacionNombreIlustrativo\": \"Imprimir Transferencia\"}, {\"id\": 255, \"seccionId\": 30, \"operacionId\": 1, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 256, \"seccionId\": 30, \"operacionId\": 18, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"IngresarDevolucionCliente\", \"operacionNombreIlustrativo\": \"Ingresar Devolucion Cliente\"}, {\"id\": 259, \"seccionId\": 30, \"operacionId\": 12, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"ImprimirDevolucion\", \"operacionNombreIlustrativo\": \"Imprimir Devolucion\"}, {\"id\": 264, \"seccionId\": 30, \"operacionId\": 24, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 265, \"seccionId\": 30, \"operacionId\": 25, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 266, \"seccionId\": 30, \"operacionId\": 26, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 267, \"seccionId\": 30, \"operacionId\": 27, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 268, \"seccionId\": 30, \"operacionId\": 28, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 323, \"seccionId\": 30, \"operacionId\": 29, \"nombreSeccion\": \"MovimientoDevolucion\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 201, \"seccionId\": 4, \"operacionId\": 29, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 33, \"nombre\": \"ProductoUnidades\"}, \"operaciones\": [{\"id\": 312, \"seccionId\": 33, \"operacionId\": 29, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 303, \"seccionId\": 33, \"operacionId\": 1, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 308, \"seccionId\": 33, \"operacionId\": 25, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 307, \"seccionId\": 33, \"operacionId\": 24, \"nombreSeccion\": \"ProductoUnidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 7, \"nombre\": \"Impuesto\"}, \"operaciones\": [{\"id\": 302, \"seccionId\": 7, \"operacionId\": 29, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 89, \"seccionId\": 7, \"operacionId\": 24, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 29, \"seccionId\": 7, \"operacionId\": 1, \"nombreSeccion\": \"Impuesto\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 20, \"nombre\": \"Suplidor\"}, \"operaciones\": [{\"id\": 206, \"seccionId\": 20, \"operacionId\": 29, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 120, \"seccionId\": 20, \"operacionId\": 25, \"nombreSeccion\": \"Suplidor\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}]}, {\"seccion\": {\"id\": 22, \"nombre\": \"Unidades\"}, \"operaciones\": [{\"id\": 75, \"seccionId\": 22, \"operacionId\": 1, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 102, \"seccionId\": 22, \"operacionId\": 24, \"nombreSeccion\": \"Unidades\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 387, \"seccionId\": 38, \"operacionId\": 65, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos\", \"operacionNombreIlustrativo\": \"Movimientos\"}, {\"id\": 391, \"seccionId\": 38, \"operacionId\": 69, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Movimientos-MovimientoDevolucion\", \"operacionNombreIlustrativo\": \"Movimientos-MovimientoDevolucion\"}]}]',_binary ''),(28,32,'[{\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 26, \"nombre\": \"Caja\"}, \"operaciones\": [{\"id\": 207, \"seccionId\": 26, \"operacionId\": 1, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 211, \"seccionId\": 26, \"operacionId\": 24, \"nombreSeccion\": \"Caja\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 23, \"nombre\": \"Usuarios\"}, \"operaciones\": [{\"id\": 79, \"seccionId\": 23, \"operacionId\": 1, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 80, \"seccionId\": 23, \"operacionId\": 2, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 81, \"seccionId\": 23, \"operacionId\": 3, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 83, \"seccionId\": 23, \"operacionId\": 17, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"CambiarContrasena\", \"operacionNombreIlustrativo\": \"Cambiar Contrasena\"}, {\"id\": 103, \"seccionId\": 23, \"operacionId\": 24, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 123, \"seccionId\": 23, \"operacionId\": 25, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 143, \"seccionId\": 23, \"operacionId\": 26, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 163, \"seccionId\": 23, \"operacionId\": 27, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 183, \"seccionId\": 23, \"operacionId\": 28, \"nombreSeccion\": \"Usuarios\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 35, \"nombre\": \"Roles\"}, \"operaciones\": [{\"id\": 324, \"seccionId\": 35, \"operacionId\": 1, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 330, \"seccionId\": 35, \"operacionId\": 25, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 329, \"seccionId\": 35, \"operacionId\": 24, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 340, \"seccionId\": 35, \"operacionId\": 29, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}, {\"id\": 339, \"seccionId\": 35, \"operacionId\": 30, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"AsignarRoleUsuario\", \"operacionNombreIlustrativo\": \"Asignar Rol Usuario\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 392, \"seccionId\": 38, \"operacionId\": 70, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Usuarios\", \"operacionNombreIlustrativo\": \"Usuarios\"}, {\"id\": 393, \"seccionId\": 38, \"operacionId\": 71, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Usuarios-Usuarios\", \"operacionNombreIlustrativo\": \"Usuarios-Usuarios\"}]}]',_binary ''),(29,33,'[{\"seccion\": {\"id\": 35, \"nombre\": \"Roles\"}, \"operaciones\": [{\"id\": 324, \"seccionId\": 35, \"operacionId\": 1, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 325, \"seccionId\": 35, \"operacionId\": 2, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"Crear\", \"operacionNombreIlustrativo\": \"Crear\"}, {\"id\": 326, \"seccionId\": 35, \"operacionId\": 3, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"Modificar\", \"operacionNombreIlustrativo\": \"Modificar\"}, {\"id\": 329, \"seccionId\": 35, \"operacionId\": 24, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 330, \"seccionId\": 35, \"operacionId\": 25, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 331, \"seccionId\": 35, \"operacionId\": 26, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 332, \"seccionId\": 35, \"operacionId\": 27, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 333, \"seccionId\": 35, \"operacionId\": 28, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 339, \"seccionId\": 35, \"operacionId\": 30, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"AsignarRoleUsuario\", \"operacionNombreIlustrativo\": \"Asignar Rol Usuario\"}, {\"id\": 340, \"seccionId\": 35, \"operacionId\": 29, \"nombreSeccion\": \"Roles\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 36, \"nombre\": \"Secciones\"}, \"operaciones\": [{\"id\": 341, \"seccionId\": 36, \"operacionId\": 1, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 342, \"seccionId\": 36, \"operacionId\": 24, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 343, \"seccionId\": 36, \"operacionId\": 25, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 344, \"seccionId\": 36, \"operacionId\": 26, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 345, \"seccionId\": 36, \"operacionId\": 27, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 346, \"seccionId\": 36, \"operacionId\": 28, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 347, \"seccionId\": 36, \"operacionId\": 29, \"nombreSeccion\": \"Secciones\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 37, \"nombre\": \"Operaciones\"}, \"operaciones\": [{\"id\": 348, \"seccionId\": 37, \"operacionId\": 1, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 349, \"seccionId\": 37, \"operacionId\": 24, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 350, \"seccionId\": 37, \"operacionId\": 25, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 351, \"seccionId\": 37, \"operacionId\": 26, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 352, \"seccionId\": 37, \"operacionId\": 27, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerPaginadoPorActivos\", \"operacionNombreIlustrativo\": \"Leer Paginado Por Activos\"}, {\"id\": 353, \"seccionId\": 37, \"operacionId\": 28, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}, {\"id\": 354, \"seccionId\": 37, \"operacionId\": 29, \"nombreSeccion\": \"Operaciones\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 1, \"nombre\": \"Permisos\"}, \"operaciones\": [{\"id\": 1, \"seccionId\": 1, \"operacionId\": 1, \"nombreSeccion\": \"Permisos\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 84, \"seccionId\": 1, \"operacionId\": 24, \"nombreSeccion\": \"Permisos\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 104, \"seccionId\": 1, \"operacionId\": 25, \"nombreSeccion\": \"Permisos\", \"operacionNombre\": \"LeerPorId\", \"operacionNombreIlustrativo\": \"Leer Por Id\"}, {\"id\": 124, \"seccionId\": 1, \"operacionId\": 26, \"nombreSeccion\": \"Permisos\", \"operacionNombre\": \"LeerPaginadoBasico\", \"operacionNombreIlustrativo\": \"Leer Paginado Basico\"}, {\"id\": 164, \"seccionId\": 1, \"operacionId\": 28, \"nombreSeccion\": \"Permisos\", \"operacionNombre\": \"LeerPaginadoAvanzado\", \"operacionNombreIlustrativo\": \"Leer Paginado Avanzado\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 392, \"seccionId\": 38, \"operacionId\": 70, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Usuarios\", \"operacionNombreIlustrativo\": \"Usuarios\"}, {\"id\": 394, \"seccionId\": 38, \"operacionId\": 72, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Usuarios-Roles\", \"operacionNombreIlustrativo\": \"Usuarios-Roles\"}]}]',_binary ''),(31,22,'[{\"seccion\": {\"id\": 4, \"nombre\": \"Producto\"}, \"operaciones\": [{\"id\": 22, \"seccionId\": 4, \"operacionId\": 15, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"MovimientosProducto\", \"operacionNombreIlustrativo\": \"Movimientos Producto\"}, {\"id\": 201, \"seccionId\": 4, \"operacionId\": 29, \"nombreSeccion\": \"Producto\", \"operacionNombre\": \"LeerPorFiltro\", \"operacionNombreIlustrativo\": \"Leer Por Filtro\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 383, \"seccionId\": 38, \"operacionId\": 61, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-movimientosProducto\", \"operacionNombreIlustrativo\": \"Reportes-movimientosProducto\"}]}]',_binary ''),(32,21,'[{\"seccion\": {\"id\": 11, \"nombre\": \"Almacen\"}, \"operaciones\": [{\"id\": 45, \"seccionId\": 11, \"operacionId\": 1, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}, {\"id\": 92, \"seccionId\": 11, \"operacionId\": 24, \"nombreSeccion\": \"Almacen\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}]}, {\"seccion\": {\"id\": 16, \"nombre\": \"Localidad\"}, \"operaciones\": [{\"id\": 358, \"seccionId\": 16, \"operacionId\": 36, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"InventarioAlmacenes\", \"operacionNombreIlustrativo\": \"Inventario Almacenes\"}, {\"id\": 97, \"seccionId\": 16, \"operacionId\": 24, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"LeerTodos\", \"operacionNombreIlustrativo\": \"Leer Todos\"}, {\"id\": 58, \"seccionId\": 16, \"operacionId\": 1, \"nombreSeccion\": \"Localidad\", \"operacionNombre\": \"Leer\", \"operacionNombreIlustrativo\": \"Leer\"}]}, {\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 379, \"seccionId\": 38, \"operacionId\": 57, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes\", \"operacionNombreIlustrativo\": \"Reportes\"}, {\"id\": 382, \"seccionId\": 38, \"operacionId\": 60, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-ReporteInventario\", \"operacionNombreIlustrativo\": \"Reportes-ReporteInventario\"}]}]',_binary ''),(33,34,'[{\"seccion\": {\"id\": 38, \"nombre\": \"Menu\"}, \"operaciones\": [{\"id\": 395, \"seccionId\": 38, \"operacionId\": 73, \"nombreSeccion\": \"Menu\", \"operacionNombre\": \"Reportes-estadoCuentaClientes\", \"operacionNombreIlustrativo\": \"Estado de Cuenta\"}]}, {\"seccion\": {\"id\": 8, \"nombre\": \"Factura\"}, \"operaciones\": [{\"id\": 396, \"seccionId\": 8, \"operacionId\": 74, \"nombreSeccion\": \"Factura\", \"operacionNombre\": \"EstadoCuenta\", \"operacionNombreIlustrativo\": \"Estado de Cuenta\"}]}]',_binary ''),(34,35,'[{\"seccion\":{\"id\":38,\"nombre\":\"Menu\"},\"operaciones\":[{\"seccionId\":38,\"nombreSeccion\":\"Menu\",\"id\":424,\"operacionId\":76,\"operacionNombre\":\"Mantenimiento-Vendedores\",\"operacionNombreIlustrativo\":\"Mantenimiento de vendedores\"}]},{\"seccion\":{\"id\":39,\"nombre\":\"Vendedor\"},\"operaciones\":[{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":403,\"operacionId\":1,\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":407,\"operacionId\":24,\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":411,\"operacionId\":28,\"operacionNombre\":\"LeerPaginadoAvanzado\",\"operacionNombreIlustrativo\":\"Leer Paginado Avanzado\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":412,\"operacionId\":29,\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":408,\"operacionId\":25,\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":404,\"operacionId\":2,\"operacionNombre\":\"Crear\",\"operacionNombreIlustrativo\":\"Crear\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":405,\"operacionId\":3,\"operacionNombre\":\"Modificar\",\"operacionNombreIlustrativo\":\"Modificar\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":409,\"operacionId\":26,\"operacionNombre\":\"LeerPaginadoBasico\",\"operacionNombreIlustrativo\":\"Leer Paginado Basico\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":406,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"},{\"seccionId\":39,\"nombreSeccion\":\"Vendedor\",\"id\":410,\"operacionId\":27,\"operacionNombre\":\"LeerPaginadoPorActivos\",\"operacionNombreIlustrativo\":\"Leer Paginado Por Activos\"}]}]',_binary ''),(35,37,'[{\"seccion\":{\"id\":38,\"nombre\":\"Menu\"},\"operaciones\":[{\"seccionId\":38,\"nombreSeccion\":\"Menu\",\"id\":423,\"operacionId\":75,\"operacionNombre\":\"Mantenimiento-Zonas\",\"operacionNombreIlustrativo\":\"Mantenimiento de zonas\"}]}]',_binary ''),(36,37,'[{\"seccion\":{\"id\":38,\"nombre\":\"Menu\"},\"operaciones\":[{\"seccionId\":38,\"nombreSeccion\":\"Menu\",\"id\":423,\"operacionId\":75,\"operacionNombre\":\"Mantenimiento-Zonas\",\"operacionNombreIlustrativo\":\"Mantenimiento de zonas\"}]},{\"seccion\":{\"id\":40,\"nombre\":\"Zona\"},\"operaciones\":[{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":413,\"operacionId\":1,\"operacionNombre\":\"Leer\",\"operacionNombreIlustrativo\":\"Leer\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":417,\"operacionId\":24,\"operacionNombre\":\"LeerTodos\",\"operacionNombreIlustrativo\":\"Leer Todos\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":414,\"operacionId\":2,\"operacionNombre\":\"Crear\",\"operacionNombreIlustrativo\":\"Crear\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":415,\"operacionId\":3,\"operacionNombre\":\"Modificar\",\"operacionNombreIlustrativo\":\"Modificar\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":416,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":418,\"operacionId\":25,\"operacionNombre\":\"LeerPorId\",\"operacionNombreIlustrativo\":\"Leer Por Id\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":419,\"operacionId\":26,\"operacionNombre\":\"LeerPaginadoBasico\",\"operacionNombreIlustrativo\":\"Leer Paginado Basico\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":420,\"operacionId\":27,\"operacionNombre\":\"LeerPaginadoPorActivos\",\"operacionNombreIlustrativo\":\"Leer Paginado Por Activos\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":421,\"operacionId\":28,\"operacionNombre\":\"LeerPaginadoAvanzado\",\"operacionNombreIlustrativo\":\"Leer Paginado Avanzado\"},{\"seccionId\":40,\"nombreSeccion\":\"Zona\",\"id\":422,\"operacionId\":29,\"operacionNombre\":\"LeerPorFiltro\",\"operacionNombreIlustrativo\":\"Leer Por Filtro\"}]}]',_binary ''),(37,38,'[]',_binary ''),(38,38,'[{\"seccion\":{\"id\":8,\"nombre\":\"Factura\"},\"operaciones\":[{\"seccionId\":8,\"nombreSeccion\":\"Factura\",\"id\":36,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"}]}]',_binary ''),(39,39,'[{\"seccion\":{\"id\":24,\"nombre\":\"Cotizacion\"},\"operaciones\":[{\"seccionId\":24,\"nombreSeccion\":\"Cotizacion\",\"id\":188,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"}]}]',_binary ''),(40,40,'[{\"seccion\":{\"id\":26,\"nombre\":\"Caja\"},\"operaciones\":[{\"seccionId\":26,\"nombreSeccion\":\"Caja\",\"id\":210,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"}]}]',_binary ''),(41,41,'[{\"seccion\":{\"id\":15,\"nombre\":\"Gasto\"},\"operaciones\":[{\"seccionId\":15,\"nombreSeccion\":\"Gasto\",\"id\":56,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"}]}]',_binary ''),(42,42,'[{\"seccion\":{\"id\":18,\"nombre\":\"PagoGasto\"},\"operaciones\":[{\"seccionId\":18,\"nombreSeccion\":\"PagoGasto\",\"id\":66,\"operacionId\":4,\"operacionNombre\":\"Borrar\",\"operacionNombreIlustrativo\":\"Borrar\"}]}]',_binary '');
/*!40000 ALTER TABLE `roleseccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolestree`
--

DROP TABLE IF EXISTS `rolestree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rolestree` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ParentRoleId` int(11) NOT NULL,
  `ChildRoleId` int(11) NOT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolestree`
--

LOCK TABLES `rolestree` WRITE;
/*!40000 ALTER TABLE `rolestree` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolestree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salidaproductos`
--

DROP TABLE IF EXISTS `salidaproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salidaproductos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `ClienteId` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `UnidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `salidaproductos_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `salidaproductos_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `salidaproductos_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salidaproductos`
--

LOCK TABLES `salidaproductos` WRITE;
/*!40000 ALTER TABLE `salidaproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `salidaproductos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secciones`
--

DROP TABLE IF EXISTS `secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `secciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secciones`
--

LOCK TABLES `secciones` WRITE;
/*!40000 ALTER TABLE `secciones` DISABLE KEYS */;
INSERT INTO `secciones` VALUES (1,'Permisos'),(2,'TarifarioCliente'),(4,'Producto'),(5,'Pago'),(6,'Moneda'),(7,'Impuesto'),(8,'Factura'),(10,'Cliente'),(11,'Almacen'),(12,'Todas'),(13,'AlmacenProducto'),(14,'ControlNCF'),(15,'Gasto'),(16,'Localidad'),(18,'PagoGasto'),(20,'Suplidor'),(21,'TarifarioCliente'),(22,'Unidades'),(23,'Usuarios'),(24,'Cotizacion'),(25,'TipoPago'),(26,'Caja'),(27,'AperturaCaja'),(28,'montosapertura'),(29,'MovimientoEntrada'),(30,'MovimientoDevolucion'),(31,'MovimientoDevolucionCliente'),(32,'TransferenciaAlmacen'),(33,'ProductoUnidades'),(35,'Roles'),(36,'Secciones'),(37,'Operaciones'),(38,'Menu'),(39,'Vendedor'),(40,'Zona');
/*!40000 ALTER TABLE `secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seccionoperaciones`
--

DROP TABLE IF EXISTS `seccionoperaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seccionoperaciones` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `SeccionId` int(11) NOT NULL,
  `OperacionId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=425 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seccionoperaciones`
--

LOCK TABLES `seccionoperaciones` WRITE;
/*!40000 ALTER TABLE `seccionoperaciones` DISABLE KEYS */;
INSERT INTO `seccionoperaciones` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,2,1),(6,2,2),(7,2,3),(8,2,4),(18,4,1),(19,4,2),(20,4,3),(21,4,4),(22,4,15),(23,5,20),(24,5,1),(25,6,1),(26,6,2),(27,6,3),(28,6,4),(29,7,1),(30,7,2),(31,7,3),(32,7,4),(33,8,1),(34,8,2),(35,8,3),(36,8,4),(37,8,21),(38,8,6),(39,8,7),(40,8,8),(41,10,1),(42,10,2),(43,10,3),(44,10,4),(45,11,1),(46,11,2),(47,11,3),(48,11,4),(49,14,1),(50,14,2),(51,14,3),(52,14,4),(53,15,1),(54,15,2),(55,15,3),(56,15,4),(57,15,9),(58,16,1),(59,16,2),(60,16,3),(61,16,4),(62,18,1),(63,18,22),(64,18,2),(65,18,3),(66,18,4),(67,20,1),(68,20,2),(69,20,3),(70,20,4),(71,21,1),(72,21,2),(73,21,3),(74,21,4),(75,22,1),(76,22,2),(77,22,3),(78,22,4),(79,23,1),(80,23,2),(81,23,3),(82,23,4),(83,23,17),(84,1,24),(85,2,24),(86,4,24),(87,5,24),(88,6,24),(89,7,24),(90,8,24),(91,10,24),(92,11,24),(93,12,24),(94,13,24),(95,14,24),(96,15,24),(97,16,24),(99,18,24),(100,20,24),(101,21,24),(102,22,24),(103,23,24),(104,1,25),(105,2,25),(106,4,25),(107,5,25),(108,6,25),(109,7,25),(110,8,25),(111,10,25),(112,11,25),(113,12,25),(114,13,25),(115,14,25),(116,15,25),(117,16,25),(119,18,25),(120,20,25),(121,21,25),(122,22,25),(123,23,25),(124,1,26),(125,2,26),(126,4,26),(127,5,26),(128,6,26),(129,7,26),(130,8,26),(131,10,26),(132,11,26),(133,12,26),(134,13,26),(135,14,26),(136,15,26),(137,16,26),(139,18,26),(140,20,26),(141,21,26),(142,22,26),(143,23,26),(144,1,27),(145,2,27),(146,4,27),(147,5,27),(148,6,27),(149,7,27),(150,8,27),(151,10,27),(152,11,27),(153,12,27),(154,13,27),(155,14,27),(156,15,27),(157,16,27),(159,18,27),(160,20,27),(161,21,27),(162,22,27),(163,23,27),(164,1,28),(165,2,28),(166,4,28),(167,5,28),(168,6,28),(169,7,28),(170,8,28),(171,10,28),(172,11,28),(173,12,28),(174,13,28),(175,14,28),(176,15,28),(177,16,28),(179,18,28),(180,20,28),(181,21,28),(182,22,28),(183,23,28),(185,24,1),(186,24,2),(187,24,3),(188,24,4),(189,24,21),(190,24,6),(191,24,7),(192,24,8),(193,24,24),(194,24,25),(195,24,26),(196,24,27),(197,24,28),(200,10,29),(201,4,29),(202,24,31),(203,4,32),(204,25,1),(205,25,24),(206,20,29),(207,26,1),(208,26,2),(209,26,3),(210,26,4),(211,26,24),(212,26,25),(213,26,26),(214,26,27),(215,26,28),(222,27,1),(223,27,2),(224,27,3),(225,27,4),(226,27,24),(227,27,25),(228,27,26),(229,27,27),(230,27,28),(237,27,33),(238,28,1),(239,28,24),(240,29,1),(242,29,13),(243,29,4),(246,29,14),(249,29,24),(250,29,25),(251,29,26),(252,29,27),(253,29,28),(255,30,1),(256,30,18),(258,30,4),(259,30,12),(264,30,24),(265,30,25),(266,30,26),(267,30,27),(268,30,28),(270,31,1),(271,31,18),(273,31,4),(274,31,12),(279,31,24),(280,31,25),(281,31,26),(282,31,27),(283,31,28),(285,32,1),(288,32,4),(292,32,19),(293,32,16),(294,32,24),(295,32,25),(296,32,26),(297,32,27),(298,32,28),(300,29,2),(301,29,3),(302,7,29),(303,33,1),(304,33,2),(305,33,3),(306,33,4),(307,33,24),(308,33,25),(309,33,26),(310,33,27),(311,33,28),(312,33,29),(318,32,29),(320,13,1),(321,8,29),(322,31,29),(323,30,29),(324,35,1),(325,35,2),(326,35,3),(327,35,4),(329,35,24),(330,35,25),(331,35,26),(332,35,27),(333,35,28),(339,35,30),(340,35,29),(341,36,1),(342,36,24),(343,36,25),(344,36,26),(345,36,27),(346,36,28),(347,36,29),(348,37,1),(349,37,24),(350,37,25),(351,37,26),(352,37,27),(353,37,28),(354,37,29),(355,4,34),(356,10,35),(357,11,29),(358,16,36),(359,38,37),(360,38,38),(361,38,39),(362,38,40),(363,38,41),(364,38,42),(365,38,43),(366,38,44),(367,38,45),(368,38,46),(369,38,47),(370,38,48),(371,38,49),(372,38,50),(373,38,51),(374,38,52),(375,38,53),(376,38,54),(377,38,55),(378,38,56),(379,38,57),(380,38,58),(381,38,59),(382,38,60),(383,38,61),(384,38,62),(385,38,63),(386,38,64),(387,38,65),(388,38,66),(389,38,67),(390,38,68),(391,38,69),(392,38,70),(393,38,71),(394,38,72),(395,38,73),(396,8,74),(403,39,1),(404,39,2),(405,39,3),(406,39,4),(407,39,24),(408,39,25),(409,39,26),(410,39,27),(411,39,28),(412,39,29),(413,40,1),(414,40,2),(415,40,3),(416,40,4),(417,40,24),(418,40,25),(419,40,26),(420,40,27),(421,40,28),(422,40,29),(423,38,75),(424,38,76);
/*!40000 ALTER TABLE `seccionoperaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suplidores`
--

DROP TABLE IF EXISTS `suplidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suplidores` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Direccion` varchar(500) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(2000) DEFAULT NULL,
  `CedulaRnc` varchar(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suplidores`
--

LOCK TABLES `suplidores` WRITE;
/*!40000 ALTER TABLE `suplidores` DISABLE KEYS */;
INSERT INTO `suplidores` VALUES (13,'VILLA GARCÍA','C/ VICTORIA Esq. C/22 ent. El milloncito, S.D.N','8095905729','Administrador ss','2020-07-10 01:22:03','Luis Alberto','0001-01-01 00:00:00',_binary '\0',NULL,'101723475'),(14,'Molinos del Ozama','C/ Olegario Vargas ·1, Villa Duarte, Santo Domingo Estes','8097882222','Administrador ss','2020-07-10 01:23:53','Luis Alberto','0001-01-01 00:00:00',_binary '\0',NULL,'101-80850-2'),(15,'LUCALZA','Autopista Duarte km.17 C/ San Antonio, los alcarrizos','8095180697','Luis Alberto','2020-07-13 12:02:11',NULL,'0001-01-01 00:00:00',_binary '',NULL,'130744017');
/*!40000 ALTER TABLE `suplidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarifariocliente`
--

DROP TABLE IF EXISTS `tarifariocliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tarifariocliente` (
  `Id` int(11) NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `ClienteId` int(11) NOT NULL,
  `Tarifa` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  CONSTRAINT `tarifariocliente_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `tarifariocliente_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarifariocliente`
--

LOCK TABLES `tarifariocliente` WRITE;
/*!40000 ALTER TABLE `tarifariocliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarifariocliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipomovimiento`
--

DROP TABLE IF EXISTS `tipomovimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipomovimiento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `FechaCreachion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `Codigo` varchar(5) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Codigo_UNIQUE` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipomovimiento`
--

LOCK TABLES `tipomovimiento` WRITE;
/*!40000 ALTER TABLE `tipomovimiento` DISABLE KEYS */;
INSERT INTO `tipomovimiento` VALUES (5,'Entrada','2018-04-14 00:00:00','noel',NULL,NULL,_binary '','IN'),(6,'Salida','2018-04-14 00:00:00','noel',NULL,NULL,_binary '','OUT');
/*!40000 ALTER TABLE `tipomovimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopago`
--

DROP TABLE IF EXISTS `tipopago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipopago` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Codigo` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Codigo_UNIQUE` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopago`
--

LOCK TABLES `tipopago` WRITE;
/*!40000 ALTER TABLE `tipopago` DISABLE KEYS */;
INSERT INTO `tipopago` VALUES (3,'EFECTIVO','NOEL','2017-01-01 00:00:00',NULL,NULL,_binary '','EFE'),(4,'CHEQUE','NOEL','2017-01-01 00:00:00',NULL,NULL,_binary '','CHK'),(5,'TARJETA DE CREDITO','NOEL','2017-01-01 00:00:00',NULL,NULL,_binary '','TC');
/*!40000 ALTER TABLE `tipopago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transferenciaalmacen`
--

DROP TABLE IF EXISTS `transferenciaalmacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transferenciaalmacen` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AlmacenOrigenId` int(10) unsigned NOT NULL,
  `AlmacenDestinoId` int(10) unsigned NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  `Referencia` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AlmacenOrigenId` (`AlmacenOrigenId`),
  KEY `AlmacenDestinoId` (`AlmacenDestinoId`),
  KEY `ProductoId` (`ProductoId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `transferenciaalmacen_ibfk_1` FOREIGN KEY (`AlmacenOrigenId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `transferenciaalmacen_ibfk_2` FOREIGN KEY (`AlmacenDestinoId`) REFERENCES `almacen` (`Id`),
  CONSTRAINT `transferenciaalmacen_ibfk_3` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `transferenciaalmacen_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transferenciaalmacen`
--

LOCK TABLES `transferenciaalmacen` WRITE;
/*!40000 ALTER TABLE `transferenciaalmacen` DISABLE KEYS */;
/*!40000 ALTER TABLE `transferenciaalmacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unidades` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,'UND','Administrador ss','2019-08-13 06:19:57','Usuario Marpos RD usuario','2019-09-05 07:47:51',_binary '');
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userclaims`
--

DROP TABLE IF EXISTS `userclaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(128) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `ApplicationUser_Claims` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userclaims`
--

LOCK TABLES `userclaims` WRITE;
/*!40000 ALTER TABLE `userclaims` DISABLE KEYS */;
INSERT INTO `userclaims` VALUES (10,'a55cd251-d6bb-4039-a91b-83f24e40d644','Seccion','{Nombre:\'*\', Operaciones:[]}',_binary '');
/*!40000 ALTER TABLE `userclaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userlogins`
--

DROP TABLE IF EXISTS `userlogins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userlogins` (
  `LoginProvider` varchar(128) NOT NULL,
  `ProviderKey` varchar(128) NOT NULL,
  `UserId` varchar(128) NOT NULL,
  PRIMARY KEY (`LoginProvider`,`ProviderKey`,`UserId`),
  KEY `ApplicationUser_Logins` (`UserId`),
  CONSTRAINT `ApplicationUser_Logins` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userlogins`
--

LOCK TABLES `userlogins` WRITE;
/*!40000 ALTER TABLE `userlogins` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlogins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `userroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userroles` (
  `UserId` varchar(128) NOT NULL,
  `RoleId` int(11) NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IdentityRole_Users` (`RoleId`),
  CONSTRAINT `ApplicationUser_Roles` FOREIGN KEY (`UserId`) REFERENCES `users` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `IdentityRole_Users` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userroles`
--

LOCK TABLES `userroles` WRITE;
/*!40000 ALTER TABLE `userroles` DISABLE KEYS */;
/*!40000 ALTER TABLE `userroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `Id` varchar(128) NOT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL,
  `PasswordHash` longtext,
  `SecurityStamp` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` tinyint(1) NOT NULL,
  `TwoFactorEnabled` tinyint(1) NOT NULL,
  `LockoutEndDateUtc` datetime DEFAULT NULL,
  `LockoutEnabled` tinyint(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `UserName` varchar(256) NOT NULL,
  `Nombre` varchar(200) DEFAULT NULL,
  `Apellido` varchar(200) DEFAULT NULL,
  `Cedula` varchar(11) DEFAULT NULL,
  `LocalidadId` int(11) NOT NULL,
  `Activo` int(11) NOT NULL,
  `CajaId` int(11) DEFAULT NULL,
  `TiempoAperturaCajaHoras` int(11) DEFAULT NULL,
  `AlmacenId` int(11) DEFAULT NULL,
  `AplicacionId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1887abc0-efbf-4d05-a33d-51114a7eb8e4','lalberto@email.com',0,'AOasrZLEYZ4V+R3SQijaoFMMh6LWLeEoWf/2qBwODr3SDBoez2tUU1Dmn+TTRO1++Q==','66aea9fd-1d1e-4996-9b51-97444622a64c',NULL,0,0,'0001-01-01 00:00:00',1,0,'lalberto','Luis','Alberto','00000000000',1,1,NULL,NULL,2,'NoelCLaveGEN348712#@'),('a55cd251-d6bb-4039-a91b-83f24e40d644','admin@gmail.com',0,'AASBu2f8x89DLRH8w0Zj+WhCt+4AeTrHn3/1Q7+jYtsYoKzltVuQGs8tqyXb7fKKFQ==','dfeb3224-a5fa-4061-8ea8-466d71b4c83d','',0,0,'2018-03-27 10:51:37',0,0,'admin','Administrador','ss','00000000000',1,1,NULL,NULL,2,'NoelCLaveGEN348712#@');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendedor`
--

DROP TABLE IF EXISTS `vendedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendedor` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Cedula` varchar(100) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `Detalles` longtext,
  `Activo` bit(1) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Direccion` varchar(2000) DEFAULT NULL,
  `ZonaId` int(11) DEFAULT NULL,
  `Codigo` varchar(50) DEFAULT NULL,
  `PorcentajeComision` decimal(18,2) NOT NULL DEFAULT '0.00',
  `ComisionPorProducto` bit(1) NOT NULL,
  `ComisionFija` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `vendedor_ibfk_1` (`ZonaId`),
  CONSTRAINT `vendedor_ibfk_1` FOREIGN KEY (`ZonaId`) REFERENCES `zona` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedor`
--

LOCK TABLES `vendedor` WRITE;
/*!40000 ALTER TABLE `vendedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zona`
--

DROP TABLE IF EXISTS `zona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zona` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `detalles` longtext,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zona`
--

LOCK TABLES `zona` WRITE;
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
INSERT INTO `zona` VALUES (3,'Distrito nacional','[]','Luis Alberto',NULL,'2020-07-13 11:32:29','0001-01-01 00:00:00',_binary '');
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'db_a63aa6_luisa'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-27 11:21:23
