CREATE DATABASE  IF NOT EXISTS `contabilidad` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `contabilidad`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: contabilidad
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `almacen`
--

DROP TABLE IF EXISTS `almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacen` (
  `Id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `Nombre` varchar(200) NOT NULL,
  `LocalidadId` int(32) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Codigo_Unique` (`Codigo`),
  KEY `LocalidadId` (`LocalidadId`),
  KEY `AlmacenProducto_Localidad_idx` (`LocalidadId`),
  CONSTRAINT `AlmacenProducto_Localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `almacen_ibfk_1` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacen`
--

LOCK TABLES `almacen` WRITE;
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;
INSERT INTO `almacen` VALUES (20,'admin',NULL,'2017-10-29 20:35:44','0001-01-01 00:00:00','','ALMACEN PRINCIPAL',4,'AP'),(21,'admin',NULL,'2017-12-02 00:00:00',NULL,'','DEFECTUOSOS',4,'DEF'),(22,'admin',NULL,'2018-05-20 21:41:37','0001-01-01 00:00:00','','Salon Jaquelin Villa Juana',4,'SJVJ'),(23,'admin',NULL,'2018-05-29 20:02:49','0001-01-01 00:00:00','','SALON JUANITA',4,'SAJUA01');
/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacenproducto`
--

DROP TABLE IF EXISTS `almacenproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacenproducto` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(32) unsigned NOT NULL,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  `LocalidadId` int(32) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  KEY `almacenproducto_localidad_idx` (`LocalidadId`),
  CONSTRAINT `almacenproducto_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `almacenproducto_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `almacenproducto_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacenproducto`
--

LOCK TABLES `almacenproducto` WRITE;
/*!40000 ALTER TABLE `almacenproducto` DISABLE KEYS */;
INSERT INTO `almacenproducto` VALUES (1,20,2,31.00,'2017-11-01 21:05:26','0001-01-01 00:00:00','admin','','',23,NULL),(2,20,3,59.00,'2017-11-01 21:17:06','0001-01-01 00:00:00','admin','','',23,NULL),(3,20,4,7.00,'2017-10-29 21:46:44','0001-01-01 00:00:00','admin','','',23,NULL),(4,20,5,0.00,'2018-09-24 17:54:06','0001-01-01 00:00:00','admin','','',23,NULL),(5,20,6,0.00,'2018-08-18 14:12:46','0001-01-01 00:00:00','admin','','',23,NULL),(6,20,9,17.00,'2018-08-06 19:59:53','2018-08-22 11:54:45','admin','admin','',23,NULL),(7,20,1,1.00,'2018-08-18 14:12:47','2018-05-29 20:07:06','admin','admin','',23,NULL),(8,20,8,0.00,'2017-11-01 20:41:12','0001-01-01 00:00:00','admin','','',23,NULL),(9,20,7,16.00,'2018-06-20 14:15:22','2018-08-06 20:23:56','admin','admin','',23,NULL),(10,20,10,0.00,'2018-08-04 16:29:57','0001-01-01 00:00:00','admin','','',23,NULL),(11,20,12,0.00,'2017-11-04 16:08:07','0001-01-01 00:00:00','admin','','',23,NULL),(12,20,14,14.00,'2018-03-30 18:36:32','0001-01-01 00:00:00','admin','','',23,NULL),(13,20,15,19.00,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin','','',23,NULL),(14,20,21,25.00,'2018-04-18 09:09:14','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(15,20,26,7.00,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin','','',23,NULL),(16,20,22,3.00,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin','','',23,NULL),(17,20,23,5.00,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(18,20,24,1.00,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(19,20,19,10.00,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(20,20,16,7.00,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(21,20,20,8.00,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(22,20,36,6.00,'2018-05-10 11:35:05','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(23,20,37,9.00,'2018-05-10 11:37:00','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(24,20,38,10.00,'2018-05-10 11:37:00','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(25,20,25,14.00,'2018-05-10 11:42:01','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(26,20,39,10.00,'2018-08-06 19:59:53','0001-01-01 00:00:00','admin','','',23,NULL),(27,20,40,3.00,'2018-05-10 12:21:54','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(28,20,41,23.00,'2018-05-10 15:42:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(29,23,1,2.00,'2018-05-29 20:07:06','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(30,20,62,4.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(31,20,60,4.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(32,20,61,2.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(33,20,59,9.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(34,20,58,7.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(35,20,57,10.00,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(36,20,56,4.00,'2018-07-10 15:08:47','2018-08-22 09:51:57','admin','admin','',23,NULL),(37,20,54,5.00,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(38,20,53,3.00,'2018-07-10 15:08:47','2018-08-22 09:51:57','admin','admin','',23,NULL),(39,20,55,7.00,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(40,20,50,7.00,'2018-07-10 15:08:47','2018-08-22 09:51:57','admin','admin','',23,NULL),(41,20,49,10.00,'2018-07-10 15:08:47','2018-08-22 09:51:57','admin','admin','',23,NULL),(42,20,48,30.00,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(43,20,46,12.00,'2018-07-10 15:08:47','2018-08-22 11:08:55','admin','admin','',23,NULL),(44,20,45,29.00,'2018-07-10 15:08:47','2018-08-22 11:08:55','admin','admin','',23,NULL),(45,20,44,25.00,'2018-07-10 15:08:47','2018-08-22 11:08:55','admin','admin','',23,NULL),(46,20,32,22.00,'2018-07-10 15:08:48','2018-08-22 12:07:26','admin','admin','',23,NULL),(47,20,31,22.00,'2018-07-10 15:08:48','2018-08-22 12:07:26','admin','admin','',23,NULL),(48,20,30,24.00,'2018-07-10 15:08:48','2018-08-22 12:07:26','admin','admin','',23,NULL),(49,20,29,27.00,'2018-07-10 15:08:48','2018-08-22 12:07:26','admin','admin','',23,NULL),(50,20,28,0.00,'2018-08-04 16:29:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(51,20,33,10.00,'2018-08-06 19:59:53','2018-08-22 12:07:26','admin','admin','',23,NULL),(52,23,29,2.00,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(53,23,30,2.00,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(54,23,31,2.00,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(55,23,45,2.00,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(56,23,44,2.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(57,23,46,2.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(58,23,33,2.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(59,23,7,2.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(60,23,53,1.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(61,23,50,1.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(62,23,56,1.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(63,23,49,1.00,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(64,20,27,2.00,'2018-08-08 14:33:37','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(65,20,47,32.00,'2018-08-08 14:33:37','2018-08-22 11:08:55','admin','admin','',23,NULL),(66,20,64,4.00,'2018-08-08 14:46:06','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(67,20,63,1.00,'2018-08-08 14:46:06','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(68,20,35,22.00,'2018-08-18 14:12:46','2018-08-22 12:07:26','admin','admin','',23,NULL),(69,20,67,13.00,'2018-08-18 14:28:32','2018-08-22 09:51:57','admin','admin','',23,NULL),(70,20,68,27.00,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin','','',23,NULL),(71,20,69,27.00,'2018-08-18 16:03:03','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(72,20,11,8.00,'2018-08-18 16:05:35','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(73,20,66,5.00,'2018-08-18 16:17:27','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(74,20,70,3.00,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(75,20,72,2.00,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(76,20,71,3.00,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(77,20,75,1.00,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(78,20,78,180.00,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(79,22,53,1.00,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(80,22,56,1.00,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(81,22,50,1.00,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(82,22,49,1.00,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(83,22,67,1.00,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(84,22,44,1.00,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(85,22,45,1.00,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(86,22,46,1.00,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(87,22,47,1.00,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,NULL),(88,22,33,2.00,'2018-08-22 11:08:55','2018-08-22 12:07:26','admin','admin','',23,NULL),(89,22,35,2.00,'2018-08-22 11:08:55','2018-08-22 12:07:26','admin','admin','',23,NULL),(90,22,29,1.00,'2018-08-22 11:13:12','2018-08-22 12:07:26','admin','admin','',23,NULL),(91,22,30,1.00,'2018-08-22 11:13:12','2018-08-22 12:07:26','admin','admin','',23,NULL),(92,22,31,1.00,'2018-08-22 11:13:12','2018-08-22 12:07:26','admin','admin','',23,NULL),(93,22,32,1.00,'2018-08-22 11:13:12','2018-08-22 12:07:26','admin','admin','',23,NULL),(94,22,9,1.00,'2018-08-22 11:54:45','0001-01-01 00:00:00','admin',NULL,'',23,NULL);
/*!40000 ALTER TABLE `almacenproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacenproductodefectuoso`
--

DROP TABLE IF EXISTS `almacenproductodefectuoso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacenproductodefectuoso` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(10) unsigned NOT NULL,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `almacenproductodefectuoso_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `almacenproductodefectuoso_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `almacenproductodefectuoso_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacenproductodefectuoso`
--

LOCK TABLES `almacenproductodefectuoso` WRITE;
/*!40000 ALTER TABLE `almacenproductodefectuoso` DISABLE KEYS */;
/*!40000 ALTER TABLE `almacenproductodefectuoso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aperturacaja`
--

DROP TABLE IF EXISTS `aperturacaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aperturacaja` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `NombreUsuario` varchar(200) NOT NULL,
  `UserId` varchar(128) NOT NULL,
  `LocalidadId` int(32) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `CajaId` int(32) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaApertura` datetime NOT NULL,
  `FechaCierre` datetime NOT NULL,
  `FechaCierreMaxima` datetime NOT NULL,
  `DetallesApertura` json NOT NULL,
  `DetallesCierre` json NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `MontoAperturaTotal` decimal(18,2) NOT NULL,
  `MontoCierreTotal` decimal(18,2) NOT NULL,
  `DiferenciaAperturaCierre` decimal(18,2) NOT NULL,
  `MontoTotalPagos` decimal(18,2) NOT NULL,
  `Estado` char(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `aperturacaja_Localidad_idx` (`LocalidadId`),
  KEY `aperturacaja_caja_idx` (`CajaId`),
  KEY `aperturacaja_moneda_idx` (`MonedaId`),
  CONSTRAINT `aperturacaja_caja` FOREIGN KEY (`CajaId`) REFERENCES `cajaregistradora` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `aperturacaja_localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `aperturacaja_moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aperturacaja`
--

LOCK TABLES `aperturacaja` WRITE;
/*!40000 ALTER TABLE `aperturacaja` DISABLE KEYS */;
INSERT INTO `aperturacaja` VALUES (10,'carlos jimenez','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',4,11,3,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-07 13:26:01','2018-11-07 13:26:01','2018-11-07 13:29:07','2018-11-07 23:26:01','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:168\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:169\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:170\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:171\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:172\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:173\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:174\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:175\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:176\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:177\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','[{\"monto\": 7110, \"codigo\": \"EFE\", \"nombre\": \"EFECTIVO\", \"detalles\": [{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:175\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:176\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:177\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:178\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:179\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:180\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:181\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"cantidad\": 8, \"detalles\": null, \"$$hashKey\": \"object:182\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"cantidad\": null, \"detalles\": null, \"$$hashKey\": \"object:183\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": null, \"detalles\": null, \"$$hashKey\": \"object:184\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}], \"$$hashKey\": \"object:168\"}, {\"monto\": 4000, \"codigo\": \"CHK\", \"nombre\": \"CHEQUE\", \"detalles\": null, \"$$hashKey\": \"object:167\"}, {\"monto\": 10000, \"codigo\": \"TC\", \"nombre\": \"TARJETA DE CREDITO\", \"detalles\": null, \"$$hashKey\": \"object:169\"}, {\"monto\": 0, \"detalles\": null, \"$$hashKey\": \"object:170\"}]','a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-07 13:29:07','\0',19455.00,21110.00,1655.00,0.00,'C'),(11,'Administrador ss','a55cd251-d6bb-4039-a91b-83f24e40d644',4,11,1,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-07 13:44:09','2018-11-07 13:44:09','2018-11-08 18:10:08','2018-11-07 21:44:09','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:222\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:223\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:224\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:225\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:226\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:227\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:228\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:229\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:230\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 7, \"detalles\": null, \"$$hashKey\": \"object:231\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','[{\"monto\": 33070, \"codigo\": \"EFE\", \"nombre\": \"EFECTIVO\", \"detalles\": [{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:439\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 4, \"detalles\": null, \"$$hashKey\": \"object:440\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"cantidad\": 2, \"detalles\": null, \"$$hashKey\": \"object:441\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"cantidad\": 1, \"detalles\": null, \"$$hashKey\": \"object:442\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"cantidad\": 100, \"detalles\": null, \"$$hashKey\": \"object:443\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"cantidad\": 200, \"detalles\": null, \"$$hashKey\": \"object:444\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:445\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"detalles\": null, \"$$hashKey\": \"object:446\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"detalles\": null, \"$$hashKey\": \"object:447\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 4, \"detalles\": null, \"$$hashKey\": \"object:448\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}], \"$$hashKey\": \"object:428\"}, {\"monto\": 8000, \"codigo\": \"CHK\", \"nombre\": \"CHEQUE\", \"detalles\": null, \"$$hashKey\": \"object:427\"}, {\"monto\": 4000, \"codigo\": \"TC\", \"nombre\": \"TARJETA DE CREDITO\", \"detalles\": null, \"$$hashKey\": \"object:429\"}, {\"monto\": 0, \"detalles\": null, \"$$hashKey\": \"object:430\"}]','a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-08 18:10:08','',27237.00,45070.00,17833.00,0.00,'C'),(12,'carlos jimenez','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',4,11,1,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-08 18:10:26','2018-11-08 18:10:26','2018-11-08 18:12:50','2018-11-09 02:10:26','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 20, \"detalles\": null, \"$$hashKey\": \"object:490\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 20, \"detalles\": null, \"$$hashKey\": \"object:491\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"cantidad\": 20, \"detalles\": null, \"$$hashKey\": \"object:492\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"cantidad\": 22, \"detalles\": null, \"$$hashKey\": \"object:493\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"detalles\": null, \"$$hashKey\": \"object:494\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"detalles\": null, \"$$hashKey\": \"object:495\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:496\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"detalles\": null, \"$$hashKey\": \"object:497\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"detalles\": null, \"$$hashKey\": \"object:498\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 10, \"detalles\": null, \"$$hashKey\": \"object:499\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','[{\"monto\": 10000, \"codigo\": \"EFE\", \"nombre\": \"EFECTIVO\", \"detalles\": [{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"detalles\": null, \"$$hashKey\": \"object:576\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"detalles\": null, \"$$hashKey\": \"object:577\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"detalles\": null, \"$$hashKey\": \"object:578\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"detalles\": null, \"$$hashKey\": \"object:579\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"detalles\": null, \"$$hashKey\": \"object:580\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"detalles\": null, \"$$hashKey\": \"object:581\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:582\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"detalles\": null, \"$$hashKey\": \"object:583\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"detalles\": null, \"$$hashKey\": \"object:584\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:585\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}], \"$$hashKey\": \"object:565\"}, {\"monto\": 0, \"codigo\": \"CHK\", \"nombre\": \"CHEQUE\", \"detalles\": null, \"$$hashKey\": \"object:564\"}, {\"monto\": 0, \"codigo\": \"TC\", \"nombre\": \"TARJETA DE CREDITO\", \"detalles\": null, \"$$hashKey\": \"object:566\"}, {\"monto\": 0, \"detalles\": null, \"$$hashKey\": \"object:567\"}]','a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-08 18:12:50','',20870.00,10000.00,-10870.00,0.00,'C'),(13,'carlos jimenez','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',4,11,1,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-15 15:10:50','2018-11-15 15:10:50','0001-01-01 00:00:00','2018-11-15 23:10:50','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"detalles\": null, \"$$hashKey\": \"object:514\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:515\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"detalles\": null, \"$$hashKey\": \"object:516\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"detalles\": null, \"$$hashKey\": \"object:517\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"detalles\": null, \"$$hashKey\": \"object:518\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"cantidad\": 10, \"detalles\": null, \"$$hashKey\": \"object:519\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:520\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"cantidad\": 4, \"detalles\": null, \"$$hashKey\": \"object:521\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"cantidad\": 2, \"detalles\": null, \"$$hashKey\": \"object:522\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"cantidad\": 1, \"detalles\": null, \"$$hashKey\": \"object:523\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','{}',NULL,'0001-01-01 00:00:00','\0',7025.00,0.00,0.00,0.00,'A'),(14,'Administrador ss','a55cd251-d6bb-4039-a91b-83f24e40d644',4,11,1,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-15 15:14:04','2018-11-15 15:14:04','0001-01-01 00:00:00','2018-11-15 23:14:04','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 0, \"detalles\": null, \"$$hashKey\": \"object:875\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:876\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"detalles\": null, \"$$hashKey\": \"object:877\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"detalles\": null, \"$$hashKey\": \"object:878\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"detalles\": null, \"$$hashKey\": \"object:879\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"detalles\": null, \"$$hashKey\": \"object:880\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:881\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"detalles\": null, \"$$hashKey\": \"object:882\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"detalles\": null, \"$$hashKey\": \"object:883\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"detalles\": null, \"$$hashKey\": \"object:884\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','{}',NULL,'0001-01-01 00:00:00','',25.00,0.00,0.00,0.00,'A'),(15,'carlos jimenez','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',4,11,1,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-12-25 01:36:50','2018-12-25 01:36:50','0001-01-01 00:00:00','2018-12-25 09:36:50','[{\"id\": 17, \"monto\": 1, \"orden\": 0, \"activo\": true, \"nombre\": \"1\", \"cantidad\": 5, \"detalles\": null, \"$$hashKey\": \"object:447\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 18, \"monto\": 5, \"orden\": 1, \"activo\": true, \"nombre\": \"5\", \"cantidad\": 0, \"detalles\": null, \"$$hashKey\": \"object:448\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 19, \"monto\": 10, \"orden\": 2, \"activo\": true, \"nombre\": \"10\", \"detalles\": null, \"$$hashKey\": \"object:449\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": null, \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 20, \"monto\": 25, \"orden\": 3, \"activo\": true, \"nombre\": \"25\", \"detalles\": null, \"$$hashKey\": \"object:450\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 21, \"monto\": 50, \"orden\": 4, \"activo\": true, \"nombre\": \"50\", \"detalles\": null, \"$$hashKey\": \"object:451\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 22, \"monto\": 100, \"orden\": 5, \"activo\": true, \"nombre\": \"100\", \"detalles\": null, \"$$hashKey\": \"object:452\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 23, \"monto\": 200, \"orden\": 6, \"activo\": true, \"nombre\": \"200\", \"detalles\": null, \"$$hashKey\": \"object:453\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 24, \"monto\": 500, \"orden\": 7, \"activo\": true, \"nombre\": \"500\", \"detalles\": null, \"$$hashKey\": \"object:454\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 25, \"monto\": 1000, \"orden\": 8, \"activo\": true, \"nombre\": \"1000\", \"detalles\": null, \"$$hashKey\": \"object:455\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}, {\"id\": 26, \"monto\": 2000, \"orden\": 9, \"activo\": true, \"nombre\": \"2000\", \"detalles\": null, \"$$hashKey\": \"object:456\", \"creadoPor\": \"noel\", \"fechaCreacion\": \"2018-01-01T00:00:00\", \"modificadoPor\": \"noel\", \"fechaModificacion\": \"2018-01-01T00:00:00\"}]','{}',NULL,'0001-01-01 00:00:00','',5.00,0.00,0.00,0.00,'A');
/*!40000 ALTER TABLE `aperturacaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancecliente`
--

DROP TABLE IF EXISTS `balancecliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balancecliente` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(32) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MontoAdeudado` decimal(18,2) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Factura_Moneda_Balance` (`MonedaId`),
  KEY `Factura_Cliente_Balance` (`ClienteId`),
  CONSTRAINT `Factura_Cliente_Balance` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Factura_Moneda_Balance` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancecliente`
--

LOCK TABLES `balancecliente` WRITE;
/*!40000 ALTER TABLE `balancecliente` DISABLE KEYS */;
INSERT INTO `balancecliente` VALUES (4,1,11,2450.00,NULL,'0001-01-01 00:00:00'),(5,3,11,500.00,'','0001-01-01 00:00:00'),(6,7,11,18850.00,NULL,'0001-01-01 00:00:00'),(7,6,11,8000.00,'','0001-01-01 00:00:00'),(8,4,11,6400.00,NULL,'0001-01-01 00:00:00'),(10,9,11,0.00,NULL,'0001-01-01 00:00:00'),(11,10,11,950.00,NULL,'0001-01-01 00:00:00'),(12,11,11,1299.00,NULL,'0001-01-01 00:00:00'),(13,12,11,0.00,NULL,'0001-01-01 00:00:00'),(14,13,11,0.00,NULL,'0001-01-01 00:00:00'),(15,14,11,8260.00,NULL,'0001-01-01 00:00:00'),(16,15,11,15050.00,NULL,'0001-01-01 00:00:00'),(17,16,11,19325.00,NULL,'0001-01-01 00:00:00'),(19,17,11,11550.00,NULL,'0001-01-01 00:00:00'),(20,18,11,4926.50,NULL,'0001-01-01 00:00:00');
/*!40000 ALTER TABLE `balancecliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balancesuplidores`
--

DROP TABLE IF EXISTS `balancesuplidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balancesuplidores` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(32) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Factura_Moneda_Balance` (`MonedaId`),
  KEY `Factura_Suplidor_Balance` (`SuplidorId`),
  CONSTRAINT `Factura_Moneda_Balance1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Factura_Suplidor_Balance1` FOREIGN KEY (`SuplidorId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balancesuplidores`
--

LOCK TABLES `balancesuplidores` WRITE;
/*!40000 ALTER TABLE `balancesuplidores` DISABLE KEYS */;
/*!40000 ALTER TABLE `balancesuplidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baseserviciocompuesto`
--

DROP TABLE IF EXISTS `baseserviciocompuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baseserviciocompuesto` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoCompuestoId` int(32) unsigned NOT NULL,
  `ProductoBaseId` int(32) unsigned NOT NULL,
  `ProductoUnidadBaseId` int(32) DEFAULT NULL,
  `CreadoPor` varchar(50) NOT NULL,
  `ModificadoPor` varchar(50) DEFAULT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'1',
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CostoTotal` decimal(18,6) DEFAULT NULL,
  `PrecioTotal` decimal(18,6) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoCompuestoId` (`ProductoCompuestoId`),
  KEY `ProductoBaseId` (`ProductoBaseId`),
  KEY `ProductoUnidadBaseId` (`ProductoUnidadBaseId`),
  CONSTRAINT `baseserviciocompuesto_ibfk_1` FOREIGN KEY (`ProductoUnidadBaseId`) REFERENCES `productounidadesequivalencia` (`Id`) ON DELETE NO ACTION,
  CONSTRAINT `baseserviciocompuesto_ibfk_2` FOREIGN KEY (`ProductoCompuestoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION,
  CONSTRAINT `baseserviciocompuesto_ibfk_3` FOREIGN KEY (`ProductoBaseId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baseserviciocompuesto`
--

LOCK TABLES `baseserviciocompuesto` WRITE;
/*!40000 ALTER TABLE `baseserviciocompuesto` DISABLE KEYS */;
INSERT INTO `baseserviciocompuesto` VALUES (1,34,29,30,'admin','admin','',1.00,'2018-04-21 18:03:35','2018-08-22 09:53:54',0.000000,0.000000),(2,34,30,31,'admin','admin','',1.00,'2018-04-21 18:03:35','2018-08-22 09:53:54',0.000000,0.000000),(3,34,31,32,'admin','admin','',1.00,'2018-04-21 18:03:35','2018-08-22 09:53:54',0.000000,0.000000),(4,34,33,34,'admin','admin','',1.00,'2018-04-21 18:03:35','2018-08-22 09:53:54',0.000000,0.000000),(5,34,32,33,'admin','admin','',1.00,'2018-04-21 18:03:35','2018-08-22 09:53:54',0.000000,0.000000),(6,34,35,35,'admin','admin','\0',1.00,'2018-04-21 18:03:35','2018-07-10 15:15:18',0.000000,0.000000),(7,79,44,58,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',0.000000,600.000000),(8,79,45,59,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',0.000000,600.000000),(9,79,46,60,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',0.000000,400.000000),(10,79,47,61,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',0.000000,450.000000),(11,79,33,45,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',80.000000,500.000000),(12,79,35,83,'admin','admin','',1.00,'2018-08-22 10:27:30','2018-08-22 10:31:47',100.000000,0.000000);
/*!40000 ALTER TABLE `baseserviciocompuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cajaregistradora`
--

DROP TABLE IF EXISTS `cajaregistradora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cajaregistradora` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `localidadId` int(32) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `CodigoCaja_UNIQUE` (`Codigo`),
  KEY `cajaregistradora_localidad_idx` (`localidadId`),
  CONSTRAINT `cajaregistradora_localidad` FOREIGN KEY (`localidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajaregistradora`
--

LOCK TABLES `cajaregistradora` WRITE;
/*!40000 ALTER TABLE `cajaregistradora` DISABLE KEYS */;
INSERT INTO `cajaregistradora` VALUES (1,'Caja 1','BOX_1','admin','2018-10-02 21:56:26','admin','2018-10-02 21:56:40','',4),(3,'Caja 2','BOX_2','admin','2018-10-02 21:56:59',NULL,'0001-01-01 00:00:00','',4);
/*!40000 ALTER TABLE `cajaregistradora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `CedulaRnc` varchar(100) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `Detalles` longtext,
  `ClaseDetalles` varchar(500) DEFAULT NULL,
  `AplicarItbis` bit(1) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `DiasVencimiento` int(32) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MonedaId` int(32) DEFAULT NULL,
  `Direccion` varchar(2000) DEFAULT NULL,
  `AlmacenId` int(32) unsigned DEFAULT NULL,
  `Codigo` varchar(50) DEFAULT NULL,
  `TipoNcf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `MonedaId` (`MonedaId`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'ZOILA PEREZ','00108283979','8094126415','admin','2017-10-29 21:50:40',NULL,'','\0','',30,'admin','2018-08-28 19:35:32',11,'CALLE 4TA. #14, CAFE 4 DE HERRERA. STO. OESTE',NULL,'CLI00018',NULL),(2,'SALON MADE BEATY CENTER/4946','00100002209','8095637708','admin','2017-11-01 18:52:05',NULL,'','\0','',30,'admin','2018-08-28 19:35:29',11,'AVENNIDA SAN CRISTOBAL  ESQ. LOPEZ DE VENGA , ENSANCHE LA FE',NULL,'CLI00017',NULL),(3,'SALON JAQUELIN --VILLA JUANA','00102762804','8299105747','admin','2017-11-01 18:56:26',NULL,'','\0','',30,'admin','2018-08-28 19:35:24',11,'C/ JOSE DE JESUS RAVELO NO.70, VILLA JUANA,  CASI ESQ. MARIA MONTEZ.,',NULL,'CLI00016',NULL),(4,'SALON JAQUELIN- CRITO REY','00113568273','8095620083','admin','2017-11-01 19:00:48',NULL,'','\0','',30,'admin','2018-08-28 19:35:20',11,'C/ 39 CASI ESQ.38 CRISTO REY, AL LADO CLINICA RODRIGUEZ',NULL,'CLI00015',NULL),(5,'MAGALIS VECINA RADIANTE','00104068718','8095689305','admin','2017-11-01 19:11:16',NULL,NULL,'\0','',30,'admin','2018-08-28 19:35:16',11,'CALLE LA PAZ NO. 2, RADIANTE , KM 10 1/2 VILLA MELLA',NULL,'CLI00014',NULL),(6,'D \'SARITA SALON SAN MARTIN','00151198541','1721553306','admin','2017-11-01 19:14:59',NULL,NULL,'\0','',10,'admin','2018-08-28 19:35:13',11,'ISLA SAN MARTIN 829-273-4261',NULL,'CLI00013',NULL),(7,'SALON  JUANITA','00103762308','8095357723','admin','2017-11-01 19:21:26',NULL,NULL,'\0','',30,'admin','2018-08-28 19:35:09',11,'C/ LAS GABIOTA NO.5 ESQ. CORALES.  ESQ. CORALES',23,'CLI00012',NULL),(8,'SUPLY ANGHY','22400001990','8299934097','admin','2017-11-01 19:31:16',NULL,'','\0','',30,'admin','2018-08-28 19:35:05',11,'BANCO POPULAR',NULL,'CLI00011',NULL),(9,'FERIAS','809569774','8095697774','admin','2018-07-10 13:57:09',NULL,NULL,'\0','',0,'admin','2018-08-28 19:34:59',11,'CALLE LA PAZ NO. 58, RADIANTE  VM',20,'CLI00010',NULL),(10,'SALON IBELISE','','8099406826','admin','2018-07-16 17:38:38',NULL,NULL,'\0','',30,'admin','2018-08-28 19:34:55',11,'KM 13, AV. SANCHEZ, CALLE DE LA PLAYA  (POR DONDE LIBANI)',20,'CLI00009',NULL),(11,'SALON LIBANI','','8297763530','admin','2018-07-16 19:35:57',NULL,NULL,'\0','',30,'admin','2018-08-28 19:34:50',11,'KM 13 CARRETERA SANCHEZ, MIRAMAR',20,'CLI00008',NULL),(12,'CARMEN DISTRIBUIDORA','','8097295885','admin','2018-07-16 20:04:29',NULL,NULL,'\0','',30,'admin','2018-08-28 19:34:45',11,'ARROYO HONDO',NULL,'CLI00007',NULL),(13,'JL SUPPLY  IMPORT   KAREN REDR','','8099049328','admin','2018-07-25 20:24:56',NULL,NULL,'\0','',30,'admin','2018-08-28 19:34:40',11,'ENSANCHE LA FE',NULL,'CLI00006',NULL),(14,'CONTADO','22400115873','8095697774','admin','2018-07-30 19:54:26',NULL,NULL,'\0','',0,'admin','2018-10-08 22:39:42',11,'CALLE LA PAZ NO.58 RADIANTE VM\nKM 10 1/2 SANTO DOMINGO,NORTE',NULL,'CLI00005',NULL),(15,'FERIA OLIMPICO','','8095697774','admin','2018-08-08 12:46:13',NULL,NULL,'\0','',0,'admin','2018-08-26 19:00:37',11,'CALLE LA PAZ NO. 58 RADIANTE',20,'CLI00003',NULL),(16,'JR IMPORT ANGELA RODRIGUEZ','00113057160','8099049320','admin','2018-08-18 12:41:41',NULL,NULL,'\0','',29,'admin','2018-08-26 19:00:24',11,'C/PEDRO OLIVIO CEDENO, RESIDENCIAL ORTEGA Y GASSET EDIFICIO B-1 APARTAMENTO 101',NULL,'CLI00002',NULL),(17,'CORTE TROPICAL','8098444512','8095331292','admin','2018-08-18 15:30:35',NULL,NULL,'\0','',15,'admin','2018-08-26 19:01:01',11,'AV. INDEPENDENCIA 2001 HONDURAS',NULL,'CLI00001',NULL),(18,'Noel','22344400040','8095697774','admin','2018-08-26 19:01:32',NULL,NULL,'','',0,'admin','2018-09-04 07:32:07',11,'Roberto Pastoriza 603, Evaristo Morales, Santo Domingo',NULL,'CLI00004','01'),(19,'juan','palote','8095556666','a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-09 12:23:24',NULL,NULL,'','',30,'cjimenez','2018-12-22 01:16:47',11,'El manguito',20,'CLI00019','01');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlncf`
--

DROP TABLE IF EXISTS `controlncf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controlncf` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) NOT NULL,
  `Secuencia` int(32) NOT NULL,
  `ControlNumerico` int(32) NOT NULL,
  `Cantidad` int(32) NOT NULL,
  `Tipo` varchar(2) NOT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Serie` char(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Nombre_UNIQUE` (`Nombre`),
  UNIQUE KEY `Codigo_UNIQUE` (`Tipo`),
  UNIQUE KEY `Secuencia_UNIQUE` (`Secuencia`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlncf`
--

LOCK TABLES `controlncf` WRITE;
/*!40000 ALTER TABLE `controlncf` DISABLE KEYS */;
INSERT INTO `controlncf` VALUES (1,'Cliente Fina',14589,5,95,'01','admin',NULL,'2018-09-04 00:17:04','0001-01-01 00:00:00','','B');
/*!40000 ALTER TABLE `controlncf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controlsecuencias`
--

DROP TABLE IF EXISTS `controlsecuencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controlsecuencias` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ControlNumerico` int(32) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `CreadoPor` varchar(45) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controlsecuencias`
--

LOCK TABLES `controlsecuencias` WRITE;
/*!40000 ALTER TABLE `controlsecuencias` DISABLE KEYS */;
INSERT INTO `controlsecuencias` VALUES (1,42,'FAC','Secuencia de facturas','noel','','2017-01-01 00:00:00','0001-01-01 00:00:00',''),(2,1,'DEV','SECUENCIA DEVOLUCIONES','NOEL',NULL,'2017-01-01 00:00:00','0001-01-01 00:00:00',''),(3,7,'TRF','CONTROL DE TRANSFERENCIAS','NOEL',NULL,'2017-01-01 00:00:00','0001-01-01 00:00:00',''),(4,37,'R','CONTROL DE PAGOS','NOEL','','2017-01-01 00:00:00','0001-01-01 00:00:00',''),(8,0,'NC','NOTAS DE CREDITO','NOEL',NULL,'2018-04-14 00:00:00',NULL,''),(9,27,'ENT','ENTRADAS','noel','','2018-04-14 00:00:00','0001-01-01 00:00:00',''),(10,0,'R','Recibos de ingreso','noel',NULL,'2018-04-14 00:00:00',NULL,''),(11,0,'DS','Devoluciones a suplidor','noel',NULL,'2018-04-14 00:00:00',NULL,''),(13,9,'PG','Pagos','noel',NULL,'2018-04-30 00:00:00','0001-01-01 00:00:00',''),(14,14,'RG','Recibo de gastos','noel',NULL,'2018-04-30 00:00:00','0001-01-01 00:00:00',''),(15,4,'COT','Cotizaciones','noel',NULL,'2018-07-08 00:00:00','0001-01-01 00:00:00',''),(16,19,'CLI','Clientes','noel',NULL,'2018-01-01 00:00:00','0001-01-01 00:00:00','');
/*!40000 ALTER TABLE `controlsecuencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalledevolucion`
--

DROP TABLE IF EXISTS `detalledevolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalledevolucion` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductoId` int(10) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `CantidadDevolucion` decimal(18,2) NOT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `Fecha` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FacturaId` int(11) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT b'0',
  `Defectuoso` bit(1) DEFAULT b'0',
  `ImpuestoId` int(11) NOT NULL,
  `MontoGrabado` decimal(18,2) NOT NULL,
  `Comentario` varchar(200) DEFAULT NULL,
  `MontoDop` decimal(18,2) NOT NULL,
  `AlmacenId` int(10) unsigned DEFAULT NULL,
  `UnidadId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `detalleProducto` (`ProductoId`),
  KEY `FacturaId` (`FacturaId`),
  KEY `ImpuestoId` (`ImpuestoId`),
  KEY `detalledevolucion_ibfk_4` (`AlmacenId`),
  KEY `detalledevolucion_ibfk_5` (`UnidadId`),
  CONSTRAINT `detalledevolucion_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalledevolucion_ibfk_2` FOREIGN KEY (`FacturaId`) REFERENCES `factura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalledevolucion_ibfk_3` FOREIGN KEY (`ImpuestoId`) REFERENCES `impuesto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalledevolucion_ibfk_4` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalledevolucion_ibfk_5` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalledevolucion`
--

LOCK TABLES `detalledevolucion` WRITE;
/*!40000 ALTER TABLE `detalledevolucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalledevolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefactura`
--

DROP TABLE IF EXISTS `detallefactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefactura` (
  `Id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `Fecha` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FacturaId` int(32) NOT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `ImpuestoId` int(32) NOT NULL,
  `MontoGrabado` decimal(18,2) NOT NULL,
  `Comentario` varchar(200) DEFAULT NULL,
  `MontoDop` decimal(18,2) NOT NULL,
  `AlmacenId` int(32) unsigned DEFAULT NULL,
  `UnidadId` int(32) DEFAULT NULL,
  `MontoDescuento` decimal(18,2) NOT NULL,
  `MontoNotaCreditoAplicada` decimal(18,2) NOT NULL,
  `DetallePadreId` int(32) DEFAULT NULL,
  `PorcientoDescuento` decimal(18,2) NOT NULL,
  `EsGratis` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  KEY `detalleProducto` (`ProductoId`),
  KEY `FacturaId` (`FacturaId`),
  KEY `ImpuestoId` (`ImpuestoId`),
  KEY `detallefactura_ibfk_4` (`AlmacenId`),
  KEY `detallefactura_ibfk_5` (`UnidadId`),
  CONSTRAINT `detalleProducto` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`),
  CONSTRAINT `detallefactura_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detallefactura_ibfk_2` FOREIGN KEY (`FacturaId`) REFERENCES `factura` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detallefactura_ibfk_3` FOREIGN KEY (`ImpuestoId`) REFERENCES `impuesto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detallefactura_ibfk_4` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detallefactura_ibfk_5` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefactura`
--

LOCK TABLES `detallefactura` WRITE;
/*!40000 ALTER TABLE `detallefactura` DISABLE KEYS */;
INSERT INTO `detallefactura` VALUES (1,4,10.00,350.00,0.00,3500.00,'2017-10-29 22:11:47','system',4,'2017-10-29 22:11:47','','2017-10-29 22:11:47','',13,3500.00,'',3500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(2,2,4.00,350.00,0.00,1400.00,'2017-10-29 22:11:47','system',4,'2017-10-29 22:11:47','','2017-10-29 22:11:47','',13,1400.00,'',1400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(3,3,6.00,350.00,0.00,2100.00,'2017-10-29 22:11:47','system',4,'2017-10-29 22:11:47','','2017-10-29 22:11:47','',13,2100.00,'',2100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(4,10,2.00,260.00,0.00,520.00,'2017-12-02 17:25:11','admin',5,'2017-12-02 17:25:11','','2017-12-02 17:25:11','',13,520.00,'',520.00,20,23,0.00,0.00,NULL,0.00,'\0'),(5,1,1.00,1500.00,0.00,1500.00,'2017-12-02 17:25:11','admin',5,'2017-12-02 17:25:11','','2017-12-02 17:25:11','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(6,7,1.00,800.00,0.00,800.00,'2018-03-30 12:47:38','admin',6,'2018-03-30 12:47:38','','2018-03-30 12:47:38','',13,800.00,'',800.00,20,23,0.00,0.00,NULL,0.00,'\0'),(7,7,6.00,550.00,0.00,3300.00,'2018-03-30 18:18:12','admin',7,'2018-03-30 18:18:12','','2018-03-30 18:18:12','',13,3300.00,'',3300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(8,5,4.00,500.00,0.00,2000.00,'2018-03-30 18:18:12','admin',7,'2018-03-30 18:18:12','','2018-03-30 18:18:12','',13,2000.00,'',2000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(9,6,4.00,600.00,0.00,2400.00,'2018-03-30 18:18:12','admin',7,'2018-03-30 18:18:12','','2018-03-30 18:18:12','',13,2400.00,'',2400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(10,12,14.00,300.00,0.00,4200.00,'2018-03-30 18:18:12','admin',7,'2018-03-30 18:18:12','','2018-03-30 18:18:12','',13,4200.00,'',4200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(11,9,10.00,400.00,0.00,4000.00,'2018-03-30 18:18:12','admin',7,'2018-03-30 18:18:12','','2018-03-30 18:18:12','',13,4000.00,'',4000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(12,15,10.00,200.00,0.00,2000.00,'2018-03-30 18:46:16','admin',7,'2018-03-30 18:46:16','','2018-03-30 18:46:16','',13,2000.00,'',2000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(13,14,10.00,250.00,0.00,2500.00,'2018-03-30 18:46:16','admin',7,'2018-03-30 18:46:16','','2018-03-30 18:46:16','',13,2500.00,'',2500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(14,8,4.00,400.00,0.00,1600.00,'2018-03-30 18:46:16','admin',7,'2018-03-30 18:46:16','','2018-03-30 18:46:16','',13,1600.00,'',1600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(15,1,15.00,1000.00,0.00,15000.00,'2018-03-30 18:53:21','admin',8,'2018-03-30 18:53:21','','2018-03-30 18:53:21','',13,15000.00,'',15000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(16,16,2.00,550.00,0.00,1100.00,'2018-04-21 17:22:21','admin',9,'2018-04-21 17:22:21','','2018-04-21 17:22:21','\0',13,1100.00,'',1100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(17,19,2.00,500.00,0.00,1000.00,'2018-04-21 17:22:21','admin',9,'2018-04-21 17:22:21','','2018-04-21 17:22:21','',13,1000.00,'',1000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(18,20,2.00,300.00,0.00,600.00,'2018-04-21 17:22:21','admin',9,'2018-04-21 17:22:21','','2018-04-21 17:22:21','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(19,5,2.00,650.00,0.00,1300.00,'2018-05-10 12:31:30','admin',10,'2018-05-10 12:31:30','','2018-05-10 12:31:30','',13,1300.00,'',1300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(20,6,1.00,650.00,0.00,650.00,'2018-05-10 12:31:31','admin',10,'2018-05-10 12:31:31','','2018-05-10 12:31:31','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(21,16,2.00,550.00,0.00,1100.00,'2018-05-10 12:52:58','admin',9,'2018-05-10 12:52:58','','2018-05-10 12:52:58','',13,1100.00,'',1100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(22,39,2.00,550.00,0.00,1100.00,'2018-05-10 12:52:58','admin',9,'2018-05-10 12:52:58','','2018-05-10 12:52:58','',13,1100.00,'',1100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(23,40,2.00,300.00,0.00,600.00,'2018-05-10 12:52:59','admin',9,'2018-05-10 12:52:59','','2018-05-10 12:52:59','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(24,5,1.00,650.00,0.00,650.00,'2018-05-10 12:59:01','admin',11,'2018-05-10 12:59:01','','2018-05-10 12:59:01','',13,650.00,'ES DE LAURA SOSA',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(25,10,1.00,200.00,0.00,200.00,'2018-05-10 13:13:25','admin',12,'2018-05-10 13:13:25','','2018-05-10 13:13:25','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(26,26,1.00,200.00,0.00,200.00,'2018-05-10 13:13:25','admin',12,'2018-05-10 13:13:25','','2018-05-10 13:13:25','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(27,19,2.00,450.00,0.00,900.00,'2018-05-10 13:13:25','admin',12,'2018-05-10 13:13:25','','2018-05-10 13:13:25','',13,900.00,'',900.00,20,23,0.00,0.00,NULL,0.00,'\0'),(28,38,1.00,750.00,0.00,750.00,'2018-05-10 13:13:25','admin',12,'2018-05-10 13:13:25','','2018-05-10 13:13:25','',13,750.00,'',750.00,20,23,0.00,0.00,NULL,0.00,'\0'),(29,16,1.00,450.00,0.00,450.00,'2018-05-10 13:13:25','admin',12,'2018-05-10 13:13:25','','2018-05-10 13:13:25','',13,450.00,'',450.00,20,23,0.00,0.00,NULL,0.00,'\0'),(30,9,1.00,600.00,0.00,600.00,'2018-05-10 13:13:26','admin',12,'2018-05-10 13:13:26','','2018-05-10 13:13:26','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(31,39,1.00,0.00,0.00,0.00,'2018-05-10 13:13:26','admin',12,'2018-05-10 13:13:26','','2018-05-10 13:13:26','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(32,39,1.00,500.00,0.00,500.00,'2018-05-10 13:13:26','admin',12,'2018-05-10 13:13:26','','2018-05-10 13:13:26','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(33,40,1.00,300.00,0.00,300.00,'2018-05-10 13:13:26','admin',12,'2018-05-10 13:13:26','','2018-05-10 13:13:26','',13,300.00,'',300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(34,20,1.00,0.00,0.00,0.00,'2018-05-10 13:13:26','admin',12,'2018-05-10 13:13:26','','2018-05-10 13:13:26','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(35,1,2.00,1500.00,0.00,3000.00,'2018-05-10 15:28:33','admin',9,'2018-05-10 15:28:33','','2018-05-10 15:28:33','',13,3000.00,'CONSIGNACION',3000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(36,39,1.00,0.00,0.00,0.00,'2018-05-10 15:28:33','admin',9,'2018-05-10 15:28:33','','2018-05-10 15:28:33','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(37,1,1.00,1500.00,0.00,1500.00,'2018-05-10 16:13:20','admin',13,'2018-05-10 16:13:20','','2018-05-10 16:13:20','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(38,41,3.00,500.00,0.00,1500.00,'2018-05-10 16:13:20','admin',13,'2018-05-10 16:13:20','','2018-05-10 16:13:20','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(39,41,1.00,0.00,0.00,0.00,'2018-05-10 16:13:20','admin',13,'2018-05-10 16:13:20','','2018-05-10 16:13:20','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(40,19,2.00,500.00,0.00,1000.00,'2018-05-10 16:13:20','admin',13,'2018-05-10 16:13:20','','2018-05-10 16:13:20','',13,1000.00,'',1000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(41,20,1.00,300.00,0.00,300.00,'2018-05-10 16:13:21','admin',13,'2018-05-10 16:13:21','','2018-05-10 16:13:21','',13,300.00,'',300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(42,20,1.00,0.00,0.00,0.00,'2018-05-10 16:13:21','admin',13,'2018-05-10 16:13:21','','2018-05-10 16:13:21','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(43,38,1.00,750.00,0.00,750.00,'2018-05-10 16:13:21','admin',13,'2018-05-10 16:13:21','','2018-05-10 16:13:21','',13,750.00,'',750.00,20,23,0.00,0.00,NULL,0.00,'\0'),(44,39,1.00,550.00,0.00,550.00,'2018-05-10 16:13:21','admin',13,'2018-05-10 16:13:21','','2018-05-10 16:13:21','',13,550.00,'',550.00,20,23,0.00,0.00,NULL,0.00,'\0'),(45,40,1.00,300.00,0.00,300.00,'2018-05-10 16:13:21','admin',13,'2018-05-10 16:13:21','','2018-05-10 16:13:21','',13,300.00,'',300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(46,40,1.00,0.00,0.00,0.00,'2018-05-10 16:13:22','admin',13,'2018-05-10 16:13:22','','2018-05-10 16:13:22','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(47,16,1.00,500.00,0.00,500.00,'2018-05-10 16:13:22','admin',13,'2018-05-10 16:13:22','','2018-05-10 16:13:22','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(48,5,2.00,850.00,0.00,1700.00,'2018-07-08 18:39:17','admin',14,'2018-07-08 18:39:17',NULL,'2018-07-08 18:39:17','',13,1700.00,'',1700.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(49,5,3.00,850.00,0.00,2550.00,'2018-07-08 18:39:17','admin',14,'2018-07-08 18:39:17',NULL,'2018-07-08 18:39:17','',13,2550.00,'',2550.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(50,7,1.00,800.00,0.00,800.00,'2018-07-08 18:39:17','admin',14,'2018-07-08 18:39:17',NULL,'2018-07-08 18:39:17','',13,800.00,'',800.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(52,5,12.00,850.00,0.00,10200.00,'2018-07-10 14:03:49','admin',16,'2018-07-10 14:03:49','','2018-07-10 14:03:49','',13,10200.00,'',10200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(53,36,10.00,350.00,0.00,3500.00,'2018-07-10 14:03:49','admin',16,'2018-07-10 14:03:49','','2018-07-10 14:03:49','',13,3500.00,'',3500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(54,37,5.00,400.00,0.00,2000.00,'2018-07-10 14:03:49','admin',16,'2018-07-10 14:03:49','','2018-07-10 14:03:49','',13,2000.00,'',2000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(55,1,15.00,1500.00,0.00,22500.00,'2018-07-10 14:03:49','admin',16,'2018-07-10 14:03:49','','2018-07-10 14:03:49','',13,22500.00,'',22500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(56,6,8.00,950.00,0.00,7600.00,'2018-07-10 14:03:49','admin',16,'2018-07-10 14:03:49','','2018-07-10 14:03:49','',13,7600.00,'',7600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(57,21,2.00,1425.00,0.00,2850.00,'2018-07-16 17:49:27','admin',17,'2018-07-16 17:49:27','','2018-07-16 17:49:27','',13,2850.00,'DESCUENTO KIT',2850.00,20,23,0.00,0.00,NULL,0.00,'\0'),(58,36,1.00,250.00,0.00,250.00,'2018-07-16 17:49:27','admin',17,'2018-07-16 17:49:27','','2018-07-16 17:49:27','',13,250.00,'',250.00,20,23,0.00,0.00,NULL,0.00,'\0'),(59,37,1.00,250.00,0.00,250.00,'2018-07-16 17:49:27','admin',17,'2018-07-16 17:49:27','','2018-07-16 17:49:27','',13,250.00,'',250.00,20,23,0.00,0.00,NULL,0.00,'\0'),(60,22,2.00,300.00,0.00,600.00,'2018-07-16 17:49:27','admin',17,'2018-07-16 17:49:27','','2018-07-16 17:49:27','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(61,5,1.00,650.00,0.00,650.00,'2018-07-16 18:30:34','admin',18,'2018-07-16 18:30:34','','2018-07-16 18:30:34','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(62,5,1.00,650.00,0.00,650.00,'2018-07-16 19:28:10','admin',19,'2018-07-16 19:28:10','','2018-07-16 19:28:10','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(63,14,4.00,250.00,0.00,1000.00,'2018-07-16 19:38:46','admin',20,'2018-07-16 19:38:46','','2018-07-16 19:38:46','',13,1000.00,'FERIA PRECIO',1000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(64,39,3.00,333.00,0.00,999.00,'2018-07-16 19:38:46','admin',20,'2018-07-16 19:38:46','','2018-07-16 19:38:46','',13,999.00,'',999.00,20,23,0.00,0.00,NULL,0.00,'\0'),(65,21,4.00,1050.00,0.00,4200.00,'2018-07-16 20:05:50','admin',21,'2018-07-16 20:05:50','','2018-07-16 20:05:50','',13,4200.00,'',4200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(66,34,1.00,2500.00,0.00,2500.00,'2018-07-25 20:01:03','admin',22,'2018-07-25 20:01:03',NULL,'2018-07-25 20:01:03','',13,2500.00,'',2500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(67,6,2.00,550.00,0.00,1100.00,'2018-07-25 21:21:06','admin',23,'2018-07-25 21:21:06','','2018-07-25 21:21:06','',13,1100.00,'',1100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(68,5,1.00,550.00,0.00,550.00,'2018-07-25 21:21:06','admin',23,'2018-07-25 21:21:06','','2018-07-25 21:21:06','',13,550.00,'',550.00,20,23,0.00,0.00,NULL,0.00,'\0'),(69,34,1.00,1300.00,0.00,1300.00,'2018-07-30 20:41:34','admin',24,'2018-07-30 20:41:34',NULL,'2018-07-30 20:41:34','',13,1300.00,'',1300.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(70,21,4.00,1000.00,0.00,4000.00,'2018-08-04 13:05:53','admin',25,'2018-08-04 13:05:53','','2018-08-04 13:05:53','',13,4000.00,'',4000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(71,5,2.00,650.00,0.00,1300.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,1300.00,'',1300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(72,6,1.00,650.00,0.00,650.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(73,59,1.00,650.00,0.00,650.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(74,62,1.00,650.00,0.00,650.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(75,28,4.00,200.00,0.00,800.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,800.00,'',800.00,20,23,0.00,0.00,NULL,0.00,'\0'),(76,10,2.00,200.00,0.00,400.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(77,37,1.00,400.00,0.00,400.00,'2018-08-04 16:43:18','admin',30,'2018-08-04 16:43:18','','2018-08-04 16:43:18','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(78,40,1.00,200.00,0.00,200.00,'2018-08-08 13:31:29','admin',31,'2018-08-08 13:31:29','','2018-08-08 13:31:29','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(100,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(101,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(102,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(103,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(104,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(105,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(106,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(107,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(108,26,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(109,62,1.00,600.00,0.00,600.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(110,8,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(111,7,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(112,5,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(113,5,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(114,62,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(115,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(116,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(117,5,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(118,6,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(119,7,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(120,59,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(121,8,1.00,500.00,0.00,500.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(122,29,1.00,200.00,0.00,200.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(123,31,1.00,200.00,0.00,200.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(124,32,1.00,200.00,0.00,200.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(125,7,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(126,8,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(127,9,1.00,400.00,0.00,400.00,'2018-08-08 13:51:08','admin',31,'2018-08-08 13:51:08','','2018-08-08 13:51:08','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(128,5,1.00,500.00,0.00,500.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(129,6,1.00,500.00,0.00,500.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(130,62,1.00,500.00,0.00,500.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(131,10,1.00,150.00,0.00,150.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(132,28,1.00,150.00,0.00,150.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,150.00,'',150.00,20,23,0.00,0.00,NULL,0.00,'\0'),(133,37,1.00,200.00,0.00,200.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(134,2,1.00,200.00,0.00,200.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(135,62,1.00,400.00,0.00,400.00,'2018-08-08 13:51:09','admin',31,'2018-08-08 13:51:09','','2018-08-08 13:51:09','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(136,64,10.00,150.00,0.00,1500.00,'2018-08-08 15:26:04','admin',31,'2018-08-08 15:26:04','','2018-08-08 15:26:04','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(137,63,7.00,300.00,0.00,2100.00,'2018-08-08 15:26:04','admin',31,'2018-08-08 15:26:04','','2018-08-08 15:26:04','',13,2100.00,'',2100.00,20,23,0.00,0.00,NULL,0.00,'\0'),(138,60,1.00,350.00,0.00,350.00,'2018-08-18 12:54:45','admin',32,'2018-08-18 12:54:45','','2018-08-18 12:54:45','',13,350.00,'',350.00,20,23,0.00,0.00,NULL,0.00,'\0'),(139,7,1.00,400.00,0.00,400.00,'2018-08-18 12:54:45','admin',32,'2018-08-18 12:54:45','','2018-08-18 12:54:45','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(140,59,5.00,550.00,0.00,2750.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,2750.00,'',2750.00,20,23,0.00,0.00,NULL,0.00,'\0'),(141,38,5.00,400.00,0.00,2000.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,2000.00,'',2000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(142,16,2.00,0.00,0.00,0.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(143,60,4.00,350.00,0.00,1400.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,1400.00,'',1400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(144,61,2.00,0.00,0.00,0.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(145,12,4.00,0.00,0.00,0.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(146,9,5.00,375.00,0.00,1875.00,'2018-08-18 14:05:09','admin',33,'2018-08-18 14:05:09','','2018-08-18 14:05:09','',13,1875.00,'',1875.00,20,23,0.00,0.00,NULL,0.00,'\0'),(147,39,5.00,350.00,0.00,1750.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,1750.00,'',1750.00,20,23,0.00,0.00,NULL,0.00,'\0'),(148,40,1.00,0.00,0.00,0.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(149,7,5.00,400.00,0.00,2000.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,2000.00,'',2000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(150,36,3.00,200.00,0.00,600.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(151,5,3.00,550.00,0.00,1650.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,1650.00,'',1650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(152,56,1.00,400.00,0.00,400.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(153,50,1.00,400.00,0.00,400.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(154,49,1.00,400.00,0.00,400.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(155,53,1.00,400.00,0.00,400.00,'2018-08-18 14:05:10','admin',33,'2018-08-18 14:05:10','','2018-08-18 14:05:10','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(156,37,3.00,250.00,0.00,750.00,'2018-08-18 14:54:24','admin',33,'2018-08-18 14:54:24','','2018-08-18 14:54:24','',13,750.00,'',750.00,20,23,0.00,0.00,NULL,0.00,'\0'),(157,6,3.00,600.00,0.00,1800.00,'2018-08-18 14:54:24','admin',33,'2018-08-18 14:54:24','','2018-08-18 14:54:24','',13,1800.00,'',1800.00,20,23,0.00,0.00,NULL,0.00,'\0'),(158,67,1.00,400.00,0.00,400.00,'2018-08-18 14:54:24','admin',33,'2018-08-18 14:54:24','','2018-08-18 14:54:24','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(163,61,0.00,0.00,0.00,0.00,'2018-08-18 15:23:11','admin',32,'2018-08-18 15:23:11','','2018-08-18 15:23:11','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(164,7,1.00,0.00,0.00,0.00,'2018-08-18 15:23:11','admin',32,'2018-08-18 15:23:11','','2018-08-18 15:23:11','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(167,7,2.00,500.00,0.00,1000.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,1000.00,'',1000.00,20,23,0.00,0.00,NULL,0.00,'\0'),(168,59,2.00,650.00,0.00,1300.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,1300.00,'',1300.00,20,23,0.00,0.00,NULL,0.00,'\0'),(169,9,2.00,450.00,0.00,900.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,900.00,'',900.00,20,23,0.00,0.00,NULL,0.00,'\0'),(170,50,1.00,500.00,0.00,500.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,500.00,'',500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(171,2,3.00,300.00,0.00,900.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,900.00,'',900.00,20,23,0.00,0.00,NULL,0.00,'\0'),(172,60,2.00,0.00,0.00,0.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,0.00,'',0.00,20,23,0.00,0.00,NULL,0.00,''),(173,38,2.00,450.00,0.00,900.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,900.00,'',900.00,20,23,0.00,0.00,NULL,0.00,'\0'),(174,10,3.00,200.00,0.00,600.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(175,28,1.00,200.00,0.00,200.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,200.00,'',200.00,20,23,0.00,0.00,NULL,0.00,'\0'),(176,27,2.00,200.00,0.00,400.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,400.00,'',400.00,20,23,0.00,0.00,NULL,0.00,'\0'),(177,5,1.00,650.00,0.00,650.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(178,6,1.00,650.00,0.00,650.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(179,39,2.00,450.00,0.00,900.00,'2018-08-18 15:41:51','admin',35,'2018-08-18 15:41:51','','2018-08-18 15:41:51','',13,900.00,'',900.00,20,23,0.00,0.00,NULL,0.00,'\0'),(180,68,2.00,350.00,0.00,700.00,'2018-08-18 16:09:32','admin',35,'2018-08-18 16:09:32','','2018-08-18 16:09:32','',13,700.00,'',700.00,20,23,0.00,0.00,NULL,0.00,'\0'),(181,11,12.00,50.00,0.00,600.00,'2018-08-18 16:09:32','admin',35,'2018-08-18 16:09:32','','2018-08-18 16:09:32','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(182,69,2.00,350.00,0.00,700.00,'2018-08-18 16:09:32','admin',35,'2018-08-18 16:09:32','','2018-08-18 16:09:32','',13,700.00,'',700.00,20,23,0.00,0.00,NULL,0.00,'\0'),(183,66,1.00,650.00,0.00,650.00,'2018-08-18 16:18:16','admin',35,'2018-08-18 16:18:16','','2018-08-18 16:18:16','',13,650.00,'',650.00,20,23,0.00,0.00,NULL,0.00,'\0'),(184,79,1.00,2600.00,0.00,2600.00,'2018-08-22 10:38:24','admin',36,'2018-08-22 10:38:24',NULL,'2018-08-22 10:38:24','',13,2600.00,'',2600.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(185,44,1.00,600.00,0.00,600.00,'2018-08-22 10:45:38','admin',37,'2018-08-22 10:45:38','','2018-08-22 10:45:38','',13,600.00,'',600.00,20,23,0.00,0.00,NULL,0.00,'\0'),(186,79,1.00,2600.00,0.00,2600.00,'2018-08-22 10:58:04','admin',38,'2018-08-22 10:58:04',NULL,'2018-08-22 10:58:04','',13,2600.00,'',2600.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(187,29,1.00,650.00,0.00,650.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,650.00,'',650.00,22,23,0.00,0.00,NULL,0.00,'\0'),(188,30,1.00,650.00,0.00,650.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,650.00,'',650.00,22,23,0.00,0.00,NULL,0.00,'\0'),(189,31,1.00,700.00,0.00,700.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,700.00,'',700.00,22,23,0.00,0.00,NULL,0.00,'\0'),(190,32,1.00,500.00,0.00,500.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,500.00,'',500.00,22,23,0.00,0.00,NULL,0.00,'\0'),(191,33,1.00,0.00,0.00,0.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,0.00,'',0.00,22,23,0.00,0.00,NULL,0.00,''),(192,35,1.00,0.00,0.00,0.00,'2018-08-22 11:58:40','admin',39,'2018-08-22 11:58:40','','2018-08-22 11:58:40','',13,0.00,'',0.00,22,23,0.00,0.00,NULL,0.00,'\0'),(193,29,1.00,650.00,0.00,650.00,'2018-09-03 21:15:18','admin',14,'2018-09-03 21:15:18',NULL,'2018-09-03 21:15:18','',13,650.00,'',650.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(194,36,1.00,350.00,0.00,350.00,'2018-09-03 21:15:18','admin',14,'2018-09-03 21:15:18',NULL,'2018-09-03 21:15:18','',13,350.00,'',350.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(195,44,1.00,600.00,0.00,600.00,'2018-09-03 21:15:18','admin',14,'2018-09-03 21:15:18',NULL,'2018-09-03 21:15:18','',13,600.00,'',600.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(196,69,1.00,600.00,0.00,600.00,'2018-09-03 21:15:18','admin',14,'2018-09-03 21:15:18',NULL,'2018-09-03 21:15:18','',13,600.00,'',600.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(197,30,1.00,650.00,0.00,650.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,650.00,'',650.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(198,45,1.00,600.00,0.00,600.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,600.00,'',600.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(199,15,1.00,500.00,0.00,500.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(200,22,1.00,500.00,0.00,500.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(201,23,1.00,500.00,0.00,500.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(202,24,1.00,500.00,0.00,500.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(203,41,1.00,500.00,0.00,500.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(204,63,1.00,450.00,0.00,450.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,450.00,'',450.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(205,2,1.00,400.00,0.00,400.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,400.00,'',400.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(206,64,1.00,300.00,0.00,300.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,300.00,'',300.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(207,66,1.00,800.00,0.00,800.00,'2018-09-03 21:17:51','admin',14,'2018-09-03 21:17:51',NULL,'2018-09-03 21:17:51','',13,800.00,'',800.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(208,1,1.00,1500.00,0.00,1500.00,'2018-09-03 21:18:56','admin',14,'2018-09-03 21:18:56',NULL,'2018-09-03 21:18:56','',13,1500.00,'',1500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(209,34,1.00,3000.00,0.00,3000.00,'2018-09-03 21:18:56','admin',14,'2018-09-03 21:18:56',NULL,'2018-09-03 21:18:56','',13,3000.00,'',3000.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(210,35,1.00,0.00,0.00,0.00,'2018-09-03 21:18:56','admin',14,'2018-09-03 21:18:56',NULL,'2018-09-03 21:18:56','',13,0.00,'',0.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(211,79,1.00,2600.00,0.00,2600.00,'2018-09-03 21:18:56','admin',14,'2018-09-03 21:18:56',NULL,'2018-09-03 21:18:56','',13,2600.00,'',2600.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(212,4,1.00,650.00,0.00,650.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,650.00,'',650.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(213,31,1.00,700.00,0.00,700.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,700.00,'',700.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(214,50,1.00,700.00,0.00,700.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,700.00,'',700.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(215,51,1.00,500.00,0.00,500.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(216,51,1.00,500.00,0.00,500.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(217,52,1.00,500.00,0.00,500.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,500.00,'',500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(218,53,1.00,700.00,0.00,700.00,'2018-09-03 21:31:59','admin',14,'2018-09-03 21:31:59',NULL,'2018-09-03 21:31:59','',13,700.00,'',700.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(219,80,2.00,500.00,0.00,1000.00,'2018-09-04 00:18:10','admin',40,'2018-09-04 00:18:10','','2018-09-04 00:18:10','',13,1000.00,'',1000.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(220,80,3.00,500.00,256.50,1681.50,'2018-09-04 06:40:44','admin',41,'2018-09-04 06:40:44','','2018-09-04 06:40:44','',14,1425.00,'',1770.00,NULL,NULL,75.00,0.00,NULL,5.00,'\0'),(221,5,2.00,850.00,0.00,1700.00,'2018-09-08 11:36:03','admin',42,'2018-09-08 11:35:59','','2018-09-08 11:36:03','',13,1700.00,'',1700.00,20,23,0.00,0.00,NULL,0.00,'\0'),(222,6,1.00,950.00,0.00,950.00,'2018-09-08 11:36:03','admin',42,'2018-09-08 11:36:03','','2018-09-08 11:36:03','',13,950.00,'',950.00,20,23,0.00,0.00,NULL,0.00,'\0'),(223,5,1.00,850.00,153.00,1003.00,'2018-09-09 10:52:36','admin',43,'2018-09-09 10:52:36',NULL,'2018-09-09 10:52:36','',14,850.00,'',1003.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(224,29,1.00,650.00,117.00,767.00,'2018-09-09 10:52:36','admin',43,'2018-09-09 10:52:36',NULL,'2018-09-09 10:52:36','',14,650.00,'',767.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(225,44,2.00,600.00,216.00,1416.00,'2018-09-09 10:52:36','admin',43,'2018-09-09 10:52:36',NULL,'2018-09-09 10:52:36','',14,1200.00,'',1416.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(226,5,1.00,850.00,0.00,850.00,'2018-09-09 10:52:36','admin',43,'2018-09-09 10:52:36',NULL,'2018-09-09 10:52:36','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(227,5,1.00,850.00,0.00,850.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(228,5,1.00,850.00,0.00,850.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(229,5,1.00,850.00,0.00,850.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(230,36,1.00,350.00,0.00,350.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,350.00,'',350.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(231,69,1.00,600.00,0.00,600.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,600.00,'',600.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(232,5,1.00,850.00,0.00,850.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(233,69,1.00,600.00,0.00,600.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,600.00,'',600.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(234,36,1.00,350.00,0.00,350.00,'2018-09-13 22:16:09','admin',14,'2018-09-13 22:16:09',NULL,'2018-09-13 22:16:09','',13,350.00,'',350.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(235,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(236,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(237,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(238,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(239,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(240,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(241,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(242,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(243,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(244,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(245,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(246,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(247,29,1.00,650.00,0.00,650.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,650.00,'',650.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(248,5,1.00,850.00,0.00,850.00,'2018-09-13 22:17:35','admin',14,'2018-09-13 22:17:35',NULL,'2018-09-13 22:17:35','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(249,5,4.00,850.00,0.00,3400.00,'2018-09-17 23:20:23','admin',14,'2018-09-17 23:20:23',NULL,'2018-09-17 23:20:23','',13,3400.00,'',3400.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(250,30,20.00,650.00,0.00,13000.00,'2018-09-17 23:20:23','admin',14,'2018-09-17 23:20:23',NULL,'2018-09-17 23:20:23','',13,13000.00,'',13000.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(251,22,5.00,500.00,0.00,2500.00,'2018-09-17 23:20:23','admin',14,'2018-09-17 23:20:23',NULL,'2018-09-17 23:20:23','',13,2500.00,'',2500.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(252,5,1.00,850.00,0.00,850.00,'2018-09-24 17:56:05','admin',45,'2018-09-24 17:56:05','','2018-09-24 17:56:05','',13,850.00,'',850.00,20,23,0.00,0.00,NULL,0.00,'\0'),(253,6,1.00,950.00,171.00,1121.00,'2018-10-01 22:01:30','admin',47,'2018-10-01 22:01:30','','2018-10-01 22:01:30','',14,950.00,'',1121.00,20,23,0.00,0.00,NULL,0.00,'\0'),(254,15,1.00,500.00,90.00,590.00,'2018-10-01 22:01:31','admin',47,'2018-10-01 22:01:31','','2018-10-01 22:01:31','',14,500.00,'',590.00,20,23,0.00,0.00,NULL,0.00,'\0'),(255,80,1.00,500.00,0.00,500.00,'2018-10-08 22:37:38','admin',51,'2018-10-08 22:37:38','','2018-10-08 22:37:38','',13,500.00,'',500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(256,38,1.00,800.00,0.00,800.00,'2018-10-08 23:08:08','admin',53,'2018-10-08 23:08:08','','2018-10-08 23:08:08','',13,800.00,'',800.00,20,23,0.00,0.00,NULL,0.00,'\0'),(257,80,1.00,500.00,0.00,500.00,'2018-10-08 23:08:10','admin',53,'2018-10-08 23:08:10','','2018-10-08 23:08:10','',13,500.00,'',500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(258,38,1.00,800.00,144.00,944.00,'2018-10-09 07:54:33','admin',54,'2018-10-09 07:54:33','','2018-10-09 07:54:33','',14,800.00,'',944.00,20,23,0.00,0.00,NULL,0.00,'\0'),(259,80,1.00,500.00,90.00,590.00,'2018-10-09 07:54:34','admin',54,'2018-10-09 07:54:34','','2018-10-09 07:54:34','',14,500.00,'',590.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(260,1,1.00,1500.00,0.00,1500.00,'2018-11-15 15:14:31','a55cd251-d6bb-4039-a91b-83f24e40d644',55,'2018-11-15 15:14:31','','2018-11-15 15:14:31','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(261,1,1.00,1500.00,0.00,1500.00,'2018-11-15 15:17:29','a55cd251-d6bb-4039-a91b-83f24e40d644',56,'2018-11-15 15:17:29','','2018-11-15 15:17:29','',13,1500.00,'',1500.00,20,23,0.00,0.00,NULL,0.00,'\0'),(262,5,1.00,850.00,0.00,850.00,'2018-12-25 01:37:39','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',57,'2018-12-25 01:37:39',NULL,'2018-12-25 01:37:39','',13,850.00,'',850.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(263,80,1.00,500.00,0.00,500.00,'2018-12-25 01:37:39','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',57,'2018-12-25 01:37:39',NULL,'2018-12-25 01:37:39','',13,500.00,'',500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(264,6,1.00,950.00,0.00,950.00,'2018-12-25 01:38:31','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',57,'2018-12-25 01:38:31',NULL,'2018-12-25 01:38:31','',13,950.00,'',950.00,NULL,23,0.00,0.00,NULL,0.00,'\0'),(265,80,1.00,500.00,0.00,500.00,'2018-12-25 01:44:19','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',60,'2018-12-25 01:44:19',NULL,'2018-12-25 01:44:19','',13,500.00,'',500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(266,80,1.00,500.00,0.00,500.00,'2018-12-25 01:44:26','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',61,'2018-12-25 01:44:26','','2018-12-25 01:44:26','',13,500.00,'',500.00,NULL,NULL,0.00,0.00,NULL,0.00,'\0'),(267,10,1.00,260.00,0.00,260.00,'2018-12-25 02:03:01','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',62,'2018-12-25 02:03:01','','2018-12-25 02:03:01','',13,260.00,'',260.00,20,23,0.00,0.00,NULL,0.00,'\0');
/*!40000 ALTER TABLE `detallefactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(32) NOT NULL,
  `NumeroFactura` varchar(50) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Estado` varchar(1) NOT NULL,
  `FechaFacturado` datetime DEFAULT NULL,
  `MontoItbis` decimal(18,6) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `MontoGrabado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MontoDevuelto` decimal(18,6) NOT NULL,
  `MontoRecibido` decimal(18,6) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `MontoDescuento` decimal(18,6) NOT NULL,
  `MontoNotaCreditoAplicada` decimal(18,6) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `PorcientoDescuento` decimal(18,6) NOT NULL,
  `LocalidadId` int(32) DEFAULT NULL,
  `NCF` varchar(100) DEFAULT NULL,
  `TipoNCF` varchar(45) DEFAULT NULL,
  `rnc` varchar(100) DEFAULT NULL,
  `NoDocumento` varchar(50) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `AlmacenId` int(32) NOT NULL DEFAULT '0',
  `CajaId` int(32) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `NumeroFactura_UNIQUE` (`NumeroFactura`),
  KEY `Factura_Moneda` (`MonedaId`),
  KEY `Factura_Cliente` (`ClienteId`),
  KEY `Factura_localidad_idx` (`LocalidadId`),
  KEY `factura_caja_idx` (`CajaId`),
  CONSTRAINT `Factura_Cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Factura_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Factura_localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `factura_caja` FOREIGN KEY (`CajaId`) REFERENCES `cajaregistradora` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (4,1,'FAC00001',11,7000.000000,4550.000000,'','F','2017-10-29 22:11:47',0.000000,'admin',7000.000000,NULL,'2018-08-28 19:44:26',0.000000,0.000000,2450.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2017-10-29 22:11:47',0,NULL),(5,3,'FAC00002',11,2020.000000,2020.000000,'','P','2017-12-02 17:25:11',0.000000,'admin',2020.000000,'',NULL,0.000000,0.000000,0.000000,0.000000,0.000000,'',0.000000,4,'','',NULL,NULL,'2017-12-02 17:25:11',0,NULL),(6,7,'FAC00003',11,800.000000,0.000000,'','A','2018-03-30 12:47:38',0.000000,'admin',800.000000,'admin',NULL,0.000000,0.000000,800.000000,0.000000,0.000000,'',0.000000,4,'','',NULL,NULL,'2018-03-30 12:47:38',0,NULL),(7,6,'FAC00004',11,22000.000000,22000.000000,'','P','2018-03-30 18:18:12',0.000000,'admin',22000.000000,'admin','2018-07-16 18:56:27',-22000.000000,0.000000,0.000000,0.000000,0.000000,'',0.000000,4,'','',NULL,NULL,'2018-03-30 18:18:12',0,NULL),(8,6,'FAC00005',11,15000.000000,7000.000000,'','F','2018-03-30 18:53:21',0.000000,'admin',15000.000000,NULL,'2018-07-16 18:56:27',0.000000,0.000000,8000.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2018-03-30 18:53:21',0,NULL),(9,7,'FAC00006',11,7400.000000,0.000000,'','F','2018-04-21 17:22:20',0.000000,'admin',7400.000000,'admin','2018-08-04 12:41:35',-7400.000000,0.000000,7400.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00103762308',NULL,'2018-04-21 17:22:20',0,NULL),(10,7,'FAC00007',11,1950.000000,1950.000000,'','P','2018-05-10 12:31:30',0.000000,'admin',1950.000000,'admin','2018-07-30 20:00:53',-1950.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2018-05-10 12:31:30',0,NULL),(11,7,'FAC00008',11,650.000000,650.000000,'','P','2018-05-10 12:59:01',0.000000,'admin',650.000000,NULL,'2018-07-30 20:00:53',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2018-05-10 12:59:01',0,NULL),(12,3,'FAC00009',11,3900.000000,3900.000000,'','P','2018-05-10 13:13:24',0.000000,'admin',3900.000000,NULL,'2018-07-16 18:19:43',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2018-05-10 13:13:24',0,NULL),(13,4,'FAC00010',11,6400.000000,0.000000,'','A','2018-05-10 16:13:20',0.000000,'admin',6400.000000,'admin','2018-07-30 22:14:00',0.000000,0.000000,6400.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,NULL,NULL,'2018-05-10 16:13:20',0,NULL),(14,2,'COT00001',11,60200.000000,0.000000,'','C',NULL,0.000000,'admin',60200.000000,'admin','2018-09-17 23:20:23',-60200.000000,0.000000,60200.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00100002209','COT00001','2018-08-20 00:00:00',0,NULL),(16,9,'FAC00011',11,45800.000000,45800.000000,'','P','2018-07-10 14:03:49',0.000000,'admin',45800.000000,NULL,'2018-07-16 19:07:11',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'809569774',NULL,'2018-07-10 14:03:49',0,NULL),(17,10,'FAC00012',11,3950.000000,3000.000000,'','F','2018-07-16 17:49:26',0.000000,'admin',3950.000000,NULL,'2018-07-16 18:20:34',0.000000,0.000000,950.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-07-16 17:49:26',0,NULL),(18,3,'FAC00013',11,650.000000,650.000000,'','P','2018-07-16 18:30:34',0.000000,'admin',650.000000,NULL,'2018-07-16 18:32:36',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00102762804',NULL,'2018-07-16 18:30:34',0,NULL),(19,3,'FAC00014',11,650.000000,650.000000,'','P','2018-07-16 19:28:09',0.000000,'admin',650.000000,NULL,'2018-07-30 19:57:59',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00102762804',NULL,'2018-07-16 19:28:09',0,NULL),(20,11,'FAC00015',11,1999.000000,700.000000,'','F','2018-07-16 19:38:46',0.000000,'admin',1999.000000,NULL,'2018-07-16 19:45:19',0.000000,0.000000,1299.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-07-16 19:38:46',0,NULL),(21,12,'FAC00016',11,4200.000000,4200.000000,'','P','2018-07-16 20:05:50',0.000000,'admin',4200.000000,NULL,'2018-07-16 20:06:50',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-07-16 20:05:50',0,NULL),(22,3,'FAC00017',11,2500.000000,2500.000000,'','P','2018-07-25 20:01:03',0.000000,'admin',2500.000000,NULL,'2018-08-04 12:32:13',0.000000,0.000000,0.000000,0.000000,0.000000,'ESTE BOLSO ERA DE LA CONSIGNACION',0.000000,4,NULL,NULL,'00102762804',NULL,'2018-07-25 20:01:03',0,NULL),(23,13,'FAC00018',11,1650.000000,1650.000000,'','P','2018-07-25 21:21:06',0.000000,'admin',1650.000000,NULL,'2018-07-25 21:22:22',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-07-25 21:21:06',0,NULL),(24,14,'FAC00019',11,1300.000000,1300.000000,'','P','2018-07-30 20:41:34',0.000000,'admin',1300.000000,NULL,'2018-08-04 16:20:41',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-07-30 20:41:34',0,NULL),(25,14,'FAC00020',11,4000.000000,4000.000000,'','P','2018-08-04 13:05:53',0.000000,'admin',4000.000000,NULL,'2018-08-04 16:20:41',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-08-04 13:05:53',0,NULL),(30,7,'FAC00021',11,4850.000000,0.000000,'','F','2018-08-04 16:43:18',0.000000,'admin',4850.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,4850.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00103762308',NULL,'2018-08-04 16:43:18',0,NULL),(31,15,'FAC00022',11,15050.000000,0.000000,'','F','2018-08-08 13:31:29',0.000000,'admin',15050.000000,'admin','2018-08-08 15:26:04',-15050.000000,0.000000,15050.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'',NULL,'2018-08-08 13:31:29',0,NULL),(32,16,'FAC00023',11,750.000000,0.000000,'','F','2018-08-18 12:54:45',0.000000,'admin',750.000000,'admin','2018-08-21 19:11:25',-750.000000,0.000000,750.000000,0.000000,0.000000,'2 DA. FACTURA',0.000000,4,NULL,NULL,'00113057160',NULL,'2018-08-18 12:54:45',0,NULL),(33,16,'FAC00024',11,18575.000000,0.000000,'','F','2018-08-18 14:05:09',0.000000,'admin',18575.000000,'admin','2018-08-21 19:03:22',-18575.000000,0.000000,18575.000000,0.000000,0.000000,'DISTRIBUIDORA KAREN',0.000000,4,NULL,NULL,'00113057160',NULL,'2018-08-18 14:05:09',0,NULL),(35,17,'FAC00025',11,11550.000000,0.000000,'','F','2018-08-18 15:41:51',0.000000,'admin',11550.000000,'admin','2018-08-21 19:40:20',-11550.000000,0.000000,11550.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'8098444512',NULL,'2018-08-18 15:41:51',0,NULL),(36,7,'FAC00026',11,2600.000000,0.000000,'','A','2018-08-22 10:38:24',0.000000,'admin',2600.000000,'admin','2018-08-22 10:44:00',0.000000,0.000000,2600.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00103762308',NULL,'2018-08-22 10:38:24',0,NULL),(37,7,'FAC00027',11,600.000000,0.000000,'','A','2018-08-22 10:45:38',0.000000,'admin',600.000000,'admin','2018-08-22 10:51:32',0.000000,0.000000,600.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00103762308',NULL,'2018-08-22 10:45:38',0,NULL),(38,7,'FAC00028',11,2600.000000,0.000000,'','A','2018-08-22 10:58:04',0.000000,'admin',2600.000000,'admin','2018-08-22 10:59:54',0.000000,0.000000,2600.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00103762308',NULL,'2018-08-22 10:58:04',0,NULL),(39,3,'FAC00029',11,2500.000000,2000.000000,'','F','2018-08-22 11:58:40',0.000000,'admin',2500.000000,NULL,'2018-08-26 20:32:07',0.000000,0.000000,500.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'00102762804',NULL,'2018-08-22 11:58:40',0,NULL),(40,14,'FAC00030',11,1000.000000,0.000000,'','A','2018-09-04 00:18:10',0.000000,'admin',1000.000000,'admin','2018-09-04 06:33:52',-1000.000000,0.000000,1000.000000,0.000000,0.000000,NULL,0.000000,4,'B0100014584','01','22400115873',NULL,'2018-09-04 00:18:10',0,NULL),(41,18,'FAC00031',11,1681.500000,0.000000,'','F','2018-09-04 06:40:44',256.500000,'admin',1425.000000,'admin','2018-09-04 08:19:32',-1681.500000,0.000000,1681.500000,75.000000,0.000000,NULL,5.000000,4,'B0100014585','01','22344400040',NULL,'2018-09-04 06:40:44',0,NULL),(42,14,'FAC00032',11,2650.000000,0.000000,'','F','2018-09-08 11:35:38',0.000000,'admin',2650.000000,'admin','2018-09-24 16:32:49',0.000000,0.000000,2650.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-09-08 11:35:24',0,NULL),(43,18,'COT00002',11,4036.000000,0.000000,'','C',NULL,486.000000,'admin',3550.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,4036.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22344400040','COT00002','2018-09-09 10:52:34',0,NULL),(45,14,'FAC00033',11,850.000000,850.000000,'','P','2018-09-24 17:56:05',0.000000,'admin',850.000000,NULL,'0001-01-01 00:00:00',150.000000,1000.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-09-24 17:56:05',0,NULL),(46,14,'FAC00034',11,500.000000,0.000000,'','F','2018-09-24 04:00:00',0.000000,'admin',500.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,500.000000,0.000000,0.000000,'ascasd',0.000000,4,NULL,NULL,NULL,NULL,'2018-09-24 17:59:25',0,NULL),(47,18,'FAC00035',11,1711.000000,0.000000,'','F','2018-10-01 22:01:30',261.000000,'admin',1450.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,1711.000000,0.000000,0.000000,NULL,0.000000,4,'B0100014586','01','22344400040',NULL,'2018-10-01 22:01:29',20,NULL),(51,14,'FAC00036',11,500.000000,40.000000,'','F','2018-10-08 22:37:38',0.000000,'admin',500.000000,NULL,'2018-12-25 07:40:13',0.000000,0.000000,460.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-10-08 22:37:36',20,NULL),(53,14,'FAC00037',11,1300.000000,0.000000,'','F','2018-10-08 23:08:08',0.000000,'admin',1300.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,1300.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-10-08 23:08:07',20,NULL),(54,18,'FAC00038',11,1534.000000,0.000000,'','F','2018-10-09 07:54:33',234.000000,'admin',1300.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,1534.000000,0.000000,0.000000,NULL,0.000000,4,'B0100014587','01','22344400040',NULL,'2018-10-09 07:54:31',20,NULL),(55,14,'FAC00039',11,1500.000000,0.000000,'','F','2018-11-15 15:14:31',0.000000,'a55cd251-d6bb-4039-a91b-83f24e40d644',1500.000000,NULL,'0001-01-01 00:00:00',0.000000,0.000000,1500.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-11-15 15:14:31',20,1),(56,14,'FAC00040',11,1500.000000,1500.000000,'','P','2018-11-15 15:17:29',0.000000,'a55cd251-d6bb-4039-a91b-83f24e40d644',1500.000000,NULL,'0001-01-01 00:00:00',0.000000,1500.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873',NULL,'2018-11-15 15:17:29',20,1),(57,14,'COT00003',11,2300.000000,0.000000,'','C',NULL,0.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6',2300.000000,'cjimenez','2018-12-25 01:38:31',-2300.000000,0.000000,2300.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873','COT00003','2018-12-25 01:37:39',0,1),(60,14,'COT00004',11,500.000000,0.000000,'','D',NULL,0.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6',500.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6','2018-12-25 01:44:26',0.000000,0.000000,500.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873','COT00004','2018-12-25 01:44:19',20,1),(61,14,'FAC00041',11,500.000000,500.000000,'','P','2018-12-25 01:44:26',0.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6',500.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6','2018-12-25 07:40:13',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,NULL,NULL,'22400115873','COT00004','2018-12-25 01:44:19',20,1),(62,14,'FAC00042',11,260.000000,260.000000,'','P','2018-12-25 02:03:01',0.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6',260.000000,NULL,'2018-12-25 07:40:13',0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,4,'B0100014588','01','22400115873',NULL,'2018-12-25 02:03:01',20,1);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturarproductoservicio`
--

DROP TABLE IF EXISTS `facturarproductoservicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturarproductoservicio` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `EsServicio` bit(1) NOT NULL,
  `EsCompuesto` bit(1) NOT NULL,
  `NombreDeClase` varchar(2000) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturarproductoservicio`
--

LOCK TABLES `facturarproductoservicio` WRITE;
/*!40000 ALTER TABLE `facturarproductoservicio` DISABLE KEYS */;
INSERT INTO `facturarproductoservicio` VALUES (20,'\0','\0','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarProducto, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0'),(21,'','\0','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarServicioSimple, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0'),(22,'','','PuntoDeVenta.Service.Helpers.FacturarServicios.FacturarServicioCompuesto, PuntoDeVenta.Service, culture=neutral , version=1.0.0.0');
/*!40000 ALTER TABLE `facturarproductoservicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastoempresa`
--

DROP TABLE IF EXISTS `gastoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastoempresa` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(32) NOT NULL,
  `ReferenciaGasto` varchar(50) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Estado` varchar(1) NOT NULL,
  `FechaEmision` datetime NOT NULL,
  `MontoItbis` decimal(18,6) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `MontoGrabado` decimal(18,6) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `MontoDevuelto` decimal(18,6) NOT NULL,
  `MontoDado` decimal(18,6) NOT NULL,
  `MontoAdeudado` decimal(18,6) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `LocalidadId` int(32) DEFAULT NULL,
  `NCF` varchar(100) DEFAULT NULL,
  `FechaCreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `ReferenciaGasto_UNIQUE` (`ReferenciaGasto`),
  KEY `Gasto_Moneda` (`MonedaId`),
  KEY `Gasto_Suplidor` (`SuplidorId`),
  KEY `Gasto_Localidad_idx` (`LocalidadId`),
  CONSTRAINT `Gasto_Localidad` FOREIGN KEY (`LocalidadId`) REFERENCES `localidad` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Gasto_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Gasto_Suplidor` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastoempresa`
--

LOCK TABLES `gastoempresa` WRITE;
/*!40000 ALTER TABLE `gastoempresa` DISABLE KEYS */;
INSERT INTO `gastoempresa` VALUES (1,17,'PG00008',11,2356.000000,1500.000000,'','I','2018-09-24 04:00:00',856.000000,'admin',1500.000000,NULL,'2018-09-24 17:19:15',0.000000,0.000000,856.000000,'algo',4,'0','2018-09-24 16:42:24'),(2,17,'PG00009',11,1750.000000,300.000000,'','I','2018-12-11 04:00:00',250.000000,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6',1500.000000,NULL,'0001-01-01 00:00:00',0.000000,300.000000,1450.000000,'compra de materia prima',4,'A2101301020','2018-12-25 07:48:54');
/*!40000 ALTER TABLE `gastoempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuesto`
--

DROP TABLE IF EXISTS `impuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impuesto` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Porciento` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impuesto`
--

LOCK TABLES `impuesto` WRITE;
/*!40000 ALTER TABLE `impuesto` DISABLE KEYS */;
INSERT INTO `impuesto` VALUES (13,'NO APLICA',0.00,'admin','2017-10-29 20:37:27',NULL,'0001-01-01 00:00:00 ',''),(14,'ITBIS',0.18,'admin','2017-10-29 20:37:48',NULL,'0001-01-01 00:00:00 ','');
/*!40000 ALTER TABLE `impuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
INSERT INTO `localidad` VALUES (4,'PRINCIPAL','admin','2017-10-29 20:35:22',NULL,'0001-01-01 00:00:00','');
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moneda`
--

DROP TABLE IF EXISTS `moneda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moneda` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Codigo` varchar(45) NOT NULL,
  `Tasa` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `EsMonedaLocal` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moneda`
--

LOCK TABLES `moneda` WRITE;
/*!40000 ALTER TABLE `moneda` DISABLE KEYS */;
INSERT INTO `moneda` VALUES (11,'PESOS DOMINICANOS','DOP',1.00,'admin','2017-10-29 20:36:10',NULL,'0001-01-01 00:00:00','','');
/*!40000 ALTER TABLE `moneda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `montosapertura`
--

DROP TABLE IF EXISTS `montosapertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `montosapertura` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Monto` varchar(128) NOT NULL,
  `Orden` int(32) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `montosapertura`
--

LOCK TABLES `montosapertura` WRITE;
/*!40000 ALTER TABLE `montosapertura` DISABLE KEYS */;
INSERT INTO `montosapertura` VALUES (17,'1','1',0,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(18,'5','5',1,'noel','2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00',''),(19,'10','10',2,'noel','2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00',''),(20,'25','25',3,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(21,'50','50',4,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(22,'100','100',5,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(23,'200','200',6,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(24,'500','500',7,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(25,'1000','1000',8,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00',''),(26,'2000','2000',9,'noel','2018-01-01 00:00:00','noel','2018-01-01 00:00:00','');
/*!40000 ALTER TABLE `montosapertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientoalmacen`
--

DROP TABLE IF EXISTS `movimientoalmacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoalmacen` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `AlmacenId` int(32) unsigned NOT NULL,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,6) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  `LocalidadId` int(32) NOT NULL,
  `TipoMovimiento` varchar(5) NOT NULL,
  `Referencia` varchar(50) NOT NULL DEFAULT '',
  `BalanceActual` decimal(18,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`Id`),
  KEY `movimiento_producto` (`ProductoId`),
  KEY `movimiento_AlmacenId` (`AlmacenId`),
  KEY `movimiento_unidad` (`UnidadId`),
  KEY `movimiento_TipoMovimiento` (`TipoMovimiento`),
  CONSTRAINT `movimientoalmacen_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoalmacen_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoalmacen_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoalmacen_ibfk_4` FOREIGN KEY (`TipoMovimiento`) REFERENCES `tipomovimiento` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=387 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientoalmacen`
--

LOCK TABLES `movimientoalmacen` WRITE;
/*!40000 ALTER TABLE `movimientoalmacen` DISABLE KEYS */;
INSERT INTO `movimientoalmacen` VALUES (33,20,2,35.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(34,20,3,59.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(35,20,4,7.000000,'2017-10-29 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(36,20,5,20.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(37,20,6,11.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(38,20,9,11.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(39,20,1,20.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(40,20,8,3.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(41,20,7,10.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(42,20,10,2.000000,'2017-11-01 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(43,20,12,4.000000,'2017-11-04 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(44,20,14,18.000000,'2018-03-30 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(45,20,15,0.000000,'2018-03-30 00:00:00','2018-01-01 00:00:00','admin','admin','',23,0,'IN','INICIO',0.000000),(46,20,21,35.000000,'2018-04-18 09:09:14','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00004',0.000000),(47,20,26,4.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(48,20,22,3.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(49,20,23,5.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(50,20,24,1.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(51,20,19,14.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(52,20,16,12.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(53,20,20,11.000000,'2018-04-18 09:24:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00005',0.000000),(54,20,16,-2.000000,'2018-04-21 17:22:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(55,20,19,-2.000000,'2018-04-21 17:22:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(56,20,20,-2.000000,'2018-04-21 17:22:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(57,20,36,20.000000,'2018-05-10 11:35:05','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00006',0.000000),(58,20,37,20.000000,'2018-05-10 11:37:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00007',0.000000),(59,20,38,20.000000,'2018-05-10 11:37:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00007',0.000000),(60,20,25,14.000000,'2018-05-10 11:42:01','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00008',0.000000),(61,20,39,11.000000,'2018-05-10 12:21:54','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00009',0.000000),(62,20,40,8.000000,'2018-05-10 12:21:54','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00009',0.000000),(63,20,5,-2.000000,'2018-05-10 12:31:31','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00007',0.000000),(64,20,6,-1.000000,'2018-05-10 12:31:31','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00007',0.000000),(65,20,16,2.000000,'2018-05-10 12:52:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00006',0.000000),(66,20,16,-2.000000,'2018-05-10 12:52:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(67,20,39,-1.000000,'2018-05-10 12:52:59','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(68,20,40,-1.000000,'2018-05-10 12:52:59','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(69,20,5,-1.000000,'2018-05-10 12:59:01','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00008',0.000000),(70,20,10,-1.000000,'2018-05-10 13:13:25','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(71,20,26,-1.000000,'2018-05-10 13:13:25','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(72,20,19,-2.000000,'2018-05-10 13:13:25','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(73,20,38,-1.000000,'2018-05-10 13:13:25','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(74,20,16,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(75,20,9,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(76,20,39,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(77,20,39,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(78,20,40,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(79,20,20,-1.000000,'2018-05-10 13:13:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00009',0.000000),(80,20,1,-2.000000,'2018-05-10 15:28:33','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(81,20,39,-1.000000,'2018-05-10 15:28:33','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00006',0.000000),(82,20,41,23.000000,'2018-05-10 15:42:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00010',0.000000),(83,20,1,-1.000000,'2018-05-10 16:13:20','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(84,20,41,-3.000000,'2018-05-10 16:13:20','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(85,20,41,-1.000000,'2018-05-10 16:13:20','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(86,20,19,-2.000000,'2018-05-10 16:13:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(87,20,20,-1.000000,'2018-05-10 16:13:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(88,20,20,-1.000000,'2018-05-10 16:13:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(89,20,38,-1.000000,'2018-05-10 16:13:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(90,20,39,-1.000000,'2018-05-10 16:13:21','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(91,20,40,-1.000000,'2018-05-10 16:13:22','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(92,20,40,-1.000000,'2018-05-10 16:13:22','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(93,20,16,-1.000000,'2018-05-10 16:13:22','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00010',0.000000),(94,20,1,-2.000000,'2018-05-29 20:07:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00001',0.000000),(95,23,1,2.000000,'2018-05-29 20:07:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00001',0.000000),(96,20,7,20.000000,'2018-06-20 14:15:22','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00012',0.000000),(98,20,5,-12.000000,'2018-07-10 14:03:49','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00011',0.000000),(99,20,36,-10.000000,'2018-07-10 14:03:49','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00011',0.000000),(100,20,37,-5.000000,'2018-07-10 14:03:49','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00011',0.000000),(101,20,1,-15.000000,'2018-07-10 14:03:49','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00011',0.000000),(102,20,6,-8.000000,'2018-07-10 14:03:49','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00011',0.000000),(103,20,62,9.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(104,20,60,11.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(105,20,61,4.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(106,20,59,18.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(107,20,58,7.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(108,20,57,10.000000,'2018-07-10 14:45:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00013',0.000000),(109,20,56,7.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(110,20,54,5.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(111,20,53,6.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(112,20,55,7.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(113,20,50,11.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(114,20,49,13.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(115,20,48,30.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(116,20,46,15.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(117,20,45,32.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(118,20,44,28.000000,'2018-07-10 15:08:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(119,20,15,20.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(120,20,39,5.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(121,20,32,25.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(122,20,31,27.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(123,20,30,28.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(124,20,29,32.000000,'2018-07-10 15:08:48','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00014',0.000000),(125,20,21,-2.000000,'2018-07-16 17:49:27','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00012',0.000000),(126,20,36,-1.000000,'2018-07-16 17:49:27','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00012',0.000000),(127,20,37,-1.000000,'2018-07-16 17:49:27','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00012',0.000000),(128,20,22,-2.000000,'2018-07-16 17:49:27','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00012',0.000000),(129,20,5,-1.000000,'2018-07-16 18:30:34','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00013',0.000000),(130,20,5,-1.000000,'2018-07-16 19:28:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00014',0.000000),(131,20,14,-4.000000,'2018-07-16 19:38:46','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00015',0.000000),(132,20,39,-3.000000,'2018-07-16 19:38:46','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00015',0.000000),(133,20,21,-4.000000,'2018-07-16 20:05:50','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00016',0.000000),(134,20,6,-2.000000,'2018-07-25 21:21:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00018',0.000000),(135,20,5,-1.000000,'2018-07-25 21:21:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00018',0.000000),(136,20,1,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(137,20,41,3.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(138,20,41,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(139,20,19,2.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(140,20,20,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(141,20,20,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(142,20,38,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(143,20,39,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(144,20,40,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(145,20,40,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(146,20,16,1.000000,'2018-07-30 22:14:00','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00010',0.000000),(147,20,39,-1.000000,'2018-08-04 12:41:35','0001-01-01 00:00:00','system',NULL,'',23,0,'OUT','FAC00006',0.000000),(148,20,40,-1.000000,'2018-08-04 12:41:35','0001-01-01 00:00:00','system',NULL,'',23,0,'OUT','FAC00006',0.000000),(149,20,21,-4.000000,'2018-08-04 13:05:54','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00020',0.000000),(150,20,5,9.000000,'2018-08-04 16:29:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00015',0.000000),(151,20,6,4.000000,'2018-08-04 16:29:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00015',0.000000),(152,20,28,11.000000,'2018-08-04 16:29:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00015',0.000000),(153,20,10,11.000000,'2018-08-04 16:29:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00015',0.000000),(154,20,5,-2.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(155,20,6,-1.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(156,20,59,-1.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(157,20,62,-1.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(158,20,28,-4.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(159,20,10,-2.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(160,20,37,-1.000000,'2018-08-04 16:43:18','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00021',0.000000),(161,20,39,9.000000,'2018-08-06 19:59:53','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00017',0.000000),(162,20,9,16.000000,'2018-08-06 19:59:53','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00017',0.000000),(163,20,33,15.000000,'2018-08-06 19:59:53','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00017',0.000000),(164,20,29,-2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(165,23,29,2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(166,20,30,-2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(167,23,30,2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(168,20,31,-2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(169,23,31,2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(170,20,45,-2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(171,23,45,2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(172,20,44,-2.000000,'2018-08-06 20:23:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(173,23,44,2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(174,20,46,-2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(175,23,46,2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(176,20,33,-2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(177,23,33,2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(178,20,7,-2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(179,23,7,2.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(180,20,53,-1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(181,23,53,1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(182,20,50,-1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(183,23,50,1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(184,20,56,-1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(185,23,56,1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(186,20,49,-1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00002',0.000000),(187,23,49,1.000000,'2018-08-06 20:23:56','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00002',0.000000),(188,20,40,-1.000000,'2018-08-08 13:31:29','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(210,20,28,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(211,20,28,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(212,20,10,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(213,20,10,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(214,20,10,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(215,20,28,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(216,20,28,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(217,20,10,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(218,20,26,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(219,20,62,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(220,20,8,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(221,20,7,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(222,20,5,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(223,20,5,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(224,20,62,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(225,20,28,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(226,20,10,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(227,20,5,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(228,20,6,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(229,20,7,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(230,20,59,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(231,20,8,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(232,20,29,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(233,20,31,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(234,20,32,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(235,20,7,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(236,20,8,-1.000000,'2018-08-08 13:51:08','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(237,20,9,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(238,20,5,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(239,20,6,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(240,20,62,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(241,20,10,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(242,20,28,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(243,20,37,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(244,20,2,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(245,20,62,-1.000000,'2018-08-08 13:51:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(246,20,27,4.000000,'2018-08-08 14:33:37','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00018',0.000000),(247,20,47,33.000000,'2018-08-08 14:33:37','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00018',0.000000),(248,20,64,14.000000,'2018-08-08 14:46:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00019',0.000000),(249,20,63,8.000000,'2018-08-08 14:46:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00019',0.000000),(250,20,64,-10.000000,'2018-08-08 15:26:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(251,20,63,-7.000000,'2018-08-08 15:26:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00022',0.000000),(252,20,60,-1.000000,'2018-08-18 12:54:45','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00023',0.000000),(253,20,7,-1.000000,'2018-08-18 12:54:45','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00023',0.000000),(254,20,59,-5.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(255,20,38,-5.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(256,20,16,-2.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(257,20,60,-4.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(258,20,61,-2.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(259,20,12,-4.000000,'2018-08-18 14:05:09','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(260,20,9,-5.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(261,20,39,-5.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(262,20,40,-1.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(263,20,7,-5.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(264,20,36,-3.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(265,20,5,-3.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(266,20,56,-5.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(267,20,50,-2.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(268,20,49,-6.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(269,20,53,-5.000000,'2018-08-18 14:05:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(270,20,6,5.000000,'2018-08-18 14:12:46','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00020',0.000000),(271,20,35,25.000000,'2018-08-18 14:12:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00020',0.000000),(272,20,1,2.000000,'2018-08-18 14:12:47','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00020',0.000000),(273,20,67,15.000000,'2018-08-18 14:28:32','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00021',0.000000),(274,20,37,-3.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(275,20,6,-3.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(276,20,67,-1.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00024',0.000000),(277,20,53,4.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00024',0.000000),(278,20,50,1.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00024',0.000000),(279,20,49,5.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00024',0.000000),(280,20,56,4.000000,'2018-08-18 14:54:24','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00024',0.000000),(285,20,61,0.000000,'2018-08-18 15:23:11','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00023',0.000000),(286,20,7,-1.000000,'2018-08-18 15:23:11','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00023',0.000000),(289,20,7,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(290,20,59,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(291,20,9,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(292,20,50,-1.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(293,20,2,-3.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(294,20,60,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(295,20,38,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(296,20,10,-3.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(297,20,28,-1.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(298,20,27,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(299,20,5,-1.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(300,20,6,-1.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(301,20,39,-2.000000,'2018-08-18 15:41:51','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(302,20,68,12.000000,'2018-08-18 16:03:03','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00022',0.000000),(303,20,69,29.000000,'2018-08-18 16:03:03','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00022',0.000000),(304,20,11,20.000000,'2018-08-18 16:05:35','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00023',0.000000),(305,20,68,-2.000000,'2018-08-18 16:09:32','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(306,20,11,-12.000000,'2018-08-18 16:09:32','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(307,20,69,-2.000000,'2018-08-18 16:09:32','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(308,20,66,6.000000,'2018-08-18 16:17:27','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00024',0.000000),(309,20,66,-1.000000,'2018-08-18 16:18:16','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00025',0.000000),(310,20,22,2.000000,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00025',0.000000),(311,20,70,3.000000,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00025',0.000000),(312,20,72,2.000000,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00025',0.000000),(313,20,71,3.000000,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00025',0.000000),(314,20,75,1.000000,'2018-08-18 17:05:58','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00025',0.000000),(315,20,68,17.000000,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00026',0.000000),(316,20,26,5.000000,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00026',0.000000),(317,20,78,180.000000,'2018-08-21 20:02:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00026',0.000000),(318,20,53,-1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00003',0.000000),(319,22,53,1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00003',0.000000),(320,20,56,-1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00003',0.000000),(321,22,56,1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00003',0.000000),(322,20,50,-1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00003',0.000000),(323,22,50,1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00003',0.000000),(324,20,49,-1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00003',0.000000),(325,22,49,1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00003',0.000000),(326,20,67,-1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00003',0.000000),(327,22,67,1.000000,'2018-08-22 09:51:57','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00003',0.000000),(328,20,44,-1.000000,'2018-08-22 10:45:38','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00027',0.000000),(329,20,44,1.000000,'2018-08-22 10:51:32','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00027',0.000000),(330,20,44,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(331,22,44,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(332,20,45,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(333,22,45,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(334,20,46,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(335,22,46,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(336,20,47,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(337,22,47,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(338,20,33,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(339,22,33,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(340,20,35,-1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00004',0.000000),(341,22,35,1.000000,'2018-08-22 11:08:55','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00004',0.000000),(342,20,29,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(343,22,29,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(344,20,30,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(345,22,30,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(346,20,31,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(347,22,31,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(348,20,32,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(349,22,32,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(350,20,33,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(351,22,33,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(352,20,35,-1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00005',0.000000),(353,22,35,1.000000,'2018-08-22 11:13:12','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00005',0.000000),(354,20,9,-1.000000,'2018-08-22 11:54:45','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00006',0.000000),(355,22,9,1.000000,'2018-08-22 11:54:45','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00006',0.000000),(356,22,29,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(357,22,30,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(358,22,31,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(359,22,32,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(360,22,33,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(361,22,35,-1.000000,'2018-08-22 11:58:40','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00029',0.000000),(362,20,29,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(363,22,29,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(364,20,30,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(365,22,30,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(366,20,31,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(367,22,31,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(368,20,32,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(369,22,32,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(370,20,33,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(371,22,33,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(372,20,35,-1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','TRF00007',0.000000),(373,22,35,1.000000,'2018-08-22 12:07:26','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','TRF00007',0.000000),(374,20,5,-1.000000,'2018-09-08 11:36:03','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00032',0.000000),(375,20,6,-1.000000,'2018-09-08 11:36:04','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00032',0.000000),(376,20,5,1.000000,'2018-09-08 11:56:28','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','FAC00032',0.000000),(377,20,5,-1.000000,'2018-09-24 16:32:49','0001-01-01 00:00:00','system',NULL,'',23,0,'OUT','FAC00032',0.000000),(378,20,5,1.000000,'2018-09-24 17:54:06','0001-01-01 00:00:00','admin',NULL,'',23,0,'IN','ENT00027',0.000000),(379,20,5,-1.000000,'2018-09-24 17:56:05','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00033',0.000000),(380,20,6,-1.000000,'2018-10-01 22:01:31','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00035',0.000000),(381,20,15,-1.000000,'2018-10-01 22:01:31','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00035',0.000000),(382,20,38,-1.000000,'2018-10-08 23:08:10','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00037',0.000000),(383,20,38,-1.000000,'2018-10-09 07:54:34','0001-01-01 00:00:00','admin',NULL,'',23,0,'OUT','FAC00038',0.000000),(384,20,1,-1.000000,'2018-11-15 15:14:31','0001-01-01 00:00:00','a55cd251-d6bb-4039-a91b-83f24e40d644',NULL,'',23,0,'OUT','FAC00039',0.000000),(385,20,1,-1.000000,'2018-11-15 15:17:29','0001-01-01 00:00:00','a55cd251-d6bb-4039-a91b-83f24e40d644',NULL,'',23,0,'OUT','FAC00040',0.000000),(386,20,10,-1.000000,'2018-12-25 02:03:01','0001-01-01 00:00:00','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',NULL,'',23,0,'OUT','FAC00042',0.000000);
/*!40000 ALTER TABLE `movimientoalmacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodevolucion`
--

DROP TABLE IF EXISTS `movimientodevolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientodevolucion` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `SuplidorId` int(32) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(32) unsigned NOT NULL,
  `MontoItbis` decimal(18,2) DEFAULT '0.00',
  `MontoTotal` decimal(18,2) DEFAULT '0.00',
  `UnidadId` int(32) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `SuplidorId` (`SuplidorId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `movimientodevolucion_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucion_ibfk_2` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucion_ibfk_3` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucion_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodevolucion`
--

LOCK TABLES `movimientodevolucion` WRITE;
/*!40000 ALTER TABLE `movimientodevolucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientodevolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodevolucioncliente`
--

DROP TABLE IF EXISTS `movimientodevolucioncliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientodevolucioncliente` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `ClienteId` int(32) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(32) unsigned DEFAULT NULL,
  `MontoItbis` decimal(18,2) DEFAULT '0.00',
  `MontoTotal` decimal(18,2) DEFAULT '0.00',
  `UnidadId` int(32) DEFAULT NULL,
  `Defectuoso` bit(1) NOT NULL,
  `NoFactura` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `movimientodevolucioncliente_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucioncliente_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucioncliente_ibfk_3` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientodevolucioncliente_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodevolucioncliente`
--

LOCK TABLES `movimientodevolucioncliente` WRITE;
/*!40000 ALTER TABLE `movimientodevolucioncliente` DISABLE KEYS */;
INSERT INTO `movimientodevolucioncliente` VALUES (1,5,1.00,'2018-09-08 11:56:28','admin','0001-01-01 00:00:00',NULL,'',14,'DEV00001',20,0.00,850.00,23,'\0','FAC00032');
/*!40000 ALTER TABLE `movimientodevolucioncliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientoentrada`
--

DROP TABLE IF EXISTS `movimientoentrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientoentrada` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `SuplidorId` int(32) NOT NULL,
  `Referencia` varchar(200) NOT NULL,
  `AlmacenId` int(32) unsigned DEFAULT NULL,
  `MontoItbis` decimal(18,2) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `UnidadId` (`UnidadId`),
  KEY `AlmacenId` (`AlmacenId`),
  KEY `SuplidorId` (`SuplidorId`),
  KEY `ProductoId` (`ProductoId`),
  CONSTRAINT `movimientoentrada_ibfk_1` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoentrada_ibfk_2` FOREIGN KEY (`AlmacenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoentrada_ibfk_3` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `movimientoentrada_ibfk_4` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COMMENT='		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientoentrada`
--

LOCK TABLES `movimientoentrada` WRITE;
/*!40000 ALTER TABLE `movimientoentrada` DISABLE KEYS */;
INSERT INTO `movimientoentrada` VALUES (1,2,10.00,'2017-10-29 21:46:44','admin','0001-01-01 00:00:00',NULL,'',17,'100',20,324.00,2124.00,23),(2,3,10.00,'2017-10-29 21:46:44','admin','0001-01-01 00:00:00',NULL,'',17,'100',20,324.00,2124.00,23),(3,4,17.00,'2017-10-29 21:46:44','admin','0001-01-01 00:00:00',NULL,'',17,'100',20,612.00,4012.00,23),(4,5,22.00,'2017-11-01 20:00:57','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,1485.00,9735.00,23),(5,6,12.00,'2017-11-01 20:07:21','admin','0001-01-01 00:00:00',NULL,'',17,'3585772',20,810.00,5310.00,23),(6,9,12.00,'2017-11-01 20:34:50','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,410.40,2690.39,23),(7,1,33.00,'2017-11-01 20:34:51','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,3861.00,25311.00,23),(8,8,20.00,'2017-11-01 20:41:12','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,684.00,4484.00,23),(9,7,14.00,'2017-11-01 20:48:05','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,756.00,4956.00,23),(10,10,5.00,'2017-11-01 20:54:18','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,108.00,708.00,23),(11,2,40.00,'2017-11-01 21:05:26','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,1296.00,8496.00,23),(12,3,77.00,'2017-11-01 21:17:06','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,2494.80,16354.80,23),(13,12,18.00,'2017-11-04 16:08:07','admin','0001-01-01 00:00:00',NULL,'',17,'358572',20,648.00,4248.00,23),(14,14,30.00,'2018-03-30 18:36:32','admin','0001-01-01 00:00:00',NULL,'',17,'1659320',20,0.00,4500.00,23),(15,15,10.00,'2018-03-30 18:36:32','admin','0001-01-01 00:00:00',NULL,'',17,'1659320',20,0.00,1200.00,23),(16,21,35.00,'2018-04-18 09:09:14','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00004',20,4095.00,26845.00,23),(17,26,4.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,86.40,566.40,23),(18,22,3.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,81.00,531.00,23),(19,23,5.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,135.00,885.00,23),(20,24,1.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,27.00,177.00,23),(21,19,14.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,441.00,2891.00,23),(22,16,12.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,324.00,2124.00,23),(23,20,11.00,'2018-04-18 09:24:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00005',20,247.50,1622.50,23),(24,36,20.00,'2018-05-10 11:35:05','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00006',20,540.00,3540.00,23),(25,37,20.00,'2018-05-10 11:37:00','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00007',20,720.00,4720.00,23),(26,38,20.00,'2018-05-10 11:37:00','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00007',20,1080.00,7080.00,23),(27,25,14.00,'2018-05-10 11:42:01','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00008',20,756.00,4956.00,23),(28,39,11.00,'2018-05-10 12:21:54','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00009',20,445.50,2920.50,23),(29,40,8.00,'2018-05-10 12:21:54','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00009',20,180.00,1180.00,23),(30,41,23.00,'2018-05-10 15:42:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00010',20,455.40,2985.40,23),(31,7,20.00,'2018-06-20 14:15:22','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00012',20,1080.00,7080.00,23),(32,62,9.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,445.50,2920.50,23),(33,60,11.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,396.00,2596.00,23),(34,61,4.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,72.00,472.00,23),(35,59,18.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,972.00,6372.00,23),(36,58,7.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,100.80,660.80,23),(37,57,10.00,'2018-07-10 14:45:04','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00013',20,216.00,1416.00,23),(38,56,7.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,277.20,1817.20,23),(39,54,5.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,108.00,708.00,23),(40,53,6.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,237.60,1557.60,23),(41,55,7.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,100.80,660.80,23),(42,50,11.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,435.60,2855.60,23),(43,49,13.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,514.80,3374.80,23),(44,48,30.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,648.00,4248.00,23),(45,46,15.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,270.00,1770.00,23),(46,45,32.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,864.00,5664.00,23),(47,44,28.00,'2018-07-10 15:08:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,756.00,4956.00,23),(48,15,20.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,432.00,2832.00,23),(49,39,5.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,202.50,1327.50,23),(50,32,25.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,562.50,3687.50,23),(51,31,27.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,850.50,5575.50,23),(52,30,28.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,856.80,5616.80,23),(53,29,32.00,'2018-07-10 15:08:48','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00014',20,979.20,6419.20,23),(54,5,9.00,'2018-08-04 16:29:56','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00015',20,607.50,3982.50,23),(55,6,4.00,'2018-08-04 16:29:57','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00015',20,270.00,1770.00,23),(56,28,11.00,'2018-08-04 16:29:57','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00015',20,237.60,1557.60,23),(57,10,11.00,'2018-08-04 16:29:57','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00015',20,237.60,1557.60,23),(58,39,9.00,'2018-08-06 19:59:53','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00017',20,364.50,2389.50,23),(59,9,16.00,'2018-08-06 19:59:53','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00017',20,547.20,3587.20,23),(60,33,15.00,'2018-08-06 19:59:53','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00017',20,337.50,2212.50,23),(61,27,4.00,'2018-08-08 14:33:37','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00018',20,86.40,566.40,23),(62,47,33.00,'2018-08-08 14:33:37','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00018',20,475.20,3115.20,23),(63,64,14.00,'2018-08-08 14:46:06','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00019',20,189.00,1239.00,23),(64,63,8.00,'2018-08-08 14:46:06','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00019',20,180.00,1180.00,23),(65,6,5.00,'2018-08-18 14:12:46','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00020',20,337.50,2212.50,23),(66,35,25.00,'2018-08-18 14:12:46','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00020',20,450.00,2950.00,23),(67,1,2.00,'2018-08-18 14:12:47','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00020',20,234.00,1534.00,23),(68,67,15.00,'2018-08-18 14:28:32','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00021',20,675.00,4425.00,23),(69,68,12.00,'2018-08-18 16:03:03','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00022',20,432.00,2832.00,23),(70,69,29.00,'2018-08-18 16:03:03','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00022',20,783.00,5133.00,23),(71,11,20.00,'2018-08-18 16:05:35','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00023',20,540.00,3540.00,23),(72,66,6.00,'2018-08-18 16:17:27','admin','0001-01-01 00:00:00',NULL,'',18,'ENT00024',20,334.80,2194.80,23),(73,22,2.00,'2018-08-18 17:05:58','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00025',20,54.00,354.00,23),(74,70,3.00,'2018-08-18 17:05:58','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00025',20,59.40,389.40,23),(75,72,2.00,'2018-08-18 17:05:58','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00025',20,39.60,259.60,23),(76,71,3.00,'2018-08-18 17:05:58','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00025',20,59.40,389.40,23),(77,75,1.00,'2018-08-18 17:05:58','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00025',20,19.80,129.80,23),(78,68,17.00,'2018-08-21 20:02:55','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00026',20,612.00,4012.00,23),(79,26,5.00,'2018-08-21 20:02:55','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00026',20,108.00,708.00,23),(80,78,180.00,'2018-08-21 20:02:55','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00026',20,486.00,3186.00,23),(81,5,1.00,'2018-09-24 17:54:06','admin','0001-01-01 00:00:00',NULL,'',17,'ENT00027',20,67.50,442.50,23);
/*!40000 ALTER TABLE `movimientoentrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notacredito`
--

DROP TABLE IF EXISTS `notacredito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notacredito` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `NoFacturaOrigenId` varchar(50) NOT NULL,
  `NoFacturaAplicadaId` varchar(50) DEFAULT NULL,
  `Monto` decimal(18,2) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Aplicado` bit(1) NOT NULL,
  `Secuencia` varchar(200) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Secuencia_UNIQUE` (`Secuencia`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `FacturaOrigenId_UNIQUE` (`NoFacturaOrigenId`),
  UNIQUE KEY `FacturaAplicadaId_UNIQUE` (`NoFacturaAplicadaId`),
  KEY `NoFacturaOrigenId` (`NoFacturaOrigenId`),
  KEY `NCMonedaId` (`MonedaId`),
  KEY `NoFacturaAplicadaId` (`NoFacturaAplicadaId`),
  CONSTRAINT `notacredito_ibfk_1` FOREIGN KEY (`NoFacturaOrigenId`) REFERENCES `factura` (`NumeroFactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notacredito_ibfk_2` FOREIGN KEY (`NoFacturaAplicadaId`) REFERENCES `factura` (`NumeroFactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notacredito_ibfk_3` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notacredito`
--

LOCK TABLES `notacredito` WRITE;
/*!40000 ALTER TABLE `notacredito` DISABLE KEYS */;
INSERT INTO `notacredito` VALUES (1,'FAC00032',NULL,850.00,11,'2018-09-08 11:56:28','0001-01-01 00:00:00','admin',NULL,'','\0','DEV00001');
/*!40000 ALTER TABLE `notacredito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(32) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MonedaFacturaId` int(32) NOT NULL,
  `TipoPagoId` int(32) NOT NULL,
  `NoFactura` varchar(50) NOT NULL,
  `MontoPagado` decimal(18,2) NOT NULL,
  `TasaConversion` decimal(18,2) NOT NULL,
  `MontoPendiente` decimal(18,2) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Secuencia` varchar(100) NOT NULL,
  `MontoTotal` decimal(18,2) NOT NULL DEFAULT '0.00',
  `NoTalonario` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Pago_Moneda` (`MonedaId`),
  KEY `Pago_Cliente` (`ClienteId`),
  KEY `pago_factura` (`NoFactura`),
  KEY `Moneda_Factura` (`MonedaFacturaId`),
  KEY `Pago_TipoPago` (`TipoPagoId`),
  CONSTRAINT `Moneda_Factura` FOREIGN KEY (`MonedaFacturaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_Cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_Moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_MonedaFactura` FOREIGN KEY (`MonedaFacturaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_TipoPago` FOREIGN KEY (`TipoPagoId`) REFERENCES `tipopago` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pago_factura` FOREIGN KEY (`NoFactura`) REFERENCES `factura` (`NumeroFactura`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,3,11,11,3,'FAC00002',1600.00,1.00,420.00,'',NULL,'2017-12-02 17:28:21','admin',NULL,'0001-01-01 00:00:00','R00001',2020.00,NULL),(2,6,11,11,3,'FAC00004',20000.00,1.00,2000.00,'',NULL,'2018-03-30 18:51:26','admin',NULL,'0001-01-01 00:00:00','R00002',22000.00,NULL),(3,3,11,11,3,'FAC00002',420.00,1.00,0.00,'',NULL,'2018-03-30 19:01:24','admin',NULL,'0001-01-01 00:00:00','R00006',2020.00,NULL),(4,3,11,11,3,'FAC00009',3900.00,1.00,0.00,'',NULL,'2018-07-16 18:19:43','system',NULL,'0001-01-01 00:00:00','R00019',3900.00,NULL),(5,10,11,11,3,'FAC00012',3000.00,1.00,950.00,'',NULL,'2018-07-16 18:20:34','system',NULL,'0001-01-01 00:00:00','R00020',3950.00,NULL),(6,3,11,11,3,'FAC00013',650.00,1.00,0.00,'','PRIMER GARRAFON','2018-07-16 18:32:36','system',NULL,'0001-01-01 00:00:00','R00021',650.00,NULL),(7,6,11,11,3,'FAC00004',2000.00,1.00,0.00,'',NULL,'2018-07-16 18:56:27','system',NULL,'0001-01-01 00:00:00','R00022',22000.00,NULL),(8,6,11,11,3,'FAC00005',7000.00,1.00,8000.00,'',NULL,'2018-07-16 18:56:27','system',NULL,'0001-01-01 00:00:00','R00022',15000.00,NULL),(9,1,11,11,3,'FAC00001',4050.00,1.00,2950.00,'','','2018-07-16 19:02:17','system',NULL,'0001-01-01 00:00:00','R00023',7000.00,NULL),(10,9,11,11,3,'FAC00011',45800.00,1.00,0.00,'',NULL,'2018-07-16 19:07:11','system',NULL,'0001-01-01 00:00:00','R00024',45800.00,NULL),(11,11,11,11,3,'FAC00015',700.00,1.00,1299.00,'','ABONO','2018-07-16 19:45:19','system',NULL,'0001-01-01 00:00:00','R00025',1999.00,NULL),(12,12,11,11,3,'FAC00016',4200.00,1.00,0.00,'',NULL,'2018-07-16 20:06:50','system',NULL,'0001-01-01 00:00:00','R00026',4200.00,NULL),(13,3,11,11,3,'FAC00017',2000.00,1.00,500.00,'','ABONO BOLSO \n\n CONSIGNACION','2018-07-25 20:19:36','system',NULL,'0001-01-01 00:00:00','R00027',2500.00,NULL),(14,13,11,11,3,'FAC00018',1650.00,1.00,0.00,'',NULL,'2018-07-25 21:22:22','system',NULL,'0001-01-01 00:00:00','R00028',1650.00,NULL),(15,3,11,11,3,'FAC00014',650.00,1.00,0.00,'',NULL,'2018-07-30 19:57:59','system',NULL,'0001-01-01 00:00:00','R00029',650.00,NULL),(16,7,11,11,3,'FAC00007',1950.00,1.00,0.00,'','ABONO CTA','2018-07-30 20:00:52','system',NULL,'0001-01-01 00:00:00','R00030',1950.00,NULL),(17,7,11,11,3,'FAC00008',650.00,1.00,0.00,'','ABONO CTA','2018-07-30 20:00:53','system',NULL,'0001-01-01 00:00:00','R00030',650.00,NULL),(18,3,11,11,3,'FAC00017',500.00,1.00,0.00,'','saldo bolso  consignacion dorado','2018-08-04 12:32:13','system',NULL,'0001-01-01 00:00:00','R00031',2500.00,NULL),(19,14,11,11,3,'FAC00019',1300.00,1.00,0.00,'','lucy perrone','2018-08-04 16:20:41','system',NULL,'0001-01-01 00:00:00','R00032',1300.00,NULL),(20,14,11,11,3,'FAC00020',4000.00,1.00,0.00,'','lucy perrone','2018-08-04 16:20:41','system',NULL,'0001-01-01 00:00:00','R00032',4000.00,NULL),(21,3,11,11,3,'FAC00029',2000.00,1.00,500.00,'',NULL,'2018-08-26 20:32:07','system',NULL,'0001-01-01 00:00:00','R00033',2500.00,NULL),(22,1,11,11,3,'FAC00001',500.00,1.00,2450.00,'',NULL,'2018-08-28 19:44:26','system',NULL,'0001-01-01 00:00:00','R00034',7000.00,'2548'),(23,14,11,11,3,'FAC00033',850.00,1.00,0.00,'',NULL,'2018-09-24 17:56:05','admin',NULL,'0001-01-01 00:00:00','R00035',850.00,NULL),(24,14,11,11,3,'FAC00040',1500.00,1.00,0.00,'',NULL,'2018-11-15 15:17:29','a55cd251-d6bb-4039-a91b-83f24e40d644',NULL,'0001-01-01 00:00:00','R00036',1500.00,NULL),(25,14,11,11,3,'FAC00036',40.00,1.00,460.00,'',NULL,'2018-12-25 07:40:13','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',NULL,'0001-01-01 00:00:00','R00037',500.00,'2352'),(26,14,11,11,3,'FAC00041',500.00,1.00,0.00,'',NULL,'2018-12-25 07:40:13','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',NULL,'0001-01-01 00:00:00','R00037',500.00,'2352'),(27,14,11,11,3,'FAC00042',260.00,1.00,0.00,'',NULL,'2018-12-25 07:40:13','4b22f58d-e27f-469e-b783-c6d1f93dd7e6',NULL,'0001-01-01 00:00:00','R00037',260.00,'2352');
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagogasto`
--

DROP TABLE IF EXISTS `pagogasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagogasto` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `SuplidorId` int(32) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `MonedaGastoId` int(32) NOT NULL,
  `TipoPagoId` int(32) NOT NULL,
  `ReferenciaGasto` varchar(50) NOT NULL,
  `MontoPagado` decimal(18,6) NOT NULL,
  `TasaConversion` decimal(18,6) NOT NULL,
  `MontoPendiente` decimal(18,6) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(500) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Secuencia` varchar(100) NOT NULL,
  `MontoTotal` decimal(18,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `Pago_Moneda` (`MonedaId`),
  KEY `Pago_Suplidor` (`SuplidorId`),
  KEY `pago_GastoEmpresa` (`ReferenciaGasto`),
  KEY `Moneda_Gasto` (`MonedaGastoId`),
  KEY `Pago_TipoPago` (`TipoPagoId`),
  CONSTRAINT `Pago_MonedaGasto_Constrain` FOREIGN KEY (`MonedaGastoId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_Moneda_Constrain` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_Suplidor_Constrain` FOREIGN KEY (`SuplidorId`) REFERENCES `suplidores` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Pago_TipoPago_Constrain` FOREIGN KEY (`TipoPagoId`) REFERENCES `tipopago` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `pago_Gastoempresa_Constrain` FOREIGN KEY (`ReferenciaGasto`) REFERENCES `gastoempresa` (`ReferenciaGasto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagogasto`
--

LOCK TABLES `pagogasto` WRITE;
/*!40000 ALTER TABLE `pagogasto` DISABLE KEYS */;
INSERT INTO `pagogasto` VALUES (1,17,11,11,3,'PG00008',1000.000000,1.000000,1356.000000,'',NULL,'2018-09-24 17:16:30','admin',NULL,'0001-01-01 00:00:00','RG00013',2356.000000),(2,17,11,11,3,'PG00008',500.000000,1.000000,856.000000,'',NULL,'2018-09-24 17:19:14','admin',NULL,'0001-01-01 00:00:00','RG00014',2356.000000);
/*!40000 ALTER TABLE `pagogasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `Id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Descripcion` varchar(500) DEFAULT NULL,
  `Precio` decimal(18,2) NOT NULL,
  `Detalles` longtext,
  `ClaseDetalles` varchar(200) DEFAULT NULL,
  `FechaCreacion` datetime NOT NULL,
  `CreadoPor` varchar(50) NOT NULL,
  `Activo` bit(1) NOT NULL,
  `EsServicio` bit(1) NOT NULL,
  `CodigoBarra` varchar(500) DEFAULT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Costo` decimal(18,2) NOT NULL,
  `MonedaId` int(32) NOT NULL,
  `Existencia` decimal(18,2) NOT NULL,
  `EsCompuesto` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `MonedaId` (`MonedaId`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='Tabla donde se registran los productos.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'BOLSOS HIDRATANTE 5 PASOS','BOLSOS HIDRARANTE 5 PASOS',1500.00,'[{\"id\":0,\"producto\":null,\"productoId\":1,\"monedaId\":11,\"unidadId\":1,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":650,\"suplidorId\":17,\"$$hashKey\":\"object:883\"}]','','2017-10-29 21:22:42','admin','','\0','','admin','2018-04-17 13:28:49',650.00,11,0.00,'\0'),(2,'CAJA AMPOLLA ANTICAIDAD DE 6UNID.','6 UNIDADES DE 10 ML',400.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":180,\"suplidorId\":17,\"$$hashKey\":\"object:265\"}]','','2017-10-29 21:29:42','admin','','\0','','','0001-01-01 00:00:00',180.00,11,42.00,'\0'),(3,'CAJA AMPOLLA RESTRUCTURANTE 6 UNID','6 UNIDAD',400.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":180,\"suplidorId\":17,\"$$hashKey\":\"object:332\"}]','','2017-10-29 21:34:34','admin','','\0','','admin','2017-12-02 17:18:39',180.00,11,81.00,'\0'),(4,'MASCARILLA MAMEY 22 OZ.','MASCARILLA  MAMAY',650.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":200,\"suplidorId\":17,\"$$hashKey\":\"object:383\"}]','','2017-10-29 21:39:51','admin','','\0','','','0001-01-01 00:00:00',200.00,11,7.00,'\0'),(5,'GARRAFON DE SHAMPU','SHAMPU DE GARRAFON 320 ONZAS',850.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":375,\"suplidorId\":17,\"$$hashKey\":\"object:591\"}]','','2017-11-01 19:44:10','admin','','\0','','admin','2018-06-04 20:05:37',375.00,11,-2.00,'\0'),(6,'GARRAFON  CONDITION','GARRAFON CONDICION',950.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":375,\"suplidorId\":17,\"$$hashKey\":\"object:639\"}]','','2017-11-01 19:46:49','admin','','\0','','admin','2018-08-18 15:57:37',375.00,11,-3.00,'\0'),(7,'LEAVE IN HIDRATANTE 32 OZ.','LEAVE IN HIDRATANTE 32 0Z',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:705\"}]','','2017-11-01 19:50:22','admin','','\0','','admin','2018-08-08 14:16:34',300.00,11,16.00,'\0'),(8,'LUMINOSITA DE BRILLO 8 OZ','GOTA DE BRILLO 8 ONZ',750.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":190,\"suplidorId\":17,\"$$hashKey\":\"object:757\"}]','','2017-11-01 19:52:43','admin','','\0','','admin','2018-06-04 19:57:35',190.00,11,13.00,'\0'),(9,'GEL LACIADOR DE 8 ONZAS','GEL LACIADOR DE  8 ONZAS',650.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"costo\":190,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"suplidorId\":17,\"$$hashKey\":\"object:1176\"}]','','2017-11-01 20:18:18','admin','','\0','','','0001-01-01 00:00:00',190.00,11,9.00,'\0'),(10,'PEROSSIDO DE 30 VOLUMEN','PEROXIDO DE 30 VOLUMEN',260.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:1637\"}]','','2017-11-01 20:51:35','admin','','\0','','admin','2018-04-17 14:05:08',120.00,11,1.00,'\0'),(11,'AMPOLLA LACIADORA EN GEL 15 M L','AMPOLLA LACIADORAEN GEL 15 ML',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:1897\"}]','','2017-11-01 20:59:33','admin','','\0','','admin','2018-04-17 13:38:10',150.00,11,8.00,'\0'),(12,'CAJA AMPOLLA LACIADORA 15 ML','CAJA AMPOLLA LACIADORA 15 ML',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":200,\"suplidorId\":17,\"$$hashKey\":\"object:2412\"}]','','2017-11-01 21:25:35','admin','','\0','','','0001-01-01 00:00:00',200.00,11,0.00,'\0'),(13,'servicio','descripcion',400.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:402\"}]','','2018-02-04 18:59:04','admin','\0','\0','3459','','0001-01-01 00:00:00',300.00,11,0.00,'\0'),(14,'Gotero crescita caida crecimiento 4 oz','gotero 4 onzas',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:621\"}]','','2018-03-30 18:23:58','admin','','\0','','','0001-01-01 00:00:00',150.00,11,16.00,'\0'),(15,'BAGNO DI COLOR CLEAR 18 ONZ','Bagno Di Color',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:659\"}]','','2018-03-30 18:27:17','admin','','\0','','admin','2018-04-17 13:31:10',120.00,11,19.00,'\0'),(16,'EMERGENCIA DE 16 ONZAS','EMERGENCIA DE 16 ONZAS',450.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:460\"}]','','2018-04-17 13:06:13','admin','','\0','EM16','admin','2018-06-04 19:46:02',150.00,11,7.00,'\0'),(17,'EMERGENCIA 8 ONZAS','EMERGENCIA DE 8 ONZAS',300.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":130,\"suplidorId\":17,\"$$hashKey\":\"object:498\"}]',NULL,'2018-04-17 13:08:55','admin','','\0','EM08','admin','2018-06-04 19:44:05',130.00,11,0.00,'\0'),(18,'EMERGENCIA 32 ONZAS','EMERGENCIA DE 32 ONZAS',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":250,\"suplidorId\":17,\"$$hashKey\":\"object:642\"}]',NULL,'2018-04-17 13:16:40','admin','\0','\0','EM36','admin','2018-06-04 19:44:56',250.00,11,0.00,'\0'),(19,'JALEA CRESCITA DE 16 ONZAS','JALEA CRESCITA DE 16 OZ',450.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":175,\"suplidorId\":17,\"$$hashKey\":\"object:734\"}]','','2018-04-17 13:19:18','admin','','\0','JA16','admin','2018-06-04 19:29:07',175.00,11,10.00,'\0'),(20,'JALEA CRESCITA 8 ONZAS','JALEA CRESCITA 8 OZ.',275.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":125,\"suplidorId\":17,\"$$hashKey\":\"object:772\"}]','','2018-04-17 13:20:54','admin','','\0','JL08','admin','2018-06-04 19:28:19',125.00,11,8.00,'\0'),(21,'KIT NO LAY  6 UNIDADES','KIT NO LAY DE 6 UNIDADES',1850.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":650,\"suplidorId\":17,\"$$hashKey\":\"object:810\"}]','','2018-04-17 13:24:05','admin','','\0','','admin','2018-04-21 17:53:55',650.00,11,25.00,'\0'),(22,'BAGNO DI COLOR NEGRO 18 0Z.','BAGNO DI COLOR NEGRO 18 ONZAS',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:1391\"}]','','2018-04-17 13:41:24','admin','','\0','BA18 NEGRO','','0001-01-01 00:00:00',150.00,11,3.00,'\0'),(23,'BAGNO DI COLOR CHOCOLATE 18 OZ','BAGNO DI COLOR CHOCOLATE 18 00NZAS',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:1431\"}]','','2018-04-17 13:43:27','admin','','\0','BA 18 CHOCOLA','','0001-01-01 00:00:00',150.00,11,5.00,'\0'),(24,'BAGNO DI COLOR RUBIO COBRIZO  18 0Z','BAGNO DI COLOR RUBIO COBRIZO 18OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"costo\":150,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"suplidorId\":17,\"$$hashKey\":\"object:1453\"}]','','2018-04-17 13:45:38','admin','','\0','BANO 18 RUBIO COB','','0001-01-01 00:00:00',150.00,11,1.00,'\0'),(25,'CREMA ALISADORA 64 ONZAS','CREMA ALISADORA 64 ONZAS',850.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:1477\"}]',NULL,'2018-04-17 13:50:15','admin','','\0','CREALI64 OZ','admin','2018-06-04 19:24:45',300.00,11,14.00,'\0'),(26,'PEROSSIDO DI 40   VOLUMEN','PEROSSIDO DI 40  VOLUMEN',260.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:1573\"}]','','2018-04-17 14:00:13','admin','','\0','PERO 40 VO','admin','2018-04-17 14:08:34',120.00,11,7.00,'\0'),(27,'PEROSSIDO DI 10 VOLUMEN','PEROXIDO DI 10 VOLUMEN',260.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:1601\"}]','','2018-04-17 14:02:02','admin','','\0','PERO 10 VO','admin','2018-04-17 14:06:28',120.00,11,2.00,'\0'),(28,'PEROSSIDO DI 20 VOLUMEN','PEROSSIDO DE 20 VOLUMEN',260.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":\"\",\"id\":17,\"modificadoPor\":\"\",\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:2011\"}]',NULL,'2018-04-17 14:11:34','admin','','\0','PERO 20 VO',NULL,'0001-01-01 00:00:00',120.00,11,0.00,'\0'),(29,'SHAMPOO RESTRUCTURANTE 36 OZ','SHAMPOO RESTRUCTURANTE 36 OZ',650.00,'[{\"id\":0,\"producto\":null,\"productoId\":29,\"monedaId\":11,\"unidadId\":30,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"suplidorId\":17,\"costo\":170,\"$$hashKey\":\"object:1343\"}]',NULL,'2018-04-21 17:57:33','admin','','\0','','admin','2018-06-04 19:22:38',0.00,11,30.00,'\0'),(30,'CONDITION RESTRUCTURANTE 36 OZ','CONDITION RESTRUCTURANTE 36 OZ',650.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"suplidorId\":17,\"costo\":170,\"$$hashKey\":\"object:1272\"}]',NULL,'2018-04-21 17:58:37','admin','','\0','','admin','2018-06-04 19:20:58',0.00,11,27.00,'\0'),(31,'MASCARILLA  RESTRUC  22 OZ','MASCARILLA  RESTRUC.  22 OZ',700.00,'[{\"id\":0,\"producto\":null,\"productoId\":31,\"monedaId\":11,\"unidadId\":32,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"suplidorId\":17,\"costo\":175,\"$$hashKey\":\"object:1160\"}]',NULL,'2018-04-21 17:59:14','admin','','\0',NULL,'admin','2018-07-10 15:18:00',0.00,11,25.00,'\0'),(32,'LEAVE IN RESTRUCTURANTE 8 OZ','LEAVE IN  RESTRUCTURANTE  8 OZ',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":32,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":125,\"suplidorId\":17,\"$$hashKey\":\"object:534\"}]',NULL,'2018-04-21 18:00:36','admin','','\0','','admin','2018-06-04 18:54:57',0.00,11,23.00,'\0'),(33,'GOTA DE BRILLO DE  2.5 OZ','GOTA DE BRILLO DE  2.5 OZ',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":33,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":80,\"suplidorId\":17,\"$$hashKey\":\"object:2105\"}]',NULL,'2018-04-21 18:01:30','admin','','\0',NULL,'admin','2018-08-22 10:18:21',80.00,11,14.00,'\0'),(34,'BOLSO  5 P RESTRUTURANTE','BOLSO 5 P RESTRUCTURANT',3000.00,NULL,NULL,'2018-04-21 18:03:35','admin','','','','admin','2018-08-22 09:53:54',800.00,11,0.00,''),(35,'BOLSO VACIO','BOLSO VACIO',0.00,'[{\"id\":0,\"producto\":null,\"productoId\":35,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":100,\"suplidorId\":17,\"$$hashKey\":\"object:1226\"}]',NULL,'2018-04-21 18:05:54','admin','','\0','','admin','2018-08-17 11:46:07',100.00,11,24.00,'\0'),(36,'SHAMPOO NEUTRALIZANTE 1/2 GALON','SHAMPOO NEUTRALIZANTE 1/2 GALON',350.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:706\"}]',NULL,'2018-05-10 11:26:53','admin','','\0','','admin','2018-06-04 19:03:41',150.00,11,6.00,'\0'),(37,'TRATAMIENTO NEUTRALIZANTE DE 1/2 GALON','TRATAMIENTO NEUTRALIZANTE 1/2 GALON',400.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":200,\"suplidorId\":17,\"$$hashKey\":\"object:754\"}]',NULL,'2018-05-10 11:29:43','admin','','\0',NULL,'admin','2018-06-04 19:03:02',200.00,11,9.00,'\0'),(38,'EMERGENCIA SE 32 ONZAS','EMERGENCIA DE 32 ONZAS',800.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:809\"}]',NULL,'2018-05-10 11:31:13','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',300.00,11,10.00,'\0'),(39,'SPREY LACIADOR 8 ONZAS','SPREY LACIADOR',550.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":225,\"suplidorId\":17,\"$$hashKey\":\"object:354\"}]',NULL,'2018-05-10 12:17:35','admin','','\0','',NULL,'0001-01-01 00:00:00',225.00,11,10.00,'\0'),(40,'SPREY LACIADOR DE 4 OZ','SPREY LACIADOR DE 4 ONZAS',300.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":125,\"suplidorId\":17,\"$$hashKey\":\"object:384\"}]',NULL,'2018-05-10 12:20:10','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',125.00,11,3.00,'\0'),(41,'BAGNO COLOR CLEAR','BAGNO COLOR CLEAR',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":0,\"suplidor\":{\"direccion\":\"\",\"telefono\":\"8095659494\",\"cedulaRnc\":\"101000252\",\"creadoPor\":\"admin\",\"fechaModificacion\":\"0001-01-01T00:00:00\",\"fechaCreacion\":\"2017-10-29T21:15:29\",\"activo\":true,\"detalles\":null,\"modificadoPor\":null,\"nombre\":\"Agencias Internacionales\",\"id\":17},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:536\"}]',NULL,'2018-05-10 15:39:33','admin','','\0','',NULL,'0001-01-01 00:00:00',110.00,11,23.00,'\0'),(42,'MASCARILLA RESTRUC. DE 38 0Z','MASCARILLA RESTRUC. DE 38 0Z',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":350,\"suplidorId\":17,\"$$hashKey\":\"object:2265\"}]',NULL,'2018-06-04 20:23:55','admin','\0','','','admin','2018-07-10 15:18:53',350.00,11,0.00,'\0'),(43,'BOLSOS 5 P LACADOR','BOLSO 5 P LACIADOR',2500.00,NULL,NULL,'2018-06-04 20:26:05','admin','\0','','','admin','2018-07-10 15:12:16',0.00,11,0.00,'\0'),(44,'SHAMPOO LACIADOR 32 OZ','SHAMPOO LACIADOR 32 OZ',600.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:2506\"}]',NULL,'2018-06-04 20:29:58','admin','','\0','',NULL,'0001-01-01 00:00:00',0.00,11,28.00,'\0'),(45,'CONDICION LACEADOR DE 32 OZ','CONDICION LACEADOR DE 32 OZ',600.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:417\"}]',NULL,'2018-06-11 18:41:55','admin','','\0','',NULL,'0001-01-01 00:00:00',0.00,11,32.00,'\0'),(46,'TRATAMIENTO LACIADOR DE 16 OZ','TRATAMIENTO LACIADOR DE 16 OZ',400.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":100,\"suplidorId\":17,\"$$hashKey\":\"object:465\"}]',NULL,'2018-06-11 18:44:08','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,15.00,'\0'),(47,'LEAVE IN LACIADOR DE 8 OZ','LEAVE IN LACIADOR DE 8 OZ',450.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":80,\"suplidorId\":17,\"$$hashKey\":\"object:520\"}]',NULL,'2018-06-11 18:46:13','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,33.00,'\0'),(48,'TRATAMIENTO LACIADOR DE 22 OZ','TRATAMIENTO LACIADOR DE 22 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:568\"}]',NULL,'2018-06-11 18:48:37','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,30.00,'\0'),(49,'TRATAMIENTO  LACIADORA DE 38 0Z','TRATAMIENTO  LACIADORA DE38 0Z',800.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":220,\"suplidorId\":17,\"$$hashKey\":\"object:616\"}]',NULL,'2018-06-11 18:51:21','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,12.00,'\0'),(50,'MASCARILLA DE LECHE 38 OZ','MASCARILLA DE LECHE 38 OZ',700.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":220,\"suplidorId\":17,\"$$hashKey\":\"object:664\"}]',NULL,'2018-06-11 18:56:29','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,9.00,'\0'),(51,'MASCARILLA DE LECHE 22 OZ','MASCARILLA DE LECHE 22 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:712\"}]',NULL,'2018-06-11 19:00:49','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,0.00,'\0'),(52,'MASCARILLA DE LECHE 16 OZ','MASCARILLA DE LECHE 16 ONZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":80,\"suplidorId\":17,\"$$hashKey\":\"object:768\"}]',NULL,'2018-06-11 19:04:01','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,0.00,'\0'),(53,'MASCARILLA DE AJO 38 OZ','MASCARILLA DE AJO 38 OZ',700.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":220,\"suplidorId\":17,\"$$hashKey\":\"object:816\"}]',NULL,'2018-06-11 19:06:02','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,5.00,'\0'),(54,'MASCARILLA DE AJO 22 OZ','MASCARILLA DE AJO 22 OZ',450.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:864\"}]',NULL,'2018-06-11 19:07:30','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,5.00,'\0'),(55,'MASCARILLA DE AJO DE 16 OZ','MASCARILLA DE AJO 16 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":80,\"suplidorId\":17,\"$$hashKey\":\"object:912\"}]',NULL,'2018-06-11 19:09:01','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,7.00,'\0'),(56,'MASCARILLA DE COCO 38 OZ','MASCARILLA DE COCO 38 OZ',700.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":220,\"suplidorId\":17,\"$$hashKey\":\"object:960\"}]',NULL,'2018-06-11 19:10:39','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,6.00,'\0'),(57,'MASCARILLA DE COCO 22 OZ','MASCARILLA DE COCO 22 OZ',450.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":120,\"suplidorId\":17,\"$$hashKey\":\"object:1008\"}]',NULL,'2018-06-11 19:11:58','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,10.00,'\0'),(58,'MASCARILLA DE COCO16 OZ','MASCARILLA DE COCO 16 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":80,\"suplidorId\":17,\"$$hashKey\":\"object:1056\"}]',NULL,'2018-06-11 19:13:08','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,7.00,'\0'),(59,'MASCARILLA REGENERADORA 64 OZ','MACARILLA REGENERADORA 64 OZ',1000.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:1104\"}]',NULL,'2018-06-11 19:16:36','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,9.00,'\0'),(60,'ACEITE CAPILAR DE 8 OZ','ACEITE CAPILAR DE 8 OZ',600.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":200,\"suplidorId\":17,\"$$hashKey\":\"object:1152\"}]',NULL,'2018-06-11 19:20:51','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,4.00,'\0'),(61,'ACEITE CAPILAR DE 4 OZ','ACEITE CAPILAR DE 4 OZ',350.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":100,\"suplidorId\":17,\"$$hashKey\":\"object:1200\"}]',NULL,'2018-06-11 19:22:22','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',0.00,11,2.00,'\0'),(62,'POLVO DECOLORTE 16 OZ','POLVO DECOLORANTE 16 OZ',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":275,\"suplidorId\":17,\"$$hashKey\":\"object:478\"}]',NULL,'2018-06-11 19:28:41','admin','\0','\0','','admin','2018-08-08 14:42:08',0.00,11,4.00,'\0'),(63,'POLVO DECOLORANTE 8 OZ','POLVO DECOLORANTE 8 OZ',450.00,'[{\"id\":0,\"producto\":null,\"productoId\":63,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":125,\"suplidorId\":17,\"$$hashKey\":\"object:6697\"}]',NULL,'2018-06-11 19:30:17','admin','','\0',NULL,'admin','2018-08-08 14:38:43',200.00,11,1.00,'\0'),(64,'POLVO DECOLORANTE 4 OZ','POLVO DECOLORANTE 4 OZ',300.00,'[{\"id\":0,\"producto\":null,\"productoId\":64,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":75,\"suplidorId\":17,\"$$hashKey\":\"object:6759\"}]',NULL,'2018-06-11 19:31:22','admin','','\0',NULL,'admin','2018-08-08 14:39:22',100.00,11,4.00,'\0'),(65,'LEAVE IN LACIADOR 8 ONZAS','LEAVE IN LACIADOR DE 8 0Z',450.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":125,\"suplidorId\":17,\"$$hashKey\":\"object:819\"}]',NULL,'2018-08-08 14:11:01','admin','\0','\0','',NULL,'0001-01-01 00:00:00',125.00,11,0.00,'\0'),(66,'POLVO DECORORATE DE 16 0Z.','POLVO DECOLORANTE 16 OZ',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":310,\"suplidorId\":17,\"$$hashKey\":\"object:1440\"}]',NULL,'2018-08-08 15:30:12','admin','','\0','','admin','2018-08-08 15:41:45',310.00,11,5.00,'\0'),(67,'MACHERA RECONSTRUCTORA DE 38 OZ','MACHERA RECONSTRUCTOR DE 38 ONZAS',800.00,'[{\"id\":0,\"producto\":null,\"productoId\":67,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":300,\"suplidorId\":17,\"$$hashKey\":\"object:2777\"}]',NULL,'2018-08-18 14:15:44','admin','','\0','','admin','2018-08-18 14:35:22',300.00,11,14.00,'\0'),(68,'CAJA AMOLLA DE OLEO 6 UN','CAJA AMPOLLA DE OLEO 6UND',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":200,\"suplidorId\":17,\"$$hashKey\":\"object:5420\"}]',NULL,'2018-08-18 15:43:52','admin','','\0','',NULL,'0001-01-01 00:00:00',200.00,11,27.00,'\0'),(69,'SHAMPOO ANTIGRASO DE 32 OZ','SHAMPOO ANTIGRASO DE 32 OZ',600.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":150,\"suplidorId\":17,\"$$hashKey\":\"object:5447\"}]',NULL,'2018-08-18 15:47:01','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',150.00,11,27.00,'\0'),(70,'BAGNO DI COLOR ROJISMO 18 OZ','BAGNO COLOR ROJISIMO 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2035\"}]',NULL,'2018-08-18 16:47:34','admin','','\0','',NULL,'0001-01-01 00:00:00',110.00,11,3.00,'\0'),(71,'BAGNO DI COLORE MARRON 18 OZ','BAGNO DI COLORE MARRON',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2071\"}]',NULL,'2018-08-18 16:49:01','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,3.00,'\0'),(72,'BAGNO DI COLORE CAOBA 18 OZ','BAGNO DI COLORE CAOBA 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2110\"}]',NULL,'2018-08-18 16:50:38','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,2.00,'\0'),(73,'BAGNO DI COLORE MARRON CLARO 18 OZ','BAGNO DI COLOR MARRON CLARO 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2152\"}]',NULL,'2018-08-18 16:52:48','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,0.00,'\0'),(74,'BAGNO DI COLORE CHOCOLATE 18 OZ','BAGNO DI COLORES CHOCOLATE 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2197\"}]',NULL,'2018-08-18 16:55:27','admin','\0','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,0.00,'\0'),(75,'BAGNO DI COLORES COBRE 18 OZ','BAGNO DI COLORES COBRE 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2245\"}]',NULL,'2018-08-18 16:57:56','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,1.00,'\0'),(76,'BAGNO DI COLOR CASTANO 18 OZ','BAGNO DI COLORE CASTANO 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:2303\"}]',NULL,'2018-08-18 16:59:20','admin','','\0',NULL,NULL,'0001-01-01 00:00:00',110.00,11,0.00,'\0'),(77,'BAGNO DI COLORES BRONCE 18 OZ','BAGNO DI COLORE BRONCE 18 OZ',500.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":110,\"suplidorId\":17,\"$$hashKey\":\"object:3016\"}]',NULL,'2018-08-18 17:08:07','admin','','\0','',NULL,'0001-01-01 00:00:00',110.00,11,0.00,'\0'),(78,'OLEO AMPOLLA SUELTA 10 ML','OLEO AMPOLLA SUELTA DE 10 ML',50.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":15,\"suplidorId\":17,\"$$hashKey\":\"object:740\"}]',NULL,'2018-08-21 20:01:05','admin','','\0','',NULL,'0001-01-01 00:00:00',15.00,11,180.00,'\0'),(79,'BOLSO LACEADOR DE 5 PASOS','BOLSO LAACEADOR DE 5 PASOS',2600.00,NULL,NULL,'2018-08-22 10:27:30','admin','','','','admin','2018-08-22 10:31:47',750.00,11,0.00,''),(80,'Servicio','',500.00,NULL,NULL,'2018-09-04 00:17:32','admin','','','',NULL,'0001-01-01 00:00:00',100.00,11,0.00,'\0'),(81,'Power Spring','',150.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":50,\"suplidorId\":17,\"$$hashKey\":\"object:1338\"}]',NULL,'2018-10-01 20:42:57','admin','','\0','ElVerdaderoProducto','admin','2018-11-09 12:24:53',50.00,11,0.00,'\0'),(82,'La Pela','Mata esa jeva sin importar la edad',200.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":100,\"suplidorId\":17,\"$$hashKey\":\"object:694\"}]',NULL,'2018-11-09 12:25:45','a55cd251-d6bb-4039-a91b-83f24e40d644','','\0','',NULL,'0001-01-01 00:00:00',100.00,11,0.00,'\0'),(83,'aaa','',60.00,'[{\"id\":0,\"producto\":null,\"productoId\":0,\"monedaId\":11,\"unidadId\":23,\"suplidor\":{\"id\":17,\"nombre\":\"Agencias Internacionales\"},\"costo\":40,\"suplidorId\":17,\"$$hashKey\":\"object:484\"}]',NULL,'2018-12-22 01:27:32','4b22f58d-e27f-469e-b783-c6d1f93dd7e6','','\0','',NULL,'0001-01-01 00:00:00',40.00,11,0.00,'\0'),(84,'servicio AAA','',100.00,NULL,NULL,'2018-12-22 01:27:57','4b22f58d-e27f-469e-b783-c6d1f93dd7e6','','','',NULL,'0001-01-01 00:00:00',50.00,11,0.00,'\0');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productounidadesequivalencia`
--

DROP TABLE IF EXISTS `productounidadesequivalencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productounidadesequivalencia` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  `Equivalencia` decimal(18,4) NOT NULL,
  `EsPrincipal` bit(1) NOT NULL,
  `Orden` int(32) NOT NULL,
  `PrecioCompra` decimal(18,2) NOT NULL,
  `PrecioVenta` decimal(18,2) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `productounidadesequivalencia_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `productounidadesequivalencia_ibfk_2` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productounidadesequivalencia`
--

LOCK TABLES `productounidadesequivalencia` WRITE;
/*!40000 ALTER TABLE `productounidadesequivalencia` DISABLE KEYS */;
INSERT INTO `productounidadesequivalencia` VALUES (1,1,'admin','2017-10-29 21:22:42','admin','2018-04-17 13:28:49','',23,1.0000,'',0,650.00,1500.00),(2,2,'admin','2017-10-29 21:29:42','','0001-01-01 00:00:00','',23,1.0000,'',0,180.00,400.00),(3,3,'admin','2017-10-29 21:34:34','admin','2017-12-02 17:18:39','',23,1.0000,'',0,180.00,400.00),(4,4,'admin','2017-10-29 21:39:51','','0001-01-01 00:00:00','',23,1.0000,'',0,200.00,650.00),(5,5,'admin','2017-11-01 19:44:10','admin','2018-04-17 13:35:21','\0',23,1.0000,'',0,375.00,900.00),(6,6,'admin','2017-11-01 19:46:49','','0001-01-01 00:00:00','\0',23,1.0000,'',0,375.00,1100.00),(7,7,'admin','2017-11-01 19:50:22','admin','2018-08-08 14:16:34','',23,1.0000,'',0,300.00,800.00),(8,8,'admin','2017-11-01 19:52:43','','0001-01-01 00:00:00','\0',23,1.0000,'',0,190.00,695.00),(9,9,'admin','2017-11-01 20:18:18','','0001-01-01 00:00:00','',23,1.0000,'',0,190.00,650.00),(10,10,'admin','2017-11-01 20:51:35','admin','2018-04-17 14:05:08','',23,1.0000,'',0,120.00,260.00),(11,11,'admin','2017-11-01 20:59:33','admin','2018-04-17 13:38:10','',23,1.0000,'',0,150.00,500.00),(12,12,'admin','2017-11-01 21:25:35','','0001-01-01 00:00:00','',23,1.0000,'',0,200.00,500.00),(13,13,'admin','2018-02-04 18:59:05',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,300.00,400.00),(14,14,'admin','2018-03-30 18:23:58','','0001-01-01 00:00:00','',23,1.0000,'',0,150.00,500.00),(15,15,'admin','2018-03-30 18:27:17','admin','2018-04-17 13:31:10','',23,1.0000,'',0,120.00,500.00),(16,16,'admin','2018-04-17 13:06:13','admin','2018-06-04 19:46:02','',23,1.0000,'',0,150.00,450.00),(17,17,'admin','2018-04-17 13:08:55',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,130.00,400.00),(18,18,'admin','2018-04-17 13:16:40','admin','2018-06-04 19:44:56','',23,1.0000,'',0,250.00,800.00),(19,19,'admin','2018-04-17 13:19:18','admin','2018-04-17 13:32:03','\0',23,1.0000,'',0,175.00,500.00),(20,20,'admin','2018-04-17 13:20:55','','0001-01-01 00:00:00','\0',23,1.0000,'',0,125.00,300.00),(21,21,'admin','2018-04-17 13:24:05','admin','2018-04-18 09:12:52','\0',23,1.0000,'',0,650.00,1850.00),(22,22,'admin','2018-04-17 13:41:24','','0001-01-01 00:00:00','',23,1.0000,'',0,150.00,500.00),(23,23,'admin','2018-04-17 13:43:27','','0001-01-01 00:00:00','',23,1.0000,'',0,150.00,500.00),(24,24,'admin','2018-04-17 13:45:38','','0001-01-01 00:00:00','',23,1.0000,'',0,150.00,500.00),(25,25,'admin','2018-04-17 13:50:15','','0001-01-01 00:00:00','\0',23,1.0000,'',0,300.00,750.00),(26,26,'admin','2018-04-17 14:00:13','admin','2018-04-17 14:08:34','',23,1.0000,'',0,120.00,260.00),(27,27,'admin','2018-04-17 14:02:02','admin','2018-04-17 14:06:28','',23,1.0000,'',0,120.00,260.00),(28,28,'admin','2018-04-17 14:11:34','','0001-01-01 00:00:00','',23,1.0000,'',0,120.00,260.00),(29,21,'admin','2018-04-17 13:24:05','admin','2018-04-21 17:53:55','',23,1.0000,'',0,650.00,1850.00),(30,29,'admin','2018-04-21 17:57:33','admin','2018-04-21 17:57:55','\0',23,1.0000,'',0,0.00,0.00),(31,30,'admin','2018-04-21 17:58:37',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,0.00),(32,31,'admin','2018-04-21 17:59:14','admin','2018-04-21 17:59:27','\0',23,1.0000,'',0,0.00,0.00),(33,32,'admin','2018-04-21 18:00:36','admin','2018-06-04 18:54:57','',23,1.0000,'',0,0.00,500.00),(34,33,'admin','2018-04-21 18:01:30',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,0.00),(35,35,'admin','2018-04-21 18:05:54',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,0.00),(36,36,'admin','2018-05-10 11:26:53','','0001-01-01 00:00:00','\0',23,1.0000,'',0,150.00,400.00),(37,37,'admin','2018-05-10 11:29:43','','0001-01-01 00:00:00','\0',23,1.0000,'',0,200.00,500.00),(38,38,'admin','2018-05-10 11:31:13','','0001-01-01 00:00:00','',23,1.0000,'',0,300.00,800.00),(39,39,'admin','2018-05-10 12:17:35','','0001-01-01 00:00:00','',23,1.0000,'',0,225.00,550.00),(40,40,'admin','2018-05-10 12:20:10','','0001-01-01 00:00:00','',23,1.0000,'',0,125.00,300.00),(41,41,'admin','2018-05-10 15:39:33','','0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(42,33,'admin','2018-04-21 18:01:30',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,500.00),(43,37,'admin','2018-05-10 11:29:43','admin','2018-06-04 19:03:02','',23,1.0000,'',0,200.00,400.00),(44,36,'admin','2018-05-10 11:26:53','admin','2018-06-04 19:03:41','',23,1.0000,'',0,150.00,350.00),(45,33,'admin','2018-04-21 18:01:30','admin','2018-08-22 10:18:21','',23,1.0000,'',0,80.00,500.00),(46,31,'admin','2018-04-21 17:59:14','admin','2018-07-10 15:18:00','',23,1.0000,'',0,0.00,700.00),(47,30,'admin','2018-04-21 17:58:37','admin','2018-06-04 19:20:58','',23,1.0000,'',0,0.00,650.00),(48,29,'admin','2018-04-21 17:57:33','admin','2018-06-04 19:22:38','',23,1.0000,'',0,0.00,650.00),(49,25,'admin','2018-04-17 13:50:15',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,300.00,850.00),(50,20,'admin','2018-04-17 13:20:54','admin','2018-06-04 19:28:19','',23,1.0000,'',0,125.00,275.00),(51,19,'admin','2018-04-17 13:19:18',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,175.00,450.00),(52,19,'admin','2018-04-17 13:19:18','admin','2018-06-04 19:29:07','',23,1.0000,'',0,175.00,450.00),(53,17,'admin','2018-04-17 13:08:55',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,130.00,300.00),(54,8,'admin','2017-11-01 19:52:43','admin','2018-06-04 19:57:35','',23,1.0000,'',0,190.00,750.00),(55,6,'admin','2017-11-01 19:46:49','admin','2018-08-18 15:57:37','',23,1.0000,'',0,375.00,950.00),(56,5,'admin','2017-11-01 19:44:10','admin','2018-06-04 20:05:37','',23,1.0000,'',0,375.00,850.00),(57,42,'admin','2018-06-04 20:23:55',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,800.00),(58,44,'admin','2018-06-04 20:29:58','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,600.00),(59,45,'admin','2018-06-11 18:41:55','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,600.00),(60,46,'admin','2018-06-11 18:44:08','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,400.00),(61,47,'admin','2018-06-11 18:46:13','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,450.00),(62,48,'admin','2018-06-11 18:48:37','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,500.00),(63,49,'admin','2018-06-11 18:51:22','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,800.00),(64,50,'admin','2018-06-11 18:56:30','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,700.00),(65,51,'admin','2018-06-11 19:00:50',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,0.00,500.00),(66,52,'admin','2018-06-11 19:04:01',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,0.00,500.00),(67,53,'admin','2018-06-11 19:06:02','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,700.00),(68,54,'admin','2018-06-11 19:07:30','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,450.00),(69,55,'admin','2018-06-11 19:09:01','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,500.00),(70,56,'admin','2018-06-11 19:10:39','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,700.00),(71,57,'admin','2018-06-11 19:11:58','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,450.00),(72,58,'admin','2018-06-11 19:13:08','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,500.00),(73,59,'admin','2018-06-11 19:16:37','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,1000.00),(74,60,'admin','2018-06-11 19:20:52','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,600.00),(75,61,'admin','2018-06-11 19:22:22','','0001-01-01 00:00:00','',23,1.0000,'',0,0.00,350.00),(76,62,'admin','2018-06-11 19:28:41','','0001-01-01 00:00:00','\0',23,1.0000,'',0,0.00,800.00),(77,63,'admin','2018-06-11 19:30:18','admin','2018-08-08 14:38:43','',23,1.0000,'',0,200.00,450.00),(78,64,'admin','2018-06-11 19:31:22','admin','2018-08-08 14:39:22','',23,1.0000,'',0,100.00,300.00),(79,65,'admin','2018-08-08 14:11:01',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,125.00,450.00),(80,62,'admin','2018-06-11 19:28:41',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,0.00,800.00),(81,66,'admin','2018-08-08 15:30:12','admin','2018-08-08 15:41:45','',23,1.0000,'',0,310.00,800.00),(82,35,'admin','2018-04-21 18:05:54',NULL,'0001-01-01 00:00:00','\0',23,1.0000,'',0,100.00,0.00),(83,35,'admin','2018-04-21 18:05:54','admin','2018-08-17 11:46:07','',23,1.0000,'',0,100.00,0.00),(84,67,'admin','2018-08-18 14:15:44','admin','2018-08-18 14:35:22','',23,1.0000,'',0,300.00,800.00),(85,68,'admin','2018-08-18 15:43:52','','0001-01-01 00:00:00','',23,1.0000,'',0,200.00,500.00),(86,69,'admin','2018-08-18 15:47:01','','0001-01-01 00:00:00','',23,1.0000,'',0,150.00,600.00),(87,70,'admin','2018-08-18 16:47:34','','0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(88,71,'admin','2018-08-18 16:49:01','','0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(89,72,'admin','2018-08-18 16:50:38','','0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(90,73,'admin','2018-08-18 16:52:48',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(91,74,'admin','2018-08-18 16:55:27',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(92,75,'admin','2018-08-18 16:57:56','','0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(93,76,'admin','2018-08-18 16:59:20',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(94,77,'admin','2018-08-18 17:08:07',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,110.00,500.00),(95,78,'admin','2018-08-21 20:01:05','','0001-01-01 00:00:00','',23,1.0000,'',0,15.00,50.00),(96,81,'admin','2018-10-01 20:42:57','admin','2018-11-09 12:24:53','',23,1.0000,'',0,50.00,150.00),(97,82,'a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-09 12:25:45',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,100.00,200.00),(98,83,'4b22f58d-e27f-469e-b783-c6d1f93dd7e6','2018-12-22 01:27:32',NULL,'0001-01-01 00:00:00','',23,1.0000,'',0,40.00,60.00);
/*!40000 ALTER TABLE `productounidadesequivalencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolestree`
--

DROP TABLE IF EXISTS `rolestree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolestree` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ParentRoleId` int(32) NOT NULL,
  `ChildRoleId` int(32) NOT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolestree`
--

LOCK TABLES `rolestree` WRITE;
/*!40000 ALTER TABLE `rolestree` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolestree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salidaproductos`
--

DROP TABLE IF EXISTS `salidaproductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salidaproductos` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `ClienteId` int(32) NOT NULL,
  `Fecha` datetime NOT NULL,
  `UnidadId` int(32) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `salidaproductos_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `salidaproductos_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `salidaproductos_ibfk_3` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salidaproductos`
--

LOCK TABLES `salidaproductos` WRITE;
/*!40000 ALTER TABLE `salidaproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `salidaproductos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suplidores`
--

DROP TABLE IF EXISTS `suplidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suplidores` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) NOT NULL,
  `Direccion` varchar(500) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Detalles` varchar(2000) DEFAULT NULL,
  `CedulaRnc` varchar(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suplidores`
--

LOCK TABLES `suplidores` WRITE;
/*!40000 ALTER TABLE `suplidores` DISABLE KEYS */;
INSERT INTO `suplidores` VALUES (16,'SUPLIDOR','La Paz no.58 el radiante','8095697774','admin','2017-10-29 20:39:21','admin','2017-10-29 21:15:46','\0','','000000000'),(17,'Agencias Internacionales','','8095659494','admin','2017-10-29 21:15:29',NULL,'0001-01-01 00:00:00','',NULL,'101000252'),(18,'TOUJOURS NOUVELLE SILKY','ZONA INDUSTRIAL DE HERRERA','8095402459','admin','2018-08-18 12:46:36',NULL,'0001-01-01 00:00:00','',NULL,'8095400008'),(19,'Cuadre de inventario','','0000','admin','2018-08-26 15:48:29',NULL,'0001-01-01 00:00:00','',NULL,'0000'),(20,'Los Ladrones','','0000000000','a55cd251-d6bb-4039-a91b-83f24e40d644','2018-11-09 12:32:19',NULL,'0001-01-01 00:00:00','',NULL,'00000000000');
/*!40000 ALTER TABLE `suplidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarifariocliente`
--

DROP TABLE IF EXISTS `tarifariocliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarifariocliente` (
  `Id` int(32) NOT NULL,
  `ProductoId` int(32) unsigned NOT NULL,
  `ClienteId` int(32) NOT NULL,
  `Tarifa` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ProductoId` (`ProductoId`),
  KEY `ClienteId` (`ClienteId`),
  CONSTRAINT `tarifariocliente_ibfk_1` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tarifariocliente_ibfk_2` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarifariocliente`
--

LOCK TABLES `tarifariocliente` WRITE;
/*!40000 ALTER TABLE `tarifariocliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarifariocliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipomovimiento`
--

DROP TABLE IF EXISTS `tipomovimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipomovimiento` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `FechaCreachion` datetime NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) DEFAULT NULL,
  `Codigo` varchar(5) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Codigo_UNIQUE` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipomovimiento`
--

LOCK TABLES `tipomovimiento` WRITE;
/*!40000 ALTER TABLE `tipomovimiento` DISABLE KEYS */;
INSERT INTO `tipomovimiento` VALUES (5,'Entrada','2018-04-14 00:00:00','noel',NULL,NULL,'','IN'),(6,'Salida','2018-04-14 00:00:00','noel',NULL,NULL,'','OUT');
/*!40000 ALTER TABLE `tipomovimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopago`
--

DROP TABLE IF EXISTS `tipopago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopago` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `Codigo` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Codigo_UNIQUE` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopago`
--

LOCK TABLES `tipopago` WRITE;
/*!40000 ALTER TABLE `tipopago` DISABLE KEYS */;
INSERT INTO `tipopago` VALUES (3,'EFECTIVO','NOEL','2017-01-01 00:00:00',NULL,NULL,'','EFE'),(4,'CHEQUE','NOEL','2017-01-01 00:00:00',NULL,NULL,'','CHK'),(5,'TARJETA DE CREDITO','NOEL','2017-01-01 00:00:00',NULL,NULL,'','TC');
/*!40000 ALTER TABLE `tipopago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transferenciaalmacen`
--

DROP TABLE IF EXISTS `transferenciaalmacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transferenciaalmacen` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `AlmacenOrigenId` int(32) unsigned NOT NULL,
  `AlmacenDestinoId` int(32) unsigned NOT NULL,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,2) NOT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  `Referencia` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `AlmacenOrigenId` (`AlmacenOrigenId`),
  KEY `AlmacenDestinoId` (`AlmacenDestinoId`),
  KEY `ProductoId` (`ProductoId`),
  KEY `UnidadId` (`UnidadId`),
  CONSTRAINT `transferenciaalmacen_ibfk_1` FOREIGN KEY (`AlmacenOrigenId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transferenciaalmacen_ibfk_2` FOREIGN KEY (`AlmacenDestinoId`) REFERENCES `almacen` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transferenciaalmacen_ibfk_3` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transferenciaalmacen_ibfk_4` FOREIGN KEY (`UnidadId`) REFERENCES `unidades` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transferenciaalmacen`
--

LOCK TABLES `transferenciaalmacen` WRITE;
/*!40000 ALTER TABLE `transferenciaalmacen` DISABLE KEYS */;
INSERT INTO `transferenciaalmacen` VALUES (1,20,23,1,2.00,'admin','2018-05-29 20:07:06',NULL,'0001-01-01 00:00:00','',23,'TRF00001'),(2,20,23,29,2.00,'admin','2018-08-06 20:23:55',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(3,20,23,30,2.00,'admin','2018-08-06 20:23:55',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(4,20,23,31,2.00,'admin','2018-08-06 20:23:55',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(5,20,23,45,2.00,'admin','2018-08-06 20:23:55',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(6,20,23,44,2.00,'admin','2018-08-06 20:23:55',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(7,20,23,46,2.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(8,20,23,33,2.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(9,20,23,7,2.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(10,20,23,53,1.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(11,20,23,50,1.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(12,20,23,56,1.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(13,20,23,49,1.00,'admin','2018-08-06 20:23:56',NULL,'0001-01-01 00:00:00','',23,'TRF00002'),(14,20,22,53,1.00,'admin','2018-08-22 09:51:57',NULL,'0001-01-01 00:00:00','',23,'TRF00003'),(15,20,22,56,1.00,'admin','2018-08-22 09:51:57',NULL,'0001-01-01 00:00:00','',23,'TRF00003'),(16,20,22,50,1.00,'admin','2018-08-22 09:51:57',NULL,'0001-01-01 00:00:00','',23,'TRF00003'),(17,20,22,49,1.00,'admin','2018-08-22 09:51:57',NULL,'0001-01-01 00:00:00','',23,'TRF00003'),(18,20,22,67,1.00,'admin','2018-08-22 09:51:57',NULL,'0001-01-01 00:00:00','',23,'TRF00003'),(19,20,22,44,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(20,20,22,45,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(21,20,22,46,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(22,20,22,47,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(23,20,22,33,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(24,20,22,35,1.00,'admin','2018-08-22 11:08:55',NULL,'0001-01-01 00:00:00','',23,'TRF00004'),(25,20,22,29,1.00,'admin','2018-08-22 11:13:11',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(26,20,22,30,1.00,'admin','2018-08-22 11:13:12',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(27,20,22,31,1.00,'admin','2018-08-22 11:13:12',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(28,20,22,32,1.00,'admin','2018-08-22 11:13:12',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(29,20,22,33,1.00,'admin','2018-08-22 11:13:12',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(30,20,22,35,1.00,'admin','2018-08-22 11:13:12',NULL,'0001-01-01 00:00:00','',23,'TRF00005'),(31,20,22,9,1.00,'admin','2018-08-22 11:54:45',NULL,'0001-01-01 00:00:00','',23,'TRF00006'),(32,20,22,29,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007'),(33,20,22,30,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007'),(34,20,22,31,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007'),(35,20,22,32,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007'),(36,20,22,33,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007'),(37,20,22,35,1.00,'admin','2018-08-22 12:07:26',NULL,'0001-01-01 00:00:00','',23,'TRF00007');
/*!40000 ALTER TABLE `transferenciaalmacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `Id` int(32) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (23,'UND','admin','2017-10-29 20:36:23','admin','2018-05-20 21:37:43',''),(24,'CAJA','admin','2017-10-29 20:36:29',NULL,'0001-01-01 00:00:00',''),(25,'GALON','admin','2017-10-29 20:36:36',NULL,'0001-01-01 00:00:00',''),(26,'Kg','admin','2017-10-29 20:36:50',NULL,'0001-01-01 00:00:00','');
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'contabilidad'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `actualizarMontosMovimientos` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `actualizarMontosMovimientos` ON SCHEDULE EVERY 5 MINUTE STARTS '2018-05-28 20:21:02' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
    CALL `contabilidad`.`ObtenerAcumulativoMovimientosAlmacen`();
    end */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `prueba` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `prueba` ON SCHEDULE EVERY 1 HOUR STARTS '2018-04-15 11:26:18' ON COMPLETION NOT PRESERVE ENABLE DO call ObtenerAcumulativoMovimientosAlmacen() */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'contabilidad'
--
/*!50003 DROP PROCEDURE IF EXISTS `ObtenerAcumulativoMovimientosAlmacen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ObtenerAcumulativoMovimientosAlmacen`()
begin

declare v_max int unsigned default 0;
declare v_counter int unsigned default 0;

set v_max=(select  (id+1) as Id from movimientoalmacen where id>0 order by id desc limit 0,1);
set v_counter=0;


  
  start transaction;
   CREATE TABLE `movimientoalmacentemp` (
  `Id` int(32) NOT NULL,
  `AlmacenId` int(32) unsigned NOT NULL,
  `ProductoId` int(32) unsigned NOT NULL,
  `Cantidad` decimal(18,6) NOT NULL,
  `FechaCreacion` datetime NOT NULL,
  `FechaModificacion` datetime DEFAULT NULL,
  `CreadoPor` varchar(45) NOT NULL,
  `ModificadoPor` varchar(45) DEFAULT NULL,
  `Activo` bit(1) NOT NULL,
  `UnidadId` int(32) NOT NULL,
  `LocalidadId` int(32) NOT NULL,
  `TipoMovimiento` varchar(5) NOT NULL,
  `Referencia` varchar(50) NOT NULL DEFAULT '',
  `BalanceActual` decimal(18,6) NOT NULL DEFAULT '0.000000');
  
  INSERT INTO `movimientoalmacentemp`
(`Id`,
`AlmacenId`,
`ProductoId`,
`Cantidad`,
`FechaCreacion`,
`FechaModificacion`,
`CreadoPor`,
`ModificadoPor`,
`Activo`,
`UnidadId`,
`LocalidadId`,
`TipoMovimiento`,
`Referencia`,
`BalanceActual`)
(select * from movimientoalmacen where id>0);

  while v_counter < v_max do
  
update movimientoalmacen as m2 set BalanceActual=(select ifnull((select sum(m.cantidad) from movimientoalmacentemp as m where m.ProductoId=m2.ProductoId and m.AlmacenId=m2.AlmacenId and m.UnidadId=m2.UnidadId
 and m.LocalidadId=m2.LocalidadId and m.id<=v_counter),0))
where 	m2.Id=v_counter;

    set v_counter=v_counter+1;
  end while;
  
  drop table movimientoalmacentemp;
  commit;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-25 12:56:38
