﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess
{
   public class BuquedaCompleja
    {
        public List<FiltroBusqueda> Filtros { get; set; }
        public bool Activo { get; set; }
        public int? Page { get; set; }
        public int? Size { get; set; }
    }

    public class BusquedaReporteRecibos
    {
        public int? vendedorId { get; set; }
        public int? clienteId { get; set; }
        public int? monedaId { get; set; }
        public int TipoReporte { get; set; }
        public DateTime fechaInicial { get; set; } = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        public DateTime fechaFinal { get; set; } = DateTime.Now;
        public string NoFactura { get; set; } = null;
    }

    public class BusquedaReporteRecibosExcel
    {
        public int? vendedorId { get; set; }
        public int? clienteId { get; set; }
        public int? monedaId { get; set; }
        public int TipoReporte { get; set; }
        public string fechaInicial { get; set; }
        public string fechaFinal { get; set; }
        public string NoFactura { get; set; } = null;
    }

    public enum EstadosFacturas
    {
        F, //FACTURADO
        D, //COTIZADO
        A, //ANULADO
        C, //CONVERTIDO
        P //PAGADO
    }

    public class FiltroBusqueda
    {
        public int TipoCampo { get; set; }
        public string Valor { get; set; }
        public string Campo { get; set; }
    }
    public enum TipoFiltro
    {
        Like = 0,
        Equal = 1,
        Date = 2,
        EqualString = 3,
        GreaterOrEqual = 4,
        LessOrEqual = 5,
        Greater = 6,
        Less = 7,
        IsNUll = 8,
        GreaterOrEqualDate=9,
        LessOrEqualDate=10,
        NotEquealString=11,
        IN=12,
        NotIn=13
    }
}

