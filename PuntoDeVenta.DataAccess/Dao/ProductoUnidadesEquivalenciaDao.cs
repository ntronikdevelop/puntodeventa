﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class ProductoUnidadesEquivalenciaDao : BaseDb<ProductoUnidadesEquivalencia>
    {
        public List<ProductoUnidadesEquivalencia> ObtenerUnidadesDeProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<ProductoUnidadesEquivalencia>().Expression.ToString()} where t0.Activo=1  and  t0.ProductoId={productoId}  ";
                var resultado = dbContext.Consulta<ProductoUnidadesEquivalencia>(queryInicial);
                return resultado;
            }
        }
    }
}
