﻿using System;
using System.Linq;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class TransferenciaAlmacenDao : BaseDb<TransferenciaAlmacen>
    {
        public override PagedResult<TransferenciaAlmacen> ObtenerListaPaginado(int page, int size, bool activos)
        {
            using (var dbContext = InicializarDb()) 
            {

                string query = $"select t.Id, t.AlmacenOrigenId,t.CreadoPor,t.FechaCreacion, t.AlmacenDestinoId, t.Cantidad, t.Activo, t.UnidadId, t.Referencia, a.Nombre as AlmacenOrigenNombre," +
                    $" a2.Nombre as AlmacenDestinoNombre, p.Nombre as ProductoNombre, u.Nombre as UnidadNombre from transferenciaalmacen t left join producto p on t.ProductoId=p.id " +
                    $" left join almacen a on t.AlmacenOrigenId= a.Id left join almacen a2 on t.AlmacenDestinoId= a2.Id " +
                    $" left join unidades u on t.UnidadId=u.id where t.Activo={Convert.ToInt16(activos)}   limit {((page - 1) * size)},{size};";
                var resultado = dbContext.Consulta<TransferenciaAlmacenCustom>(query).Select(x => new TransferenciaAlmacen()
                {
                    Id = x.Id,
                    CreadoPor = x.CreadoPor,
                    Activo = x.Activo,
                    AlmacenDestinoId = x.AlmacenDestinoId,
                    FechaCreacion = x.FechaCreacion,
                    AlmacenOrigenId = x.AlmacenOrigenId,
                    Cantidad = x.Cantidad,
                    ProductoId = x.ProductoId,
                    Referencia = x.Referencia,
                    UnidadId = x.UnidadId,
                    AlmacenOrigen = new Almacen() { Id = x.AlmacenOrigenId, Nombre = x.AlmacenOrigenNombre },
                    AlmacenDestino = new Almacen() { Id = x.AlmacenDestinoId, Nombre = x.AlmacenDestinoNombre },
                    Producto = new Producto() { Id = x.ProductoId, Nombre = x.ProductoNombre },
                    Unidad = new Unidades() { Id = x.UnidadId, Nombre = x.UnidadNombre }
                }).ToList();
                return new PagedResult<TransferenciaAlmacen>() { Result = resultado, Count = dbContext.GetTableCount<TransferenciaAlmacen>(), CurrentPage = page };
            }
        }
    }
}
