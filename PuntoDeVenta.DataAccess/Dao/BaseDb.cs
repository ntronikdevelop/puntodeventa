﻿using System;
using MyORMV2;
using System.Configuration;
using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class BaseDb<T> : IBaseDb<T> where T : class
    {
        public BaseDb()
        {
        }
        protected DbContext InicializarDb()
        {
            return new DbContext(ConfigurationManager.ConnectionStrings["MyPosConnection"].ConnectionString, Server.MYSQLSERVER);
        }


        public virtual List<T> ObtenerLista()
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<T>().Expression.ToString()} where t0.Activo=1";
                var result = dbContext.Consulta<T>(queryInicial);
                return result;
            }
        }

        public virtual PagedResult<T> ObtenerListaPaginado(int page, int size)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPaged<T>(page, size);
                resultado.Count = dbContext.GetTableCount<T>();
                return resultado;
            }

        }

        public virtual PagedResult<T> ObtenerListaPaginado(int page, int size, bool activos)
        {

            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPaged<T>(page, size, activos);
                resultado.Count = dbContext.GetTableCount<T>();
                return resultado;
            }

        }

        public virtual int ObtenerCuentaDeTabla()
        {
            using (var dbContext = InicializarDb())
            {
                return dbContext.GetTableCount<T>();
            }
        }

        public virtual List<T> ObtenerListaPorConsulta(string query)
        {
            using (var dbContext = InicializarDb())
            {
                return dbContext.Consulta<T>(query);
            }
        }

        public virtual List<K> ObtenerListaPorConsulta<K>(string query)
        {
            using (var dbContext = InicializarDb())
                return dbContext.Consulta<K>(query);
        }

        //public virtual List<T> ObtenerListaPorConsulta(Expression<Func<T, bool>> exp)
        //{
        //    using (var dbContext = InicializarDb()){}
        //    return dbContext.Consulta<T>(exp);
        //}

        //public virtual PagedResult<T> ObtenerListaPorConsultaPaginado(Expression<Func<T, bool>> exp, int page, int size)
        //{
        //    PagedResult<T> resultado = new PagedResult<T>();
        //    using (var dbContext = InicializarDb()){}
        //    resultado.Result= dbContext.ConsultaPaginado<T>(exp, page, size);
        //    resultado.Count= dbContext.GetTableCountByExpression<T>(exp);
        //    return resultado;
        //}
        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise, string nombreCampo, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.Consulta<T>(pageZise, nombreCampo, caracteresABuscar);
                return resultado;
            }
        }

        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise, string nombreCampo, bool activos, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.Consulta<T>(pageZise, nombreCampo, activos, caracteresABuscar);
                return resultado;
            }
        }

        public virtual T ObtenerPorId(int id)
        {
            using (var dbContext = InicializarDb())
                return dbContext.Find<T>(id);
        }

        public virtual T ObtenerPorId(string id)
        {
            using (var dbContext = InicializarDb())
                return dbContext.Find<T>(id);
        }

        public virtual List<T> ObtenerUltimoRegistro()
        {
            using (var dbContext = InicializarDb())
                return dbContext.ConsultaUltimoRegistro<T>();
        }

        public virtual T Insertar(T obj)
        {
            using (var dbContext = InicializarDb())
                return dbContext.Insert<T>(obj);
        }

        public virtual bool Actualizar(T obj)
        {
            using (var dbContext = InicializarDb())
                return dbContext.Update<T>(obj);
        }

        public virtual void Borrar(int id)
        {
            using (var dbContext = InicializarDb())
                dbContext.Delete<T>(id);
        }

        //public virtual List<T> ObtenerListaPorConsulta(Expression<Func<T, bool>> exp, int cantidadMaxima)
        //{
        //    using (var dbContext = InicializarDb()){}
        //    return dbContext.Consulta<T>(exp,cantidadMaxima);
        //}

        public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            using (var dbContext = InicializarDb())
            {
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPagedAndFiltered<T>(page, size, activos, campo, tipoCampo, valor, out outQuery);
                resultado.Count = dbContext.GetTableCountForFilter<T>(outQuery);
                return resultado;
            }
        }

        public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<T>().Expression.ToString();
                Type tipo = typeof(T);
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();

                string queryDeFiltro = string.Format(" where t0.Activo={1}  ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<T>(queryInicial);
                resultado.Count = dbContext.GetTableCountForFilter<T>(queryDeFiltro);
                return resultado;
            }

        }

        public virtual PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<T>().Expression.ToString();
                Type tipo = typeof(T);
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();

                string queryDeFiltro = string.Format(" where t0.Activo={1}  ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                // queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<T>(queryInicial);
                resultado.Count = resultado.Result.Count;
                return resultado;
            }

        }
    }
}
