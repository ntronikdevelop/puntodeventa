﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class ProductoImpuestoDao : BaseDb<ProductoImpuesto>
    {
        public List<ProductoImpuesto> ObtenerImpuestosDeProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<ProductoImpuesto>().Expression.ToString()} where t0.Activo=1  and  t0.ProductoId={productoId}  ";
                var resultado = dbContext.Consulta<ProductoImpuesto>(queryInicial);
                return resultado;
            }
        }
    }
}
