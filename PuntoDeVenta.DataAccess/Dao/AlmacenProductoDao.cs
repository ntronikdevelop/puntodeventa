﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
  public  class AlmacenProductoDao : BaseDb<AlmacenProducto>
    {

        public List<AlmacenProducto> ObtenerExistenciaDeProductoEnAlmacen(int almacenId, int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<AlmacenProducto>().Expression.ToString()} where t0.Activo=1  and t0.AlmacenId={almacenId} and t0.ProductoId={productoId} ";
                return dbContext.Consulta<AlmacenProducto>(queryInicial);
            }
        }
        public List<AlmacenProducto> ObtenerProductosDeAlmacen(int almacenId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<AlmacenProducto>().Expression.ToString()} where t0.Activo=1  and t0.AlmacenId={almacenId}  ";
                return dbContext.Consulta<AlmacenProducto>(queryInicial);
            }
        }

        public List<AlmacenProducto> ObtenerExistenciaDeProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<AlmacenProducto>().Expression.ToString()} where t0.Activo=1  and  t0.ProductoId={productoId} ";
                return dbContext.Consulta<AlmacenProducto>(queryInicial);
            }
        }
    }
}
