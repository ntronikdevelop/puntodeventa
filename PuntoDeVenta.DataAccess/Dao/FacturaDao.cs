﻿using System;
using System.Collections.Generic;
using System.Linq;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class FacturaDao : BaseDb<Factura>, IFacturaDb
    {
        public List<Factura> ObtenerCXC(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, int? vendedorId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Factura>().Expression.ToString()} where t0.Activo=1  and t0.Estado='F' and t0.MontoAdeudado>0";
                if (FechaInicio.HasValue)
                    queryInicial += $" and t0.FechaFacturado >= '{FechaInicio.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (fechaFinal.HasValue)
                    queryInicial += $" and t0.FechaFacturado <= '{fechaFinal.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (idCliente.HasValue)
                {
                    queryInicial += $" and t0.ClienteId={idCliente.Value}";
                }
                if (monedaId.HasValue)
                {
                    queryInicial += $" and t0.MonedaId={monedaId.Value}";
                }

                if (vendedorId.HasValue)
                {
                    queryInicial += $" and t0.VendedorId={vendedorId.Value}";
                }

                return dbContext.Consulta<Factura>(queryInicial);
            }
        }

        public List<Factura> ObtenerVentas(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, Nullable<int> vendedorId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Factura>().Expression.ToString()} where t0.Activo=1  and t0.Estado!='A' ";
                if (FechaInicio.HasValue)
                    queryInicial += $" and t0.FechaFacturado >= '{FechaInicio.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (fechaFinal.HasValue)
                    queryInicial += $" and t0.FechaFacturado <= '{fechaFinal.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (idCliente.HasValue)
                {
                    queryInicial += $" and t0.ClienteId={idCliente.Value}";
                }
                if (monedaId.HasValue)
                {
                    queryInicial += $" and t0.MonedaId={monedaId.Value}";
                }
                if (vendedorId.HasValue)
                {
                    queryInicial += $" and t0.VendedorId={vendedorId.Value}";
                }
                return dbContext.Consulta<Factura>(queryInicial);
            }
        }

        public Factura ObtenerPorNumeroFactura(string numeroFactura)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Factura>().Expression.ToString()} where t0.Activo=1  and t0.NumeroFactura = '{numeroFactura}' ";
                var resultado = dbContext.Consulta<Factura>(queryInicial);
                return resultado.FirstOrDefault();
            }
        }

        public virtual PagedResult<Factura> ObtenerListaCotizacionPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<Factura>().Expression.ToString();
                Type tipo = typeof(Factura);
                string outQuery = string.Empty;
                PagedResult<Factura> resultado = new PagedResult<Factura>();

                string queryDeFiltro = string.Format(" where t0.Activo={1} AND t0.Estado='C' ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<Factura>(queryInicial);
                resultado.Count = dbContext.GetTableCountForFilter<Factura>(queryDeFiltro);
                return resultado;
            }

        }

        public virtual PagedResult<Factura> ObtenerListaCotizacionPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            using (var dbContext = InicializarDb())
            {
                string queryDeFiltro = string.Empty;
                PagedResult<Factura> resultado = new PagedResult<Factura>();
                string queryInicial = dbContext.Entities<Factura>().Expression.ToString();
                string query = $"{queryInicial} where t0.Activo={Convert.ToInt16(activos)} AND t0.Estado='C'  ";
                //string query = string.Format("select * from {0} where Activo={1} and Id >0  ", tipo.Name, Convert.ToInt16(Activos));
                if (!string.IsNullOrEmpty(valor) && !string.IsNullOrEmpty(campo))
                {
                    query += " AND t0." + campo + " ";
                    queryDeFiltro += " where t0.Activo= " + Convert.ToInt16(activos) + " AND t0." + campo + " ";
                    switch (tipoCampo)
                    {
                        case 0:
                            query += "like '%" + valor + "%' ";
                            queryDeFiltro += "like '%" + valor + "%' ";
                            break;
                        case 2:
                            DateTime fechaIni = Convert.ToDateTime(valor).Date;
                            query += $">= '{fechaIni.ToString("yyyy-MM-dd HH:mm:ss")}' AND t0.{campo} < '{fechaIni.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")}'";
                            queryDeFiltro += $">= '{fechaIni.ToString("yyyy-MM-dd HH:mm:ss")}' AND t0.{campo} < '{fechaIni.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss")}'";
                            break;
                        case 3:
                            query += "= '" + valor + "' ";
                            queryDeFiltro += "= '" + valor + "' ";
                            break;
                        default:
                            query += "= " + valor + " ";
                            queryDeFiltro += "= " + valor + " ";
                            break;
                    }
                }
                string outQuery = (queryDeFiltro);
                query += "order by t0.Id desc limit " + Convert.ToString(((page - 1) * size)) + " ,  " + Convert.ToString(size);


                resultado.Result = dbContext.EntitiesPagedAndFiltered<Factura>(page, size, activos, campo, tipoCampo, valor, out outQuery);
                resultado.Count = dbContext.GetTableCountForFilter<Factura>(outQuery);
                return resultado;
            }
        }

        public List<EstadoCuentaCliente> ObtenerEstadoCuenta(DateTime? FechaInicio, DateTime? fechaFinal, int? idCliente, int? monedaId)
        {
            BaseDb<EstadoCuentaCliente> service = new BaseDb<EstadoCuentaCliente>();
            return service.ObtenerListaPorConsulta($"CALL `dbo.ObtenerEstadoCuentaCliente`({idCliente});");
        }
    }
}
