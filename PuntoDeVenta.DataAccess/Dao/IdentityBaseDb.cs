﻿using System; using MyORMV2;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.DataAccess.Dao
{
   public   class IdentityBaseDb<T>: IBaseDb<T> where T:class
    {
       public  IdentityBaseDb()
        {
        }
        protected DbContext InicializarDb()
        {
            return new DbContext(ConfigurationManager.ConnectionStrings["MySqlIdentityConnection"].ConnectionString, Server.MYSQLSERVER);
        }


       public virtual  List<T> ObtenerLista()
        {
            using (var dbContext = InicializarDb())
            return dbContext.Entities<T>().ToList();
        }

       public virtual PagedResult<T> ObtenerListaPaginado(int page, int size)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPaged<T>(page, size);
                resultado.Count = dbContext.GetTableCount<T>();
                return resultado;
            }
           
        }

       public virtual List<T> ObtenerUltimoRegistro()
        {
            using (var dbContext = InicializarDb())
            return dbContext.ConsultaUltimoRegistro<T>();
        }

       public virtual int ObtenerCuentaDeTabla()
        {
            using (var dbContext = InicializarDb())
            return dbContext.GetTableCount<T>();
        }

       public virtual List<T> ObtenerListaPorConsulta(string query)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Consulta<T>(query);
        }

       public virtual List<T> ObtenerListaPorConsulta(Expression<Func<T, bool>> exp)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Consulta<T>(exp);
        }

       public virtual PagedResult<T> ObtenerListaPorConsultaPaginado(Expression<Func<T, bool>> exp, int page, int size)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.ConsultaPaginado<T>(exp, page, size);
                resultado.Count = dbContext.GetTableCountByExpression<T>(exp);
                return resultado;
            }
        }
      public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise,string nombreCampo,string   caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.Consulta<T>(pageZise, nombreCampo, caracteresABuscar);
                return resultado;
            }
        }

       public virtual T ObtenerPorId(int id)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Find<T>(id);
        }

       public virtual T Insertar(T obj)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Insert<T>(obj);
        }

       public virtual bool Actualizar(T obj)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Update<T>(obj);
        }

       public virtual void Borrar(int id)
        {
            using (var dbContext = InicializarDb())
            dbContext.Delete<T>(id);
        }

       public virtual List<T> ObtenerListaPorConsulta(Expression<Func<T, bool>> exp, int cantidadMaxima)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Consulta<T>(exp,cantidadMaxima);
        }

       public virtual PagedResult<T> ObtenerListaPaginado(int page, int size, bool activos)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPaged<T>(page, size, activos);
                resultado.Count = dbContext.GetTableCount<T>();
                return resultado;
            }
        }

       public virtual T ObtenerPorId(string id)
        {
            using (var dbContext = InicializarDb())
            return dbContext.Find<T>(id);
        }

       public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            using (var dbContext = InicializarDb())
            {
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.EntitiesPagedAndFiltered<T>(page, size, activos, campo, tipoCampo, valor, out outQuery);
                resultado.Count = dbContext.GetTableCountForFilter<T>(outQuery);
                return resultado;
            }
        }

       public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise, string nombreDeCampo, bool activo, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<T> resultado = new PagedResult<T>();
                resultado.Result = dbContext.Consulta<T>(pageZise, nombreDeCampo, activo, caracteresABuscar);
                return resultado;
            }
        }

       public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {

            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<T>().Expression.ToString();
                Type tipo = typeof(T);
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();

                string queryDeFiltro = string.Format(" where t0.Activo={1}  ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<T>(queryInicial);
                resultado.Count = dbContext.GetTableCountForFilter<T>(queryDeFiltro);
                return resultado;
            }
        }

       public virtual  PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<T>().Expression.ToString();
                Type tipo = typeof(T);
                string outQuery = string.Empty;
                PagedResult<T> resultado = new PagedResult<T>();

                string queryDeFiltro = string.Format(" where t0.Activo={1}  ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                // queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<T>(queryInicial);
                resultado.Count = resultado.Result.Count;
                return resultado;
            }

        }
    }
}
