﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class DetalleFacturaDao : BaseDb<DetalleFactura>, IDetalleFacturaDb
    {
        public List<DetalleFactura> DetallesPorProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<DetalleFactura>().Expression.ToString()} where t0.Activo=1  and t0.ProductoId ={productoId} ";
                var resultado = dbContext.Consulta<DetalleFactura>(queryInicial);
                return resultado;
            }
        }

        public List<DetalleFactura> ObtenerDetallesDeFactura(int facturaId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<DetalleFactura>().Expression.ToString()} where t0.Activo=1  and t0.FacturaId ={facturaId} ";
                var resultado = dbContext.Consulta<DetalleFactura>(queryInicial);
                return resultado;
            }
        }

        public List<DetalleFactura> ObtenerDetallesHijos(int detallePadreId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<DetalleFactura>().Expression.ToString()} where t0.Activo=1  and t0.DetallePadreId ={detallePadreId} ";
                var resultado = dbContext.Consulta<DetalleFactura>(queryInicial);
                return resultado;
            }
        }

        public List<DetalleFactura> ObtenerDetallesPadresDeFactura(int facturaId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<DetalleFactura>().Expression.ToString()} where t0.Activo=1  and t0.FacturaId ={facturaId} and t0.DetallePadreId is null ";
                var resultado = dbContext.Consulta<DetalleFactura>(queryInicial);
                return resultado;
            }

           
        }
    }
}
