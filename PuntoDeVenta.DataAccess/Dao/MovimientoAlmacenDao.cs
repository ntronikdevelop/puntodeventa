﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
  public  class MovimientoAlmacenDao : BaseDb<MovimientoAlmacen>
    {


        public List<MovimientoProductoModel> ObtenerMovimientosPorProducto(int? productoId, int? almacenId, string fechaInicio, string fechaFinal)
        {
            using (var dbContext = InicializarDb())
            {

                List<MovimientoProductoModel> resultado = new List<MovimientoProductoModel>();
                string query = $"select p.Nombre as ProductoNombre, a.Nombre as AlmacenNombre,m.Cantidad,m.TipoMovimiento,m.FechaCreacion, a.Codigo as CodigoAlmacen, m.Referencia, u.Nombre as UnidadNombre," +
                    "  u.Id as UnidadId from movimientoalmacen as m inner join producto as p on m.ProductoId=p.Id inner join almacen as a on m.AlmacenId=a.Id inner join unidades as u on m.UnidadId=u.Id  where m.Activo=1 ";
                if (!string.IsNullOrEmpty(fechaInicio))
                    query += $" and m.FechaCreacion >= '{fechaInicio}' ";

                if (!string.IsNullOrEmpty(fechaFinal))
                    query += $" and m.FechaCreacion <= '{fechaFinal}' ";

                if (productoId.HasValue)
                    query += $" and m.ProductoId = {productoId.Value.ToString()} ";

                if (almacenId.HasValue)
                    query += $" and m.AlmacenId = {almacenId.Value.ToString()} ";

                resultado = base.ObtenerListaPorConsulta<MovimientoProductoModel>(query);

                return resultado;
            }
        }

        public List<MovimientoAlmacen> ObtenerHistorialDeProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<MovimientoAlmacen>().Expression.ToString()} where t0.Activo=1  and t0.ProductoId={productoId} ";
                return dbContext.Consulta<MovimientoAlmacen>(queryInicial);
            }
        }
    }
}
