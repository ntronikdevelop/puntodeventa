﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class BalanceClienteDao : BaseDb<BalanceCliente>
    {
        public List<BalanceCliente> BalanceDeClientePorMoneda(int clienteId, int monedaId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<BalanceCliente>().Expression.ToString()} where  t0.ClienteId={clienteId} and t0.MonedaId={monedaId} ";
                return dbContext.Consulta<BalanceCliente>(queryInicial);
            }
        }
    }
}
