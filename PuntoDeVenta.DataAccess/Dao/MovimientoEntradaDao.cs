﻿using System;
using System.Linq;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class MovimientoEntradaDao : BaseDb<MovimientoEntrada>
    {
        public override PagedResult<MovimientoEntrada> ObtenerListaPaginado(int page, int size, bool activos)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<MovimientoEntrada> result = new PagedResult<MovimientoEntrada>();

                Type tipo = typeof(MovimientoEntrada);
                result.Result = dbContext.Consulta<MovimientoEntradaCustom>(
                    string.Format("select m.Id,m.ProductoId,m.SuplidorId,m.Cantidad,m.UnidadId,m.CreadoPor,m.Referencia, m.AlmacenId,m.Activo," +
                    " u.Nombre as UnidadNombre,m.FechaCreacion, p.Nombre as ProductoNombre, a.Nombre as AlmacenNombre,s.Nombre as SuplidorNombre from movimientoentrada m "
                    + "left join producto p on m.ProductoId=p.Id left join almacen a on m.AlmacenId =a.Id left join suplidores s on m.SuplidorId = s.Id left join unidades u on m.UnidadId=u.Id"
                    + " where m.Activo={3} group by Referencia limit {1},{2} ",
                    tipo.Name, ((page - 1) * size), size, Convert.ToInt16(activos))).Select(x => new MovimientoEntrada()
                    {
                        Activo = x.Activo,
                        Almacen = new Almacen() { Id = x.AlmacenId, Nombre = x.AlmacenNombre },
                        AlmacenId = x.AlmacenId,
                        Cantidad = x.Cantidad,
                        FechaCreacion = x.FechaCreacion,
                        CreadoPor = x.CreadoPor,
                        Id = x.Id,
                        Producto = new Producto() { Id = x.ProductoId, Nombre = x.ProductoNombre },
                        ProductoId = x.ProductoId,
                        Referencia = x.Referencia,
                        Suplidor = new Suplidores() { Id = x.SuplidorId, Nombre = x.SuplidorNombre },
                        SuplidorId = x.SuplidorId,
                        Unidad = new Unidades() { Id = x.UnidadId, Nombre = x.UnidadNombre },
                        UnidadId = x.UnidadId
                    }).ToList();
                result.Count = dbContext.GetTableCountGrouped<MovimientoEntrada>("select Count(*) from movimientoentrada group by Referencia");
                return result;
            }
        }
    }
}
