﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dao.Interfaces
{
   public interface IDetalleFacturaDb :IBaseDb<DetalleFactura>
    {
        List<DetalleFactura> ObtenerDetallesPadresDeFactura(int facturaId);
        List<DetalleFactura> ObtenerDetallesDeFactura(int facturaId);
        List<DetalleFactura> ObtenerDetallesHijos(int detallePadreId);
        List<DetalleFactura> DetallesPorProducto(int productoId);
    }
}
