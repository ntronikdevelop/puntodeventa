﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dao.Interfaces
{
    public interface IProductoDb : IBaseDb<Producto>
    {
        PagedResult<Producto> ObtenerSoloProductosFiltradosPorCampoYLimitada(int pageZise, string nombreDeCampo, string caracteresABuscar);

        PagedResult<Producto> ObtenerSoloServiciosFiltradosPorCampoYLimitada(int pageZise, string nombreDeCampo, string caracteresABuscar);

        PagedResult<Producto> ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar);

        PagedResult<Producto> ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar);
        PagedResult<Producto> ObtenerFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar);

    }
}
