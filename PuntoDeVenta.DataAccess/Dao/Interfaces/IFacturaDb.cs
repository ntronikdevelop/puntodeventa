﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dao.Interfaces
{
   public interface IFacturaDb : IBaseDb<Factura>
    {
        List<Factura> ObtenerCXC(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, int? vendedorId);

        List<EstadoCuentaCliente> ObtenerEstadoCuenta(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId);
    }
}
