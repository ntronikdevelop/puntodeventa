﻿using PuntoDeVenta.DataAccess.Dto;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dao.Interfaces
{
 public   interface IBaseDb<T> where T:class
    {


          List<T> ObtenerLista();

 

        PagedResult<T> ObtenerListaPaginado(int page, int size, bool activos);
        PagedResult<T> ObtenerListaPaginadoYFiltro(int page, int size, bool activos, string campo,int tipoCampo, string valor);

        int ObtenerCuentaDeTabla();

          List<T> ObtenerListaPorConsulta(string query);


        PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise,string nombreDeCampo, string caracteresABuscar);

        PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int pageZise, string nombreDeCampo,bool activo, string caracteresABuscar);

        T ObtenerPorId(int id);
        T ObtenerPorId(string id);

        T Insertar(T obj);

          bool Actualizar(T obj);

          void Borrar(int id);
        List<T> ObtenerUltimoRegistro();

        PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda);

        PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda);

    }
}
