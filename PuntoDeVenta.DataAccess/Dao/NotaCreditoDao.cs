﻿using System.Linq;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class NotaCreditoDao : BaseDb<NotaCredito>
    {
        public NotaCredito ObtenerPorSecuencia(string referencia)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<NotaCredito>().Expression.ToString()} where t0.Activo=1  and t0.Secuencia = '{referencia}' ";
                var resultado = dbContext.Consulta<NotaCredito>(queryInicial);
                return resultado.FirstOrDefault();
            }
        }
    }
}
