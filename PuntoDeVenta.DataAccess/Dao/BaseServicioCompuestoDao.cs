﻿using System.Collections.Generic;
using System.Linq;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class BaseServicioCompuestoDao : BaseDb<BaseServicioCompuesto>
    {
        public List<BaseServicioCompuesto> ObtenerProductosDerivados(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<BaseServicioCompuesto>().Expression.ToString()} where t0.Activo=1  and t0.ProductoBaseId = {productoId} ";
                var resultado = dbContext.Consulta<BaseServicioCompuesto>(queryInicial);
                return resultado;
            }
        }

        public List<BaseServicioCompuesto> ObtenerBasesDeProducto(int productoId)
        {
            using (var dbContext = InicializarDb())
            {
                string query = "select b.ProductoCompuestoId,b.ProductoBaseId,b.ProductoUnidadBaseId, b.Cantidad,b.CostoTotal,b.PrecioTotal,b.CreadoPor,b.FechaModificacion,b.FechaCreacion, b.Activo,b.ModificadoPor,b.Id," +
                    $"pue.ProductoId as ProductoUnidadBaseProductoId, pue.UnidadId as ProductoUnidadBaseUnidadId, pue.Equivalencia as ProductoUnidadBaseEquivalencia, pue.EsPrincipal as ProductoUnidadBaseEsPrincipal, pue.Orden as ProductoUnidadBaseOrden," +
                $" pue.PrecioCompra as ProductoUnidadBasePrecioCompra, pue.precioVenta as ProductoUnidadBasePrecioVenta, u.Nombre as NombreUnidad, " +
                    $"p.nombre as NombreProductoBase  from baseserviciocompuesto b left join  ProductoUnidadesEquivalencia pue on b.ProductoUnidadBaseId =pue.Id" +
                $" left join unidades u on pue.unidadId = u.id left join producto p on b.productoBaseId= p.id where b.Activo=1  and b.ProductoCompuestoId = {productoId} ";
                //  string queryInicial = $"{dbContext.Entities<BaseServicioCompuesto>().Expression.ToString()} where t0.Activo=1  and t0.ProductoCompuestoId = {productoId} ";
                var resultadoCustom = dbContext.Consulta<BaseServicioCompuestoCustom>(query);
                var resultado = resultadoCustom.Select(x => new BaseServicioCompuesto()
                {
                    Id = x.Id,
                    Activo = x.Activo,
                    Cantidad = x.Cantidad,
                    CostoTotal = x.CostoTotal,
                    CreadoPor = x.CreadoPor,
                    FechaCreacion = x.FechaCreacion,
                    FechaModificacion = x.FechaModificacion,
                    ModificadoPor = x.ModificadoPor,
                    PrecioTotal = x.PrecioTotal,
                    ProductoBaseId = x.ProductoBaseId,
                    ProductoCompuestoId = x.ProductoCompuestoId,
                    ProductoUnidadBaseId = x.ProductoUnidadBaseId,
                    NombreServicioBase = x.NombreProductoBase,
                    ProductoUnidadBase = x.ProductoUnidadBaseId.HasValue ? new ProductoUnidadesEquivalencia()
                    {
                        Activo = true,
                        Equivalencia = x.ProductoUnidadBaseEquivalencia,
                        EsPrincipal = x.ProductoUnidadBaseEsPrincipal,
                        Orden = x.ProductoUnidadBaseOrden,
                        PrecioCompra = x.ProductoUnidadBasePrecioCompra,
                        PrecioVenta = x.ProductoUnidadBasePrecioVenta,
                        Unidad = new Unidades() { Id = x.ProductoUnidadBaseUnidadId, Nombre = x.NombreUnidad }
                    } : null,
                    Unidad = x.ProductoUnidadBaseId.HasValue ? new Unidades() { Id = x.ProductoUnidadBaseUnidadId, Nombre = x.NombreUnidad } : null
                }).ToList();
                return resultado;
            }
        }
    }
}
