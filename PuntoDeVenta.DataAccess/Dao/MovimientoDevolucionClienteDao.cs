﻿using System;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class MovimientoDevolucionClienteDao : BaseDb<MovimientoDevolucionCliente>
    {
        public override PagedResult<MovimientoDevolucionCliente> ObtenerListaPaginado(int page, int size, bool activos)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<MovimientoDevolucionCliente> result = new PagedResult<MovimientoDevolucionCliente>();

                Type tipo = typeof(MovimientoDevolucionCliente);
                result.Result = base.ObtenerListaPorConsulta(
                    string.Format("select * from {0} where Activo={3} group by Referencia limit {1},{2} ",
                    tipo.Name, ((page - 1) * size), size, Convert.ToInt16(activos)));
                result.Count = dbContext.GetTableCountGrouped<MovimientoDevolucionCliente>("select Count(*) from movimientodevolucioncliente group by Referencia");
                return result;
            }
        }
    }
}
