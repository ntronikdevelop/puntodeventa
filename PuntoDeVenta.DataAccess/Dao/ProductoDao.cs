﻿using System.Collections.Generic;
using System.Linq;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class ProductoDao : BaseDb<Producto>, IProductoDb
    {

        public PagedResult<Producto> ObtenerSoloProductosFiltradosPorCampoYLimitada(int pageZise, string nombreDeCampo, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and t0.{nombreDeCampo} like '%{caracteresABuscar}%' AND t0.EsServicio=0 limit 0,{pageZise} ";
                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return new PagedResult<Producto>() { Count = resultado.Count, CurrentPage = 0, Result = resultado };
            }
        }

        public List<Producto> ObtenerPorNombre(string nombre)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and t0.Nombre = '{nombre}' ";
                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return resultado;
            }
        }

        public PagedResult<Producto> ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and (t0.{nombreDeCampo1} like '%{caracteresABuscar}%' OR t0.{nombreDeCampo2} like '%{caracteresABuscar}%' OR t0.descripcion like '%{caracteresABuscar}%')" +
                    $" AND t0.EsServicio=1 limit 0,{pageZise} ";

                //  base.InicializarDb();
                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return new PagedResult<Producto>() { Count = resultado.Count, CurrentPage = 0, Result = resultado };
            }
        }

        public PagedResult<Producto> ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and (t0.{nombreDeCampo1} like '%{caracteresABuscar}%' OR t0.{nombreDeCampo2} like '%{caracteresABuscar}%' OR t0.descripcion like '%{caracteresABuscar}%')" +
                                $" AND t0.EsServicio=0 limit 0,{pageZise} ";
                // base.InicializarDb();
                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return new PagedResult<Producto>() { Count = resultado.Count, CurrentPage = 0, Result = resultado };
            }
        }


        public PagedResult<Producto> ObtenerSoloServiciosFiltradosPorCampoYLimitada(int pageZise, string nombreDeCampo1, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and (t0.{nombreDeCampo1} like '%{caracteresABuscar}%'  OR t0.descripcion like '%{caracteresABuscar}%')AND t0.EsServicio=1 limit 0,{pageZise} ";

                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return new PagedResult<Producto>() { Count = resultado.Count, CurrentPage = 0, Result = resultado };
            }
        }

        public PagedResult<Producto> ObtenerFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<Producto>().Expression.ToString()} where t0.Activo=1  and (t0.{nombreDeCampo1} like '%{caracteresABuscar}%' OR t0.{nombreDeCampo2} like '%{caracteresABuscar}%'  OR t0.descripcion like '%{caracteresABuscar}%')" +
               $"  limit 0,{pageZise} ";
                //  base.InicializarDb();
                var resultado = dbContext.Consulta<Producto>(queryInicial);
                return new PagedResult<Producto>() { Count = resultado.Count, CurrentPage = 0, Result = resultado };
            }
        }
    }
}
