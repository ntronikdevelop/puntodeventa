﻿using System;
using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class GastoEmpresaDao : BaseDb<GastoEmpresa>
    {
        public List<GastoEmpresa> ObtenerCxPGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> suplidorId, Nullable<int> monedaId, Nullable<int> localidadId)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = $"{dbContext.Entities<GastoEmpresa>().Expression.ToString()} where t0.Activo=1  and t0.Estado='I' and t0.MontoAdeudado>0";
                if (FechaInicio.HasValue)
                    queryInicial += $" and t0.FechaEmision >= '{FechaInicio.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (fechaFinal.HasValue)
                    queryInicial += $" and t0.FechaEmision <= '{fechaFinal.Value.ToString("yyyy-MM-dd HH:mm:ss")}'";

                if (suplidorId.HasValue)
                {
                    queryInicial += $" and t0.SuplidorId={suplidorId.Value}";
                }

                if (localidadId.HasValue)
                {
                    queryInicial += $" and t0.LocalidadId={localidadId.Value}";
                }

                if (monedaId.HasValue)
                {
                    queryInicial += $" and t0.MonedaId={monedaId.Value}";
                }

                return dbContext.Consulta<GastoEmpresa>(queryInicial);
            }
        }
    }
}
