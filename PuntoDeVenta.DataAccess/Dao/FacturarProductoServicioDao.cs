﻿using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class FacturarProductoServicioDao : BaseDb<FacturarProductoServicio>
    {
        public override List<FacturarProductoServicio> ObtenerLista()
        {
            using (var dbContext = InicializarDb()) 
            {
                string queryInicial = $"{dbContext.Entities<FacturarProductoServicio>().Expression.ToString()} where id>0";
                return dbContext.Consulta<FacturarProductoServicio>(queryInicial);
            }
        }
    }
}
