﻿using System;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.DataAccess.Dao
{
    public class SeccionOperacionesDao : IdentityBaseDb<SeccionOperaciones>
    {
        public override PagedResult<SeccionOperaciones> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            using(var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<SeccionOperaciones>().Expression.ToString();
                Type tipo = typeof(SeccionOperaciones);
                string outQuery = string.Empty;
                PagedResult<SeccionOperaciones> resultado = new PagedResult<SeccionOperaciones>();

                string queryDeFiltro = string.Format(" where t0.Id>0 ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<SeccionOperaciones>(queryInicial);
                resultado.Count = dbContext.GetTableCountForFilter<SeccionOperaciones>(queryDeFiltro);
                return resultado;
            }
         

        }

        public override PagedResult<SeccionOperaciones> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            using (var dbContext = InicializarDb())
            {
                string queryInicial = dbContext.Entities<SeccionOperaciones>().Expression.ToString();
                Type tipo = typeof(SeccionOperaciones);
                string outQuery = string.Empty;
                PagedResult<SeccionOperaciones> resultado = new PagedResult<SeccionOperaciones>();

                string queryDeFiltro = string.Format(" where t0.Id>0  ", tipo.Name, Convert.ToInt16(buqueda.Activo));
                foreach (var filtro in buqueda.Filtros)
                    queryDeFiltro += DbContext.GenerarFiltroBusqueda(filtro.Campo, filtro.TipoCampo, filtro.Valor);
                queryInicial += queryDeFiltro;
                // queryInicial += " order by t0.id desc limit " + Convert.ToString(((buqueda.Page - 1) * buqueda.Size)) + " ,  " + Convert.ToString(buqueda.Size);

                resultado.Result = dbContext.Consulta<SeccionOperaciones>(queryInicial);
                resultado.Count = resultado.Result.Count;
                return resultado;
            }

        }
    }
}
