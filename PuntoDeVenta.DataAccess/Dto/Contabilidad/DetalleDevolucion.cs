﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
    public class DetalleDevolucion : DetalleFactura, IEquatable<DetalleDevolucion>
    {

        public decimal CantidadDevolucion { get; set; }
        public bool Defectuoso { get; set; }
        public DetalleDevolucion() { }

       

        public DetalleDevolucion(DetalleDevolucion nuevoDetalle)
        {
            this.Id = nuevoDetalle.Id;
            this.ProductoId = nuevoDetalle.ProductoId;
            this.Producto = nuevoDetalle.Producto ?? null;
            this.Activo = nuevoDetalle.Activo;
            this.AlmacenId = nuevoDetalle.AlmacenId ?? null;
            this.Cantidad = nuevoDetalle.Cantidad;
            this.Comentario = nuevoDetalle.Comentario;
            this.CreadoPor = nuevoDetalle.CreadoPor ?? string.Empty;
            this.Detalles = nuevoDetalle.Detalles ?? string.Empty;
            this.FacturaId = nuevoDetalle.FacturaId;
            this.Fecha = nuevoDetalle.Fecha;
            this.FechaCreacion = nuevoDetalle.FechaCreacion;
            this.FechaModificacion = nuevoDetalle.FechaModificacion;
            this.ModificadoPor = nuevoDetalle.ModificadoPor ?? string.Empty;
            this.Monto = nuevoDetalle.Monto;
            this.MontoDop = nuevoDetalle.MontoDop;
            this.MontoGrabado = nuevoDetalle.MontoGrabado;
            this.MontoItbis = nuevoDetalle.MontoItbis;
            this.MontoTotal = nuevoDetalle.MontoTotal;
            this.Nombre = nuevoDetalle.Nombre ?? string.Empty;
            this.Unidad = nuevoDetalle.Unidad ?? null;
            this.UnidadId = nuevoDetalle.UnidadId ?? null;
            this.CantidadDevolucion = nuevoDetalle.CantidadDevolucion;
            this.Defectuoso = nuevoDetalle.Defectuoso;
        }
     
        public bool Equals(DetalleDevolucion other)
        {
            return (this.Id == other.Id && this.ProductoId == other.ProductoId && this.Cantidad == other.Cantidad && this.Monto == other.Monto &&
                this.UnidadId == other.UnidadId && this.MontoItbis == other.MontoItbis && this.MontoGrabado == other.MontoGrabado && this.MontoTotal == other.MontoTotal &&
                this.FacturaId == other.FacturaId && this.CantidadDevolucion==other.CantidadDevolucion);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ this.Id;
                hashCode = (hashCode * 397) ^ this.ProductoId;
                hashCode = (hashCode * 397) ^ this.FacturaId;
                hashCode = this.UnidadId.HasValue ? (hashCode * 397) ^ this.UnidadId.Value : hashCode;

                var hashCodeDecimal = this.Cantidad.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal;

                var hashCodeDecimal2 = this.Monto.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal2;

                var hashCodeDecimal3 = this.MontoGrabado.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal3;

                var hashCodeDecimal4 = this.MontoItbis.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal4;

                var hashCodeDecimal5 = this.MontoTotal.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal5;

                var hashCodeDecimal6 = this.CantidadDevolucion.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal6;



                return hashCode;
            }
        }


    }
}
