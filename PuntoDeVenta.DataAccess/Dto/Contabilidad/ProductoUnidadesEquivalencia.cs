﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Interfaces;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
  public  class ProductoUnidadesEquivalencia : IDatosComunes, IEquatable<ProductoUnidadesEquivalencia>
    {


        public int ProductoId { get; set; }

        public int UnidadId { get; set; }
        public decimal Equivalencia { get; set; }
        public bool EsPrincipal { get; set; }
        public int Orden { get; set; }
        
     

        public decimal PrecioCompra { get; set; }

        public decimal PrecioVenta { get; set; }

        public bool Equals(ProductoUnidadesEquivalencia other)
        {
            return (this.Id == other.Id && this.ProductoId == other.ProductoId && this.Equivalencia == other.Equivalencia && this.PrecioCompra == other.PrecioCompra &&
                this.UnidadId == other.UnidadId && this.PrecioVenta == other.PrecioVenta && this.Activo==other.Activo);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ this.Id;
                hashCode = (hashCode * 397) ^ this.ProductoId;
                hashCode = (hashCode * 397) ^ this.UnidadId;

                var hashCodeDecimal = this.Equivalencia.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal;

                var hashCodeDecimal2 = this.PrecioCompra.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal2;

                var hashCodeDecimal3 = this.PrecioVenta.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal3;




                return hashCode;
            }
        }

        public  string CreadoPor
        {
            get; set;
        }


        public  DateTime FechaModificacion
        {
            get; set;
        }

        public  DateTime FechaCreacion
        {
            get; set;
        }

        public  bool Activo { get; set; }

        public  string ModificadoPor
        {
            get; set;
        }

        [AutoGenerated]
        [PrimaryKey]
        public  int Id { get; set; }
        [Join("UnidadId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Unidades Unidad { get; set; }

        //[Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        //public Producto Producto { get; set; }

        [AutoGeneratedNoDB]
        public string Detalles { get; set; }

        [AutoGeneratedNoDB]
        public string Nombre { get; set; }
    }

    public class ProductoUnidadesCustom : IDatosComunes, IEquatable<ProductoUnidadesCustom>
    {


        public int ProductoId { get; set; }

        public int UnidadId { get; set; }
        public decimal Equivalencia { get; set; }
        public bool EsPrincipal { get; set; }
        public int Orden { get; set; }

        public string NombreUnidad { get; set; }

        public decimal PrecioCompra { get; set; }

        public decimal PrecioVenta { get; set; }

        public bool Equals(ProductoUnidadesCustom other)
        {
            return (this.Id == other.Id && this.ProductoId == other.ProductoId && this.Equivalencia == other.Equivalencia && this.PrecioCompra == other.PrecioCompra &&
                this.UnidadId == other.UnidadId && this.PrecioVenta == other.PrecioVenta && this.Activo == other.Activo);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ this.Id;
                hashCode = (hashCode * 397) ^ this.ProductoId;
                hashCode = (hashCode * 397) ^ this.UnidadId;

                var hashCodeDecimal = this.Equivalencia.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal;

                var hashCodeDecimal2 = this.PrecioCompra.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal2;

                var hashCodeDecimal3 = this.PrecioVenta.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal3;




                return hashCode;
            }
        }

        public string CreadoPor
        {
            get; set;
        }


        public DateTime FechaModificacion
        {
            get; set;
        }

        public DateTime FechaCreacion
        {
            get; set;
        }

        public bool Activo { get; set; }

        public string ModificadoPor
        {
            get; set;
        }

        [AutoGenerated]
        [PrimaryKey]
        public int Id { get; set; }
      


        [AutoGeneratedNoDB]
        public string Detalles { get; set; }

        [AutoGeneratedNoDB]
        public string Nombre { get; set; }
    }
}
