﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Interfaces;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
   public class BalanceCliente :IDatosComunes
    {
      

        public int ClienteId { get; set; }

        public int MonedaId { get; set; }

        public decimal MontoAdeudado { get; set; }

        public  DateTime FechaModificacion{ get; set;  }


        public  string ModificadoPor  {get; set; }
        
        [AutoGenerated]
        [PrimaryKey]
        public  int Id { get; set; }




        [Join("ClienteId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Cliente Cliente { get; set; }


        [Join("MonedaId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Moneda Moneda { get; set; }


        [AutoGeneratedNoDB]
        public DateTime FechaCreacion{  get; set;    }

        [AutoGeneratedNoDB]
        public string CreadoPor { get; set;  }

        [AutoGeneratedNoDB]
        public bool Activo { get; set; }

        [AutoGeneratedNoDB]
        public string Detalles { get; set; }


        [AutoGeneratedNoDB]
        public string Nombre { get; set; }
    }
}
