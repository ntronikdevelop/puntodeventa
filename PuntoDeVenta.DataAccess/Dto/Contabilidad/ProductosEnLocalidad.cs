﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
 public   class ProductosEnLocalidad
    {
        public int LocalidadId { get; set; }
        [Join("LocalidadId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Localidad Localidad { get; set; }
        public List<AlmacenProducto> ProductoPorAlmacen { get; set; }
       

    }
}
