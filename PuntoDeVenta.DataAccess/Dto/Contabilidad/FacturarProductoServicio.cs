﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
public    class FacturarProductoServicio 
    {
        public int Id { get; set; }

        public bool EsServicio { get; set; }
        public bool EsCompuesto { get; set; }
        public string NombreDeClase { get; set; }
    }
}
