﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
public    class SalidaProductos : DatosComunes
    {

        public int ProductId { get; set; }
        public int ClienteId { get; set; }
        public DateTime Fecha { get; set; }

        public int UnidadId { get; set; }
        
        [Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Producto Producto { get; set; }
        
        [Join("UnidadId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Unidades Unidad { get; set; }
    }
}
