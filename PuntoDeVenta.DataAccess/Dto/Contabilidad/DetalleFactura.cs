﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using MyORMV2.Enums;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
    public class DetalleFactura : IDatosComunes, IEquatable<DetalleFactura>
    {
        public DetalleFactura() { }

        public DetalleFactura(DetalleFactura nuevoDetalle)
        {
            this.Id = nuevoDetalle.Id;
            this.ProductoId = nuevoDetalle.ProductoId;
            this.Producto = nuevoDetalle.Producto ?? null;
            this.Activo = nuevoDetalle.Activo;
            this.AlmacenId = nuevoDetalle.AlmacenId ?? null;
            this.Cantidad = nuevoDetalle.Cantidad;
            this.Comentario = nuevoDetalle.Comentario;
            this.CreadoPor = nuevoDetalle.CreadoPor ?? string.Empty;
            this.Detalles = nuevoDetalle.Detalles ?? string.Empty;
            this.FacturaId = nuevoDetalle.FacturaId;
            this.Fecha = nuevoDetalle.Fecha;
            this.FechaCreacion = nuevoDetalle.FechaCreacion;
            this.FechaModificacion = nuevoDetalle.FechaModificacion;
            this.ModificadoPor = nuevoDetalle.ModificadoPor ?? string.Empty;
            this.Monto = nuevoDetalle.Monto;
            this.MontoDop = nuevoDetalle.MontoDop;
            this.MontoGrabado = nuevoDetalle.MontoGrabado;
            this.MontoItbis = nuevoDetalle.MontoItbis;
            this.MontoTotal = nuevoDetalle.MontoTotal;
            this.Nombre = nuevoDetalle.Nombre ?? string.Empty;
            this.Unidad = nuevoDetalle.Unidad ?? null;
            this.UnidadId = nuevoDetalle.UnidadId ?? null;
            this.MontoDescuento = nuevoDetalle.MontoDescuento;
            this.MontoNotaCreditoAplicada = nuevoDetalle.MontoNotaCreditoAplicada;
            this.DetallePadreId = nuevoDetalle.DetallePadreId;
            this.InsertarRegistro = nuevoDetalle.InsertarRegistro;
            this.PorcientoDescuento = nuevoDetalle.PorcientoDescuento;
            this.EsGratis = nuevoDetalle.EsGratis;
            this.PorcentajeVendedor = nuevoDetalle.PorcentajeVendedor;
            this.Costo = nuevoDetalle.Costo;
        }

        public int ProductoId { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PorcentajeVendedor { get; set; } = 0;
        public decimal Costo { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public bool EsGratis { get; set; }
        public List<ProductoImpuesto> Impuestos { get; set; }
        public DateTime Fecha { get; set; }

        public int FacturaId { get; set; }

        public decimal MontoGrabado { get; set; }

        public string Comentario { get; set; }

        public decimal MontoDop { get; set; }
        public decimal PorcientoDescuento { get; set; }

      
        
      


        public Nullable<int> AlmacenId { get; set; }

        

        public decimal MontoDescuento { get; set; }
        public decimal MontoNotaCreditoAplicada { get; set; }

        public Nullable<int> DetallePadreId { get; set; }

        public Nullable<int> UnidadId { get; set; }

        public bool Equals(DetalleFactura other)
        {
            return (this.Id == other.Id && this.ProductoId == other.ProductoId && this.Cantidad == other.Cantidad && this.Monto == other.Monto && this.Costo==other.Costo &&
                this.UnidadId == other.UnidadId && this.MontoItbis == other.MontoItbis && this.MontoGrabado == other.MontoGrabado && this.MontoTotal == other.MontoTotal &&
                this.FacturaId == other.FacturaId && this.MontoNotaCreditoAplicada==other.MontoNotaCreditoAplicada && this.MontoDescuento==other.MontoDescuento && this.PorcientoDescuento==other.PorcientoDescuento
                && this.EsGratis== other.EsGratis && this.DetallePadreId==other.DetallePadreId && this.AlmacenId==other.AlmacenId &&
                this.Activo==other.Activo && this.Fecha== other.Fecha && this.FechaCreacion==other.FechaCreacion );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ this.Id;

                hashCode = (hashCode * 397) ^ this.ProductoId;
                hashCode = (hashCode * 397) ^ this.FacturaId;
                


                hashCode = this.UnidadId.HasValue ? (hashCode * 397) ^ this.UnidadId.Value : hashCode;
                hashCode = this.DetallePadreId.HasValue ? (hashCode * 397) ^ this.DetallePadreId.Value : hashCode;
                hashCode = this.AlmacenId.HasValue ? (hashCode * 397) ^ this.AlmacenId.Value : hashCode;
                hashCode = (hashCode * 397) ^ Convert.ToInt32(this.Activo);

                var hashCodeDecimal = this.Cantidad.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal;

                var hashCodeDecimal2 = this.Monto.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal2;

                var hashCodeDecimal3 = this.MontoGrabado.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal3;

                var hashCodeDecimal4 = this.MontoItbis.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal4;

                var hashCodeDecimal5 = this.MontoTotal.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal5;

                var hashCodeDecimal6 = this.MontoDescuento.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal6;

                var hashCodeDecimal7 = this.MontoNotaCreditoAplicada.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal7;

                var hashCodeDecimal8 = this.PorcientoDescuento.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal8;

                var hashCodeDecimal9 = this.Costo.GetHashCode();
                hashCode = hashCode ^ hashCodeDecimal9;

                return hashCode;
            }
        }

        public  string CreadoPor
        {
            get; set;
        }


        public  DateTime FechaModificacion
        {
            get; set;
        }
        public  string ModificadoPor
        {
            get; set;
        }

        public  DateTime FechaCreacion
        {
            get; set;
        }

        public  bool Activo { get; set; }


        [AutoGenerated]
        [PrimaryKey]
        public  int Id { get; set; }
        [Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Producto Producto { get; set; }

        

        [Join("UnidadId", "Id",Jointype.LEFT)]
        public Unidades Unidad { get; set; }

        [AutoGeneratedNoDB]
        public bool InsertarRegistro { get; set; } = true;

        [AutoGeneratedNoDB]
        public string Detalles { get; set; }
        [AutoGeneratedNoDB]
        public string Nombre { get; set; }
    }
}
