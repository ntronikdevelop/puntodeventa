﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
 public   class DevolucionCliente
    {
        public Factura Factura { get; set; }
        

        public List<DetalleDevolucion> DetallesDevolucion { get; set; }
    }
}
