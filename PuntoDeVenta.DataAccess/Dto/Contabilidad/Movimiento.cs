﻿using PuntoDeVenta.DataAccess.Dto.Interfaces;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
    public class MovimientoEntrada : IDatosComunes
    {
       

        public int ProductoId { get; set; }

        public decimal Cantidad { get; set; }
       
        public string Referencia { get; set; }
        public int AlmacenId { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public int SuplidorId { get; set; }
       

       

        public  string CreadoPor
        {
            get; set;
        }


        public  DateTime FechaModificacion
        {
            get; set;
        }

        public  DateTime FechaCreacion
        {
            get; set;
        }

        public  bool Activo { get; set; }
        public  string ModificadoPor
        {
            get; set;
        }

        [AutoGenerated]
        [PrimaryKey]
        public  int Id { get; set; }

        [Join("SuplidorId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Suplidores Suplidor { get; set; }
        [Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Producto Producto { get; set; }
        [Join("AlmacenId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Almacen Almacen { get; set; }
        public int UnidadId { get; set; }

        [Join("UnidadId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Unidades Unidad { get; set; }
        [AutoGeneratedNoDB]
        public string Nombre{ get; set; }
        [AutoGeneratedNoDB]
        public string Detalles { get; set; }
    }

    public class MovimientoEntradaCustom
    {


        public int ProductoId { get; set; }
        public decimal Cantidad { get; set; }
        public string Referencia { get; set; }
        public int AlmacenId { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public int SuplidorId { get; set; }
        public string CreadoPor {get; set;  }
        public bool Activo { get; set; }
        [AutoGenerated]
        [PrimaryKey]
        public int Id { get; set; }
        public string SuplidorNombre { get; set; }
        public string ProductoNombre { get; set; }
        public string AlmacenNombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int UnidadId { get; set; }
        public string UnidadNombre { get; set; }
 
    }

    public class MovimientoDevolucion : IDatosComunes
    {
       

        public int ProductoId { get; set; }

        public decimal Cantidad { get; set; }
        public string Referencia { get; set; }
        public int AlmacenId { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public int SuplidorId { get; set; }
      
        public int UnidadId { get; set; }

        

        public  string CreadoPor
        {
            get; set;
        }


        public  DateTime FechaModificacion
        {
            get; set;
        }

        public  DateTime FechaCreacion
        {
            get; set;
        }

        public  bool Activo { get; set; }

        public  string ModificadoPor
        {
            get; set;
        }

        [AutoGenerated]
        [PrimaryKey]
        public  int Id { get; set; }

        [Join("SuplidorId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Suplidores Suplidor { get; set; }

        [Join("UnidadId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Unidades Unidad { get; set; }


        [Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Producto Producto { get; set; }

        [Join("AlmacenId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Almacen Almacen { get; set; }


        [AutoGeneratedNoDB]
        public string Nombre { get; set; }
        [AutoGeneratedNoDB]
        public string Detalles { get; set; }


    }
    public class MovimientoDevolucionCliente : IDatosComunes
    {
        [AutoGeneratedNoDB]
        public  string Detalles { get; set; }

        [AutoGeneratedNoDB]
        public  string Nombre { get; set; }

        public int ProductoId { get; set; }

        public decimal Cantidad { get; set; }
        public string Referencia { get; set; }
        public string NCF { get; set; }
        public string TipoNCF { get; set; }
        public string NoFactura { get; set; }
        public Nullable<int> AlmacenId { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public int ClienteId { get; set; }

        public bool Defectuoso { get; set; }

        public Nullable<int> UnidadId { get; set; }
        public string CreadoPor
        {
            get; set;
        }


        public DateTime FechaModificacion
        {
            get; set;
        }

        public DateTime FechaCreacion
        {
            get; set;
        }

        public bool Activo { get; set; }



        [AutoGenerated]
        [PrimaryKey]
        public int Id { get; set; }

        public string ModificadoPor
        {
            get; set;
        }


        [Join("TipoNCF", "Tipo", MyORMV2.Enums.Jointype.LEFT)]
        public ControlNCF ControlNCF { get; set; }

        [Join("ClienteId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Cliente Cliente { get; set; }
        [Join("ProductoId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Producto Producto { get; set; }
        [Join("AlmacenId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Almacen Almacen { get; set; }

    
        
    }

}
