﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
    public class BaseProductoCompuesto 
    {
        public int ProductoCompuestoId { get; set; }
        public int ProductoBaseId { get; set; }
        public int UnidadId { get; set; }

        public decimal Cantidad { get; set; }

        public Producto ProductoBase { get; set; }
        public Producto ProductoCompuesto { get; set; }

        public Unidades Unidad { get; set; }

    }
}
