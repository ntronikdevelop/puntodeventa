﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
   public class AlmacenProductoDefectuoso : AlmacenProducto
    {

        public AlmacenProductoDefectuoso() { }

        public AlmacenProductoDefectuoso(int almacenId, int productoId, decimal cantidad, string creadoPor,bool activo, int unidadId)
        {
            this.AlmacenId = almacenId;
            this.ProductoId = productoId;
            this.Cantidad = cantidad;
            this.FechaCreacion = DateTime.Now;
            this.CreadoPor = creadoPor;
            this.Activo = activo;
            this.UnidadId = unidadId;
        }

    }
}
