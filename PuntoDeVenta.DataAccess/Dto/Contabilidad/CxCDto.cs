﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Contabilidad
{
 public   class CxCDto : DatosComunes
    {
        public CxCDto() { }

        
        
        public List<Factura> Facturas { get; set; }

       
        public List<FacturasPorMoneda> FacturasPorMonedas { get; set; }
        public decimal Total { get; set; }
        public Nullable<int> ClienteId { get; set; }

      
        public Cliente Cliente { get; set; }
    }

    public class FacturasPorMoneda
    {
        public Moneda Moneda { get; set; }
        public string CodigoMoneda { get; set; }

        public List<Factura> Facturas { get; set; }

        public decimal MontoGrabado { get; set; }
        public decimal MontoPorcentajeVendedor { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public decimal MontoPagado { get; set; }
        public decimal MontoAdeudado { get; set; }
        public decimal MontoNotaCredito { get; set; }
        public decimal MontoDescuento { get; set; }
    }

    public class ReporteRecibosDto : DatosComunes
    {
        public ReporteRecibosDto() { }



        public List<Pago> Pagos { get; set; }


        public List<RecibosPorMoneda> RecibosPorMoneda { get; set; }
        public decimal Total { get; set; }
        public Nullable<int> ClienteId { get; set; }


        public Cliente Cliente { get; set; }
    }

    public class RecibosPorMoneda
    {
        public Moneda Moneda { get; set; }
        public string CodigoMoneda { get; set; }

        public List<Pago> Pagos { get; set; }

        public decimal MontoGrabado { get; set; }
        public decimal MontoPorcentajeVendedor { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public decimal MontoPagado { get; set; }
        public decimal MontoAdeudado { get; set; }
    }

    public class CxPDto : DatosComunes
    {
        public CxPDto() { }

       
       
        public List<GastoEmpresa> Gastos { get; set; }

       
        public List<GastosPorMoneda> GastosPorMonedas { get; set; }
        public decimal Total { get; set; }
        public Nullable<int> SuplidorId { get; set; }

     
        public Suplidores Suplidor { get; set; }
    }

    public class GastosPorMoneda
    {
        public Moneda Moneda { get; set; }
        public string CodigoMoneda { get; set; }

        public List<GastoEmpresa> Gastos { get; set; }

        public decimal MontoGrabado { get; set; }
        public decimal MontoItbis { get; set; }
        public decimal MontoTotal { get; set; }
        public decimal MontoPagado { get; set; }
        public decimal MontoAdeudado { get; set; }
        public decimal MontoVendedor { get; set; }
    }

    public class EstadoCuentaCliente
    {
        public string Referencia { get; set; }
        public string Concepto { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public int MonedaId { get; set; }
        public string MonedaNombre { get; set; }
        public decimal TasaConversion { get; set; }
        public decimal MontoAdeudado { get; set; }
        public string Vendedor { get; set; }
        public decimal PorcentajeVendedor { get; set; }
        public int CantidadDias
        {
            get
            {
                return Convert.ToInt32((DateTime.Now - this.Fecha).TotalDays);
            }
        }
    }
    public class DetalleComision
    {
        public string CodigoMoneda { get; set; }
        public string Vendedor { get; set; }
        public string Referencia { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoTotalFactura { get; set; }
        public decimal MontoGrabadoFactura { get; set; }
        public decimal MontoPagado { get; set; }
        public string Cliente { get; set; }
        public int CantidadDias
        {
            get
            {
                return Convert.ToInt32((DateTime.Now - this.Fecha).TotalDays);
            }
        }
    }

    public class ComisionesPorMoneda
    {
        public string CodigoMoneda { get; set; }

        public decimal MontoTotal { get; set; }
        public decimal MontoGrabado { get; set; }
        public string Moneda { get; set; }
        public List<DetalleComision> Detalles { get; set; }
    }

    public class ReporteComisiones
    {
        public List<ComisionesPorMoneda> ComisionesPorMonedas { get; set; }

    }

    public class ResultadoEstadoCuenta
    {
        public EstadoCuentaCliente Factura { get; set; }

        public List<EstadoCuentaCliente> Pagos { get; set; }
        public decimal MontoComprado { get; set; }
        public decimal PorcentajeVendedor { get; set; }
        public decimal MontoPagado { get; set; }
        public decimal MontoAdeudado { get { return Math.Abs( MontoComprado) -Math.Abs( MontoPagado); } }
    }

    public class ResultadoReporteImpuestos
    {
        public Moneda Moneda { get; set; }
        public List<FacturaImpuesto> ImpuestosVentas { get; set; }
        public List<FacturaImpuesto> ImpuestosPagos { get; set; }
        public decimal MontoTotalVentas { get; set; }
        public decimal MontoTotalPagos { get; set; }
        
    }
    
}
