﻿using PuntoDeVenta.DataAccess.Dto.Interfaces;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto
{
  public  class Operaciones 
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreIlustrativo { get; set; }
    }

    public class Secciones 
    {
        public int Id { get; set; }
        
        public string Nombre { get; set; }
    }

    public class SeccionOperaciones 
    {
        public int Id { get; set; }
        public int SeccionId  { get; set; }
        public int OperacionId { get; set; }
        [Join("SeccionId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Secciones Seccion { get; set; }
        [Join("OperacionId", "Id", MyORMV2.Enums.Jointype.LEFT)]
        public Operaciones Operacion { get; set; }
    }
}
