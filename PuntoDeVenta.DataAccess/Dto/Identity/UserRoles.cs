﻿using MyORMV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Identity
{
   public class UserRoles
    {
     public bool Activo { get; set; }
        public int RoleId { get; set; }
        public string UserId { get; set; }
    }
}
