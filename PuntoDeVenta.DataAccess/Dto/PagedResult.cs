﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto
{
   public class PagedResult<T> where T:class
    {
        public int Count { get; set; }
        public List<T> Result { get; set; }
        public int  CurrentPage { get; set; }
    }
}
