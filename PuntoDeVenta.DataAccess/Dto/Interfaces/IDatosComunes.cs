﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Interfaces
{
  public  interface IDatosComunes
    {
        string CreadoPor { get; set; }
        string Nombre { get; set; }

        string ModificadoPor { get; set; }
        DateTime FechaCreacion { get; set; }

        DateTime FechaModificacion { get; set; }
        string Detalles { get; set; }

        int Id { get; set; }

        bool Activo { get; set; }
    }

    public interface IIdentityDatosComunes
    {
       

        string Id { get; set; }


    }
}
