﻿using PuntoDeVenta.DataAccess.Dto.Interfaces;
using System;
using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;
using System.Runtime.Caching;

namespace PuntoDeVenta.DataAccess.Dto.Helpers
{
    public class DtoHelper
    {
        public static IDatosComunes LlenarCamposPorDefectoCreacion(IDatosComunes dto)
        {
            
            dto.CreadoPor = GetUserName();
            dto.FechaCreacion = DateTime.Now;
            dto.Activo = true;
            return dto;
        }

        public static IDatosComunes LlenarCamposPorDefectoEdicion(IDatosComunes dto)
        {
            dto.ModificadoPor = GetUserName();
            dto.FechaModificacion = DateTime.Now;

            return dto;
        }

        public static string GetUserName()
        {

            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (userId != null)
            {
                var user =  (MemoryCache.Default.Get(userId) as IUserData);
                if (user != null)
                    return $"{user.Nombre} {user.Apellido}";
                else
                    return "";
            }
            else
                return "";
        }
    }
}
