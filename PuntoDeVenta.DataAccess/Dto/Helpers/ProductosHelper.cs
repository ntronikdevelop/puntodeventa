﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Helpers
{
  public  class ProductosHelper
    {
        public static decimal ConvertirAUnidadPrincipalProducto(decimal cantidad,int unidadId, List<ProductoUnidadesEquivalencia> unidades)
        {
            var unidadActual = unidades.Where(u => u.UnidadId == unidadId).FirstOrDefault();
            if (unidadActual == null) throw new Exception("El producto no contiene dicha unidad.");
          var  unidadPadre = unidades.Where(u => u.EsPrincipal).FirstOrDefault();
            if (unidadPadre == null) throw new Exception("El producto no contiene unidad principal.");

            decimal equivalencia = (cantidad * unidadPadre.Equivalencia) / unidadActual.Equivalencia;

            return equivalencia;
        }

        public static decimal ConvertirAUnidadDesdePrincipalProducto(decimal cantidad, int unidadId, List<ProductoUnidadesEquivalencia> unidades)
        {
            var unidadActual = unidades.Where(u => u.UnidadId == unidadId).FirstOrDefault();
            if (unidadActual == null) throw new Exception("El producto no contiene dicha unidad.");

            decimal equivalencia = (cantidad * unidadActual.Equivalencia);

            return equivalencia;
        }
    }
}
