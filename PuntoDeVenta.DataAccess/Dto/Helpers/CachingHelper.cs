﻿using System; using MyORMV2;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using System.Linq.Expressions;

namespace PuntoDeVenta.DataAccess.Dto.Helpers
{
    public static class CachingHelper
    {
        //public static Dictionary<string, List<IDatosComunes>> ListasEnCache { get; set; } = new Dictionary<string, List<IDatosComunes>>();
        public static void InsertarDtoEnCache<T>(T obj) where T : class, IDatosComunes
        {

            var tipo = typeof(T);
            List<T> listaEnCache = (MemoryCache.Default.Get($"CacheItem-{tipo.FullName}") ?? new object()) as List<T>;

            if (listaEnCache == null || listaEnCache.Count == 0)
            {
                listaEnCache = new List<T>() { obj };
                MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));

            }
            else
            {
                if (!listaEnCache.Exists(x => x.Id == obj.Id))
                {
                    listaEnCache.Add(obj);
                    MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));

                }

            }
        }

        public static void ActualizarDtoEnCache<T>(T obj) where T : class, IDatosComunes
        {

            var tipo = typeof(T);
            List<T> listaEnCache = (MemoryCache.Default.Get($"CacheItem-{tipo.FullName}") ?? new object()) as List<T>;
            if (listaEnCache == null || listaEnCache.Count == 0)
            {
                listaEnCache = new List<T>() { obj };
                MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));
            }
            else
            {
                int index = listaEnCache.FindIndex(r => r.Id == obj.Id);
                if (index >= 0)
                {
                    listaEnCache[index] = obj;
                }
                else
                {
                    listaEnCache.Add(obj);
                }
                MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));

            }
        }
        public static void InsertarListaDtoEnCache<T>(List<T> obj) where T : class, IDatosComunes
        {

            var tipo = typeof(T);
            List<T> listaEnCache = (MemoryCache.Default.Get($"CacheItem-{tipo.FullName}") ?? new object()) as List<T>;
            if (listaEnCache == null)
            {
                listaEnCache = new List<T>(obj);
                MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));
            }
            else
            {
                obj.ForEach(x =>
                {
                    int index = listaEnCache.FindIndex(y => y.Id == x.Id);
                    if (index >= 0)
                    {
                        listaEnCache.RemoveAt(index);
                    }
                });


                listaEnCache.AddRange(obj);
                MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));


            }
        }

        public static T ObtenerDtoDeCache<T>(int id) where T : class, IDatosComunes
        {
            if (id > 0)
            {
                var tipo = typeof(T);
                List<T> listaEnCache = (MemoryCache.Default.Get($"CacheItem-{tipo.FullName}") ?? new object()) as List<T>;
                if (listaEnCache != null && listaEnCache.Count > 0)
                {
                    T objEnLista = listaEnCache.Where(x => x.Id == id).FirstOrDefault() as T;
                    if (objEnLista != null)
                        return objEnLista;
                    else
                    {
                        IBaseDb<T> service = new BaseManager<T>();
                        var objEnDb = service.ObtenerPorId(id);
                        if (objEnDb != null)
                        {
                            listaEnCache.Add(objEnDb);
                            MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));
                        }

                        return objEnDb;
                    }
                }
                else
                {
                    IBaseDb<T> service = new BaseManager<T>();
                    var objEnDb = service.ObtenerPorId(id);
                    listaEnCache = objEnDb != null ? new List<T>() { objEnDb } : new List<T>();

                    MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));


                    return objEnDb;
                }
            }
            return null;

        }

     





        public static void EliminarDtoDeCache<T>(int id) where T : class, IDatosComunes
        {
            var tipo = typeof(T);
            List<T> listaEnCache = (MemoryCache.Default.Get($"CacheItem-{tipo.FullName}") ?? new object()) as List<T>;
            if (listaEnCache != null)
            {
                int index = listaEnCache.FindIndex(x => x.Id == id);
                if (index >= 0)
                {


                    listaEnCache.RemoveAt(index);
                    MemoryCache.Default.Set($"CacheItem-{tipo.FullName}", listaEnCache, DateTime.Now.AddMinutes(60));

                }

            }
        }

    }


}
