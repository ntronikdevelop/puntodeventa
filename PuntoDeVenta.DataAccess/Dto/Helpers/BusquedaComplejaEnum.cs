﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Dto.Helpers
{
    public enum BusquedaComplejaEnum
    {
        Like = 0,
        EQUALSTRING = 3,
        EQUALDATE = 2,
        EQUAL = 1,
        GREATEROREQUAL = 4,
        LESSOREQUAL = 5,
        GREATER = 6,
        LESS = 7,
        ISNULL = 8,
        GREATEROREQUALDATE = 9,
        LESSOREQUALDATE = 10
    }
}
