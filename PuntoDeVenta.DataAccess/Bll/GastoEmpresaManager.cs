﻿using System;
using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.DataAccess.Bll
{
 public   class GastoEmpresaManager : BaseManager<GastoEmpresa>
    {
        public List<GastoEmpresa> ObtenerCxPGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> suplidorId, Nullable<int> monedaId, Nullable<int> localidadId)
        {
            return new GastoEmpresaDao().ObtenerCxPGeneral(FechaInicio,fechaFinal, suplidorId,  monedaId,  localidadId);
        }

    }
}
