﻿using System;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.DataAccess.Bll
{
    public class TransferenciaAlmacenManager : BaseManager<TransferenciaAlmacen>
    {

        public override PagedResult<TransferenciaAlmacen> ObtenerListaPaginado(int page, int size, bool activos)
        {
            using (var dbContext = InicializarDb())
            {
                PagedResult<TransferenciaAlmacen> result = new PagedResult<TransferenciaAlmacen>();

                Type tipo = typeof(TransferenciaAlmacen);
                result.Result = base.ObtenerListaPorConsulta(
                    string.Format("select * from {0} where Activo={3} group by Referencia limit {1},{2} ",
                    tipo.Name, ((page - 1) * size), size, Convert.ToInt16(activos)));
                result.Count = dbContext.GetTableCountGrouped<TransferenciaAlmacen>("select Count(*) from transferenciaalmacen group by Referencia");
                return result;
            }
        }
    }
}
