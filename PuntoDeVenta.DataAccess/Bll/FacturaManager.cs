﻿using System;
using System.Collections.Generic;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Bll.Interfaces;

namespace PuntoDeVenta.DataAccess.Bll
{
    public class FacturaManager : BaseManager<Factura>, IFacturaManager
    {
        FacturaDao facturaDb = new FacturaDao();
        public List<Factura> ObtenerCXC(DateTime? FechaInicio, DateTime? fechaFinal, int? idCliente, int? monedaId, int? vendedorId)
        {
            return facturaDb.ObtenerCXC(FechaInicio, fechaFinal, idCliente, monedaId,vendedorId);
        }

        public Factura ObtenerPorNumeroFactura(string numeroFactura)
        {
            return facturaDb.ObtenerPorNumeroFactura(numeroFactura);
        }
    }
}
