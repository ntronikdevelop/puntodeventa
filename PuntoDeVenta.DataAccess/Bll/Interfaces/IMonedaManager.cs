﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Bll.Interfaces
{
public    interface IMonedaManager : IBaseManager<Moneda>
    {
    }
}
