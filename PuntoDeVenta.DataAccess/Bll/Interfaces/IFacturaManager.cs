﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Bll.Interfaces
{
public    interface IFacturaManager : IBaseManager<Factura>
    {
        List<Factura> ObtenerCXC(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, int? vendedorId);
    }
}
