﻿using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Bll.Interfaces
{
public    interface IAlmacenProductoManager : IBaseDb<AlmacenProducto>
    {
        List<AlmacenProducto> ObtenerExistenciaDeProducto(int productoId);
        List<AlmacenProducto> ObtenerExistenciaDeProductoEnAlmacen(int productoId, int almacenId);
        List<AlmacenProducto> ObtenerProductosDeAlmacen(int id);
    }
}
