﻿using PuntoDeVenta.DataAccess.Dao.Interfaces;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Bll.Interfaces
{
 public   interface IBaseManager<T>: IBaseDb<T> where T:class
    {

    }
}
