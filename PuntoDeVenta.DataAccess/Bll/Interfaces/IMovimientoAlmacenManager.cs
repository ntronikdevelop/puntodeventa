﻿using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System; using MyORMV2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.DataAccess.Bll.Interfaces
{
public    interface IMovimientoAlmacenManager : IBaseDb<MovimientoAlmacen>
    {
        List<MovimientoAlmacen> ObtenerHistorialDeProducto(int productoId);

        MovimientoAlmacen ObtenerExistenciaProducto(int productoId, int almacenId);

        List<MovimientoProductoModel> ObtenerMovimientosPorProducto(int? productoId, int? almacenId, string fechaInicio, string fechaFinal);
    }
}
