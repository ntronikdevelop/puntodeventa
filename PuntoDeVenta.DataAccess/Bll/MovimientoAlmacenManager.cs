﻿using System.Collections.Generic;
using System.Linq;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.DataAccess.Bll
{
    public class MovimientoAlmacenManager : MovimientoAlmacenDao, IMovimientoAlmacenManager
    {
        public MovimientoAlmacen ObtenerExistenciaProducto(int productoId, int almacenId)
        {
            ProductosUnidadesManager productos = new ProductosUnidadesManager();
            var unidadesProducto = productos.ObtenerUnidadesDeProducto( productoId );
            List<MovimientoAlmacen> resultList = new List<MovimientoAlmacen>();
            var lista = base.ObtenerMovimientosPorProducto(productoId ,almacenId ,null,null).GroupBy(x=>x.UnidadId).ToList();
            foreach (var unidad in lista)
            {
                MovimientoAlmacen unidadResumida = new MovimientoAlmacen(
                    almacenId,
                    productoId,
                    unidad.Sum(x => x.Cantidad),
                    "",
                    true,
                    unidad.Key,
                    0,
                    "",
                    "");

                unidadResumida.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(unidadResumida.Cantidad, unidadResumida.UnidadId, unidadesProducto);
                resultList.Add(unidadResumida);
            }
            var result = new MovimientoAlmacen(
                almacenId, 
                productoId,
                resultList.Sum(x => x.Cantidad),
                "",
                true,
                unidadesProducto.Where(x => x.Activo).FirstOrDefault().UnidadId,
                0,
                "",
                "");

            return result;
        }

       
        
    }
}
