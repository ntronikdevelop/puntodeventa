﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Constants.Lib
{
    public enum EstadosFactura
    {
        Pagado='P',
        Facturado='F',
        Anulado='A',
        Cotizado='C',
        Convertido='D'
    }

    public enum EstadosGastos 
    {
    Generado='G',
    Pagado='P',
    Anulado='A'
    }

    public enum TiposFiltros
    {
        ContieneTexto = 0,
        IgualNumero = 1,
        DiaCompleto = 2,
        IgualTexto = 3,
        MayorIgual = 4,
        MenorIgual = 5,
        Mayor = 6,
        Menor = 7,
        EsNulo = 8,
        MayorIgualNoNumero = 9,
        MenorIgualNoNumero = 10,
        DiferenteTexto = 11,
        EnLista = 12,
        NoEnLista = 13,
        EnListaTexto = 14,
        NoEnListaTexto = 15
    }

}
