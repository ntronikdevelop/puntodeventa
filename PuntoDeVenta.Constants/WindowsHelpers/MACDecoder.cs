﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Constants.WindowsHelpers
{
  public  class MACDecoder
    {
        public static string ObtenerMACAddress()
        {
            string result = string.Empty;
            var macAddr =
    (
        from nic in NetworkInterface.GetAllNetworkInterfaces()
        select nic.GetPhysicalAddress().ToString()
    ).ToArray();
            result = string.Join("-", macAddr.FirstOrDefault());
            return result;
        }
    }
}
