﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dao;

namespace PuntoDeVenta.Service
{
    public class TransferenciaAlmacenService : BaseService<TransferenciaAlmacen>, ITransferenciaAlmacenService
    {
        private IClienteService clienteService;
        private IAlmacenService almacenService;
        private ISuplidoresService suplidorService;
        private IProductoService productoService;
        private AlmacenProductoDao almacenProductoService;
        private IUnidadesService unidades;
        private TransferenciaAlmacenDao _dbContext;
        private IMovimientoAlmacenService logMovimientos;
        private IProductosUnidadesService productosUnidades;

        public TransferenciaAlmacenService() : base(new TransferenciaAlmacenManager())
        {
            this.clienteService = new ClienteService();
            this.almacenService = new AlmacenService();
            this.suplidorService = new SuplidoresService();
            this.productoService = new ProductoService();
            this.almacenProductoService = new AlmacenProductoDao();
            this.unidades = new UnidadesService();
            this.logMovimientos = new MovimientoAlmacenService();
            this.productosUnidades = new ProductoUnidadesService();
            _dbContext = new TransferenciaAlmacenDao();


        }
        public override PagedResult<TransferenciaAlmacen> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<TransferenciaAlmacen> resultadoPaginacion =
                _dbContext.ObtenerListaPaginado(page, size, activos);

   
            return resultadoPaginacion;
        }

        public override TransferenciaAlmacen ObtenerPorId(int id)
        {
            TransferenciaAlmacen resultado = base.ObtenerPorId(id);
                      
            return resultado;

        }

        public override TransferenciaAlmacen Insertar(TransferenciaAlmacen obj)
        {
            obj.Producto.ProductoUnidades = obj.Producto.ProductoUnidades == null || obj.Producto.ProductoUnidades.Count == 0 ? productosUnidades.ObtenerUnidadesDeProducto(obj.ProductoId) : obj.Producto.ProductoUnidades;
            obj.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
                    obj.Cantidad,
                    obj.UnidadId,
                    obj.Producto.ProductoUnidades
                    );

            TransferenciaAlmacen movimiento = null;
            Task<AlmacenProducto> t_almacenOrigen = Task.Factory.StartNew<AlmacenProducto>((arg) =>
            {
                TransferenciaAlmacen datos = arg as TransferenciaAlmacen;
                AlmacenProducto almacenOrigen = almacenProductoService.ObtenerExistenciaDeProductoEnAlmacen(datos.AlmacenOrigenId, datos.ProductoId).FirstOrDefault();
                return almacenOrigen;

            }, obj);

            Task<AlmacenProducto> t_almacenDestino = Task.Factory.StartNew<AlmacenProducto>((arg) =>
            {
                TransferenciaAlmacen datos = arg as TransferenciaAlmacen;
                AlmacenProducto almacen = almacenProductoService.ObtenerExistenciaDeProductoEnAlmacen(datos.AlmacenDestinoId, datos.ProductoId).FirstOrDefault();
                return almacen;

            }, obj);


            var existenciaOrigenActual = t_almacenOrigen.Result??null;
            var existenciaDestinoActual = t_almacenDestino.Result??null;



            if (existenciaOrigenActual != null)
            {
                existenciaOrigenActual.Producto.ProductoUnidades= existenciaOrigenActual.Producto.ProductoUnidades == null || existenciaOrigenActual.Producto.ProductoUnidades.Count == 0 ? 
                    productosUnidades.ObtenerUnidadesDeProducto(existenciaOrigenActual.ProductoId) : existenciaOrigenActual.Producto.ProductoUnidades;
                existenciaOrigenActual.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
                    existenciaOrigenActual.Cantidad,
                   existenciaOrigenActual.Producto.ProductoUnidades.Where(u=>u.EsPrincipal).FirstOrDefault().UnidadId,
                    existenciaOrigenActual.Producto.ProductoUnidades
                    );

                
                if (existenciaOrigenActual.Cantidad < obj.Cantidad)
                    throw new Exception("La cantidad existente del producto en el almacen de origen es menor que la cantidad especificada. Operación abortada.");

                
                    var unidadesOrigen = existenciaOrigenActual.Producto.ProductoUnidades;
                    decimal cantidadPrincipalOrigen = existenciaOrigenActual.Cantidad;
                    MovimientoAlmacen movimientoAlmacenOrigen = new MovimientoAlmacen(existenciaOrigenActual.AlmacenId, obj.ProductoId, obj.Cantidad * -1, obj.CreadoPor, true, unidadesOrigen.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "OUT",obj.Referencia??string.Empty);
                    logMovimientos.Insertar(movimientoAlmacenOrigen);

                

                if (existenciaDestinoActual != null)
                {
                    existenciaDestinoActual.Producto.ProductoUnidades = existenciaDestinoActual.Producto.ProductoUnidades == null || existenciaDestinoActual.Producto.ProductoUnidades.Count == 0 ?
                   productosUnidades.ObtenerUnidadesDeProducto(existenciaDestinoActual.ProductoId) : existenciaDestinoActual.Producto.ProductoUnidades;

                    existenciaDestinoActual.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
                    existenciaDestinoActual.Cantidad,
                    existenciaOrigenActual.Producto.ProductoUnidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId,
                    existenciaDestinoActual.Producto.ProductoUnidades
                    );


                    existenciaDestinoActual.Cantidad += obj.Cantidad;
                    existenciaDestinoActual.ModificadoPor = obj.CreadoPor;
                    existenciaDestinoActual.FechaModificacion = DateTime.Now;

                    var unidadesDestino = existenciaDestinoActual.Producto.ProductoUnidades;
                    decimal cantidadPrincipalDestino = existenciaDestinoActual.Cantidad;
                    MovimientoAlmacen movimientoAlmacenDestino = new MovimientoAlmacen(existenciaDestinoActual.AlmacenId, obj.ProductoId, obj.Cantidad , obj.CreadoPor, true, unidadesDestino.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "IN", obj.Referencia??string.Empty);
                    logMovimientos.Insertar(movimientoAlmacenDestino);

                    almacenProductoService.Actualizar(existenciaDestinoActual);
                }
                else
                {
                    AlmacenProducto aInsertar = new AlmacenProducto(
                        obj.AlmacenDestinoId,
                        obj.ProductoId,
                        obj.Cantidad,
                        obj.CreadoPor, 
                        obj.Activo,
                        obj.Producto.ProductoUnidades.Where(x=>x.EsPrincipal).FirstOrDefault().UnidadId);

                   
                    MovimientoAlmacen movimientoAlmacenDestino = new MovimientoAlmacen(aInsertar.AlmacenId, aInsertar.ProductoId, aInsertar.Cantidad, obj.CreadoPor, true, aInsertar.UnidadId, 0, "IN", obj.Referencia??"");
                    logMovimientos.Insertar(movimientoAlmacenDestino);
                    almacenProductoService.Insertar(aInsertar);
                }

                existenciaOrigenActual.Cantidad -= obj.Cantidad;
                existenciaOrigenActual.ModificadoPor = obj.CreadoPor;
                existenciaOrigenActual.FechaModificacion = DateTime.Now;
                almacenProductoService.Actualizar(existenciaOrigenActual);
                obj.Cantidad = ProductosHelper.ConvertirAUnidadDesdePrincipalProducto(obj.Cantidad, obj.UnidadId, obj.Producto.ProductoUnidades);
                movimiento = base.Insertar(obj);
            }
            else
                throw new Exception("El producto no existe en el almacen de origen. Operación abortada");

            if (movimiento == null)
                throw new Exception("Error al insertar este producto en el almacen seleccionado. Favor contactar el administrador");
            return movimiento;
        }

        public override bool Actualizar(TransferenciaAlmacen obj)
        {
 
                throw new Exception("Hubo un problema al actualizar la existencia de este producto en el almacen elegido. Favor contactar el administrador");


        }

       
    }
}
