﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public class MovimientoEntradaService : BaseService<MovimientoEntrada>, IMovimientoEntradaService
    {
        private IClienteService clienteService;
        private MovimientoEntradaDao privateService;
        private IAlmacenService almacenService;
        private ISuplidoresService suplidorService;
        private IProductoService productoService;
        private IUnidadesService unidades;
        private IAlmacenProductoService almacenProductoService;
        private IMovimientoAlmacenService logMovimientos;

        public MovimientoEntradaService() : base(new MovimientoEntradaManager())
        {
            this.clienteService = new ClienteService();
            this.almacenService = new AlmacenService();
            this.suplidorService = new SuplidoresService();
            this.productoService = new ProductoService();
            this.privateService = new MovimientoEntradaDao();
            this.almacenProductoService = new AlmacenProductoService();
            this.unidades = new UnidadesService();
            this.logMovimientos = new MovimientoAlmacenService();
        }
        public override PagedResult<MovimientoEntrada> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<MovimientoEntrada> resultadoPaginacion = privateService.ObtenerListaPaginado(page, size, activos);

            return resultadoPaginacion;
        }

      
        public override MovimientoEntrada ObtenerPorId(int id)
        {
            MovimientoEntrada resultado = base.ObtenerPorId(id);
         
           
            return resultado;

        }

        public override MovimientoEntrada Insertar(MovimientoEntrada obj)
        {
            obj.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
                   obj.Cantidad,
                   obj.UnidadId,
                   obj.Producto.ProductoUnidades
                   );


            MovimientoEntrada movimiento = null;
            var existenciaActual = almacenProductoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "AlmacenId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.AlmacenId}" },
                 new FiltroBusqueda() { Campo = "ProductoId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.ProductoId}" }}
            }).Result.FirstOrDefault();

            AlmacenProducto existenciaEnAlmacen = new AlmacenProducto(obj.AlmacenId, obj.ProductoId, obj.Cantidad, obj.CreadoPor, obj.Activo, obj.Producto.ProductoUnidades.FirstOrDefault(x=>x.EsPrincipal).UnidadId);

            if (existenciaActual != null)
            {
                existenciaActual.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
       existenciaActual.Cantidad,
       existenciaActual.UnidadId,
       obj.Producto.ProductoUnidades
       );


                existenciaEnAlmacen.Cantidad += existenciaActual.Cantidad;
                existenciaEnAlmacen.Id = existenciaActual.Id;
                existenciaEnAlmacen.ModificadoPor = existenciaActual.ModificadoPor ?? "";
                existenciaEnAlmacen.FechaModificacion = (existenciaActual.FechaModificacion == null || existenciaActual.FechaModificacion== DateTime.MinValue) ? DateTime.Now : existenciaActual.FechaModificacion;
                bool resultado = almacenProductoService.Actualizar(existenciaEnAlmacen);
            }
            else
            {
                var result = almacenProductoService.Insertar(existenciaEnAlmacen);
            }

            if (!obj.Producto.EsServicio)
            {
                var unidades = obj.Producto.ProductoUnidades;
               // decimal cantidadPrincipal = ProductosHelper.ConvertirAUnidadPrincipalProducto(obj.Cantidad, devolucionProducto.UnidadId.Value, unidades);
                MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(obj.AlmacenId, obj.ProductoId, obj.Cantidad, obj.CreadoPor, true, unidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "IN", obj.Referencia??string.Empty);
                logMovimientos.Insertar(movimientoAlmacen);

            }
            movimiento = base.Insertar(obj);
            var producto = productoService.ObtenerPorId(obj.ProductoId);
            producto.Existencia += obj.Cantidad;
            productoService.Actualizar(producto);
            if (movimiento == null)
                throw new Exception("Error al insertar este producto en el almacen seleccionado. Favor contactar el administrador");
            return movimiento;
        }

        public override bool Actualizar(MovimientoEntrada obj)
        {
            throw new NotImplementedException("Acción no implementada por el momento.");

  

        }
    }
}
