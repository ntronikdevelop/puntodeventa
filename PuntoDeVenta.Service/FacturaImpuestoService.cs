﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class FacturaImpuestoService : BaseService<FacturaImpuesto>, IFacturaImpuestoService
    {
        public FacturaImpuestoService():base(new FacturaImpuestoManager()) { }

        public List<FacturaImpuesto> ObtenerImpuestosDeFactura(string NoFactura)
        {
            return this.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo=true,
                Filtros=new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="NoFactura",
                        TipoCampo=3,
                        Valor=NoFactura
                    }
                }
            }).Result;
        }

        public List<FacturaImpuesto> ObtenerImpuestosDeFactura(int facturaId)
        {
            return this.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="FacturaId",
                        TipoCampo=1,
                        Valor=facturaId.ToString()
                    }
                }
            }).Result;
        }
    }
}
