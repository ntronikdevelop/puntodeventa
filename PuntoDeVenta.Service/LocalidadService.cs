﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.Service
{
    public class LocalidadService : BaseService<Localidad>, ILocalidadService
    {
        public LocalidadService()
        {
            this.almacenService = new AlmacenService();
        }

        public LocalidadService(IAlmacenService newAlmacenService) 
        {
            this.almacenService = newAlmacenService;
        }

        private IAlmacenService almacenService;
        public override PagedResult<Localidad> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Localidad> resultadoPaginacion = base.ObtenerTodosPaginado(page, size, activos);

            return resultadoPaginacion;
        }

        public override PagedResult<Localidad> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            PagedResult<Localidad> resultadoPaginacion = base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);

            resultadoPaginacion.Result.ForEach(x =>
            {

                x.Almacenes = almacenService.ObtenerAlmacenesPorLocalidad( x.Id);
            });

            return resultadoPaginacion;
        }

        public override Localidad ObtenerPorId(int id)
        {
            Localidad resultado = base.ObtenerPorId(id);

            resultado.Almacenes = almacenService.ObtenerAlmacenesPorLocalidad( resultado.Id);
      
            return resultado;

        }

        public  Localidad ObtenerBasico(int id)
        {
            Localidad resultado = base.ObtenerPorId(id);
            return resultado;

        }
        private Localidad ObtenerConAlmacenes(int localidadId)
        {
            Localidad resultado = base.ObtenerPorId(localidadId);

            resultado.Almacenes = almacenService.ObtenerAlmacenesPorLocalidadConProductos(resultado.Id);

            return resultado;
        }
        public List<Localidad> ObtenerExistenciaEnAlmacenes(Nullable<int> localidadId)
        {
            List<Localidad> resultado = new List<Localidad>();
            if (localidadId.HasValue)
                resultado.Add(this.ObtenerConAlmacenes(localidadId.Value));
            else
                resultado.AddRange(this.ObtenerTodosConProductos());
            return resultado;
        }

        public override List<Localidad> ObtenerTodos()
        {
            var resultado= base.ObtenerTodos();
           
            resultado.ForEach(x => {
               
                    x.Almacenes = almacenService.ObtenerAlmacenesPorLocalidad( x.Id);
               
            });
            return resultado;
        }

        public  List<Localidad> ObtenerTodosBasicos()
        {
            var resultado = base.ObtenerTodos();
            return resultado;
        }
        public  List<Localidad> ObtenerTodosConProductos()
        {
            var resultado = base.ObtenerTodos();

            resultado.ForEach(x => {

                x.Almacenes = almacenService.ObtenerAlmacenesPorLocalidadConProductos( x.Id);

            });
            return resultado;
        }

    }
}
