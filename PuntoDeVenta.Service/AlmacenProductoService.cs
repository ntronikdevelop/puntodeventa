﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.Service
{
    public class AlmacenProductoService : BaseService<AlmacenProducto>, IAlmacenProductoService
    {
        private IProductoService productoService;
        private IAlmacenProductoManager internalService;
        private IUnidadesService unidadesService;
        private IBaseDb<Localidad> localidades;
        private IProductosUnidadesService productoUnidades;
        public AlmacenProductoService() : base(new AlmacenProductoManager())
        {
            this.productoService = new ProductoService();
            this.internalService = new AlmacenProductoManager();
            this.unidadesService = new UnidadesService();
            this.localidades = new LocalidadManager();
            this.productoUnidades = new ProductoUnidadesService();
        }

        public override PagedResult<AlmacenProducto> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            var result= base.ObtenerTodosPaginado(page, size, activos);
        

            return result;
        }

        public override AlmacenProducto ObtenerPorId(int id)
        {
            var result= base.ObtenerPorId(id);
             return result;
        }

        public List<AlmacenProducto> ObtenerExistenciaDeProducto(int productoId)
        {
            
            var result = internalService.ObtenerExistenciaDeProducto(productoId);

            return result;
        }


        public List<AlmacenProducto> ObtenerExistenciaDeProductoEnAlmacen(int productoId, int almacenId)
        {
            var result = internalService.ObtenerExistenciaDeProductoEnAlmacen(almacenId, productoId);

            return result;
        }


        public List<AlmacenProducto> ObtenerProductosDeAlmacen(int id)
        {
            var result = internalService.ObtenerProductosDeAlmacen(id);

            return result;
        }
    }
}
