﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.Constants.Lib;
using System.Transactions;

namespace PuntoDeVenta.Service
{
    public class GastoAgendadoService : BaseService<GastoAgendado>, IGastoAgendadoService
    {
        private ISuplidoresService suplidoresService;
        private IMonedaService monedaService;
        private ILocalidadService localidadService;
        private IGastoImpuestoService gastoImpuestoService;
        private GastoEmpresaManager manager = new GastoEmpresaManager();

        public GastoAgendadoService()
        {
            this.gastoImpuestoService = new GastoImpuestoService();
            this.monedaService = new MonedaService();
            this.suplidoresService = new SuplidoresService();
            this.localidadService = new LocalidadService();
        }
        public CxPDto ObtenerCxPGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> suplidorId, Nullable<int> monedaId, Nullable<int> localidadId)
        {
            CxPDto resultado = new CxPDto();

            resultado.Gastos = manager.ObtenerCxPGeneral(FechaInicio, fechaFinal, suplidorId, monedaId, localidadId);

            resultado.Total = resultado.Gastos.Sum(f => f.MontoTotal);
            resultado.GastosPorMonedas = new List<GastosPorMoneda>();
            var GastoEmpresasPorMoneda = resultado.Gastos != null ? resultado.Gastos.OrderByDescending(f => f.FechaEmision).GroupBy(x => x.MonedaId).ToList() : null;
            if (GastoEmpresasPorMoneda != null)
            {
                GastoEmpresasPorMoneda.ForEach(fm =>
                {
                    resultado.GastosPorMonedas.Add(new GastosPorMoneda()
                    {
                        Moneda = fm.FirstOrDefault().Moneda,
                        CodigoMoneda = fm.FirstOrDefault().Moneda.Codigo,
                        Gastos = fm.ToList(),
                        MontoGrabado = fm.Sum(x => x.MontoGrabado),
                        MontoItbis = fm.Sum(x => x.MontoItbis),
                        MontoTotal = fm.Sum(x => x.MontoTotal),
                        MontoPagado = fm.Sum(x => x.MontoPagado),
                        MontoAdeudado = fm.Sum(x => x.MontoAdeudado),

                    });
                });
            }

            return resultado;
        }





        public override GastoAgendado Insertar(GastoAgendado obj)
        {


            obj.ReferenciaGasto = string.IsNullOrEmpty(obj.ReferenciaGasto) ? SecuenciasHelper.CrearControlDeGastosAgendados() : obj.ReferenciaGasto;
            obj.FechaEmision = obj.FechaEmision == DateTime.MinValue ? DateTime.Now : obj.FechaEmision;
            obj.Estado = (char)EstadosGastos.Generado;
            obj.Dia = Convert.ToByte(obj.FechaEmision.Day);
            var gasto = base.Insertar(obj);
            GastoHelper.ActualizarImpuestosDeGasto(obj, gastoImpuestoService);

            return gasto;
        }





        public override PagedResult<GastoAgendado> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<GastoAgendado> resultadoPaginacion = base.ObtenerTodosPaginado(page, size, activos);


            return resultadoPaginacion;
        }

        public override GastoAgendado ObtenerPorId(int id)
        {
            GastoAgendado resultado = base.ObtenerPorId(id);
            resultado.Impuestos = gastoImpuestoService.ObtenerImpuestosDeGastoAgendado(id) ?? new List<GastoAgendadoImpuesto>();

            return resultado;

        }

        public override void Eliminar(int id, string userName)
        {
            IPagoGastoService pagoGastoService = new PagoGastoService();
            GastoAgendado GastoEmpresa = ObtenerPorId(id);
            var pagos = pagoGastoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="ReferenciaGasto",
                        TipoCampo=3,
                        Valor=GastoEmpresa.ReferenciaGasto
                    }
                }
            }).Result;

            if (pagos != null && pagos.Count > 0)
                throw new Exception($" El Gasto {GastoEmpresa.ReferenciaGasto} ya ha sido procesada. No se puede anular.");



            GastoEmpresa.ModificadoPor = userName;
            GastoEmpresa.Estado = (char)EstadosGastos.Anulado;
            GastoEmpresa.FechaModificacion = DateTime.Now;
            base.Actualizar(GastoEmpresa);

            var impuestos = gastoImpuestoService.ObtenerImpuestosDeGasto(id);
            impuestos.ForEach(i =>
            {
                gastoImpuestoService.Eliminar(i.Id, userName);
            });
        }

        public override bool Actualizar(GastoAgendado obj)
        {
            var viejoBalanceDeGastoEmpresa = this.ObtenerPorId(obj.Id);

            obj.FechaModificacion = DateTime.Now;
            obj.LocalidadId = obj.Localidad.Id;
            obj.Dia = Convert.ToByte(obj.FechaEmision.Day);
            var GastoEmpresa = new GastoAgendado(obj);
            GastoEmpresa.LocalidadId = GastoEmpresa.LocalidadId == 0 ? GastoEmpresa.Localidad.Id : GastoEmpresa.LocalidadId;

            var resultado = base.Actualizar(obj);
            GastoHelper.ActualizarImpuestosDeGasto(obj, gastoImpuestoService);
            return resultado;
        }

        public bool ActualizarMontos(GastoAgendado obj)
        {



            obj.FechaModificacion = DateTime.Now;
            var GastoEmpresa = new GastoAgendado(obj);
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public override List<GastoAgendado> ConsultarPorQuery(string query)
        {


            var resultado = base.ConsultarPorQuery(query);
            resultado.ForEach(f =>
            {
                f.Moneda = monedaService.ObtenerPorId(f.MonedaId);
                f.Suplidor = suplidoresService.ObtenerPorId(f.SuplidorId);
            });

            return resultado;
        }

        public override PagedResult<GastoAgendado> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {

            PagedResult<GastoAgendado> resultadoPaginacion = base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);


            return resultadoPaginacion;
        }

        public override PagedResult<GastoAgendado> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            var resultado = base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);

            return resultado;
        }

        public override PagedResult<GastoAgendado> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            PagedResult<GastoAgendado> resultadoPaginacion = base.ObtenerListaPaginadoYFiltro(buqueda);



            return resultadoPaginacion;
        }

        public void GenerarGastosAgendados()
        {
            var gastoService = new GastoEmpresaService();
            DateTime fechaActual = DateTime.Now;
            var agendadosDelMes = base.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="Dia",
                        TipoCampo=(int)TiposFiltros.MenorIgual,
                        Valor=fechaActual.Day.ToString()
                    }
                }
            }).Result ?? new List<GastoAgendado>();

            foreach (var agendado in agendadosDelMes)
            {
                agendado.Impuestos = this.gastoImpuestoService.ObtenerImpuestosDeGastoAgendado(agendado.Id);


                var gastoCreado = gastoService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>()
                    {
                    new FiltroBusqueda()
                    {
                    Campo="FechaEmision",
                    TipoCampo =(int)TiposFiltros.DiaCompleto,
                    Valor= new DateTime(fechaActual.Year,fechaActual.Month,agendado.Dia).ToString("yyyy-MM-dd")
                    },
                    new FiltroBusqueda()
                    {
                    Campo="SuplidorId",
                    TipoCampo=(int)TiposFiltros.IgualNumero,
                    Valor=agendado.SuplidorId.ToString()
                    },
                    new FiltroBusqueda()
                    {
                    Campo="Estado",
                    Valor=((char)EstadosGastos.Anulado).ToString(),
                    TipoCampo= (int)TiposFiltros.DiferenteTexto
                    }
                    }
                }).Result;

                if (gastoCreado == null || gastoCreado.Count == 0)
                {
                    var option = new TransactionOptions();
                    option.IsolationLevel = IsolationLevel.ReadCommitted;
                    option.Timeout = TimeSpan.FromMinutes(2);


                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, option))
                    {
                        try
                        {
                            GastoEmpresa gastoEmpresa = new GastoEmpresa(agendado);
                            gastoService.Insertar(gastoEmpresa);
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {

                        }
                    }


                }

            }
        }
    }
}
