﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
    public interface IFacturaService : IProcessCrud<Factura>
    {
        CxCDto ObtenerCxCGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, int? vendedorId, bool sumarizar=false);
        List<EstadoCuentaCliente> ObtenerEstadoCuenta(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId);

        CxCDto ObtenerVentas(DateTime? FechaInicio = default(DateTime?), DateTime? fechaFinal = default(DateTime?), int? clienteId = null, int? vendedorId = null);

        Factura InsertarFacturaAntigua(Factura obj);

        bool ActualizarMontos(Factura obj);
        Factura ObtenerPorNumeroFactura(string numeroFactura);
        ReporteComisiones ComisionesPorVentas(BusquedaReporteRecibos busqueda);
        List<DetalleComision> ComisionesPorVentasExcel(BusquedaReporteRecibos busqueda);
        List<FacturaImpuesto> ReporteDeImpuestosDeFacturas(DateTime? fechaInicial, DateTime? fechaFinal);
    }

    public interface ICotizacionService : IProcessCrud<Factura>
    {
        bool FacturarCotizacion(int id, string userId);
        bool ActualizarMontos(Factura obj);
        Factura ObtenerPorNumeroFactura(string numeroFactura);
        bool DuplicarCotizacion(int id, string userId, int? clienteId);
    }
}
