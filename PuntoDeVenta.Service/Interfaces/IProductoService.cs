﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IProductoService : IProcessCrud<Producto>
    {
        PagedResult<Producto> ObtenerProductosOServiciosFiltradosPorCampoYLimitado(int pageZise, string nombreDeCampo,string nombreDeCampo2, string caracteresABuscar, bool esServicio);

        PagedResult<Producto> ObtenerProductosYServiciosFiltradosPorCampoYLimitado(int pageZise, string nombreDeCampo, string nombreDeCampo2, string caracteresABuscar);

        PagedResult<Producto> ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar);

        PagedResult<Producto> ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar);

        bool ActualizarBasico(Producto obj);

        



    }
}
