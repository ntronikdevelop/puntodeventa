﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IGastoImpuestoService : IProcessCrud<GastoImpuesto>
    {
        List<GastoImpuesto> ObtenerImpuestosDeGasto(string referencia);
        List<GastoImpuesto> ObtenerImpuestosDeGasto(int gastoEmpresaId);

        List<GastoAgendadoImpuesto> ObtenerImpuestosDeGastoAgendado(int gastoEmpresaId);
        void EliminarImpuestoAgendado(int id, string userNamer);
    }
}
