﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface ILocalidadService : IProcessCrud<Localidad>
    {
        List<Localidad> ObtenerExistenciaEnAlmacenes(Nullable<int> localidadId);
        Localidad ObtenerBasico(int id);

        List<Localidad> ObtenerTodosConProductos();
        List<Localidad> ObtenerTodosBasicos();

    }
}
