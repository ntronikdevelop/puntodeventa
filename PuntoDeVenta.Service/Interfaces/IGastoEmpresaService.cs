﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IGastoEmpresaService : IProcessCrud<GastoEmpresa>
    {
        CxPDto ObtenerCxPGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> suplidorId, Nullable<int> monedaId, Nullable<int> localidadId);

      

        bool ActualizarMontos(GastoEmpresa obj);
    }
}
