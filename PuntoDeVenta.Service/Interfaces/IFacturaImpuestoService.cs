﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IFacturaImpuestoService : IProcessCrud<FacturaImpuesto>
    {
        List<FacturaImpuesto> ObtenerImpuestosDeFactura(string NoFactura);
        List<FacturaImpuesto> ObtenerImpuestosDeFactura(int facturaId);
    }
}
