﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IAlmacenService :IProcessCrud<Almacen>
    {
         List<Almacen> ObtenerAlmacenesPorLocalidad(int localidadId);
        List<Almacen> ObtenerAlmacenesPorLocalidadConProductos(int id);
        Almacen ObtenerPorCodigo(string codigo);
    }
}
