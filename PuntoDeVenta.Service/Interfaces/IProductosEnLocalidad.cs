﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Interfaces
{
   public interface IProductosEnLocalidadService
    {
        ProductosEnLocalidad ObtenerExistenciaDeProductoEnLocalidad(int localidadId, int productoId);
    }
}
