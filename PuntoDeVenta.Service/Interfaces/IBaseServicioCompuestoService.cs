﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
    public interface IBaseServicioCompuestoService : IProcessCrud<BaseServicioCompuesto>
    {
        List<BaseServicioCompuesto> ObtenerProductosDerivados(int productoId);
        List<BaseServicioCompuesto> ObtenerBasesDeProducto(int productoId);
    }
}
