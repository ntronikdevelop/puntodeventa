﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IDetalleFacturaService : IProcessCrud<DetalleFactura>
    {
        List<DetalleFactura> ObtenerDetallesPadresDeFactura(int facturaId);
        List<DetalleFactura> ObtenerDetallesDeFactura(int facturaId);
        bool EliminarDetallesDeFactura(int facturaId);

        List<DetalleFactura> ObtenerDetallesHijos(int detallePadreId);
        List<DetalleFactura> DetallesPorProducto(int productoId);
    }
}
