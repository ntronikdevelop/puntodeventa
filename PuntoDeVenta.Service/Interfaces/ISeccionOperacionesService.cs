﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface ISeccionOperacionesService : IProcessCrud<SeccionOperaciones>
    {
    }
}
