﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IPagoService : IProcessCrud<Pago>
    {
        Pago PagoDeFacturacion(Pago pago);
        ReporteRecibosDto ReporteDeIngresos(BusquedaReporteRecibos busqueda);
        ReporteComisiones ComisionesPorCobros(BusquedaReporteRecibos busqueda);

        List<DetalleComision> ComisionesPorCobrosExcel(BusquedaReporteRecibos busqueda);
        List<Pago> ReporteDeIngresosExcel(BusquedaReporteRecibos busqueda);
    }
}
