﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IMovimientoAlmacenService : IProcessCrud<MovimientoAlmacen>
    {
     

        MovimientoAlmacen ObtenerExistenciaDeProducto(int productoId, int almacenId);
        List<MovimientoProductoModel> ObtenerMovimientosPorProducto(int? productoId, int? almacenId, string fechaInicio, string fechaFinal);
    }
}
