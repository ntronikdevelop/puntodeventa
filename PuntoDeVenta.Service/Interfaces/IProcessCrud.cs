﻿using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Interfaces
{
    public interface IProcessCrud<T> where T : class
    {
        T Insertar(T obj);
        bool Actualizar(T obj);
        void Eliminar(int id,string userNamer);
       
        List<T> ConsultarPorQuery(string query);
        List<T> ObtenerTodos();
      

        PagedResult<T> ObtenerTodosPaginado(int page, int size, bool activos);

        PagedResult<T> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo,string valor);

        PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar);

        PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, bool activos, string caracteresABuscar);

        T ObtenerPorId(int id);
        T ObtenerPorIdBasico(int id);
        T ObtenerPorId(string id);

        List<T> ObtenerUltimoRegistro();

        PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda);
        PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda);
    }
}
