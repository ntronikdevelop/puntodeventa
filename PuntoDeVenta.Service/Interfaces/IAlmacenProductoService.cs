﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IAlmacenProductoService : IProcessCrud<AlmacenProducto>
    {
     

        List<AlmacenProducto> ObtenerExistenciaDeProducto(int productoId);
        List<AlmacenProducto> ObtenerExistenciaDeProductoEnAlmacen(int productoId, int almacenId);
        List<AlmacenProducto> ObtenerProductosDeAlmacen(int id);
    }
}
