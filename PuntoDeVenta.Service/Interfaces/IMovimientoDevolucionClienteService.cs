﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service.Interfaces
{
 public   interface IMovimientoDevolucionClienteService : IProcessCrud<MovimientoDevolucionCliente>
    {
        int AplicarDevolucion(DevolucionCliente devolucion,string creadoPor, string codigo);

        NotaCredito ObtenerNotaDeCredito(string referencia);
    }
}
