﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public class MovimientoDevolucionService : BaseService<MovimientoDevolucion>, IMovimientoDevolucionService
    {
        private IClienteService clienteService;
        private IAlmacenService almacenService;
        private ISuplidoresService suplidorService;
        private MovimientoDevolucionDao privateService;
        private IProductoService productoService;
        private IAlmacenProductoService almacenProductoService;
        private IUnidadesService unidades;
        private IMovimientoAlmacenService logMovimientos;
        private IProductosUnidadesService productosUnidades;

        public MovimientoDevolucionService() : base(new MovimientoDevolucionManager())
        {
            this.clienteService = new ClienteService();
            this.almacenService = new AlmacenService();
            this.suplidorService = new SuplidoresService();
            this.productoService = new ProductoService();
            this.almacenProductoService = new AlmacenProductoService();
            this.unidades = new UnidadesService();
            this.logMovimientos = new MovimientoAlmacenService();
            this.productosUnidades = new ProductoUnidadesService();
        }
        public override PagedResult<MovimientoDevolucion> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            return base.ObtenerTodosPaginado(page, size, activos);
        }

      

        public override MovimientoDevolucion ObtenerPorId(int id)
        {
            MovimientoDevolucion resultado = base.ObtenerPorId(id);
          
            return resultado;

        }



        public override MovimientoDevolucion Insertar(MovimientoDevolucion obj)
        {
            MovimientoDevolucion movimiento = null;
            obj.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
                   obj.Cantidad,
                   obj.UnidadId,
                   obj.Producto.ProductoUnidades
                   );

           

            var existenciaActual = almacenProductoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "AlmacenId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.AlmacenId}" },
                 new FiltroBusqueda() { Campo = "ProductoId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.ProductoId}" }}
            }).Result.FirstOrDefault();

            AlmacenProducto existenciaEnAlmacen = new AlmacenProducto(obj.AlmacenId, obj.ProductoId, obj.Cantidad, obj.CreadoPor, obj.Activo, obj.Producto.ProductoUnidades.FirstOrDefault(x=>x.EsPrincipal).UnidadId);

            if (existenciaActual != null)
            {
                existenciaActual.Cantidad = ProductosHelper.ConvertirAUnidadPrincipalProducto(
       existenciaActual.Cantidad,
       existenciaActual.UnidadId,
       existenciaActual.Producto.ProductoUnidades ==null || existenciaActual.Producto.ProductoUnidades.Count==0? productosUnidades.ObtenerUnidadesDeProducto(existenciaActual.ProductoId): existenciaActual.Producto.ProductoUnidades
       );


                existenciaActual.Cantidad -= existenciaEnAlmacen.Cantidad;
                if (existenciaActual.Cantidad < 0)
                    throw new Exception($"No hay suficiente cantidad del producto {obj.Producto.Nombre} en el almacen seleccionado.");

                existenciaActual.ModificadoPor = obj.CreadoPor ?? "";
                existenciaActual.FechaModificacion = obj.FechaCreacion;

                
                    MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(
                        existenciaEnAlmacen.AlmacenId,
                        existenciaEnAlmacen.ProductoId, 
                        existenciaEnAlmacen.Cantidad*-1, 
                        existenciaEnAlmacen.CreadoPor,
                        true, 
                        obj.Producto.ProductoUnidades.FirstOrDefault(u => u.EsPrincipal).UnidadId,
                        0,
                        "OUT",obj.Referencia??"");
                    logMovimientos.Insertar(movimientoAlmacen);

                
                bool resultado = almacenProductoService.Actualizar(existenciaActual);
            }
            else
            {
                throw new Exception($"No hay existencia del producto {obj.Producto.Nombre} en el almacen seleccionado.");
            }
            movimiento = base.Insertar(obj);
            var producto = productoService.ObtenerPorId(obj.ProductoId);
            producto.Existencia -= obj.Cantidad;
            if (producto.Existencia < 0)
                throw new Exception($"No hay existencia suficiente del producto {producto.Nombre} para esta transacción.");
            productoService.Actualizar(producto);
            if (movimiento == null)
                throw new Exception($"Error al insertar el producto {producto.Nombre} en el almacen seleccionado. Favor contactar el administrador");
            return movimiento;
        }

        public override bool Actualizar(MovimientoDevolucion obj)
        {
            throw new NotImplementedException("Acción no implementada por el momento.");
        }
    }
}
