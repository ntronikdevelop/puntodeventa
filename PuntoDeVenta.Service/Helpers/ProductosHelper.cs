﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers
{
    public class ProductosHelper
    {


        public static bool VerificarProductoEnFacturas(int productoId, IDetalleFacturaService newservice = null)
        {
            IDetalleFacturaService detalleFacturaService = newservice ?? new DetalleFacturaService();
            if (detalleFacturaService.DetallesPorProducto( productoId).Count > 0)
                return true;
            return false;
        }

        public static bool VerificarProductoEnAlmacen(int productoId, IAlmacenProductoService newService = null)
        {
            IAlmacenProductoService productoService = newService ?? new AlmacenProductoService();
            if (productoService.ObtenerExistenciaDeProducto(productoId).Where(e=>e.Cantidad>0).Count()> 0)
                return true;
            return false;
        }

        public static bool VerificarSiProductoEsBase(int productoId, IBaseServicioCompuestoService newService = null)
        {
            IBaseServicioCompuestoService baseService = newService ?? new BaseServicioCompuestoService();
            if (baseService.ObtenerProductosDerivados( productoId).Count() > 0)
                return true;
            return false;
        }

        public static bool InsertarUnidades(Producto producto, IProductosUnidadesService service)
        {
            producto.ProductoUnidades.ForEach(u =>
            {
                u.CreadoPor = producto.CreadoPor;
                u.FechaCreacion = DateTime.Now;
                u.Activo = true;
                u.ProductoId = producto.Id;
                service.Insertar(u);
            });

            return true;
        }

        public static bool InsertarImpuestos(Producto producto, IProductoImpuestoService service)
        {
            producto.Impuestos.ForEach(u =>
            {
                u.CreadoPor = producto.CreadoPor;
                u.FechaCreacion = DateTime.Now;
                u.Activo = true;
                u.ProductoId = producto.Id;
                service.Insertar(u);
            });

            return true;
        }

        public static bool InsertarProductosBase(Producto producto, IBaseServicioCompuestoService service)
        {
            if (producto.EsServicio && producto.EsCompuesto)
                producto.UnidadesProductosBase.ForEach(u =>
                {
                    u.CreadoPor = producto.CreadoPor;
                    u.FechaCreacion = DateTime.Now;
                    u.Activo = true;
                    u.ProductoCompuestoId = producto.Id;
                    service.Insertar(u);
                });

            return true;
        }

        public static bool ActualizarUnidadesProducto(Producto producto, IProductosUnidadesService productoUnidades)
        {
            List<ProductoUnidadesEquivalencia> unidadesDeProducto = producto.ProductoUnidades ?? new List<ProductoUnidadesEquivalencia>();
            var unidadesAnteriores = productoUnidades.ObtenerUnidadesDeProducto(producto.Id);

            if (producto.EsServicio)
            {
                unidadesDeProducto = new List<ProductoUnidadesEquivalencia>();
                var unidadesEliminadas = unidadesAnteriores.Except(unidadesDeProducto).ToList();
                if (unidadesEliminadas != null)
                    unidadesEliminadas.ForEach(e => { productoUnidades.Eliminar(e.Id, producto.ModificadoPor); });
            }

            if (!producto.EsServicio)
            {
                if (unidadesDeProducto.Count == 0)
                    throw new Exception("El producto necesita por lo menos una unidad. Proceso abortado");

                if (!unidadesDeProducto.Exists(u => u.EsPrincipal))
                    throw new Exception("Por lo menos una unidad debe de ser la principal. Proceso abortado.");
            }
            if (!producto.EsServicio && unidadesAnteriores != null)
            {
                unidadesDeProducto.ForEach(u =>
                {
                    if (u.Id == 0)
                    {
                        u.Activo = true;
                        u.CreadoPor = producto.CreadoPor;
                        u.FechaCreacion = producto.FechaCreacion;
                        productoUnidades.Insertar(u);
                    }
                    else
                    {
                        var unidadAnteriorEquivalente = unidadesAnteriores.Where(e => e.Id == u.Id).FirstOrDefault();
                        unidadAnteriorEquivalente.Equivalencia = u.Equivalencia;
                        unidadAnteriorEquivalente.EsPrincipal = u.EsPrincipal;
                        unidadAnteriorEquivalente.PrecioCompra = u.PrecioCompra;
                        unidadAnteriorEquivalente.PrecioVenta = u.PrecioVenta;
                        unidadAnteriorEquivalente.ModificadoPor = producto.ModificadoPor ?? string.Empty;
                        unidadAnteriorEquivalente.FechaModificacion = producto.FechaModificacion;
                        productoUnidades.Actualizar(unidadAnteriorEquivalente);
                    }


                });

                var unidadesEliminadas = unidadesAnteriores.Except(unidadesDeProducto).ToList();
                if (unidadesEliminadas != null)
                    unidadesEliminadas.ForEach(e =>
                    {
                        try
                        {
                            productoUnidades.Eliminar(e.Id, producto.ModificadoPor);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("La unidad de este producto está asociada a un servicio compuesto. No se puede borrar");
                        }

                    });

            }
            return true;
        }

        public static bool ActualizarImpuestosDeProducto(Producto producto, IProductoImpuestoService productoImpuestos)
        {
            List<ProductoImpuesto> impuestosDeProducto = producto.Impuestos ?? new List<ProductoImpuesto>();
            var impuestosAnteriores = productoImpuestos.ObtenerImpuestosDeProducto(producto.Id);

           
            if ( impuestosAnteriores != null)
            {
                impuestosDeProducto.ForEach(u =>
                {
                    if (u.Id == 0)
                    {
                        u.Activo = true;
                        u.CreadoPor = producto.CreadoPor;
                        u.FechaCreacion = producto.FechaCreacion;
                        u.ProductoId = producto.Id;
                        productoImpuestos.Insertar(u);
                    }
                    else
                    {
                        var unidadAnteriorEquivalente = impuestosDeProducto.Where(e => e.Id == u.Id).FirstOrDefault();
                       
                        unidadAnteriorEquivalente.ImpuestoId = u.ImpuestoId;
                        unidadAnteriorEquivalente.ModificadoPor = producto.ModificadoPor ?? string.Empty;
                        unidadAnteriorEquivalente.FechaModificacion = producto.FechaModificacion;
                        productoImpuestos.Actualizar(unidadAnteriorEquivalente);
                    }


                });

                var unidadesEliminadas = impuestosAnteriores.Except(impuestosDeProducto).ToList();
                if (unidadesEliminadas != null)
                    unidadesEliminadas.ForEach(e =>
                    {
                        try
                        {
                            productoImpuestos.Eliminar(e.Id, producto.ModificadoPor);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("El impuesto No se pudo borrar");
                        }

                    });

            }
            return true;
        }

        public static bool ActualizarBasesDeProducto(Producto producto, IBaseServicioCompuestoService productosBase)
        {
            List<BaseServicioCompuesto> basesDeProducto = producto.UnidadesProductosBase ?? new List<BaseServicioCompuesto>();
            var basesAnteriores = productosBase.ObtenerBasesDeProducto(producto.Id)?? new List<BaseServicioCompuesto>();

            if (!producto.EsServicio)
            {
                basesDeProducto = new List<BaseServicioCompuesto>();
                var basesEliminadas = basesAnteriores.Except(basesDeProducto).ToList();
                if (basesEliminadas != null)
                    basesEliminadas.ForEach(e => { productosBase.Eliminar(e.Id, producto.ModificadoPor); });
            }


            if (producto.EsServicio && producto.EsCompuesto )
            {
                basesDeProducto.ForEach(u =>
                {
                    if (u.Id == 0)
                    {
                        u.Activo = true;
                        u.CreadoPor = producto.CreadoPor;
                        u.FechaCreacion = producto.FechaCreacion;
                        productosBase.Insertar(u);
                    }
                    else
                    {
                        var baseAnteriorEquivalente = basesAnteriores.Where(e => e.Id == u.Id).FirstOrDefault();
                        if (baseAnteriorEquivalente != null) {
                            baseAnteriorEquivalente.Cantidad = u.Cantidad;
                            baseAnteriorEquivalente.ProductoBaseId = u.ProductoBaseId;
                            baseAnteriorEquivalente.ProductoCompuestoId = u.ProductoCompuestoId;
                            baseAnteriorEquivalente.ProductoUnidadBaseId = u.ProductoUnidadBaseId ?? null;
                            baseAnteriorEquivalente.ModificadoPor = producto.ModificadoPor ?? string.Empty;
                            baseAnteriorEquivalente.FechaModificacion = producto.FechaModificacion;
                            productosBase.Actualizar(baseAnteriorEquivalente);
                        }
                       
                    }


                });

                var unidadesEliminadas = basesAnteriores.Except(basesDeProducto).ToList();
                if (unidadesEliminadas != null)
                    unidadesEliminadas.ForEach(e =>
                    {
                        try
                        {
                            productosBase.Eliminar(e.Id, producto.ModificadoPor);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("La unidad de este producto está asociada a un servicio compuesto. No se puede borrar");
                        }

                    });

            }
            else if ((producto.EsServicio && !producto.EsCompuesto ) || !producto.EsServicio)
            {
                basesAnteriores.ForEach(e =>
                {
                    try
                    {
                        productosBase.Eliminar(e.Id, producto.ModificadoPor);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("La unidad de este producto está asociada a un servicio compuesto. No se puede borrar");
                    }

                });


            }
            return true;
        }
    }
}
