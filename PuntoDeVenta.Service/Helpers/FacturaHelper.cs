﻿using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PuntoDeVenta.Service.Helpers
{
    public class FacturaHelper
    {
        public static string CrearControlDeFactura(IControlSecuenciaService newService = null)
        {
            IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
            string resultado = string.Empty;
            ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "FAC" } }
            }).Result.FirstOrDefault();

            resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
            ultimoControl.ControlNumerico += 1;
            service.Actualizar(ultimoControl);
            return resultado;

        }

        public static Factura AplicarNotaDeCredito(Factura factura, NotaCredito notaCreditoAAplicar, out NotaCredito notaCredito)
        {
            if (!string.IsNullOrEmpty(factura.NotaCreditoAplicada))
            {


                if (notaCreditoAAplicar == null)
                    throw new Exception($"La nota de credito {factura.NotaCreditoAplicada} no existe.");

                if (notaCreditoAAplicar.Aplicado)
                    throw new Exception($"La nota de credito {factura.NotaCreditoAplicada} Ya fue aplicada a la factura {notaCreditoAAplicar.NoFacturaAplicadaId}. por el usuario {notaCreditoAAplicar.ModificadoPor}");

                if (notaCreditoAAplicar.MonedaId != factura.MonedaId)
                    throw new Exception("La moneda de la nota de credito es distinta a la de la factura. No se puede aplicar.");

                if (notaCreditoAAplicar.Monto > factura.MontoTotal)
                    throw new Exception("El monto de la nota de credito es mayor al  de la factura. No se puede aplicar.");

                notaCreditoAAplicar.Aplicado = true;

                notaCreditoAAplicar.ModificadoPor = factura.CreadoPor;
                notaCreditoAAplicar.FechaModificacion = DateTime.Now;
                notaCreditoAAplicar.NoFacturaAplicadaId = factura.NumeroFactura;
                notaCredito = notaCreditoAAplicar;
                factura.NotaCreditoAplicada = notaCreditoAAplicar.Secuencia;
                factura.MontoNotaCreditoAplicada = notaCreditoAAplicar.Monto;
                factura.MontoPagado += notaCreditoAAplicar.Monto;
               // factura.MontoAdeudado -= notaCreditoAAplicar.Monto;
                factura.DetallesFactura.ForEach(d =>
                {
                    decimal porcentajeNC = (d.MontoTotal/factura.MontoTotal  ) / d.Cantidad;
                    d.MontoNotaCreditoAplicada = porcentajeNC * notaCreditoAAplicar.Monto;


                });

                return factura;

            }
            notaCredito = notaCreditoAAplicar;
            return factura;
        }
        public static Pago AplicarPagoFactura(Pago pago, IPagoService newPagoService = null)
        {
            IPagoService pagoService = newPagoService ?? new PagoService();
            return pagoService.PagoDeFacturacion(pago);
        }
    }

    
}
