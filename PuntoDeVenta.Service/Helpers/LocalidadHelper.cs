﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers
{
   public class LocalidadHelper
    {
        private static ILocalidadService localidades = new LocalidadService();

        public static Localidad ObtenerLocalidad(int localidadId)
        {
            return localidades.ObtenerPorId(localidadId);
        }
    }
}
