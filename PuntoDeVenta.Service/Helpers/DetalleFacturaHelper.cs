﻿using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Helpers;

using System.Transactions;

namespace PuntoDeVenta.Service.Helpers
{
    public class DetalleFacturaHelper
    {
        public static Factura ActualizarDetallesDeFactura(Factura factura, IDetalleFacturaService newService = null, IFacturaService newFacturaService = null)
        {
            IDetalleFacturaService detalleService = newService ?? new DetalleFacturaService();

            IFacturaService facturaService = newFacturaService ?? new FacturaService();
            IProductosUnidadesService productosUnidades = new ProductoUnidadesService();
            IProductoService productoService = new ProductoService();
            IAlmacenProductoService almacenProductoService = new AlmacenProductoService();
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            IProductoImpuestoService productoImpuestoService = new ProductoImpuestoService();

            var detallesViejos = detalleService.ObtenerDetallesDeFactura(factura.Id);
            var detallesNuevos = factura.DetallesFactura.ToList();
            detallesNuevos.ForEach(d =>
            {
                d.Id = 0;
            });


            // BORRA LOS QUE YA NO EXISTEN EN LA FACTURA NUEVA
            detallesViejos.ForEach(d =>
            {
              
                    detalleService.Eliminar(d.Id, string.Empty);
                    InventarioEnAlmacenHelper.SumarInventarioAlAlmacen(d, factura,null,null,false);
                
            });

            //INSERTAR DETALLES NUEVOS
            Factura newFactura = new Factura(factura);
            newFactura.DetallesFactura = detallesNuevos;
            InsertarDetalles(newFactura,null,null,null,false);
            ActualizarImpuestosDeFactura(factura, facturaImpuestoService, productoImpuestoService);
            return factura;
        }

        public static void InsertarDetalles(Factura factura, IDetalleFacturaService newdetalleService = null,
            IProductoService newProductoService = null, IAlmacenProductoService newAlmacenProducto = null, bool actualizarImpuestos=true)
        {
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            IProductoImpuestoService productoImpuestoService = new ProductoImpuestoService();
            IDetalleFacturaService detalleService = newdetalleService ?? new DetalleFacturaService();
            IProductoService productoService = newProductoService ?? new ProductoService();
            IAlmacenProductoService almacenProductoService = newAlmacenProducto ?? new AlmacenProductoService();
          
            factura.DetallesFactura.ForEach(x =>
            {
                x.FacturaId = factura.Id;
                x.FechaCreacion = DateTime.Now;
                x.CreadoPor = factura.CreadoPor;
                x.Activo = true;
                x.Fecha = DateTime.Now;
                x.FechaModificacion = DateTime.Now;
                x = InventarioEnAlmacenHelper.ActualizarProductoEnAlmacen(factura.Localidad, x, productoService, almacenProductoService, detalleService, factura);
                if (x.InsertarRegistro)
                    detalleService.Insertar(x);
            });
            if(actualizarImpuestos)
            ActualizarImpuestosDeFactura(factura, facturaImpuestoService, productoImpuestoService);

        }

        static void ActualizarImpuestosDeFactura(Factura factura, IFacturaImpuestoService facturaImpuestoService, 
            IProductoImpuestoService productoImpuestoService)
        {
            var impuestosFactura = facturaImpuestoService.ObtenerImpuestosDeFactura(factura.NumeroFactura);
            if (impuestosFactura != null && impuestosFactura.Count > 0)
            {
                foreach (FacturaImpuesto impuesto in impuestosFactura)
                {
                    facturaImpuestoService.Eliminar(impuesto.Id, factura.CreadoPor ?? "");
                }
            }
            Dictionary<int, decimal> montosPorImpuesto = new Dictionary<int, decimal>();
            factura.DetallesFactura.ForEach(x =>
            {
                x.FacturaId = factura.Id;
              
                var impuestosProducto = x.Producto?.Impuestos != null && x.Producto?.Impuestos.Count > 0 ? x.Producto.Impuestos : productoImpuestoService.ObtenerImpuestosDeProducto(x.ProductoId);
                foreach (ProductoImpuesto impuesto in impuestosProducto)
                {
                    if (montosPorImpuesto.Any(i => i.Key == impuesto.ImpuestoId))
                    {
                        montosPorImpuesto[impuesto.ImpuestoId] += impuesto.Impuesto.Porciento * x.MontoGrabado;
                    }
                    else
                        montosPorImpuesto.Add(impuesto.ImpuestoId, impuesto.Impuesto.Porciento * x.MontoGrabado);
                }
            });

            foreach (var montoImpuestoFactura in montosPorImpuesto)
            {
                FacturaImpuesto facturaImpuesto = new FacturaImpuesto()
                {
                    CreadoPor = factura.CreadoPor ?? "",
                    Activo = true,
                    FacturaId = factura.Id,
                    Fecha = DateTime.Now,
                    FechaCreacion = DateTime.Now,
                    ImpuestoId = montoImpuestoFactura.Key,
                    MonedaId = factura.MonedaId,
                    MontoImpuesto = montoImpuestoFactura.Value,
                    NoFactura = string.IsNullOrEmpty(factura.NumeroFactura) ? factura.NoDocumento : factura.NumeroFactura,

                };
                facturaImpuestoService.Insertar(facturaImpuesto);
            }
        }
        public static Factura ActualizarDetallesDeCotizacion(Factura factura, IDetalleFacturaService newService = null, IFacturaService newFacturaService = null)
        {
            IDetalleFacturaService detalleService = newService ?? new DetalleFacturaService();

            IFacturaService facturaService = newFacturaService ?? new FacturaService();
            IProductosUnidadesService productosUnidades = new ProductoUnidadesService();
            IProductoService productoService = new ProductoService();
            IAlmacenProductoService almacenProductoService = new AlmacenProductoService();
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            IProductoImpuestoService productoImpuestoService = new ProductoImpuestoService();

            var detallesViejos = detalleService.ObtenerDetallesDeFactura(factura.Id);
            var detallesNuevos = factura.DetallesFactura.ToList();

            detallesNuevos.ForEach(d => {
                d.Id = 0;
            });

            // BORRA LOS QUE YA NO EXISTEN EN LA FACTURA NUEVA
            detalleService.EliminarDetallesDeFactura(factura.Id);

            //INSERTAR DETALLES NUEVOS
            Factura newFactura = new Factura(factura);
            newFactura.DetallesFactura = detallesNuevos;
            InsertarDetallesCotizacion(newFactura);

            ActualizarImpuestosDeFactura(factura, facturaImpuestoService, productoImpuestoService);
            return factura;
        }

        public static void InsertarDetallesCotizacion(Factura factura, IDetalleFacturaService newdetalleService = null, IProductoService newProductoService = null, IAlmacenProductoService newAlmacenProducto = null)
        {
            IDetalleFacturaService detalleService = newdetalleService ?? new DetalleFacturaService();
            IProductoService productoService = newProductoService ?? new ProductoService();
            IAlmacenProductoService almacenProductoService = newAlmacenProducto ?? new AlmacenProductoService();
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            IProductoImpuestoService productoImpuestoService = new ProductoImpuestoService();

            factura.DetallesFactura.ForEach(x =>
            {
                x.FacturaId = factura.Id;
                x.FechaCreacion = DateTime.Now;
                x.CreadoPor = factura.CreadoPor;
                x.Activo = true;
                x.Fecha = DateTime.Now;
                x.FechaModificacion = DateTime.Now;
                detalleService.Insertar(x);

            });

            ActualizarImpuestosDeFactura(factura, facturaImpuestoService, productoImpuestoService);
        }




    }
}
