﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers
{
    public enum ExcelColumnsDef
    {
        TEXT=1,
        DECIMAL=2,
        INTEGER=3,
        DATETIME=4
    }
}
