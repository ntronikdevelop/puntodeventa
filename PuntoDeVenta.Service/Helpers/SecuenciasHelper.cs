﻿using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers
{
    public class SecuenciasHelper
    {
        public static string CrearControlDeFactura(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja() {
                    Activo=true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo="Codigo", TipoCampo= (int)BusquedaComplejaEnum.EQUALSTRING, Valor="FAC" } }
                }).Result.FirstOrDefault();

                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de factura.");
            }


        }

        internal static string CrearControlDeVendedores(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "VND" } }
                }).Result.FirstOrDefault();

                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de vendedor.");
            }
        }

        public static string CrearControlDeClientes(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "CLI" } }
                }).Result.FirstOrDefault();

                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de cliente.");
            }


        }

        public static string CrearControlDeCotizacion(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "COT" } }
                }).Result.FirstOrDefault();

                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de cotizacion.");
            }


        }

        public static string CrearControlDeGastos(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "PG" } }
                }).Result.FirstOrDefault(); 
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de gasto.");
            }


        }
        public static string CrearControlDeGastosAgendados(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "GA" } }
                }).Result.FirstOrDefault();
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de gasto.");
            }


        }

        public static string CrearControlDeTransferencias(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "TRF" } }
                }).Result.FirstOrDefault();
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de transferencia.");
            }



        }

        public static string CrearControlDeEntradasInventario(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "ENT" } }
                }).Result.FirstOrDefault();
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de inventario.");
            }



        }

        public static string CrearControlDeDevoluciones(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "DEV" } }
                }).Result.FirstOrDefault();
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de devolución.");
            }



        }

        public static string CrearControlDeDevolucionesSuplidor(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "DS" } }
                }).Result.FirstOrDefault(); 
                if (ultimoControl == null)
                    throw new Exception("No existe secuencia para devoluciones a suplidor. Favor ingresar un control con codigo 'DS'");
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de devolución.");
            }



        }

        public static string CrearControlDePagos(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "R" } }
                }).Result.FirstOrDefault(); 
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de pago.");
            }


        }

        public static string CrearControlDePagoGastos(IControlSecuenciaService newService = null)
        {
            try
            {
                IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
                string resultado = string.Empty;
                ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "RG" } }
                }).Result.FirstOrDefault();
                resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
                ultimoControl.ControlNumerico += 1;
                service.Actualizar(ultimoControl);
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de ingresar secuencia de pago.");
            }


        }
    }
}
