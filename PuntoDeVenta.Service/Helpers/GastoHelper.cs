﻿using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PuntoDeVenta.Service.Helpers
{
    public class GastoHelper
    {
        public static string CrearControlDeGasto(IControlSecuenciaService newService = null)
        {
            IControlSecuenciaService service = newService ?? new ControlSecuenciaService();
            string resultado = string.Empty;
            ControlSecuencias ultimoControl = service.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = "PG" } }
            }).Result.FirstOrDefault();

            resultado = String.Format("{0}{1:00000}", ultimoControl.Codigo, (ultimoControl.ControlNumerico + 1));
            ultimoControl.ControlNumerico += 1;
            service.Actualizar(ultimoControl);
            return resultado;

        }
        public static void ActualizarImpuestosDeGasto(GastoEmpresa gastoEmpresa, IGastoImpuestoService gastoImpuestoService)
        {
            gastoEmpresa.Impuestos = gastoEmpresa.Impuestos == null ? new List<GastoImpuesto>() : gastoEmpresa.Impuestos;
            var impuestosGasto = gastoImpuestoService.ObtenerImpuestosDeGasto(gastoEmpresa.ReferenciaGasto);
            if (impuestosGasto != null && impuestosGasto.Count > 0)
            {
                foreach (GastoImpuesto impuesto in impuestosGasto)
                {
                    gastoImpuestoService.Eliminar(impuesto.Id, gastoEmpresa.ModificadoPor ?? gastoEmpresa.CreadoPor??"");
                }
            }
           
          

            foreach (var montoImpuestoGasto in gastoEmpresa.Impuestos)
            {
                GastoImpuesto gastoImpuesto = new GastoImpuesto()
                {
                    CreadoPor = gastoEmpresa.CreadoPor ?? "",
                    Activo = true,
                    GastoEmpresaId = gastoEmpresa.Id,
                    Fecha = DateTime.Now,
                    FechaCreacion = DateTime.Now,
                    ImpuestoId = montoImpuestoGasto.ImpuestoId,
                    MonedaId = gastoEmpresa.MonedaId,
                    MontoImpuesto = montoImpuestoGasto.MontoImpuesto,
                    Referencia = gastoEmpresa.ReferenciaGasto,

                };
                gastoImpuestoService.Insertar(gastoImpuesto);
            }
        }

        public static void ActualizarImpuestosDeGasto(GastoAgendado gastoEmpresa, IGastoImpuestoService gastoImpuestoService)
        {
            BaseService<GastoAgendadoImpuesto> impuestos = new BaseService<GastoAgendadoImpuesto>();
            gastoEmpresa.Impuestos = gastoEmpresa.Impuestos == null ? new List<GastoAgendadoImpuesto>() : gastoEmpresa.Impuestos;
            var impuestosGasto = gastoImpuestoService.ObtenerImpuestosDeGastoAgendado(gastoEmpresa.Id);
            if (impuestosGasto != null && impuestosGasto.Count > 0)
            {
                foreach (GastoAgendadoImpuesto impuesto in impuestosGasto)
                {
                    gastoImpuestoService.EliminarImpuestoAgendado(impuesto.Id, gastoEmpresa.ModificadoPor ?? gastoEmpresa.CreadoPor ?? "");
                }
            }



            foreach (var montoImpuestoGasto in gastoEmpresa.Impuestos)
            {
                GastoAgendadoImpuesto gastoImpuesto = new GastoAgendadoImpuesto()
                {
                    CreadoPor = gastoEmpresa.CreadoPor ?? "",
                    Activo = true,
                    GastoAgendadoId = gastoEmpresa.Id,
                    Fecha = DateTime.Now,
                    FechaCreacion = DateTime.Now,
                    ImpuestoId = montoImpuestoGasto.ImpuestoId,
                    MonedaId = gastoEmpresa.MonedaId,
                    MontoImpuesto = montoImpuestoGasto.MontoImpuesto,
                    Referencia = gastoEmpresa.ReferenciaGasto,

                };
                impuestos.Insertar(gastoImpuesto);
            }
        }

        public static PagoGasto AplicarPagoGasto(PagoGasto pago, IPagoGastoService newPagoService = null)
        {
            IPagoGastoService pagoService = newPagoService ?? new PagoGastoService();
            return pagoService.PagoDeIngresoDeGasto(pago);
        }

    }

    
}
