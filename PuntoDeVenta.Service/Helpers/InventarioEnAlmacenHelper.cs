﻿using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Helpers;

using System.Transactions;
using PuntoDeVenta.Service.Helpers.FacturarServicios;

namespace PuntoDeVenta.Service.Helpers
{
   public class InventarioEnAlmacenHelper
    {
        public static void ActualizarInventarioEnAlmacen(DetalleFactura detalle, IProductoService service, IAlmacenProductoService almacenProductoService, Almacen almacenActual, Factura factura)
        {
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            IMovimientoAlmacenService logMovimientos = new MovimientoAlmacenService();
            AlmacenProducto existenciaActual = almacenProductoService.ObtenerExistenciaDeProductoEnAlmacen( detalle.ProductoId,almacenActual.Id ).FirstOrDefault();
            if (existenciaActual != null)
            {
                detalle.Cantidad= PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
       detalle.Cantidad,
       detalle.UnidadId.Value,
      detalle.Producto.ProductoUnidades
       );
                detalle.UnidadId = detalle.Producto.ProductoUnidades.FirstOrDefault(pu => pu.EsPrincipal).UnidadId;
                existenciaActual.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
       existenciaActual.Cantidad,
       detalle.Producto.ProductoUnidades.Where(pu => pu.EsPrincipal).FirstOrDefault().UnidadId,
      detalle.Producto.ProductoUnidades
       );


                if (existenciaActual.Cantidad < detalle.Cantidad)
                    throw new Exception($"Existencia insuficiente para el producto {detalle.Producto.Nombre}. Solo existen {existenciaActual.Cantidad.ToString()} en el almacen {almacenActual.Nombre}. Factura cancelada.");
                else
                {
                    existenciaActual.Cantidad -= detalle.Cantidad;
                    Producto producto = service.ObtenerPorId(detalle.ProductoId);
                    producto.Existencia -= detalle.Cantidad;
                    service.Actualizar(producto);
                    almacenProductoService.Actualizar(existenciaActual);

                   
                        var unidades = detalle.Producto.ProductoUnidades ?? productoUnidades.ObtenerUnidadesDeProducto( detalle.ProductoId);
                        MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(almacenActual.Id, detalle.ProductoId, detalle.Cantidad*-1, detalle.CreadoPor??"system", true, unidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "OUT",factura.NumeroFactura??string.Empty);
                        logMovimientos.Insertar(movimientoAlmacen);

                    
                }
            }
            else
                throw new Exception("No existe inventario de ese producto. No se puede facturar");

        }


        public static void SumarInventarioAlAlmacen(DetalleFactura detalle, Factura factura, IProductoService service = null, IAlmacenProductoService almacenProductoService = null, bool insertarLog=true)
        {
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            IMovimientoAlmacenService logMovimientos = new MovimientoAlmacenService();
            if (!detalle.Producto.EsServicio)
            {
                detalle.Producto.ProductoUnidades = detalle.Producto.ProductoUnidades ?? productoUnidades.ObtenerUnidadesDeProducto( detalle.ProductoId);
                IProductoService newService = service ?? new ProductoService();
                IAlmacenProductoService newalmacenProductoService = almacenProductoService ?? new AlmacenProductoService();
                if (detalle.AlmacenId.HasValue)
                {
                    AlmacenProducto existenciaActual = newalmacenProductoService.ObtenerExistenciaDeProductoEnAlmacen(detalle.ProductoId, detalle.AlmacenId.Value).FirstOrDefault();
                    if (existenciaActual != null)
                    {

                        existenciaActual.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
               existenciaActual.Cantidad,
               detalle.Producto.ProductoUnidades.Where(pu => pu.EsPrincipal).FirstOrDefault().UnidadId,
               detalle.Producto.ProductoUnidades
               );
                        detalle.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
               detalle.Cantidad,
               detalle.UnidadId.Value,
                detalle.Producto.ProductoUnidades
               );
                        var unidades = detalle.Producto.ProductoUnidades ?? productoUnidades.ObtenerUnidadesDeProducto(detalle.ProductoId);
                        MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(existenciaActual.AlmacenId, detalle.ProductoId, detalle.Cantidad , detalle.CreadoPor ?? "system", true, unidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "IN", factura.NumeroFactura??string.Empty);
                        if(insertarLog)
                        logMovimientos.Insertar(movimientoAlmacen);

                        existenciaActual.Cantidad += detalle.Cantidad;
                        Producto producto = newService.ObtenerPorId(detalle.ProductoId);
                        producto.Existencia += detalle.Cantidad;
                        newService.Actualizar(producto);
                        newalmacenProductoService.Actualizar(existenciaActual);

                    }
                    else
                        throw new Exception("No existe inventario de ese producto. No se puede facturar");
                }
            }



        }


        public static void ReinsertarInventarioEnAlmacen(DetalleFactura detalle, IProductoService service, IAlmacenProductoService almacenProductoService, Almacen almacenActual)
        {
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            AlmacenProducto existenciaActual = almacenProductoService.ObtenerExistenciaDeProductoEnAlmacen(  detalle.ProductoId, almacenActual.Id).FirstOrDefault();
            if (existenciaActual != null)
            {
                detalle.Producto.ProductoUnidades = detalle.Producto.ProductoUnidades ?? productoUnidades.ObtenerUnidadesDeProducto(detalle.ProductoId);
                existenciaActual.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
       existenciaActual.Cantidad,
       detalle.Producto.ProductoUnidades.Where(pu => pu.EsPrincipal).FirstOrDefault().UnidadId,
        detalle.Producto.ProductoUnidades
       );

                detalle.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
           detalle.Cantidad,
           detalle.UnidadId.Value,
           detalle.Producto.ProductoUnidades
           );

                existenciaActual.Cantidad += detalle.Cantidad;
                Producto producto = service.ObtenerPorId(detalle.ProductoId);
                producto.Existencia += detalle.Cantidad;
                service.Actualizar(producto);
                almacenProductoService.Actualizar(existenciaActual);

            }
            else
                almacenProductoService.Insertar(new AlmacenProducto()
                {
                    Activo = true,
                    Cantidad = detalle.Cantidad,
                    AlmacenId = almacenActual.Id,
                    CreadoPor = detalle.CreadoPor,
                    FechaCreacion = DateTime.Now,
                    ProductoId = detalle.ProductoId,
                    UnidadId = detalle.Producto.ProductoUnidades.Where(u => u.EsPrincipal).FirstOrDefault().Id
                });

        }

        public static DetalleFactura ActualizarProductoEnAlmacen(Localidad localidad, DetalleFactura detalle, IProductoService service,
            IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            DetalleFactura newDetalle = new DetalleFactura(detalle);
           
            var instanciaFacturarProducto = InstanciarClaseFacturarProductoServicio.InstanciarFacturador(detalle.Producto);
            newDetalle = instanciaFacturarProducto.ProcesarProductoServicio(localidad.Id, detalle, service, almacenProductoService,detalleService,factura);



            return newDetalle;
        }
    }
}
