﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios
{
    public class FacturarServicioCompuesto : FacturarProductoServicioBase
    {
        protected override DetalleFactura ProcesarDetalle(int localidad, DetalleFactura detalle, IProductoService service, IAlmacenProductoService almacenProductoService,IDetalleFacturaService detalleService, Factura factura)
        {
            IBaseServicioCompuestoService baseServicioCompuestoService = new BaseServicioCompuestoService();
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            var listaProductosBase = detalle.Producto.UnidadesProductosBase ==null || detalle.Producto.UnidadesProductosBase.Count==0? baseServicioCompuestoService.ObtenerBasesDeProducto(detalle.ProductoId): detalle.Producto.UnidadesProductosBase;
            detalle.AlmacenId = null;
            detalle.UnidadId = null;
            var detalleResult = detalleService.Insertar(detalle);

            listaProductosBase.ForEach(p =>
            {
                var productoActual = service.ObtenerPorId(p.ProductoBaseId);
                var productoUnidadActual = p.ProductoUnidadBase == null ? p.ProductoUnidadBaseId.HasValue ? productoUnidades.ObtenerPorId(p.ProductoUnidadBaseId.Value) : null : p.ProductoUnidadBase;
                var facturador = InstanciarClaseFacturarProductoServicio.InstanciarFacturador(productoActual);
                // decimal nuevaCantidad = p.Cantidad * detalle.Cantidad;
                DetalleFactura detalleTemp = new DetalleFactura(detalle)
                {
                    Cantidad = p.Cantidad * detalle.Cantidad,
                    UnidadId = productoUnidadActual != null ? productoUnidadActual.Unidad.Id : p.ProductoUnidadBaseId,
                    ProductoId = p.ProductoBaseId,
                    DetallePadreId = detalleResult.Id,
                    Producto = productoActual,
                    Monto = 0,
                    MontoDescuento = 0,
                    MontoDop = 0,
                    MontoItbis = 0,
                    MontoGrabado = 0,
                    MontoTotal = 0,
                    MontoNotaCreditoAplicada = 0,
                    EsGratis=true
                };
                var resultado = facturador.ProcesarProductoServicio(localidad, detalleTemp, service, almacenProductoService, detalleService, factura);
               if(resultado.InsertarRegistro)
                    detalleService.Insertar(resultado);
            });
            detalle.InsertarRegistro = false;
            return detalle;
         }

        protected override DetalleFactura ProcesarDevolucionDetalle(int localidad, DetalleDevolucion detalle, IProductoService service, 
            IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            var listaDetallesBase = detalleService.ObtenerDetallesHijos(detalle.Id) ?? new List<DetalleFactura>();
          //  detalle.AlmacenId = null;
          //  detalle.UnidadId = null;


            listaDetallesBase.ForEach(p =>
            {
                var productoActual =p.Producto?? service.ObtenerPorId(p.ProductoId);

               

                var facturador = InstanciarClaseFacturarProductoServicio.InstanciarFacturador(productoActual);
                // decimal nuevaCantidad = p.Cantidad * detalle.Cantidad;
                DetalleDevolucion detalleTemp = new DetalleDevolucion()
                {
                    Id=p.Id,
                    Activo=p.Activo,
                    AlmacenId=p.AlmacenId??null,
                    Cantidad=p.Cantidad * ( detalle.CantidadDevolucion / detalle.Cantidad),
                    CantidadDevolucion= p.Cantidad  *(detalle.CantidadDevolucion / detalle.Cantidad),
                    DetallePadreId=p.DetallePadreId??null,
                    Defectuoso=detalle.Defectuoso,
                    FacturaId=p.FacturaId,
                    Producto=p.Producto,
                    ProductoId=p.ProductoId,
                    Unidad=p.Unidad,
                    UnidadId=p.UnidadId
                    

                }; 

                var resultado = facturador.DevolverProductoServicio(localidad, detalleTemp, service, almacenProductoService,detalleService, factura);
             
            });
            return detalle;
        }
    }
}
