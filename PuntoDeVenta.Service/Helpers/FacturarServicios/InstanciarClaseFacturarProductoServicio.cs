﻿using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Helpers.FacturarServicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios
{
 public   class InstanciarClaseFacturarProductoServicio
    {
        private static IFacturarProductoServicios ObtenerServicioParaFacturar(List<FacturarProductoServicio> lista, Producto producto)
        {
            string clase = lista.Where(x => x.EsCompuesto == producto.EsCompuesto && x.EsServicio == producto.EsServicio).FirstOrDefault()?.NombreDeClase;

            if (string.IsNullOrEmpty(clase))
                throw new Exception("No existe clase para facturar este producto.");
           
            Type serviceType = Type.GetType(clase);
            return (IFacturarProductoServicios)Activator.CreateInstance(serviceType);

        }

        public static IFacturarProductoServicios InstanciarFacturador(Producto producto)
        {
            IBaseDb<FacturarProductoServicio> eleccionDb = new FacturarProductoServicioDao();
            var eleccionParaFacturar = MemoryCache.Default.Get("FacturarProductoServicio") as List<FacturarProductoServicio>;
            if (eleccionParaFacturar == null)
            {
             
                eleccionParaFacturar= eleccionDb.ObtenerLista();
                MemoryCache.Default.Add("FacturarProductoServicio", eleccionParaFacturar, DateTime.Now.AddMinutes(10));
            }
            
            IFacturarProductoServicios reporteador = ObtenerServicioParaFacturar(eleccionParaFacturar,producto);
            return reporteador;

        }
    }
}
