﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios.Interfaces
{
    public interface IFacturarProductoServicios
    {
        DetalleFactura ProcesarProductoServicio(int localidad, DetalleFactura detalle,IProductoService service,
            IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura);

        DetalleFactura DevolverProductoServicio(int localidad, DetalleDevolucion detalle, IProductoService service,
           IAlmacenProductoService almacenProductoService,IDetalleFacturaService detalleService, Factura factura);


    }
}
