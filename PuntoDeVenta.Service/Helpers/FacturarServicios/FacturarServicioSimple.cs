﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios
{
  public  class FacturarServicioSimple : FacturarProductoServicioBase
    {
        protected override DetalleFactura ProcesarDetalle(int localidad, DetalleFactura detalle, IProductoService productos, IAlmacenProductoService almacenProductoService,IDetalleFacturaService detallesService, Factura factura)
        {
            var newDetalle = new DetalleFactura(detalle);
            newDetalle.AlmacenId = null;
            newDetalle.UnidadId = null;

        
            return newDetalle;
        }

        protected override DetalleFactura ProcesarDevolucionDetalle(int localidad, DetalleDevolucion detalle, IProductoService service, 
            IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            return detalle;
        }
    }
}
