﻿using PuntoDeVenta.Service.Helpers.FacturarServicios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios
{
    public abstract class FacturarProductoServicioBase : IFacturarProductoServicios
    {


        public DetalleFactura ProcesarProductoServicio(int localidadId, DetalleFactura detalle, IProductoService service, IAlmacenProductoService almacenProductoService , IDetalleFacturaService detalleService, Factura factura)
        {
            return ProcesarDetalle(localidadId, detalle, service, almacenProductoService,detalleService, factura);
        }

        public DetalleFactura DevolverProductoServicio(int localidadId, DetalleDevolucion detalle, IProductoService service,
            IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            return ProcesarDevolucionDetalle(localidadId, detalle, service, almacenProductoService,detalleService, factura);
        }


        protected abstract DetalleFactura ProcesarDetalle(int localidadId, DetalleFactura detalle, IProductoService service, IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura);

        protected abstract DetalleFactura ProcesarDevolucionDetalle(int localidadId, DetalleDevolucion detalle, IProductoService service, IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura facturatura);

    }
}
