﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service.Helpers.FacturarServicios
{
    public class FacturarProducto : FacturarProductoServicioBase
    {
        protected override DetalleFactura ProcesarDetalle(int localidadId, DetalleFactura detalle, IProductoService productos, IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            IAlmacenService almacenes = new AlmacenService();
            DetalleFactura newDetalle = new DetalleFactura(detalle);
            detalle.Producto.ProductoUnidades = detalle.Producto.ProductoUnidades == null || detalle.Producto.ProductoUnidades.Count == 0 ?
                    productoUnidades.ObtenerUnidadesDeProducto(detalle.ProductoId) : detalle.Producto.ProductoUnidades;

            decimal cantidadDeAlmacen = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
newDetalle.Cantidad,
newDetalle.UnidadId.Value,
newDetalle.Producto.ProductoUnidades
);
            decimal acumulativoDeCantidad = cantidadDeAlmacen;

            DetalleFactura nuevoDetalle = new DetalleFactura(newDetalle)
            {

                Cantidad = cantidadDeAlmacen
            };
            ////ARREGLARRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

            var almacen =( factura.AlmacenId<=0)? almacenes.ConsultarPorQuery($"select A.id, A.activo,A.localidadId,A.nombre,A.codigo,A.creadoPor, A.FechaCreacion from almacen as A left join almacenproducto as AP on a.id= ap.almacenId where"+
                $" a.localidadId= {localidadId} and a.codigo !='DEF' and ap.cantidad >={cantidadDeAlmacen} and ap.productoId={newDetalle.ProductoId}").FirstOrDefault():almacenes.ObtenerPorId(factura.AlmacenId);

            if (( factura.AlmacenId > 0) && (almacenProductoService.ObtenerListaFiltrada(new BuquedaCompleja()
                 {
                     Activo = true,
                     Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "ProductoId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{newDetalle.ProductoId}" },
                    new FiltroBusqueda() { Campo = "AlmacenId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{factura.AlmacenId}" },
                    new FiltroBusqueda() { Campo = "Cantidad", TipoCampo = (int)BusquedaComplejaEnum.GREATEROREQUAL, Valor = $"{cantidadDeAlmacen}" }
                }
                 }).Result.Count == 0))
                throw new Exception($"El almacen elegido no posee la cantidad suficiente para el producto {newDetalle.Producto?.Nombre??string.Empty}.");

                if (almacen == null)
            {
                string queryProductos = $"select AP.id, AP.almacenId, AP.ProductoId,AP.Cantidad,Ap.Activo,Ap.UnidadId,AP.LocalidadId,AP.CreadoPor,AP.FechaCreacion,Ap.ModificadoPor," +
                    $"AP.FechaModificacion from almacen as A left join"+$" almacenproducto as AP on a.id= ap.almacenId where a.localidadId= {localidadId} and a.codigo !='DEF'" +
                    $" and ap.cantidad >0 and ap.productoId={newDetalle.ProductoId}";

                var productoEnLocalidad = almacenProductoService.ConsultarPorQuery(queryProductos);
                if (productoEnLocalidad.Count == 0)
                    throw new Exception($"No hay existencia de {newDetalle.Producto.Nombre} en la sucursal. Proceso abortado");

                decimal cantidadEnTodosAlmacenes = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
 productoEnLocalidad.Sum(p => p.Cantidad),
 productoEnLocalidad.FirstOrDefault().UnidadId,
 newDetalle.Producto.ProductoUnidades
 );
                if (cantidadEnTodosAlmacenes < cantidadDeAlmacen)
                    throw new Exception($"No hay existencia suficiente del producto {newDetalle.Producto.Nombre} en la sucursal. Proceso abortado.");
                foreach (AlmacenProducto productoEnAlmacen in productoEnLocalidad)
                {
                    if (acumulativoDeCantidad <= 0)
                        break;
                    decimal cantidadEnAlmacenActual = productoEnAlmacen.Cantidad;
                    if (acumulativoDeCantidad < cantidadEnAlmacenActual)
                        cantidadEnAlmacenActual = acumulativoDeCantidad;

                    nuevoDetalle = new DetalleFactura(newDetalle);
                    nuevoDetalle.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadDesdePrincipalProducto(
 cantidadEnAlmacenActual,
 newDetalle.UnidadId.Value,
 newDetalle.Producto.ProductoUnidades
 );
                    nuevoDetalle.AlmacenId = productoEnAlmacen.AlmacenId;
                  //  nuevoDetalle.UnidadId = nuevoDetalle.Producto.ProductoUnidades.FirstOrDefault(x => x.EsPrincipal).UnidadId;
                    InventarioEnAlmacenHelper.ActualizarInventarioEnAlmacen(nuevoDetalle, productos, almacenProductoService,
                        almacenes.ObtenerPorIdBasico(productoEnAlmacen.AlmacenId),factura);
                    newDetalle.AlmacenId = productoEnAlmacen.AlmacenId;

                    acumulativoDeCantidad -= cantidadEnAlmacenActual;
                }


            }
            else
            {
                newDetalle.AlmacenId = almacen.Id;
                nuevoDetalle.Unidad =  nuevoDetalle.Producto.ProductoUnidades.FirstOrDefault(x => x.EsPrincipal).Unidad;
                nuevoDetalle.UnidadId = nuevoDetalle.Unidad.Id;
                InventarioEnAlmacenHelper.ActualizarInventarioEnAlmacen(nuevoDetalle, productos, almacenProductoService, almacen,factura);

            }

            return newDetalle;
        }

        protected override DetalleFactura ProcesarDevolucionDetalle(int localidadId, DetalleDevolucion detalle, 
            IProductoService service, IAlmacenProductoService almacenProductoService, IDetalleFacturaService detalleService, Factura factura)
        {
            AlmacenProducto existenciaActual = null;
            IAlmacenService almacenes = new AlmacenService();
            IProductoService newService = service ?? new ProductoService();
            IAlmacenProductoService newalmacenProductoService = almacenProductoService ?? new AlmacenProductoService();
            IProductosUnidadesService productoUnidades = new ProductoUnidadesService();
            IMovimientoAlmacenService logMovimientos = new MovimientoAlmacenService();

            if (detalle.AlmacenId.HasValue)
            {
                detalle.Producto.ProductoUnidades = detalle.Producto.ProductoUnidades==null || detalle.Producto.ProductoUnidades.Count==0?
                    productoUnidades.ObtenerUnidadesDeProducto(detalle.ProductoId) : detalle.Producto.ProductoUnidades;

                if (detalle.Defectuoso)
                {
                    var almacenDefectuosos = almacenes.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = $"DEF" },
                    new FiltroBusqueda() { Campo = "LocalidadId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{localidadId}" }
                }
                    }).Result.FirstOrDefault();
                    if (almacenDefectuosos == null)
                        throw new Exception("Esta localidad no posee un almacen para defectos. Favor crear uno con el codigo 'DEF'.");
                    existenciaActual = newalmacenProductoService.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "AlmacenId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{almacenDefectuosos.Id}" },
                    new FiltroBusqueda() { Campo = "ProductoId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{ detalle.ProductoId}" },
                     new FiltroBusqueda() { Campo = "LocalidadId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{localidadId}" }
                }
                    }).Result.FirstOrDefault() ??
                new AlmacenProducto()
                {
                    AlmacenId = almacenDefectuosos.Id,
                    ProductoId = detalle.ProductoId,
                    Activo = true,
                    Cantidad = 0,
                    CreadoPor = "system",
                    FechaCreacion = DateTime.Now,
                    LocalidadId = localidadId,
                    UnidadId = detalle.Producto.ProductoUnidades.FirstOrDefault(u => u.EsPrincipal).UnidadId,
                    Producto=detalle.Producto
                };
                }
                else
                {
                    existenciaActual = newalmacenProductoService.ObtenerListaFiltrada(new BuquedaCompleja()
                    {
                        Activo = true,
                        Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "AlmacenId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{detalle.AlmacenId}" },
                    new FiltroBusqueda() { Campo = "ProductoId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{detalle.ProductoId}" }
                }
                    }).Result.FirstOrDefault();
                }
                if (existenciaActual != null)
                {
                    existenciaActual.Producto.ProductoUnidades= existenciaActual.Producto.ProductoUnidades == null || existenciaActual.Producto.ProductoUnidades.Count == 0 ?
                    productoUnidades.ObtenerUnidadesDeProducto(existenciaActual.ProductoId) : existenciaActual.Producto.ProductoUnidades;

                    existenciaActual.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
         existenciaActual.Cantidad,
         detalle.Producto.ProductoUnidades.FirstOrDefault(pu => pu.EsPrincipal).UnidadId,
         detalle.Producto.ProductoUnidades
         );
                    detalle.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
           detalle.Cantidad,
           detalle.UnidadId.Value,
           detalle.Producto.ProductoUnidades
           );
                    var unidades = detalle.Producto.ProductoUnidades == null || detalle.Producto.ProductoUnidades.Count == 0 ?
                    productoUnidades.ObtenerUnidadesDeProducto(detalle.ProductoId) : detalle.Producto.ProductoUnidades;

                    MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(existenciaActual.AlmacenId, detalle.ProductoId, detalle.Cantidad, detalle.CreadoPor ?? "system", true, unidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "IN",factura.NumeroFactura??string.Empty);
                    logMovimientos.Insertar(movimientoAlmacen);

                    existenciaActual.Cantidad += detalle.Cantidad;
                    existenciaActual.UnidadId = existenciaActual.Producto.ProductoUnidades.FirstOrDefault(x => x.EsPrincipal).UnidadId;
                    Producto producto = newService.ObtenerPorId(detalle.ProductoId);
                    producto.Existencia += detalle.Cantidad;
                    newService.Actualizar(producto);
                    if (existenciaActual.Id > 0)
                        newalmacenProductoService.Actualizar(existenciaActual);
                    else
                        newalmacenProductoService.Insertar(existenciaActual);
                }
                else
                    throw new Exception("No existe inventario anterior de este producto. No se puede procesar la devolución.");

            }

            return detalle;
        }
    }
}
