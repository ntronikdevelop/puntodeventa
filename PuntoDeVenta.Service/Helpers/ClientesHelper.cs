﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service.Helpers
{
    public class ClientesHelper
    {
        public static string CrearCodigoCliente(int id)
        {
            string resultado = string.Empty;
            resultado = String.Format("{0}{1:00000}", "C", (id));
            return resultado;

        }
    }
}
