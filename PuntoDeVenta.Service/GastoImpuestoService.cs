﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class GastoImpuestoService : BaseService<GastoImpuesto>, IGastoImpuestoService
    {
        private BaseManager<GastoAgendadoImpuesto> impuestosAgendados = new BaseManager<GastoAgendadoImpuesto>();
        
        public GastoImpuestoService():base(new GastoImpuestoManager()) { }

        public List<GastoImpuesto> ObtenerImpuestosDeGasto(string NoFactura)
        {
            return this.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo=true,
                Filtros=new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="Referencia",
                        TipoCampo=3,
                        Valor=NoFactura
                    }
                }
            }).Result;
        }

        public List<GastoImpuesto> ObtenerImpuestosDeGasto(int facturaId)
        {
            return this.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="GastoEmpresaId",
                        TipoCampo=1,
                        Valor=facturaId.ToString()
                    }
                }
            }).Result;
        }

        public List<GastoAgendadoImpuesto> ObtenerImpuestosDeGastoAgendado(int gastoEmpresaId)
        {
            return impuestosAgendados.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="GastoAgendadoId",
                        TipoCampo=1,
                        Valor=gastoEmpresaId.ToString()
                    }
                }
            }).Result;
        }

        public  void EliminarImpuestoAgendado(int id, string userNamer) 
        {
            impuestosAgendados.Borrar(id);
        }
    }
}
