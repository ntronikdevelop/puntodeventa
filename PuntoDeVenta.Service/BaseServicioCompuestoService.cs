﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dao;

namespace PuntoDeVenta.Service
{
    public class BaseServicioCompuestoService : BaseService<BaseServicioCompuesto>, IBaseServicioCompuestoService
    {
        IProductoDb productos = new ProductoManager();
        IProductosUnidadesService unidades = new ProductoUnidadesService();
        BaseServicioCompuestoDao dbService = new BaseServicioCompuestoDao();

        public List<BaseServicioCompuesto> ObtenerBasesDeProducto(int productoId)
        {
            return dbService.ObtenerBasesDeProducto(productoId);
        }

        public List<BaseServicioCompuesto> ObtenerProductosDerivados(int productoId)
        {
            return dbService.ObtenerProductosDerivados(productoId);
        }
    }
}
