﻿using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using System.Linq.Expressions;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.Service
{
    public class IdentityBaseService<T> : IProcessCrud<T> where T : class
    {
        private IBaseDb<T> manager;

        public IdentityBaseService()
        {
            this.manager = new IdentityBaseManager<T>();
        }

        public IdentityBaseService(IBaseDb<T> newManager)
        {
            this.manager = newManager;
        }

        public virtual bool Actualizar(T obj)
        {
            return manager.Actualizar(obj);
        }

       

        public virtual List<T> ConsultarPorQuery(string query)
        {
            return manager.ObtenerListaPorConsulta(query);
        }

        public virtual void Eliminar(int id, string userName)
        {
            manager.Borrar(id);
        }

        public virtual T Insertar(T obj)
        {
            return manager.Insertar(obj);
        }

        public virtual PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            return manager.ObtenerListaFiltrada(buqueda);
        }

        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            return manager.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
        }

        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, bool activos, string caracteresABuscar)
        {
            return manager.ObtenerListaPaginadoYFiltro(new BuquedaCompleja() {  Size=cantidadMax, Activo=activos, Page=1, Filtros= new List<FiltroBusqueda>() {
                new FiltroBusqueda(){ Campo=NombreCampo, Valor=caracteresABuscar, TipoCampo= (int)BusquedaComplejaEnum.EQUALSTRING }
            } });
        }

        public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            return manager.ObtenerListaPaginadoYFiltro(buqueda);
        }

        public virtual T ObtenerPorId(string id)
        {
            return manager.ObtenerPorId(id);
        }

        public virtual T ObtenerPorId(int id)
        {
            return manager.ObtenerPorId(id);
        }

        public virtual T ObtenerPorIdBasico(int id)
        {
            throw new NotImplementedException();
        }

        public virtual List<T> ObtenerTodos()
        {
            return manager.ObtenerLista();
        }

        public virtual PagedResult<T> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            return manager.ObtenerListaPaginado(page, size, activos);
        }

        public virtual PagedResult<T> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            throw new NotImplementedException();
        }

        public virtual List<T> ObtenerUltimoRegistro()
        {
            var lista = manager.ObtenerUltimoRegistro();
            return lista;
        }
    }
}
