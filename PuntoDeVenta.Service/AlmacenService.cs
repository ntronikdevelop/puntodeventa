﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public class AlmacenService : BaseService<Almacen>, IAlmacenService
    {
      
        private IAlmacenProductoService productosService;
        public AlmacenService( IAlmacenProductoService productosAlmacenService=null) : base(new AlmacenManager())
        {
           
            this.productosService = productosAlmacenService ?? new AlmacenProductoService();
        }

        public override PagedResult<Almacen> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            var result= base.ObtenerTodosPaginado(page, size, activos);
            //result.Result.ForEach(x => {
            //    x.Productos = productosService.ConsultarPorExpresion(y => y.AlmacenId == x.Id);
                
            //});
            return result;
        }

        public override PagedResult<Almacen> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {

            var result = base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);
            //result.Result.ForEach(x => {
            //    x.Productos = productosService.ConsultarPorExpresion(y => y.AlmacenId == x.Id);
            //});
            return result;
        }

       

     
        public override Almacen ObtenerPorId(int id)
        {
            var result= base.ObtenerPorId(id);

            //Task<List<AlmacenProducto>> t_productos = Task.Factory.StartNew<List<AlmacenProducto>>((arg) => {
            //    var productos = productosService.ConsultarPorExpresion(p => p.AlmacenId == (int)arg);
            //    return productos;
            //},result.Id); 
            //result.Productos = t_productos.Result;
            return result;
        }

        public List<Almacen> ObtenerAlmacenesPorLocalidad(int localidadId)
        {
        return    base.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "LocalidadId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{localidadId}" } }
            }).Result;
        }

        public List<Almacen> ObtenerAlmacenesPorLocalidadConProductos(int id)
        {
            var result = base.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "LocalidadId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{id}" } }
            }).Result;

            result.ForEach(r => {
                r.Productos = productosService.ObtenerProductosDeAlmacen(r.Id);
            });
            return result;
        }

        public Almacen ObtenerPorCodigo(string codigo)
        {
            var result = base.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Codigo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = $"{codigo}" } }
            }).Result;

            return result.FirstOrDefault();
        }
    }
}
