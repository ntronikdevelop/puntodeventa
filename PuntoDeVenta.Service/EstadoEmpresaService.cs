﻿using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeVenta.Service
{
    public class EstadoEmpresaService : IEstadoEmpresaService
    {
        IPagoGastoService pagoGastoService  = new PagoGastoService();
        IPagoService pagoService = new PagoService();
        IFacturaService facturaService = new FacturaService();

        public List<ResultadoEstadoCuenta> EstadoFinancieroEmpresa(DateTime? fechaInicial, DateTime? fechaFinal)
        {
            List<EstadoCuentaCliente> resultado = new List<EstadoCuentaCliente>();
          

             fechaInicial = fechaInicial == null ? new DateTime(DateTime.Now.Year,DateTime.Now.Month,1,0,0,0) : fechaInicial;
             fechaFinal = fechaFinal == null? new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.AddDays(1).Day)): fechaFinal;
            var filtroFechas = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo = "FechaCreacion",
                        TipoCampo=9,
                        Valor=fechaInicial.Value.ToString("yyyy-MM-dd")
                    },
                     new DataAccess.FiltroBusqueda()
                    {
                        Campo = "FechaCreacion",
                        TipoCampo=10,
                        Valor=fechaFinal.Value.ToString("yyyy-MM-dd")
                    }
                };
           resultado.AddRange(  pagoGastoService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = filtroFechas
            }).Result.Select(x=> new EstadoCuentaCliente()
            {
                Concepto=x.Suplidor.Nombre,
                Referencia=x.Secuencia,
                Fecha=x.FechaCreacion,
                MonedaId=x.MonedaId,
                MonedaNombre=$"{x.Moneda.Codigo}",
                TasaConversion=x.Moneda.Tasa,
                MontoAdeudado= -1 * x.MontoPendiente,
                Monto=-1 * x.MontoPagado
            }).ToList());

           resultado.AddRange( pagoService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo=true,
                Filtros= filtroFechas
            }).Result.Select(x => new EstadoCuentaCliente()
            {
                Concepto = x.Cliente.CodigoYNombre,
                Referencia = x.Secuencia,
                Fecha = x.FechaCreacion,
                MonedaId = x.MonedaId,
                MonedaNombre = $"{x.Moneda.Codigo}",
                TasaConversion = x.Moneda.Tasa,
                MontoAdeudado =  x.MontoPendiente,
                Monto =  x.MontoPagado
            }).ToList());

            filtroFechas.AddRange(new List<DataAccess.FiltroBusqueda>()
            {
                new DataAccess.FiltroBusqueda()
                {
                    Campo="Estado",
                    TipoCampo=13,
                    Valor="'A','C','D'"
                },
             
            });
            resultado.AddRange(facturaService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja()
            {
                Activo = true,
                Filtros = filtroFechas
            }).Result.Select(x => new EstadoCuentaCliente()
            {
                Concepto = "Costo Factura",
                Referencia = x.NumeroFactura,
                Fecha = x.FechaFacturado!=null?x.FechaFacturado.Value:x.FechaCreacion,
                MonedaId = x.MonedaId,
                MonedaNombre = $"{x.Moneda.Codigo}",
                TasaConversion = x.Moneda.Tasa,
                MontoAdeudado = x.MontoAdeudado,
                Monto = -1 * x.Costo
            }).ToList());


            var resultadoAgrupado = new List<ResultadoEstadoCuenta>();
            resultado.GroupBy(x => x.MonedaId).ToList().ForEach(e=> {
                resultadoAgrupado.Add(new ResultadoEstadoCuenta()
                {
                    Pagos= e.ToList(),
                    MontoComprado =e.Where(p=>p.Monto<0).Sum(r=>r.Monto),
                    MontoPagado= e.Where(p=>p.Monto>0).Sum(r=>r.Monto),
                    Factura= new EstadoCuentaCliente()
                    {
                        MonedaId=e.Key,
                        MonedaNombre=e.FirstOrDefault()?.MonedaNombre,
                        
                    }
                });
            });

            return resultadoAgrupado;
        }
    }
}
