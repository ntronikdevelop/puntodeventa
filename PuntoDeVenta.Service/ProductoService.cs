﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao;

namespace PuntoDeVenta.Service
{
    public class ProductoService : BaseService<Producto>, IProductoService
    {
        private ProductoDao productoService;
        private IMonedaService monedaService;
        private ISuplidoresService suplidorService;
        private IProductosUnidadesService productoUnidades;
        private IProductoImpuestoService productoImpuestoService;
        private IBaseServicioCompuestoService productosBase;
        private BaseService<ProductoUnidadesCustom> customProductoUnidades = new BaseService<ProductoUnidadesCustom>();
        private BaseService<BaseServicioCompuestoCustom> custombaseServicioCompuesto = new BaseService<BaseServicioCompuestoCustom>(); 
        private string unidadesCustomBaseQuery = "select pu.Id, pu.ProductoId, pu.Activo,pu.UnidadId,pu.Equivalencia,pu.EsPrincipal,pu.Orden,pu.PrecioCompra,pu.PrecioVenta,u.Nombre as NombreUnidad"+
            " from productounidadesequivalencia pu inner join unidades u on pu.UnidadId=u.id where pu.Activo=1 ";
      
        public ProductoService() : base(new ProductoManager())
        {
            this.productoImpuestoService = new ProductoImpuestoService();
            this.productoService = new ProductoDao();
            this.monedaService = new MonedaService();
            this.suplidorService = new SuplidoresService();
            this.productoUnidades = new ProductoUnidadesService();
            this.productosBase = new BaseServicioCompuestoService();

        }

        public PagedResult<Producto> ObtenerProductosOServiciosFiltradosPorCampoYLimitado(int pageZise, string nombreDeCampo, string caracteresABuscar, bool esServicio)
        {
            PagedResult<Producto> resultado = new PagedResult<Producto>();

            switch (esServicio)
            {
                case true:
                    resultado = productoService.ObtenerSoloServiciosFiltradosPorCampoYLimitada(pageZise, nombreDeCampo, caracteresABuscar);
                    break;
                default:
                    resultado = productoService.ObtenerSoloProductosFiltradosPorCampoYLimitada(pageZise, nombreDeCampo, caracteresABuscar);
                    break;
            }

            return resultado;
        }

        public override void Eliminar(int id, string userNamer)
        {
            if (PuntoDeVenta.Service.Helpers.ProductosHelper.VerificarProductoEnFacturas(id))
                throw new Exception("El producto tiene facturas emitidas. No se puede eliminar");
            if (PuntoDeVenta.Service.Helpers.ProductosHelper.VerificarProductoEnAlmacen(id))
                throw new Exception("El producto existe en almacen. No se puede eliminar");

            if (PuntoDeVenta.Service.Helpers.ProductosHelper.VerificarSiProductoEsBase(id))
                throw new Exception("El producto existe como base de otro producto / servicio. No se puede eliminar");

            base.Eliminar(id, userNamer);

        }

        public PagedResult<Producto> ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar)
        {
            PagedResult<Producto> resultado = new PagedResult<Producto>();
            resultado = productoService.ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(pageZise, nombreDeCampo1, nombreDeCampo2, caracteresABuscar);

            resultado.Result.ForEach(x =>
            {
               
                x.ProductoUnidades = customProductoUnidades.ConsultarPorQuery(unidadesCustomBaseQuery+ $" and pu.ProductoId = {x.Id};").Select(y=> new ProductoUnidadesEquivalencia() {
Activo=y.Activo,
Equivalencia=y.Equivalencia,
EsPrincipal=y.EsPrincipal,
Id=y.Id,
Orden=y.Orden,
PrecioCompra=y.PrecioCompra,
PrecioVenta=y.PrecioVenta,
ProductoId=y.ProductoId,
UnidadId=y.UnidadId,
Unidad = new Unidades() {Id=y.UnidadId, Nombre=y.NombreUnidad, Activo=true }
                }).ToList();
                if(x.EsCompuesto)
                x.UnidadesProductosBase = custombaseServicioCompuesto.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {x.Id}").Select(z=>new BaseServicioCompuesto() {
                    Activo=z.Activo,
                    Cantidad=z.Cantidad,
                    CostoTotal=z.CostoTotal,
                    Id=z.Id,
                    PrecioTotal=z.PrecioTotal,
                    ProductoBaseId=z.ProductoBaseId,
                    ProductoCompuestoId=z.ProductoCompuestoId,
                    ProductoUnidadBaseId=z.ProductoUnidadBaseId,
                    Unidad=z.ProductoUnidadBaseId.HasValue? new Unidades() { Nombre= z.NombreUnidad,Activo=true, Id=z.ProductoUnidadBaseUnidadId}:null,
                    ProductoUnidadBase= z.ProductoUnidadBaseId.HasValue? new ProductoUnidadesEquivalencia() {
                        Activo=true, Equivalencia=z.ProductoUnidadBaseEquivalencia,EsPrincipal=z.ProductoUnidadBaseEsPrincipal, PrecioCompra= z.ProductoUnidadBasePrecioCompra,
                        PrecioVenta=z.ProductoUnidadBasePrecioVenta, Id=z.ProductoUnidadBaseId.Value, ProductoId=z.ProductoBaseId, UnidadId=z.ProductoUnidadBaseUnidadId, Orden=z.ProductoUnidadBaseOrden
                    } :null
                }).ToList();
            });

            return resultado;
        }

        public PagedResult<Producto> ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(int pageZise, string nombreDeCampo1, string nombreDeCampo2, string caracteresABuscar)
        {
            PagedResult<Producto> resultado = new PagedResult<Producto>();
            resultado = productoService.ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(pageZise, nombreDeCampo1, nombreDeCampo2, caracteresABuscar);
            resultado.Result.ForEach(x =>
            {
                x.ProductoUnidades  = customProductoUnidades.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {x.Id};").Select(y => new ProductoUnidadesEquivalencia()
                {
                    Activo = y.Activo,
                    Equivalencia = y.Equivalencia,
                    EsPrincipal = y.EsPrincipal,
                    Id = y.Id,
                    Orden = y.Orden,
                    PrecioCompra = y.PrecioCompra,
                    PrecioVenta = y.PrecioVenta,
                    ProductoId = y.ProductoId,
                    UnidadId = y.UnidadId,
                    Unidad = new Unidades() { Id = y.UnidadId, Nombre = y.NombreUnidad, Activo = true }
                }).ToList();
                if (x.EsCompuesto)
                    x.UnidadesProductosBase = custombaseServicioCompuesto.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {x.Id}").Select(z => new BaseServicioCompuesto()
                    {
                        Activo = z.Activo,
                        Cantidad = z.Cantidad,
                        CostoTotal = z.CostoTotal,
                        Id = z.Id,
                        PrecioTotal = z.PrecioTotal,
                        ProductoBaseId = z.ProductoBaseId,
                        ProductoCompuestoId = z.ProductoCompuestoId,
                        ProductoUnidadBaseId = z.ProductoUnidadBaseId,
                        Unidad = z.ProductoUnidadBaseId.HasValue ? new Unidades() { Nombre = z.NombreUnidad, Activo = true, Id = z.ProductoUnidadBaseUnidadId } : null,
                        ProductoUnidadBase = z.ProductoUnidadBaseId.HasValue ? new ProductoUnidadesEquivalencia()
                        {
                            Activo = true,
                            Equivalencia = z.ProductoUnidadBaseEquivalencia,
                            EsPrincipal = z.ProductoUnidadBaseEsPrincipal,
                            PrecioCompra = z.ProductoUnidadBasePrecioCompra,
                            PrecioVenta = z.ProductoUnidadBasePrecioVenta,
                            Id = z.ProductoUnidadBaseId.Value,
                            ProductoId = z.ProductoBaseId,
                            UnidadId = z.ProductoUnidadBaseUnidadId,
                            Orden = z.ProductoUnidadBaseOrden
                        } : null
                    }).ToList();
            });
            if (base.cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<Producto>(resultado.Result);
            return resultado;
        }

        public PagedResult<Producto> ObtenerProductosOServiciosFiltradosPorCampoYLimitado(int pageZise, string nombreDeCampo, string nombreDeCampo2, string caracteresABuscar, bool esServicio)
        {
            PagedResult<Producto> resultado = new PagedResult<Producto>();

            switch (esServicio)
            {
                case true:
                    resultado = productoService.ObtenerSoloServiciosFiltradosPorVariosCamposYLimitada(pageZise, nombreDeCampo, nombreDeCampo2, caracteresABuscar);
                    break;
                default:
                    resultado = productoService.ObtenerSoloProductosFiltradosPorVariosCamposYLimitada(pageZise, nombreDeCampo, nombreDeCampo2, caracteresABuscar);
                    break;
            }

       

            return resultado;
        }

        public PagedResult<Producto> ObtenerProductosYServiciosFiltradosPorCampoYLimitado(int pageZise, string nombreDeCampo, string nombreDeCampo2, string caracteresABuscar)
        {
            PagedResult<Producto> resultado = new PagedResult<Producto>();


            resultado = productoService.ObtenerFiltradosPorVariosCamposYLimitada(pageZise, nombreDeCampo, nombreDeCampo2, caracteresABuscar);

            resultado.Result.ForEach(x =>
            {
               
                x.ProductoUnidades = customProductoUnidades.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {x.Id};").Select(y => new ProductoUnidadesEquivalencia()
                {
                    Activo = y.Activo,
                    Equivalencia = y.Equivalencia,
                    EsPrincipal = y.EsPrincipal,
                    Id = y.Id,
                    Orden = y.Orden,
                    PrecioCompra = y.PrecioCompra,
                    PrecioVenta = y.PrecioVenta,
                    ProductoId = y.ProductoId,
                    UnidadId = y.UnidadId,
                    Unidad = new Unidades() { Id = y.UnidadId, Nombre = y.NombreUnidad, Activo = true }
                }).ToList();
                if (x.EsCompuesto)
                    x.UnidadesProductosBase = custombaseServicioCompuesto.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {x.Id}").Select(z => new BaseServicioCompuesto()
                    {
                        Activo = z.Activo,
                        Cantidad = z.Cantidad,
                        CostoTotal = z.CostoTotal,
                        Id = z.Id,
                        PrecioTotal = z.PrecioTotal,
                        ProductoBaseId = z.ProductoBaseId,
                        ProductoCompuestoId = z.ProductoCompuestoId,
                        ProductoUnidadBaseId = z.ProductoUnidadBaseId,
                        Unidad = z.ProductoUnidadBaseId.HasValue ? new Unidades() { Nombre = z.NombreUnidad, Activo = true, Id = z.ProductoUnidadBaseUnidadId } : null,
                        ProductoUnidadBase = z.ProductoUnidadBaseId.HasValue ? new ProductoUnidadesEquivalencia()
                        {
                            Activo = true,
                            Equivalencia = z.ProductoUnidadBaseEquivalencia,
                            EsPrincipal = z.ProductoUnidadBaseEsPrincipal,
                            PrecioCompra = z.ProductoUnidadBasePrecioCompra,
                            PrecioVenta = z.ProductoUnidadBasePrecioVenta,
                            Id = z.ProductoUnidadBaseId.Value,
                            ProductoId = z.ProductoBaseId,
                            UnidadId = z.ProductoUnidadBaseUnidadId,
                            Orden = z.ProductoUnidadBaseOrden
                        } : null
                    }).ToList();
            });

            return resultado;
        }

        public override PagedResult<Producto> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Producto> resultadoPaginacion = base.ObtenerTodosPaginado( page, size, activos);

    
            return resultadoPaginacion;
        }

        public override Producto ObtenerPorId(int id)
        {

            Producto resultado = base.ObtenerPorId(id);
            resultado.Impuestos =  productoImpuestoService.ObtenerImpuestosDeProducto(id);
            resultado.ProductoUnidades = customProductoUnidades.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {id};").Select(y => new ProductoUnidadesEquivalencia()
            {
                Activo = y.Activo,
                Equivalencia = y.Equivalencia,
                EsPrincipal = y.EsPrincipal,
                Id = y.Id,
                Orden = y.Orden,
                PrecioCompra = y.PrecioCompra,
                PrecioVenta = y.PrecioVenta,
                ProductoId = y.ProductoId,
                UnidadId = y.UnidadId,
                Unidad = new Unidades() { Id = y.UnidadId, Nombre = y.NombreUnidad, Activo = true }
            }).ToList();
            if (resultado.EsCompuesto)
                resultado.UnidadesProductosBase = custombaseServicioCompuesto.ConsultarPorQuery(unidadesCustomBaseQuery + $" and pu.ProductoId = {resultado.Id}").Select(z => new BaseServicioCompuesto()
                {
                    Activo = z.Activo,
                    Cantidad = z.Cantidad,
                    CostoTotal = z.CostoTotal,
                    Id = z.Id,
                    PrecioTotal = z.PrecioTotal,
                    ProductoBaseId = z.ProductoBaseId,
                    ProductoCompuestoId = z.ProductoCompuestoId,
                    ProductoUnidadBaseId = z.ProductoUnidadBaseId,
                    Unidad = z.ProductoUnidadBaseId.HasValue ? new Unidades() { Nombre = z.NombreUnidad, Activo = true, Id = z.ProductoUnidadBaseUnidadId } : null,
                    ProductoUnidadBase = z.ProductoUnidadBaseId.HasValue ? new ProductoUnidadesEquivalencia()
                    {
                        Activo = true,
                        Equivalencia = z.ProductoUnidadBaseEquivalencia,
                        EsPrincipal = z.ProductoUnidadBaseEsPrincipal,
                        PrecioCompra = z.ProductoUnidadBasePrecioCompra,
                        PrecioVenta = z.ProductoUnidadBasePrecioVenta,
                        Id = z.ProductoUnidadBaseId.Value,
                        ProductoId = z.ProductoBaseId,
                        UnidadId = z.ProductoUnidadBaseUnidadId,
                        Orden = z.ProductoUnidadBaseOrden
                    } : null
                }).ToList();

            return resultado;

        }

        public override Producto Insertar(Producto obj)
        {
            var productoExistente = productoService.ObtenerPorNombre(obj.Nombre);
            if (productoExistente != null && productoExistente.Count > 0)
                throw new Exception("Ya se encuentra un producto con ese nombre");

            obj.UnidadesProductosBase = obj.UnidadesProductosBase ?? new List<BaseServicioCompuesto>();
            obj.Impuestos = obj.Impuestos ?? new List<ProductoImpuesto>();
            List<ProductoUnidadesEquivalencia> unidadesDeProducto = obj.ProductoUnidades ?? new List<ProductoUnidadesEquivalencia>();
            if (obj.EsServicio)
                unidadesDeProducto = new List<ProductoUnidadesEquivalencia>();

            if (!obj.EsServicio && unidadesDeProducto.Count == 0)
                throw new Exception("El producto necesita por lo menos una unidad. Proceso abortado");

            //if (obj.Impuestos==null  || obj.Impuestos.Count==0)
            //    throw new Exception("El producto necesita por lo menos un impuesto. Proceso abortado");

            if (!obj.EsServicio && !unidadesDeProducto.Exists(u => u.EsPrincipal))
                throw new Exception("Por lo menos una unidad debe de ser la principal. Proceso abortado.");

            var resultado = base.Insertar(obj);
            obj.Id = resultado.Id;
            PuntoDeVenta.Service.Helpers.ProductosHelper.InsertarUnidades(obj, productoUnidades);
            PuntoDeVenta.Service.Helpers.ProductosHelper.InsertarImpuestos(obj, productoImpuestoService);
            PuntoDeVenta.Service.Helpers.ProductosHelper.InsertarProductosBase(obj, productosBase);

            return resultado;
        }

        public override bool Actualizar(Producto obj)
        {

            //if (obj.Impuestos == null || obj.Impuestos.Count == 0)
            //    throw new Exception("El producto necesita por lo menos un impuesto. Proceso abortado");

            PuntoDeVenta.Service.Helpers.ProductosHelper.ActualizarUnidadesProducto(obj, productoUnidades);
            PuntoDeVenta.Service.Helpers.ProductosHelper.ActualizarBasesDeProducto(obj, productosBase);
            PuntoDeVenta.Service.Helpers.ProductosHelper.ActualizarImpuestosDeProducto(obj, productoImpuestoService);
            var resultado = base.Actualizar(obj);

            return resultado;

        }

        public bool ActualizarBasico(Producto obj)
        {
            if (!obj.EsServicio)
            {
                if (obj.ProductoUnidades.Count == 0)
                    throw new Exception("El producto necesita por lo menos una unidad. Proceso abortado");

                if (!obj.ProductoUnidades.Exists(u => u.EsPrincipal))
                    throw new Exception("Por lo menos una unidad debe de ser la principal. Proceso abortado.");
            }


            return base.Actualizar(obj);
        }
    }
}
