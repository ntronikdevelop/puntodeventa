﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess;
using System.Configuration;
using PuntoDeVenta.DataAccess.Dao;
using System.Runtime.Caching;
using PuntoDeVenta.Constants.Lib;

namespace PuntoDeVenta.Service
{
    public class CotizacionService : BaseService<Factura>, ICotizacionService
    {
        private IDetalleFacturaService detallesService;
        private IMonedaService monedaService;
        private IClienteService clienteService;
        private ILocalidadService localidadService;
        private IFacturaService facturaService;
        private IBalanceClienteService balanceService;
        private IAlmacenService almacenService;
        private INotaCreditoService notaCreditoService;
        private FacturaDao facturaDb = new FacturaDao();
        private IProductoImpuestoService productoImpuestoService;

        public CotizacionService()
        {
            this.detallesService = new DetalleFacturaService();
            this.monedaService = new MonedaService();
            this.clienteService = new ClienteService();
            this.localidadService = new LocalidadService();
            this.productoImpuestoService = new ProductoImpuestoService();
            this.balanceService = new BalanceClienteService();
            this.notaCreditoService = new NotaCreditoService();
            this.facturaService = new FacturaService();
            this.almacenService = new AlmacenService();
        }
    

     

 

        public override Factura Insertar(Factura obj)
        {
            obj.Id = 0;
            obj.NoDocumento = "";
            obj.NumeroFactura = "";
            obj.ZonaId = obj.Vendedor !=null && obj.Vendedor.ZonaId.HasValue ? obj.Vendedor.ZonaId.Value : 0;
            if (obj.DetallesFactura.Count <= 0)
                throw new Exception("La Cotización no tiene articulos");
            List<DetalleFactura> detalles = obj.DetallesFactura;
            if (obj.Vendedor != null && obj.Vendedor.Id > 0)
            {
                detalles.ForEach(d => {
                  
                    decimal comision = obj.Vendedor.ComisionFija ? ((d.MontoGrabado) * obj.Vendedor.PorcentajeComision) : 0;
                    comision += obj.Vendedor.ComisionPorProducto ? ((d.MontoGrabado) * d.Producto?.PorcentajeVendedor).Value : 0;
                    d.PorcentajeVendedor = comision;
                });
                obj.PorcentajeVendedor = detalles.Sum(x => x.PorcentajeVendedor);
            }
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(detalles.Select(x => x.ProductoId).ToArray());
           
            detalles.ForEach(d => {

                d.Id = 0;
                d.FacturaId = 0;
                d.MontoDescuento = d.Monto * d.PorcientoDescuento /100;
                d.MontoGrabado = (d.Monto - d.MontoDescuento) * d.Cantidad;
                d.MontoItbis = (impuestos.Where(i=>i.ProductoId==d.ProductoId).Sum(i => i.Impuesto.Porciento) * (d.MontoGrabado));

                d.MontoTotal = ((d.MontoGrabado) + d.MontoItbis);
            });
            obj.MontoGrabado = detalles.Sum(x => x.MontoGrabado);
            obj.MontoItbis = detalles.Sum(x => x.MontoItbis);
            obj.MontoTotal = detalles.Sum(x => x.MontoTotal);
            obj.MontoDescuento = detalles.Sum(x => x.MontoDescuento * x.Cantidad);
            obj.MontoAdeudado = obj.MontoTotal - obj.MontoNotaCreditoAplicada - obj.MontoPagado;
            obj.MontoAdeudado = obj.MontoAdeudado < 0 ? 0 : obj.MontoAdeudado;
            obj.MontoDevuelto = obj.MontoRecibido - obj.MontoTotal;

            obj.NoDocumento = SecuenciasHelper.CrearControlDeCotizacion();
            obj.NumeroFactura = obj.NoDocumento;
            var localidadTemp = obj.Localidad;
            obj.Estado = (char)EstadosFactura.Cotizado;
           
      
            obj.MontoDevuelto = obj.MontoDevuelto < 0 ? 0 : obj.MontoDevuelto;
           
            var factura = base.Insertar(obj);
           
            factura.DetallesFactura = detalles;
            factura.Localidad = localidadTemp;
            DetalleFacturaHelper.InsertarDetallesCotizacion(factura);
            return factura;
        }

     

      

        public override PagedResult<Factura> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Factura> resultadoPaginacion = base.ObtenerTodosPaginado( page, size, activos);

           
            return resultadoPaginacion;
        }

        public override Factura ObtenerPorId(int id)
        {
            Factura resultado = base.ObtenerPorId(id);
              
                var detallesFactura = detallesService.ObtenerDetallesPadresDeFactura(id).OrderBy(x=>x.Id).ToList();
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(detallesFactura.Select(x => x.ProductoId).ToArray());
            detallesFactura.ForEach(d => {
                d.Impuestos = impuestos.Where(x => x.ProductoId == d.ProductoId).ToList();
            });

            resultado.DetallesFactura = detallesFactura;
            return resultado;

        }

        public override void Eliminar(int id, string userName)
        {
        
            Factura factura = ObtenerPorId(id);
            if (factura.Estado != (char)EstadosFactura.Cotizado)
                throw new Exception($"La cotización {factura.NoDocumento} ya ha sido procesada. No se puede anular.");
            
            factura.Estado = (char)EstadosFactura.Anulado;
           
     
            factura.ModificadoPor = userName;
            factura.FechaModificacion = DateTime.Now;
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            var impuestos = facturaImpuestoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="NoFactura",
                        TipoCampo=3,
                        Valor=!string.IsNullOrEmpty(factura.NumeroFactura)?factura.NumeroFactura:factura.NoDocumento
                    }
                }
            }).Result;
            if (impuestos != null)
            {
                impuestos.ForEach(i => {
                    ;
                    facturaImpuestoService.Eliminar(i.Id, userName);
                });
            }
            base.Actualizar(factura);
        }

        public override bool Actualizar(Factura obj)
        {
            obj.ZonaId = obj.Vendedor != null && obj.Vendedor.ZonaId.HasValue ? obj.Vendedor.ZonaId.Value : 0;
            if (obj.DetallesFactura.Count <= 0)
                throw new Exception("La cotización no tiene articulos");
            List<DetalleFactura> detalles = obj.DetallesFactura;

            if (obj.Vendedor != null && obj.Vendedor.Id > 0)
            {
                detalles.ForEach(d => {
                    decimal comision = obj.Vendedor.ComisionFija ? ((d.MontoGrabado) * obj.Vendedor.PorcentajeComision) : 0;
                    comision += obj.Vendedor.ComisionPorProducto ? ((d.MontoGrabado) * d.Producto?.PorcentajeVendedor).Value : 0;
                    d.PorcentajeVendedor = comision;
                });
                obj.PorcentajeVendedor = detalles.Sum(x => x.PorcentajeVendedor);
            }
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(detalles.Select(x => x.ProductoId).ToArray());

            detalles.ForEach(d => {


                d.MontoDescuento = d.Monto * d.PorcientoDescuento / 100;
                d.MontoGrabado = (d.Monto - d.MontoDescuento) * d.Cantidad;
                d.MontoItbis = (impuestos.Where(i => i.ProductoId == d.ProductoId).Sum(i => i.Impuesto.Porciento) * (d.MontoGrabado));

                d.MontoTotal = ((d.MontoGrabado) + d.MontoItbis);
            });
            obj.MontoGrabado = detalles.Sum(x => x.MontoGrabado);
            obj.MontoItbis = detalles.Sum(x => x.MontoItbis);
            obj.MontoTotal = detalles.Sum(x => x.MontoTotal);
            obj.MontoDescuento = detalles.Sum(x => x.MontoDescuento * x.Cantidad);
            obj.MontoAdeudado = obj.MontoTotal - obj.MontoNotaCreditoAplicada - obj.MontoPagado;
            obj.MontoAdeudado = obj.MontoAdeudado < 0 ? 0 : obj.MontoAdeudado;
            obj.MontoDevuelto = obj.MontoRecibido - obj.MontoTotal;

            DetalleFacturaHelper.ActualizarDetallesDeCotizacion(obj);
            obj.FechaModificacion = DateTime.Now;
            obj.LocalidadId = obj.Localidad.Id;
            var factura = new Factura(obj);
            factura.LocalidadId = factura.Localidad.Id;
           
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public  bool ActualizarMontos(Factura obj)
        {

   

            obj.FechaModificacion = DateTime.Now;
            var factura = new Factura(obj);
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public override List<Factura> ConsultarPorQuery(string query)
        {


            var resultado = base.ConsultarPorQuery(query);
            resultado.ForEach(f =>
            {
                f.Moneda = monedaService.ObtenerPorId(f.MonedaId);
            });
           
            return resultado;
        }

        public override PagedResult<Factura> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
           
            PagedResult<Factura> resultadoPaginacion = facturaDb.ObtenerListaCotizacionPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);

            return resultadoPaginacion;
        }

        public override PagedResult<Factura> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            var resultado= base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
            resultado.Result.ForEach(f => {
                Task<List<DetalleFactura>> t_detalles = Task.Factory.StartNew<List<DetalleFactura>>((arg) =>
                {
                    int facturaId = (int)arg;
                    var detallesFactura = detallesService.ObtenerDetallesPadresDeFactura(facturaId);
                    return detallesFactura;
                }, f.Id);

                if (t_detalles.IsFaulted)
                    throw new Exception("Error al tratar de procesar la información de la factura");

                f.DetallesFactura = t_detalles.Result;

            });
            return resultado;
        }

        public override PagedResult<Factura> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            PagedResult<Factura> resultadoPaginacion= facturaDb.ObtenerListaCotizacionPaginadoYFiltro(buqueda);
            return resultadoPaginacion;
        }

        public Factura ObtenerPorNumeroFactura(string numeroFactura)
        {
            return facturaDb.ObtenerPorNumeroFactura(numeroFactura);
        }

        public bool FacturarCotizacion(int id, string userId)
        {
            if (userId == null)
                throw new Exception("Session expirada");
            var factura = this.ObtenerPorId(id);
           
            factura = DtoHelper.LlenarCamposPorDefectoEdicion(factura) as Factura;
            var usuario = MemoryCache.Default.Get(userId) as IUserData;
            var cliente = clienteService.ObtenerPorId(factura.ClienteId);
            factura.TipoNCF = string.IsNullOrEmpty(factura.TipoNCF) && !string.IsNullOrEmpty(factura.Cliente?.TipoNcf) ? factura.Cliente?.TipoNcf : factura.TipoNCF;
            factura.LocalidadId = usuario.LocalidadId;
            factura.AlmacenId = cliente.AlmacenId.HasValue && cliente.AlmacenId > 0 ? cliente.AlmacenId.Value :
                (usuario.AlmacenId>0?usuario.AlmacenId:almacenService.ObtenerAlmacenesPorLocalidad(usuario.LocalidadId).Where(a=>a.Codigo.ToLower()!="def").FirstOrDefault().Id);

            var newFactura = new Factura(factura);
            newFactura.Estado = (char)EstadosFactura.Facturado;
           
            var facturacionResult = facturaService.Insertar(newFactura);
            factura.Estado = (char)EstadosFactura.Convertido;
            return facturaService.ActualizarMontos(factura);
        }

        public bool DuplicarCotizacion(int id, string userId, int? clienteId)
        {
            try
            {
                if (userId == null)
                    throw new Exception("Session expirada");
                var factura = this.ObtenerPorId(id);
              

                factura = DtoHelper.LlenarCamposPorDefectoEdicion(factura) as Factura;
                var usuario = MemoryCache.Default.Get(userId) as IUserData;
                Cliente cliente = clienteId.HasValue && clienteId > 0 ? clienteService.ObtenerPorId(clienteId.Value) : clienteService.ObtenerPorId(factura.ClienteId); 
                factura.LocalidadId = usuario.LocalidadId;
                factura.ClienteId = cliente.Id;
                factura.TipoNCF = cliente.TipoNcf;
                factura.AlmacenId = cliente.AlmacenId.HasValue && cliente.AlmacenId > 0 ? cliente.AlmacenId.Value :
                    (usuario.AlmacenId > 0 ? usuario.AlmacenId : almacenService.ObtenerAlmacenesPorLocalidad(usuario.LocalidadId).Where(a => a.Codigo.ToLower() != "def").FirstOrDefault().Id);

                var newFactura = new Factura(factura);

                var facturacionResult = this.Insertar(newFactura);
                return true;
            }

            catch 
            {
                return false;
            }
           
        }
    }
}
