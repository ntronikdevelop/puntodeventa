﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class ClienteService : BaseService<Cliente>, IClienteService
    {
        private IMonedaService monedaService;

        public ClienteService()
        {
            this.monedaService = new MonedaService();
        }

        public override Cliente Insertar(Cliente obj)
        {
            obj.Codigo = SecuenciasHelper.CrearControlDeClientes();
            return base.Insertar(obj);
        }

        public override bool Actualizar(Cliente obj)
        {
            obj.Codigo = string.IsNullOrEmpty(obj.Codigo) ? SecuenciasHelper.CrearControlDeClientes() : obj.Codigo;
            return base.Actualizar(obj);
        }

        public override PagedResult<Cliente> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Cliente> resultadoPaginacion = base.ObtenerTodosPaginado(page,size,activos);

            

            return resultadoPaginacion;
        }

        public override PagedResult<Cliente> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            PagedResult<Cliente> resultado= base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
          

            return resultado;

        }

        public override Cliente ObtenerPorId(int id)
        {
            Cliente resultado = base.ObtenerPorId(id);
             return resultado;

        }
    }
}
