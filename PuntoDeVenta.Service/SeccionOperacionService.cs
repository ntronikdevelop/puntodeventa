﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class SeccionOperacionService : IdentityBaseService<SeccionOperaciones>, ISeccionOperacionesService
    {
        public SeccionOperacionService() : base(new SeccionOperacionesManager()) { }

        private SeccionOperacionesDao dbService = new SeccionOperacionesDao();

        public override PagedResult<SeccionOperaciones> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            return dbService.ObtenerListaFiltrada(buqueda);
        }
        public override PagedResult<SeccionOperaciones> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            return dbService.ObtenerListaPaginadoYFiltro(buqueda);
        }

    }
}
