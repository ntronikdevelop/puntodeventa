﻿using PuntoDeVenta.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;

namespace PuntoDeVenta.Service
{
  public  class ProductoEnLocalidadService : IProductosEnLocalidadService
    {
        private IAlmacenService almacenes;
        private IAlmacenProductoService productosEnAlmacen;
        private IUnidadesService unidades;

        public ProductoEnLocalidadService()
        {
            this.almacenes = new AlmacenService();
            this.productosEnAlmacen = new AlmacenProductoService();
            this.unidades = new UnidadesService();
        }

        public ProductoEnLocalidadService(IAlmacenService newAlmacenService, IAlmacenProductoService newProductoService, IUnidadesService newUnidades)
        {
            this.almacenes = newAlmacenService;
            this.productosEnAlmacen = newProductoService;
            this.unidades = newUnidades;
        }


        public ProductosEnLocalidad ObtenerExistenciaDeProductoEnLocalidad(int localidadId, int productoId)
        {
            var listaAlmacenes = almacenes.ConsultarPorQuery($"select * from almacen where LocalidadId={localidadId} and Activo=1;");
            string ids = string.Join(",", listaAlmacenes.Select(x => x.Id).ToArray());
            var listaDeProductos = productosEnAlmacen.ConsultarPorQuery($"select * from almacenproducto where AlmacenId in ({ids}) and ProductoId ={productoId} and Activo=1;");
            listaDeProductos.ForEach(p => {
                p.Unidad = unidades.ObtenerPorId(p.UnidadId);
                p.NombreAlmacen = listaAlmacenes.Where(a => a.Id == p.AlmacenId).FirstOrDefault().Nombre;
            });

            ProductosEnLocalidad resultado = new ProductosEnLocalidad() { ProductoPorAlmacen = listaDeProductos };
            return resultado;

        }
    }
}
