﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class PagoService : BaseService<Pago>, IPagoService
    {
        IDetallePagoService detallesService = new DetallePagoService();
        IClienteService clienteService = new ClienteService();
        IMonedaService monedaService = new MonedaService();
        IFacturaService facturaService = new FacturaService();
        IBalanceClienteService balanceClienteService = new BalanceClienteService();


        public PagoService():base(new PagoManager()) { }

        public override PagedResult<Pago> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            var resultado= base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);

  

            return resultado;
        }

        public override Pago ObtenerPorId(int id)
        {

            var resultado = base.ObtenerPorId(id);


          

            return resultado;

        }

        public override bool Actualizar(Pago obj)
        {
            throw new Exception("No se puede actualizar un pago. Favor Anular;");
        }

        public override Pago Insertar(Pago obj)
        {
            Factura factura = facturaService.ObtenerPorNumeroFactura(obj.NoFactura);
            if (factura == null)
                throw new Exception("La factura no existe");

            if (factura.MontoAdeudado != obj.MontoPendienteActual)
                throw new Exception("El monto pendiente de la factura no está actualizado. favor consultar nuevamente");
            if (factura.MontoAdeudado <=0)
                throw new Exception("La factura ya está  pagada. No se admiten más pagos.");

            var pagosAnteriores = base.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja() {
                Activo=true,
                Filtros= new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo="NoFactura",
                        TipoCampo=(int)TipoFiltro.EqualString,
                        Valor=obj.NoFactura
                    }
                }
            });
            factura.MontoPagado += obj.MontoPagado;
            obj.MontoTotal = factura.MontoTotal;
            decimal porcientoComision = factura.PorcentajeVendedor / (factura.MontoGrabado);

            factura.MontoPagado = factura.MontoPagado > factura.MontoTotal ? factura.MontoTotal : factura.MontoPagado;
            factura.MontoAdeudado -= obj.MontoPagado;
            factura.MontoAdeudado = factura.MontoAdeudado < 0 ? 0 : factura.MontoAdeudado;
            obj.MontoPendiente = factura.MontoAdeudado;
            if (factura.MontoAdeudado == 0)
                factura.Estado = 'P';
            facturaService.ActualizarMontos(factura);
            var balanceCliente = balanceClienteService.BalanceDeClientePorMoneda(obj.ClienteId, obj.MonedaId).FirstOrDefault();
            if (balanceCliente != null) {
                balanceCliente.MontoAdeudado -= obj.MontoPagado;
                balanceCliente.MontoAdeudado = balanceCliente.MontoAdeudado <0 ? 0 : balanceCliente.MontoAdeudado;
                balanceClienteService.Actualizar(balanceCliente);
            }
            decimal porcentajeImpuesto = factura.MontoItbis / factura.MontoTotal;
            decimal montoPagoNetoParaComision = (obj.MontoPagado - (obj.MontoPagado * porcentajeImpuesto));
            obj.PorcentajeVendedor =( pagosAnteriores.Result!=null && pagosAnteriores.Result.Sum(x=>x.PorcentajeVendedor)>=factura.PorcentajeVendedor)?0: (montoPagoNetoParaComision * porcientoComision);

            var resultado = base.Insertar(obj);


            return resultado;

        }

        public  Pago PagoDeFacturacion(Pago obj)
        {
           
            var resultado = base.Insertar(obj);
            return resultado;

        }

        public ReporteRecibosDto ReporteDeIngresos(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo=true,
                Filtros= new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      },
                      string.IsNullOrEmpty(busqueda.NoFactura)?new FiltroBusqueda()
                      {
                          Campo="NoFactura",
                          TipoCampo=11,
                          Valor=""
                      }: new FiltroBusqueda()
                      {
                          Campo="NoFactura",
                          TipoCampo=0,
                          Valor=busqueda.NoFactura
                      }
                }
            }).Result;

            ReporteRecibosDto reporte = new ReporteRecibosDto()
            {
                ClienteId = busqueda.clienteId,
                Cliente = busqueda.clienteId.HasValue ? recibos.FirstOrDefault().Cliente : null,
                Pagos = recibos,
                RecibosPorMoneda = new List<RecibosPorMoneda>(),
                Total = 0
            };

            var recibosPorMoneda = recibos.GroupBy(x => x.Moneda.Codigo).ToList();
          
            recibosPorMoneda.ForEach(y => {
                decimal montoTotalAdeudado = 0;
                decimal montoTotalPagado = y.Sum(x => x.MontoPagado);
                var gruposPorFacturas = y.GroupBy(x => x.NoFactura).ToList();
               
                gruposPorFacturas.ForEach(grupo => {
                    if (!grupo.Any(x => x.MontoPendiente <= 0))
                    {
                        montoTotalAdeudado += grupo.OrderBy(x => x.Secuencia).OrderBy(x=>x.Id).LastOrDefault().MontoPendiente;
                    }
                       
                });

                reporte.RecibosPorMoneda.Add(new RecibosPorMoneda()
                {
                    CodigoMoneda = y.Key,
                    Moneda = y.FirstOrDefault().Moneda,
                    Pagos = y.ToList(),
                    MontoAdeudado = montoTotalAdeudado,
                    MontoPagado = montoTotalPagado,
                    MontoPorcentajeVendedor = y.Sum(x => x.PorcentajeVendedor)
                });
            });

            return reporte;
        }

        public List<Pago> ReporteDeIngresosExcel(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      },
                      string.IsNullOrEmpty(busqueda.NoFactura)?new FiltroBusqueda()
                      {
                          Campo="NoFactura",
                          TipoCampo=11,
                          Valor=""
                      }: new FiltroBusqueda()
                      {
                          Campo="NoFactura",
                          TipoCampo=0,
                          Valor=busqueda.NoFactura
                      }
                }
            }).Result;

          

           
            return recibos;
        }

        public ReporteComisiones ComisionesPorCobros(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      }, new FiltroBusqueda()
                      {
                          Campo="PorcentajeVendedor",
                          TipoCampo=6,
                          Valor="0"
                      }
                }
            }).Result;

            ReporteComisiones reporte = new ReporteComisiones()
            {
               ComisionesPorMonedas= new List<ComisionesPorMoneda>()
            };

            var recibosPorMoneda = recibos.GroupBy(x => x.Moneda.Codigo).ToList();

            recibosPorMoneda.ForEach(y => {
                reporte.ComisionesPorMonedas.Add(new ComisionesPorMoneda()
                {
                    CodigoMoneda = y.FirstOrDefault().Moneda.Codigo,
                    Moneda =$"{y.FirstOrDefault().Moneda.Codigo} {y.FirstOrDefault().Moneda.Nombre}",
                    MontoTotal=y.Sum(x=>x.PorcentajeVendedor),
                    Detalles = y.Select(x => new DetalleComision()
                    {
                        Cliente= $"{x.Cliente.CodigoYNombre}",
                        Fecha=x.FechaCreacion,
                        MontoTotalFactura=x.MontoTotal,
                        MontoPagado=x.MontoPagado,
                        Monto=x.PorcentajeVendedor,
                        Referencia=$"{x.Secuencia} - {x.NoFactura}",
                        Vendedor=$"{x.Vendedor.CodigoYNombre}"
                    }).ToList()

                });
            });

            return reporte;
        }

        public List<DetalleComision> ComisionesPorCobrosExcel(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaCreacion",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      }, new FiltroBusqueda()
                      {
                          Campo="PorcentajeVendedor",
                          TipoCampo=6,
                          Valor="0"
                      }
                }
            }).Result;

            return recibos.Select(x => new DetalleComision()
            {
                CodigoMoneda = x.Moneda.Codigo,
                Cliente = $"{x.Cliente.CodigoYNombre}",
                Fecha = x.FechaCreacion,
                Monto = x.PorcentajeVendedor,
                Referencia = $"{x.Secuencia} - {x.NoFactura}",
                Vendedor = $"{x.Vendedor.CodigoYNombre}",
                MontoTotalFactura = x.MontoTotal,
                MontoPagado = x.MontoPagado,
            }).ToList();
        }

        public override void Eliminar(int id, string userNamer)
        {
            var obj = this.ObtenerPorId(id);
            obj.Activo = false;
             
            Factura factura = facturaService.ObtenerPorNumeroFactura( obj.NoFactura);

            factura.MontoPagado -= obj.MontoPagado;
            factura.MontoPagado = factura.MontoPagado <0 ? 0 : factura.MontoPagado;
            factura.MontoAdeudado += obj.MontoPagado;
            factura.MontoAdeudado = factura.MontoAdeudado >factura.MontoTotal ? factura.MontoTotal : factura.MontoAdeudado;
            if (factura.Estado != 'F')
                factura.Estado = 'F';
            facturaService.ActualizarMontos(factura);
            BalanceCliente balanceCliente = balanceClienteService.BalanceDeClientePorMoneda(obj.ClienteId, obj.MonedaId).FirstOrDefault();
            if (balanceCliente != null)
            {
                balanceCliente.MontoAdeudado += obj.MontoPagado;
                balanceClienteService.Actualizar(balanceCliente);
            }
            obj.ModificadoPor = userNamer;
            obj.FechaModificacion = DateTime.Now;
            base.Actualizar(obj);
        }

       
    }
}
