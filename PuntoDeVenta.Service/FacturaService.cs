﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess;
using System.Configuration;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.Constants.Lib;

namespace PuntoDeVenta.Service
{
    public class FacturaService : BaseService<Factura>, IFacturaService
    {
        private IDetalleFacturaService detallesService;
        private IMonedaService monedaService;
        private IClienteService clienteService;
        private IZonaService zonaService;
        private IVendedorService vendedorService;
        private ILocalidadService localidadService;
        private IBalanceClienteService balanceService;
        private INotaCreditoService notaCreditoService;
        private IProductoImpuestoService productoImpuestoService;
        private FacturaDao facturaDb = new FacturaDao();


        public FacturaService()
        {
            this.detallesService = new DetalleFacturaService();
            this.monedaService = new MonedaService();
            this.clienteService = new ClienteService();
            this.zonaService = new ZonaService();
            this.localidadService = new LocalidadService();
            this.balanceService = new BalanceClienteService();
            this.productoImpuestoService = new ProductoImpuestoService();
            this.notaCreditoService = new NotaCreditoService();
            this.vendedorService = new VendedorService();

        }
        public CxCDto ObtenerCxCGeneral(DateTime? FechaInicio, DateTime? fechaFinal, Nullable<int> idCliente, Nullable<int> monedaId, int? vendedorId, bool sumarizar = false)
        {

            CxCDto resultado = new CxCDto();

            resultado.Facturas = facturaDb.ObtenerCXC(FechaInicio, fechaFinal, idCliente, monedaId, vendedorId);
            resultado.Facturas = !sumarizar ? resultado.Facturas : (resultado.Facturas.Count > 0 ? SumarizarFacturasPorCliente(resultado.Facturas) : new List<Factura>());
            resultado.Total = resultado.Facturas.Sum(f => f.MontoTotal);
            resultado.FacturasPorMonedas = new List<FacturasPorMoneda>();
            var facturasPorMoneda = resultado.Facturas != null ? resultado.Facturas.GroupBy(x => x.MonedaId).ToList() : null;
            if (facturasPorMoneda != null)
            {
                facturasPorMoneda.ForEach(fm =>
                {
                    resultado.FacturasPorMonedas.Add(new FacturasPorMoneda()
                    {
                        Moneda = fm.FirstOrDefault().Moneda,
                        CodigoMoneda = fm.FirstOrDefault().Moneda.Codigo,
                        Facturas = !sumarizar ? fm.OrderBy(x=>x.ClienteNombre).ToList() : ( fm.Count() > 0?SumarizarFacturasPorCliente(fm.OrderBy(x => x.ClienteNombre).ToList()): new List<Factura>()),
                        MontoGrabado = fm.Sum(x => x.MontoGrabado),
                        MontoPorcentajeVendedor = fm.Sum(x => x.PorcentajeVendedor),
                        MontoItbis = fm.Sum(x => x.MontoItbis),
                        MontoTotal = fm.Sum(x => x.MontoTotal),
                        MontoPagado = fm.Sum(x => x.MontoPagado),
                        MontoAdeudado = fm.Sum(x => x.MontoAdeudado),
                        MontoDescuento = fm.Sum(x => x.MontoDescuento),
                        MontoNotaCredito = fm.Sum(x => x.MontoNotaCreditoAplicada),

                    });
                });
            }

            return resultado;
        }

        List<Factura> SumarizarFacturasPorCliente(List<Factura> facturas)
        {
            List<Factura> resultado = new List<Factura>();
            facturas.GroupBy(x => x.ClienteId).ToList().ForEach(grupoFacturas =>
            {
                var summ = new Factura()
                {
                    Activo = true,
                    Id=grupoFacturas.FirstOrDefault().Id,
                    AlmacenId = 0,
                    MontoAdeudado = grupoFacturas.Sum(x => x.MontoAdeudado),
                    MontoNotaCreditoAplicada = grupoFacturas.Sum(x => x.MontoNotaCreditoAplicada),
                    NotaCreditoAplicada = "",
                    Cliente = grupoFacturas.FirstOrDefault().Cliente,
                    ClienteId = grupoFacturas.Key,
                    Costo = grupoFacturas.Sum(x => x.Costo),
                    Moneda = grupoFacturas.FirstOrDefault().Moneda,
                    MonedaId = grupoFacturas.FirstOrDefault().MonedaId,
                    MontoDescuento = grupoFacturas.Sum(x => x.MontoDescuento),
                    MontoDevuelto = grupoFacturas.Sum(x => x.MontoDevuelto),
                    MontoGrabado = grupoFacturas.Sum(x => x.MontoGrabado),
                    MontoItbis = grupoFacturas.Sum(x => x.MontoItbis),
                    MontoPagado = grupoFacturas.Sum(x => x.MontoPagado),
                    MontoRecibido = grupoFacturas.Sum(x => x.MontoRecibido),
                    MontoTotal = grupoFacturas.Sum(x => x.MontoTotal),
                    FechaModificacion=DateTime.Now,
                    NumeroFactura = "SUMARIZADO",
                    Rnc = "SUMARIZADO",
                    FechaCreacion = DateTime.Now,
                    Detalles = "SUMARIZADO",
                    FechaFacturado = DateTime.Now,
                    NCF = "SUMARIZADO",
                    NoDocumento = "SUMARIZADO",
                    PorcientoDescuento = grupoFacturas.Average(x => x.PorcientoDescuento),
                    PorcentajeVendedor = grupoFacturas.Average(x => x.PorcentajeVendedor),
                    CreadoPor = grupoFacturas.FirstOrDefault()?.CreadoPor,
                };
                resultado.Add(summ);
            });
            return resultado;
        }

        public CxCDto ObtenerVentas(DateTime? FechaInicio = default(DateTime?), DateTime? fechaFinal = default(DateTime?), int? clienteId = null, int? vendedorId = null)
        {
            CxCDto resultado = new CxCDto();

            //  resultado.Facturas = this.ConsultarPorQuery(query);
            resultado.Facturas = facturaDb.ObtenerVentas(FechaInicio ?? new DateTime(), fechaFinal ?? DateTime.Now, clienteId, null, vendedorId);
            resultado.Total = resultado.Facturas.Sum(f => f.MontoTotal);
            resultado.FacturasPorMonedas = new List<FacturasPorMoneda>();
            var facturasPorMoneda = resultado.Facturas != null ? resultado.Facturas.OrderByDescending(x => x.FechaFacturado).GroupBy(x => x.MonedaId).ToList() : null;
            if (facturasPorMoneda != null)
            {
                facturasPorMoneda.ForEach(fm =>
                {
                    resultado.FacturasPorMonedas.Add(new FacturasPorMoneda()
                    {
                        Moneda = fm.FirstOrDefault().Moneda,
                        CodigoMoneda = fm.FirstOrDefault().Moneda.Codigo,
                        Facturas = fm.ToList(),
                        MontoGrabado = fm.Sum(x => x.MontoGrabado),
                        MontoItbis = fm.Sum(x => x.MontoItbis),
                        MontoTotal = fm.Sum(x => x.MontoTotal),
                        MontoPagado = fm.Sum(x => x.MontoPagado),
                        MontoPorcentajeVendedor = fm.Sum(x => x.PorcentajeVendedor),
                        MontoAdeudado = fm.Sum(x => x.MontoAdeudado),
                        MontoDescuento = fm.Sum(x => x.MontoDescuento),
                        MontoNotaCredito = fm.Sum(x => x.MontoNotaCreditoAplicada)
                    });
                });
            }

            return resultado;
        }



        public override Factura Insertar(Factura obj)
        {
            CrearNCF(obj);
            obj.Vendedor = obj.Vendedor == null && obj.VendedorId.HasValue && obj.VendedorId.Value > 0 ? this.vendedorService.ObtenerPorId(obj.VendedorId.Value) : obj.Vendedor;
            obj.ZonaId = obj.Vendedor != null ? (obj.Vendedor.ZonaId.HasValue ? obj.Vendedor.ZonaId.Value : 0) : 0;
            NotaCredito notaCreditoAplicada = new NotaCredito();
            if (!string.IsNullOrEmpty(obj.NotaCreditoAplicada))
                notaCreditoAplicada = notaCreditoService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Secuencia", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.NotaCreditoAplicada } }
                }).Result.FirstOrDefault();

            if (obj.DetallesFactura.Count <= 0)
                throw new Exception("La factura no tiene articulos");
            List<DetalleFactura> detalles = obj.DetallesFactura;

            if (obj.Vendedor != null && obj.Vendedor.Id > 0)
            {
                detalles.ForEach(d =>
                {
                    decimal comision = obj.Vendedor.ComisionFija ? ((d.MontoGrabado) * obj.Vendedor.PorcentajeComision) : 0;
                    comision += obj.Vendedor.ComisionPorProducto ? ((d.MontoGrabado) * d.Producto?.PorcentajeVendedor).Value : 0;
                    d.PorcentajeVendedor = comision;

                });
                obj.PorcentajeVendedor = detalles.Sum(x => x.PorcentajeVendedor);

            }
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(detalles.Select(x => x.ProductoId).ToArray());

            detalles.ForEach(d =>
            {


                d.MontoDescuento = d.Monto * d.PorcientoDescuento / 100;
                d.MontoGrabado = (d.Monto - d.MontoDescuento) * d.Cantidad;
                d.MontoItbis = (impuestos.Where(i => i.ProductoId == d.ProductoId).Sum(i => i.Impuesto.Porciento) * (d.MontoGrabado));

                d.MontoTotal = ((d.MontoGrabado) + d.MontoItbis);
            });
            obj.MontoGrabado = detalles.Sum(x => x.MontoGrabado);
            obj.MontoItbis = detalles.Sum(x => x.MontoItbis);
            obj.MontoTotal = detalles.Sum(x => x.MontoTotal);
            obj.MontoDescuento = detalles.Sum(x => x.MontoDescuento * x.Cantidad);
            obj.MontoAdeudado = obj.MontoTotal - obj.MontoNotaCreditoAplicada - obj.MontoPagado;
            obj.MontoAdeudado = obj.MontoAdeudado < 0 ? 0 : obj.MontoAdeudado;
            obj.MontoDevuelto = obj.MontoRecibido - obj.MontoTotal;

            obj.NumeroFactura = SecuenciasHelper.CrearControlDeFactura();
            obj.FechaFacturado = DateTime.Now;
            var localidadTemp = obj.Localidad;
            obj.Estado = (obj.MontoPagado == obj.MontoTotal && obj.MontoAdeudado == 0) ? (char)EstadosFactura.Pagado : (char)EstadosFactura.Facturado;
            obj = FacturaHelper.AplicarNotaDeCredito(obj, notaCreditoAplicada, out notaCreditoAplicada);
            if (obj.MontoAdeudado > 0)
            {
                var balance = balanceService.BalanceDeClientePorMoneda(obj.ClienteId, obj.MonedaId).FirstOrDefault() ??
                    new BalanceCliente() { ClienteId = obj.ClienteId, MonedaId = obj.MonedaId, Id = 0, Activo = true };
                obj.Cliente = obj.Cliente != null && obj.Cliente.Id > 0 ? obj.Cliente : clienteService.ObtenerPorId(obj.ClienteId);

                balance.MontoAdeudado += obj.MontoAdeudado;
                if (balance.MonedaId == obj.Cliente.MonedaId && obj.Cliente.MontoLimiteDeCredito > 0 && balance.MontoAdeudado > obj.Cliente.MontoLimiteDeCredito)
                    throw new Exception("El cliente a sobrepasado el limite de credito asignado. No se puede facturar");
                if (balance.Id > 0)
                    balanceService.Actualizar(balance);
                else
                    balanceService.Insertar(balance);

            }
            obj.MontoDevuelto = obj.MontoDevuelto < 0 ? 0 : obj.MontoDevuelto;

            var factura = base.Insertar(obj);
            if (!string.IsNullOrEmpty(notaCreditoAplicada.Secuencia))
                notaCreditoService.Actualizar(notaCreditoAplicada);
            factura.DetallesFactura = detalles;
            factura.Localidad = localidadTemp;
            DetalleFacturaHelper.InsertarDetalles(factura);
            if (obj.MontoPagado > 0 && obj.Pagos != null && obj.Pagos.Count > 0)
            {
                string secuenciaPago = SecuenciasHelper.CrearControlDePagos();
                foreach (var pago in obj.Pagos)
                {
                    pago.NoFactura = obj.NumeroFactura;
                    pago.CreadoPor = obj.CreadoPor;
                    pago.FechaCreacion = obj.FechaCreacion;
                    pago.MontoPendienteActual = pago.MontoPendiente;
                    pago.Secuencia = secuenciaPago;
                    FacturaHelper.AplicarPagoFactura(pago);
                }
            }

            return factura;
        }

        private Factura CrearNCF(Factura obj)
        {
            if (!string.IsNullOrEmpty(obj.TipoNCF) && obj.TipoNCF != "NA")
            {
                var ncfService = new ControlNCFService();
                ControlNCF control = ncfService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Tipo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.TipoNCF } }
                }).Result.FirstOrDefault();
                if (control.Cantidad <= 0)
                    throw new Exception($"Ya no quedan NCF disponibles para {control.Nombre}.");
                obj.NCF = $"{control.Serie}{control.Tipo}{string.Format("{0:00000000}", control.Secuencia)}";
                control.Secuencia++;
                control.Cantidad--;
                control.ControlNumerico++;
                ncfService.Actualizar(control);
            }
            return obj;
        }

        public Factura InsertarFacturaAntigua(Factura obj)
        {
            // CrearNCF(obj);
            obj.ZonaId = obj.Vendedor.ZonaId.HasValue ? obj.Vendedor.ZonaId.Value : 0;
            NotaCredito notaCreditoAplicada = new NotaCredito();
            if (!string.IsNullOrEmpty(obj.NotaCreditoAplicada))
                notaCreditoAplicada = notaCreditoService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Secuencia", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.NotaCreditoAplicada } }
                }).Result.FirstOrDefault();

            obj.NumeroFactura = SecuenciasHelper.CrearControlDeFactura();
            obj.FechaFacturado = obj.FechaFacturado == DateTime.MinValue ? DateTime.Now : obj.FechaFacturado;
            var localidadTemp = obj.Localidad;
            obj.Estado = (char)EstadosFactura.Facturado;
            obj = FacturaHelper.AplicarNotaDeCredito(obj, notaCreditoAplicada, out notaCreditoAplicada);
            if (obj.MontoAdeudado > 0)
            {
                var balance = balanceService.BalanceDeClientePorMoneda(obj.ClienteId, obj.MonedaId).FirstOrDefault() ??
                    new BalanceCliente() { ClienteId = obj.ClienteId, MonedaId = obj.MonedaId, Id = 0, Activo = true };

                balance.MontoAdeudado += obj.MontoAdeudado;

                if (balance.Id > 0)
                    balanceService.Actualizar(balance);
                else
                    balanceService.Insertar(balance);

            }
            obj.MontoDevuelto = obj.MontoDevuelto < 0 ? 0 : obj.MontoDevuelto;

            var factura = base.Insertar(obj);
            if (!string.IsNullOrEmpty(notaCreditoAplicada.Secuencia))
                notaCreditoService.Actualizar(notaCreditoAplicada);
            factura.Localidad = localidadTemp;


            return factura;
        }

        public override PagedResult<Factura> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Factura> resultadoPaginacion = base.ObtenerTodosPaginado(page, size, activos);


            return resultadoPaginacion;
        }

        public override Factura ObtenerPorId(int id)
        {
            Factura resultado = base.ObtenerPorId(id);

            var detallesFactura = detallesService.ObtenerDetallesPadresDeFactura(id).OrderBy(x => x.Id).ToList();
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(detallesFactura.Select(x => x.ProductoId).ToArray());
            detallesFactura.ForEach(d =>
            {
                d.Impuestos = impuestos.Where(x => x.ProductoId == d.ProductoId).ToList();
            });

            resultado.DetallesFactura = detallesFactura;
            return resultado;

        }

        public override void Eliminar(int id, string userName)
        {

            Factura factura = ObtenerPorId(id);
            if (factura.Estado != (char)EstadosFactura.Facturado || factura.MontoPagado > 0 || ((factura.MontoAdeudado + factura.MontoDescuento) < factura.MontoTotal))
                throw new Exception($"La factura {factura.NumeroFactura} ya ha sido procesada. No se puede anular.");

            factura.Estado = (char)EstadosFactura.Anulado;
            var balance = balanceService.BalanceDeClientePorMoneda(factura.ClienteId, factura.MonedaId).FirstOrDefault() ??
                   new BalanceCliente() { ClienteId = factura.ClienteId, MonedaId = factura.MonedaId, Id = 0, Activo = true };

            balance.MontoAdeudado -= factura.MontoAdeudado;
            balance.MontoAdeudado = balance.MontoAdeudado < 0 ? 0 : balance.MontoAdeudado;
            foreach (DetalleFactura detalle in factura.DetallesFactura)
            {
                InventarioEnAlmacenHelper.SumarInventarioAlAlmacen(detalle, factura);

                var detallesHijos = detallesService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "DetallePadreId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = detalle.Id.ToString() } }
                }).Result ?? new List<DetalleFactura>();
                detallesHijos.ForEach(d =>
                {
                    InventarioEnAlmacenHelper.SumarInventarioAlAlmacen(d, factura);
                });
            }
            factura.ModificadoPor = userName;
            factura.FechaModificacion = DateTime.Now;
            if (!string.IsNullOrEmpty(factura.NoDocumento))
            {
                var cotizacionPadre = this.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "NoDocumento", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor=factura.NoDocumento },
                    new FiltroBusqueda(){ Campo = "Estado", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor="D" }
                    }
                }).Result.FirstOrDefault();
                if (cotizacionPadre != null)
                {
                    cotizacionPadre.Estado = (char)EstadosFactura.Cotizado;
                    cotizacionPadre.ModificadoPor = userName;
                    cotizacionPadre.FechaModificacion = DateTime.Now;
                    base.Actualizar(cotizacionPadre);
                }
            }
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            var impuestos = facturaImpuestoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="NoFactura",
                        TipoCampo=3,
                        Valor=!string.IsNullOrEmpty(factura.NumeroFactura)?factura.NumeroFactura:factura.NoDocumento
                    }
                }
            }).Result;
            if (impuestos != null)
            {
                impuestos.ForEach(i =>
                {
                    ;
                    facturaImpuestoService.Eliminar(i.Id, userName);
                });
            }
            base.Actualizar(factura);

        }

        public override bool Actualizar(Factura obj)
        {
            obj.ZonaId = obj.Vendedor != null && obj.Vendedor.ZonaId.HasValue ? obj.Vendedor.ZonaId.Value : 0;
            var viejoBalanceDeFactura = this.ObtenerPorId(obj.Id);

            if (!string.IsNullOrEmpty(viejoBalanceDeFactura.NotaCreditoAplicada) &&
                !string.Equals(obj.NotaCreditoAplicada, viejoBalanceDeFactura.NotaCreditoAplicada))
                throw new Exception("La factura ya tiene una nota de crédito aplicada. Solo se puede aplicar una nota de crédito por factura");

            NotaCredito notaCreditoAplicada = new NotaCredito();
            if (!string.IsNullOrEmpty(obj.NotaCreditoAplicada) &&
                !string.Equals(obj.NotaCreditoAplicada, viejoBalanceDeFactura.NotaCreditoAplicada))
            {
                notaCreditoAplicada = notaCreditoService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Secuencia", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.NotaCreditoAplicada } }
                }).Result.FirstOrDefault();
                obj = FacturaHelper.AplicarNotaDeCredito(obj, notaCreditoAplicada, out notaCreditoAplicada);
                if (!string.IsNullOrEmpty(notaCreditoAplicada.Secuencia))
                    notaCreditoService.Actualizar(notaCreditoAplicada);
            }



            if (viejoBalanceDeFactura.MontoPagado > 0 || viejoBalanceDeFactura.MontoRecibido > 0 || viejoBalanceDeFactura.MontoAdeudado <= 0)
                throw new Exception("La factura tiene pagos aplicados. No se puede modificar.");
            //  throw new Exception("Funcion no disponible por el momento.");
            if (obj.DetallesFactura.Count <= 0)
                throw new Exception("La factura no tiene articulos");
            if (obj.Vendedor != null && obj.Vendedor.Id > 0)
            {
                obj.DetallesFactura.ForEach(d =>
                {
                    decimal comision = obj.Vendedor.ComisionFija ? ((d.MontoGrabado) * obj.Vendedor.PorcentajeComision) : 0;
                    comision += obj.Vendedor.ComisionPorProducto ? ((d.MontoGrabado) * d.Producto?.PorcentajeVendedor).Value : 0;
                    d.PorcentajeVendedor = comision;
                });
                obj.PorcentajeVendedor = obj.DetallesFactura.Sum(x => x.PorcentajeVendedor);
            }
            var impuestos = productoImpuestoService.ObtenerImpuestosDeProductoS(obj.DetallesFactura.Select(x => x.ProductoId).ToArray());

            obj.DetallesFactura.ForEach(d =>
            {


                d.MontoDescuento = d.Monto * d.PorcientoDescuento / 100;
                d.MontoGrabado = (d.Monto - d.MontoDescuento) * d.Cantidad;
                d.MontoItbis = (impuestos.Where(i => i.ProductoId == d.ProductoId).Sum(i => i.Impuesto.Porciento) * (d.MontoGrabado));

                d.MontoTotal = ((d.MontoGrabado) + d.MontoItbis);
            });
            obj.MontoGrabado = obj.DetallesFactura.Sum(x => x.MontoGrabado);
            obj.MontoItbis = obj.DetallesFactura.Sum(x => x.MontoItbis);
            obj.MontoTotal = obj.DetallesFactura.Sum(x => x.MontoTotal);
            obj.MontoDescuento = obj.DetallesFactura.Sum(x => x.MontoDescuento * x.Cantidad);
            obj.MontoAdeudado = obj.MontoTotal - obj.MontoNotaCreditoAplicada - obj.MontoPagado;
            obj.MontoAdeudado = obj.MontoAdeudado < 0 ? 0 : obj.MontoAdeudado;
            obj.MontoDevuelto = obj.MontoRecibido - obj.MontoTotal;

            DetalleFacturaHelper.ActualizarDetallesDeFactura(obj);
            obj.FechaModificacion = DateTime.Now;
            obj.LocalidadId = obj.Localidad.Id;
            var factura = new Factura(obj);
            factura.LocalidadId = factura.Localidad.Id;
            if (obj.MontoAdeudado > 0)
            {
                var balance = balanceService.BalanceDeClientePorMoneda(obj.ClienteId, obj.MonedaId).FirstOrDefault() ??
                    new BalanceCliente() { ClienteId = obj.ClienteId, MonedaId = obj.MonedaId, Id = 0, Activo = true };
                balance.MontoAdeudado += (obj.MontoAdeudado - viejoBalanceDeFactura.MontoAdeudado);

                if (balance.Id > 0)
                    balanceService.Actualizar(balance);
                else
                    balanceService.Insertar(balance);

            }
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public bool ActualizarMontos(Factura obj)
        {



            obj.FechaModificacion = DateTime.Now;
            var factura = new Factura(obj);
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public override List<Factura> ConsultarPorQuery(string query)
        {


            var resultado = base.ConsultarPorQuery(query);
            resultado.ForEach(f =>
            {
                f.Moneda = monedaService.ObtenerPorId(f.MonedaId);
            });

            return resultado;
        }

        public override PagedResult<Factura> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {

            PagedResult<Factura> resultadoPaginacion = base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);


            return resultadoPaginacion;
        }

        public override PagedResult<Factura> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            var resultado = base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
            resultado.Result.ForEach(f =>
            {
                Task<List<DetalleFactura>> t_detalles = Task.Factory.StartNew<List<DetalleFactura>>((arg) =>
                {
                    int facturaId = (int)arg;
                    var detallesFactura = detallesService.ObtenerDetallesPadresDeFactura(facturaId);
                    return detallesFactura;
                }, f.Id);

                if (t_detalles.IsFaulted)
                    throw new Exception("Error al tratar de procesar la información de la factura");

                f.DetallesFactura = t_detalles.Result;

            });
            return resultado;
        }

        public override PagedResult<Factura> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            PagedResult<Factura> resultadoPaginacion = base.ObtenerListaPaginadoYFiltro(buqueda);
            return resultadoPaginacion;
        }

        public Factura ObtenerPorNumeroFactura(string numeroFactura)
        {
            return facturaDb.ObtenerPorNumeroFactura(numeroFactura);
        }

        public List<EstadoCuentaCliente> ObtenerEstadoCuenta(DateTime? FechaInicio, DateTime? fechaFinal, int? idCliente, int? monedaId)
        {
            return facturaDb.ObtenerEstadoCuenta(FechaInicio, fechaFinal, idCliente, monedaId);
        }

        public ReporteComisiones ComisionesPorVentas(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaFacturado",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaFacturado",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      }, new FiltroBusqueda()
                      {
                          Campo="PorcentajeVendedor",
                          TipoCampo=6,
                          Valor="0"
                      },
                        new FiltroBusqueda()
                      {
                          Campo="Estado",
                          TipoCampo=12,
                          Valor="'F','P'"
                      }
                }
            }).Result;

            ReporteComisiones reporte = new ReporteComisiones()
            {
                ComisionesPorMonedas = new List<ComisionesPorMoneda>()
            };

            var recibosPorMoneda = recibos.GroupBy(x => x.Moneda.Codigo).ToList();

            recibosPorMoneda.ForEach(y =>
            {
                reporte.ComisionesPorMonedas.Add(new ComisionesPorMoneda()
                {
                    CodigoMoneda = y.FirstOrDefault().Moneda.Codigo,
                    Moneda = $"{y.FirstOrDefault().Moneda.Codigo} {y.FirstOrDefault().Moneda.Nombre}",
                    MontoTotal = y.Sum(x => x.PorcentajeVendedor),
                    Detalles = y.Select(x => new DetalleComision()
                    {
                        Cliente = $"{x.Cliente.CodigoYNombre}",
                        Fecha = x.FechaFacturado.Value,
                        Monto = x.PorcentajeVendedor,
                        Referencia = $"{x.NumeroFactura}",
                        Vendedor = $"{x.Vendedor.CodigoYNombre}",
                        MontoTotalFactura = x.MontoTotal,
                        MontoGrabadoFactura = x.MontoGrabado,
                        MontoPagado = x.MontoPagado,
                    }).ToList()

                });
            });

            return reporte;
        }

        public List<DetalleComision> ComisionesPorVentasExcel(BusquedaReporteRecibos busqueda)
        {
            var recibos = this.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="FechaFacturado",
                        TipoCampo=9,
                        Valor=busqueda.fechaInicial.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      new FiltroBusqueda()
                    {
                        Campo="FechaFacturado",
                        TipoCampo=10,
                        Valor=busqueda.fechaFinal.ToString("yyyy-MM-dd HH:mm:ss")
                    },
                      !busqueda.clienteId.HasValue?new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="ClienteId",
                          TipoCampo=1,
                          Valor=busqueda.clienteId.Value.ToString()
                      },
                      !busqueda.monedaId.HasValue?new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="MonedaId",
                          TipoCampo=1,
                          Valor=busqueda.monedaId.Value.ToString()
                      },
                        !busqueda.vendedorId.HasValue?new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=4,
                          Valor="0"
                      }: new FiltroBusqueda()
                      {
                          Campo="VendedorId",
                          TipoCampo=1,
                          Valor=busqueda.vendedorId.Value.ToString()
                      }, new FiltroBusqueda()
                      {
                          Campo="PorcentajeVendedor",
                          TipoCampo=6,
                          Valor="0"
                      },
                        new FiltroBusqueda()
                      {
                          Campo="Estado",
                          TipoCampo=12,
                          Valor="'F','P'"
                      }
                }
            }).Result;







            return recibos.Select(x => new DetalleComision()
            {
                CodigoMoneda = x.Moneda.Codigo,
                Cliente = $"{x.Cliente.CodigoYNombre}",
                Fecha = x.FechaFacturado.Value,
                Monto = x.PorcentajeVendedor,
                Referencia = $"{x.NumeroFactura}",
                Vendedor = $"{x.Vendedor.CodigoYNombre}",
                MontoTotalFactura = x.MontoTotal,
                MontoPagado = x.MontoPagado,
            }).ToList();

        }

        public List<FacturaImpuesto> ReporteDeImpuestosDeFacturas(DateTime? fechaInicial, DateTime? fechaFinal)
        {
            List<FacturaImpuesto> resultado = new List<FacturaImpuesto>();
            IFacturaImpuestoService facturaImpuestoService = new FacturaImpuestoService();
            IGastoImpuestoService gastoEmpresaService = new GastoImpuestoService();

            fechaInicial = fechaInicial == null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0) : fechaInicial;
            fechaFinal = fechaFinal == null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.AddDays(1).Day)) : fechaFinal;
            var filtroFechas = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo = "Fecha",
                        TipoCampo=9,
                        Valor=fechaInicial.Value.ToString("yyyy-MM-dd")
                    },
                     new DataAccess.FiltroBusqueda()
                    {
                        Campo = "Fecha",
                        TipoCampo=10,
                        Valor=fechaFinal.Value.ToString("yyyy-MM-dd")
                    },
                     new FiltroBusqueda()
                     {
                         Campo="NoFactura",
                         TipoCampo=0,
                         Valor="FAC"
                     }
                };
            resultado.AddRange(facturaImpuestoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = filtroFechas
            }).Result);

            filtroFechas = new List<DataAccess.FiltroBusqueda>()
                {
                    new DataAccess.FiltroBusqueda()
                    {
                        Campo = "Fecha",
                        TipoCampo=9,
                        Valor=fechaInicial.Value.ToString("yyyy-MM-dd")
                    },
                     new DataAccess.FiltroBusqueda()
                    {
                        Campo = "Fecha",
                        TipoCampo=10,
                        Valor=fechaFinal.Value.ToString("yyyy-MM-dd")
                    }
                };
            resultado.AddRange(gastoEmpresaService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = filtroFechas
            }).Result.Select(x => new FacturaImpuesto()
            {
                Activo = x.Activo,
                CreadoPor = x.CreadoPor,
                Detalles = x.Detalles,
                Fecha = x.Fecha,
                FacturaId = x.GastoEmpresaId,
                Impuesto = x.Impuesto,
                FechaCreacion = x.FechaCreacion,
                FechaModificacion = x.FechaModificacion,
                Id = x.Id,
                ImpuestoId = x.ImpuestoId,
                ModificadoPor = x.ModificadoPor,
                Moneda = x.Moneda,
                MonedaId = x.MonedaId,
                MontoImpuesto = -1 * x.MontoImpuesto,
                NoFactura = x.Referencia,
                Nombre = x.Nombre,
                Factura = new Factura()
                {
                    MontoAdeudado = x.GastoEmpresa.MontoAdeudado,
                    MontoItbis = x.GastoEmpresa.MontoItbis,
                    MontoPagado = x.GastoEmpresa.MontoPagado,
                    MontoDevuelto = x.GastoEmpresa.MontoDevuelto,
                    MontoGrabado = x.GastoEmpresa.MontoGrabado,
                    MontoTotal = x.GastoEmpresa.MontoTotal
                }
            }));

            return resultado;
        }
    }
}
