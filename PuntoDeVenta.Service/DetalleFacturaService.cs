﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class DetalleFacturaService : BaseService<DetalleFactura>, IDetalleFacturaService
    {
        private IProductoService productosService;
        private IImpuestoService impuestosService;
        private IUnidadesService unidades;
        private IAlmacenProductoService inventario;
        private IDetalleFacturaDb internalDbService;


        public DetalleFacturaService(IProductoService newProductoService=null, IImpuestoService newImpuestosService=null, IUnidadesService newUnidadesService=null,
            IAlmacenProductoService newInventarioService=null)
        {
            this.productosService = newProductoService?? new ProductoService();
            this.impuestosService = newImpuestosService?? new ImpuestoService();
            this.unidades = newUnidadesService ?? new UnidadesService();
            this.inventario = newInventarioService ?? new AlmacenProductoService();
            this.internalDbService = new DetalleFacturaManager();
        }


      

        public List<DetalleFactura> ObtenerDetallesPadresDeFactura(int facturaId)
        {
            return internalDbService.ObtenerDetallesPadresDeFactura(facturaId);
        }
        public List<DetalleFactura> ObtenerDetallesDeFactura(int facturaId)
        {
            return internalDbService.ObtenerDetallesDeFactura(facturaId);
        }

        public List<DetalleFactura> ObtenerDetallesHijos(int detallePadreId)
        {
            return internalDbService.ObtenerDetallesHijos(detallePadreId);
        }

        public List<DetalleFactura> DetallesPorProducto(int productoId)
        {
            return internalDbService.DetallesPorProducto(productoId);
        }

        public bool EliminarDetallesDeFactura(int facturaId)
        {
            bool result = false;
            base.ConsultarPorQuery($"delete from detallefactura where facturaId = {facturaId}");
            return result;
        }
    }
}
