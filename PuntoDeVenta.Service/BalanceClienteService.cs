﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao;

namespace PuntoDeVenta.Service
{
    public class BalanceClienteService : BaseService<BalanceCliente>, IBalanceClienteService
    {
        private IMonedaService monedaService;
        private IClienteService clienteService;
        private BalanceClienteDao dbService;

        public BalanceClienteService()
        {
            this.monedaService = new MonedaService();
            this.clienteService = new ClienteService();
            this.dbService = new BalanceClienteDao();
        }

        public List<BalanceCliente> BalanceDeClientePorMoneda(int clienteId, int monedaId)
        {
            return dbService.BalanceDeClientePorMoneda( clienteId,  monedaId);
        }
    }
}
