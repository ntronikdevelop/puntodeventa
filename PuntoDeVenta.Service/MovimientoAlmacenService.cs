﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess.Dao.Interfaces;

namespace PuntoDeVenta.Service
{
    public class MovimientoAlmacenService : BaseService<MovimientoAlmacen>, IMovimientoAlmacenService
    {
        private IProductoService productoService;
        private IMovimientoAlmacenManager internalService;
        private IUnidadesService unidadesService;
        private IBaseDb<Localidad> localidades;
        public MovimientoAlmacenService() : base(new MovimientoAlmacenManager())
        {
            this.productoService = new ProductoService();
            this.internalService = new MovimientoAlmacenManager();
            this.unidadesService = new UnidadesService();
            this.localidades = new LocalidadManager();
        }

        public override PagedResult<MovimientoAlmacen> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            var result= base.ObtenerTodosPaginado(page, size, activos);
          
            return result;
        }

        public override MovimientoAlmacen ObtenerPorId(int id)
        {
            var result= base.ObtenerPorId(id);
          
            return result;
        }

        public MovimientoAlmacen ObtenerExistenciaDeProducto(int productoId, int almacenId)
        {
            
            var result = internalService.ObtenerExistenciaProducto(productoId, almacenId);

            return result;
        }

        

        public List<MovimientoProductoModel> ObtenerMovimientosPorProducto(int? productoId, int? almacenId, string fechaInicio, string fechaFinal)
        {
           return internalService.ObtenerMovimientosPorProducto(productoId, almacenId, fechaInicio, fechaFinal);
        }

       

    }
}
