﻿using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.Service.Interfaces;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Dto;
using System.Linq.Expressions;
using PuntoDeVenta.DataAccess.Dto.Interfaces;
using PuntoDeVenta.DataAccess.Bll.Interfaces;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using System.Configuration;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public  class BaseService<T> : IProcessCrud<T> where T : class, IDatosComunes
    {
        private IBaseDb<T> manager;
        protected bool cacheActivated = bool.Parse(ConfigurationManager.AppSettings["CacheActivated"] ?? "false");

        public BaseService()
        {
            this.manager = new BaseManager<T>();
        }

        public BaseService(IBaseDb<T> newManager)
        {
            this.manager = newManager;
        }

        public virtual bool Actualizar(T obj)
        {
            if (cacheActivated)
                CachingHelper.ActualizarDtoEnCache<T>(obj);
            return manager.Actualizar(obj);
        }

        

   

        public virtual List<T> ObtenerUltimoRegistro()
        {
            var lista = manager.ObtenerUltimoRegistro();
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(lista);
            return lista;
        }

 

        public virtual List<T> ConsultarPorQuery(string query)
        {
            var resultado = manager.ObtenerListaPorConsulta(query);
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado);
            return resultado;
        }

        public virtual void Eliminar(int id, string userName)
        {
            T obj = manager.ObtenerPorId(id);
            obj.Activo = false;
            obj.ModificadoPor = userName;
            manager.Actualizar(obj);
            if (cacheActivated)
                CachingHelper.EliminarDtoDeCache<T>(id);
        }

        public virtual T Insertar(T obj)
        {
            T resultado = manager.Insertar(obj);
            if (cacheActivated)
                CachingHelper.InsertarDtoEnCache<T>(resultado);
            return resultado;
        }

        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            var resultado = manager.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado.Result);
            return resultado;
        }

        public virtual PagedResult<T> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, bool activos, string caracteresABuscar)
        {
            var resultado = manager.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, activos, caracteresABuscar);
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado.Result);
            return resultado;
        }

        public virtual T ObtenerPorId(int id)
        {
            T resultado = cacheActivated ? CachingHelper.ObtenerDtoDeCache<T>(id) : manager.ObtenerPorId(id);
            return resultado;
        }

        public virtual List<T> ObtenerTodos()
        {
            var resultado = manager.ObtenerLista();
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado);
            return resultado;
        }

        public virtual PagedResult<T> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            var resultado = manager.ObtenerListaPaginado(page, size, activos);
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado.Result);
            return resultado;
        }

        public T ObtenerPorId(string id)
        {
            throw new NotImplementedException();
        }

        public virtual PagedResult<T> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            var resultado = manager.ObtenerListaPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);
            if (cacheActivated)
                CachingHelper.InsertarListaDtoEnCache<T>(resultado.Result);
            return resultado;
        }

        public T ObtenerPorIdBasico(int id)
        {
            return this.ObtenerPorId(id);
        }

        public virtual PagedResult<T> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            return manager.ObtenerListaPaginadoYFiltro(buqueda);
        }

        public PagedResult<T> ObtenerListaFiltrada(BuquedaCompleja buqueda)
        {
            return manager.ObtenerListaFiltrada(buqueda);
        }
    }
}
