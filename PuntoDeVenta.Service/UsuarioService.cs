﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{


    public class UsuarioService : IdentityBaseService<Users>, IusuarioService
    {
        public UsuarioService() : base(new UsuarioManager()) { }

        public override PagedResult<Users> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            int valor = activos ? 1 : 0;
            return base.ObtenerTodosPaginado(page, size,activos);
        }



    }
}
