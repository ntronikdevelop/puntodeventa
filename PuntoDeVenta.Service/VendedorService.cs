﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class VendedorService : BaseService<Vendedor>, IVendedorService
    {
        private IMonedaService monedaService;

        public VendedorService()
        {
            this.monedaService = new MonedaService();
        }

        public override Vendedor Insertar(Vendedor obj)
        {
            obj.Codigo = SecuenciasHelper.CrearControlDeVendedores();
            return base.Insertar(obj);
        }

        public override bool Actualizar(Vendedor obj)
        {
            obj.Codigo = string.IsNullOrEmpty(obj.Codigo) ? SecuenciasHelper.CrearControlDeVendedores() : obj.Codigo;
            return base.Actualizar(obj);
        }

        public override PagedResult<Vendedor> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Vendedor> resultadoPaginacion = base.ObtenerTodosPaginado(page,size,activos);

            

            return resultadoPaginacion;
        }

        public override PagedResult<Vendedor> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            PagedResult<Vendedor> resultado= base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
          

            return resultado;

        }

        public override Vendedor ObtenerPorId(int id)
        {
            Vendedor resultado = base.ObtenerPorId(id);
             return resultado;

        }
    }
}
