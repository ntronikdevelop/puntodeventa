﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class NotaCreditoService : BaseService<NotaCredito>, INotaCreditoService
    {
        IMonedaService monedas;
        NotaCreditoDao dbService = new NotaCreditoDao();
        public NotaCreditoService():base(new NotaCreditoManager())
        {
            this.monedas = new MonedaService();
        }

      

        public override NotaCredito ObtenerPorId(int id)
        {
           var resultado = base.ObtenerPorId(id);

          
            return resultado;
        }

        public NotaCredito ObtenerPorSecuencia(string referencia)
        {
            return dbService.ObtenerPorSecuencia(referencia);
        }
    }
}
