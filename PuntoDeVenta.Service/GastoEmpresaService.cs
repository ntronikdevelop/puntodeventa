﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public class GastoEmpresaService : BaseService<GastoEmpresa>, IGastoEmpresaService
    {
        private ISuplidoresService suplidoresService;
        private IMonedaService monedaService;
        private ILocalidadService localidadService;
        private IGastoImpuestoService gastoImpuestoService;
        private GastoEmpresaManager manager = new GastoEmpresaManager();

        public GastoEmpresaService()
        {
            this.gastoImpuestoService = new GastoImpuestoService();
            this.monedaService = new MonedaService();
            this.suplidoresService = new SuplidoresService();
            this.localidadService = new LocalidadService();
        }
        public CxPDto ObtenerCxPGeneral(DateTime? FechaInicio , DateTime? fechaFinal, Nullable<int> suplidorId, Nullable<int> monedaId, Nullable<int> localidadId)
        {
            CxPDto resultado = new CxPDto();
           
            resultado.Gastos = manager.ObtenerCxPGeneral( FechaInicio , fechaFinal,suplidorId, monedaId, localidadId);

            resultado.Total = resultado.Gastos.Sum(f => f.MontoTotal);
            resultado.GastosPorMonedas = new List<GastosPorMoneda>();
            var GastoEmpresasPorMoneda = resultado.Gastos != null ? resultado.Gastos.OrderByDescending(f=>f.FechaEmision).GroupBy(x => x.MonedaId).ToList() : null;
            if (GastoEmpresasPorMoneda != null)
            {
                GastoEmpresasPorMoneda.ForEach(fm =>
                {
                    resultado.GastosPorMonedas.Add(new GastosPorMoneda()
                    {
                        Moneda = fm.FirstOrDefault().Moneda,
                        CodigoMoneda = fm.FirstOrDefault().Moneda.Codigo,
                        Gastos = fm.ToList(),
                        MontoGrabado = fm.Sum(x => x.MontoGrabado),
                        MontoItbis = fm.Sum(x => x.MontoItbis),
                        MontoTotal = fm.Sum(x => x.MontoTotal),
                        MontoPagado = fm.Sum(x => x.MontoPagado),
                        MontoAdeudado = fm.Sum(x=>x.MontoAdeudado),
                        
                    });
                });
            }

            return resultado;
        }

       

 

        public override GastoEmpresa Insertar(GastoEmpresa obj)
        {
            obj.ReferenciaGasto = "";
            obj.ReferenciaGasto =string.IsNullOrEmpty(obj.ReferenciaGasto)? SecuenciasHelper.CrearControlDeGastos():obj.ReferenciaGasto;
            obj.FechaEmision = obj.FechaEmision==DateTime.MinValue? DateTime.Now:obj.FechaEmision;
            var localidadTemp = obj.Localidad;
            obj.Estado = (obj.MontoPagado==obj.MontoTotal && obj.MontoAdeudado==0)?'P': 'I';
          
            obj.MontoDevuelto = obj.MontoDevuelto < 0 ? 0 : obj.MontoDevuelto;
           
            var gasto = base.Insertar(obj);
            GastoHelper.ActualizarImpuestosDeGasto(obj, gastoImpuestoService);
            if (obj.MontoPagado > 0)
            {
                string secuenciaPago = SecuenciasHelper.CrearControlDePagoGastos();
                var pago = new PagoGasto()
                {
                    Activo = true,
                    MonedaId = obj.MonedaId,
                    MonedaGastoId = obj.MonedaId,
                    CreadoPor = obj.CreadoPor,
                    FechaCreacion = DateTime.Now,
                    MontoPagado = obj.MontoPagado,
                    MontoPendiente = obj.MontoAdeudado,
                    MontoTotal = obj.MontoTotal,
                    ReferenciaGasto = obj.ReferenciaGasto,
                    Secuencia = secuenciaPago,
                    SuplidorId = obj.SuplidorId,
                    TasaConversion = obj.Moneda.Tasa,
                    TipoPagoId = obj.TipoPagoId
                };
                    GastoHelper.AplicarPagoGasto(pago);
            }
            return gasto;
        }

      

      

        public override PagedResult<GastoEmpresa> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<GastoEmpresa> resultadoPaginacion = base.ObtenerTodosPaginado( page, size, activos);

           
            return resultadoPaginacion;
        }

        public override GastoEmpresa ObtenerPorId(int id)
        {
            GastoEmpresa resultado = base.ObtenerPorId(id);
            resultado.Impuestos = gastoImpuestoService.ObtenerImpuestosDeGasto(id)?? new List<GastoImpuesto>();
         
            return resultado;

        }

        public override void Eliminar(int id, string userName)
        {
            IPagoGastoService pagoGastoService = new PagoGastoService();
            GastoEmpresa GastoEmpresa = ObtenerPorId(id);
            var pagos = pagoGastoService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>()
                {
                    new FiltroBusqueda()
                    {
                        Campo="ReferenciaGasto",
                        TipoCampo=3,
                        Valor=GastoEmpresa.ReferenciaGasto
                    }
                }
            }).Result;
            
            if (pagos!=null && pagos.Count>0)
                throw new Exception($" El Gasto {GastoEmpresa.ReferenciaGasto} ya ha sido procesada. No se puede anular.");
            
            GastoEmpresa.Estado = 'A';
          
          
            GastoEmpresa.ModificadoPor = userName;
            GastoEmpresa.FechaModificacion = DateTime.Now;
            base.Actualizar(GastoEmpresa);

            var impuestos = gastoImpuestoService.ObtenerImpuestosDeGasto(id);
            impuestos.ForEach(i => {
                gastoImpuestoService.Eliminar(i.Id, userName);
            });
        }

        public override bool Actualizar(GastoEmpresa obj)
        {
            var viejoBalanceDeGastoEmpresa = this.ObtenerPorId(obj.Id);

           
            
            if (viejoBalanceDeGastoEmpresa.MontoPagado > 0 || viejoBalanceDeGastoEmpresa.MontoDado > 0 || viejoBalanceDeGastoEmpresa.MontoAdeudado <= 0)
                throw new Exception("Tiene pagos aplicados. No se puede modificar.");
          
         
            obj.FechaModificacion = DateTime.Now;
            obj.LocalidadId = obj.Localidad.Id;
            var GastoEmpresa = new GastoEmpresa(obj);
            GastoEmpresa.LocalidadId = GastoEmpresa.LocalidadId==0? GastoEmpresa.Localidad.Id: GastoEmpresa.LocalidadId;
          
            var resultado = base.Actualizar(obj);
            GastoHelper.ActualizarImpuestosDeGasto(obj, gastoImpuestoService);
            return resultado;
        }

        public  bool ActualizarMontos(GastoEmpresa obj)
        {

   

            obj.FechaModificacion = DateTime.Now;
            var GastoEmpresa = new GastoEmpresa(obj);
            var resultado = base.Actualizar(obj);
            return resultado;
        }

        public override List<GastoEmpresa> ConsultarPorQuery(string query)
        {


            var resultado = base.ConsultarPorQuery(query);
            resultado.ForEach(f =>
            {
                f.Moneda = monedaService.ObtenerPorId(f.MonedaId);
                f.Suplidor = suplidoresService.ObtenerPorId(f.SuplidorId);
            });
           
            return resultado;
        }

        public override PagedResult<GastoEmpresa> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            
            PagedResult<GastoEmpresa> resultadoPaginacion = base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);

           
            return resultadoPaginacion;
        }

        public override PagedResult<GastoEmpresa> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            var resultado= base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
            
            return resultado;
        }

        public override PagedResult<GastoEmpresa> ObtenerListaPaginadoYFiltro(BuquedaCompleja buqueda)
        {
            PagedResult<GastoEmpresa> resultadoPaginacion= base.ObtenerListaPaginadoYFiltro(buqueda);

          

            return resultadoPaginacion;
        }
    }
}
