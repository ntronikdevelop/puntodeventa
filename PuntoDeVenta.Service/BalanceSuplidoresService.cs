﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.DataAccess.Dto.Helpers;

namespace PuntoDeVenta.Service
{
    public class BalanceSuplidoresService : BaseService<BalanceSuplidores>, IBalanceSuplidoresService
    {
        private IMonedaService monedaService;
        private ISuplidoresService suplidoresService;

        public BalanceSuplidoresService()
        {
            this.monedaService = new MonedaService();
            this.suplidoresService = new SuplidoresService();
        }


    }
}
