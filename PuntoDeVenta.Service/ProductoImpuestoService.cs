﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.Constants.Lib;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class ProductoImpuestoService : BaseService<ProductoImpuesto>, IProductoImpuestoService
    {
        private ProductoImpuestoDao dbService = new ProductoImpuestoDao();


       

        public override ProductoImpuesto ObtenerPorId(int id)
        {
            var resultado= base.ObtenerPorId(id);
            return resultado;
        }

        public List<ProductoImpuesto> ObtenerImpuestosDeProducto(int productoId)
        {
            return dbService.ObtenerImpuestosDeProducto( productoId);
        }

        public List<ProductoImpuesto> ObtenerImpuestosDeProductoS(int[] productosIds)
        {
            return dbService.ObtenerListaFiltrada(new DataAccess.BuquedaCompleja() { 
            Activo=true,
            Filtros= new List<DataAccess.FiltroBusqueda>() 
            {
            new DataAccess.FiltroBusqueda()
            {
            Campo="ProductoId",
            TipoCampo= (int)TiposFiltros.EnLista,
            Valor=string.Join(",",productosIds)
            }
            }
            }).Result;
        }
    }
}
