﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dao;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class ProductoUnidadesService : BaseService<ProductoUnidadesEquivalencia>, IProductosUnidadesService
    {
        private ProductoUnidadesEquivalenciaDao dbService = new ProductoUnidadesEquivalenciaDao();


       

        public override ProductoUnidadesEquivalencia ObtenerPorId(int id)
        {
            var resultado= base.ObtenerPorId(id);
            return resultado;
        }

        public List<ProductoUnidadesEquivalencia> ObtenerUnidadesDeProducto(int productoId)
        {
            return dbService.ObtenerUnidadesDeProducto( productoId);
        }
    }
}
