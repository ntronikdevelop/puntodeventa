﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class PagoGastoService : BaseService<PagoGasto>, IPagoGastoService
    {
        ISuplidoresService SuplidoresService = new SuplidoresService();
        IMonedaService monedaService = new MonedaService();
        IGastoEmpresaService GastoEmpresaService = new GastoEmpresaService();
        IBalanceSuplidoresService balanceSuplidoresService = new BalanceSuplidoresService();


        public PagoGastoService():base(new PagoGastoManager()) { }

        public override PagedResult<PagoGasto> ObtenerTodosPaginadoYFiltro(int page, int size, bool activos, string campo, int tipoCampo, string valor)
        {
            var resultado= base.ObtenerTodosPaginadoYFiltro(page, size, activos, campo, tipoCampo, valor);

  

            return resultado;
        }

        public override PagoGasto ObtenerPorId(int id)
        {

            var resultado = base.ObtenerPorId(id);


          

            return resultado;

        }

        public override bool Actualizar(PagoGasto obj)
        {
            throw new Exception("No se puede actualizar un PagoGasto. Favor Anular;");
        }

        public override PagoGasto Insertar(PagoGasto obj)
        {
            GastoEmpresa gastoEmpresa = GastoEmpresaService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "ReferenciaGasto", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = $"{obj.ReferenciaGasto}" }}
            }).Result.FirstOrDefault();

            if (gastoEmpresa.MontoAdeudado != obj.MontoPendienteActual)
                throw new Exception("El monto pendiente del gasto no está actualizado. favor consultar nuevamente");
            if (gastoEmpresa.MontoAdeudado <=0)
                throw new Exception("El gasto ya está  pagada. No se admiten más PagoGastos.");

            gastoEmpresa.MontoPagado += obj.MontoPagado;
            obj.MontoTotal = gastoEmpresa.MontoTotal;

            gastoEmpresa.MontoPagado = gastoEmpresa.MontoPagado > gastoEmpresa.MontoTotal ? gastoEmpresa.MontoTotal : gastoEmpresa.MontoPagado;
            gastoEmpresa.MontoAdeudado -= obj.MontoPagado;
            gastoEmpresa.MontoAdeudado = gastoEmpresa.MontoAdeudado < 0 ? 0 : gastoEmpresa.MontoAdeudado;
            obj.MontoPendiente = gastoEmpresa.MontoAdeudado;
            if (gastoEmpresa.MontoAdeudado == 0)
                gastoEmpresa.Estado = 'P';
            GastoEmpresaService.ActualizarMontos(gastoEmpresa);
            //var balanceSuplidores = balanceSuplidoresService.ObtenerListaFiltrada(new BuquedaCompleja()
            //{
            //    Activo = true,
            //    Filtros = new List<FiltroBusqueda>() {
            //        new FiltroBusqueda() { Campo = "SuplidorId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.SuplidorId}" },
            //        new FiltroBusqueda() { Campo = "MonedaId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.MonedaId}" }
            //    }
            //}).Result.FirstOrDefault();

            //if (balanceSuplidores != null) {
            //    balanceSuplidores.MontoAdeudado -= obj.MontoPagado;
            //    balanceSuplidores.MontoAdeudado = balanceSuplidores.MontoAdeudado <0 ? 0 : balanceSuplidores.MontoAdeudado;
            //    balanceSuplidoresService.Actualizar(balanceSuplidores);
            //}



            var resultado = base.Insertar(obj);


            return resultado;

        }

        public PagoGasto PagoDeIngresoDeGasto(PagoGasto obj)
        {

            var resultado = base.Insertar(obj);
            return resultado;

        }

        public override void Eliminar(int id, string userNamer)
        {
            var obj = this.ObtenerPorId(id);
            obj.Activo = false;
             
            GastoEmpresa GastoEmpresa = GastoEmpresaService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "ReferenciaGasto", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = $"{obj.ReferenciaGasto}" }}
            }).Result.FirstOrDefault();

            GastoEmpresa.MontoPagado -= obj.MontoPagado;
            GastoEmpresa.MontoPagado = GastoEmpresa.MontoPagado <0 ? 0 : GastoEmpresa.MontoPagado;
            GastoEmpresa.MontoAdeudado += obj.MontoPagado;
            GastoEmpresa.MontoAdeudado = GastoEmpresa.MontoAdeudado >GastoEmpresa.MontoTotal ? GastoEmpresa.MontoTotal : GastoEmpresa.MontoAdeudado;
            if (GastoEmpresa.Estado != 'I')
                GastoEmpresa.Estado = 'I';
            GastoEmpresaService.ActualizarMontos(GastoEmpresa);
            BalanceSuplidores balanceSuplidores = balanceSuplidoresService.ObtenerListaFiltrada(new BuquedaCompleja()
            {
                Activo = true,
                Filtros = new List<FiltroBusqueda>() {
                    new FiltroBusqueda() { Campo = "SuplidorId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.SuplidorId}" },
                    new FiltroBusqueda() { Campo = "MonedaId", TipoCampo = (int)BusquedaComplejaEnum.EQUAL, Valor = $"{obj.MonedaId}" }
                }
            }).Result.FirstOrDefault();

            if (balanceSuplidores != null)
            {
                balanceSuplidores.MontoAdeudado += obj.MontoPagado;
                balanceSuplidoresService.Actualizar(balanceSuplidores);
            }
            obj.ModificadoPor = userNamer;
            obj.FechaModificacion = DateTime.Now;
            base.Actualizar(obj);
        }

       
    }
}
