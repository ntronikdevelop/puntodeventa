﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Helpers;
using PuntoDeVenta.Service.Interfaces;

namespace PuntoDeVenta.Service
{
    public class ZonaService : BaseService<Zona>, IZonaService
    {
        private IMonedaService monedaService;

        public ZonaService()
        {
            this.monedaService = new MonedaService();
        }

        public override PagedResult<Zona> ObtenerTodosPaginado(int page, int size, bool activos)
        {
            PagedResult<Zona> resultadoPaginacion = base.ObtenerTodosPaginado(page,size,activos);

            

            return resultadoPaginacion;
        }

        public override PagedResult<Zona> ObtenerListaFiltradaPorCampoYLimitada(int cantidadMax, string NombreCampo, string caracteresABuscar)
        {
            PagedResult<Zona> resultado= base.ObtenerListaFiltradaPorCampoYLimitada(cantidadMax, NombreCampo, caracteresABuscar);
          

            return resultado;

        }

        public override Zona ObtenerPorId(int id)
        {
            Zona resultado = base.ObtenerPorId(id);
             return resultado;

        }
    }
}
