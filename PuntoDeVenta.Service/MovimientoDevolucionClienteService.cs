﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PuntoDeVenta.DataAccess.Bll;
using PuntoDeVenta.DataAccess.Dto;
using PuntoDeVenta.DataAccess.Dto.Contabilidad;
using PuntoDeVenta.Service.Interfaces;
using PuntoDeVenta.DataAccess.Dao.Interfaces;
using PuntoDeVenta.DataAccess.Dto.Helpers;
using PuntoDeVenta.Service.Helpers.FacturarServicios;
using PuntoDeVenta.DataAccess;

namespace PuntoDeVenta.Service
{
    public class MovimientoDevolucionClienteService : BaseService<MovimientoDevolucionCliente>, IMovimientoDevolucionClienteService
    {

        private IClienteService clienteService;
        private IFacturaService facturaService;
        private IAlmacenService almacenService;
        private IAlmacenProductoService almacenProductos;
        private ISuplidoresService suplidorService;
        private INotaCreditoService notaCreditoService;
        private IProductoService productoService;
        private IDetalleFacturaService detallesFacturas;
        private ILocalidadService localidadService;
        private IMovimientoAlmacenService logMovimientos;
        private IProductosUnidadesService productosUnidades;

        public MovimientoDevolucionClienteService() : base(new MovimientoDevolucionClienteManager())
        {
            this.clienteService = new ClienteService();
            this.almacenService = new AlmacenService();
            this.suplidorService = new SuplidoresService();
            this.notaCreditoService = new NotaCreditoService();
            this.productoService = new ProductoService();
            this.almacenProductos = new AlmacenProductoService();
            this.detallesFacturas = new DetalleFacturaService();
            this.facturaService = new FacturaService();
            this.localidadService = new LocalidadService();
            this.logMovimientos = new MovimientoAlmacenService();
            this.productosUnidades = new ProductoUnidadesService();

        }
        public override PagedResult<MovimientoDevolucionCliente> ObtenerTodosPaginado(int page, int size, bool activos)
        {
              return base.ObtenerTodosPaginado(page,size,activos);
        }

        public override MovimientoDevolucionCliente ObtenerPorId(int id)
        {
            MovimientoDevolucionCliente resultado = base.ObtenerPorId(id);
         
            return resultado;

        }

        public override MovimientoDevolucionCliente Insertar(MovimientoDevolucionCliente obj)
        {
            //            obj.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
            //obj.Cantidad,
            //obj.UnidadId.Value,
            //obj.Producto.ProductoUnidades
            //);
            if (obj.UnidadId.HasValue)
            {
                var unidades = productosUnidades.ObtenerUnidadesDeProducto(obj.ProductoId);
                decimal cantidadPrincipal = ProductosHelper.ConvertirAUnidadPrincipalProducto(obj.Cantidad, obj.UnidadId.Value, unidades);
                MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(obj.AlmacenId.Value, obj.ProductoId, cantidadPrincipal, obj.CreadoPor, true, unidades.FirstOrDefault(u => u.EsPrincipal).UnidadId, 0, "IN", obj.Referencia??obj.NoFactura??string.Empty);
                logMovimientos.Insertar(movimientoAlmacen);

            }
            
            return base.Insertar(obj);
        }

        public override bool Actualizar(MovimientoDevolucionCliente obj)
        {
//            obj.Cantidad = PuntoDeVenta.DataAccess.Dto.Helpers.ProductosHelper.ConvertirAUnidadPrincipalProducto(
//obj.Cantidad,
//obj.UnidadId.Value,
//obj.Producto.ProductoUnidades
//);
            return base.Actualizar(obj);
        }

        public int AplicarDevolucion(DevolucionCliente devolucion, string creadoPor, string codigo)
        {
            if (devolucion.Factura.Estado=='A')
                throw new Exception("Ya la factura fue Anulada. No se puede aplicar devolución.");
            if (FacturaYaProcesada(devolucion.Factura))
                throw new Exception("Factura inválida para nota de crédito");

            int resultado = -1;
            var aDevolver = devolucion.DetallesDevolucion.Where(d => d.CantidadDevolucion > 0).ToList();
            decimal montoTotalADevolver = aDevolver.Sum(d => (((d.MontoTotal)*d.CantidadDevolucion)/d.Cantidad));
            devolucion.Factura = CrearNCF(devolucion.Factura);
            NotaCredito notaCredito = new NotaCredito()
            {
                Activo = true,
                Aplicado = false,
                CreadoPor = creadoPor,
                MonedaId = devolucion.Factura.MonedaId,
                NoFacturaOrigenId = devolucion.Factura.NumeroFactura,
                FechaCreacion = DateTime.Now,
                Secuencia = codigo,
                Monto = montoTotalADevolver,
                TipoNCF=devolucion.Factura.TipoNCF,
                NCF= devolucion.Factura.NCF
            };

            var resultadoNC = notaCreditoService.Insertar(notaCredito);
            resultado = resultadoNC.Id;
         
            foreach (var detalle in aDevolver)
            {
                var productoActual = detalle.Producto ?? productoService.ObtenerPorId(detalle.ProductoId);
                var facturador = InstanciarClaseFacturarProductoServicio.InstanciarFacturador(productoActual);
                MovimientoDevolucionCliente devolucionProducto = new MovimientoDevolucionCliente()
                {
                    Activo = true,
                    AlmacenId = (detalle.Producto.EsServicio ? null : detalle.AlmacenId),
                    Cantidad = detalle.CantidadDevolucion,
                    ClienteId = devolucion.Factura.ClienteId,
                    CreadoPor = creadoPor,
                    Defectuoso = detalle.Defectuoso,
                    NCF=notaCredito.NCF,
                    TipoNCF=notaCredito.TipoNCF,
                    FechaCreacion = DateTime.Now,
                    MontoItbis = detalle.MontoItbis,
                    ProductoId = detalle.ProductoId,
                    UnidadId = (detalle.Producto.EsServicio ? null : detalle.UnidadId),
                    Referencia = codigo,
                    MontoTotal = (detalle.CantidadDevolucion * (detalle.Monto + detalle.MontoItbis)),
                    NoFactura = devolucion.Factura.NumeroFactura
                };
                //if (devolucionProducto.UnidadId.HasValue)
                //{
                //    var unidades =detalle.Producto.ProductoUnidades?? productosUnidades.ConsultarPorExpresion(x => x.ProductoId == devolucionProducto.ProductoId);
                //    decimal cantidadPrincipal = ProductosHelper.ConvertirAUnidadPrincipalProducto(devolucionProducto.Cantidad, devolucionProducto.UnidadId.Value, unidades);
                //    MovimientoAlmacen movimientoAlmacen = new MovimientoAlmacen(devolucionProducto.AlmacenId.Value, devolucionProducto.ProductoId, cantidadPrincipal, devolucionProducto.CreadoPor, true, unidades.Where(u => u.EsPrincipal).FirstOrDefault().UnidadId, 0, "IN");
                //    logMovimientos.Insertar(movimientoAlmacen);

                //}
                base.Insertar(devolucionProducto);

                detalle.Cantidad = detalle.CantidadDevolucion;
                facturador.DevolverProductoServicio(devolucion.Factura.LocalidadId, detalle, productoService, almacenProductos, detallesFacturas, devolucion.Factura);
                //InventarioEnAlmacenHelper.SumarInventarioAlAlmacen(detalle);
            }
            devolucion.Factura.MontoDevuelto = montoTotalADevolver;
            devolucion.Factura.MontoAdeudado -= montoTotalADevolver;

            if(devolucion.Factura.MontoAdeudado<0)
                devolucion.Factura.MontoAdeudado = 0;

            this.facturaService.ActualizarMontos(devolucion.Factura);

            return resultado;
        }

        private Factura CrearNCF(Factura obj)
        {
            if (!string.IsNullOrEmpty(obj.TipoNCF) && obj.TipoNCF != "NA")
            {
                var ncfService = new ControlNCFService();
                ControlNCF control = ncfService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "Tipo", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.TipoNCF } }
                }).Result.FirstOrDefault();
                if (control.Cantidad <= 0)
                    throw new Exception($"Ya no quedan NCF disponibles para {control.Nombre}.");
                obj.NCF = $"{control.Serie}{control.Tipo}{string.Format("{0:00000000}", control.Secuencia)}";
                control.Secuencia++;
                control.Cantidad--;
                control.ControlNumerico++;
                ncfService.Actualizar(control);
            }
            return obj;
        }

        private bool FacturaYaProcesada(Factura obj)
        {
            bool result = true;
            if (string.IsNullOrEmpty(obj.NumeroFactura))
            {
                return result;
            }
               
                NotaCredito nc = notaCreditoService.ObtenerListaFiltrada(new BuquedaCompleja()
                {
                    Activo = true,
                    Filtros = new List<FiltroBusqueda>() { new FiltroBusqueda() { Campo = "NoFacturaOrigenId", TipoCampo = (int)BusquedaComplejaEnum.EQUALSTRING, Valor = obj.NumeroFactura } }
                }).Result.FirstOrDefault();
            if (nc != null && nc.Id > 0)
                result = true;
            else
                result = false;
            
            return result;
        }

        public NotaCredito ObtenerNotaDeCredito(string referencia)
        {
            NotaCredito resultado = notaCreditoService.ObtenerPorSecuencia( referencia);
            if (resultado == null)
                throw new Exception($"La nota de credito de secuencia {referencia} no existe.");
            return resultado;
        }
    }
}
